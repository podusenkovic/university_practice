/*
 *
 * Stateflow code generation for chart:
 *    mark_cep/Chart
 * 
 * Target Name                          : sfun
 * Model Version                        : 1.188
 * Stateflow Version                    : 5.0.0.13.00.1.000001
 * Date of code generation              : 28-Mar-2005 15:43:44
 *
 */

#include "mark_cep_sfun.h"
#include "mark_cep_sfun_c1.h"
#define mexPrintf                       sf_mex_printf
#ifdef printf
#undef printf
#endif
#define printf                          sf_mex_printf
#define CHARTINSTANCE_CHARTNUMBER       (chartInstance.chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER    (chartInstance.instanceNumber)
#include "mark_cep_sfun_debug_macros.h"
#define IN_NO_ACTIVE_CHILD              (0)
#define IN_m0_c1_s1_Rain                1
#define IN_m0_c1_s2_Snow                2
#define IN_m0_c1_s3_Sunny               3
static SFmark_cep_sfun_c1InstanceStruct chartInstance;
#define OutputData_m0_c1_d1_nrain       (((real_T *)(ssGetOutputPortSignal(chartInstance.S,1)))[0])
#define OutputData_m0_c1_d2_nsunny      (((real_T *)(ssGetOutputPortSignal(chartInstance.S,2)))[0])
#define OutputData_m0_c1_d3_nsnow       (((real_T *)(ssGetOutputPortSignal(chartInstance.S,3)))[0])
#define OutputData_m0_c1_d4_N           (((real_T *)(ssGetOutputPortSignal(chartInstance.S,4)))[0])

static void enter_atomic_m0_c1_s1_Rain(void);
static void exit_atomic_m0_c1_s1_Rain(void);
static void enter_atomic_m0_c1_s2_Snow(void);
static void exit_atomic_m0_c1_s2_Snow(void);
static void enter_atomic_m0_c1_s3_Sunny(void);
static void exit_atomic_m0_c1_s3_Sunny(void);
void mark_cep_sfun_c1(void)
{
  real_T __sfTemp1;
  real_T __sfTemp2;
  real_T __sfTemp3;
  real_T __sfTemp4;
  real_T __sfTemp5;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG,0);
  if(chartInstance.State.is_active_mark_cep_sfun_c1 == 0) {
    _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG,0);
    chartInstance.State.is_active_mark_cep_sfun_c1 = 1;
    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG,0);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,0);
    _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,0);
    _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,0);
    _SFD_CT_CALL(TRANSITION_BEFORE_TRANS_ACTION_TAG,0);
    OutputData_m0_c1_d2_nsunny = 0.0;
    _SFD_CCT_CALL(TRANSITION_TRANSITION_ACTION_COVERAGE_TAG,0,0);
    OutputData_m0_c1_d1_nrain = 0.0;
    _SFD_CCT_CALL(TRANSITION_TRANSITION_ACTION_COVERAGE_TAG,0,1);
    OutputData_m0_c1_d3_nsnow = 0.0;
    _SFD_CCT_CALL(TRANSITION_TRANSITION_ACTION_COVERAGE_TAG,0,2);
    OutputData_m0_c1_d4_N = 0.0;
    _SFD_CCT_CALL(TRANSITION_TRANSITION_ACTION_COVERAGE_TAG,0,3);
    _SFD_CT_CALL(TRANSITION_AFTER_TRANS_ACTION_TAG,0);
    _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,0);
    enter_atomic_m0_c1_s3_Sunny();
  } else {
    switch(chartInstance.State.is_mark_cep_sfun_c1) {
     case IN_m0_c1_s1_Rain:
      CV_CHART_EVAL(0,0,1);
      _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG,2);
      _SFD_CCS_CALL(STATE_DURING_COVERAGE_TAG,2,0);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,11);
      _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,11);
      _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,11);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG,10);
      sf_mex_assign_from_mx_array(sf_matlab_evalin(0, 1, "rand(1)>0.5"),
       &__sfTemp4, 0, 0, 0);
      if(CV_TRANSITION_EVAL(10, _SFD_CCP_CALL(10,0,(__sfTemp4 != 0.0)))) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,10);
        _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,10);
        _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,10);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG,13);
        sf_mex_assign_from_mx_array(sf_matlab_evalin(0, 1, "rand(1)>0.5"),
         &__sfTemp5, 0, 0, 0);
        if(CV_TRANSITION_EVAL(13, _SFD_CCP_CALL(13,0,(__sfTemp5 != 0.0)))) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,13);
          _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,13);
          _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,13);
          exit_atomic_m0_c1_s1_Rain();
          enter_atomic_m0_c1_s3_Sunny();
        } else {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,7);
          _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,7);
          _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,7);
          exit_atomic_m0_c1_s1_Rain();
          enter_atomic_m0_c1_s2_Snow();
        }
      } else {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,9);
        _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,9);
        _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,9);
        exit_atomic_m0_c1_s1_Rain();
        enter_atomic_m0_c1_s1_Rain();
      }
      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG,2);
      break;
     case IN_m0_c1_s2_Snow:
      CV_CHART_EVAL(0,0,2);
      _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG,1);
      _SFD_CCS_CALL(STATE_DURING_COVERAGE_TAG,1,0);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,4);
      _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,4);
      _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,4);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG,5);
      sf_mex_assign_from_mx_array(sf_matlab_evalin(0, 1, "rand(1)>0.5"),
       &__sfTemp2, 0, 0, 0);
      if(CV_TRANSITION_EVAL(5, _SFD_CCP_CALL(5,0,(__sfTemp2 != 0.0)))) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,5);
        _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,5);
        _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,5);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG,8);
        sf_mex_assign_from_mx_array(sf_matlab_evalin(0, 1, "rand(1)>0.5"),
         &__sfTemp3, 0, 0, 0);
        if(CV_TRANSITION_EVAL(8, _SFD_CCP_CALL(8,0,(__sfTemp3 != 0.0)))) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,8);
          _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,8);
          _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,8);
          exit_atomic_m0_c1_s2_Snow();
          enter_atomic_m0_c1_s3_Sunny();
        } else {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,6);
          _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,6);
          _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,6);
          exit_atomic_m0_c1_s2_Snow();
          enter_atomic_m0_c1_s1_Rain();
        }
      } else {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,12);
        _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,12);
        _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,12);
        exit_atomic_m0_c1_s2_Snow();
        enter_atomic_m0_c1_s2_Snow();
      }
      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG,1);
      break;
     case IN_m0_c1_s3_Sunny:
      CV_CHART_EVAL(0,0,3);
      _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG,0);
      _SFD_CCS_CALL(STATE_DURING_COVERAGE_TAG,0,0);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,3);
      _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,3);
      _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,3);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG,2);
      sf_mex_assign_from_mx_array(sf_matlab_evalin(0, 1, "rand(1)>0.5"),
       &__sfTemp1, 0, 0, 0);
      if(CV_TRANSITION_EVAL(2, _SFD_CCP_CALL(2,0,(__sfTemp1 != 0.0)))) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,2);
        _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,2);
        _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,2);
        exit_atomic_m0_c1_s3_Sunny();
        enter_atomic_m0_c1_s1_Rain();
      } else {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,1);
        _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,1);
        _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,1);
        exit_atomic_m0_c1_s3_Sunny();
        enter_atomic_m0_c1_s2_Snow();
      }
      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG,0);
      break;
     default:
      CV_CHART_EVAL(0,0,0);
      break;
    }
  }
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG,0);
}

static void enter_atomic_m0_c1_s1_Rain(void)
{
  _SFD_CS_CALL(STATE_ENTER_ENTRY_FUNCTION_TAG,2);
  _SFD_CCS_CALL(STATE_ENTRY_COVERAGE_TAG,2,0);
  chartInstance.State.is_mark_cep_sfun_c1 = IN_m0_c1_s1_Rain;
  _SFD_CS_CALL(STATE_ACTIVE_TAG,2);
  _SFD_CS_CALL(STATE_BEFORE_ENTRY_ACTION_TAG,2);
  ++OutputData_m0_c1_d1_nrain;
  _SFD_CCS_CALL(STATE_ENTRY_COVERAGE_TAG,2,1);
  ++OutputData_m0_c1_d4_N;
  _SFD_CCS_CALL(STATE_ENTRY_COVERAGE_TAG,2,2);
  _SFD_CS_CALL(STATE_AFTER_ENTRY_ACTION_TAG,2);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG,2);
}

static void exit_atomic_m0_c1_s1_Rain(void)
{
  _SFD_CS_CALL(STATE_ENTER_EXIT_FUNCTION_TAG,2);
  _SFD_CCS_CALL(STATE_EXIT_COVERAGE_TAG,2,0);
  chartInstance.State.is_mark_cep_sfun_c1 = IN_NO_ACTIVE_CHILD;
  _SFD_CS_CALL(STATE_INACTIVE_TAG,2);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG,2);
}

static void enter_atomic_m0_c1_s2_Snow(void)
{
  _SFD_CS_CALL(STATE_ENTER_ENTRY_FUNCTION_TAG,1);
  _SFD_CCS_CALL(STATE_ENTRY_COVERAGE_TAG,1,0);
  chartInstance.State.is_mark_cep_sfun_c1 = IN_m0_c1_s2_Snow;
  _SFD_CS_CALL(STATE_ACTIVE_TAG,1);
  _SFD_CS_CALL(STATE_BEFORE_ENTRY_ACTION_TAG,1);
  ++OutputData_m0_c1_d3_nsnow;
  _SFD_CCS_CALL(STATE_ENTRY_COVERAGE_TAG,1,1);
  ++OutputData_m0_c1_d4_N;
  _SFD_CCS_CALL(STATE_ENTRY_COVERAGE_TAG,1,2);
  _SFD_CS_CALL(STATE_AFTER_ENTRY_ACTION_TAG,1);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG,1);
}

static void exit_atomic_m0_c1_s2_Snow(void)
{
  _SFD_CS_CALL(STATE_ENTER_EXIT_FUNCTION_TAG,1);
  _SFD_CCS_CALL(STATE_EXIT_COVERAGE_TAG,1,0);
  chartInstance.State.is_mark_cep_sfun_c1 = IN_NO_ACTIVE_CHILD;
  _SFD_CS_CALL(STATE_INACTIVE_TAG,1);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG,1);
}

static void enter_atomic_m0_c1_s3_Sunny(void)
{
  _SFD_CS_CALL(STATE_ENTER_ENTRY_FUNCTION_TAG,0);
  _SFD_CCS_CALL(STATE_ENTRY_COVERAGE_TAG,0,0);
  chartInstance.State.is_mark_cep_sfun_c1 = IN_m0_c1_s3_Sunny;
  _SFD_CS_CALL(STATE_ACTIVE_TAG,0);
  _SFD_CS_CALL(STATE_BEFORE_ENTRY_ACTION_TAG,0);
  ++OutputData_m0_c1_d2_nsunny;
  _SFD_CCS_CALL(STATE_ENTRY_COVERAGE_TAG,0,1);
  ++OutputData_m0_c1_d4_N;
  _SFD_CCS_CALL(STATE_ENTRY_COVERAGE_TAG,0,2);
  _SFD_CS_CALL(STATE_AFTER_ENTRY_ACTION_TAG,0);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG,0);
}

static void exit_atomic_m0_c1_s3_Sunny(void)
{
  _SFD_CS_CALL(STATE_ENTER_EXIT_FUNCTION_TAG,0);
  _SFD_CCS_CALL(STATE_EXIT_COVERAGE_TAG,0,0);
  chartInstance.State.is_mark_cep_sfun_c1 = IN_NO_ACTIVE_CHILD;
  _SFD_CS_CALL(STATE_INACTIVE_TAG,0);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG,0);
}

void sf_mark_cep_sfun_c1_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3058405464U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3241962561U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1379036277U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2469195500U);
}
static void restore_last_major_step( void *chartInstanceVoidPtr )
{
  chartInstance.State = chartInstance.StateBuffer;
  OutputData_m0_c1_d1_nrain = (chartInstance.OutputDataBuffer).m0_c1_d1_nrain;

  OutputData_m0_c1_d2_nsunny = (chartInstance.OutputDataBuffer).m0_c1_d2_nsunny;

  OutputData_m0_c1_d3_nsnow = (chartInstance.OutputDataBuffer).m0_c1_d3_nsnow;

  OutputData_m0_c1_d4_N = (chartInstance.OutputDataBuffer).m0_c1_d4_N;

  sf_debug_restore_previous_state_configuration(_mark_cepMachineNumber_,chartInstance.chartNumber,chartInstance.instanceNumber);
}
static void restore_before_last_major_step( void *chartInstanceVoidPtr )
{
  chartInstance.State = chartInstance.StateBuffer2;
  OutputData_m0_c1_d1_nrain = (chartInstance.OutputDataBuffer2).m0_c1_d1_nrain;

  OutputData_m0_c1_d2_nsunny = (chartInstance.OutputDataBuffer2).m0_c1_d2_nsunny;

  OutputData_m0_c1_d3_nsnow = (chartInstance.OutputDataBuffer2).m0_c1_d3_nsnow;

  OutputData_m0_c1_d4_N = (chartInstance.OutputDataBuffer2).m0_c1_d4_N;

  sf_debug_restore_previous_state_configuration2(_mark_cepMachineNumber_,chartInstance.chartNumber,chartInstance.instanceNumber);
}
static void reset_major_step_config( void *chartInstanceVoidPtr)
{
  memset((void *)&(chartInstance.StateBuffer), 0,
   sizeof(chartInstance.StateBuffer));
  memset((void *)&(chartInstance.StateBuffer2), 0,
   sizeof(chartInstance.StateBuffer2));
  memset((void
    *)&(chartInstance.OutputDataBuffer),0,sizeof(chartInstance.OutputDataBuffer));
  memset((void
    *)&(chartInstance.OutputDataBuffer2),0,sizeof(chartInstance.OutputDataBuffer2));
}
static void store_current_config( void *chartInstanceVoidPtr )
{
  chartInstance.StateBuffer2 = chartInstance.StateBuffer;
  chartInstance.StateBuffer = chartInstance.State;
  chartInstance.OutputDataBuffer2 = chartInstance.OutputDataBuffer;
  chartInstance.OutputDataBuffer.m0_c1_d1_nrain =
    (real_T)OutputData_m0_c1_d1_nrain;

  chartInstance.OutputDataBuffer.m0_c1_d2_nsunny =
    (real_T)OutputData_m0_c1_d2_nsunny;

  chartInstance.OutputDataBuffer.m0_c1_d3_nsnow =
    (real_T)OutputData_m0_c1_d3_nsnow;

  chartInstance.OutputDataBuffer.m0_c1_d4_N = (real_T)OutputData_m0_c1_d4_N;

  sf_debug_store_current_state_configuration(_mark_cepMachineNumber_,chartInstance.chartNumber,chartInstance.instanceNumber);
}
/*
 * Chart initialization function
 */
/* work around the buggy macro in simstruc.h until it is fixed */
#define cdrGetOutputPortReusable(S,port) \
  ( (S)->portInfo.outputs[(port)].attributes.optimOpts != \
   SS_NOT_REUSABLE_AND_GLOBAL )

static void initialize_mark_cep_sfun_c1( SimStruct *S)
{
  reset_major_step_config(NULL);

  {
    if(!cdrGetOutputPortReusable(S,1)) {
      OutputData_m0_c1_d1_nrain = 0.0;
    }
    if(!cdrGetOutputPortReusable(S,2)) {
      OutputData_m0_c1_d2_nsunny = 0.0;
    }
    if(!cdrGetOutputPortReusable(S,3)) {
      OutputData_m0_c1_d3_nsnow = 0.0;
    }
    if(!cdrGetOutputPortReusable(S,4)) {
      OutputData_m0_c1_d4_N = 0.0;
    }
  }

  /* Initialize chart's state configuration */
  memset((void*)&(chartInstance.State),0,sizeof(chartInstance.State));

  {
    if(ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting */
      if(!sim_mode_is_rtw_gen(S)) {
        {
          unsigned int chartAlreadyPresent;
          chartAlreadyPresent =
            sf_debug_initialize_chart(_mark_cepMachineNumber_,
            1,
            3,
            14,
            4,
            0,
            0,
            0,
            0,
            &(chartInstance.chartNumber),
            &(chartInstance.instanceNumber),
            ssGetPath((SimStruct *)S),
            (void *)S);
          if(chartAlreadyPresent==0) {
            /* this is the first instance */
            sf_debug_set_chart_disable_implicit_casting(_mark_cepMachineNumber_,chartInstance.chartNumber,0);
            sf_debug_set_chart_event_thresholds(_mark_cepMachineNumber_,
             chartInstance.chartNumber,
             0,
             0,
             0);

            _SFD_SET_DATA_PROPS(1,
             2,
             0,
             1,
             SF_DOUBLE,
             0,
             NULL,
             0,
             0.0,
             1.0,
             0);
            _SFD_SET_DATA_PROPS(3,
             2,
             0,
             1,
             SF_DOUBLE,
             0,
             NULL,
             0,
             0.0,
             1.0,
             0);
            _SFD_SET_DATA_PROPS(2,
             2,
             0,
             1,
             SF_DOUBLE,
             0,
             NULL,
             0,
             0.0,
             1.0,
             0);
            _SFD_SET_DATA_PROPS(0,
             2,
             0,
             1,
             SF_DOUBLE,
             0,
             NULL,
             0,
             0.0,
             1.0,
             0);
            _SFD_STATE_INFO(2,0,0);
            _SFD_STATE_INFO(1,0,0);
            _SFD_STATE_INFO(0,0,0);
            _SFD_CH_SUBSTATE_COUNT(3);
            _SFD_CH_SUBSTATE_DECOMP(0);
            _SFD_CH_SUBSTATE_INDEX(0,2);
            _SFD_CH_SUBSTATE_INDEX(1,1);
            _SFD_CH_SUBSTATE_INDEX(2,0);
            _SFD_ST_SUBSTATE_COUNT(2,0);
            _SFD_ST_SUBSTATE_COUNT(1,0);
            _SFD_ST_SUBSTATE_COUNT(0,0);
          }
          _SFD_CV_INIT_CHART(3,1,0,0);
          {
            _SFD_CV_INIT_STATE(2,0,0,0,0,0,NULL,NULL);
          }
          {
            _SFD_CV_INIT_STATE(1,0,0,0,0,0,NULL,NULL);
          }
          {
            _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
          }

          _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

          _SFD_CV_INIT_TRANS(3,0,NULL,NULL,0,NULL);

          {
            static unsigned int sStartGuardMap[] = {1};
            static unsigned int sEndGuardMap[] = {18};
            static int sPostFixPredicateTree[] = {0};
            _SFD_CV_INIT_TRANS(2,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,&(sPostFixPredicateTree[0]));
          }
          _SFD_CV_INIT_TRANS(1,0,NULL,NULL,0,NULL);

          _SFD_CV_INIT_TRANS(11,0,NULL,NULL,0,NULL);

          {
            static unsigned int sStartGuardMap[] = {1};
            static unsigned int sEndGuardMap[] = {18};
            static int sPostFixPredicateTree[] = {0};
            _SFD_CV_INIT_TRANS(10,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,&(sPostFixPredicateTree[0]));
          }
          _SFD_CV_INIT_TRANS(9,0,NULL,NULL,0,NULL);

          _SFD_CV_INIT_TRANS(7,0,NULL,NULL,0,NULL);

          {
            static unsigned int sStartGuardMap[] = {1};
            static unsigned int sEndGuardMap[] = {18};
            static int sPostFixPredicateTree[] = {0};
            _SFD_CV_INIT_TRANS(13,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,&(sPostFixPredicateTree[0]));
          }
          _SFD_CV_INIT_TRANS(4,0,NULL,NULL,0,NULL);

          _SFD_CV_INIT_TRANS(12,0,NULL,NULL,0,NULL);

          {
            static unsigned int sStartGuardMap[] = {1};
            static unsigned int sEndGuardMap[] = {18};
            static int sPostFixPredicateTree[] = {0};
            _SFD_CV_INIT_TRANS(5,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,&(sPostFixPredicateTree[0]));
          }
          {
            static unsigned int sStartGuardMap[] = {1};
            static unsigned int sEndGuardMap[] = {18};
            static int sPostFixPredicateTree[] = {0};
            _SFD_CV_INIT_TRANS(8,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,&(sPostFixPredicateTree[0]));
          }
          _SFD_CV_INIT_TRANS(6,0,NULL,NULL,0,NULL);

          _SFD_STATE_COV_WTS(2,3,1,1);
          if(chartAlreadyPresent==0)
          {
            static unsigned int sStartEntryMap[] = {0,13,22};
            static unsigned int sEndEntryMap[] = {0,20,25};
            static unsigned int sStartDuringMap[] = {0};
            static unsigned int sEndDuringMap[] = {0};
            static unsigned int sStartExitMap[] = {0};
            static unsigned int sEndExitMap[] = {0};

            _SFD_STATE_COV_MAPS(2,
             3,&(sStartEntryMap[0]),&(sEndEntryMap[0]),
             1,&(sStartDuringMap[0]),&(sEndDuringMap[0]),
             1,&(sStartExitMap[0]),&(sEndExitMap[0]));
          }
          _SFD_STATE_COV_WTS(1,3,1,1);
          if(chartAlreadyPresent==0)
          {
            static unsigned int sStartEntryMap[] = {0,13,22};
            static unsigned int sEndEntryMap[] = {0,20,25};
            static unsigned int sStartDuringMap[] = {0};
            static unsigned int sEndDuringMap[] = {0};
            static unsigned int sStartExitMap[] = {0};
            static unsigned int sEndExitMap[] = {0};

            _SFD_STATE_COV_MAPS(1,
             3,&(sStartEntryMap[0]),&(sEndEntryMap[0]),
             1,&(sStartDuringMap[0]),&(sEndDuringMap[0]),
             1,&(sStartExitMap[0]),&(sEndExitMap[0]));
          }
          _SFD_STATE_COV_WTS(0,3,1,1);
          if(chartAlreadyPresent==0)
          {
            static unsigned int sStartEntryMap[] = {0,14,24};
            static unsigned int sEndEntryMap[] = {0,22,27};
            static unsigned int sStartDuringMap[] = {0};
            static unsigned int sEndDuringMap[] = {0};
            static unsigned int sStartExitMap[] = {0};
            static unsigned int sEndExitMap[] = {0};

            _SFD_STATE_COV_MAPS(0,
             3,&(sStartEntryMap[0]),&(sEndEntryMap[0]),
             1,&(sStartDuringMap[0]),&(sEndDuringMap[0]),
             1,&(sStartExitMap[0]),&(sEndExitMap[0]));
          }
          _SFD_TRANS_COV_WTS(0,0,0,0,4);
          if(chartAlreadyPresent==0)
          {
            static unsigned int sStartTransitionActionMap[] = {2,12,21,30};
            static unsigned int sEndTransitionActionMap[] = {11,20,29,34};
            _SFD_TRANS_COV_MAPS(0,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL,
             4,&(sStartTransitionActionMap[0]),&(sEndTransitionActionMap[0]));
          }
          _SFD_TRANS_COV_WTS(3,0,0,0,0);
          if(chartAlreadyPresent==0)
          {
            _SFD_TRANS_COV_MAPS(3,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL);
          }
          _SFD_TRANS_COV_WTS(2,0,1,0,0);
          if(chartAlreadyPresent==0)
          {
            static unsigned int sStartGuardMap[] = {1};
            static unsigned int sEndGuardMap[] = {18};
            _SFD_TRANS_COV_MAPS(2,
             0,NULL,NULL,
             1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
             0,NULL,NULL,
             0,NULL,NULL);
          }
          _SFD_TRANS_COV_WTS(1,0,0,0,0);
          if(chartAlreadyPresent==0)
          {
            _SFD_TRANS_COV_MAPS(1,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL);
          }
          _SFD_TRANS_COV_WTS(11,0,0,0,0);
          if(chartAlreadyPresent==0)
          {
            _SFD_TRANS_COV_MAPS(11,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL);
          }
          _SFD_TRANS_COV_WTS(10,0,1,0,0);
          if(chartAlreadyPresent==0)
          {
            static unsigned int sStartGuardMap[] = {1};
            static unsigned int sEndGuardMap[] = {18};
            _SFD_TRANS_COV_MAPS(10,
             0,NULL,NULL,
             1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
             0,NULL,NULL,
             0,NULL,NULL);
          }
          _SFD_TRANS_COV_WTS(9,0,0,0,0);
          if(chartAlreadyPresent==0)
          {
            _SFD_TRANS_COV_MAPS(9,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL);
          }
          _SFD_TRANS_COV_WTS(7,0,0,0,0);
          if(chartAlreadyPresent==0)
          {
            _SFD_TRANS_COV_MAPS(7,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL);
          }
          _SFD_TRANS_COV_WTS(13,0,1,0,0);
          if(chartAlreadyPresent==0)
          {
            static unsigned int sStartGuardMap[] = {1};
            static unsigned int sEndGuardMap[] = {18};
            _SFD_TRANS_COV_MAPS(13,
             0,NULL,NULL,
             1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
             0,NULL,NULL,
             0,NULL,NULL);
          }
          _SFD_TRANS_COV_WTS(4,0,0,0,0);
          if(chartAlreadyPresent==0)
          {
            _SFD_TRANS_COV_MAPS(4,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL);
          }
          _SFD_TRANS_COV_WTS(12,0,0,0,0);
          if(chartAlreadyPresent==0)
          {
            _SFD_TRANS_COV_MAPS(12,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL);
          }
          _SFD_TRANS_COV_WTS(5,0,1,0,0);
          if(chartAlreadyPresent==0)
          {
            static unsigned int sStartGuardMap[] = {1};
            static unsigned int sEndGuardMap[] = {18};
            _SFD_TRANS_COV_MAPS(5,
             0,NULL,NULL,
             1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
             0,NULL,NULL,
             0,NULL,NULL);
          }
          _SFD_TRANS_COV_WTS(8,0,1,0,0);
          if(chartAlreadyPresent==0)
          {
            static unsigned int sStartGuardMap[] = {1};
            static unsigned int sEndGuardMap[] = {18};
            _SFD_TRANS_COV_MAPS(8,
             0,NULL,NULL,
             1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
             0,NULL,NULL,
             0,NULL,NULL);
          }
          _SFD_TRANS_COV_WTS(6,0,0,0,0);
          if(chartAlreadyPresent==0)
          {
            _SFD_TRANS_COV_MAPS(6,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL);
          }
          _SFD_SET_DATA_VALUE_PTR(1,(void *)(&OutputData_m0_c1_d1_nrain));
          _SFD_SET_DATA_VALUE_PTR(3,(void *)(&OutputData_m0_c1_d2_nsunny));
          _SFD_SET_DATA_VALUE_PTR(2,(void *)(&OutputData_m0_c1_d3_nsnow));
          _SFD_SET_DATA_VALUE_PTR(0,(void *)(&OutputData_m0_c1_d4_N));
        }
      }
    }else{
      sf_debug_reset_current_state_configuration(_mark_cepMachineNumber_,chartInstance.chartNumber,chartInstance.instanceNumber);
    }
  }
  chartInstance.chartInfo.chartInitialized = 1;
}

void mark_cep_sfun_c1_sizes_registry(SimStruct *S)
{
  ssSetNumInputPorts((SimStruct *)S, 1);
  ssSetInputPortWidth((SimStruct *)S,0,1);
  ssSetInputPortDirectFeedThrough((SimStruct *)S,0,1);
  ssSetNumOutputPorts((SimStruct *)S, 5);
  ssSetOutputPortDataType((SimStruct *)S,0,SS_DOUBLE);
  ssSetOutputPortWidth((SimStruct *)S,0,1);
  ssSetOutputPortDataType((SimStruct *)S,1,SS_DOUBLE); /* OutputData_m0_c1_d1_nrain */
  ssSetOutputPortWidth((SimStruct *)S,1,1);
  ssSetOutputPortDataType((SimStruct *)S,2,SS_DOUBLE); /* OutputData_m0_c1_d2_nsunny */
  ssSetOutputPortWidth((SimStruct *)S,2,1);
  ssSetOutputPortDataType((SimStruct *)S,3,SS_DOUBLE); /* OutputData_m0_c1_d3_nsnow */
  ssSetOutputPortWidth((SimStruct *)S,3,1);
  ssSetOutputPortDataType((SimStruct *)S,4,SS_DOUBLE); /* OutputData_m0_c1_d4_N */
  ssSetOutputPortWidth((SimStruct *)S,4,1);
  if(sim_mode_is_rtw_gen(S)) {
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable("mark_cep",1);
    ssSetStateflowIsInlinable((SimStruct *)S,chartIsInlinable);
    if(chartIsInlinable) {
      sf_mark_chart_reusable_outputs((SimStruct *)S,"mark_cep",1,4);
    }
    {
      int dtId;
      char *chartInstanceTypedefName =
        sf_chart_instance_typedef_name("mark_cep",1);
      dtId = ssRegisterDataType(S, chartInstanceTypedefName);
      if (dtId == INVALID_DTYPE_ID ) return;
      /* Register the size of the udt */
      if (!ssSetDataTypeSize(S, dtId, 8)) return;
      if(!ssSetNumDWork(S,1)) return;
      ssSetDWorkDataType(S, 0, dtId);
      ssSetDWorkWidth(S, 0, 1);
      ssSetDWorkName(S, 0, "ChartInstance"); /*optional name, less than 16 chars*/
    }
  }
  ssSetChecksum0(S,(3058405464U));
  ssSetChecksum1(S,(3241962561U));
  ssSetChecksum2(S,(1379036277U));
  ssSetChecksum3(S,(2469195500U));
}

void terminate_mark_cep_sfun_c1(SimStruct *S)
{
}
static void mdlRTW_mark_cep_sfun_c1(SimStruct *S)
{
}

void sf_mark_cep_sfun_c1( void *);
void mark_cep_sfun_c1_registry(SimStruct *S)
{
  chartInstance.chartInfo.chartInstance = NULL;
  chartInstance.chartInfo.chartInitialized = 0;
  chartInstance.chartInfo.sFunctionGateway = sf_mark_cep_sfun_c1;
  chartInstance.chartInfo.initializeChart = initialize_mark_cep_sfun_c1;
  chartInstance.chartInfo.terminateChart = terminate_mark_cep_sfun_c1;
  chartInstance.chartInfo.mdlRTW = mdlRTW_mark_cep_sfun_c1;
  chartInstance.chartInfo.restoreLastMajorStepConfiguration =
    restore_last_major_step;
  chartInstance.chartInfo.restoreBeforeLastMajorStepConfiguration =
    restore_before_last_major_step;
  chartInstance.chartInfo.storeCurrentConfiguration = store_current_config;
  chartInstance.chartInfo.sampleTime = CONTINUOUS_SAMPLE_TIME;
  chartInstance.S = S;
  ssSetUserData((SimStruct *)S,(void *)(&(chartInstance.chartInfo))); /* register the chart instance with simstruct */
  ssSetSampleTime((SimStruct *)S, 0, chartInstance.chartInfo.sampleTime);
  if (chartInstance.chartInfo.sampleTime == INHERITED_SAMPLE_TIME) {
    ssSetOffsetTime((SimStruct *)S, 0, FIXED_IN_MINOR_STEP_OFFSET);
  } else if (chartInstance.chartInfo.sampleTime == CONTINUOUS_SAMPLE_TIME) {
    ssSetOffsetTime((SimStruct *)S, 0, 0.0);
  }
  ssSetCallSystemOutput((SimStruct *)S,0);
}

void sf_mark_cep_sfun_c1(void *chartInstanceVoidPtr)
{
  /* Save current event being processed */
  uint8_T previousEvent;
  previousEvent = _sfEvent_;

  /* Update Stateflow time variable */
  _sfTime_ = ssGetT(chartInstance.S);

  /* Call this chart */
  _sfEvent_ = CALL_EVENT;
  mark_cep_sfun_c1();
  _sfEvent_ = previousEvent;
}

