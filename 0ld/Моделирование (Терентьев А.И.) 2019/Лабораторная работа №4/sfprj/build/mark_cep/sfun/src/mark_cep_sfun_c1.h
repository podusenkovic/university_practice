/*
 *
 * Stateflow code generation for chart:
 *    mark_cep/Chart
 * 
 * Target Name                          : sfun
 * Model Version                        : 1.188
 * Stateflow Version                    : 5.0.0.13.00.1.000001
 * Date of code generation              : 28-Mar-2005 15:43:44
 *
 */

#ifndef __mark_cep_sfun_c1_h__
#define __mark_cep_sfun_c1_h__
#include "sfc_sf.h"
#include "sfc_mex.h"
extern void sf_mark_cep_sfun_c1_get_check_sum(mxArray *plhs[]);
extern void mark_cep_sfun_c1_registry(SimStruct *simStructPtr);
extern void mark_cep_sfun_c1_sizes_registry(SimStruct *simStructPtr);
extern void mark_cep_sfun_c1(void);

typedef struct SFmark_cep_sfun_c1OutputDataStruct{
  real_T m0_c1_d1_nrain;
  real_T m0_c1_d2_nsunny;
  real_T m0_c1_d3_nsnow;
  real_T m0_c1_d4_N;
} SFmark_cep_sfun_c1OutputDataStruct;
typedef struct SFmark_cep_sfun_c1StateStruct{
  unsigned char is_active_mark_cep_sfun_c1;
  unsigned char is_mark_cep_sfun_c1;
} SFmark_cep_sfun_c1StateStruct;

typedef struct S_SFmark_cep_sfun_c1InstanceStruct {
  SFmark_cep_sfun_c1OutputDataStruct OutputDataBuffer;
  SFmark_cep_sfun_c1OutputDataStruct OutputDataBuffer2;
  SFmark_cep_sfun_c1StateStruct State;
  SFmark_cep_sfun_c1StateStruct StateBuffer;
  SFmark_cep_sfun_c1StateStruct StateBuffer2;
  SimStruct *S;
  ChartInfoStruct chartInfo;
  unsigned int chartNumber;
  unsigned int instanceNumber;
} SFmark_cep_sfun_c1InstanceStruct;

#endif

