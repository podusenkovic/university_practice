/*
 *
 * Stateflow code generation for machine:
 *    mark_cep
 * 
 * Target Name                          : sfun
 * Model Version                        : 1.188
 * Stateflow Version                    : 5.0.0.13.00.1.000001
 * Date of code generation              : 28-Mar-2005 15:43:43
 *
 */

#include "mark_cep_sfun.h"
#include "mark_cep_sfun_c1.h"

/* Global machine event */
uint8_T _sfEvent_;
#include "mark_cep_sfun_debug_macros.h"
unsigned int _mark_cepMachineNumber_=UNREASONABLE_NUMBER;
unsigned int sf_mark_cep_process_check_sum_call( int nlhs, mxArray * plhs[], int
 nrhs, const mxArray * prhs[] )
{
#ifdef MATLAB_MEX_FILE
  char commandName[20];
  if (nrhs<1 || !mxIsChar(prhs[0]) ) return 0;
  /* Possible call to get the checksum */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if(strcmp(commandName,"sf_get_check_sum")) return 0;
  plhs[0] = mxCreateDoubleMatrix( 1,4,mxREAL);
  if(nrhs>1 && mxIsChar(prhs[1])) {
    mxGetString(prhs[1], commandName,sizeof(commandName)/sizeof(char));
    commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
    if(!strcmp(commandName,"machine")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(4178647107U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3006013819U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1336582821U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2941055714U);
    }else if(!strcmp(commandName,"exportedFcn")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0U);
    }else if(!strcmp(commandName,"makefile")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1413244230U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2179279172U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(4263428504U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(570596491U);
    }else if(nrhs==3 && !strcmp(commandName,"chart")) {
      unsigned int chartFileNumber;
      chartFileNumber = (unsigned int)mxGetScalar(prhs[2]);
      switch(chartFileNumber) {
       case 1:
        {
          extern void sf_mark_cep_sfun_c1_get_check_sum(mxArray *plhs[]);
          sf_mark_cep_sfun_c1_get_check_sum(plhs);
          break;
        }

       default:
        ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0.0);
      }
    }else if(!strcmp(commandName,"target")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1739055594U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(992227416U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3469248181U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(994826831U);
    }else {
      return 0;
    }
  } else{
    ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3384845960U);
    ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(724413533U);
    ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1639792729U);
    ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(952346488U);
  }
  return 1;
#else
  return 0;
#endif
}
/* Stateflow time variable */
real_T _sfTime_;

/* Machine initialize */
void mark_cep_initializer(void)
{
  _mark_cepMachineNumber_ =
    sf_debug_initialize_machine("mark_cep","sfun",0,1,0,0,0);
  sf_debug_set_machine_event_thresholds(_mark_cepMachineNumber_,0,0);
  sf_debug_set_machine_data_thresholds(_mark_cepMachineNumber_,0);
}

unsigned int mark_cep_registry(SimStruct *simstructPtr,char *chartName, int
 initializeFlag)
{
  if(!strcmp_ignore_ws(chartName,"mark_cep/Chart/ SFunction ")) {
    mark_cep_sfun_c1_registry(simstructPtr);
    return 1;
  }
  return 0;
}
unsigned int mark_cep_sizes_registry(SimStruct *simstructPtr,char *chartName)
{
  if(!strcmp_ignore_ws(chartName,"mark_cep/Chart/ SFunction ")) {
    mark_cep_sfun_c1_sizes_registry(simstructPtr);
    return 1;
  }
  return 0;
}
void mark_cep_terminator(void)
{
}

