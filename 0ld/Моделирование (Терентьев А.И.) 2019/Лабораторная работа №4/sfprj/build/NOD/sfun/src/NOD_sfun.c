/* Include files */
#include "NOD_sfun.h"
#include "c1_NOD.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */
uint8_T _sfEvent_;
uint32_T _NODMachineNumber_;
real_T _sfTime_;

/* Function Declarations */

/* Function Definitions */
void NOD_initializer(void)
{
  _sfEvent_ = CALL_EVENT;
}

void NOD_terminator(void)
{
}

/* SFunction Glue Code */
unsigned int sf_NOD_method_dispatcher(SimStruct *simstructPtr, const char
 *chartName, int_T method, void *data)
{
  if(!strcmp_ignore_ws(chartName,"NOD/Chart/ SFunction ")) {
    c1_NOD_method_dispatcher(simstructPtr, method, data);
    return 1;
  }
  return 0;
}
unsigned int sf_NOD_process_check_sum_call( int nlhs, mxArray * plhs[], int
 nrhs, const mxArray * prhs[] )
{
#ifdef MATLAB_MEX_FILE
  char commandName[20];
  if (nrhs<1 || !mxIsChar(prhs[0]) ) return 0;
  /* Possible call to get the checksum */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if(strcmp(commandName,"sf_get_check_sum")) return 0;
  plhs[0] = mxCreateDoubleMatrix( 1,4,mxREAL);
  if(nrhs>1 && mxIsChar(prhs[1])) {
    mxGetString(prhs[1], commandName,sizeof(commandName)/sizeof(char));
    commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
    if(!strcmp(commandName,"machine")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(4181657004U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1870034423U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1359621521U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1824992257U);
    }else if(!strcmp(commandName,"exportedFcn")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0U);
    }else if(!strcmp(commandName,"makefile")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1435328207U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3262765056U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(713361692U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(3660896325U);
    }else if(nrhs==3 && !strcmp(commandName,"chart")) {
      unsigned int chartFileNumber;
      chartFileNumber = (unsigned int)mxGetScalar(prhs[2]);
      switch(chartFileNumber) {
       case 1:
        {
          extern void sf_c1_NOD_get_check_sum(mxArray *plhs[]);
          sf_c1_NOD_get_check_sum(plhs);
          break;
        }

       default:
        ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0.0);
      }
    }else if(!strcmp(commandName,"target")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1518981232U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(4172013795U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2963200744U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(709200955U);
    }else {
      return 0;
    }
  } else{
    ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1436471845U);
    ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(436231802U);
    ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2890338559U);
    ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(3164455243U);
  }
  return 1;
#else
  return 0;
#endif
}

unsigned int sf_NOD_autoinheritance_info( int nlhs, mxArray * plhs[], int nrhs,
 const mxArray * prhs[] )
{
#ifdef MATLAB_MEX_FILE
  char commandName[32];
  if (nrhs<2 || !mxIsChar(prhs[0]) ) return 0;
  /* Possible call to get the autoinheritance_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if(strcmp(commandName,"get_autoinheritance_info")) return 0;
  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch(chartFileNumber) {
     case 1:
      {
        extern mxArray *sf_c1_NOD_get_autoinheritance_info(void);
        plhs[0] = sf_c1_NOD_get_autoinheritance_info();
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }
  return 1;
#else
  return 0;
#endif
}
void NOD_debug_initialize(void)
{
  _NODMachineNumber_ = sf_debug_initialize_machine("NOD","sfun",0,1,0,0,0);
  sf_debug_set_machine_event_thresholds(_NODMachineNumber_,0,0);
  sf_debug_set_machine_data_thresholds(_NODMachineNumber_,0);
}

void NOD_register_exported_symbols(SimStruct* S)
{
}
