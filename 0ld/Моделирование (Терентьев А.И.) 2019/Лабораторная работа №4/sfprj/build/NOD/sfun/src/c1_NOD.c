/* Include files */
#include "NOD_sfun.h"
#include "c1_NOD.h"
#define CHARTINSTANCE_CHARTNUMBER       (chartInstance.chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER    (chartInstance.instanceNumber)
#include "NOD_sfun_debug_macros.h"

/* Type Definitions */

/* Named Constants */
#define c1_IN_NO_ACTIVE_CHILD           (0)
#define c1_IN_answer                    (1)

/* Variable Declarations */

/* Variable Definitions */
static SFc1_NODInstanceStruct chartInstance;

/* Function Declarations */
static void initialize_c1_NOD(void);
static void initialize_params_c1_NOD(void);
static void enable_c1_NOD(void);
static void disable_c1_NOD(void);
static void finalize_c1_NOD(void);
static void sf_c1_NOD(void);
static real_T *c1_n1(void);
static real_T *c1_n2(void);
static real_T *c1_answer(void);
static void init_test_point_addr_map(void);
static void **get_test_point_address_map(void);
static rtwCAPI_ModelMappingInfo *get_test_point_mapping_info(void);
static void init_dsm_address_info(void);
static void sf_save_state_c1_NOD(FILE *c1_file);
static void sf_load_state_c1_NOD(FILE *c1_file);

/* Function Definitions */
static void initialize_c1_NOD(void)
{
  chartInstance.c1_tp_answer = 0U;
  chartInstance.c1_is_active_c1_NOD = 0U;
  chartInstance.c1_is_c1_NOD = 0U;
  chartInstance.c1_nn1 = 0.0;
  chartInstance.c1_nn2 = 0.0;
  if(!(cdrGetOutputPortReusable(chartInstance.S, 1) != 0)) {
    *c1_answer() = 0.0;
  }
}

static void initialize_params_c1_NOD(void)
{
}

static void enable_c1_NOD(void)
{
}

static void disable_c1_NOD(void)
{
}

static void finalize_c1_NOD(void)
{
}

static void sf_c1_NOD(void)
{
  uint8_T c1_previousEvent;
  real_T c1_b_nn1;
  real_T c1_b_nn2;
  real_T c1_c_nn2;
  real_T c1_c_nn1;
  real_T c1_b_answer;
  _sfTime_ = (real_T)ssGetT(chartInstance.S);
  _SFD_DATA_RANGE_CHECK(*c1_n1(), 4U);
  _SFD_DATA_RANGE_CHECK(*c1_n2(), 3U);
  _SFD_DATA_RANGE_CHECK(*c1_answer(), 0U);
  _SFD_DATA_RANGE_CHECK(chartInstance.c1_nn1, 1U);
  _SFD_DATA_RANGE_CHECK(chartInstance.c1_nn2, 2U);
  c1_previousEvent = _sfEvent_;
  _sfEvent_ = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG,0);
  if(chartInstance.c1_is_active_c1_NOD == 0) {
    _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG,0);
    chartInstance.c1_is_active_c1_NOD = 1U;
    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG,0);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,1);
    chartInstance.c1_nn1 = *c1_n1();
    _SFD_DATA_RANGE_CHECK(chartInstance.c1_nn1, 1U);
    c1_b_nn1 = chartInstance.c1_nn1;
    sf_mex_printf("%s =\\n", "nn1");
    sf_mex_call("disp", 0U, 1U, 6, c1_b_nn1);
    chartInstance.c1_nn2 = *c1_n2();
    _SFD_DATA_RANGE_CHECK(chartInstance.c1_nn2, 2U);
    c1_b_nn2 = chartInstance.c1_nn2;
    sf_mex_printf("%s =\\n", "nn2");
    sf_mex_call("disp", 0U, 1U, 6, c1_b_nn2);
    while(1) {
      while(CV_TRANSITION_EVAL(4U,
        (int32_T)_SFD_CCP_CALL(4,0,((chartInstance.c1_nn1 <
           chartInstance.c1_nn2)!=0))) != 0) {
        if(sf_debug_transition_conflict_check_enabled()) {
          unsigned int transitionList[2];
          unsigned int numTransitions=1;
          transitionList[0] = 4;
          sf_debug_transition_conflict_check_begin();
          if(chartInstance.c1_nn2 < chartInstance.c1_nn1) {
            transitionList[numTransitions] = 5;
            numTransitions++;
          }
          sf_debug_transition_conflict_check_end();
          if(numTransitions>1) {
            _SFD_TRANSITION_CONFLICT(&(transitionList[0]),numTransitions);
          }
        }
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,4);
        chartInstance.c1_nn2 = chartInstance.c1_nn2 - chartInstance.c1_nn1;
        _SFD_DATA_RANGE_CHECK(chartInstance.c1_nn2, 2U);
        c1_c_nn2 = chartInstance.c1_nn2;
        sf_mex_printf("%s =\\n", "nn2");
        sf_mex_call("disp", 0U, 1U, 6, c1_c_nn2);
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,2);
        sf_mex_listen_for_ctrl_c(chartInstance.S);
      }
      if(CV_TRANSITION_EVAL(5U,
        (int32_T)_SFD_CCP_CALL(5,0,((chartInstance.c1_nn2 <
           chartInstance.c1_nn1)!=0))) == 0) {
        break;
      }
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,5);
      chartInstance.c1_nn1 = chartInstance.c1_nn1 - chartInstance.c1_nn2;
      _SFD_DATA_RANGE_CHECK(chartInstance.c1_nn1, 1U);
      c1_c_nn1 = chartInstance.c1_nn1;
      sf_mex_printf("%s =\\n", "nn1");
      sf_mex_call("disp", 0U, 1U, 6, c1_c_nn1);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,0);
      sf_mex_listen_for_ctrl_c(chartInstance.S);
    }
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,3);
    *c1_answer() = chartInstance.c1_nn1;
    _SFD_DATA_RANGE_CHECK(*c1_answer(), 0U);
    c1_b_answer = *c1_answer();
    sf_mex_printf("%s =\\n", "answer");
    sf_mex_call("disp", 0U, 1U, 6, c1_b_answer);
    chartInstance.c1_is_c1_NOD = (uint8_T)c1_IN_answer;
    _SFD_CS_CALL(STATE_ACTIVE_TAG,0);
    chartInstance.c1_tp_answer = 1U;
  }
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG,0);
  _sfEvent_ = c1_previousEvent;
  sf_debug_check_for_state_inconsistency();
}

static real_T *c1_n1(void)
{
  return (real_T *)ssGetInputPortSignal(chartInstance.S, 0);
}

static real_T *c1_n2(void)
{
  return (real_T *)ssGetInputPortSignal(chartInstance.S, 1);
}

static real_T *c1_answer(void)
{
  return (real_T *)ssGetOutputPortSignal(chartInstance.S, 1);
}

static void init_test_point_addr_map(void)
{
  chartInstance.c1_testPointAddrMap[0] = &chartInstance.c1_nn1;
  chartInstance.c1_testPointAddrMap[1] = &chartInstance.c1_nn2;
  chartInstance.c1_testPointAddrMap[2] = &chartInstance.c1_tp_answer;
}

static void **get_test_point_address_map(void)
{
  return &chartInstance.c1_testPointAddrMap[0];
}

static rtwCAPI_ModelMappingInfo *get_test_point_mapping_info(void)
{
  return &chartInstance.c1_testPointMappingInfo;
}

static void init_dsm_address_info(void)
{
}

static void sf_save_state_c1_NOD(FILE *c1_file)
{
  fwrite(&chartInstance.c1_tp_answer, 1, sizeof(chartInstance.c1_tp_answer),
   c1_file);
  fwrite(&chartInstance.c1_is_active_c1_NOD, 1,
   sizeof(chartInstance.c1_is_active_c1_NOD), c1_file);
  fwrite(&chartInstance.c1_is_c1_NOD, 1, sizeof(chartInstance.c1_is_c1_NOD),
   c1_file);
  fwrite(&chartInstance.c1_nn1, 1, sizeof(chartInstance.c1_nn1), c1_file);
  fwrite(&chartInstance.c1_nn2, 1, sizeof(chartInstance.c1_nn2), c1_file);
}

static void sf_load_state_c1_NOD(FILE *c1_file)
{
  fread(&chartInstance.c1_tp_answer, 1, sizeof(chartInstance.c1_tp_answer),
   c1_file);
  fread(&chartInstance.c1_is_active_c1_NOD, 1,
   sizeof(chartInstance.c1_is_active_c1_NOD), c1_file);
  fread(&chartInstance.c1_is_c1_NOD, 1, sizeof(chartInstance.c1_is_c1_NOD),
   c1_file);
  fread(&chartInstance.c1_nn1, 1, sizeof(chartInstance.c1_nn1), c1_file);
  fread(&chartInstance.c1_nn2, 1, sizeof(chartInstance.c1_nn2), c1_file);
}

/* SFunction Glue Code */
static void init_test_point_mapping_info(SimStruct *S);
void sf_c1_NOD_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3222018968U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(243960435U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(4213867305U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1795254301U);
}

mxArray *sf_c1_NOD_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] =
  {"checksum","inputs","parameters","outputs"};
  mxArray *mxAutoinheritanceInfo =
  mxCreateStructMatrix(1,1,4,autoinheritanceFields);
  {
    mxArray *mxChecksum = mxCreateDoubleMatrix(4,1,mxREAL);
    double *pr = mxGetPr(mxChecksum);
    pr[0] = (double)(2748902964U);
    pr[1] = (double)(1505651078U);
    pr[2] = (double)(1917755861U);
    pr[3] = (double)(3153210689U);
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }
  {
    const char *dataFields[] = {"size","type","complexity"};
    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);
    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }
    {
      const char *typeFields[] = {"base","aliasId","fixpt"};
      mxArray *mxType = mxCreateStructMatrix(1,1,3,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"aliasId",mxCreateDoubleScalar(0));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }
    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }
    {
      const char *typeFields[] = {"base","aliasId","fixpt"};
      mxArray *mxType = mxCreateStructMatrix(1,1,3,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"aliasId",mxCreateDoubleScalar(0));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }
    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }
  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,mxREAL));
  }
  {
    const char *dataFields[] = {"size","type","complexity"};
    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);
    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }
    {
      const char *typeFields[] = {"base","aliasId","fixpt"};
      mxArray *mxType = mxCreateStructMatrix(1,1,3,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"aliasId",mxCreateDoubleScalar(0));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }
    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }
  return(mxAutoinheritanceInfo);
}

static void chart_debug_initialization(SimStruct *S)
{
  if(ssIsFirstInitCond(S)) {
    /* do this only if simulation is starting */
    if(!sim_mode_is_rtw_gen(S)) {
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart(_NODMachineNumber_,
         1,
         1,
         6,
         5,
         0,
         0,
         0,
         0,
         &(chartInstance.chartNumber),
         &(chartInstance.instanceNumber),
         ssGetPath(S),
         (void *)S);
        if(chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting(_NODMachineNumber_,chartInstance.chartNumber,0);
          sf_debug_set_chart_event_thresholds(_NODMachineNumber_,
           chartInstance.chartNumber,
           0,
           0,
           0);

          _SFD_SET_DATA_PROPS(4,1,1,0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,"n1",0);
          _SFD_SET_DATA_PROPS(3,1,1,0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,"n2",0);
          _SFD_SET_DATA_PROPS(0,2,0,1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,"answer",0);
          _SFD_SET_DATA_PROPS(1,0,0,0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,"nn1",0);
          _SFD_SET_DATA_PROPS(2,0,0,0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,"nn2",0);
          _SFD_STATE_INFO(0,0,0);
          _SFD_CH_SUBSTATE_COUNT(1);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_ST_SUBSTATE_COUNT(0,0);
        }
        _SFD_CV_INIT_CHART(1,0,0,0);
        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(1,0,NULL,NULL,0,NULL);

        {
          static unsigned int sStartGuardMap[] = {1};
          static unsigned int sEndGuardMap[] = {8};
          static int sPostFixPredicateTree[] = {0};
          _SFD_CV_INIT_TRANS(4,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,&(sPostFixPredicateTree[0]));
        }
        _SFD_CV_INIT_TRANS(2,0,NULL,NULL,0,NULL);

        _SFD_CV_INIT_TRANS(3,0,NULL,NULL,0,NULL);

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        {
          static unsigned int sStartGuardMap[] = {1};
          static unsigned int sEndGuardMap[] = {8};
          static int sPostFixPredicateTree[] = {0};
          _SFD_CV_INIT_TRANS(5,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,&(sPostFixPredicateTree[0]));
        }
        _SFD_TRANS_COV_WTS(1,0,0,2,0);
        if(chartAlreadyPresent==0)
        {
          _SFD_TRANS_COV_MAPS(1,
           0,NULL,NULL,
           0,NULL,NULL,
           2,NULL,NULL,
           0,NULL,NULL);
        }
        _SFD_TRANS_COV_WTS(4,0,1,1,0);
        if(chartAlreadyPresent==0)
        {
          static unsigned int sStartGuardMap[] = {1};
          static unsigned int sEndGuardMap[] = {8};
          _SFD_TRANS_COV_MAPS(4,
           0,NULL,NULL,
           1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
           1,NULL,NULL,
           0,NULL,NULL);
        }
        _SFD_TRANS_COV_WTS(2,0,0,0,0);
        if(chartAlreadyPresent==0)
        {
          _SFD_TRANS_COV_MAPS(2,
           0,NULL,NULL,
           0,NULL,NULL,
           0,NULL,NULL,
           0,NULL,NULL);
        }
        _SFD_TRANS_COV_WTS(3,0,0,1,0);
        if(chartAlreadyPresent==0)
        {
          _SFD_TRANS_COV_MAPS(3,
           0,NULL,NULL,
           0,NULL,NULL,
           1,NULL,NULL,
           0,NULL,NULL);
        }
        _SFD_TRANS_COV_WTS(0,0,0,0,0);
        if(chartAlreadyPresent==0)
        {
          _SFD_TRANS_COV_MAPS(0,
           0,NULL,NULL,
           0,NULL,NULL,
           0,NULL,NULL,
           0,NULL,NULL);
        }
        _SFD_TRANS_COV_WTS(5,0,1,1,0);
        if(chartAlreadyPresent==0)
        {
          static unsigned int sStartGuardMap[] = {1};
          static unsigned int sEndGuardMap[] = {8};
          _SFD_TRANS_COV_MAPS(5,
           0,NULL,NULL,
           1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
           1,NULL,NULL,
           0,NULL,NULL);
        }
        _SFD_SET_DATA_VALUE_PTR(4U, c1_n1());
        _SFD_SET_DATA_VALUE_PTR(3U, c1_n2());
        _SFD_SET_DATA_VALUE_PTR(0U, c1_answer());
        _SFD_SET_DATA_VALUE_PTR(1U, &chartInstance.c1_nn1);
        _SFD_SET_DATA_VALUE_PTR(2U, &chartInstance.c1_nn2);
      }
    }
  } else {
    sf_debug_reset_current_state_configuration(_NODMachineNumber_,chartInstance.chartNumber,chartInstance.instanceNumber);
  }
}

static void sf_opaque_initialize_c1_NOD(void *chartInstanceVar)
{
  chart_debug_initialization(chartInstance.S);
  initialize_params_c1_NOD();
  initialize_c1_NOD();
}

static void sf_opaque_enable_c1_NOD(void *chartInstanceVar)
{
  enable_c1_NOD();
}

static void sf_opaque_disable_c1_NOD(void *chartInstanceVar)
{
  disable_c1_NOD();
}

static void sf_opaque_gateway_c1_NOD(void *chartInstanceVar)
{
  sf_c1_NOD();
}

static void sf_opaque_terminate_c1_NOD(void *chartInstanceVar)
{
  finalize_c1_NOD();
}

static void mdlProcessParameters_c1_NOD(SimStruct *S)
{
  int i;
  for(i=0;i<ssGetNumRunTimeParams(S);i++) {
    if(ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }
  initialize_params_c1_NOD();
}

static void mdlSetWorkWidths_c1_NOD(SimStruct *S)
{
  if(sim_mode_is_rtw_gen(S)) {
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable("NOD",1);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop("NOD",1,"gatewayCannotBeInlinedMultipleTimes"));
    if(chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,"NOD",1,2);
      sf_mark_chart_reusable_outputs(S,"NOD",1,1);
    }
    if (!sf_is_chart_instance_optimized_out("NOD",1)) {
      int dtId;
      char *chartInstanceTypedefName =
        sf_chart_instance_typedef_name("NOD",1);
      dtId = ssRegisterDataType(S, chartInstanceTypedefName);
      if (dtId == INVALID_DTYPE_ID ) return;
      /* Register the size of the udt */
      if (!ssSetDataTypeSize(S, dtId, 8)) return;
      if(!ssSetNumDWork(S,1)) return;
      ssSetDWorkDataType(S, 0, dtId);
      ssSetDWorkWidth(S, 0, 1);
      ssSetDWorkName(S, 0, "ChartInstance"); /*optional name, less than 16 chars*/
      sf_set_rtw_identifier(S);
    }
    ssSetHasSubFunctions(S,!(chartIsInlinable));
    ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  }

  ssSetChecksum0(S,(3222018968U));
  ssSetChecksum1(S,(243960435U));
  ssSetChecksum2(S,(4213867305U));
  ssSetChecksum3(S,(1795254301U));

  ssSetExplicitFCSSCtrl(S,1);
}

static void mdlRTW_c1_NOD(SimStruct *S)
{
  if(sim_mode_is_rtw_gen(S)) {
    sf_write_symbol_mapping(S, "NOD", 1);
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void sf_save_debug_c1_NOD(SFc1_NODInstanceStruct* chartInstance, FILE*
 file)
{
  int machineNumber = _NODMachineNumber_;
  int chartNumber = chartInstance->chartNumber;
  int instanceNumber = chartInstance->instanceNumber;
  int bufferSize = sf_debug_get_state_vectors_data_size(machineNumber,
   chartNumber, instanceNumber);
  unsigned char* buffer = (unsigned char*) malloc(bufferSize);
  unsigned int chartActive = sf_debug_get_chart_active(machineNumber,
   chartNumber, instanceNumber);
  fwrite(&chartActive, sizeof(chartActive),1,file);
  sf_debug_get_state_vectors_data(machineNumber, chartNumber, instanceNumber,
   buffer, bufferSize);
  fwrite(&bufferSize, sizeof(bufferSize), 1, file); /* Write number of bytes */
  fwrite(buffer, 1, bufferSize,file);   /* Write buffer */
  free(buffer);
}
static void sf_load_debug_c1_NOD(SFc1_NODInstanceStruct* chartInstance,FILE*
 file)
{
  int machineNumber = _NODMachineNumber_;
  int chartNumber = chartInstance->chartNumber;
  int instanceNumber = chartInstance->instanceNumber;
  int bufferSize = 0;
  unsigned char* buffer = NULL;
  unsigned int chartActive = 0;
  fread(&chartActive, sizeof(chartActive),1,file);
  sf_debug_set_chart_active(machineNumber, chartNumber, instanceNumber,
   chartActive);
  fread(&bufferSize, sizeof(bufferSize), 1, file);
  buffer = (unsigned char*) malloc(bufferSize);
  fread(buffer, 1, bufferSize, file);
  sf_debug_set_state_vectors_data(machineNumber, chartNumber, instanceNumber,
   buffer, bufferSize);
  free(buffer);
}
static void sf_c1_NOD_sim_ctx_io(SimStruct* S, const char io, FILE* file)
{
  if(io == 'r') {
    sf_load_state_c1_NOD(file);
    sf_load_debug_c1_NOD(&chartInstance, file);
  } else {
    sf_save_state_c1_NOD(file);
    sf_save_debug_c1_NOD(&chartInstance, file);
  }
}
static void mdlStart_c1_NOD(SimStruct *S)
{
  chartInstance.chartInfo.chartInstance = NULL;
  chartInstance.chartInfo.isEMLChart = 0;
  chartInstance.chartInfo.chartInitialized = 0;
  chartInstance.chartInfo.sFunctionGateway = sf_opaque_gateway_c1_NOD;
  chartInstance.chartInfo.initializeChart = sf_opaque_initialize_c1_NOD;
  chartInstance.chartInfo.terminateChart = sf_opaque_terminate_c1_NOD;
  chartInstance.chartInfo.enableChart = sf_opaque_enable_c1_NOD;
  chartInstance.chartInfo.disableChart = sf_opaque_disable_c1_NOD;
  chartInstance.chartInfo.mdlRTW = mdlRTW_c1_NOD;
  chartInstance.chartInfo.mdlStart = mdlStart_c1_NOD;
  chartInstance.chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c1_NOD;
  chartInstance.chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance.chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance.chartInfo.storeCurrentConfiguration = NULL;
  chartInstance.S = S;
  ssSetUserData(S,(void *)(&(chartInstance.chartInfo))); /* register the chart instance with simstruct */
  ssSetmdlSimulationContextIO(S, sf_c1_NOD_sim_ctx_io);

  if(!sim_mode_is_rtw_gen(S)) {
    init_test_point_mapping_info(S);
  }
  if(!sim_mode_is_rtw_gen(S)) {
    init_dsm_address_info();
  }
}

void c1_NOD_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c1_NOD(S);
    break;
   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c1_NOD(S);
    break;
   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c1_NOD(S);
    break;
   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
     "Error calling c1_NOD_method_dispatcher.\n"
     "Can't handle method %d.\n", method);
    break;
  }
}

static const rtwCAPI_DataTypeMap dataTypeMap[] = {
  /* cName, mwName, numElements, elemMapIndex, dataSize, slDataId, isComplex, isPointer */
  {"real_T", "real_T", 0, 0, sizeof(real_T), SS_DOUBLE, 0, 0},
  {"uint8_T", "uint8_T", 0, 0, sizeof(uint8_T), SS_UINT8, 0, 0}
};

static const rtwCAPI_FixPtMap fixedPointMap[] = {
  /* *fracSlope, *bias, scaleType, wordLength, exponent, isSigned */
  {NULL, NULL, rtwCAPI_FIX_RESERVED, 64, 0, 0}
};

static const rtwCAPI_DimensionMap dimensionMap[] = {
  /* dataOrientation, dimArrayIndex, numDims*/
  {rtwCAPI_SCALAR, 0, 2}
};

static const uint_T dimensionArray[] = {
  1, 1
};

static real_T sfCAPIsampleTimeZero = 0.0;
static const rtwCAPI_SampleTimeMap sampleTimeMap[] = {
  /* *period, *offset, taskId, mode */
  {&sfCAPIsampleTimeZero, &sfCAPIsampleTimeZero, 0, 0}
};

static const rtwCAPI_Signals testPointSignals[] = {
  /* addrMapIndex, sysNum, SFRelativePath, dataName, portNumber, dataTypeIndex, dimIndex, fixPtIdx, sTimeIndex */
  {0, 0,"StateflowChart/nn1", "nn1", 0, 0, 0, 0, 0},
  {1, 0,"StateflowChart/nn2", "nn2", 0, 0, 0, 0, 0},
  {2, 0, "StateflowChart/answer", "answer", 0, 1, 0, 0, 0}
};

static rtwCAPI_ModelMappingStaticInfo testPointMappingStaticInfo = {
  /* block signal monitoring */
  {
    testPointSignals,                   /* Block signals Array  */
    3                                   /* Num Block IO signals */
  },

  /* parameter tuning */
  {
    NULL,                               /* Block parameters Array    */
    0,                                  /* Num block parameters      */
    NULL,                               /* Variable parameters Array */
    0                                   /* Num variable parameters   */
  },

  /* block states */
  {
    NULL,                               /* Block States array        */
    0                                   /* Num Block States          */
  },

  /* Static maps */
  {
    dataTypeMap,                        /* Data Type Map            */
    dimensionMap,                       /* Data Dimension Map       */
    fixedPointMap,                      /* Fixed Point Map          */
    NULL,                               /* Structure Element map    */
    sampleTimeMap,                      /* Sample Times Map         */
    dimensionArray                      /* Dimension Array          */
  },

  /* Target type */
  "float"
};

static void init_test_point_mapping_info(SimStruct *S) {
  rtwCAPI_ModelMappingInfo *testPointMappingInfo;
  void **testPointAddrMap;

  init_test_point_addr_map();
  testPointMappingInfo = get_test_point_mapping_info();
  testPointAddrMap = get_test_point_address_map();

  rtwCAPI_SetStaticMap(*testPointMappingInfo, &testPointMappingStaticInfo);
  rtwCAPI_SetLoggingStaticMap(*testPointMappingInfo, NULL);
  rtwCAPI_SetInstanceLoggingInfo(*testPointMappingInfo, NULL);
  rtwCAPI_SetPath(*testPointMappingInfo, "");
  rtwCAPI_SetFullPath(*testPointMappingInfo, NULL);
  rtwCAPI_SetDataAddressMap(*testPointMappingInfo, testPointAddrMap);
  rtwCAPI_SetChildMMIArray(*testPointMappingInfo, NULL);
  rtwCAPI_SetChildMMIArrayLen(*testPointMappingInfo, 0);

  ssSetModelMappingInfoPtr(S, testPointMappingInfo);
}

