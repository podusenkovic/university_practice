#ifndef __c1_NOD_h__
#define __c1_NOD_h__

/* Include files */
#include "sfc_sf.h"
#include "sfc_mex.h"
#include "rtw_capi.h"
#include "rtw_modelmap.h"

/* Type Definitions */
typedef struct {
  real_T c1_nn1;
  real_T c1_nn2;
  SimStruct *S;
  void *c1_testPointAddrMap[3];
  uint32_T chartNumber;
  uint32_T instanceNumber;
  uint8_T c1_is_active_c1_NOD;
  uint8_T c1_is_c1_NOD;
  uint8_T c1_tp_answer;
  rtwCAPI_ModelMappingInfo c1_testPointMappingInfo;
  ChartInfoStruct chartInfo;
} SFc1_NODInstanceStruct;

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */

extern void sf_c1_NOD_get_check_sum(mxArray *plhs[]);
extern void c1_NOD_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif

