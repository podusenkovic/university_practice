/*
 *
 * Stateflow code generation for chart:
 *    test/Chart
 * 
 * Target Name                          : sfun
 * Model Version                        : 1.188
 * Stateflow Version                    : 5.0.0.13.00.1.000001
 * Date of code generation              : 28-Mar-2005 15:49:51
 *
 */

#include "test_sfun.h"
#include "test_sfun_c1.h"
#define mexPrintf                       sf_mex_printf
#ifdef printf
#undef printf
#endif
#define printf                          sf_mex_printf
#define CHARTINSTANCE_CHARTNUMBER       (chartInstance.chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER    (chartInstance.instanceNumber)
#include "test_sfun_debug_macros.h"
#define IN_NO_ACTIVE_CHILD              (0)
#define IN_m0_c1_s1_answer              1
static SFtest_sfun_c1InstanceStruct chartInstance;
#define InputData_m0_c1_d3_n1           (((real_T *)(ssGetInputPortSignal(chartInstance.S,0)))[0])
#define InputData_m0_c1_d4_n2           (((real_T *)(ssGetInputPortSignal(chartInstance.S,1)))[0])
#define OutputData_m0_c1_d5_answer      (((real_T *)(ssGetOutputPortSignal(chartInstance.S,1)))[0])

void test_sfun_c1(void)
{
  real_T __sfTemp1;
  real_T __sfTemp2;
  real_T __sfTemp3;
  real_T __sfTemp4;
  real_T __sfTemp5;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG,0);
  if(chartInstance.State.is_active_test_sfun_c1 == 0) {
    _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG,0);
    chartInstance.State.is_active_test_sfun_c1 = 1;
    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG,0);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,3);
    _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,3);
    _SFD_CT_CALL(TRANSITION_BEFORE_COND_ACTION_TAG,3);
    sf_mex_printf("%s =\n", "nn1");
    chartInstance.LocalData.m0_c1_d1_nn1 = InputData_m0_c1_d3_n1;
    __sfTemp1 = chartInstance.LocalData.m0_c1_d1_nn1;
    ml_call_function("disp", 0, 1, 4, sf_mex_create_mx_array(&__sfTemp1, 0, 0,
      0));
    _SFD_CCT_CALL(TRANSITION_CONDITION_ACTION_COVERAGE_TAG,3,0);
    sf_mex_printf("%s =\n", "nn2");
    chartInstance.LocalData.m0_c1_d2_nn2 = InputData_m0_c1_d4_n2;
    __sfTemp2 = chartInstance.LocalData.m0_c1_d2_nn2;
    ml_call_function("disp", 0, 1, 4, sf_mex_create_mx_array(&__sfTemp2, 0, 0,
      0));
    _SFD_CCT_CALL(TRANSITION_CONDITION_ACTION_COVERAGE_TAG,3,1);
    _SFD_CT_CALL(TRANSITION_AFTER_COND_ACTION_TAG,3);
    _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,3);
    while(1) {
      while(1) {
        sf_mex_listen_for_ctrl_c(chartInstance.S);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG,1);
        if(!CV_TRANSITION_EVAL(1,
          _SFD_CCP_CALL(1,0,(chartInstance.LocalData.m0_c1_d1_nn1 <
            chartInstance.LocalData.m0_c1_d2_nn2))
          )) {
          break;
        }
        if(sf_debug_transition_conflict_check_enabled()) {
          unsigned int transitionList[2];
          unsigned int numTransitions=1;
          transitionList[0] = 1;
          sf_debug_transition_conflict_check_begin();
          if(chartInstance.LocalData.m0_c1_d2_nn2 <
           chartInstance.LocalData.m0_c1_d1_nn1) {
            transitionList[numTransitions] = 2;
            numTransitions++;
          }
          sf_debug_transition_conflict_check_end();
          if(numTransitions>1) {
            _SFD_TRANSITION_CONFLICT(&(transitionList[0]),numTransitions);
          }
        }
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,1);
        _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,1);
        _SFD_CT_CALL(TRANSITION_BEFORE_COND_ACTION_TAG,1);
        sf_mex_printf("%s =\n", "nn2");
        chartInstance.LocalData.m0_c1_d2_nn2 -=
          chartInstance.LocalData.m0_c1_d1_nn1;
        __sfTemp3 = chartInstance.LocalData.m0_c1_d2_nn2;
        ml_call_function("disp", 0, 1, 4, sf_mex_create_mx_array(&__sfTemp3, 0,
          0, 0));
        _SFD_CCT_CALL(TRANSITION_CONDITION_ACTION_COVERAGE_TAG,1,0);
        _SFD_CT_CALL(TRANSITION_AFTER_COND_ACTION_TAG,1);
        _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,1);
        sf_mex_listen_for_ctrl_c(chartInstance.S);
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,0);
        _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,0);
        _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,0);
      }
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG,2);
      if(!CV_TRANSITION_EVAL(2,
        _SFD_CCP_CALL(2,0,(chartInstance.LocalData.m0_c1_d2_nn2 <
          chartInstance.LocalData.m0_c1_d1_nn1))
        )) {
        break;
      }
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,2);
      _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,2);
      _SFD_CT_CALL(TRANSITION_BEFORE_COND_ACTION_TAG,2);
      sf_mex_printf("%s =\n", "nn1");
      chartInstance.LocalData.m0_c1_d1_nn1 -=
        chartInstance.LocalData.m0_c1_d2_nn2;
      __sfTemp4 = chartInstance.LocalData.m0_c1_d1_nn1;
      ml_call_function("disp", 0, 1, 4, sf_mex_create_mx_array(&__sfTemp4, 0, 0,
        0));
      _SFD_CCT_CALL(TRANSITION_CONDITION_ACTION_COVERAGE_TAG,2,0);
      _SFD_CT_CALL(TRANSITION_AFTER_COND_ACTION_TAG,2);
      _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,2);
      sf_mex_listen_for_ctrl_c(chartInstance.S);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,5);
      _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,5);
      _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,5);
    }
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG,4);
    _SFD_CT_CALL(TRANSITION_WHEN_VALID_TAG,4);
    _SFD_CT_CALL(TRANSITION_BEFORE_COND_ACTION_TAG,4);
    sf_mex_printf("%s =\n", "answer");
    OutputData_m0_c1_d5_answer = chartInstance.LocalData.m0_c1_d1_nn1;
    __sfTemp5 = OutputData_m0_c1_d5_answer;
    ml_call_function("disp", 0, 1, 4, sf_mex_create_mx_array(&__sfTemp5, 0, 0,
      0));
    _SFD_CCT_CALL(TRANSITION_CONDITION_ACTION_COVERAGE_TAG,4,0);
    _SFD_CT_CALL(TRANSITION_AFTER_COND_ACTION_TAG,4);
    _SFD_CT_CALL(TRANSITION_INACTIVE_TAG,4);
    _SFD_CS_CALL(STATE_ENTER_ENTRY_FUNCTION_TAG,0);
    _SFD_CCS_CALL(STATE_ENTRY_COVERAGE_TAG,0,0);
    chartInstance.State.is_test_sfun_c1 = IN_m0_c1_s1_answer;
    _SFD_CS_CALL(STATE_ACTIVE_TAG,0);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG,0);
  }
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG,0);
}

void sf_test_sfun_c1_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(2216518746U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3391478485U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3803223498U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(935393248U);
}
/*
 * Chart initialization function
 */
/* work around the buggy macro in simstruc.h until it is fixed */
#define cdrGetOutputPortReusable(S,port) \
  ( (S)->portInfo.outputs[(port)].attributes.optimOpts != \
   SS_NOT_REUSABLE_AND_GLOBAL )

static void initialize_test_sfun_c1( SimStruct *S)
{

  {
    chartInstance.LocalData.m0_c1_d1_nn1 = 0.0;
    chartInstance.LocalData.m0_c1_d2_nn2 = 0.0;
    if(!cdrGetOutputPortReusable(S,1)) {
      OutputData_m0_c1_d5_answer = 0.0;
    }
  }

  /* Initialize chart's state configuration */
  memset((void*)&(chartInstance.State),0,sizeof(chartInstance.State));

  {
    if(ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting */
      if(!sim_mode_is_rtw_gen(S)) {
        {
          unsigned int chartAlreadyPresent;
          chartAlreadyPresent = sf_debug_initialize_chart(_testMachineNumber_,
            1,
            1,
            6,
            5,
            0,
            0,
            0,
            0,
            &(chartInstance.chartNumber),
            &(chartInstance.instanceNumber),
            ssGetPath((SimStruct *)S),
            (void *)S);
          if(chartAlreadyPresent==0) {
            /* this is the first instance */
            sf_debug_set_chart_disable_implicit_casting(_testMachineNumber_,chartInstance.chartNumber,0);
            sf_debug_set_chart_event_thresholds(_testMachineNumber_,
             chartInstance.chartNumber,
             0,
             0,
             0);

            _SFD_SET_DATA_PROPS(3,
             1,
             1,
             0,
             SF_DOUBLE,
             0,
             NULL,
             0,
             0.0,
             1.0,
             0);
            _SFD_SET_DATA_PROPS(2,
             1,
             1,
             0,
             SF_DOUBLE,
             0,
             NULL,
             0,
             0.0,
             1.0,
             0);
            _SFD_SET_DATA_PROPS(1,
             2,
             0,
             1,
             SF_DOUBLE,
             0,
             NULL,
             0,
             0.0,
             1.0,
             0);
            _SFD_SET_DATA_PROPS(0,
             0,
             0,
             0,
             SF_DOUBLE,
             0,
             NULL,
             0,
             0.0,
             1.0,
             0);
            _SFD_SET_DATA_PROPS(4,
             0,
             0,
             0,
             SF_DOUBLE,
             0,
             NULL,
             0,
             0.0,
             1.0,
             0);
            _SFD_STATE_INFO(0,0,0);
            _SFD_CH_SUBSTATE_COUNT(1);
            _SFD_CH_SUBSTATE_DECOMP(0);
            _SFD_CH_SUBSTATE_INDEX(0,0);
            _SFD_ST_SUBSTATE_COUNT(0,0);
          }
          _SFD_CV_INIT_CHART(1,0,0,0);
          {
            _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
          }

          _SFD_CV_INIT_TRANS(3,0,NULL,NULL,0,NULL);

          {
            static unsigned int sStartGuardMap[] = {1};
            static unsigned int sEndGuardMap[] = {8};
            static int sPostFixPredicateTree[] = {0};
            _SFD_CV_INIT_TRANS(1,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,&(sPostFixPredicateTree[0]));
          }
          _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

          _SFD_CV_INIT_TRANS(4,0,NULL,NULL,0,NULL);

          _SFD_CV_INIT_TRANS(5,0,NULL,NULL,0,NULL);

          {
            static unsigned int sStartGuardMap[] = {1};
            static unsigned int sEndGuardMap[] = {8};
            static int sPostFixPredicateTree[] = {0};
            _SFD_CV_INIT_TRANS(2,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,&(sPostFixPredicateTree[0]));
          }
          _SFD_STATE_COV_WTS(0,1,1,1);
          if(chartAlreadyPresent==0)
          {
            static unsigned int sStartEntryMap[] = {0};
            static unsigned int sEndEntryMap[] = {0};
            static unsigned int sStartDuringMap[] = {0};
            static unsigned int sEndDuringMap[] = {0};
            static unsigned int sStartExitMap[] = {0};
            static unsigned int sEndExitMap[] = {0};

            _SFD_STATE_COV_MAPS(0,
             1,&(sStartEntryMap[0]),&(sEndEntryMap[0]),
             1,&(sStartDuringMap[0]),&(sEndDuringMap[0]),
             1,&(sStartExitMap[0]),&(sEndExitMap[0]));
          }
          _SFD_TRANS_COV_WTS(3,0,0,2,0);
          if(chartAlreadyPresent==0)
          {
            static unsigned int sStartConditionActionMap[] = {1,8};
            static unsigned int sEndConditionActionMap[] = {8,15};
            _SFD_TRANS_COV_MAPS(3,
             0,NULL,NULL,
             0,NULL,NULL,
             2,&(sStartConditionActionMap[0]),&(sEndConditionActionMap[0]),
             0,NULL,NULL);
          }
          _SFD_TRANS_COV_WTS(1,0,1,1,0);
          if(chartAlreadyPresent==0)
          {
            static unsigned int sStartGuardMap[] = {1};
            static unsigned int sEndGuardMap[] = {8};
            static unsigned int sStartConditionActionMap[] = {10};
            static unsigned int sEndConditionActionMap[] = {22};
            _SFD_TRANS_COV_MAPS(1,
             0,NULL,NULL,
             1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
             1,&(sStartConditionActionMap[0]),&(sEndConditionActionMap[0]),
             0,NULL,NULL);
          }
          _SFD_TRANS_COV_WTS(0,0,0,0,0);
          if(chartAlreadyPresent==0)
          {
            _SFD_TRANS_COV_MAPS(0,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL);
          }
          _SFD_TRANS_COV_WTS(4,0,0,1,0);
          if(chartAlreadyPresent==0)
          {
            static unsigned int sStartConditionActionMap[] = {1};
            static unsigned int sEndConditionActionMap[] = {12};
            _SFD_TRANS_COV_MAPS(4,
             0,NULL,NULL,
             0,NULL,NULL,
             1,&(sStartConditionActionMap[0]),&(sEndConditionActionMap[0]),
             0,NULL,NULL);
          }
          _SFD_TRANS_COV_WTS(5,0,0,0,0);
          if(chartAlreadyPresent==0)
          {
            _SFD_TRANS_COV_MAPS(5,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL,
             0,NULL,NULL);
          }
          _SFD_TRANS_COV_WTS(2,0,1,1,0);
          if(chartAlreadyPresent==0)
          {
            static unsigned int sStartGuardMap[] = {1};
            static unsigned int sEndGuardMap[] = {8};
            static unsigned int sStartConditionActionMap[] = {10};
            static unsigned int sEndConditionActionMap[] = {22};
            _SFD_TRANS_COV_MAPS(2,
             0,NULL,NULL,
             1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),
             1,&(sStartConditionActionMap[0]),&(sEndConditionActionMap[0]),
             0,NULL,NULL);
          }
          _SFD_SET_DATA_VALUE_PTR(3,(void *)(&InputData_m0_c1_d3_n1));
          _SFD_SET_DATA_VALUE_PTR(2,(void *)(&InputData_m0_c1_d4_n2));
          _SFD_SET_DATA_VALUE_PTR(1,(void *)(&OutputData_m0_c1_d5_answer));
          _SFD_SET_DATA_VALUE_PTR(0,(void
            *)(&chartInstance.LocalData.m0_c1_d1_nn1));
          _SFD_SET_DATA_VALUE_PTR(4,(void
            *)(&chartInstance.LocalData.m0_c1_d2_nn2));
        }
      }
    }else{
      sf_debug_reset_current_state_configuration(_testMachineNumber_,chartInstance.chartNumber,chartInstance.instanceNumber);
    }
  }
  chartInstance.chartInfo.chartInitialized = 1;
}

void test_sfun_c1_sizes_registry(SimStruct *S)
{
  ssSetNumInputPorts((SimStruct *)S, 2);
  ssSetInputPortDataType((SimStruct *)S,0,SS_DOUBLE); /* InputData_m0_c1_d3_n1 */
  ssSetInputPortRequiredContiguous(S,0,1);
  ssSetInputPortWidth((SimStruct *)S,0,1);
  ssSetInputPortDirectFeedThrough((SimStruct *)S,0,1);
  ssSetInputPortDataType((SimStruct *)S,1,SS_DOUBLE); /* InputData_m0_c1_d4_n2 */
  ssSetInputPortRequiredContiguous(S,1,1);
  ssSetInputPortWidth((SimStruct *)S,1,1);
  ssSetInputPortDirectFeedThrough((SimStruct *)S,1,1);
  ssSetNumOutputPorts((SimStruct *)S, 2);
  ssSetOutputPortDataType((SimStruct *)S,0,SS_DOUBLE);
  ssSetOutputPortWidth((SimStruct *)S,0,1);
  ssSetOutputPortDataType((SimStruct *)S,1,SS_DOUBLE); /* OutputData_m0_c1_d5_answer */
  ssSetOutputPortWidth((SimStruct *)S,1,1);
  if(sim_mode_is_rtw_gen(S)) {
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable("test",1);
    ssSetStateflowIsInlinable((SimStruct *)S,chartIsInlinable);
    if(chartIsInlinable) {
      ssSetInputPortReusable((SimStruct *)S,0,1);
      ssSetInputPortReusable((SimStruct *)S,1,1);
      sf_mark_chart_expressionable_inputs((SimStruct *)S,"test",1,2);
      sf_mark_chart_reusable_outputs((SimStruct *)S,"test",1,1);
    }
    {
      int dtId;
      char *chartInstanceTypedefName =
        sf_chart_instance_typedef_name("test",1);
      dtId = ssRegisterDataType(S, chartInstanceTypedefName);
      if (dtId == INVALID_DTYPE_ID ) return;
      /* Register the size of the udt */
      if (!ssSetDataTypeSize(S, dtId, 8)) return;
      if(!ssSetNumDWork(S,1)) return;
      ssSetDWorkDataType(S, 0, dtId);
      ssSetDWorkWidth(S, 0, 1);
      ssSetDWorkName(S, 0, "ChartInstance"); /*optional name, less than 16 chars*/
    }
  }
  ssSetChecksum0(S,(2216518746U));
  ssSetChecksum1(S,(3391478485U));
  ssSetChecksum2(S,(3803223498U));
  ssSetChecksum3(S,(935393248U));
}

void terminate_test_sfun_c1(SimStruct *S)
{
}
static void mdlRTW_test_sfun_c1(SimStruct *S)
{
}

void sf_test_sfun_c1( void *);
void test_sfun_c1_registry(SimStruct *S)
{
  chartInstance.chartInfo.chartInstance = NULL;
  chartInstance.chartInfo.chartInitialized = 0;
  chartInstance.chartInfo.sFunctionGateway = sf_test_sfun_c1;
  chartInstance.chartInfo.initializeChart = initialize_test_sfun_c1;
  chartInstance.chartInfo.terminateChart = terminate_test_sfun_c1;
  chartInstance.chartInfo.mdlRTW = mdlRTW_test_sfun_c1;
  chartInstance.chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance.chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance.chartInfo.storeCurrentConfiguration = NULL;
  chartInstance.chartInfo.sampleTime = INHERITED_SAMPLE_TIME;
  chartInstance.S = S;
  ssSetUserData((SimStruct *)S,(void *)(&(chartInstance.chartInfo))); /* register the chart instance with simstruct */
  ssSetSampleTime((SimStruct *)S, 0, chartInstance.chartInfo.sampleTime);
  if (chartInstance.chartInfo.sampleTime == INHERITED_SAMPLE_TIME) {
    ssSetOffsetTime((SimStruct *)S, 0, FIXED_IN_MINOR_STEP_OFFSET);
  } else if (chartInstance.chartInfo.sampleTime == CONTINUOUS_SAMPLE_TIME) {
    ssSetOffsetTime((SimStruct *)S, 0, 0.0);
  }
  ssSetCallSystemOutput((SimStruct *)S,0);
}

void sf_test_sfun_c1(void *chartInstanceVoidPtr)
{
  /* Save current event being processed */
  uint8_T previousEvent;
  previousEvent = _sfEvent_;

  /* Update Stateflow time variable */
  _sfTime_ = ssGetT(chartInstance.S);

  /* Call this chart */
  _sfEvent_ = CALL_EVENT;
  test_sfun_c1();
  _sfEvent_ = previousEvent;
}

