/*
 *
 * Stateflow code generation for chart:
 *    test/Chart
 * 
 * Target Name                          : sfun
 * Model Version                        : 1.188
 * Stateflow Version                    : 5.0.0.13.00.1.000001
 * Date of code generation              : 28-Mar-2005 15:49:51
 *
 */

#ifndef __test_sfun_c1_h__
#define __test_sfun_c1_h__
#include "sfc_sf.h"
#include "sfc_mex.h"
extern void sf_test_sfun_c1_get_check_sum(mxArray *plhs[]);
extern void test_sfun_c1_registry(SimStruct *simStructPtr);
extern void test_sfun_c1_sizes_registry(SimStruct *simStructPtr);
extern void test_sfun_c1(void);

typedef struct SFtest_sfun_c1LocalDataStruct{
  real_T m0_c1_d1_nn1;
  real_T m0_c1_d2_nn2;
} SFtest_sfun_c1LocalDataStruct;
typedef struct SFtest_sfun_c1StateStruct{
  unsigned char is_active_test_sfun_c1;
  unsigned char is_test_sfun_c1;
} SFtest_sfun_c1StateStruct;

typedef struct S_SFtest_sfun_c1InstanceStruct {
  SFtest_sfun_c1LocalDataStruct LocalData;
  SFtest_sfun_c1StateStruct State;
  SimStruct *S;
  ChartInfoStruct chartInfo;
  unsigned int chartNumber;
  unsigned int instanceNumber;
} SFtest_sfun_c1InstanceStruct;

#endif

