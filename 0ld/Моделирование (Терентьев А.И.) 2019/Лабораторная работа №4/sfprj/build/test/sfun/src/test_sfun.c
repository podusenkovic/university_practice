/*
 *
 * Stateflow code generation for machine:
 *    test
 * 
 * Target Name                          : sfun
 * Model Version                        : 1.188
 * Stateflow Version                    : 5.0.0.13.00.1.000001
 * Date of code generation              : 28-Mar-2005 15:49:51
 *
 */

#include "test_sfun.h"
#include "test_sfun_c1.h"

/* Global machine event */
uint8_T _sfEvent_;
#include "test_sfun_debug_macros.h"
unsigned int _testMachineNumber_=UNREASONABLE_NUMBER;
unsigned int sf_test_process_check_sum_call( int nlhs, mxArray * plhs[], int
 nrhs, const mxArray * prhs[] )
{
#ifdef MATLAB_MEX_FILE
  char commandName[20];
  if (nrhs<1 || !mxIsChar(prhs[0]) ) return 0;
  /* Possible call to get the checksum */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if(strcmp(commandName,"sf_get_check_sum")) return 0;
  plhs[0] = mxCreateDoubleMatrix( 1,4,mxREAL);
  if(nrhs>1 && mxIsChar(prhs[1])) {
    mxGetString(prhs[1], commandName,sizeof(commandName)/sizeof(char));
    commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
    if(!strcmp(commandName,"machine")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(554431132U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2014617527U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(974830576U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2775916956U);
    }else if(!strcmp(commandName,"exportedFcn")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0U);
    }else if(!strcmp(commandName,"makefile")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(996753256U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3932677959U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2928430730U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(3721628109U);
    }else if(nrhs==3 && !strcmp(commandName,"chart")) {
      unsigned int chartFileNumber;
      chartFileNumber = (unsigned int)mxGetScalar(prhs[2]);
      switch(chartFileNumber) {
       case 1:
        {
          extern void sf_test_sfun_c1_get_check_sum(mxArray *plhs[]);
          sf_test_sfun_c1_get_check_sum(plhs);
          break;
        }

       default:
        ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0.0);
      }
    }else if(!strcmp(commandName,"target")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1682224296U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1034660693U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3301066214U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1413350527U);
    }else {
      return 0;
    }
  } else{
    ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(4212308423U);
    ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3542742449U);
    ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3483422963U);
    ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(3629791875U);
  }
  return 1;
#else
  return 0;
#endif
}
/* Stateflow time variable */
real_T _sfTime_;

/* Machine initialize */
void test_initializer(void)
{
  _testMachineNumber_ = sf_debug_initialize_machine("test","sfun",0,1,0,0,0);
  sf_debug_set_machine_event_thresholds(_testMachineNumber_,0,0);
  sf_debug_set_machine_data_thresholds(_testMachineNumber_,0);
}

unsigned int test_registry(SimStruct *simstructPtr,char *chartName, int
 initializeFlag)
{
  if(!strcmp_ignore_ws(chartName,"test/Chart/ SFunction ")) {
    test_sfun_c1_registry(simstructPtr);
    return 1;
  }
  return 0;
}
unsigned int test_sizes_registry(SimStruct *simstructPtr,char *chartName)
{
  if(!strcmp_ignore_ws(chartName,"test/Chart/ SFunction ")) {
    test_sfun_c1_sizes_registry(simstructPtr);
    return 1;
  }
  return 0;
}
void test_terminator(void)
{
}

