#ifndef __c1_zadanie3_h__
#define __c1_zadanie3_h__

/* Include files */
#include "sfc_sf.h"
#include "sfc_mex.h"
#include "rtw_capi.h"
#include "rtw_modelmap.h"

/* Type Definitions */
typedef struct {
  SimStruct *S;
  void *c1_testPointAddrMap[2];
  uint32_T chartNumber;
  uint32_T instanceNumber;
  uint8_T c1_is_active_c1_zadanie3;
  uint8_T c1_is_c1_zadanie3;
  uint8_T c1_tp_A;
  uint8_T c1_tp_B;
  rtwCAPI_ModelMappingInfo c1_testPointMappingInfo;
  ChartInfoStruct chartInfo;
} SFc1_zadanie3InstanceStruct;

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */

extern void sf_c1_zadanie3_get_check_sum(mxArray *plhs[]);
extern void c1_zadanie3_method_dispatcher(SimStruct *S, int_T method, void
 *data);

#endif

