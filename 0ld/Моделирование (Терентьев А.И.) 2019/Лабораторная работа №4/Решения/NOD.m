function result = NOD(a,b)
    if (a ==0 || b == 0)
        error('no zero values');
    end
    a = abs(a); b = abs(b);
    while (a ~= b)
        if (a < b)
            b = b - a;
        else a = a - b;
        end
    end
    result = a;
end