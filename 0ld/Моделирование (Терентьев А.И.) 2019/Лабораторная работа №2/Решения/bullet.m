clear; clc; 
M = 10; 
g = 9.8; 
v = 300; 
m = 0.001:0.001:3;
l = 0.1:0.1:3;
[m_meshed, l_meshed] = meshgrid(m,l);
a = acos(1 - m_meshed.^2 * v.^2 ./ (2 * (m_meshed + M).^2 * g .* l_meshed));
a = rad2deg(a);
mesh(m*1000, l, a); grid;
xlabel('m, gramm');
ylabel('l, meters');
zlabel('\alpha, \circ')
title('\alpha = f(m). v = 300 m/c');