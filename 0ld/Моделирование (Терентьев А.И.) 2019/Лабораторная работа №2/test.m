hF = figure('Units', 'points', 'MenuBar', 'none', 'Color', 'k');
	p = get(hF, 'Position')
	set(hF, 'Position', [p(1) p(2) 150 20])
	axes('Position', [0 0 1 1], 'Visible', 'off')
	% �������� ���������� ������� � ������������� �� ����������� �� ����
	hT = text('Position', [0 0], 'VerticalAlignment', 'bottom', 'FontSize', 20, 'Color', 'g');
	% ������� ������ ��� ������� ������
	TEXT = 'This is a simple example of creeping line';
	% �������� ������ ��������� ����������
	str = '';
	% ��������� ��������� ���������� � �����
	for k=1:length(TEXT)
	    if length(str)<15
	        str = [str TEXT(k)];
	    else
	        % ����� ���������� ������ ������� ������
	        str = [str(2:length(str)) TEXT(k)];
	    end
	    set(hT, 'String', str)
	    pause(0.1)
	end
