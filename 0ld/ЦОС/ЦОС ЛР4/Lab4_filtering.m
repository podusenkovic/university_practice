function hhg = lab4plot(AFC,N,Nf);
x=0;
h = ifft(AFC);
hh = [h(N-floor(Nf/2)+1:N), h(1:ceil(Nf/2))];

g = 0.54 + 0.46*cos(2*pi*([0:Nf-1]/(Nf-1)-0.5));

hhg = hh.*g;

AFChh = abs(fft([hh, zeros(1,N-Nf)]));
AFChhg = abs(fft([hhg, zeros(1,N-Nf)]));
fi=angle(fft([hhg, zeros(1,N-Nf)]));

figure;
subplot(4,2,1); plot(abs(h)), grid, title('h');
subplot(4,2,3); plot(abs(hh)), grid, title('hh');
subplot(4,2,5); plot(abs(hhg)), grid, title('hhg');

subplot(4,2,2); plot(AFC), grid, title('AFC');
subplot(4,2,4); plot(20*log(AFChh/max(AFChh))), grid, title('achh(hh), dB');
subplot(4,2,6); plot(20*log(AFChhg/max(AFChhg))), grid, title('achh(hhg), dB');
subplot(4,2,8); plot(fi), grid, title('fi(hhg)');