close all;

%% Ex. 1
close all;
fs = 720;
N = 1024;

%Lowpass
fc = 148;
fz = 220;

Nf = round(abs(3*fs / (fz-fc)));

Nc = round(N*fc / fs);
Nz = round(N*fz / fs);

AFC = [ones(1,Nc), linspace(1,0,Nz-Nc), zeros(1,N/2-Nz)];
AFC = [AFC, fliplr(AFC)];

Lab4_filtering(AFC, N, Nf);

%Highpass
fc = 220;
fz = 148;

Nf = abs(3*fs / (fz-fc));

Nc = round(N*fc / fs);
Nz = round(N*fz / fs);

AFC = [zeros(1,Nz), linspace(0,1, Nc-Nz), ones(1,N/2-Nc)];
AFC = [AFC, fliplr(AFC)];

Lab4_filtering(AFC, N, Nf);

%Bandpass
f0 = 150;
fc = 100;
fz = 170;

f = round(abs(3*fs / (fz-fc)));

N0 = round(N*f0 / fs);
Nc = round(N*fc / fs);
Nz = round(N*fz / fs);

AFC = [zeros(1,N0-round(Nz/2)), linspace(0,1,round(Nz/2)-ceil(Nc/2)), ones(1,Nc), linspace(1,0,round(Nz/2)-floor(Nc/2)), zeros(1,N/2-N0-round(Nz/2))];
AFC = [AFC, fliplr(AFC)];

Lab4_filtering(AFC, N, Nf);

%Bandstop
f0 = 150;
fc = 170;
fz = 100;

Nf = round(abs(3*fs / (fz-fc)));

N0 = round(N*f0 / fs);
Nc = round(N*fc / fs);
Nz = round(N*fz / fs);

AFC = [ones(1,N0-round(Nc/2)), linspace(1,0,round(Nc/2)-ceil(Nz/2)), zeros(1,Nz), linspace(0,1,round(Nc/2)-floor(Nz/2)), ones(1,N/2-N0-round(Nc/2))];
AFC = [AFC, fliplr(AFC)];

Lab4_filtering(AFC, N, Nf);

%Differentitator
fc = 288;

Nf = round(abs(3*fs / (f/2-fc)));
Nc = round(N*fc / fs);

AFC = [linspace(0,1,Nc), linspace(1,-1,N-2*Nc), linspace(-1,0,Nc)];

Lab4_filtering(AFC, N, Nf);

%Hilbert
fc = 72;

Nf = round(abs(3*fs / (fc)));
Nc = round(N*fc / fs);

AFC = [linspace(0,1,Nc), ones(1,N/2-Nc)];
AFC = [AFC, -fliplr(AFC)];

Lab4_filtering(AFC, N, Nf);

%% Ex. 3
close all;

N = 1024;
fs = 720;
Nimp = round(0.07 * N);

%s1
f01 = 100;
As1 = 50;
fi01 = 0;
%s2 
f02 = 250;
As2 = 25;
fi02 = 0;

q_i = 0:Nimp-1;

s1 = As1 * sin(pi * q_i * f01/(fs/2) + fi01);
s2 = As2 * sin(pi * q_i * f02/(fs/2) + fi02);

ss = s1+s2;
ss = [ss, zeros(1,N-Nimp)];
SS = fft(ss);

%Bandpass1
f01 = 100;
fc1 = 100;
fz1 = 170;

Nf = round(abs(3*fs/(fz1-fc1)));

N0 = round(N*f01/fs);
Nc = round(N*fc1/fs);
Nz = round(N*fz1/fs);

%1
AFC = [zeros(1,N0-round(Nz/2)),linspace(0,1,round(Nz/2)-ceil(Nc/2)),ones(1,Nc),linspace(1,0,round(Nz/2)-floor(Nc/2)),zeros(1,N/2-N0-round(Nz/2))];
AFC = [AFC, fliplr(AFC)];
bp1=Lab4_filtering(AFC,N,Nf);

%2
s=zeros(1,length(bp1));
for i=1:1:N
    s=[0 s(1:length(s)-1)];
    s(1)=ss(i);
    premx=bp1.*s;
    ssbp1(i)=sum(premx);
end
SSbp1=fft(ssbp1);
figure;
subplot(4,2,1); plot(ss), grid, title('ss');
subplot(4,2,5); plot(abs(ssbp1)), grid, title('ssbp1');

subplot(4,2,2); plot(abs(SS)), grid, title('SS');
subplot(4,2,4); plot(angle(SS)), grid, title('fi(SS)');
subplot(4,2,6); plot(abs(SSbp1)), grid, title('SSbp1');
subplot(4,2,8); plot(angle(SSbp1)), grid, title('fi(SSbp1)');

%Bandpass2
f02=250;
fc2=100;
fz2=170; 

Nf = round(abs(3*fs/(fz2-fc2)));

N0 = round(N*f02/fs);
Nc = round(N*fc2/fs);
Nz = round(N*fz2/fs);

%1
AFC = [zeros(1,N0-round(Nz/2)),linspace(0,1,round(Nz/2)-ceil(Nc/2)),ones(1,Nc),linspace(1,0,round(Nz/2)-floor(Nc/2)),zeros(1,N/2-N0-round(Nz/2))];
AFC = [AFC, fliplr(AFC)];
bp2=Lab4_filtering(AFC,N,Nf);

%2
s=zeros(1,length(bp2));
for i=1:1:N
    s=[0 s(1:length(s)-1)];
    s(1)=ss(i);
    premx=bp2.*s;
    ssbp2(i)=sum(premx);
end
SSbp2=fft(ssbp2);
figure;
subplot(4,2,1); plot(ss), grid, title('ss');
subplot(4,2,5); plot(abs(ssbp2)), grid, title('ssbp2');

subplot(4,2,2); plot(abs(SS)), grid, title('SS');
subplot(4,2,4); plot(angle(SS)), grid, title('fi(SS)');
subplot(4,2,6); plot(abs(SSbp2)), grid, title('SSbp2');
subplot(4,2,8); plot(angle(SSbp2)), grid, title('fi(SSbp2)');



