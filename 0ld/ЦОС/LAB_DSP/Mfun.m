%---------1-------
function result = Mfun(A,C)
    if (length(A) ~= length(C))
        then error('Different lengths');
    end
    len = length(A);
    result = [1:(2^len - 1)] .* 0;
    for i = 1:(2^len - 1)
        result(i) = A(len);
        temp = mod(sum(A .* C),2);
        A = circshift(A', 1)';
        A(1) = temp;
    end
end