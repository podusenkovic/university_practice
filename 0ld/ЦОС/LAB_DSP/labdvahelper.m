%ksp
clear all;
close all;
clc;

L = 1000;
Ni = 15; % ����� ��������� ������� � ��������
f0 = 2; fi0 = pi;
N = 128;
T = 1/36; 
shift = 5;

s = @(x) cos(2*pi*f0*x + fi0) + j*sin(2*pi*f0*x + fi0);
X = 0:T:T*(Ni-1);
S=s(X);
floor(Ni/2);
S1 = S(1:floor(Ni/2));
S2 = S(floor(Ni/2)+1:Ni);
S = [S2 zeros(1,N-Ni) S1];

figure
plot(real(S), '-b')
hold on
plot(imag(S), '-r'), legend('Cos', 'Sin');
grid, title('Signal with protective zeros:');
hold off

F1 = fft(S);
figure,plot(abs(F1)),grid;

figure, plot(atan2(imag(F1)+0.00001,real(F1)+0.001)), title('Angle:'), grid;

for i=1:length(abs(F1))
        if abs(F1(i)) == max(abs(F1))
            pos = i;
        end
        if abs(F1(i)) < 1
            %i/(length(X)*T)
        end
end

fprintf('�������� �������� ��������� ������������ ������� A = %f\n',max(abs(F1)));
fprintf('��������� �������� ��������� ������������ ������� f�0 = %d\n',pos);
pos = pos/(N*T);
fprintf('��������� �������� ��������� ������������ ������� (� �����) f�0 = %d\n',pos);
Fs = 1/T;
fprintf('��������� �������� ��������� ������������ ������� (����) (� �����) f�0 = %d\n',f0*T*N);
fprintf('���� ������������ ������� f�1 = %d\n',N/Ni);
fprintf('���� ������������ ������� f�1 = %d\n',N/Ni);
