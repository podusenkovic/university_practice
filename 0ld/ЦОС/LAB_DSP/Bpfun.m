function result = Bpfun(w,r)
    Wprev = w; 
    Wnext = zeros(1,length(w));
    for i = 0:r-2       
        stepSize = length(w) / (2^i);
        for j = 1:length(w)
            size_of_current_single_part = stepSize / 2;
            increase_current_single_part = mod(j,stepSize);
            if(increase_current_single_part == 0)
                increase_current_single_part = size_of_current_single_part + 1;
            end
            if(increase_current_single_part <= size_of_current_single_part)
                Wnext(j) = Wprev(j) + Wprev(j + size_of_current_single_part);
            else
                Wnext(j) = Wprev(j - size_of_current_single_part) - Wprev(j);
            end
        end
        Wprev = Wnext;
    end
    result = Wprev;
end