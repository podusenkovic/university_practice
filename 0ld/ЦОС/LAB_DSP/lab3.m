clc, clear, close all;

Nvar = 12;

f0 = 180 + Nvar;
F = 25 + Nvar;
fs = 800 + Nvar;
Nimp = 400 + Nvar;
fdp = 0;
c = 2 / Nvar;
N = 1024;
T = 1 / fs;

As = 1;

ti = Nimp * T;
tai = Nimp * T;

s_step = (ti / (Nimp - 1));
h_step = (tai / (Nimp - 1));

%����������� �����
s = @(t) As * (cos(pi * F * t.^2/ti + pi * 2 * f0 * t));
h = @(t) cos(-pi * F * t.^2/ti + pi * 2 * f0 * t);

S = [s(0:s_step:ti/2), zeros(1,N - Nimp + 1), s(-ti/2:s_step:-s_step)];
H = [h(0:h_step:tai/2), zeros(1,N - Nimp + 1), h(-tai/2:h_step:-h_step)];

figure('Name','����������� �����')
subplot(4,2,2); plot(abs(fft(S))), grid, title('fft(S)');
subplot(4,2,1); plot(real(S)), grid, title('real(s)');
subplot(4,2,3); plot(imag(S)), grid, title('imag(s)');
subplot(4,2,4); plot(angle(fft(S))), grid, title('angle(S)');

subplot(4,2,6); plot(abs(fft(H))), grid, title('fft(H)');
subplot(4,2,5); plot(real(H)), grid, title('real(h)');
subplot(4,2,7); plot(imag(H)), grid, title('imag(h)');
subplot(4,2,8); plot(angle(fft(H))), grid, title('angle(H)');

figure('Name','����������� ����� multiplied SjatiiSg')
SjatiiSg = ifft(fft(S).*fft(H));
subplot(4,2,1); plot(real(SjatiiSg)), grid, title('real(SjatiiSg)');
subplot(4,2,3); plot(imag(SjatiiSg)), grid, title('imag(SjatiiSg)');
subplot(4,2,5); plot(abs(SjatiiSg)), grid, title('abs(SjatiiSg)');
subplot(4,2,7); plot(20*log((abs(SjatiiSg)+0.1)/max(abs(SjatiiSg)))), grid, title('abs(SjatiiSg),dB');

multiplied = fft(S).*fft(H);
subplot(4,2,2); plot(real(multiplied)), grid, title('real(multiplied)');
subplot(4,2,4); plot(imag(multiplied)), grid, title('imag(multiplied)');
subplot(4,2,6); plot(abs(multiplied)), grid, title('abs(multiplied)');
subplot(4,2,8); plot(angle(multiplied)), grid, title('fi(multiplied)');

%����������� �� ����
s = @(t) As * (cos(pi * F * t.^2/ti + pi * (2 * f0 - F * T) * t));
h = @(t) cos(-pi * F * t.^2/ti - pi * (2 * f0 - F * T) * t);

S = [s(0:s_step:ti), zeros(1,N-Nimp)];
H = [fliplr(h(0:h_step:tai)), zeros(1,N-Nimp)];

figure('Name','����������� �� ����')
subplot(4,2,2); plot(abs(fft(S))), grid, title('fft(S)');
subplot(4,2,1); plot(real(S)), grid, title('real(s)');
subplot(4,2,3); plot(imag(S)), grid, title('imag(s)');
subplot(4,2,4); plot(angle(fft(S))), grid, title('angle(S)');

subplot(4,2,6); plot(abs(fft(H))), grid, title('fft(H)');
subplot(4,2,5); plot(real(H)), grid, title('real(h)');
subplot(4,2,7); plot(imag(H)), grid, title('imag(h)');
subplot(4,2,8); plot(angle(fft(H))), grid, title('angle(H)');

figure('Name','����������� �� ���� multiplied SjatiiSg')
SjatiiSg = ifft(fft(S).*fft(H));
subplot(4,2,1); plot(real(SjatiiSg)), grid, title('real(SjatiiSg)');
subplot(4,2,3); plot(imag(SjatiiSg)), grid, title('imag(SjatiiSg)');
subplot(4,2,5); plot(abs(SjatiiSg)), grid, title('abs(SjatiiSg)');
subplot(4,2,7); plot(20*log((abs(SjatiiSg)+0.1)/max(abs(SjatiiSg)))), grid, title('abs(SjatiiSg),dB');

multiplied = fft(S).*fft(H);
subplot(4,2,2); plot(real(multiplied)), grid, title('real(multiplied)');
subplot(4,2,4); plot(imag(multiplied)), grid, title('imag(multiplied)');
subplot(4,2,6); plot(abs(multiplied)), grid, title('abs(multiplied)');
subplot(4,2,8); plot(angle(multiplied)), grid, title('fi(multiplied)');

% ����������� �����
s = @(t) As*(cos(pi * F * t.^2/ti) + j * sin(pi * F * t.^2/ti));
h = @(t) cos(-pi * (F * (-t).^2/tai))+j * sin(-pi * (F * (-t).^2/tai));

S = [s(0:s_step:ti/2), zeros(1,N - Nimp + 1), s(-ti/2:s_step:-s_step)];
H = [h(0:h_step:tai/2), zeros(1,N - Nimp + 1), h(-tai/2:h_step:-h_step)];

figure('Name','����������� �����')
subplot(4,2,2); plot(abs(fft(S))), grid, title('fft(S)');
subplot(4,2,1); plot(real(S)), grid, title('real(s)');
subplot(4,2,3); plot(imag(S)), grid, title('imag(s)');
subplot(4,2,4); plot(angle(fft(S))), grid, title('angle(S)');

subplot(4,2,6); plot(abs(fft(H))), grid, title('fft(H)');
subplot(4,2,5); plot(real(H)), grid, title('real(h)');
subplot(4,2,7); plot(imag(H)), grid, title('imag(h)');
subplot(4,2,8); plot(angle(fft(H))), grid, title('angle(H)');

figure('Name','����������� ����� multiplied SjatiiSg')
SjatiiSg = ifft(fft(S).*fft(H));
subplot(4,2,1); plot(real(SjatiiSg)), grid, title('real(SjatiiSg)');
subplot(4,2,3); plot(imag(SjatiiSg)), grid, title('imag(SjatiiSg)');
subplot(4,2,5); plot(abs(SjatiiSg)), grid, title('abs(SjatiiSg)');
subplot(4,2,7); plot(atan2(imag(SjatiiSg)+0.00001,real(SjatiiSg)+0.001)), grid, title('fi(SjatiiSg)');

multiplied = fft(S).*fft(H);
subplot(4,2,2); plot(real(multiplied)), grid, title('real(multiplied)');
subplot(4,2,4); plot(imag(multiplied)), grid, title('imag(multiplied)');
subplot(4,2,6); plot(abs(multiplied)), grid, title('abs(multiplied)');
subplot(4,2,8); plot(angle(multiplied)), grid, title('fi(multiplied)');

%����������� �� ����
s = @(t) As*(cos(pi * (F * t.^2/ti - F * t)) + j * sin(pi * (F * t.^2/ti - F * t)));
h = @(t) cos(-pi * (F * (ti - t).^2/ti - F * (ti - t))) + j * sin(-pi * (F * (ti - t).^2/ti - F * (ti - t)));

S = [s(0:s_step:ti), zeros(1,N - Nimp)];
H = [fliplr(h(0:h_step:tai)), zeros(1,N - Nimp)];

figure('Name','����������� �� ����')
subplot(4,2,2); plot(abs(fft(S))), grid, title('fft(S)');
subplot(4,2,1); plot(real(S)), grid, title('real(s)');
subplot(4,2,3); plot(imag(S)), grid, title('imag(s)');
subplot(4,2,4); plot(angle(fft(S))), grid, title('angle(S)');

subplot(4,2,6); plot(abs(fft(H))), grid, title('fft(H)');
subplot(4,2,5); plot(real(H)), grid, title('real(h)');
subplot(4,2,7); plot(imag(H)), grid, title('imag(h)');
subplot(4,2,8); plot(angle(fft(H))), grid, title('angle(H)');

figure('Name','����������� �� ���� multiplied SjatiiSg')
SjatiiSg = ifft(fft(S).*fft(H));
subplot(4,2,1); plot(real(SjatiiSg)), grid, title('real(SjatiiSg)');
subplot(4,2,3); plot(imag(SjatiiSg)), grid, title('imag(SjatiiSg)');
subplot(4,2,5); plot(abs(SjatiiSg)), grid, title('abs(SjatiiSg)');
subplot(4,2,7); plot(atan2(imag(SjatiiSg)+0.00001,real(SjatiiSg)+0.001)), grid, title('fi(SjatiiSg)');

multiplied = fft(S).*fft(H);
subplot(4,2,2); plot(real(multiplied)), grid, title('real(multiplied)');
subplot(4,2,4); plot(imag(multiplied)), grid, title('imag(multiplied)');
subplot(4,2,6); plot(abs(multiplied)), grid, title('abs(multiplied)');
subplot(4,2,8); plot(angle(multiplied)), grid, title('fi(multiplied)');

