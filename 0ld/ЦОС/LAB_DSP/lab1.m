clc, clear, close all
%---------2-------
A = [1,0,0,0,0,0,1,1,0,0];
C1 = [0,1,1,0,0,0,1,1,1,1];
C2 = [0,1,1,0,0,1,0,1,1,1];
M1 = Mfun(A,C1);
M2 = Mfun(A,C2);
convM1 = (M1 * -2) + 1;
convM2 = (M2 * -2) + 1;
figure
subplot(3, 1, 1), hold on, grid on
plot(convM1)
subplot(3, 1, 2), hold on, grid on
plot(convM2)
M1 = convM1;
M2 = convM2;
%---------3-------
shift = 100 + 12 * 10;
invM2 = M2 .* -1;
inv_shift_m2 = [[1:shift].*0, invM2];
sumM1_invshM2 = [M1, [1:shift].*0] + inv_shift_m2;
subplot(3, 1, 3), hold on, grid on
ylim([-2, 2]);
plot(sumM1_invshM2)
%---------5-------
figure
subplot(4,2,1);
M1AKF = my_sf(M1,M1);
x = 1:length(M1AKF);
plot(x,M1AKF);
hold on, grid on;
title('��� ��� �1');
subplot(4,2,2);
plot(x,xcorr(M1,M1));
hold on, grid on;
title('��� ��� �1 ����� xcorr');
%---------6-------
subplot(4,2,3);
M2VKF = my_sf(M1,M2);
x = 1:length(M2VKF);
plot(x,M2VKF);
hold on, grid on;
title('��� ��� �1 � �2');
subplot(4,2,4);
plot(x,xcorr(M1,M2));
hold on, grid on;
title('��� ��� �1 � �2 ����� xcorr');
%---------7-------
subplot(4,2,5);
M1Filter = my_sf(sumM1_invshM2,M1);
x = 1:length(M1Filter);
plot(x,M1Filter);
hold on, grid on;
title('������ �1');
subplot(4,2,6);
plot(xcorr(sumM1_invshM2, M1));
hold on, grid on;
title('������ �1 ����� xcorr');
%---------8-------
subplot(4,2,7);
M2Filter = my_sf(sumM1_invshM2, inv_shift_m2);
x = 1:length(M2Filter);
plot(x,M2Filter);
hold on, grid on;
title('������ �2');
subplot(4,2,8);
plot(xcorr(sumM1_invshM2, inv_shift_m2));
hold on, grid on;
title('������ �2 ����� xcorr');

err = sum(M2Filter - xcorr(sumM1_invshM2, inv_shift_m2))
%---------10-------
Amplitude = 12;
Noise = Amplitude-2*Amplitude*rand(1,length(M1)); %��� ���
%---------11-------
M3 = M1 + Noise;
figure 
subplot(3,1,1);
hold on, grid on;
plot(M3);
title('M3 = M1 + ���');
%---------12-------
subplot(3,1,2);
hold on, grid on;
M3filtered = my_sf(M3,M3);
plot(M3filtered);
title('������ �1 �� M3 = ����');
%---------13-------
Db=20*log10(abs(M3filtered/max(M3filtered)));
subplot(3,1,3);
hold on, grid on;
plot(Db);
title('������ �1 �� M3 = ���� , ��');
%------------------
%-----WOLSH--------
%--------2---------
data1 = 12; data2 = 239; r = 10;
W1 = Wfun_(data1,r);
W2 = Wfun_(data2,r);
figure
subplot(3,1,1);
hold on, grid on;
plot(W1);
title('W1(12)');
subplot(3,1,2);
hold on ,grid on;
plot(W2);
title('W2(239)');
%--------3---------
Wsum = W1 + W2;
subplot(3,1,3);
hold on, grid on;
plot(Wsum);
title('Wsum');
%--------5---------
figure
hold on, grid on;
Bpf_Wsum = Bpfun(Wsum, r);
plot(Bpf_Wsum)
[minY minX] = max(Bpf_Wsum(1:length(Bpf_Wsum) / 2));
[maxY maxX] = max(Bpf_Wsum((length(Bpf_Wsum) / 2):end));
minX = 97 - 1
maxX = 495 - 1

firstValue = bi2de([de2bi(maxX,'left-msb',log2(length(W1))) 0])
secondValue = bi2de([de2bi(minX,'left-msb',log2(length(W1))) 0])
title('��� �� Wsum');
%--------6---------
Amp = data1;
Noise = Amp - 2 * Amp * rand(1, length(Wsum));
%--------7---------
Wsum_w_noise = Wsum + Noise;
%--------8---------
figure
hold on, grid on;
plot(Bpfun(Wsum_w_noise,r))
title('��� �� Wsum � �����');

