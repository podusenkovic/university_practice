clc, clear all, close all, format compact

fs = 720;
fn = fs/2;
N = 1024; 
Nn = N/2;   

% ���
fc = 148; fz = 220;
Nf = round(abs(3 * fs/ (fz - fc)));
Nf = Nf + ~mod(Nf, 2); Nf
Nc = round(N * fc/fs);
Nz = round(N * fz/fs);
FR = [ones(1, Nc), linspace(1, 0, Nz - Nc + 1), zeros(1, Nn - Nz)];
FR = [FR, fliplr(FR(2:length(FR) - 1))];
figure('Name', 'Task 1. ���');
lab_4_calc(FR, N, Nf);

% ���
fz = 148; fc = 220;
Nf = round(abs(3 * fs/(fc - fz)));
Nf = Nf + ~mod(Nf, 2); Nf
Nc = round(N * fc/fs);
Nz = round(N * fz/fs);
FR = [zeros(1, Nz), linspace(0, 1, Nc - Nz + 1), ones(1, Nn - Nc)];
FR = [FR, fliplr(FR(2:length(FR) - 1))];
figure('Name','Task 1. ���');
lab_4_calc(FR, N, Nf);

% ��
f0 = 150; fc = 100; fz = 170;
Nf = round(abs(3 * fs/(-(f0 - fz/2) + (f0 - fc/2))));
Nf = Nf + mod(Nf, 2); Nf
N0 = round(N * f0/fs);
Nc = round(N * fc/fs);
Nz = round(N * fz/fs); 
FR = [zeros(1, N0 - Nz/2), linspace(0, 1, Nz/2 - Nc/2), ones(1,Nc), linspace(1, 0, Nz/2 - Nc/2), zeros(1, Nn - N0 - Nz/2)];
FR = [FR, fliplr(FR)];
figure('Name','Task 1. ��');
lab_4_calc(FR, N, Nf);

% ��
f0 = 150; fc = 170; fz = 100;
Nf = round(abs(3 * fs/((f0 - fz/2) - (f0 - fc/2))));
Nf = Nf + mod(Nf, 2); Nf
N0 = round(N * f0/fs);
Nc = round(N * fc/fs);
Nz = round(N * fz/fs);
FR = [ones(1, N0 - Nc/2), linspace(1, 0, Nc/2 - Nz/2), zeros(1, Nz), linspace(0, 1, Nc/2 - Nz/2), ones(1, Nn - N0 - Nc/2)];
FR = [FR, fliplr(FR)];
figure('Name','Task 1. ��');
lab_4_calc(FR, N, Nf);

% ��
fc = 288; fz = fn; 
Nf = round(abs(3 * fs/(fn - fc)));
Nf = Nf + ~mod(Nf, 2); Nf
Nc = round(N * fc/fs); 
FR = [linspace(0, 1, Nc), linspace(1, -1, N - 2*Nc), linspace(-1, 0, Nc)];
FR = 1i.*FR;
figure('Name','Task 1. ��');
lab_4_calc(FR, N, Nf);

% ��
fc = 72;
Nf = round(abs(3 * fs/(fc)));
Nf = Nf + ~mod(Nf, 2); Nf 
Nc = round(N * fc/fs);
FR = [linspace(0,1, Nc), ones(1, Nn - 2*Nc), linspace(1, 0, Nc)];
FR = [FR, -fliplr(FR)];
FR = 1i.*FR;
figure('Name','Task 1. ��');
lab_4_calc(FR, N, Nf);

