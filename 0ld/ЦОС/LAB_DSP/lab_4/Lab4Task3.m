clc, clear all, close all, format compact

N = 1024;
Nn = N/2;
fs = 720;
Nimp = 0.07;

f01 = 100; As1 = 50; phi01 = 0; 
f02 = 250; As2 = 25; phi02 = 0;

Sg = @(As, f, phi) As * sin(2 * pi * f * (0:1/fs:Nimp) + phi);
sig1 = Sg(As1, f01, phi01);
sig2 = Sg(As2, f02, phi02);
sumSig = sig1 + sig2;
sumSig = [sumSig, zeros(1,N - length(sumSig))];
specSumSig = fft(sumSig);

%��1
f0 = 100; fc = 100; fz = 170;
Nf = round(abs(3 * fs/((fz - fc)/2))); 
Nf = Nf + ~mod(Nf,2); Nf
N0 = round(N * f0/fs);
Nc = round(N * fc/fs);
Nz = round(N * fz/fs);
FR = [zeros(1, N0 - round(Nz/2)), ... 
    linspace(0, 1, round(Nz/2) - ceil(Nc/2)), ...
    ones(1,Nc), ...
    linspace(1, 0, round(Nz/2) - floor(Nc/2)), ...
    zeros(1, Nn - N0 - round(Nz/2))];
FR = [FR, fliplr(FR)];
figure('Name','Task 3. ��1');
h_hemming1 = lab_4_calc(FR, N, Nf);

%��2
f0=250; fc=100; fz=170; 
Nf = round(abs(3 * fs/((fz - fc)/2)));
Nf = Nf + ~mod(Nf,2); Nf
N0 = round(N * f0/fs);
Nc = round(N * fc/fs);
Nz = round(N * fz/fs);
FR = [zeros(1, N0 - round(Nz/2)), ...
    linspace(0, 1, round(Nz/2) - ceil(Nc/2)), ...
    ones(1, Nc), ...
    linspace(1, 0, round(Nz/2) - floor(Nc/2)), ...
    zeros(1, Nn - N0 - round(Nz/2))]; 
FR = [FR, fliplr(FR)];
figure('Name','Task 3. ��2');
h_hemming2 = lab_4_calc(FR, N, Nf);

SumSigHeimming1 = conv(sumSig, h_hemming1, 'same');
specSumSigHeimming1 = fft(SumSigHeimming1);
figure('Name','sig1 �� sumSig');
subplot(4,2,1); plot((0:Nimp * fs), sumSig(1:Nimp * fs + 1)),     grid, title('SumSig');
subplot(4,2,5); plot((0:Nimp * fs), real(SumSigHeimming1(1:Nimp * fs + 1))), grid, title('SumSigHeimming 1');
 
subplot(4,2,2); plot((0:N-1) * fs/N, abs(specSumSig)/(Nimp * fs) * 2),  grid, title('abs(SS)');
subplot(4,2,4); plot((0:N-1) * fs/N, unwrap(angle(specSumSig))),        grid, title('phi(SS)');
subplot(4,2,6); plot((0:N-1) * fs/N, abs(specSumSigHeimming1)/(Nimp * fs) * 2),        grid, title('abs(SSH1)');
subplot(4,2,8); plot((0:N-1) * fs/N, unwrap(angle(specSumSigHeimming1))),              grid, title('phi(SSH1)');

SumSigHeimming2 = conv(sumSig, h_hemming2, 'same');
specSumSigHeimming2 = fft(SumSigHeimming2);
figure('Name','sig2 �� sumSig');
subplot(4,2,1); plot((0:Nimp * fs), sumSig(1:Nimp * fs + 1)),     grid, title('SumSig');
subplot(4,2,5); plot((0:Nimp * fs), real(SumSigHeimming2(1:Nimp * fs + 1))), grid, title('SumSigHeimming 2');
 
subplot(4,2,2); plot((0:N-1) * fs/N, abs(specSumSig)/(Nimp * fs) * 2),  grid, title('abs(SS)');
subplot(4,2,4); plot((0:N-1) * fs/N, unwrap(angle(specSumSig))),        grid, title('phi(SS)');
subplot(4,2,6); plot((0:N-1) * fs/N, abs(specSumSigHeimming2)/(Nimp * fs) * 2),        grid, title('abs(SSH2)');
subplot(4,2,8); plot((0:N-1) * fs/N, unwrap(angle(specSumSigHeimming2))),              grid, title('phi(SSH2)');