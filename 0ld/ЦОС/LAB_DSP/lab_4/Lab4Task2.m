clc, clear all, close all, format compact

fs = 720;
N = 8;
fn = fs/2;
Nn = N/2;
Nf = 7;

% ���
fc = 148; fz = 220;
Nf_ = round(abs(3 * fs/(fz - fc)));
Nf_ = Nf_ + ~mod(Nf_,2); Nf_
FR = [1 1 1 0 0 0 1 1];
figure('Name','Task 2. ���');
lab_4_calc(FR, N, Nf);

% ���
fc = 220; fz = 148;
Nf_ = round(abs(3 * fs/(fc - fz)));
Nf_ = Nf_ + ~mod(Nf_,2); Nf_
FR = [0 0 0 1 1 1 0 0];
figure('Name', 'Task 2. ���');
lab_4_calc(FR, N, Nf);

% ��
fc = 100; fz = 170; f0 = 150;
Nf_ = round(abs(3 * fs/(-(f0 - fz/2) + (f0 - fc/2))));
Nf_ = Nf_ + ~mod(Nf_,2); Nf_
FR = [0 0 1 0 0 0 1 0];
figure('Name','Task 2. ��');
lab_4_calc(FR, N, Nf);

% ��
fz = 100; fc = 170; f0 = 150;
Nf_ = round(abs(3 * fs/((f0 - fz/2) - (f0 - fc/2))));
Nf_ = Nf_ + ~mod(Nf_,2); Nf_
FR = [1 0 0 1 1 1 0 0];
figure('Name','Task 2. ��');
lab_4_calc(FR, N, Nf);

% ��
fc = 288;
Nf_ = round(abs(3 * fs/(fn - fc)));
Nf_ = Nf_ + ~mod(Nf_,2); Nf_
FR = [0 0.25 0.5 0.75 0 -0.75 -0.5 -0.25];
FR = 1i.*FR;
figure('Name','Task 2. ��');
lab_4_calc(FR, N, Nf);

% ��
fc = 72;
Nf_ = round(abs(3 * fs/(fc)));
Nf_ = Nf_ + ~mod(Nf_,2); Nf_
FR = [0 1 1 1 0 -1 -1 -1];
FR = 1i.*FR;
figure('Name','Task 2. ��');
lab_4_calc(FR, N, Nf);