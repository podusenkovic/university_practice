function h_hemming = lab_4_calc(FR,N,Nf)
    g = 0.54 + 0.46 * cos(2 * pi * ((0:Nf - 1)/(Nf - 1) - 0.5));
    h = ifft(FR);
    h_shift = [h((N - floor(Nf/2) + 1):N), h(1:ceil(Nf / 2))];
    FR_h_s = abs(fft([h_shift, zeros(1,N - Nf)]));
    h_hemming = h_shift.*g;
    FFT_h_hemming = fft([h_hemming, zeros(1,N - Nf)]);
    FR_h_hemming = abs(FFT_h_hemming);
    phi = unwrap(angle(FFT_h_hemming));

    subplot(4,2,1); plot((0:length(h) - 1),        real(h)),         grid, title('h');
    subplot(4,2,3); plot((0:length(h_shift) - 1),  real(h_shift)),   grid, title('h_s_h_i_f_t');
    subplot(4,2,5); plot((0:length(h_hemming) - 1),real(h_hemming)), grid, title('h_H_e_m');

    subplot(4,2,2); plot((0:length(FR)-1),           real(FR)),     grid, title('FreqResp'), hold on;
                    plot((0:length(FR)-1),           imag(FR));
    subplot(4,2,4); plot((0:length(FR_h_s)-1),       FR_h_s),       grid, title('FreqResp_s_h_i_f_t_e_d');
    subplot(4,2,6); plot((0:length(FR_h_hemming)-1), FR_h_hemming), grid, title('FreqResp_H_e_m');
    subplot(4,2,8); plot((0:length(phi)-1),          phi),          grid, title('phi_H_e_m');
end