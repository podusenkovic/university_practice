% function result = Wfun_(dataBin,r)
%     if(r > length(dataBin))
%         readyData = [zeros(1,r-length(dataBin)), dataBin];
%     else
%         readyData = dataBin(1:r);
%     end
%     len = length(readyData);
%     t_regs = zeros(2^(r - 2));
%     result = readyData(1);
%     for i = 2:r
%         if(readyData(i)==0)
%             result = [result, result];
%         else 
%             result = [result, -(result - 1)];
%         end
%     end    
% end

function Sg = Wfun(data, r)
    data = de2bi(data,r);
    data = data(end:-1:1);
    l = 2^(r-1);
    T = zeros(1,2^(r-2));
    
    Sg = zeros(1,l);
    T(1) = data(1);
    Sg(1) = data(1);
    
    i = 1;
    for z = 1:(r-1)
        while i < 2^z
            Mux = xor(T(2^(z-1)),data(z+1));
            T = [Mux T(1:end-1)];
            i = i + 1;
            Sg(i) = Mux;
        end
    end
    for i = 1:l
        if Sg(i) == 0
            Sg(i) = 1;
        else
            Sg(i) = -1;
        end
    end
end
