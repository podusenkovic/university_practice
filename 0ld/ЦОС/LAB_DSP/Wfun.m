function result = Wfun(dataBin,r)
    if(r > length(dataBin))
        readyData = [zeros(1,r-length(dataBin)), dataBin];
    else
        readyData = dataBin(1:r);
    end
    len = length(readyData);
    t_regs = zeros(2^(r - 2));
    %result = readyData(1);
    %for i = 2:r
    %    if(readyData(i)==0)
    %        result = [result, result];
    %    else 
    %        result = [result, -(result - 1)];
    %    end
    %end    
    for n = 0:(2^(len - 1) - 1)
        if (n == 0) 
            multiplexor = readyData(1);
        else
            power_2_less = length(de2bi(n)) - 1;
            reg_num = 2^(power_2_less);
            multiplexor = mod(t_regs(reg_num) + readyData(power_2_less + 1 + 1), 2);
        end
        t_regs = circshift(t_regs', 1)';
        t_regs(1) = multiplexor;
        result(n + 1) = multiplexor;
    end
    result = (result * -2) + 1;
end
