clear, clc, close all
var = 12;
comp = 6;

f1 = var; f2 = comp;
phase1 = 3*pi/4; phase2 = 2*pi/var;
fs = 64; 
Tp = 1; T = 1/fs;
U = 1; 
AmpNoise = 2;
fftk = 1024;
n = 0:fs-1;
%-------------------1----------------------
%������� f1 � f2, ����a
f1 = U*cos(2*pi*f1*n*T + phase1); 
f2 = U*cos(2*pi*f2*n*T + phase2);
f = f1 + f2;

%�� �������
figure
subplot(3,1,1);
plot(0:T:1-T,f1)
hold on, grid on;
title('f1');
subplot(3,1,2);
plot(0:T:1-T,f2)
hold on, grid on;
title('f2');
subplot(3,1,3);
plot(0:T:1-T,f)
hold on, grid on;
title('f1 + f2');

%�������� ��� ��� f1 + f2 � �������� ����������� � ������� �������
S64 = fft(f,fs);
AmpS64 = abs(S64);
PhaseS64 = atan((imag(S64) + 0.000001)./(real(S64) + 0.0001));

% ������� ���, ������������, �������� �������� 
% ��� ������� ������������� 64
figure
%subplot(3,1,1);
%//plot(n,abs(S64))
%hold on, grid on;
%/title('abs(S64)');
subplot(2,1,1);
plot(n,AmpS64)
hold on, grid on;
title('real(S64)- Amplitude');
subplot(2,1,2);
plot(n,PhaseS64)
hold on, grid on;
title('imag(S64) - Phase');

%�� �� �����, �� � �����
Noise = AmpNoise - 2 * AmpNoise * rand(1, fs);
Fnoise = f + Noise;
Snoise64 = fft(Fnoise,fs);
AmpSnoise64 = abs(Snoise64);
PhaseSnoise64 = atan((imag(Snoise64) + 0.000001)./(real(Snoise64) + 0.0001));

figure
%subplot(3,1,1);
%plot(n,abs(Snoise64))
%hold on, grid on;
title('abs(Snoise64)');
subplot(2,1,1);
plot(n,AmpSnoise64)
hold on, grid on;
title('real(Snoise64)- Amplitude');
subplot(2,1,2);
plot(n,PhaseSnoise64)
hold on, grid on;
title('imag(Snoise64) - Phase');
%------------------------------------------
%-------------------2----------------------
Ni = comp+10;
if(mod(Ni,2) == 0)
    Ni = Ni + 1;
end
varBin = de2bi(var);
varBin = varBin(length(varBin):-1:1);
f0 = (1 + bi2de(varBin(4:-1:3)));
phase0 = (1 + bi2de(varBin(2:-1:1))) * pi / (1 + bi2de(varBin(4:-1:3)))
N = 128;
T = 1/(3*f0*(1+bi2de(varBin(3:-1:1))));
if(varBin(1))
    znak = -1;
else
    znak = 1;
end
fs = 1/T;
shift = znak*bi2de(varBin(4:-1:2));

t = (-1/2):(1/Ni):(1/2);    
s = cos(f0.*t + phase0) + 1i.*sin(f0.*t + phase0);
Re = [real(s), zeros(1, N - length(s))];
Im = [imag(s), zeros(1, N - length(s))];
Re = [Re(9:(Ni)) zeros(1, (N - Ni)) Re((1):8)];
Im = [Im(9:(Ni)) zeros(1, (N - Ni)) Im((1):8)];
%ReS = [Re(1:(Ni - shift)) zeros(1, (N - Ni)) Re((Ni - shift):Ni)];
%ImS = [Im(1:(Ni - shift)) zeros(1, (N - Ni)) Im((Ni - shift):Ni)];

%s_shifted = cos(f0.*(t + shift/Ni) + phase0) + 1i.*sin(f0.*(t + shift/Ni) + phase0);
%ReS = [real(s_shifted), zeros(1, N - length(s_shifted))];
%ReS = circshift(ReS, shift);
%ImS = [imag(s_shifted), zeros(1, N - length(s_shifted))];
%ImS = circshift(ImS, shift);
ReS = circshift(Re, shift);
ImS = circshift(Im, shift);

DPF = fft(Re + 1i.*Im, N); %��������� ��� ����������� �������

Amp = abs(DPF);
Phase = atan2(imag(DPF), real(DPF)); 

figure;
subplot(3,2,1), plot(t, real(s)), title('������c');
grid on, hold on;
subplot(3,2,2), plot(t, imag(s)), title('�����');
grid on, hold on;
subplot(3,2,3), plot(Re,'b'),title('������� � ������');
grid on, hold on;
subplot(3,2,4), plot(Im,'b'),title('����� � ������');
grid on, hold on;
subplot(3,2,5), plot(ReS,'b'),title('������� �� �������');      
grid on, hold on;
subplot(3,2,6), plot(ImS,'b'),title('����� �� �������');
grid on, hold on;
figure;
grid on, hold on;
subplot(2,1,1), plot(Amp),title('���������');
grid on, hold on;
subplot(2,1,2), plot(Phase),title('����');
grid on, hold on;

%������
A = Ni; % �	�������� �������� ��������� ������������ ������� (A);
