function result = my_sf(M1, M2) 
    len_m1 = length(M1);
    len_m2 = length(M2);
    %if (len_m2 > len_m1)
    %    M1 = [M1, zeros(1, (len_m2 - len_m1))];
    %    len_m1 = len_m2;
    %end
    %if (len_m1 > len_m2)
    %    M2 = [zeros(1, (len_m1 - len_m2)), M2];
    %    len_m2 = len_m1;
    %end
    M1inv = M1(len_m1:-1:1);
    t = zeros(1, len_m1);
    len_korrel = len_m2 * 2 - 1;
    result = zeros(1, len_korrel);
    for j = 1:(len_korrel)  
        for i = (len_m1):-1:2
            t(i) = t(i - 1);
            if j > len_m2 
                t(1) = 0;
            else
                t(1) = M2(j);
            end
        end
        result(j) = sum(M1inv.*t);
    end
end