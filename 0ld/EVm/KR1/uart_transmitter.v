module uart_transmitter(data, clk, reset, data_ready,	busy, TX);

parameter parity_bit = 1'b1;		// 1/0 have/haven't parity bit
parameter data_length = 4'b1000;	// 8 or 9 bits
parameter inversed = 1'b1; 		// 1/0 inversed/normal output
parameter stop_bits = 1'b1; 		// 0 - 1 stop bit, 1 - 2 stop bits
parameter straight = 1'b1;			// 1/0 straight/reversed bits queue

parameter length = parity_bit + 1/*start*/ + data_length + stop_bits + 1 + 1;

input wire [data_length - 1:0] data; // input data to transmit
input wire clk;							 // input clk
input wire reset;							 // reset signal
input wire data_ready;					 // signal that data is ready to transmit
output reg busy;							 // output signal, its 1 when transmitter transmitting	
output reg TX;								 // output data signal 

reg [1:0] count; // creating new clock, that 4th times slower than clk

reg [3:0] state_counter = 0; // counter that shows current state of transmitting

always @(posedge clk)
	if (reset)
		count <= 1'b0;
	else count <= count + 1'b1;
	
assign slower_clk = ~|count;

//parameter length = parity_bit + 1/*start*/ + data_length + stop_bits + 1 + 1;

//wire data_update[length - 1:0] = {1, data, {parity_bit{^data}}, 1, {stop_bits{1}}};

always @(posedge clk)
	case (state)
		4'b0000: tx <= 1'b1; // keep 1 when nothing happens
		4'b0001: tx <= 1'b0; // start bit
		4'b0010: tx <= data[0];
		4'b0011: tx <= data[1];
		4'b0100: tx <= data[2];
		4'b0101: tx <= data[3];
		4'b0110: tx <= data[4];
		4'b0111: tx <= data[5];
		4'b1000: tx <= data[6];
		4'b1001: tx <= data[7];
		4'b1010: tx <= data[8];
		4'b1011: tx <= ^data; // parity_bit
		4'b1100: tx <= 1'b1; // stop bit(bits)
		default: tx <= 1'b1; 
	endcase

assign busy = ~(state_counter == 4'b0000)
	
always @(posedge slower_clk)
	if (reset) begin
		busy <= 1'b0;
		state_counter <= 4'b0000;
	end
	else if (!busy & data_ready) begin 
		state_counter <= state_counter + 1'b1;
		if (state_counter == 4'b1010 && data_length == 4'1000)
			state_counter <= state_counter + 1'b1;
		if (state_counter == 4'b1011 && parity_bit == 0)
			state_counter <= state_counter + 1'b1;
	end
	else if (state > 4'b1100)
		state_counter <= 4'b0000;

endmodule 