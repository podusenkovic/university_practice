`timescale 1ns/1ps

module miriscv_ram
  #(
    parameter size       = 16384,
    parameter init_file  = ""
    ) 
   (
    input          clk_i,

    input          ena_i,
    input          wea_i,
    input  [31:0]  aa_i,
    input  [3:0]   bwea_i,
    input  [31:0]  da_i,
    output [31:0]  qa_o,

    input          enb_i,
    input          web_i,
    input  [31:0]  ab_i,
    input  [3:0]   bweb_i,
    input  [31:0]  db_i,
    output [31:0]  qb_o
    );

   reg [31:0]         mem [0:size/4-1];
   reg [31:0]         qa_int, qb_int;
    

   integer         f, addr;
   reg[31:0]       data;
   reg [8*20-1:0]  cmd;
   
  integer ram_index; 
   
  initial begin
    for (ram_index = 0; ram_index < g_size/4-1; ram_index = ram_index + 1)
      mem[ram_index] = {32{1'b0}};
    if(init_file != "")    
      $readmemh(init_file, mem);
    end
  end

  always@(posedge clk_i) begin
    if(ena_i)
    begin
      qa_int <= mem[(aa_i / 4) % size];

      if(wea_i && bwea_i[0])
        mem [aa_i[15:2]] [7:0] <= da_i[7:0];
      if(wea_i && bwea_i[1])
        mem [aa_i[15:2]] [15:8] <= da_i[15:8];
      if(wea_i && bwea_i[2])
        mem [aa_i[15:2]] [23:16] <= da_i[23:16];
      if(wea_i && bwea_i[3])
        mem [aa_i[15:2]] [31:24] <= da_i[31:24];
    end
  end

  always@(posedge clk_i) begin
    if(enb_i)
    begin
      qb_int <= mem[(ab_i / 4) % size];

      if(web_i && bweb_i[0])
        mem [ab_i[15:2]] [7:0] <= db_i[7:0];
      if(web_i && bweb_i[1])
        mem [ab_i[15:2]] [15:8] <= db_i[15:8];
      if(web_i && bweb_i[2])
        mem [ab_i[15:2]] [23:16] <= db_i[23:16];
      if(web_i && bweb_i[3])
        mem [ab_i[15:2]] [31:24] <= db_i[31:24];
    end       
  end
   
  assign qa_o = qa_int; 
  assign qb_o = qb_int;
   

endmodule