
test_mirv_embedded.elf:     file format elf32-littleriscv


Disassembly of section .text:

00000000 <_ftext>:
   0:	00c0006f          	j	c <_entry>
   4:	0000                	unimp
	...

00000008 <_exception_entry>:
   8:	1b40006f          	j	1bc <trap_entry>

0000000c <_entry>:
   c:	00001197          	auipc	gp,0x1
  10:	c4418193          	addi	gp,gp,-956 # c50 <_gp>
  14:	00003117          	auipc	sp,0x3
  18:	fec10113          	addi	sp,sp,-20 # 3000 <_fstack>
  1c:	00000297          	auipc	t0,0x0
  20:	43428293          	addi	t0,t0,1076 # 450 <_bss_start>
  24:	00000317          	auipc	t1,0x0
  28:	42c30313          	addi	t1,t1,1068 # 450 <_bss_start>
  2c:	0002a023          	sw	zero,0(t0)
  30:	00428293          	addi	t0,t0,4
  34:	fe62ece3          	bltu	t0,t1,2c <_entry+0x20>
  38:	004000ef          	jal	ra,3c <main>

0000003c <main>:
  3c:	fe010113          	addi	sp,sp,-32
  40:	00112e23          	sw	ra,28(sp)
  44:	00812c23          	sw	s0,24(sp)
  48:	00912a23          	sw	s1,20(sp)
  4c:	02010413          	addi	s0,sp,32
  50:	800007b7          	lui	a5,0x80000
  54:	10078793          	addi	a5,a5,256 # 80000100 <_fstack+0x7fffd100>
  58:	00100713          	li	a4,1
  5c:	00e7a023          	sw	a4,0(a5)
  60:	800007b7          	lui	a5,0x80000
  64:	20078793          	addi	a5,a5,512 # 80000200 <_fstack+0x7fffd200>
  68:	00200713          	li	a4,2
  6c:	00e7a023          	sw	a4,0(a5)
  70:	800007b7          	lui	a5,0x80000
  74:	30078793          	addi	a5,a5,768 # 80000300 <_fstack+0x7fffd300>
  78:	00300713          	li	a4,3
  7c:	00e7a023          	sw	a4,0(a5)
  80:	800007b7          	lui	a5,0x80000
  84:	40078793          	addi	a5,a5,1024 # 80000400 <_fstack+0x7fffd400>
  88:	00400713          	li	a4,4
  8c:	00e7a023          	sw	a4,0(a5)
  90:	800007b7          	lui	a5,0x80000
  94:	50078793          	addi	a5,a5,1280 # 80000500 <_fstack+0x7fffd500>
  98:	00f00713          	li	a4,15
  9c:	00e7a023          	sw	a4,0(a5)
  a0:	800007b7          	lui	a5,0x80000
  a4:	60078793          	addi	a5,a5,1536 # 80000600 <_fstack+0x7fffd600>
  a8:	04f00713          	li	a4,79
  ac:	00e7a023          	sw	a4,0(a5)
  b0:	800017b7          	lui	a5,0x80001
  b4:	80078793          	addi	a5,a5,-2048 # 80000800 <_fstack+0x7fffd800>
  b8:	0007a783          	lw	a5,0(a5)
  bc:	fef42623          	sw	a5,-20(s0)
  c0:	800007b7          	lui	a5,0x80000
  c4:	70078793          	addi	a5,a5,1792 # 80000700 <_fstack+0x7fffd700>
  c8:	0007a783          	lw	a5,0(a5)
  cc:	fef42423          	sw	a5,-24(s0)
  d0:	800007b7          	lui	a5,0x80000
  d4:	50078793          	addi	a5,a5,1280 # 80000500 <_fstack+0x7fffd500>
  d8:	0007a683          	lw	a3,0(a5)
  dc:	800007b7          	lui	a5,0x80000
  e0:	50078793          	addi	a5,a5,1280 # 80000500 <_fstack+0x7fffd500>
  e4:	fe842703          	lw	a4,-24(s0)
  e8:	00e68733          	add	a4,a3,a4
  ec:	00e7a023          	sw	a4,0(a5)
  f0:	800007b7          	lui	a5,0x80000
  f4:	60078793          	addi	a5,a5,1536 # 80000600 <_fstack+0x7fffd600>
  f8:	0007a683          	lw	a3,0(a5)
  fc:	800007b7          	lui	a5,0x80000
 100:	60078793          	addi	a5,a5,1536 # 80000600 <_fstack+0x7fffd600>
 104:	fe842703          	lw	a4,-24(s0)
 108:	00e68733          	add	a4,a3,a4
 10c:	00e7a023          	sw	a4,0(a5)
 110:	800007b7          	lui	a5,0x80000
 114:	10078493          	addi	s1,a5,256 # 80000100 <_fstack+0x7fffd100>
 118:	fec42783          	lw	a5,-20(s0)
 11c:	00a00593          	li	a1,10
 120:	00078513          	mv	a0,a5
 124:	2c0000ef          	jal	ra,3e4 <__umodsi3>
 128:	00050793          	mv	a5,a0
 12c:	00f4a023          	sw	a5,0(s1)
 130:	fec42783          	lw	a5,-20(s0)
 134:	06400593          	li	a1,100
 138:	00078513          	mv	a0,a5
 13c:	2a8000ef          	jal	ra,3e4 <__umodsi3>
 140:	00050793          	mv	a5,a0
 144:	00078713          	mv	a4,a5
 148:	800007b7          	lui	a5,0x80000
 14c:	20078493          	addi	s1,a5,512 # 80000200 <_fstack+0x7fffd200>
 150:	00a00593          	li	a1,10
 154:	00070513          	mv	a0,a4
 158:	244000ef          	jal	ra,39c <__udivsi3>
 15c:	00050793          	mv	a5,a0
 160:	00f4a023          	sw	a5,0(s1)
 164:	fec42783          	lw	a5,-20(s0)
 168:	3e800593          	li	a1,1000
 16c:	00078513          	mv	a0,a5
 170:	274000ef          	jal	ra,3e4 <__umodsi3>
 174:	00050793          	mv	a5,a0
 178:	00078713          	mv	a4,a5
 17c:	800007b7          	lui	a5,0x80000
 180:	30078493          	addi	s1,a5,768 # 80000300 <_fstack+0x7fffd300>
 184:	06400593          	li	a1,100
 188:	00070513          	mv	a0,a4
 18c:	210000ef          	jal	ra,39c <__udivsi3>
 190:	00050793          	mv	a5,a0
 194:	00f4a023          	sw	a5,0(s1)
 198:	800007b7          	lui	a5,0x80000
 19c:	40078493          	addi	s1,a5,1024 # 80000400 <_fstack+0x7fffd400>
 1a0:	fec42783          	lw	a5,-20(s0)
 1a4:	3e800593          	li	a1,1000
 1a8:	00078513          	mv	a0,a5
 1ac:	1f0000ef          	jal	ra,39c <__udivsi3>
 1b0:	00050793          	mv	a5,a0
 1b4:	00f4a023          	sw	a5,0(s1)
 1b8:	ef9ff06f          	j	b0 <main+0x74>

000001bc <trap_entry>:
 1bc:	34011173          	csrrw	sp,mscratch,sp
 1c0:	ec010113          	addi	sp,sp,-320
 1c4:	00112223          	sw	ra,4(sp)
 1c8:	00312623          	sw	gp,12(sp)
 1cc:	00412823          	sw	tp,16(sp)
 1d0:	00512a23          	sw	t0,20(sp)
 1d4:	00612c23          	sw	t1,24(sp)
 1d8:	00712e23          	sw	t2,28(sp)
 1dc:	02812023          	sw	s0,32(sp)
 1e0:	02912223          	sw	s1,36(sp)
 1e4:	02a12423          	sw	a0,40(sp)
 1e8:	02b12623          	sw	a1,44(sp)
 1ec:	02c12823          	sw	a2,48(sp)
 1f0:	02d12a23          	sw	a3,52(sp)
 1f4:	02e12c23          	sw	a4,56(sp)
 1f8:	02f12e23          	sw	a5,60(sp)
 1fc:	05012023          	sw	a6,64(sp)
 200:	05112223          	sw	a7,68(sp)
 204:	05212423          	sw	s2,72(sp)
 208:	05312623          	sw	s3,76(sp)
 20c:	05412823          	sw	s4,80(sp)
 210:	05512a23          	sw	s5,84(sp)
 214:	05612c23          	sw	s6,88(sp)
 218:	05712e23          	sw	s7,92(sp)
 21c:	07812023          	sw	s8,96(sp)
 220:	07912223          	sw	s9,100(sp)
 224:	07a12423          	sw	s10,104(sp)
 228:	07b12623          	sw	s11,108(sp)
 22c:	07c12823          	sw	t3,112(sp)
 230:	07d12a23          	sw	t4,116(sp)
 234:	07e12c23          	sw	t5,120(sp)
 238:	07f12e23          	sw	t6,124(sp)
 23c:	340022f3          	csrr	t0,mscratch
 240:	30002473          	csrr	s0,mstatus
 244:	34102373          	csrr	t1,mepc
 248:	343023f3          	csrr	t2,mbadaddr
 24c:	34202e73          	csrr	t3,mcause
 250:	00512423          	sw	t0,8(sp)
 254:	08812023          	sw	s0,128(sp)
 258:	08612223          	sw	t1,132(sp)
 25c:	08712423          	sw	t2,136(sp)
 260:	09c12623          	sw	t3,140(sp)
 264:	fff00293          	li	t0,-1
 268:	08512823          	sw	t0,144(sp)
 26c:	00010513          	mv	a0,sp
 270:	00000297          	auipc	t0,0x0
 274:	02028293          	addi	t0,t0,32 # 290 <jump_table>
 278:	002e1e13          	slli	t3,t3,0x2
 27c:	01c282b3          	add	t0,t0,t3
 280:	0002a283          	lw	t0,0(t0)
 284:	00000097          	auipc	ra,0x0
 288:	05408093          	addi	ra,ra,84 # 2d8 <jump_table_return>
 28c:	00028067          	jr	t0

00000290 <jump_table>:
 290:	0370                	addi	a2,sp,396
 292:	0000                	unimp
 294:	0370                	addi	a2,sp,396
 296:	0000                	unimp
 298:	0370                	addi	a2,sp,396
 29a:	0000                	unimp
 29c:	0370                	addi	a2,sp,396
 29e:	0000                	unimp
 2a0:	0370                	addi	a2,sp,396
 2a2:	0000                	unimp
 2a4:	0370                	addi	a2,sp,396
 2a6:	0000                	unimp
 2a8:	0370                	addi	a2,sp,396
 2aa:	0000                	unimp
 2ac:	0370                	addi	a2,sp,396
 2ae:	0000                	unimp
 2b0:	0370                	addi	a2,sp,396
 2b2:	0000                	unimp
 2b4:	0370                	addi	a2,sp,396
 2b6:	0000                	unimp
 2b8:	0374                	addi	a3,sp,396
 2ba:	0000                	unimp
 2bc:	0378                	addi	a4,sp,396
 2be:	0000                	unimp
 2c0:	037c                	addi	a5,sp,396
 2c2:	0000                	unimp
 2c4:	0380                	addi	s0,sp,448
 2c6:	0000                	unimp
 2c8:	0384                	addi	s1,sp,448
 2ca:	0000                	unimp
 2cc:	0388                	addi	a0,sp,448
 2ce:	0000                	unimp
 2d0:	038c                	addi	a1,sp,448
 2d2:	0000                	unimp
 2d4:	0390                	addi	a2,sp,448
	...

000002d8 <jump_table_return>:
 2d8:	00010513          	mv	a0,sp
 2dc:	08052303          	lw	t1,128(a0)
 2e0:	08452383          	lw	t2,132(a0)
 2e4:	14010113          	addi	sp,sp,320
 2e8:	34011073          	csrw	mscratch,sp
 2ec:	34139073          	csrw	mepc,t2
 2f0:	00452083          	lw	ra,4(a0)
 2f4:	00852103          	lw	sp,8(a0)
 2f8:	00c52183          	lw	gp,12(a0)
 2fc:	01052203          	lw	tp,16(a0)
 300:	01452283          	lw	t0,20(a0)
 304:	01852303          	lw	t1,24(a0)
 308:	01c52383          	lw	t2,28(a0)
 30c:	02052403          	lw	s0,32(a0)
 310:	02452483          	lw	s1,36(a0)
 314:	02c52583          	lw	a1,44(a0)
 318:	03052603          	lw	a2,48(a0)
 31c:	03452683          	lw	a3,52(a0)
 320:	03852703          	lw	a4,56(a0)
 324:	03c52783          	lw	a5,60(a0)
 328:	04052803          	lw	a6,64(a0)
 32c:	04452883          	lw	a7,68(a0)
 330:	04852903          	lw	s2,72(a0)
 334:	04c52983          	lw	s3,76(a0)
 338:	05052a03          	lw	s4,80(a0)
 33c:	05452a83          	lw	s5,84(a0)
 340:	05852b03          	lw	s6,88(a0)
 344:	05c52b83          	lw	s7,92(a0)
 348:	06052c03          	lw	s8,96(a0)
 34c:	06452c83          	lw	s9,100(a0)
 350:	06852d03          	lw	s10,104(a0)
 354:	06c52d83          	lw	s11,108(a0)
 358:	07052e03          	lw	t3,112(a0)
 35c:	07452e83          	lw	t4,116(a0)
 360:	07852f03          	lw	t5,120(a0)
 364:	07c52f83          	lw	t6,124(a0)
 368:	02852503          	lw	a0,40(a0)
 36c:	30200073          	mret

00000370 <undefined_handler>:
 370:	0000006f          	j	370 <undefined_handler>

00000374 <irq_0_handler>:
 374:	0000006f          	j	374 <irq_0_handler>

00000378 <irq_1_handler>:
 378:	0000006f          	j	378 <irq_1_handler>

0000037c <irq_2_handler>:
 37c:	0000006f          	j	37c <irq_2_handler>

00000380 <irq_3_handler>:
 380:	0000006f          	j	380 <irq_3_handler>

00000384 <irq_4_handler>:
 384:	0000006f          	j	384 <irq_4_handler>

00000388 <irq_5_handler>:
 388:	0000006f          	j	388 <irq_5_handler>

0000038c <irq_6_handler>:
 38c:	0000006f          	j	38c <irq_6_handler>

00000390 <irq_7_handler>:
 390:	0000006f          	j	390 <irq_7_handler>

00000394 <__divsi3>:
 394:	06054063          	bltz	a0,3f4 <__umodsi3+0x10>
 398:	0605c663          	bltz	a1,404 <__umodsi3+0x20>

0000039c <__udivsi3>:
 39c:	00058613          	mv	a2,a1
 3a0:	00050593          	mv	a1,a0
 3a4:	fff00513          	li	a0,-1
 3a8:	02060c63          	beqz	a2,3e0 <__udivsi3+0x44>
 3ac:	00100693          	li	a3,1
 3b0:	00b67a63          	bleu	a1,a2,3c4 <__udivsi3+0x28>
 3b4:	00c05863          	blez	a2,3c4 <__udivsi3+0x28>
 3b8:	00161613          	slli	a2,a2,0x1
 3bc:	00169693          	slli	a3,a3,0x1
 3c0:	feb66ae3          	bltu	a2,a1,3b4 <__udivsi3+0x18>
 3c4:	00000513          	li	a0,0
 3c8:	00c5e663          	bltu	a1,a2,3d4 <__udivsi3+0x38>
 3cc:	40c585b3          	sub	a1,a1,a2
 3d0:	00d56533          	or	a0,a0,a3
 3d4:	0016d693          	srli	a3,a3,0x1
 3d8:	00165613          	srli	a2,a2,0x1
 3dc:	fe0696e3          	bnez	a3,3c8 <__udivsi3+0x2c>
 3e0:	00008067          	ret

000003e4 <__umodsi3>:
 3e4:	00008293          	mv	t0,ra
 3e8:	fb5ff0ef          	jal	ra,39c <__udivsi3>
 3ec:	00058513          	mv	a0,a1
 3f0:	00028067          	jr	t0
 3f4:	40a00533          	neg	a0,a0
 3f8:	0005d863          	bgez	a1,408 <__umodsi3+0x24>
 3fc:	40b005b3          	neg	a1,a1
 400:	f9dff06f          	j	39c <__udivsi3>
 404:	40b005b3          	neg	a1,a1
 408:	00008293          	mv	t0,ra
 40c:	f91ff0ef          	jal	ra,39c <__udivsi3>
 410:	40a00533          	neg	a0,a0
 414:	00028067          	jr	t0

00000418 <__modsi3>:
 418:	00008293          	mv	t0,ra
 41c:	0005ca63          	bltz	a1,430 <__modsi3+0x18>
 420:	00054c63          	bltz	a0,438 <__modsi3+0x20>
 424:	f79ff0ef          	jal	ra,39c <__udivsi3>
 428:	00058513          	mv	a0,a1
 42c:	00028067          	jr	t0
 430:	40b005b3          	neg	a1,a1
 434:	fe0558e3          	bgez	a0,424 <__modsi3+0xc>
 438:	40a00533          	neg	a0,a0
 43c:	f61ff0ef          	jal	ra,39c <__udivsi3>
 440:	40b00533          	neg	a0,a1
 444:	00028067          	jr	t0

Disassembly of section .comment:

00000000 <.comment>:
   0:	3a434347          	fmsub.d	ft6,ft6,ft4,ft7,rmm
   4:	2820                	fld	fs0,80(s0)
   6:	29554e47          	fmsub.s	ft8,fa0,fs5,ft5,rmm
   a:	3720                	fld	fs0,104(a4)
   c:	312e                	fld	ft2,232(sp)
   e:	302e                	fld	ft0,232(sp)
	...

Disassembly of section .debug_line:

00000000 <.debug_line>:
   0:	0172                	slli	sp,sp,0x1c
   2:	0000                	unimp
   4:	0002                	0x2
   6:	00000053          	fadd.s	ft0,ft0,ft0,rne
   a:	0101                	addi	sp,sp,0
   c:	000d0efb          	0xd0efb
  10:	0101                	addi	sp,sp,0
  12:	0101                	addi	sp,sp,0
  14:	0000                	unimp
  16:	0100                	addi	s0,sp,128
  18:	0000                	unimp
  1a:	2f01                	jal	72a <_bss_start+0x2da>
  1c:	6e6d                	lui	t3,0x1b
  1e:	2f74                	fld	fa3,216(a4)
  20:	6972                	flw	fs2,28(sp)
  22:	2d766373          	csrrsi	t1,0x2d7,12
  26:	2d756e67          	0x2d756e67
  2a:	6f74                	flw	fa3,92(a4)
  2c:	68636c6f          	jal	s8,366b2 <_fstack+0x336b2>
  30:	6961                	lui	s2,0x18
  32:	2f6e                	fld	ft10,216(sp)
  34:	6972                	flw	fs2,28(sp)
  36:	2d766373          	csrrsi	t1,0x2d7,12
  3a:	2f636367          	0x2f636367
  3e:	696c                	flw	fa1,84(a0)
  40:	6762                	flw	fa4,24(sp)
  42:	632f6363          	bltu	t5,s2,668 <_bss_start+0x218>
  46:	69666e6f          	jal	t3,666dc <_fstack+0x636dc>
  4a:	69722f67          	0x69722f67
  4e:	00766373          	csrrsi	t1,0x7,12
  52:	6400                	flw	fs0,8(s0)
  54:	7669                	lui	a2,0xffffa
  56:	532e                	lw	t1,232(sp)
  58:	0100                	addi	s0,sp,128
  5a:	0000                	unimp
  5c:	0000                	unimp
  5e:	0205                	addi	tp,tp,1
  60:	0394                	addi	a3,sp,448
  62:	0000                	unimp
  64:	0100c503          	lbu	a0,16(ra)
  68:	04090103          	lb	sp,64(s2) # 18040 <_fstack+0x15040>
  6c:	0100                	addi	s0,sp,128
  6e:	04090503          	lb	a0,64(s2)
  72:	0100                	addi	s0,sp,128
  74:	04090103          	lb	sp,64(s2)
  78:	0100                	addi	s0,sp,128
  7a:	04090103          	lb	sp,64(s2)
  7e:	0100                	addi	s0,sp,128
  80:	04090103          	lb	sp,64(s2)
  84:	0100                	addi	s0,sp,128
  86:	04090103          	lb	sp,64(s2)
  8a:	0100                	addi	s0,sp,128
  8c:	04090103          	lb	sp,64(s2)
  90:	0100                	addi	s0,sp,128
  92:	04090203          	lb	tp,64(s2)
  96:	0100                	addi	s0,sp,128
  98:	04090103          	lb	sp,64(s2)
  9c:	0100                	addi	s0,sp,128
  9e:	04090103          	lb	sp,64(s2)
  a2:	0100                	addi	s0,sp,128
  a4:	04090103          	lb	sp,64(s2)
  a8:	0100                	addi	s0,sp,128
  aa:	04090203          	lb	tp,64(s2)
  ae:	0100                	addi	s0,sp,128
  b0:	04090203          	lb	tp,64(s2)
  b4:	0100                	addi	s0,sp,128
  b6:	04090103          	lb	sp,64(s2)
  ba:	0100                	addi	s0,sp,128
  bc:	04090103          	lb	sp,64(s2)
  c0:	0100                	addi	s0,sp,128
  c2:	04090203          	lb	tp,64(s2)
  c6:	0100                	addi	s0,sp,128
  c8:	04090103          	lb	sp,64(s2)
  cc:	0100                	addi	s0,sp,128
  ce:	04090103          	lb	sp,64(s2)
  d2:	0100                	addi	s0,sp,128
  d4:	04090203          	lb	tp,64(s2)
  d8:	0100                	addi	s0,sp,128
  da:	04090503          	lb	a0,64(s2)
  de:	0100                	addi	s0,sp,128
  e0:	04090103          	lb	sp,64(s2)
  e4:	0100                	addi	s0,sp,128
  e6:	04090103          	lb	sp,64(s2)
  ea:	0100                	addi	s0,sp,128
  ec:	04090103          	lb	sp,64(s2)
  f0:	0100                	addi	s0,sp,128
  f2:	04090403          	lb	s0,64(s2)
  f6:	0100                	addi	s0,sp,128
  f8:	04090103          	lb	sp,64(s2)
  fc:	0100                	addi	s0,sp,128
  fe:	04090103          	lb	sp,64(s2)
 102:	0100                	addi	s0,sp,128
 104:	04090103          	lb	sp,64(s2)
 108:	0100                	addi	s0,sp,128
 10a:	04090203          	lb	tp,64(s2)
 10e:	0100                	addi	s0,sp,128
 110:	04090203          	lb	tp,64(s2)
 114:	0100                	addi	s0,sp,128
 116:	04090103          	lb	sp,64(s2)
 11a:	0100                	addi	s0,sp,128
 11c:	04090103          	lb	sp,64(s2)
 120:	0100                	addi	s0,sp,128
 122:	04090103          	lb	sp,64(s2)
 126:	0100                	addi	s0,sp,128
 128:	04090403          	lb	s0,64(s2)
 12c:	0100                	addi	s0,sp,128
 12e:	04090103          	lb	sp,64(s2)
 132:	0100                	addi	s0,sp,128
 134:	04090103          	lb	sp,64(s2)
 138:	0100                	addi	s0,sp,128
 13a:	04090203          	lb	tp,64(s2)
 13e:	0100                	addi	s0,sp,128
 140:	04090103          	lb	sp,64(s2)
 144:	0100                	addi	s0,sp,128
 146:	04090103          	lb	sp,64(s2)
 14a:	0100                	addi	s0,sp,128
 14c:	04090203          	lb	tp,64(s2)
 150:	0100                	addi	s0,sp,128
 152:	04090103          	lb	sp,64(s2)
 156:	0100                	addi	s0,sp,128
 158:	04090203          	lb	tp,64(s2)
 15c:	0100                	addi	s0,sp,128
 15e:	04090103          	lb	sp,64(s2)
 162:	0100                	addi	s0,sp,128
 164:	04090103          	lb	sp,64(s2)
 168:	0100                	addi	s0,sp,128
 16a:	04090103          	lb	sp,64(s2)
 16e:	0100                	addi	s0,sp,128
 170:	0409                	addi	s0,s0,2
 172:	0000                	unimp
 174:	0101                	addi	sp,sp,0

Disassembly of section .debug_info:

00000000 <.debug_info>:
   0:	00ad                	addi	ra,ra,11
   2:	0000                	unimp
   4:	0002                	0x2
   6:	0000                	unimp
   8:	0000                	unimp
   a:	0104                	addi	s1,sp,128
   c:	0000                	unimp
   e:	0000                	unimp
  10:	0394                	addi	a3,sp,448
  12:	0000                	unimp
  14:	0448                	addi	a0,sp,516
  16:	0000                	unimp
  18:	746e6d2f          	0x746e6d2f
  1c:	7369722f          	0x7369722f
  20:	672d7663          	bleu	s2,s10,68c <_bss_start+0x23c>
  24:	756e                	flw	fa0,248(sp)
  26:	742d                	lui	s0,0xfffeb
  28:	636c6f6f          	jal	t5,c665e <_fstack+0xc365e>
  2c:	6168                	flw	fa0,68(a0)
  2e:	6e69                	lui	t3,0x1a
  30:	7369722f          	0x7369722f
  34:	672d7663          	bleu	s2,s10,6a0 <_bss_start+0x250>
  38:	6c2f6363          	bltu	t5,sp,6fe <_bss_start+0x2ae>
  3c:	6269                	lui	tp,0x1a
  3e:	2f636367          	0x2f636367
  42:	666e6f63          	bltu	t3,t1,6c0 <_bss_start+0x270>
  46:	6769                	lui	a4,0x1a
  48:	7369722f          	0x7369722f
  4c:	642f7663          	bleu	sp,t5,698 <_bss_start+0x248>
  50:	7669                	lui	a2,0xffffa
  52:	532e                	lw	t1,232(sp)
  54:	2f00                	fld	fs0,24(a4)
  56:	6e6d                	lui	t3,0x1b
  58:	2f74                	fld	fa3,216(a4)
  5a:	6972                	flw	fs2,28(sp)
  5c:	2d766373          	csrrsi	t1,0x2d7,12
  60:	2d756e67          	0x2d756e67
  64:	6f74                	flw	fa3,92(a4)
  66:	68636c6f          	jal	s8,366ec <_fstack+0x336ec>
  6a:	6961                	lui	s2,0x18
  6c:	2f6e                	fld	ft10,216(sp)
  6e:	7562                	flw	fa0,56(sp)
  70:	6c69                	lui	s8,0x1a
  72:	2d64                	fld	fs1,216(a0)
  74:	2d636367          	0x2d636367
  78:	656e                	flw	fa0,216(sp)
  7a:	62696c77          	0x62696c77
  7e:	732d                	lui	t1,0xfffeb
  80:	6174                	flw	fa3,68(a0)
  82:	2f326567          	0x2f326567
  86:	6972                	flw	fs2,28(sp)
  88:	33766373          	csrrsi	t1,mhpmevent23,12
  8c:	2d32                	fld	fs10,264(sp)
  8e:	6e75                	lui	t3,0x1d
  90:	776f6e6b          	0x776f6e6b
  94:	2d6e                	fld	fs10,216(sp)
  96:	6c65                	lui	s8,0x19
  98:	2f66                	fld	ft10,88(sp)
  9a:	696c                	flw	fa1,84(a0)
  9c:	6762                	flw	fa4,24(sp)
  9e:	47006363          	bltu	zero,a6,504 <_bss_start+0xb4>
  a2:	554e                	lw	a0,240(sp)
  a4:	4120                	lw	s0,64(a0)
  a6:	2e322053          	0x2e322053
  aa:	3832                	fld	fa6,296(sp)
  ac:	302e                	fld	ft0,232(sp)
  ae:	0100                	addi	s0,sp,128
  b0:	80 02 09 04 00          	Address 0x00000000000000b0 is out of bounds.


Disassembly of section .debug_abbrev:

00000000 <.debug_abbrev>:
   0:	1101                	addi	sp,sp,-32
   2:	1000                	addi	s0,sp,32
   4:	1106                	slli	sp,sp,0x21
   6:	1201                	addi	tp,tp,-32
   8:	0301                	addi	t1,t1,0
   a:	1b08                	addi	a0,sp,432
   c:	2508                	fld	fa0,8(a0)
   e:	1308                	addi	a0,sp,416
  10:	0005                	c.addi	zero,1
	...

Disassembly of section .debug_aranges:

00000000 <.debug_aranges>:
   0:	001c                	addi	a5,sp,0
   2:	0000                	unimp
   4:	0002                	0x2
   6:	0000                	unimp
   8:	0000                	unimp
   a:	0004                	addi	s1,sp,0
   c:	0000                	unimp
   e:	0000                	unimp
  10:	0394                	addi	a3,sp,448
  12:	0000                	unimp
  14:	00b4                	addi	a3,sp,72
	...
