 module miriscy_register_file
#(
  parameter DATA_WIDTH    = 32
)
(
  // Clock and Reset
  input                   clk_i,
  input                   rst_n_i,

  //Read port R1
  input  [4:0]             raddr_a_i,
  output [DATA_WIDTH-1:0]  rdata_a_o,

  //Read port R2
  input  [4:0]             raddr_b_i,
  output [DATA_WIDTH-1:0]  rdata_b_o,


  // Write port W1
  input  [4:0]              waddr_a_i,
  input  [DATA_WIDTH-1:0]   wdata_a_i,
  input                     we_a_i

);



endmodule
