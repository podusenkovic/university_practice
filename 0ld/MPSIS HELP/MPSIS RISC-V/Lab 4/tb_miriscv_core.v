`timescale 1ns / 1ps

//`include "miriscv_defines.v"

module tb_miriscv_core();

parameter TEST_VALUES     = 454;
parameter HALF_PERIOD_CLK = 10;


  reg         clk_i;
  reg         rst_n_i;

  wire [31:0] imem_addr_o;
  wire  [31:0] imem_data_i;

  wire [31:0] dmem_addr_o;
  wire [3:0]  dmem_we_o;
  wire  [31:0] dmem_data_i;
  wire [31:0] dmem_data_o;



miriscv_core DUT(

  .clk_i   (clk_i),
  .rst_n_i (rst_n_i),

  .imem_addr_o (imem_addr_o),
  .imem_data_i (imem_data_i),
  
  .dmem_addr_o (dmem_addr_o),
  .dmem_we_o (dmem_we_o),
  .dmem_data_i (dmem_data_i),
  .dmem_data_o (dmem_data_o)
  );


wire [99:0] output_vector = { imem_addr_o, //32
                              dmem_addr_o, // 32
                              dmem_we_o, // 4
                              dmem_data_o // 32
                            };



integer     i, err_count = 0;
integer     file_test_input = 0;
integer     file_test_output = 0;



reg [99:0] result_dump = 0;

reg [63:0] input_dump = 0;


assign dmem_data_i   = input_dump[31:0];
assign imem_data_i   = input_dump[63:32];

initial begin
    clk_i = 1'b0;
    forever #HALF_PERIOD_CLK clk_i = ~clk_i;
end

initial begin
    rst_n_i = 1'b1;
    #100;
    rst_n_i = 1'b0;
    #100;
    @(posedge clk_i);
    rst_n_i = 1'b1;
    
    file_test_input = $fopen("miriscv_core_input_vector.txt" , "r");
    file_test_output = $fopen("miriscv_core_output_vector.txt" , "r");
        
    if ( file_test_input == 0 || file_test_output == 0)
      begin
        $display("ERROR files has not opened");
        $stop;
    end
    
    for ( i = 0; i < TEST_VALUES; i = i + 1 )
          begin            
            $fscanf( file_test_input    , "%b\n", input_dump);
            $fscanf( file_test_output    , "%b\n", result_dump);
            
            @(posedge clk_i);
            if( (output_vector != result_dump)) begin
              $display("\nERROR instruction: %s", imem_data_i);
            
              $display("\nSignal\t\t\t\t\tShould be\tIs");
            
              $display("imem_addr_o\t\t\t", "%h\t\t\t", imem_addr_o, "%h", result_dump[99:68]);
              $display("dmem_addr_o\t\t\t", "%h\t\t\t", dmem_addr_o, "%h", result_dump[67:36]);
              $display("dmem_we_o\t\t\t", "%h\t\t\t", dmem_we_o, "%h", result_dump[35:32]);
              $display("dmem_data_o\t\t\t", "%h\t\t\t", dmem_data_o, "%h", result_dump[31:0]);
              $display( "======================================");
              err_count = err_count + 1'b1;
              end
          end
          
        $fclose( file_test_input );
        $fclose( file_test_output );
        
        $display("Test finished. Number of errors: %d", err_count);
        
        $stop;
end




endmodule
