
`define ALU_OP_WIDTH  6

`define ALU_ADD   6'b011000
`define ALU_SUB   6'b011001
`define ALU_ADDU  6'b011010
`define ALU_SUBU  6'b011011

`define ALU_XOR   6'b101111
`define ALU_OR    6'b101110
`define ALU_AND   6'b010101

// Shifts
`define ALU_SRA   6'b100100
`define ALU_SRL   6'b100101
`define ALU_SLL   6'b100111

// Bit counting
`define ALU_FF1   6'b110110
`define ALU_FL1   6'b110111
`define ALU_CNT   6'b110100
`define ALU_CLB   6'b110101

// Sign-/zero-extensions
`define ALU_EXTS  6'b111110
`define ALU_EXT   6'b111111

// Comparisons
`define ALU_LTS   6'b000000
`define ALU_LTU   6'b000001
`define ALU_LES   6'b000100
`define ALU_LEU   6'b000101
`define ALU_GTS   6'b001000
`define ALU_GTU   6'b001001
`define ALU_GES   6'b001010
`define ALU_GEU   6'b001011
`define ALU_EQ    6'b001100
`define ALU_NE    6'b001101

// Set Lower Than operations
`define ALU_SLTS  6'b000010
`define ALU_SLTU  6'b000011
`define ALU_SLETS 6'b000110
`define ALU_SLETU 6'b000111


