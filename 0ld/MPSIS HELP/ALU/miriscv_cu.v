`timescale 1ns / 1ps
`include "miriscv_defines.v"

module miriscv_cu(

    // instructions memory
  input [31:0]                   instr_rdata_i,
  output reg                     illegal_instr_o,

  // ALU signals
  input                          alu_comparison_result_i,

  output reg [`ALU_OP_WIDTH-1:0] alu_operator_o,
  output reg [1:0]               alu_mux_b_sel_o,

  // regfile
  output reg                     regfile_we_o,
  output reg [3:0]               regfile_mux_wdata_sel_o,

  // data memory
  output reg [3:0]               dmem_we_o,
  
  //Program counter
  output reg [1:0]               pc_mux_source_sel_o
  );


  always@(*) begin
		//-------------!!!default values!!!-------------
		pc_mux_source_sel_o <= `CU_PC_NEXT;
		dmem_we_o <= 1'b0;
		regfile_we_o <= 1'b0;
		alu_mux_b_sel_o <= 1'b0;
		regfile_mux_wdata_sel_o <= `CU_REGFILE_ALU;
		illegal_instr_o <= 1'b0;
		//----------------------------------------------
		//------------------START_R-TYPE----------------------
		case(instr_rdata_i[6:0])
		7'b0110011: begin			
			alu_mux_b_sel_o <= `CU_OP_B_REGB;
			regfile_mux_wdata_sel_o <= `CU_REGFILE_ALU;
			if(instr_rdata_i[31:25] == 7'b0000000)
				case(instr_rdata_i[14:12])
					3'b000: begin 						
						alu_operator_o <= `ALU_ADD;
						regfile_we_o <= 1'b1;
					end
					3'b001: begin 						
						alu_operator_o <= `ALU_SLL;
						regfile_we_o <= 1'b1;
					end
					3'b010: begin 
						alu_operator_o <= `ALU_SLTS;
						regfile_we_o <= 1'b1;
					end
					3'b011: begin 						
						alu_operator_o <= `ALU_SLTU;
						regfile_we_o <= 1'b1;
					end
					3'b100: begin 						
						alu_operator_o <= `ALU_XOR;
						regfile_we_o <= 1'b1;
					end
					3'b101: begin 
						alu_operator_o <= `ALU_SRL;
						regfile_we_o <= 1'b1;
					end
					3'b110: begin 						
						alu_operator_o <= `ALU_OR;
						regfile_we_o <= 1'b1;
					end
					3'b111: begin 						
						alu_operator_o <= `ALU_AND;
						regfile_we_o <= 1'b1;
					end
					default: illegal_instr_o <= 1;
				endcase
			else case (instr_rdata_i[14:12])
				3'b000: begin 					
					alu_operator_o <= `ALU_SUB;
					regfile_we_o <= 1'b1;
				end
				3'b101: begin 					
					alu_operator_o <= `ALU_SRA;
					regfile_we_o <= 1'b1;
				end
				default: illegal_instr_o <= 1;
			endcase 
		end
		//------------------END_R-TYPE----------------------
		//------------------START_I-TYPE----------------------
		7'b0010011: begin
			alu_mux_b_sel_o <= `CU_OP_B_IMM_I;
			regfile_mux_wdata_sel_o <= `CU_REGFILE_ALU;
			case(instr_rdata_i[14:12])
				3'b000: begin 					
					alu_operator_o <= `ALU_ADD;
					regfile_we_o <= 1'b1;
				end
				3'b001: begin 					
					alu_operator_o <= `ALU_SLL;
					regfile_we_o <= 1'b1;
				end
				3'b010: begin 
					alu_operator_o <= `ALU_SLTS;
					regfile_we_o <= 1'b1;
				end
				3'b011: begin 					
					alu_operator_o <= `ALU_SLTU;
					regfile_we_o <= 1'b1;
				end
				3'b100: begin 					
					alu_operator_o <= `ALU_XOR;
					regfile_we_o <= 1'b1;
				end
				3'b101: begin 					
					alu_operator_o <= (instr_rdata_i[31:25] == 'b0) ? `ALU_SRL : `ALU_SRA;
					regfile_we_o <= 1'b1;
				end
				3'b110: begin 					
					alu_operator_o <= `ALU_OR;
					regfile_we_o <= 1'b1;
				end
				3'b111: begin 					
					alu_operator_o <= `ALU_AND;
					regfile_we_o <= 1'b1;
				end
				default: illegal_instr_o <= 1;
			endcase
		end
		7'b0000011: begin
			alu_mux_b_sel_o <= `CU_OP_B_IMM_I;
			alu_operator_o <= `ALU_ADD;
			case(instr_rdata_i[14:12])
				3'b000: begin 					
					regfile_mux_wdata_sel_o <= `CU_REGFILE_DMEM_B;
					regfile_we_o <= 1'b1;
				end
				3'b001: begin 					
					regfile_mux_wdata_sel_o <= `CU_REGFILE_DMEM_H;
					regfile_we_o <= 1'b1;
				end
				3'b010: begin 					
					regfile_mux_wdata_sel_o <= `CU_REGFILE_DMEM_W;
					regfile_we_o <= 1'b1;
				end
				3'b100: begin 					
					regfile_mux_wdata_sel_o <= `CU_REGFILE_DMEM_BU;
					regfile_we_o <= 1'b1;
				end
				3'b101: begin 					
					regfile_mux_wdata_sel_o <= `CU_REGFILE_DMEM_HU;
					regfile_we_o <= 1'b1;
				end
				default: illegal_instr_o <= 1;
			endcase
		end
		//------------------END_I-TYPE----------------------
		//------------------START_S-TYPE----------------------
		7'b0100011: begin
			alu_mux_b_sel_o <= `CU_OP_B_IMM_S;
			alu_operator_o <= `ALU_ADD;
			case (instr_rdata_i[14:12])
				3'b000: begin 					
					dmem_we_o <= `CU_LDST_B;
				end
				3'b001: begin					
					dmem_we_o <= `CU_LDST_H;
				end
				3'b010: begin					
					dmem_we_o <= `CU_LDST_W;
				end
				default: illegal_instr_o <= 1;
			endcase
		end 
		//------------------END_S-TYPE----------------------
		//------------------START_B-TYPE----------------------
		7'b1100011: begin
			alu_mux_b_sel_o <= `CU_OP_B_REGB;
			case (instr_rdata_i[14:12])
				3'b000: begin 					
					alu_operator_o <= `ALU_EQ;
				end
				3'b001: begin					
					alu_operator_o <= `ALU_NE;
				end
				3'b100: begin 					
					alu_operator_o <= `ALU_LTS;
				end
				3'b101: begin					
					alu_operator_o <= `ALU_GES;
				end
				3'b110: begin					
					alu_operator_o <= `ALU_LTU;
				end
				3'b111: begin					
					alu_operator_o <= `ALU_GEU;
				end
				default: illegal_instr_o <= 1;
			endcase
			pc_mux_source_sel_o <= (alu_comparison_result_i) ? `CU_PC_PCIMM_B : `CU_PC_NEXT;
		end
		//------------------END_B-TYPE----------------------
		//------------------START_J/I-TYPE----------------------
		7'b1101111: begin
			alu_operator_o <= `ALU_SLTU;
			regfile_we_o <= 1'b1;
			regfile_mux_wdata_sel_o <= `CU_REGFILE_PCNEXT;
			pc_mux_source_sel_o <= `CU_PC_PCIMM_J;
		end
		7'b1100111: begin
			alu_operator_o <= `ALU_SLTU;
			regfile_we_o <= 1'b1;
			regfile_mux_wdata_sel_o <= `CU_REGFILE_PCNEXT;
			pc_mux_source_sel_o <= `CU_PC_REGFILE;
		end
		//------------------END_J/I-TYPE----------------------
		//------------------START_U-TYPE----------------------
		7'b0110111: begin
			alu_operator_o <= `ALU_SLTU;
			regfile_we_o <= 1'b1;
			regfile_mux_wdata_sel_o <= `CU_REGFILE_IMM;
		end
		7'b0010111: begin
			alu_operator_o <= `ALU_SLTU;
			regfile_we_o <= 1'b1;
			regfile_mux_wdata_sel_o <= `CU_REGFILE_PCIMM;
		end
		//------------------END_U-TYPE------------------------
		default: illegal_instr_o <= 1'b1;
		endcase
	end
	
  
  
  
endmodule 