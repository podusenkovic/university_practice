`define RESET_VECTOR 32'h00000000



`define ALU_OP_WIDTH  6

`define ALU_ADD   6'b011000
`define ALU_SUB   6'b011001

`define ALU_XOR   6'b101111
`define ALU_OR    6'b101110
`define ALU_AND   6'b010101

// Shifts
`define ALU_SRA   6'b100100
`define ALU_SRL   6'b100101
`define ALU_SLL   6'b100111

// Comparisons
`define ALU_LTS   6'b000000
`define ALU_LTU   6'b000001
`define ALU_GES   6'b001010
`define ALU_GEU   6'b001011
`define ALU_EQ    6'b001100
`define ALU_NE    6'b001101

// Set Less Than operations
`define ALU_SLTS  6'b000010
`define ALU_SLTU  6'b000011


`define OPCODE_SYSTEM   7'h73
`define OPCODE_FENCE    7'h0f
`define OPCODE_OP       7'h33
`define OPCODE_OPIMM    7'h13
`define OPCODE_STORE    7'h23
`define OPCODE_LOAD     7'h03
`define OPCODE_BRANCH   7'h63
`define OPCODE_JALR     7'h67
`define OPCODE_JAL      7'h6f
`define OPCODE_AUIPC    7'h17
`define OPCODE_LUI      7'h37




// regfile mux write data
`define CU_REGFILE_ALU      4'b0000
`define CU_REGFILE_PCNEXT   4'b0001
`define CU_REGFILE_PCIMM    4'b0010
`define CU_REGFILE_IMM      4'b0011
`define CU_REGFILE_DMEM_W   4'b0100
`define CU_REGFILE_DMEM_B   4'b0101
`define CU_REGFILE_DMEM_H   4'b0110
`define CU_REGFILE_DMEM_BU  4'b0111
`define CU_REGFILE_DMEM_HU  4'b1000

// dmem type load store
`define CU_LDST_B           4'b0001
`define CU_LDST_H           4'b0011
`define CU_LDST_W           4'b1111

// operand b selection
`define CU_OP_B_REGB        2'b00
`define CU_OP_B_IMM_I       2'b01
`define CU_OP_B_IMM_S       2'b10
 
// program counter mux
`define CU_PC_NEXT           2'b00
`define CU_PC_PCIMM_B        2'b01
`define CU_PC_PCIMM_J        2'b10
`define CU_PC_REGFILE        2'b11
