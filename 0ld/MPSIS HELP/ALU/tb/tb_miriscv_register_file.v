`timescale 1ns / 1ps

//`include "miriscv_defines.v"

module tb_miriscv_register_file();


localparam ITERATIONS = 1000;


reg clk;
reg rst_n;

reg  [4:0] raddr_a_i;
reg  [4:0] raddr_b_i;
reg  [4:0] waddr_a_i;
reg [31:0] wdata_a_i;
reg        we_a_i;

wire [31:0] rdata_a_o;
wire [31:0] rdata_b_o;



miriscv_register_file DUT
(
  .clk_i(clk),
  .rst_n_i(rst_n),

  .raddr_a_i(raddr_a_i),
  .rdata_a_o(rdata_a_o),

  .raddr_b_i(raddr_b_i),
  .rdata_b_o(rdata_b_o),


  .waddr_a_i(waddr_a_i),
  .wdata_a_i(wdata_a_i),
  .we_a_i(we_a_i)

);

initial begin
  clk            = 0;
  forever #5 clk = ~clk;
end

integer i;
integer error_counter;


reg [31:0] regfile_reference [0:31];
reg [4:0] raddr_a;
reg [4:0] raddr_b;
reg [4:0] waddr;  
reg [31:0] wdata;


initial begin
  raddr_a_i = 5'b0;
  raddr_b_i = 5'b0;
  waddr_a_i = 32'b0;
  wdata_a_i = 32'b0;
  we_a_i = 1'b0;
  rst_n = 1'b1;
  
  error_counter <= 0;
  
  for (i = 0; i < 32; i = i+1) begin
    regfile_reference[i] <= 32'd0;
  end
  
  
  #20

  reset_check();
  #20
  
  for (i = 0; i < ITERATIONS; i = i+1)
    begin

      waddr   = $random;
      wdata   = $random;
       
      if(i != 0) 
      begin
        read_data_a(raddr_a, regfile_reference[raddr_a]); // addr, ref_data
        read_data_b(raddr_b, regfile_reference[raddr_b]); // addr, ref_data
      end
      
      write_data(waddr, wdata); // addr, data
      
      if(waddr != 0)
        regfile_reference[waddr] <= wdata;
      
      raddr_a = $random; 
      raddr_b = $random;      
      set_raddr_a(raddr_a); // addr
      set_raddr_b(raddr_b); // addr
      
      #10 we_a_i <= 0;
      
    end
    
    
    $display("TB finished with %d errors.", error_counter);
    
    //$finish;
end
        
        

task reset_check;
    begin
      rst_n     = 1'b0;
      #30 rst_n = 1'b1;
      
      for (i = 0; i < 32; i = i+1)
        begin
          raddr_a_i <= i;
          #10
          if (rdata_a_o != 32'd0) $display("ERROR register %d value does not equal zero after reset", i);
        end
        raddr_a_i <= 5'd0;
    end
endtask 


task read_data_a;
    input [4:0] register;
    input [31:0] ref_data;
    
    begin
      if (rdata_a_o != ref_data) begin
        $display("ERROR wrong data read from register %d at port A \n Should be: %h \n Data received: %h \n", register, ref_data, rdata_a_o);
        error_counter = error_counter + 1;
      end
    end
endtask 
 
 
task read_data_b;
    input [4:0] register;
    input [31:0] ref_data;
    begin
      if (rdata_b_o != ref_data) begin 
        $display("ERROR wrong data read from register %d at port B \n Should be: %h \n Data received: %h \n", register, ref_data, rdata_b_o);
        error_counter = error_counter + 1;
      end
    end
endtask 
 
 
task write_data;
    input [4:0] addr;
    input [31:0] data;
    begin
      waddr_a_i  <= addr;
      wdata_a_i  <= data;
      we_a_i     <= 1;
    end
endtask 


task set_raddr_a;
    input [4:0] addr;
    begin
      raddr_a_i  <= addr;
    end
endtask 
  
  
task set_raddr_b;
    input [4:0] addr;
    begin
      raddr_b_i  <= addr;
    end
endtask 


endmodule
