 `timescale 1ns / 1ps

//`include "miriscv_defines.v"

module tb_miriscv_cu();

parameter TEST_VALUES     = 48;
parameter TIME_OPERATION  = 100;



  wire [31:0] instr_rdata_i;
  wire        illegal_instr_o;

  // ALU signals
  wire alu_comparison_result_i;

  wire [5:0] alu_operator_o;
  wire [1:0] alu_mux_b_sel_o;

  // regfile
  wire regfile_we_o;
  wire [3:0] regfile_mux_wdata_sel_o;

  // data memory
  wire [3:0] dmem_we_o;
  
  //Program counter
  wire [1:0] pc_mux_source_sel_o;



miriscv_cu DUT(

    // instructions memory
  .instr_rdata_i (instr_rdata_i),
  .illegal_instr_o (illegal_instr_o),

  // ALU signals
  .alu_comparison_result_i (alu_comparison_result_i),

  .alu_operator_o (alu_operator_o),
  .alu_mux_b_sel_o (alu_mux_b_sel_o),

  // regfile
  .regfile_we_o (regfile_we_o),
  .regfile_mux_wdata_sel_o (regfile_mux_wdata_sel_o),

  // data memory
  .dmem_we_o (dmem_we_o),
  
  //Program counter
  .pc_mux_source_sel_o (pc_mux_source_sel_o)
  );


wire [19:0] output_vector = { illegal_instr_o, //1
                              alu_operator_o, // 6
                              alu_mux_b_sel_o, // 2
                              regfile_we_o, // 1
                              regfile_mux_wdata_sel_o, //4
                              dmem_we_o, // 4
                              pc_mux_source_sel_o // 2
                            };



integer     i, err_count = 0;
integer     file_test_input = 0;
integer     file_test_output = 0;



reg [19:0] result_dump = 0;

reg [32:0] input_dump = 0;


assign instr_rdata_i            = input_dump[31:0];
assign alu_comparison_result_i  = input_dump[32];



reg [8*9:1] instruction;


initial
  begin
    file_test_input = $fopen("C:\\Users\\u133180\\Desktop\\ALU\\tb\\miriscv_cu_input_vector.txt" , "r");
    file_test_output = $fopen("C:\\Users\\u133180\\Desktop\\ALU\\tb\\miriscv_cu_output_vector.txt" , "r");
    
    if ( file_test_input == 0 || file_test_output == 0)
      begin
        $display("ERROR files has not opened");
        $finish;
      end
    
    $display( "Start test: ");
    
    for ( i = 0; i < TEST_VALUES; i = i + 1 )
      begin
        $fscanf( file_test_input    , "%b\n", input_dump);
        $fscanf( file_test_output    , "%b\n", result_dump);
        
        #TIME_OPERATION;


        if( (output_vector != result_dump)) begin
          $display("\nERROR decoding instruction: %s", instruction);
          $display("instr_rdata_i:\t\t\t\t", "%h", instr_rdata_i);
          $display("alu_comparison_result_i:\t","%h" , alu_comparison_result_i);

          $display("\nSignal\t\t\t\tIs\t\tShould be");

          $display("illegal_instr_o\t\t\t", "%h\t\t\t", illegal_instr_o, "%h", result_dump[19]);
          $display("alu_operator_o\t\t\t", "%h\t\t\t", alu_operator_o, "%h", result_dump[18:13]);
          $display("alu_mux_b_sel_o\t\t\t", "%h\t\t\t", alu_mux_b_sel_o, "%h", result_dump[12:11]);
          $display("regfile_we_o\t\t\t\t", "%h\t\t\t", regfile_we_o, "%h", result_dump[10]);
          $display("regfile_mux_wdata_sel_o\t\t", "%h\t\t\t", regfile_mux_wdata_sel_o, "%h", result_dump[9:6]);
          $display("dmem_we_o\t\t\t\t", "%h\t\t\t", dmem_we_o, "%h", result_dump[5:2]);
          $display("pc_mux_source_sel_o\t\t\t", "%h\t\t\t", pc_mux_source_sel_o, "%h", result_dump[1:0]);
          $display( "======================================");
          err_count = err_count + 1'b1;
        end
      
      end
    
    $fclose( file_test_input );
    
    $display("Test finished. Number of errors: %d", err_count);
    
    $stop;
  end

 
always @(*) begin
  case(i)
    0  : instruction = "illegal";

    1  : instruction = "LUI";
    2  : instruction = "AUIPC";
    3  : instruction = "JAL";

    4  : instruction = "JALR";
    5  : instruction = "BEQ";
    6  : instruction = "BNE";
    7  : instruction = "BLT";
    8  : instruction = "BGE";
    9  : instruction = "BLTU";
    10 : instruction = "BGEU";


    11  : instruction = "LUI";
    12  : instruction = "AUIPC";
    13  : instruction = "JAL";

    14  : instruction = "JALR";
    15  : instruction = "BEQ";
    16  : instruction = "BNE";
    17  : instruction = "BLT";
    18  : instruction = "BGE";
    19  : instruction = "BLTU";
    20 : instruction = "BGEU";



    21 : instruction = "LB";
    22 : instruction = "LH";
    23 : instruction = "LW";
    24 : instruction = "LBU";
    25 : instruction = "LHU";

    26  : instruction = "SB";
    27  : instruction = "SH";
    28  : instruction = "SW";

    29  : instruction = "ADDI";
    30  : instruction = "SLTI";
    31  : instruction = "SLTIU";
    32  : instruction = "XORI";
    33  : instruction = "ORI";
    34  : instruction = "ANDI";

    35  : instruction = "SLLI";
    36 : instruction = "SRLI";
    37 : instruction = "SRAI";

    38 : instruction = "ADD";
    39 : instruction = "SUB";
    40 : instruction = "SLL";
    41 : instruction = "SLT";
    42 : instruction = "SLTU";

    43 : instruction = "XOR";
    44 : instruction = "SRL";
    45 : instruction = "SRA";
    46 : instruction = "OR";
    47 : instruction = "SLL";

    default   : instruction = "illegal";
  endcase
end





/*
initial
  begin
    file_test_input = $fopen("input_vector.txt" , "r");
    file_test_output = $fopen("output_vector.txt" , "w");
    
    if ( file_test_input == 0 || file_test_output == 0)
      begin
        $display("ERROR files has not opened");
        $finish;
      end
    
    $display( "Start test: ");
    
    for ( i = 0; i < TEST_VALUES; i = i + 1 )
      begin
        $fscanf( file_test_input    , "%b\n", input_dump);
        
        #TIME_OPERATION;


        $fwrite( file_test_output, "%b\n", output_vector);
      
      end
    
    $fclose( file_test_input );
    $fclose( file_test_output );

    
    $stop;
  end
*/




endmodule
