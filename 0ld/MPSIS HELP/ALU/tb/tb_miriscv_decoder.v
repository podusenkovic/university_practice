`timescale 1ns / 1ps

//`include "miriscv_defines.v"

module tb_miriscv_decoder();

parameter TEST_VALUES     = 38;
parameter TIME_OPERATION  = 100;



  wire illegal_insn_o;          // illegal instruction encountered

  // from IF/ID pipeline
  wire  [31:0] instr_rdata_i;           // instruction read from instr memory/cache


  // ALU signals
  wire [5:0] alu_operator_o; // ALU operation selection
  wire [2:0] alu_op_a_mux_sel_o;      // operand a selection: reg value, PC, immediate or zero
  wire [2:0] alu_op_b_mux_sel_o;      // oNOperand b selection: reg value or immediate

  wire [0:0] imm_a_mux_sel_o;         // immediate selection for operand a
  wire [3:0] imm_b_mux_sel_o;         // immediate selection for operand b



  // register file related signals
  wire regfile_we_o;            // write enable for regfile


  // LD/ST unit signals
  wire data_req_o;              // start transaction to data memory
  wire data_we_o;               // data memory write enable
  wire [1:0] data_type_o;             // data type on data memory: byte, half word or word
  wire data_sign_extension_o;   // sign extension on read data from data memory
  wire [1:0] data_reg_offset_o;       // offset in byte inside register for stores

  // jump/branches
  wire jump_in_id_o;            // jump is being calculated in ALU
  wire branch_in_id_o;




miriscv_decoder decoder
(

  .illegal_insn_o(illegal_insn_o),          // illegal instruction encountered

  // from IF/ID pipeline
  .instr_rdata_i(instr_rdata_i),           // instruction read from instr memory/cache

  // ALU signals
  .alu_operator_o(alu_operator_o), // ALU operation selection
  .alu_op_a_mux_sel_o(alu_op_a_mux_sel_o),      // operand a selection: reg value, PC, immediate or zero
  .alu_op_b_mux_sel_o(alu_op_b_mux_sel_o),      // oNOperand b selection: reg value or immediate

  .imm_a_mux_sel_o(imm_a_mux_sel_o),         // immediate selection for operand a
  .imm_b_mux_sel_o(imm_b_mux_sel_o),         // immediate selection for operand b


  // register file related signals
  .regfile_we_o(regfile_we_o),            // write enable for regfile

  // LD/ST unit signals
  .data_req_o(data_req_o),              // start transaction to data memory
  .data_we_o(data_we_o),               // data memory write enable
  .data_type_o(data_type_o),             // data type on data memory: byte, half word or word
  .data_sign_extension_o(data_sign_extension_o),   // sign extension on read data from data memory
  .data_reg_offset_o(data_reg_offset_o),       // offset in byte inside register for stores

  // jump/branches
  .jump_in_id_o   (jump_in_id_o),            // jump is being calculated in ALU
  .branch_in_id_o (branch_in_id_o)
);


wire [27:0] output_vector = { illegal_insn_o, // 1
                              alu_operator_o,  // 6
                              alu_op_a_mux_sel_o, // 3
                              alu_op_b_mux_sel_o, // 3
                              imm_a_mux_sel_o, // 1
                              imm_b_mux_sel_o, // 4
                              regfile_we_o, // 1
                              data_req_o, // 1
                              data_we_o, // 1
                              data_type_o, // 2
                              data_sign_extension_o, // 1
                              data_reg_offset_o, // 2
                              jump_in_id_o, // 1
                              branch_in_id_o}; // 1


integer     i, err_count = 0;
integer     file_test_input = 0;
integer     file_test_output = 0;


reg [8*9:1] instruction;

reg [27:0] result_dump = 0;

reg [31:0] input_dump = 0;


assign instr_rdata_i            = input_dump[31:0];

initial
  begin
    file_test_input = $fopen("input_vector.txt" , "r");
    file_test_output = $fopen("output_vector.txt" , "r");
    
    if ( file_test_input == 0 || file_test_output == 0)
      begin
        $display("ERROR files has not opened");
        $finish;
      end
    
    $display( "Start test: ");
    
    for ( i = 0; i < TEST_VALUES; i = i + 1 )
      begin
        $fscanf( file_test_input    , "%b\n", input_dump);
        $fscanf( file_test_output    , "%b\n", result_dump);
        
        #TIME_OPERATION;


        if( (output_vector != result_dump)) begin
          $display("ERROR decoding instruction: %s", instruction);

          
          $display("Signal\t\t\t\t\tShould be\tIs");
          $display("illegal_insn_o\t\t\t", "%h\t\t\t", illegal_insn_o, "%h", result_dump[27]);

          $display("alu_operator_o\t\t\t", "%h\t\t\t", alu_operator_o, "%h", result_dump[26:21]);
          $display("alu_op_a_mux_sel_o\t\t", "%h\t\t\t", alu_op_a_mux_sel_o, "%h", result_dump[20:18]);
          $display("alu_op_b_mux_sel_o\t\t", "%h\t\t\t", alu_op_b_mux_sel_o, "%h", result_dump[17:15]);
          $display("imm_a_mux_sel_o\t\t\t", "%h\t\t\t", imm_a_mux_sel_o, "%h", result_dump[14]);
          $display("imm_b_mux_sel_o\t\t\t", "%h\t\t\t", imm_b_mux_sel_o, "%h", result_dump[13:10]);
          $display("regfile_we_o\t\t\t", "%h\t\t\t", regfile_we_o, "%h", result_dump[9]);
          $display("data_req_o\t\t\t\t", "%h\t\t\t", data_req_o, "%h", result_dump[8]);
          $display("data_we_o\t\t\t\t", "%h\t\t\t", data_we_o, "%h", result_dump[7]);
          $display("data_type_o\t\t\t\t", "%h\t\t\t", data_type_o, "%h", result_dump[6:5]);

          $display("data_sign_extension_o\t", "%h\t\t\t", data_sign_extension_o, "%h", result_dump[4]);
          $display("data_reg_offset_o\t\t", "%h\t\t\t", data_reg_offset_o, "%h", result_dump[3:2]);
          $display("jump_in_id_o\t\t\t", "%h\t\t\t", jump_in_id_o, "%h", result_dump[1]);
          $display("branch_in_id_o\t\t\t", "%h\t\t\t", branch_in_id_o, "%h", result_dump[0]);

          err_count = err_count + 1'b1;
        end
      
      end
    
    $fclose( file_test_input );
    
    $display("Number of errors: %d", err_count);
    
    $stop;
  end

 
always @(*) begin
  case(i)
    0  : instruction = "illegal";

    1  : instruction = "LUI";
    2  : instruction = "AUIPC";
    3  : instruction = "JAL";

    4  : instruction = "JALR";
    5  : instruction = "BEQ";
    6  : instruction = "BNE";
    7  : instruction = "BLT";
    8  : instruction = "BGE";
    9  : instruction = "BLTU";
    10 : instruction = "BGEU";

    11 : instruction = "LB";
    12 : instruction = "LH";
    13 : instruction = "LW";
    14 : instruction = "LBU";
    15 : instruction = "LHU";

    16  : instruction = "SB";
    17  : instruction = "SH";
    18  : instruction = "SW";

    19  : instruction = "ADDI";
    20  : instruction = "SLTI";
    21  : instruction = "SLTIU";
    22  : instruction = "XORI";
    23  : instruction = "ORI";
    24  : instruction = "ANDI";

    25  : instruction = "SLLI";
    26 : instruction = "SRLI";
    27 : instruction = "SRAI";

    28 : instruction = "ADD";
    29 : instruction = "SUB";
    30 : instruction = "SLL";
    31 : instruction = "SLT";
    32 : instruction = "SLTU";

    33 : instruction = "XOR";
    34 : instruction = "SRL";
    35 : instruction = "SRA";
    36 : instruction = "OR";
    37 : instruction = "SLL";


    default   : instruction = "illegal";
  endcase
end

endmodule
