`timescale 1ns / 1ps

//`include "miriscv_defines.v"

module tb_miriscv_system();

parameter HALF_PERIOD_CLK = 10;

		reg         	clk_i;
		reg  [9:0]    	SW;
		reg  [3:0]	  	KEY;
		wire [6:0]    	HEX0;
		wire [6:0]    	HEX1;
		wire [6:0]    	HEX2;
		wire [6:0]    	HEX3;
		wire [1:0]		LEDG;
		wire [9:0]		LEDR;


initial begin
    clk_i = 1'b0;
    forever #HALF_PERIOD_CLK clk_i = ~clk_i;
end


miriscv_system DUT(.CLOCK_25(clk_i),
						 .SW(SW),
						 .LEDR(LEDR),
						 .LEDG(LEDG),
						 .KEY(KEY),
						 .HEX0(HEX0),
						 .HEX1(HEX1),
						 .HEX2(HEX2),
						 .HEX3(HEX3));


initial begin
			SW = 10'b1111111111;
			KEY = 4'b1111;
			KEY[0] = 1'b0;
			#10;
			KEY[0] = 1'b1;
			#50;
			SW = 10'b1;
			#50;
			KEY[3] = 1'b0;
			#50;
			KEY[3] = 1'b1;
			#50;
			SW = 10'b0101010101;
			#50;
			KEY[3] = 1'b0;
			#50;
			KEY[3] = 1'b1;
			#50;
			SW = 10'b1010101010;
			#50;
			KEY[3] = 1'b0;
			#50;
			KEY[3] = 1'b1;
			#50;
			KEY[2] = 1'b0;
			#50;
			KEY[2] = 1'b1;
			#50;
			SW = 10'b1111111111;
			#50
			KEY[2] = 1'b0;
			#50;
			KEY[2] = 1'b1;
			#50;
			KEY[2] = 1'b0;
			#50;
			KEY[2] = 1'b1;
			SW = 10'b10001101;
			#50;
			#50;
			KEY[1] = 1'b0;
			#50;
			KEY[1] = 1'b1;
			#50;
			KEY[1] = 1'b0;
			#50;
			KEY[1] = 1'b1;
			#50;

		end

endmodule
