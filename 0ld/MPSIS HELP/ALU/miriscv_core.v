`timescale 1ns / 1ps

`include "miriscv_defines.v"

module miriscv_core (

input         clk_i,
input         rst_n_i,

output [31:0] imem_addr_o,
input  [31:0] imem_data_i,

output [31:0] dmem_addr_o,
output [3:0]  dmem_we_o,
input  [31:0] dmem_data_i,
output [31:0] dmem_data_o);


wire 								regfile_we;
wire 								dmem_we;

wire 								illegal_instr;

wire 	[1:0]						alu_mux_b_sel;
wire 	[3:0]						regfile_mux_wdata_sel;
	
wire 	[4:0]						raddr_a 	= 		imem_data_i[19:15];
wire 	[4:0]						raddr_b 	= 		imem_data_i[24:20];
wire 	[31:0]					rdata_a;
wire 	[31:0]					rdata_b;

reg 	[31:0]					wdata_a;
wire 	[4:0]						waddr_a 	= 		imem_data_i[11:7];

reg 	[31:0] 					PC 		= 		32'b0;

wire 								comparision_result;
wire 	[`ALU_OP_WIDTH-1:0]	operator;
wire	[31:0]					operand_a = 	rdata_a;
reg 	[31:0]					operand_b;
wire 	[31:0]					result;
wire 	[1:0]						pc_mux_source_sel;

assign imem_addr_o = PC;
assign dmem_addr_o = waddr_a;
assign dmem_data_o = wdata_a;

miriscv_alu ALU_module(.operator_i(operator),
							  .operand_a_i(operand_a),
							  .operand_b_i(operand_b),
							  .result_o(result),
							  .comparison_result_o(comparision_result));

miriscv_cu Control_Unit(.instr_rdata_i(imem_data_i),
								.illegal_instr_o(illegal_instr),
								.alu_comparison_result_i(comparision_result),
								.alu_operator_o(operator),
								.alu_mux_b_sel_o(alu_mux_b_sel),
								.regfile_we_o(regfile_we),
								.regfile_mux_wdata_sel_o(regfile_mux_wdata_sel),
								.dmem_we_o(dmem_we_o),
								.pc_mux_source_sel_o(pc_mux_source_sel));

miriscv_register_file Register_File(.clk_i(clk_i),
												.rst_n_i(rst_n_i),
												.raddr_a_i(raddr_a),
												.rdata_a_o(rdata_a),
												.raddr_b_i(raddr_b),
												.rdata_b_o(rdata_b),
												.waddr_a_i(waddr_a),
												.wdata_a_i(wdata_a),
												.we_a_i(regfile_we));

always@(*) begin
	case (alu_mux_b_sel)
	`CU_OP_B_REGB: 	operand_b <= rdata_b;
	`CU_OP_B_IMM_I: 	operand_b <= {{20{imem_data_i[31]}},imem_data_i[31:20]};
	`CU_OP_B_IMM_S: 	operand_b <= {{15{imem_data_i[31]}},imem_data_i[31:20],imem_data_i[11:7]};
	default: 			operand_b <= rdata_b;
	endcase
	case (regfile_mux_wdata_sel)
	`CU_REGFILE_ALU: 		wdata_a <= result;
	`CU_REGFILE_PCNEXT:	wdata_a <= PC + 3'b100;
	`CU_REGFILE_PCIMM:	wdata_a <= imem_addr_o + (imem_data_i[31:20] << 12);
	`CU_REGFILE_IMM:		wdata_a <= (imem_data_i[31:20] << 12);
	`CU_REGFILE_DMEM_B:	wdata_a <= {{24{dmem_data_i[7]}},dmem_data_i[7:0]};
	`CU_REGFILE_DMEM_H:	wdata_a <= {{16{dmem_data_i[15]}},dmem_data_i[15:0]};
	`CU_REGFILE_DMEM_W:	wdata_a <= dmem_data_i;
	`CU_REGFILE_DMEM_BU:	wdata_a <= {{24{1'b0}},dmem_data_i[7:0]};
	`CU_REGFILE_DMEM_HU:	wdata_a <= {{16{1'b0}},dmem_data_i[15:0]};
	endcase 
end

always @(posedge clk_i)
	case (pc_mux_source_sel)
	`CU_PC_NEXT:			PC <= PC + 3'b100;
	`CU_PC_PCIMM_B:		PC <= PC + {{12{imem_data_i[31]}},imem_data_i[31:12]};
	`CU_PC_PCIMM_J:		PC <= PC + {imem_data_i[31],imem_data_i[21:12],imem_data_i[22],imem_data_i[30:23]};
	`CU_PC_REGFILE:		PC <= rdata_a;
	endcase


endmodule 