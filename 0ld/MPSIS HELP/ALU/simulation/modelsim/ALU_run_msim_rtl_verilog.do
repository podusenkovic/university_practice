transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+C:/Users/podus/Desktop/MPSIS\ HELP/ALU {C:/Users/podus/Desktop/MPSIS HELP/ALU/miriscv_defines.v}
vlog -vlog01compat -work work +incdir+C:/Users/podus/Desktop/MPSIS\ HELP/ALU {C:/Users/podus/Desktop/MPSIS HELP/ALU/miriscv_register_file.v}
vlog -vlog01compat -work work +incdir+C:/Users/podus/Desktop/MPSIS\ HELP/ALU {C:/Users/podus/Desktop/MPSIS HELP/ALU/miriscv_cu.v}
vlog -vlog01compat -work work +incdir+C:/Users/podus/Desktop/MPSIS\ HELP/ALU {C:/Users/podus/Desktop/MPSIS HELP/ALU/miriscv_core.v}
vlog -vlog01compat -work work +incdir+C:/Users/podus/Desktop/MPSIS\ HELP/ALU {C:/Users/podus/Desktop/MPSIS HELP/ALU/miriscv_alu.v}

