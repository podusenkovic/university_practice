library verilog;
use verilog.vl_types.all;
entity tb_miriscv_system is
    generic(
        HALF_PERIOD_CLK : integer := 10
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of HALF_PERIOD_CLK : constant is 1;
end tb_miriscv_system;
