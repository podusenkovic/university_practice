library verilog;
use verilog.vl_types.all;
entity miriscv_core is
    port(
        clk_i           : in     vl_logic;
        rst_n_i         : in     vl_logic;
        imem_addr_o     : out    vl_logic_vector(31 downto 0);
        imem_data_i     : in     vl_logic_vector(31 downto 0);
        dmem_addr_o     : out    vl_logic_vector(31 downto 0);
        dmem_we_o       : out    vl_logic_vector(3 downto 0);
        dmem_data_i     : in     vl_logic_vector(31 downto 0);
        dmem_data_o     : out    vl_logic_vector(31 downto 0)
    );
end miriscv_core;
