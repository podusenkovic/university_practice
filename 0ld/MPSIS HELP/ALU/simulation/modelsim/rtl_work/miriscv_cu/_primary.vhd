library verilog;
use verilog.vl_types.all;
entity miriscv_cu is
    port(
        instr_rdata_i   : in     vl_logic_vector(31 downto 0);
        illegal_instr_o : out    vl_logic;
        alu_comparison_result_i: in     vl_logic;
        alu_operator_o  : out    vl_logic_vector(5 downto 0);
        alu_mux_b_sel_o : out    vl_logic_vector(1 downto 0);
        regfile_we_o    : out    vl_logic;
        regfile_mux_wdata_sel_o: out    vl_logic_vector(3 downto 0);
        dmem_we_o       : out    vl_logic_vector(3 downto 0);
        pc_mux_source_sel_o: out    vl_logic_vector(1 downto 0)
    );
end miriscv_cu;
