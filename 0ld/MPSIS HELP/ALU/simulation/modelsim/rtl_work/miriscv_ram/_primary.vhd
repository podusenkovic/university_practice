library verilog;
use verilog.vl_types.all;
entity miriscv_ram is
    generic(
        size            : integer := 1024;
        init_file       : string  := "C:\Users\podus\Desktop\MPSIS HELP\ALU\resources\test_mirv_embedded.dat"
    );
    port(
        clk_i           : in     vl_logic;
        ena_i           : in     vl_logic;
        wea_i           : in     vl_logic;
        aa_i            : in     vl_logic_vector(31 downto 0);
        bwea_i          : in     vl_logic_vector(3 downto 0);
        da_i            : in     vl_logic_vector(31 downto 0);
        qa_o            : out    vl_logic_vector(31 downto 0);
        enb_i           : in     vl_logic;
        web_i           : in     vl_logic;
        ab_i            : in     vl_logic_vector(31 downto 0);
        bweb_i          : in     vl_logic_vector(3 downto 0);
        db_i            : in     vl_logic_vector(31 downto 0);
        qb_o            : out    vl_logic_vector(31 downto 0)
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of size : constant is 1;
    attribute mti_svvh_generic_type of init_file : constant is 1;
end miriscv_ram;
