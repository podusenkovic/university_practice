library verilog;
use verilog.vl_types.all;
entity miriscy_register_file is
    generic(
        DATA_WIDTH      : integer := 32
    );
    port(
        clk_i           : in     vl_logic;
        rst_n_i         : in     vl_logic;
        raddr_a_i       : in     vl_logic_vector(4 downto 0);
        rdata_a_o       : out    vl_logic_vector;
        raddr_b_i       : in     vl_logic_vector(4 downto 0);
        rdata_b_o       : out    vl_logic_vector;
        waddr_a_i       : in     vl_logic_vector(4 downto 0);
        wdata_a_i       : in     vl_logic_vector;
        we_a_i          : in     vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of DATA_WIDTH : constant is 1;
end miriscy_register_file;
