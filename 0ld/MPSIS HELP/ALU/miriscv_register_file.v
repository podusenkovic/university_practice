 module miriscv_register_file
#(
  parameter DATA_WIDTH    = 32
)
(
  // Clock and Reset
  input                   clk_i,
  input                   rst_n_i,

  //Read port R1
  input  [4:0]             raddr_a_i,
  output [DATA_WIDTH-1:0]  rdata_a_o,

  //Read port R2
  input  [4:0]             raddr_b_i,
  output [DATA_WIDTH-1:0]  rdata_b_o,


  // Write port W1
  input  [4:0]              waddr_a_i,
  input  [DATA_WIDTH-1:0]   wdata_a_i,
  input                     we_a_i

);
	integer i = 0;
	reg [DATA_WIDTH - 1:0] Mem_Reg [31:0];
	always @(posedge clk_i)
		if (!rst_n_i)
			for (i = 0; i < 32; i = i + 1)
				Mem_Reg [i] <= 'b0;
		else if (we_a_i && waddr_a_i != 0)
			Mem_Reg[waddr_a_i] <= wdata_a_i;			
	
	assign rdata_a_o = Mem_Reg[raddr_a_i];
	assign rdata_b_o = Mem_Reg[raddr_b_i];



endmodule
