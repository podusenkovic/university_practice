
`timescale 1ns/1ps

module miriscv_ram
  #(
    parameter size       = 1024,
    parameter init_file  = "test_mirv_embedded.dat"
    ) 
   (
    input          clk_i,

    input          ena_i,
    input          wea_i,
    input  [31:0]  aa_i,
    input  [3:0]   bwea_i,
    input  [31:0]  da_i,
    output [31:0]  qa_o,

    input          enb_i,
    input          web_i,
    input  [31:0]  ab_i,
    input  [3:0]   bweb_i,
    input  [31:0]  db_i,
    output [31:0]  qb_o
    );

	localparam size_imem =256;
	localparam size_dmem =256;
	
   reg [31:0]         imem [0:size_imem/4-1];
	reg [31:0]         dmem [0:size_dmem/4-1];
   //reg [31:0]         qa_int, qb_int;
    

   
  integer ram_index; 
   
  initial begin
    for (ram_index = 0; ram_index < size_imem/4-1; ram_index = ram_index + 1)
      imem[ram_index] = {32{1'b0}};
    if(init_file != "")    
      $readmemh(init_file, imem);
  end

  
  
  always @ (posedge clk_i)
	begin
		if(enb_i)
		begin
		   if(web_i && bweb_i[0])
		     dmem [(ab_i / 4) % size_dmem][7:0] <= db_i[7:0];
		   if(web_i && bweb_i[1])
		     dmem [(ab_i / 4) % size_dmem][15:8] <= db_i[15:8];
		   if(web_i && bweb_i[2])
		     dmem [(ab_i / 4) % size_dmem][23:16] <= db_i[23:16];
		   if(web_i && bweb_i[3])
		     dmem [(ab_i / 4) % size_dmem][31:24] <= db_i[31:24];

		end
	end
  
  assign qa_o = imem [(aa_i / 4) % size_imem]; 
  assign qb_o = dmem [(ab_i / 4) % size_dmem];
   

endmodule