#include <stddef.h>
#include <stdint.h>

#define BASE_HEX0    0x80000100
#define BASE_HEX1    0x80000200
#define BASE_HEX2    0x80000300
#define BASE_HEX3    0x80000400
#define BASE_LEDG    0x80000500
#define BASE_LEDR    0x80000600
#define BASE_KEY     0x80000700
#define BASE_SW      0x80000800

void main() {

  uint32_t key, sw;

  *(volatile unsigned *) ( BASE_HEX0 ) = 0x1;
  *(volatile unsigned *) ( BASE_HEX1 ) = 0x2;
  *(volatile unsigned *) ( BASE_HEX2 ) = 0x3;
  *(volatile unsigned *) ( BASE_HEX3 ) = 0x4;
  *(volatile unsigned *) ( BASE_LEDG ) = 0xF;
  *(volatile unsigned *) ( BASE_LEDR ) = 0x4F;

  for(;;){

    sw  = *(volatile unsigned *) ( BASE_SW );
    key = *(volatile unsigned *) ( BASE_KEY );

    *(volatile unsigned *) ( BASE_LEDG ) = *(volatile unsigned *) ( BASE_LEDG ) + key;
    *(volatile unsigned *) ( BASE_LEDR ) = *(volatile unsigned *) ( BASE_LEDR ) + key;

    *(volatile unsigned *) ( BASE_HEX0 ) = sw % 10;
    *(volatile unsigned *) ( BASE_HEX1 ) = sw % 100 / 10;
    *(volatile unsigned *) ( BASE_HEX2 ) = sw % 1000 / 100;
    *(volatile unsigned *) ( BASE_HEX3 ) = sw / 1000;
  }
}
