#include <stddef.h>
#include <stdint.h>

#define BASE_HEX0    0x80000100
#define BASE_HEX1    0x80000200
#define BASE_HEX2    0x80000300
#define BASE_HEX3    0x80000400
#define BASE_LEDG    0x80000500
#define BASE_LEDR    0x80000600
#define BASE_KEY     0x80000700
#define BASE_SW      0x80000800

void main() {
  
  uint32_t key, sw, shift_key;


  for(;;){

    sw  = *(volatile unsigned *) ( BASE_SW );
    key = *(volatile unsigned *) ( BASE_KEY );

    *(volatile unsigned *) ( BASE_LEDG ) = *(volatile unsigned *) ( BASE_LEDG ) + key;
    *(volatile unsigned *) ( BASE_LEDR ) = *(volatile unsigned *) ( BASE_LEDR ) + key;

    shift_key = sw << 2;

    *(volatile unsigned *) ( BASE_HEX0 ) = shift_key;
    *(volatile unsigned *) ( BASE_HEX1 ) = shift_key;
    *(volatile unsigned *) ( BASE_HEX2 ) = shift_key;
    *(volatile unsigned *) ( BASE_HEX3 ) = shift_key;
  }
}
