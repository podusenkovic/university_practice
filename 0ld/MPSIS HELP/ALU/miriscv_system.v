`timescale 1ns / 1ps

`include "miriscv_defines.v"

module miriscv_system(input				CLOCK_25,
							 input  		[9:0]	SW,
							 output reg [9:0]	LEDR,
							 output reg [1:0]	LEDG,
							 input  		[3:0]	KEY,
							 output 		[6:0]	HEX0,
							 output 		[6:0]	HEX1,
							 output 		[6:0]	HEX2,
							 output 		[6:0]	HEX3);


reg en_port_a;
reg en_port_b;		 

wire clk = CLOCK_25;
wire rst_n = KEY[0];

wire [31:0] imem_addr;
wire [31:0] imem_data;

wire [31:0] dmem_addr;
wire [31:0] dmem_data_core2mem;
wire [31:0] dmem_data_mem2core_i;
wire [31:0] dmem_data_mem2core_o;

wire [3:0] 	dmem_we;
wire 			ram_mem_en;
wire [3:0]	ram_mem_we;

reg 			we_core_data;

reg [3:0] data_to_hex;
reg [9:0] data_to_ledr;
reg [1:0] data_to_ledg;

reg[3:0] we_hex;
reg 		we_ledg;
reg 		we_ledr;

//assign LEDR = data_to_ledr;
//assign LEDG = data_ledg;

reg 		we_ram_a;
reg 		we_ram_b;

reg take_data_from_outside;
reg [31:0] data_from_outside;


dec decoder(.in(data_to_hex),
				.we(we_hex),
				.out({HEX3,HEX2,HEX1,HEX0}));
			
always@(*) begin
	data_from_outside = 32'b0;
	take_data_from_outside = 1'b0;
	we_ram_a = 1'b0;
	we_ram_b = 1'b0;
	we_hex = 4'b0;
	we_ledg = 1'b0;
	we_ledr = 1'b0;
	en_port_a = 1'b0;
	en_port_b = 1'b0;
	if (~rst_n) begin	
		data_to_hex = 4'b0;
		take_data_from_outside = 1'b0;
		data_from_outside = 32'b0;
	end
	if (dmem_addr >= 32'b0 && dmem_addr <= 32'h80000000) begin
		take_data_from_outside = 1'b0;
		en_port_b = 1'b1;
	end
	else case (dmem_addr)
	`SYS_KEYS_ADDR: begin
		data_from_outside[2:0] 	= KEY[3:1];
		take_data_from_outside 	= 1'b1;
	end
	`SYS_SW_ADDR: 	begin
		data_from_outside[9:0]	= SW[9:0];
		take_data_from_outside 	= 1'b1;
	end
	`SYS_LEDG_ADDR: begin
		LEDG 			= dmem_data_core2mem[1:0];
		we_ledg 		= 1'b1;
	end
	`SYS_LEDR_ADDR: begin
		LEDR[9:0]	= dmem_data_core2mem[9:0];
		we_ledr 		= 1'b1;
	end
	`SYS_HEX0_ADDR: begin
		data_to_hex[3:0] 	= dmem_data_core2mem[3:0];
		we_hex[0] 			= (dmem_we != 4'b0000) ? 1'b1 : 1'b0;
	end
	`SYS_HEX1_ADDR: begin
		data_to_hex[3:0] 	= dmem_data_core2mem[3:0];
		we_hex[1] 			= 1'b1;
	end
	`SYS_HEX2_ADDR: begin
		data_to_hex[3:0] 	= dmem_data_core2mem[3:0];
		we_hex[2] 			= 1'b1;
	end
	`SYS_HEX3_ADDR: begin
		data_to_hex[3:0] 	= dmem_data_core2mem[3:0];
		we_hex[3] 			= 1'b1;
	end
	endcase
end

assign dmem_data_mem2core_o = (take_data_from_outside) ? data_from_outside : dmem_data_mem2core_i;


miriscv_core Core_Module  (.clk_i(clk),
									.rst_n_i(rst_n),
									.imem_addr_o(imem_addr),
									.imem_data_i(imem_data),
									.dmem_addr_o(dmem_addr),
									.dmem_we_o(dmem_we),
									.dmem_data_i(dmem_data_mem2core_o),
									.dmem_data_o(dmem_data_core2mem));

wire dmem_we_short = (dmem_we != 4'b0) ? 1'b1 : 1'b0;
									
miriscv_ram RAM_Module (.clk_i(clk),	
								.ena_i(1'b0),							//a port is instruction port
								.wea_i(1'b0),
								.aa_i(imem_addr),
								.bwea_i(4'b0),
								.da_i(32'b0),
								.qa_o(imem_data),
								.enb_i(1'b1),							//b port is dmem port
								.web_i(dmem_we_short),
								.ab_i(dmem_addr),
								.bweb_i(dmem_we),
								.db_i(dmem_data_core2mem),
								.qb_o(dmem_data_mem2core_i));

endmodule 