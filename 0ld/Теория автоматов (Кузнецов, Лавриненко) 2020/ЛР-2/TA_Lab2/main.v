module main(input clk,
			input Kl,
			input Dl,
			output R,
			output Y,
			output G,
			output r,
			output y,
			output g,
			output Z);
	
	wire cout_K, cout_D, cout_Z;
	counter #(.END_CNTR(30)) counter_K(
					  .sclr(~K),
					  .clock(clk),
					  .cout(cout_K));
	counter #(.END_CNTR(15)) counter_D(
					  .sclr(~D),
					  .clock(clk),
					  .cout(cout_D));
	counter #(.END_CNTR(5)) counter_Z(
					  .sclr(~Z),
					  .clock(clk),
					  .cout(cout_Z));
					  
	TA_Lab2 Automat(.K(K), .D(D), .Z(Z),
					.R(R), .Y(Y), .G(G),
					.r(r), .y(y), .g(g));
					
	srff trig_K(.s(Kl), .clk(clk), .r(cout_K), .q(K));
	srff trig_D(.s(Dl), .clk(clk), .r(cout_D), .q(D));
	wire temp;
	dff d_trig_Z(.d(y | Y), .clk(clk), .q(temp));
	srff trig_Z(.s(temp & (y | Y)), .clk(clk), .r(cout_Z), .q(Z));

endmodule