transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+C:/Users/podus/TA_Lab2 {C:/Users/podus/TA_Lab2/counter.v}
vlog -vlog01compat -work work +incdir+C:/Users/podus/TA_Lab2 {C:/Users/podus/TA_Lab2/TA_Lab2.v}
vlog -vlog01compat -work work +incdir+C:/Users/podus/TA_Lab2 {C:/Users/podus/TA_Lab2/main.v}

