library verilog;
use verilog.vl_types.all;
entity TA_Lab2 is
    port(
        clk             : in     vl_logic;
        K               : in     vl_logic;
        D               : in     vl_logic;
        Z               : in     vl_logic;
        D0              : out    vl_logic;
        D1              : out    vl_logic;
        \R\             : out    vl_logic;
        \Y\             : out    vl_logic;
        \G\             : out    vl_logic;
        r               : out    vl_logic;
        y               : out    vl_logic;
        g               : out    vl_logic
    );
end TA_Lab2;
