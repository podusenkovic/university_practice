library verilog;
use verilog.vl_types.all;
entity counter is
    generic(
        END_CNTR        : integer := 5
    );
    port(
        sclr            : in     vl_logic;
        clock           : in     vl_logic;
        q               : out    vl_logic_vector(4 downto 0);
        cout            : out    vl_logic
    );
end counter;
