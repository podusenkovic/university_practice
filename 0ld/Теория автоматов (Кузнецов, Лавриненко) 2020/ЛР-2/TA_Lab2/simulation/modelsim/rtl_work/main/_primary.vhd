library verilog;
use verilog.vl_types.all;
entity main is
    port(
        clk             : in     vl_logic;
        Kl              : in     vl_logic;
        Dl              : in     vl_logic;
        \R\             : out    vl_logic;
        \Y\             : out    vl_logic;
        \G\             : out    vl_logic;
        r               : out    vl_logic;
        y               : out    vl_logic;
        g               : out    vl_logic;
        Z               : out    vl_logic
    );
end main;
