module TA_Lab2  (input clk,
				 input K,
				 input D,
				 input Z,
				 output D0,
				 output D1,
				 output R,
				 output Y,
				 output G,
				 output r,
				 output y,
				 output g);

reg Q1 = 1'b0;
reg Q2 = 1'b0;

assign D0 = (~K & Q1) | 
			(~D & Q1) | 
			(~K & Z) | 
			(~D & Z);

assign D1 = (~K & Z & ~Q2) | 
			(~D & Z & ~Q2) | 
			(~K & Z) | 
			(~D & Z);

assign R =  (~K & Z & ~Q2) | 
			(~D & Z & ~Q2) | 
			(~K & Z) | 
			(~D & Z); // or just = D1
			
assign Y =  (K & ~Z & Q1) | 
			(~D & ~Z & Q1) | 
			(~K & Q1 & Q2) | 
			(~D & Q1 & Q2);

assign G =  (K & D & Z & ~Q1 & ~Q2) | 
			(K & D & Z & Q1 & Q2);		

assign r =  (K & D & Z & ~Q1 & ~Q2) | 
			(~D & ~Z & Q1) | 
			(Z & Q1 & Q2) | 
			(~K & ~Z & Q1);
			
assign y =  (K & D & ~Q1 & Q2) | 
			(~Z & ~Q1);	
			
assign g =  (~K & Z & ~Q1) | 
			(~D & Z & ~Q1) | 
			(~K & Z & ~Q2) | 
			(~D & Z & ~Q2);							

always @(posedge clk) begin
	Q1 <= D0;
	Q2 <= D1;
end			
			
endmodule