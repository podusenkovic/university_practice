`timescale 1ns/1ps
module TA_Lab2_tb();

reg clock;
reg Kl;
reg Dl;
wire R;
wire Y;
wire G;
wire r;
wire y;
wire g;
		
main	DUT(.clk(clock),
			.Kl(Kl),
			.Dl(Dl),
			.R(R),
			.Y(Y),
			.G(G),
			.r(r),
			.y(y),
			.g(g),
			.Z(Z));
					
		reg[20:0] echoTime = 20'd10000;
		
		initial begin
			Kl = 0;
			Dl = 0;
		end
		
		initial begin
			clock = 1'b1;
			forever #10 clock = !clock;
		end
		
		initial begin
			Kl = 1;
			#100;
			Kl = 0;
			#10000500;
		end
		
endmodule