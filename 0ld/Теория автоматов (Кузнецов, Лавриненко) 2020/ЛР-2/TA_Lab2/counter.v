module counter #(parameter END_CNTR = 5) (
			   input sclr,
			   input clock,
			   output reg [4:0]q,
			   output cout);

assign cout = (q == 0 ? 0 : 1);

always @(posedge clock) begin
	if (sclr)
		q <= 5'b0;
	else if (q < END_CNTR)
		q <= q + 1'b1;
	else q <= 5'b0;
end

endmodule