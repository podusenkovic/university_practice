%import matplotlib.pyplot as plt
%import numpy as np
clear, clc
hold on, grid on

MSPS = 1024E+3;  % ������� ������������� ���
%PI = np.pi;
PI = pi;
t = [0:1 / MSPS:1];
Freq = MSPS / 8;  % �������� ������� ��������� (�������� �� �������� ��)
Amp = 0.9;  % ��������� �������
initialSignal = Amp * sin(2 * PI * Freq * t);  % ���������� �������-������� ����������

n = 16;  % ����������� ���������� ���
% �������� ������ ���������� ���
digitalSignalWithoutDistortion = round((initialSignal * (2 ^ n)));

% ���������� ������������
INL = 2.2E-6;  % +-1.1 ppm of FSR
DNL = 0.6E-6;  % +-0.3 ppm of FSR

helpArray = [0:1 / (2 ^ n):1 - 1 / (2 ^ n)];  % ��� ����� � ��������
array1 = [0:2 ^ n - 1];
% ��� �������� ���� ������������ ������������
array2 = sin(helpArray * PI) * INL;
% ��������� ���������������� ������������
array2 = array2 + (rand(1000,1) * DNL);
array2 = array2 - DNL / 2;
% ���������� ������� � ������� ��������
array2 = array2 + 1;
% �������� ���������� ������������ ��������������
nonLinearChar = array2 .* array1;

%plot(helpArray, nonLinearChar, 'b') %����� �� ����� ������������
initialSignalWithDistortion = zeros(length(initialSignal), 1);
% ���������� ������ ����������� ����� ���
% initialSignalWithDistortion = np.array([nonLinearChar[i] for i in digitalSignalWithoutDistortion])
for i = 1:length(digitalSignalWithoutDistortion)
    initialSignalWithDistortion(i) = nonLinearChar(digitalSignalWithoutDistortion(i) + 1);
end
plot(initialSignal, 'r.');
plot(initialSignalWithDistortion, 'b.');
% �������� ������ ������� ������������ ����� ���
%digitalSignalWithDistortion = (initialSignalWithDistortion * (2 ^ n)).astype(int);

% �������� �������
%fftSignalWithout = np.fft.fft(digitalSignalWithoutDistortion, 2048) % ��� ������� ��� ������������
%fftSignalWith = np.fft.fft(digitalSignalWithDistortion, 2048)       % ��� ������� � �������������

% ������� �������� �������� ��������� � ������������ ���
%plt.plot(range(1,2048), abs(fftSignalWith[range(1,2048)]), 'b')
%plt.plot(range(1,2048), abs(fftSignalWithout[range(1,2048)]), 'r')
%plt.show()
