import matplotlib.pyplot as plt
import numpy as np

MSPS = 1024E+3; # частота дискретизации АЦП
PI = np.pi;
t = np.arange(0,1,1/MSPS)
Freqs = [150E+3, 400E+3, 550E+3, 700e+3, 1000e+3]; # частоты каждой синусоиды
Sins = [0] * 5 # массив для синусоид

for i in range(5):
    Sins[i] = np.sin(2*PI*Freqs[i]*t);  # заполнение синусов значениями
    
signal = sum(Sins)                      # итоговый сигнал
fft_signal = np.fft.fft(signal);        # БПФ сигнала

plt.plot(range(1,int(MSPS/2),1), abs(fft_signal[range(1,int(MSPS/2),1)]), 'r')
                    # основной график АЧХ

bad_freq1 = 24 * pow(10,3);     # найденные по
bad_freq2 = 324 * pow(10,3);    # основному графику
bad_freq3 = 474 * pow(10,3);    # ложные частоты
plt.plot(bad_freq1, abs(fft_signal[bad_freq1]), 'bo') # отметки
plt.plot(bad_freq2, abs(fft_signal[bad_freq2]), 'bo') # ложных
plt.plot(bad_freq3, abs(fft_signal[bad_freq3]), 'bo') # частоты
 
plt.show()
