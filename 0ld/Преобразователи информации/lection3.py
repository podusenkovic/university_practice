import matplotlib.pyplot as plt
import numpy as np

MSPS = 1024E+3  # частота дискретизации АЦП
PI = np.pi
t = np.arange(0, 1, 1 / MSPS)
Freq = MSPS / 8  # четверть частоты Найквиста (четверть от половины ЧД)
Amp = 0.9  # амплитуда сигнала
initialSignal = Amp * np.sin(2 * PI * Freq * t) # заполнение массива-сигнала значениями

n = 24  # разрядность выбранного АЦП
# цифровой сигнал идеального АЦП
digitalSignalWithoutDistortion = (initialSignal * (2 ** n)).astype(int)

# реализация нелинейности
INL = 2.2E-6  # +-1.1 ppm of FSR
DNL = 0.6E-6  # +-0.3 ppm of FSR

helpArray = np.arange(0, 1, 1 / (2 ** n))  # для гОрба и графиков
array1 = np.arange(0, 2 ** n)

# тут получаем гОрб интегральной нелинейности
array2 = np.sin(helpArray * PI) * INL
# добавляем дифференциальную нелинейность
array2 += (np.random.sample(helpArray.size) * DNL) - DNL / 2
# прибавляем единицу к каждому элементу
array2 += [1] * helpArray.size
# получаем нелинейную передаточную характеристику
nonLinearChar = array2 * array1

"""#окошко для вывода на экран графика нелинейности
plt.figure(0)
plt.title('Нелинейная характеристика')
plt.plot(helpArray, nonLinearChar, 'b')"""

# дискретный сигнал пропущенный через НПХ
# !!!ОБЪЯСНЕНИЕ МАГИИ В СОЗДАНИИ МАССИВА!!!
# из-за того, что в синусе есть нижние гОрбы, которые находятся в отрицательной области и
# симметричны верхнему горбу (каждый второй такой), то
# при необходимости взять значение нелинейности из отрицательной области,
# я беру симметричное ему в положительной (abs(i))
# и умножаю на минус (знак числа), чтобы получить отрицательную область
initialSignalWithDistortion = np.array([nonLinearChar[abs(i)] * np.sign(i) for i in digitalSignalWithoutDistortion])
# делим на 2**n, чтобы амплитуду неидеального сигнала вернуть к нормальной (~0,9)
initialSignalWithDistortion = initialSignalWithDistortion / (2 ** n)
# оцифровка сигнала пропущенного через НПХ
digitalSignalWithDistortion = (initialSignalWithDistortion * (2 ** n)).astype(int)

# окошко с цифровыми сигналами
plt.figure(1)
plt.title('Цифровые сигналы')
plt.legend(('Сигнал без нелинейности', 'Сигнал c нелинейностью'))
plt.plot(digitalSignalWithoutDistortion, 'r')
plt.plot(digitalSignalWithDistortion, 'b')


# используя БПФ, получаем спектры цифровых сигналов
fftSignalWithout = np.fft.fft(digitalSignalWithoutDistortion) # БПФ сигнала без нелинейности
fftSignalWith = np.fft.fft(digitalSignalWithDistortion)       # БПФ сигнала с нелинейностью

# окошко для графиков спектров сигналов идельного и неидеального АЦП
plt.figure(2)
plt.title('Спектры сигналов идеального и неидельного АЦП')
plt.plot(abs(fftSignalWith[range(int(MSPS // 2))]), 'b')
plt.plot(abs(fftSignalWithout[range(int(MSPS // 2))]), 'r--')
plt.legend(('Сигнал без нелинейности', 'Сигнал c нелинейностью'))
plt.show()
