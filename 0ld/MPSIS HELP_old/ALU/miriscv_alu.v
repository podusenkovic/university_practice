`include "miriscv_defines.v"

module miriscv_alu
(
  input  [`ALU_OP_WIDTH-1:0] operator_i,
  input  [31:0]              operand_a_i,
  input  [31:0]              operand_b_i,

  output reg [31:0]              result_o,
  output reg                     comparison_result_o
);

	always@(*) begin
		comparison_result_o = 0;
		casez (operator_i)
			`ALU_ADD  : result_o <= operand_a_i  +  operand_b_i;
			`ALU_SUB  :	result_o <= operand_a_i  -  operand_b_i;
			
			//			Logical
			`ALU_XOR  : result_o <= operand_a_i  ^  operand_b_i;	
			`ALU_OR   : result_o <= operand_a_i  |  operand_b_i;
			`ALU_AND  :	result_o <= operand_a_i  &  operand_b_i;
							
			// 		Shifts
			`ALU_SRA  : result_o <= $signed(operand_a_i)  >>> operand_b_i[4:0];
			`ALU_SRL  : result_o <= operand_a_i  >> operand_b_i[4:0];
			`ALU_SLL  : result_o <= operand_a_i  << operand_b_i[4:0];
							
			// 		Comparisons
			`ALU_LTS  : begin 
								result_o <= ($signed(operand_a_i) < $signed(operand_b_i));
								comparison_result_o <= result_o;
							end
			`ALU_LTU  : begin 
								result_o <= (operand_a_i < operand_b_i);
								comparison_result_o <= result_o;
							end
			`ALU_GES  : begin 
								result_o <= ($signed(operand_a_i) >= $signed(operand_b_i));
								comparison_result_o <= result_o;
							end
			`ALU_GEU  : begin 
								result_o <= (operand_a_i >= operand_b_i);
								comparison_result_o <= result_o;
							end
			`ALU_EQ   : begin 
								result_o <= (operand_a_i == operand_b_i);
								comparison_result_o <= result_o;
							end
			`ALU_NE   : begin 
								result_o <= (operand_a_i != operand_b_i);
								comparison_result_o <= result_o;
							end
			// 		Set Lower Than operations
			`ALU_SLTS : begin 
								result_o <= ($signed(operand_a_i) < $signed(operand_b_i));
								comparison_result_o <= result_o;
							end
			`ALU_SLTU : begin 
								result_o <= (operand_a_i < operand_b_i);
								comparison_result_o <= result_o;
							end
			endcase
	end
endmodule 