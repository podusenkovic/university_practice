`timescale 1ns / 1ps

`include "miriscv_defines.v"

module tb_miriscv_alu();

parameter TEST_VALUES     = 10000;
parameter TIME_OPERATION  = 100;


wire [5:0]               operator_i;
wire [31:0]              operand_a_i;
wire [31:0]              operand_b_i;

wire [31:0]              result_o;
wire                     comparison_result_o;

miriscv_alu DUT
(
  .operator_i(operator_i),
  .operand_a_i(operand_a_i),
  .operand_b_i(operand_b_i),

  .result_o(result_o),
  .comparison_result_o(comparison_result_o)
);

integer     i, err_count = 0;
integer     file_dump_test = 0;
reg [8*9:1] operator_type;

wire [31:0] result_dump;
wire        comparison_result_dump;

reg [103:0] line_dump = 0;

assign operator_i             = line_dump[103:98];
assign comparison_result_dump = line_dump[97];
assign operand_a_i            = line_dump[95:64];
assign operand_b_i            = line_dump[63:32];
assign result_dump            = line_dump[31:0];

initial
  begin
    file_dump_test = $fopen("miriscv_test_data.txt" , "r");
    
    if ( file_dump_test == 0 )
      begin
        $display("ERROR files has not opened");
        $finish;
      end
    
    $display( "Start test: ");
    
    for ( i = 0; i < TEST_VALUES; i = i + 1 )
	  begin
        $fscanf( file_dump_test    , "%h\n", line_dump);
        #TIME_OPERATION;
        if( (result_dump != result_o) || (comparison_result_dump != comparison_result_o) ) begin
          $display("ERROR Operator: %s", operator_type, " operand_A: %h", operand_a_i, " operand_B: %h", operand_b_i, " result_o: %h", result_o, " result_dump: %h", result_dump, " comparison_result_o: %h", comparison_result_o, " comparison_result_dump: %h", comparison_result_dump);
          err_count = err_count + 1'b1;
        end        
      end
    
    $fclose( file_dump_test );
    
    $display("Number of errors: %d", err_count);
    
    //$finish;
  end
  
always @(*) begin
  case(operator_i)
    `ALU_ADD  : operator_type = "ALU_ADD  ";
    `ALU_SUB  : operator_type = "ALU_SUB  ";
    `ALU_XOR  : operator_type = "ALU_XOR  ";
    `ALU_OR   : operator_type = "ALU_OR   ";
    `ALU_AND  : operator_type = "ALU_AND  ";
    `ALU_SRA  : operator_type = "ALU_SRA  ";
    `ALU_SRL  : operator_type = "ALU_SRL  ";
    `ALU_SLL  : operator_type = "ALU_SLL  ";
    `ALU_LTS  : operator_type = "ALU_LTS  ";
    `ALU_LTU  : operator_type = "ALU_LTU  ";
    `ALU_GES  : operator_type = "ALU_GES  ";
    `ALU_GEU  : operator_type = "ALU_GEU  ";
    `ALU_EQ   : operator_type = "ALU_EQ   ";
    `ALU_NE   : operator_type = "ALU_NE   ";
    `ALU_SLTS : operator_type = "ALU_SLTS ";
    `ALU_SLTU : operator_type = "ALU_SLTU ";
    default   : operator_type = "NOP      ";
  endcase
end
  

endmodule
