`include "miriscv_defines.v"

module miriscv_cu(
	input clk_i,
	input rst_n_i,

	output illegal_insn_o,

	// ALU signals
	input alu_comparison_result_i,
	input [31:0] alu_result_i,

	output [`ALU_OP_WIDTH-1:0] alu_operator_o,
	output [2:0] alu_op_a_mux_sel_o,
	output [2:0] alu_op_b_mux_sel_o,

	output [3:0] imm_a_mux_sel_o,
	output [3:0] imm_b_mux_sel_o,

	// regfile
	output regfile_we_o,


	// instructions memory
	input [31:0] instr_rdata_i,
	output [31:0] instr_raddr_o,

	// data memory
	output data_req_o,
	output data_we_o,
	output [1:0] data_type_o,
	output data_sign_extension_o,
	output [1:0] data_reg_offset_o
	);









wire jump_in_id;
wire branch_in_id;



miriscv_decoder decoder
(

  .illegal_insn_o(illegal_insn_o),          // illegal instruction encountered

  // from IF/ID pipeline
  .instr_rdata_i(instr_rdata_i),           // instruction read from instr memory/cache

  // ALU signals
  .alu_operator_o(alu_operator_o), // ALU operation selection
  .alu_op_a_mux_sel_o(alu_op_a_mux_sel_o),      // operand a selection: reg value, PC, immediate or zero
  .alu_op_b_mux_sel_o(alu_op_b_mux_sel_o),      // oNOperand b selection: reg value or immediate

  .imm_a_mux_sel_o(imm_a_mux_sel_o),         // immediate selection for operand a
  .imm_b_mux_sel_o(imm_b_mux_sel_o),         // immediate selection for operand b


  // register file related signals
  .regfile_we_o(regfile_we_o),            // write enable for regfile

  // LD/ST unit signals
  .data_req_o(data_req_o),              // start transaction to data memory
  .data_we_o(data_we_o),               // data memory write enable
  .data_type_o(data_type_o),             // data type on data memory: byte, half word or word
  .data_sign_extension_o(data_sign_extension_o),   // sign extension on read data from data memory
  .data_reg_offset_o(data_reg_offset_o),       // offset in byte inside register for stores

  // jump/branches
  .jump_in_id_o   (jump_in_id),            // jump is being calculated in ALU
  .branch_in_id_o (branch_in_id)
);





reg [31:0] program_counter;

always @(posedge clk_i or negedge rst_n_i) begin
	if (~rst_n_i)
		program_counter <= `RESET_VECTOR;
	else if (jump_in_id)
		program_counter <= alu_result_i;
	else if (branch_in_id && alu_comparison_result_i)
		program_counter <= alu_result_i;
	else
		program_counter <= program_counter + 32'h00000004;
end



assign instr_raddr_o = program_counter;




endmodule