`include "miriscv_defines.v"

module miriscv_alu
(
  input  [`ALU_OP_WIDTH-1:0]  operator_i,
  input  [31:0]              operand_a_i,
  input  [31:0]              operand_b_i,

  output reg [31:0]              result_o,
  output reg                     comparison_result_o
);

	always@(*) begin
		comparison_result_o = 0;
		case (operator_i)
			`ALU_ADD  : result_o = $signed(operand_a_i)  +  $signed(operand_b_i);
			`ALU_SUB  :	result_o = $signed(operand_a_i)  -  $signed(operand_b_i);
			
			//			Logical
			`ALU_XOR  : result_o = operand_a_i  ^  operand_b_i;	
			`ALU_OR   : result_o = operand_a_i  |  operand_b_i;
			`ALU_AND  :	result_o = operand_a_i  &  operand_b_i;
							
			// 		Shifts
			`ALU_SRA  : result_o = $signed(operand_a_i)  >>> operand_b_i[4:0];
			`ALU_SRL  : result_o = operand_a_i  >> operand_b_i[4:0];
			`ALU_SLL  : result_o = operand_a_i  << operand_b_i[4:0];
							
			// 		Comparisons
			`ALU_LTS  : comparison_result_o <= (result_o = ($signed(operand_a_i) < $signed(operand_b_i)) ? 1 : 0);
			`ALU_LTU  : {comparison_result_o, result_o} = (operand_a_i < operand_b_i) ? 1 : 0;
			`ALU_GES  : {comparison_result_o, result_o} = ($signed(operand_a_i) >= $signed(operand_b_i)) ? 1 : 0;
			`ALU_GEU  : {comparison_result_o, result_o} = (operand_a_i >= operand_b_i) ? 1 : 0;
			`ALU_EQ   : {comparison_result_o, result_o} = (operand_a_i == operand_b_i) ? 1 : 0;
			`ALU_NE   : {comparison_result_o, result_o} = (operand_a_i != operand_b_i) ? 1 : 0;
			// 		Set Lower Than operations
			`ALU_SLTS : {comparison_result_o, result_o} = ($signed(operand_a_i) < $signed(operand_b_i)) ? 1 : 0;
			`ALU_SLTU : {comparison_result_o, result_o} = (operand_a_i < operand_b_i) ? 1 : 0;
		endcase
	end



endmodule 