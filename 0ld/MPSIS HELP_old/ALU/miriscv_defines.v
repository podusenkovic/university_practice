`define RESET_VECTOR 32'h00000000



`define ALU_OP_WIDTH  6

`define ALU_ADD   6'b011000
`define ALU_SUB   6'b011001

`define ALU_XOR   6'b101111
`define ALU_OR    6'b101110
`define ALU_AND   6'b010101

// Shifts
`define ALU_SRA   6'b100100
`define ALU_SRL   6'b100101
`define ALU_SLL   6'b100111

// Comparisons
`define ALU_LTS   6'b000000
`define ALU_LTU   6'b000001
`define ALU_GES   6'b001010
`define ALU_GEU   6'b001011
`define ALU_EQ    6'b001100
`define ALU_NE    6'b001101

// Set Lower Than operations
`define ALU_SLTS  6'b000010
`define ALU_SLTU  6'b000011


`define OPCODE_SYSTEM   7'h73
`define OPCODE_FENCE    7'h0f
`define OPCODE_OP       7'h33
`define OPCODE_OPIMM    7'h13
`define OPCODE_STORE    7'h23
`define OPCODE_LOAD     7'h03
`define OPCODE_BRANCH   7'h63
`define OPCODE_JALR     7'h67
`define OPCODE_JAL      7'h6f
`define OPCODE_AUIPC    7'h17
`define OPCODE_LUI      7'h37




// forwarding operand mux
`define SEL_REGFILE      2'b00
`define SEL_FW_EX        2'b01
`define SEL_FW_WB        2'b10
`define SEL_MISALIGNED   2'b11

// operand a selection
`define OP_A_REGA_OR_FWD 3'b000
`define OP_A_CURRPC      3'b001
`define OP_A_IMM         3'b010
`define OP_A_REGB_OR_FWD 3'b011
`define OP_A_REGC_OR_FWD 3'b100

// immediate a selection
`define IMMA_Z      1'b0
`define IMMA_ZERO   1'b1

// operand b selection
`define OP_B_REGB_OR_FWD 3'b000
`define OP_B_REGC_OR_FWD 3'b001
`define OP_B_IMM         3'b010
`define OP_B_REGA_OR_FWD 3'b011
`define OP_B_BMASK       3'b100
`define OP_B_ZERO        3'b101

// immediate b selection
`define IMMB_I      4'b0000
`define IMMB_S      4'b0001
`define IMMB_U      4'b0010
`define IMMB_PCINCR 4'b0011
`define IMMB_S2     4'b0100
`define IMMB_S3     4'b0101
`define IMMB_VS     4'b0110
`define IMMB_VU     4'b0111
`define IMMB_SHUF   4'b1000
`define IMMB_CLIP   4'b1001
`define IMMB_BI     4'b1011
`define IMMB_UJ	  4'b1100
`define IMMB_SB	  4'b1101