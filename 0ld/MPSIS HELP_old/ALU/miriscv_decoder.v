`include "miriscv_defines.v"

module miriscv_decoder (output 				illegal_insn_o,
								input  [31:0] 		instr_rdata_i,
								output [5:0] 		alu_operator_o,
								output [2:0] 		alu_op_a_mux_sel_o,
								output [2:0]		alu_op_b_mux_sel_o,
								output [3:0]		imm_a_mux_sel_o,
								output [3:0] 		imm_b_mux_sel_o,
								output 				regfile_we_o,
								output 				data_req_o,
								output 				data_we_o,
								output [1:0]		data_type_o,
								output 				data_sign_extension_o,
								output [1:0]		data_reg_offset_o,
								output 				jump_in_id_o,
								output 				branch_in_id_o);
	
	
	always@(*) begin
		//------------------START_R-TYPE----------------------
		if (instr_rdata_i[6:0] == `OPCODE_OP/*7b'0110011*/) begin
			alu_op_a_mux_sel_o <= `OP_A_REGA_OR_FWD; 
			alu_op_b_mux_sel_o <= `OP_B_REGA_OR_FWD;
			if(instr_rdata_i[31:25] == 7b'0000000)
				case(instr_rdata_i[14:12])
					3b'000: alu_operator_o <= `ALU_ADD;
					3b'001: alu_operator_o <= `ALU_SLL;
					3b'010: alu_operator_o <= `ALU_SLT;
					3b'011: alu_operator_o <= `ALU_SLTU;
					3b'100: alu_operator_o <= `ALU_XOR;
					3b'101: alu_operator_o <= `ALU_SRL;
					3b'110: alu_operator_o <= `ALU_OR;
					3b'111: alu_operator_o <= `ALU_AND;
				endcase
			else case (instr_rdata_i[14:12])
				3b'000: alu_operator_o <= `ALU_SUB;
				3b'101: alu_operator_o <= `ALU_SRA;
			endcase 
		end
		//------------------END_R-TYPE----------------------
		else
		//------------------START_I-TYPE----------------------
		if (instr_rdata_i[6:0] == 7b'0010011) begin
			alu_op_a_mux_sel_o <= `OP_A_REGA_OR_FWD; 
			alu_op_b_mux_sel_o <= `OP_B_IMM;
			case(instr_rdata_i[14:12])
				3b'000: alu_operator_o <= `ALU_ADD;
				3b'001: alu_operator_o <= `ALU_SLL;
				3b'010: alu_operator_o <= `ALU_SLT;
				3b'011: alu_operator_o <= `ALU_SLTU;
				3b'100: alu_operator_o <= `ALU_XOR;
				3b'101: alu_operator_o <= (instr_rdata_i[31:25] == 'b0) ? `ALU_SRL : `ALU_SRA;
				3b'110: alu_operator_o <= `ALU_OR;
				3b'111: alu_operator_o <= `ALU_AND;
			endcase
		end
		if (instr_rdata_i[6:0] == 7b'0000011) begin
			alu_op_a_mux_sel_o <= `OP_A_REGA_OR_FWD; 
			alu_op_b_mux_sel_o <= `OP_B_IMM;
			imm_b_mux_sel_o <= IMMB_I; //?!?!?!?!?!
			case(instr_rdata_i[14:12])
				3b'000: alu_operator_o <= `ALU_ADD;
				3b'001: alu_operator_o <= `ALU_SLL;
				3b'010: alu_operator_o <= `ALU_SLT;
				3b'100: alu_operator_o <= `ALU_XOR;
				3b'101: alu_operator_o <= (instr_rdata_i[31:25] == 'b0) ? `ALU_SRL : `ALU_SRA;
			endcase
		end
		//------------------END_I-TYPE----------------------
	end
								
								
		
	
	
								
								
								
								
endmodule 