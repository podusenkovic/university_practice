library verilog;
use verilog.vl_types.all;
entity tb_miriscv_alu is
    generic(
        TEST_VALUES     : integer := 10000;
        TIME_OPERATION  : integer := 100
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of TEST_VALUES : constant is 1;
    attribute mti_svvh_generic_type of TIME_OPERATION : constant is 1;
end tb_miriscv_alu;
