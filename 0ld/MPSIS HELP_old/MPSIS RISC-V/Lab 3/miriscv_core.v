`include "miriscv_defines.v"

`timescale 1ns/1ps


module miriscv_core
(
  input                       clk_i,
  input                       rst_i,

  input   [31:0]              idata_i,
  output  [31:0]              iaddr_o,

  output  [31:0]              ddata_0,
  input   [31:0]              ddata_i,
  output  [31:0]              daddr_o
);