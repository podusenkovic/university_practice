/********************************************************************************
** Form generated from reading UI file 'host.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HOST_H
#define UI_HOST_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Host
{
public:
    QLabel *label;
    QLabel *labelForData;

    void setupUi(QWidget *Host)
    {
        if (Host->objectName().isEmpty())
            Host->setObjectName(QString::fromUtf8("Host"));
        Host->resize(291, 306);
        label = new QLabel(Host);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(0, 20, 291, 111));
        label->setAlignment(Qt::AlignCenter);
        labelForData = new QLabel(Host);
        labelForData->setObjectName(QString::fromUtf8("labelForData"));
        labelForData->setGeometry(QRect(0, 160, 291, 111));
        labelForData->setAlignment(Qt::AlignCenter);

        retranslateUi(Host);

        QMetaObject::connectSlotsByName(Host);
    } // setupUi

    void retranslateUi(QWidget *Host)
    {
        Host->setWindowTitle(QApplication::translate("Host", "Server", nullptr));
        label->setText(QApplication::translate("Host", "TextLabel", nullptr));
        labelForData->setText(QApplication::translate("Host", "TextLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Host: public Ui_Host {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HOST_H
