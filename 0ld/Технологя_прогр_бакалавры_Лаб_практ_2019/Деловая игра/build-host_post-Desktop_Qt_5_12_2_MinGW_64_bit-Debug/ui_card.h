/********************************************************************************
** Form generated from reading UI file 'card.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CARD_H
#define UI_CARD_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_Card
{
public:
    QLineEdit *line_edit_number;
    QLineEdit *line_edit_name;
    QLineEdit *line_edit_date;
    QLineEdit *line_edit_cvv;
    QLabel *label_num;
    QLabel *label_name;
    QLabel *label_date;
    QLabel *label_cvv;
    QPushButton *button_ok;
    QPushButton *button_clear;
    QPushButton *button_close;
    QLabel *label_card_number;
    QLabel *label_card_valid_thru;
    QLabel *label_card_date;
    QLabel *label_card_name;

    void setupUi(QDialog *Card)
    {
        if (Card->objectName().isEmpty())
            Card->setObjectName(QString::fromUtf8("Card"));
        Card->resize(398, 175);
        Card->setAutoFillBackground(true);
        line_edit_number = new QLineEdit(Card);
        line_edit_number->setObjectName(QString::fromUtf8("line_edit_number"));
        line_edit_number->setGeometry(QRect(110, 20, 261, 21));
        line_edit_name = new QLineEdit(Card);
        line_edit_name->setObjectName(QString::fromUtf8("line_edit_name"));
        line_edit_name->setGeometry(QRect(110, 50, 261, 21));
        line_edit_date = new QLineEdit(Card);
        line_edit_date->setObjectName(QString::fromUtf8("line_edit_date"));
        line_edit_date->setGeometry(QRect(110, 80, 261, 21));
        line_edit_cvv = new QLineEdit(Card);
        line_edit_cvv->setObjectName(QString::fromUtf8("line_edit_cvv"));
        line_edit_cvv->setGeometry(QRect(110, 110, 261, 21));
        line_edit_cvv->setEchoMode(QLineEdit::Password);
        label_num = new QLabel(Card);
        label_num->setObjectName(QString::fromUtf8("label_num"));
        label_num->setGeometry(QRect(10, 20, 91, 16));
        label_num->setAlignment(Qt::AlignCenter);
        label_name = new QLabel(Card);
        label_name->setObjectName(QString::fromUtf8("label_name"));
        label_name->setGeometry(QRect(10, 50, 91, 16));
        label_name->setAlignment(Qt::AlignCenter);
        label_date = new QLabel(Card);
        label_date->setObjectName(QString::fromUtf8("label_date"));
        label_date->setGeometry(QRect(10, 80, 91, 16));
        label_date->setAlignment(Qt::AlignCenter);
        label_cvv = new QLabel(Card);
        label_cvv->setObjectName(QString::fromUtf8("label_cvv"));
        label_cvv->setGeometry(QRect(10, 110, 91, 16));
        label_cvv->setAlignment(Qt::AlignCenter);
        button_ok = new QPushButton(Card);
        button_ok->setObjectName(QString::fromUtf8("button_ok"));
        button_ok->setGeometry(QRect(110, 140, 80, 21));
        button_clear = new QPushButton(Card);
        button_clear->setObjectName(QString::fromUtf8("button_clear"));
        button_clear->setGeometry(QRect(210, 140, 80, 21));
        button_close = new QPushButton(Card);
        button_close->setObjectName(QString::fromUtf8("button_close"));
        button_close->setGeometry(QRect(310, 140, 80, 21));
        label_card_number = new QLabel(Card);
        label_card_number->setObjectName(QString::fromUtf8("label_card_number"));
        label_card_number->setEnabled(false);
        label_card_number->setGeometry(QRect(20, 20, 361, 51));
        QFont font;
        font.setFamily(QString::fromUtf8("OCR A Extended"));
        font.setPointSize(24);
        label_card_number->setFont(font);
        label_card_valid_thru = new QLabel(Card);
        label_card_valid_thru->setObjectName(QString::fromUtf8("label_card_valid_thru"));
        label_card_valid_thru->setEnabled(false);
        label_card_valid_thru->setGeometry(QRect(190, 65, 31, 31));
        label_card_date = new QLabel(Card);
        label_card_date->setObjectName(QString::fromUtf8("label_card_date"));
        label_card_date->setEnabled(false);
        label_card_date->setGeometry(QRect(220, 70, 81, 21));
        QFont font1;
        font1.setFamily(QString::fromUtf8("OCR A Extended"));
        font1.setPointSize(20);
        label_card_date->setFont(font1);
        label_card_name = new QLabel(Card);
        label_card_name->setObjectName(QString::fromUtf8("label_card_name"));
        label_card_name->setEnabled(false);
        label_card_name->setGeometry(QRect(20, 100, 361, 51));
        QFont font2;
        font2.setFamily(QString::fromUtf8("NSimSun"));
        font2.setPointSize(20);
        label_card_name->setFont(font2);
        label_card_name->raise();
        label_card_date->raise();
        label_card_number->raise();
        label_card_valid_thru->raise();
        line_edit_number->raise();
        line_edit_name->raise();
        line_edit_date->raise();
        line_edit_cvv->raise();
        label_num->raise();
        label_name->raise();
        label_date->raise();
        label_cvv->raise();
        button_ok->raise();
        button_clear->raise();
        button_close->raise();

        retranslateUi(Card);

        QMetaObject::connectSlotsByName(Card);
    } // setupUi

    void retranslateUi(QDialog *Card)
    {
        Card->setWindowTitle(QApplication::translate("Card", "Dialog", nullptr));
        label_num->setText(QApplication::translate("Card", "Card number", nullptr));
        label_name->setText(QApplication::translate("Card", "Cardholder name", nullptr));
        label_date->setText(QApplication::translate("Card", "Date expire", nullptr));
        label_cvv->setText(QApplication::translate("Card", "CVV", nullptr));
        button_ok->setText(QApplication::translate("Card", "OK", nullptr));
        button_clear->setText(QApplication::translate("Card", "Clear", nullptr));
        button_close->setText(QApplication::translate("Card", "Close", nullptr));
        label_card_number->setText(QApplication::translate("Card", "1234 1235 1235 1235", nullptr));
        label_card_valid_thru->setText(QApplication::translate("Card", "VALID\n"
"THRU", nullptr));
        label_card_date->setText(QApplication::translate("Card", "12/20", nullptr));
        label_card_name->setText(QApplication::translate("Card", "PODUSENKO VIKTOR", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Card: public Ui_Card {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CARD_H
