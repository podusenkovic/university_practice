#include <iostream>
#include <string>
#include <vector>
#include <stdint.h>
#include <algorithm>

class Wagon {
	uint8_t Number;
	uint8_t Class;
	uint16_t Seats;
	uint16_t OccupiedSeats;
public:
	Wagon(uint8_t number = 0, uint8_t _class = 0, uint16_t seats = 0, uint16_t occupiedSeats = 0) : 
		Number(number),
		Class(_class),
		Seats(seats),
		OccupiedSeats(occupiedSeats)
	{}
	void freeSeats(uint16_t amount) {
		if (amount > OccupiedSeats)
			throw 101; // Amount of passengers that gone away on station cant be more than amount of seats that has been occupied before
		OccupiedSeats -= amount;
	}
	void occupy(uint16_t amount) {
		if (amount > (Seats - OccupiedSeats))
			throw 102; // Amount of tickets that has been sold on station cant be more than amount of avaliable seats
		OccupiedSeats += amount;
	}
	uint16_t amountOfAvaliableSeats() {
		return Seats - OccupiedSeats;
	}

};

class Train {
	uint8_t Number;
	uint32_t Seats;
	std::vector<Wagon> Wagons;
public:
	Train(uint8_t number = 0) : Number(number) {
		for (int i = 0; i < rand() % 50 + 20; i++) {
			uint16_t randomed_seats = rand();
			Wagons.push_back(Wagon(rand() % 256, rand() % 256, randomed_seats, rand() % randomed_seats));
		}
	}
	Train(uint8_t number, const std::vector<Wagon> &wagons) :
		Number(number),
		Wagons(wagons)
	{}
	uint32_t amountOfAvaliableSeats() {
		uint32_t result = 0;
		std::for_each(Wagons.begin(), Wagons.end(), [&result](Wagon w) { result += w.amountOfAvaliableSeats(); });
		return result;
	}
	void freeSeats(std::vector<int> to_free) {
		for (int i = 0; i < to_free.size(); i++)
			Wagons[i].freeSeats(to_free[i]);
	}
	Wagon &getIndexWagon(int i){
		if (i >= Wagons.size())
			throw 201; // There is no wagon with this index!
		return Wagons[i];
	}
};




int main() {
	try {
		Train t1(1);
		std::cout << t1.amountOfAvaliableSeats() << std::endl;
		t1.getIndexWagon(2).freeSeats(320);
		t1.getIndexWagon(10).occupy(320);
		//t1.freeSeats(std::vector<int>(100, 10000));
	}
	catch (int e) {
		switch (e) {
		case 101: std::cout << "Amount of passengers that gone away on station cant be more than amount of seats that has been occupied before!" << std::endl; break;
		case 102: std::cout << "Amount of tickets that has been sold on station cant be more than amount of avaliable seats!" << std::endl; break;
		case 201: std::cout << "There is no wagon with this index!" << std::endl; break;
		}
	}
	return 0;
}
