#include <iostream> 
#include <math.h> 
#include <time.h> 
#include <vector> 
#include <functional>

using namespace std;

double f(double x) {
	return pow(x, 2) * 3;
}



int main() {
	srand(time(NULL));
	int N = 10E6, cnt = 0;
	// norm Monte-Carlo
	/*double rands_sum = 0;
	vector<double> coords;
	for (int i = 0; i < N; i++) {
		coords.push_back(rand() * 1.0 / RAND_MAX);
	}
	for (int i = 0; i < coords.size(); i++) {
		rands_sum += f(coords[i]);
	}
	cout << "I4 = " << rands_sum / N;*/
	
	// geometry Monte-Carlo
	/*vector<pair<double,double>> coords;
	double a = 1.0, b = 3.0;
	for (int i = 0; i < N; i++) {
		coords.push_back(pair<double,double>(rand() * a / RAND_MAX, rand() * b / RAND_MAX));
	}
	for (int i = 0; i < coords.size(); i++) {
		if (coords[i].second < f(coords[i].first))
			cnt++;
	}
	cout << "I4 = " << (a * b) * cnt / N;*/

}