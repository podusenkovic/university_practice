#include <iostream>
#include <math.h>
#include <functional>

using namespace std;

double sliceArea(function<double(double)> f, double x1, double x2, double max_slice_error) {
	// values on edges and mid
	double y1 = f(x1), y2 = f(x2);
	double xmid = (x1 + x2) / 2, ymid = f(xmid);

	// calc square big trapezoid and 2 smalls
	double sqr1To2 = (x2 - x1) * (y2 + y1) / 2;
	double sqr1ToMid = (xmid - x1) * (ymid + y1) / 2;
	double sqrMidTo2 = (x2 - xmid) * (y2 + ymid) / 2;
	
	double sqr1To2ThroughMid = sqr1ToMid + sqrMidTo2;
	double error = (sqr1To2ThroughMid - sqr1To2);
	// if error is too big, divide this area on 2 slides, and calc again
	if (abs(error) < max_slice_error)
		return sqr1To2ThroughMid;
	else return sliceArea(f, x1, xmid, max_slice_error) + sliceArea(f, xmid, x2, max_slice_error);
}

double integrateAdaptiveMidPoint(function<double(double)> f, double a, double b, int N, double max_slice_error) {
	// for small pieces
	double dx = (b - a) / N, total = 0;
	// for the whole shit
	double total_area = 0, x = a;
	for (int i = 0; i < N; i++, x += dx)
		total_area += sliceArea(f, x, x + dx, max_slice_error);
	
	return total_area;
}

int main() {
	try {
		cout << integrateAdaptiveMidPoint([](double x) { return x * x * 3; }, 0, 1, 5, 10E-2) << endl;
	}
	catch (exception e) {
		cout << e.what() << endl;
	}
}