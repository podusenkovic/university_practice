function result = polinomLagr(x, XData, YData)
    n = length(XData);
    for k = 1 : n
        resultPol(k) = YData(k).*specialPolinom(x, XData, k) ./ specialPolinom(XData(k), XData, k);
    end
    result = sum(resultPol);
end