clc, clear
x1 = 0:0.001:2*pi;
y1 = x1 .* sin(5*x1);
x2 = -2*pi:0.001:2*pi;
y2 = x2 .* sin(5*x2);
hold on, grid on, xlabel('x'), ylabel('y')
polar(x1, y1, 'r')
legend('x * sin(5*x), [0, 2pi]')
figure
hold on, grid on, xlabel('x'), ylabel('y')
polar(x2, y2, 'b')
legend('x * sin(5*x), [-2pi, 2pi]')