function [x, iter] = NewtonesMethod(xn, eps)
    f = @(x)x^3 - 3*x*x - 9*x - 5;
    df = @(x)3*x^2 - 6*x - 9;
    
    iter = 0;
    
    x = xn - f(xn)/df(xn);
    x0 = xn;
    
    while(abs(x0-x) >= eps)
        iter = iter + 1;
        x0 = x;
        x = x - f(x)/df(x);
    end
end