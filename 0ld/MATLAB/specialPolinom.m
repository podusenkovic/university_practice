function resultSpec = specialPolinom(x, XData, k)
    resultSpec = x - XData;
    resultSpec(k) = 1;
    resultSpec = prod(resultSpec);
end