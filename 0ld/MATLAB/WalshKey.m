function WK =  WalshKey(BpFunRes)
    WK = [];
    BinLength = log(length(BpFunRes)) / log(2);
    absBpFR = abs(BpFunRes); 
    t = [];
    for i = 1:length(absBpFR) 
        if(absBpFR(i)/max(absBpFR) > 0.8 )
            t = [t, i-1];
        end
    end
    for j = 0:length(t) - 1
        f = de2bi(t(j + 1));
        f = [f, zeros(1, BinLength - length(f))];
        f = f(length(f):-1:1);
        if(BpFunRes(t(j + 1) + 1) < 0)
            f = [f, 1];
        end
        WK = [WK, bi2de(f)];
    end
end
