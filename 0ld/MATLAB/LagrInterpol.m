function [maxDiff, polynom] = LagrInterpol(fun, x, y)
    maxDiff = 0;
    f = y;
    polynom = containers.Map;   
    hold on, grid on
    fplot(@(x)fun(x), [min(x) max(x)], 'red');       
    for j = 1:(length(x) - 1)
       line([x(j) x(j + 1)], [f(j) f(j + 1)]);
    end
    for m = 1:length(x)
        P = 0;
        for k = 1:length(x)
            chisl = 1;
            znam = 1;
            for j = 1:length(x)
                if (j~=k)
                    chisl = chisl * (x(m) - x(j));
                    znam = znam * (x(k) - x(j));
                end
            end            
            P = P + f(k).*(chisl./znam);
        end
        plot(x(m),P, 'ko');
    end
    xBig = min(x):1/100:max(x);
    for m = 1:length(xBig)
        P = 0;
        for k = 1:length(x)
            chisl = 1;
            znam = 1;
            for j = 1:length(x)
                if (j~=k)
                    chisl = chisl * (xBig(m) - x(j));
                    znam = znam * (x(k) - x(j));
                end
            end            
            P = P + f(k).*(chisl./znam);
        end
        polynom(num2str(xBig(m))) = P;
        plot(xBig(m),P, 'b*');
        if (abs(P - fun(xBig(m))) > maxDiff)
            maxDiff = abs(P - fun(xBig(m)));
        end
    end
end