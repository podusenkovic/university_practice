function [ind, max] = Maximum(x)
    ind = 1;
    max = abs(x(1));
    for i = 1:length(x)
        if abs(x(i)) > max
            max = abs(x(i));
            ind = i;
        end
    end
end