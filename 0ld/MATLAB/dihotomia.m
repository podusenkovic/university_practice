function [x, iter] = dihotomia(a,b,eps)
    f = @(x)x^3 - 3*x*x - 9*x - 5;
    iter = 0;
    while(b - a > eps)
        iter = iter + 1;
        c = (b + a) / 2;
        if (f(b) * f(c) <= 0)
            a = c;
        else
            b = c;        
        end
    end
    x = (a + b) / 2;
end