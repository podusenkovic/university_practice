function [yI,diff] = linInterpol(x)
    i = 0:4;
    t = i./4;
    f = sin(pi.*t);

    hold on, grid on
    fplot(@(x)sin(pi * x), [0 1], 'red');
    for j = 1:4
        line([t(j) t(j + 1)], [f(j) f(j + 1)]);
    end
    
    len = size(x);
    for j = 1:len(2)
            for k = 1:4
                if(x(j) >= t(k) && x(j) <= t(k + 1))
                    yI(j) = (x(j) - t(k)) * (f(k + 1) - f(k)) / (t(k + 1) - t(k)) + f(k);
                    yS = sin(pi * x(j));
                    plot(x(j), yI, 'ko');
                    plot(x(j), yS, 'go');
                    diff(j) = abs(yI - yS);
                    break;
                end
            end
    end
end