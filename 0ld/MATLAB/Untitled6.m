clc, clear
angle = 0:0.01:2*pi;
a = 4; b = 5;
cen_x = -3; cen_y = 2;
x = b * sin(angle) + cen_x;
y = a * cos(angle) + cen_y;
hold on, grid on, xlabel('x'), ylabel('y')
plot(x,y, 'r')
clear x y
figure
hold on, grid on, xlabel('x'), ylabel('y')
ezplot(@(x,y)((1/(b*b))*(x-cen_x)^2 + (1/(a*a))*(y-cen_y)^2 - 1), [-8, 2], [-2, 6])