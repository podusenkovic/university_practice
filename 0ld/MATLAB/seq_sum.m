function rslt = seq_sum(eps)
    N = 12;
    n = 1;
    rslt = 0;
    prev = 1 / n^(N+1);
    rslt = rslt + prev;
    while (prev > abs(eps))
        n = n + 1;
        prev = 1 / n ^(N+1);
        rslt = rslt + prev;
    end
end