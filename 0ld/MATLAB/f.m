function f()
    func = @(x)x^3 - 3*x*x - 9*x - 5;
    hold on, grid on, xlabel('x'), ylabel('y');
    fplot(func, [-10, 10]);
    
    t = 0:pi/100:2*pi;
    x = cos(t);
    y = 150*sin(t);
    
    plot(x - 1, y);
    plot(x + 5, y);
    
    p = [1, -3, -9, -5];
    r = roots(p)
end