function [xk, k] = NewtoneSolver(funs, x0, eps)
% NewtoneSolver - ������� ��� ������� ������ �� ���� ���������� ���������
% f   - ������-������� �������
% x0  - ������-������ �������� �����������
% eps - ����������� �������� ����������
% xk  - ������������ ������-������� ���������� ����������
% k   - ����� �������� ��� ��������� �������� ��������
    n = length(funs);
    values = [x0; zeros(1, n)]; % ������� ��� �������� xk � x(k+1) ��������
    k = 0;
    h = symvar(funs); % ���������� ����������, ������� ���� � ��������
    givenFuns = matlabFunction(funs); % function_handles ��� �������� ��������� ����������� �����������
    figure; hold on; grid on;
    for i = 1:n
        for j = 1:n
            dFuns(i,j) = (diff(funs(i), h(j))); % ������� ������� ����������� i-� ������� �� j-� ����������
        end
    end
    
    if (n == 2)
        for i = 1:2
            hPl = ezplot(funs(i)); % ������ ������� ������ � ������ � ����� �����������
            set(hPl, 'LineColor', 0.5 - rand(1,3) / 2, 'linewidth', 3);
        end
    end
    
    dFuns = matlabFunction(dFuns);
    
    while (1 == 1)
        if max(abs(values(1,:) - values(2,:))) < eps
            break;
        end
        values(2,:) = values(1,:);
        F = givenFuns(values(2,1), values(2,2)); % ��-�� ����� �� ����
        Fis = dFuns(values(2,1), values(2,2));   % ������� ��� ������ ���-�� ���������
        H = Fis\F;
        values(1,:) = values(2,:) - H';
        k = k + 1;
    end
    xk = values(1,:);
end