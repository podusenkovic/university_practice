A = [2 9 6 10 3 2 9 2 5 7 4 0 7 1 0 8 6 6 5 5 7 4 6 8 8 10 1 2 1 4 2 0 1 5 1 6 7 4 3 3 1 9 3 9 1 10 8 6 2 5];

sortedTable = sort(A)

Width = max(A) - min(A)

for i = min(A):max(A)
    statTable(1,i+1) = i;
    statTable(2,i+1) = sum(A==i);
    statTable(3,i+1) = statTable(2,i+1) / length(50);
end
statTable

div = min(A):Width/7:max(A)

for i = 1:length(div) - 1
    freqTable(1,i) = sum(A <= div(i + 1)) - sum(A < div(i));
    freqTable(2,i) = freqTable(1,i) / length(A);
end
freqTable

Mx = sum(A) / length(A)
Dx = sum((A.^2)) / length(A) - Mx^2

[MAX, MAXINDEX] = max(statTable(2,:));
Mode = statTable(1,MAXINDEX)
Mediana = (A(25) + A(26)) / 2

for i = 1:length(div) - 1
    MxGroup = (div(i) + Width/14) * freqTable(1,i);
end
MxGroup = MxGroup / length(A)
for i = 1:length(div) - 1
    DxGroup = (div(i) + Width/14)^2 * freqTable(1,i);
end
DxGroup = DxGroup / length(A) - MxGroup^2

[MAX, MAXINDEX] = max(freqTable(1,:))
delta = div(2) - div(1);
if (MAXINDEX ~= 1 && MAXINDEX ~= length(freqTable(1,:)))
    ModaGroup = div(MAXINDEX) + ((MAX - freqTable(1,MAXINDEX - 1))/(2*MAX - freqTable(1,MAXINDEX - 1) - freqTable(1,MAXINDEX + 1))) * delta
else 
    if (MAXINDEX == 1)
        ModaGroup = div(MAXINDEX) + ((MAX/(2*MAX - freqTable(1,MAXINDEX + 1)))) * delta
    else
        ModaGroup = div(MAXINDEX) + ((MAX - freqTable(1,MAXINDEX - 1))/(2*MAX - freqTable(1,MAXINDEX - 1))) * delta
    end
end

indexMid = length(div) / 2;
medianaGroup = div(indexMid) + ((length(A)/2 - sum(freqTable(1,1:(indexMid-1)))) / freqTable(1,indexMid)) * delta


hold on, grid on
bar(freqTable(2,:))
