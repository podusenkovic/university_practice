function [x, y] = Euler(h, x0, y0, X, f)
    x = x0:h:X;
    y(1) = y0;
    for i = 1:length(x) - 1
        y(i+1) = y(i) + h * f(x(i), y(i));
    end
end