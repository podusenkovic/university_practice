clc, clear
oscil=@(t, Y)[Y(2); -2 * Y(2) - 10 * Y(1) + sin(t)];
[T Y]=ode45(oscil, [0 20], [1 0]);
C1 = 87 / 85; C2 = 26 / 85;
F = @(t)(exp(-t).*(C1.*cos(3.*t) + C2.*sin(3.*t)) + (1/85).*(9.*sin(t) - 2.*cos(t)));
hold on, grid on
fplot(F,[0 20])
plot(T, Y(:,2))