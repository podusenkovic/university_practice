oscil = @(s, Y)[Y(2); -1./(xs + 0.01).^2];
[S Y] = ode23(oscil, [0 15], [log(0.01); 100]);

T = S + 0.01;
YA = log(T);

grid on, hold on
plot(T, Y(:, 1));
plot(T, YA);