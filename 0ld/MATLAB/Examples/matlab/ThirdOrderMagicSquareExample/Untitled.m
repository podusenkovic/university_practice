clc, clear
n = 20;
while (1 > 0)
    A = randi(10,n);
    if cond(A) > 10^3
        break;
    end
end
cond = cond(A);
X = randi(10,1,n)

if (det(A) == 0)
    error = 1
else
    f = A * X';
    
    ansRref = rref([A, f]);
    diffRref = max(ansRref(:,n + 1) - X')
    
    ansLeft = A\f;
    diffLeft = max(ansLeft - X')
    
    ansUsual = A^(-1) * f;
    diffUsual = max(ansUsual - X')
    
    for i = 1:n
        temp = A;
        temp(:, i) = f';
        ansKramer(i) = det(temp) / det(A);
    end
    diffKramer = max(ansKramer - X)
    
    ansGauss = Gauss(A, f');
    diffGauss = max(ansGauss - X')
end