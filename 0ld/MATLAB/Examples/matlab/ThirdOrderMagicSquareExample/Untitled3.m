oscil = @(t,y)[y(2); -2*y(2) - 10*y(1) + sin(t)];
Y0 = [1;0];
[T, Y] = ode45(oscil, [0 15], Y0);
T
hold on, grid on
plot (T, Y(:,1), 'r') % ishodnoen
plot (T, Y(:,2), 'k') % proizvodnaya