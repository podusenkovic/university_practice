function X = Gauss(A, f)
   n = length(f);
   bigMatr = [A, f'];
   
   if (det(A) == 0)
    error = 1;
   else
        for i = 1:n
            [maxim, maxInd] = max(bigMatr(i:n,i));
            if (1 ~= maxInd)               % main po stolbcy
                index = maxInd + i - 1;
                temp = bigMatr(i,:);
                bigMatr(i,:) = bigMatr(index, :);
                bigMatr(index, :) = temp;
            end
        end
    
        for i = 1:(n - 1)
            for j = (i + 1):n %delaem verh treug
                bigMatr(j,:) = bigMatr(j,:) ./ bigMatr(j,i) .* bigMatr(i,i);
                bigMatr(j,:) = bigMatr(j,:) - bigMatr(i,:);
            end
        end
        for i = n:-1:2
            for j = (i - 1):-1:1 % delaem diagonal tolko
                bigMatr(j,:) = bigMatr(j,:) ./ bigMatr(j,i) .* bigMatr(i,i);
                bigMatr(j,:) = bigMatr(j,:) - bigMatr(i,:);
            end
        end
    
        for i = 1:n %privodim k edinicam
            bigMatr(i,:) = bigMatr(i,:) ./ bigMatr(i,i);
        end
        %bigMatr
        %rref([A, f']);
        X = bigMatr(:, n + 1);
    end
end