clc, clear
x = -pi:0.001:pi;
y1 = x - sin(3*x);
y2 = @(param)param/sin(param);
hold on, grid on, xlabel('x'), ylabel('y')
plot(x, y1, 'r')
fplot(y2, [-2*pi/3, 2*pi/3], 'b')
legend('x - sin(3*x)', 'x/sin(x)')