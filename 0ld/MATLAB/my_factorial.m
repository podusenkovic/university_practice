function rslt = my_factorial(x)
    rslt = 1;
    if (x < 0)
        disp('Error! Negative values are prohibited!')
        rslt = -1;
    else
        if (rand(1) > 0.5)
            for i = 2:x 
                rslt = rslt * i;
            end
        else
            if (x == 0 || x == 1)
                rslt = 1;
                else rslt = x * my_factorial(x - 1);
            end
        end
    end
end