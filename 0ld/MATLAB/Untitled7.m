clc, clear
t = -5:0.001:5;
x = sin(t);
y = 2 * cos(2*t);
hold on, grid on, xlabel('x'), ylabel('y')
plot(x,y, 'r')
figure
x = 0:0.001:3; y = 0:0.001:3;
z = (x.^2 + y.^2) / (5 .* x .* y) + log(x + y);
hold on, grid on, xlabel('x'), ylabel('y')
plot3(x, y, z, 'b')
