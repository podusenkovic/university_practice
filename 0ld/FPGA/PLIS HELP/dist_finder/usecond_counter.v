module usecond_counter (input 			clk,
								input 			rst,
								input 			one_usecond,
								input [5:0]		howMuch,
								output reg 		signal);

reg [5:0] count_usecond;

always @(posedge clk)
	if(rst) count_usecond <= 6'b0;
	else if (one_usecond)
		if(count_usecond == howMuch) begin
			signal <= 1'b1;
			count_usecond <= 6'b0;
		end
		else begin
			signal <= 1'b0;
			count_usecond <= count_usecond + 1'b1;
		end
endmodule 