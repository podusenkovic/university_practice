module one_usecond_counter (input clk,
									 input rst,
									 input start,
									 output one_usecond);


reg [25:0] counter_one_usecond;
assign one_usecond = (counter_one_usecond == 26'd50) ? 1'b1 : 1'b0;
	
always @(posedge clk)
	if(rst) counter_one_usecond <= 26'b0;
	else if (start)
		if(counter_one_usecond == 26'd50) counter_one_usecond <= 26'b0;
		else counter_one_usecond <= counter_one_usecond + 1'b1;
endmodule