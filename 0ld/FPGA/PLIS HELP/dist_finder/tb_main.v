`timescale 1ns/100ps
module tb_main();

reg [9:0] SW;
reg CLOCK_50;
reg [3:0] KEY;
reg echo = 0;
wire trig;
wire [6:0] HEX0;
wire [6:0] HEX1;
wire [6:0] HEX2;
wire [6:0] HEX3;

initial begin
	CLOCK_50 = 0;
	#10 forever #20 CLOCK_50 = !CLOCK_50;
end

initial begin
	KEY = 4'b1110;
	#50;
	KEY = 4'b1111;
	#1000000;
	echo = 1;
	#5800000;
	echo = 0;
	#1000000;
	echo = 1;
	#30000000;
	echo = 0;
	#1000000;
	echo = 1;
	#5800000;
	echo = 0;
	#1000000;
	KEY = 4'b1110;
	#50;
	KEY = 4'b1111;
end

main DUT(.CLOCK_50(CLOCK_50),
			.KEY(KEY[3:0]),
			.SW(SW[9:0]),
			.echo(echo),
			.trig(trig),
			.HEX0(HEX0[6:0]),
			.HEX1(HEX1[6:0]),
			.HEX2(HEX2[6:0]),
			.HEX3(HEX3[6:0]));

endmodule 