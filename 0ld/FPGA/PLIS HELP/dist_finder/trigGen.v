module trigGen (input 			clk,
					 input 			rst,
					 input 			echo,
					 output reg 	triger);

reg [25:0] loopCounter = 26'b0;
reg echoOld = 0;

always @(posedge clk) begin
	if (rst) begin
		loopCounter <= 26'b0;
		triger <= 0;
	end
	else begin
			if (loopCounter > 26'd950000) loopCounter <= 26'b0;
			else begin
					loopCounter <= loopCounter + 1'b1;
					if (loopCounter > 26'd250) triger <= 0;
					else triger <= 1;
			end

	if (echoOld == 1 && echo == 0) begin
		echoOld <= echo;
		loopCounter <= 26'b0;
	end
	else echoOld <= echo;
	end
end
endmodule 