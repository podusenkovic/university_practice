module main(input[9:0]  SW,
				input			CLOCK_50,
				input[3:0]  KEY,
				input 		echo,
				output 		trig,
				output[6:0] HEX0, HEX1, HEX2, HEX3);

wire rst = ~KEY[0];
reg echoOld = 0;
reg [20:0] F;

trigGen trigLoop 		(.clk(CLOCK_50),
							 .rst(rst),
							 .echo(echo),
							 .triger(trig));

always @(posedge CLOCK_50) begin 
	if (rst) begin 
		echoOld <= 0;
		F <= 21'b0;
	end
	else begin
		if (echoOld == 0 && echo == 1) begin
			echoOld <= echo;
			F <= 21'b0;
		end
		else echoOld <= echo;
		if (echo) F <= F + 1'b1;
	end
end

wire [15:0] dec_F;
assign dec_F[3:0] = ((F / 58 / 25) > 500) ? 4'd12 : (F / 58 / 25) % 10;
assign dec_F[7:4] = ((F / 58 / 25) > 500) ? 4'd11 : ((F / 58 / 25) / 10) % 10;
assign dec_F[11:8] = ((F / 58 / 25) > 500) ? 4'd11 : ((F / 58 / 25) / 100) % 10;
assign dec_F[15:12] = ((F / 58 / 25) > 500) ? 4'd10 : (F / 58 / 25) / 1000;

dec dec0		(.in(dec_F[3:0]),
				 .out(HEX0));

dec dec1		(.in(dec_F[7:4]),
				 .out(HEX0));

dec dec2		(.in(dec_F[11:8]),
				 .out(HEX0));
				 
dec dec3		(.in(dec_F[15:12]),
				 .out(HEX0));				 

endmodule 