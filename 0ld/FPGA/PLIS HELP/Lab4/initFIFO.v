module initFIFO(input           CLOCK_50,
				 	 input  [13:0]   data_in,
					 input  [3:0]	  KEY,
					 output [1:0]	  LEDG,
					 output [13:0]   data_out);

	wire we;
	
	wire rst = ~KEY[0];
	
	reg key3_d0, key3_d1, key3_d2;
	reg key2_d0, key2_d1, key2_d2;
		
	wire signal_wr;
	wire signal_rd;
	
	reg[4:0] addr_rd;
	reg[4:0] addr_wr;
	
	initial begin
		addr_rd <= 5'b0;
		addr_wr <= 5'b0;
	end
	
	ram myRam(.clk(CLOCK_50),
				 .addr_rd(addr_rd),
				 .addr_wr(addr_wr),
				 .we(we),
				 .data_in(data_in),
				 .data_out(data_out));
	
	always@(posedge CLOCK_50)
		if (~KEY[0]) begin
			key3_d0 <= KEY[3];
			key3_d1 <= KEY[3];
			key3_d2 <= KEY[3];
		end
		else begin
			key3_d0 <= KEY[3];
			key3_d1 <= key3_d0;
			key3_d2 <= key3_d1;
		end
	
	always@(posedge CLOCK_50)
		if (~KEY[0]) begin
			key2_d0 <= KEY[2];
			key2_d1 <= KEY[2];
			key2_d2 <= KEY[2];
		end
		else begin
			key2_d0 <= KEY[2];
			key2_d1 <= key2_d0;
			key2_d2 <= key2_d1;
		end
	
	assign signal_wr = (key3_d2 && !key3_d1) ? 1'b1 : 1'b0;
	assign signal_rd = (key2_d2 && !key2_d1) ? 1'b1 : 1'b0;
	
	wire full = ((addr_rd - 1'b1 + 32)%32 == addr_wr) ? 1'b1 : 1'b0;
	wire empty = (addr_rd == addr_wr) ? 1'b1 : 1'b0;
	
	assign LEDG[0] = empty;
	assign LEDG[1] = full;
	
	assign we = (signal_wr && !full) ? 1'b1 : 1'b0;
	
	always @(posedge CLOCK_50) begin
		if (rst) begin
			addr_wr <= 5'b0;
			addr_rd <= 5'b0;
		end
		else if (signal_wr && !full)
					addr_wr <= (addr_wr + 1'b1)%32;
		else if (signal_rd && !empty)
					addr_rd <= (addr_rd + 1'b1)%32;
	end
	


endmodule 