module main #(parameter END_COUNT = 26'd499999,
				  parameter MAX_SECONDS = 14'd9999)
				(input           CLOCK_50,
				 input  [9:0]    SW,
				 input  [3:0]	  KEY,
				 output [4:0]    LEDR,
				 output [1:0]    LEDG,
				 output [6:0]    HEX0,
				 output [6:0]    HEX1,
				 output [6:0]    HEX2,
				 output [6:0]    HEX3);
	
	localparam STATE1 = 3'b000;
	localparam STATE2 = 3'b001;
	localparam STATE3 = 3'b010;
	localparam STATE4 = 3'b011;
	localparam STATE5 = 3'b100;
	
	wire[2:0] stateMain;
		
	automat SM(.clk(CLOCK_50),
				  .SW(SW),
				  .KEY(KEY),
				  .state(stateMain));
	
	assign LEDR[0] = (stateMain == STATE1) ? 1'b1 : 1'b0;
	assign LEDR[1] = (stateMain == STATE2) ? 1'b1 : 1'b0;
	assign LEDR[2] = (stateMain == STATE3) ? 1'b1 : 1'b0;
	assign LEDR[3] = (stateMain == STATE4) ? 1'b1 : 1'b0;
	assign LEDR[4] = (stateMain == STATE5) ? 1'b1 : 1'b0;
	
	
	assign start = (stateMain == STATE1 || stateMain == STATE4) ? 1'b1 : 1'b0;
	assign stop = (stateMain == STATE2 || stateMain == STATE5) ? 1'b1 : 1'b0;
	assign reset = (stateMain == STATE3) ? 1'b1 : 1'b0;
	
	wire secChanged;
	
	wire[13:0] toOut;
	wire[13:0] saved;
	wire[13:0] timer;
	
	one_sec_counter #(END_COUNT)
						 count_0(.clk(CLOCK_50),
									.rst(reset),
									.start(start),
									.one_sec(secChanged));
	
	sec_counter #(MAX_SECONDS)
					sec_cnt(.one_second(secChanged),
							  .rst(reset),
							  .clk(CLOCK_50),
							  .count_second(timer));
	
							  
	initFIFO myFIFO(.CLOCK_50(CLOCK_50),
						 .data_in(timer),
						 .KEY(KEY),
						 .LEDG(LEDG),
						 .data_out(saved));
	
	assign toOut = (SW[9]) ? timer : saved;
	
	/*secondsTo7Seg decod(.timer(toOut),
							  .HEX0(HEX0),
							  .HEX1(HEX1),
							  .HEX2(HEX2),
							  .HEX3(HEX3));*/
	wire[15:0] decs;
	assign decs[3:0] = toOut%10;
	assign decs[7:4] = (toOut%100)/10;
	assign decs[11:8] = (toOut%1000)/100;
	assign decs[15:12] = toOut/1000;
	
	
	dec dec0(.in(decs[3:0]),
				.out(HEX0));
				
	dec dec1(.in(decs[7:4]),
				.out(HEX1));
				
	dec dec2(.in(decs[11:8]),
				.out(HEX2));		
				
	dec dec3(.in(decs[15:12]),
				.out(HEX3));


endmodule 