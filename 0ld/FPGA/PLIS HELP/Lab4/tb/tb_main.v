`timescale 10ps/1ps
module tb_main();
		
		localparam END_COUNT = 26'd49;
		localparam MAX_SECONDS = 14'd9;
		
		reg  CLOCK_50;
		reg  [9:0]    SW;
		reg  [3:0]	  KEY;
		wire [6:0]    HEX0;
		wire [6:0]    HEX1;
		wire [6:0]    HEX2;
		wire [6:0]    HEX3;
		
		main #(.END_COUNT(END_COUNT),
				 .MAX_SECONDS(MAX_SECONDS))
				DUT(.CLOCK_50(CLOCK_50),
					.SW(SW),
					.KEY(KEY),
					.LEDR(LEDR),
					.HEX0(HEX0),
					.HEX1(HEX1),
					.HEX2(HEX2),
					.HEX3(HEX3));
		
		initial begin
			SW = 10'b0;
			SW[4] = 1;
			SW[1] = 1;
		end
		
		initial begin
			CLOCK_50 = 1'b1;
			forever #10 CLOCK_50 = !CLOCK_50;
		end
		
		initial begin
			KEY[0] = 1'b0;	// rst
			#50;
			KEY[0] = 1'b1;
			#2000;
			
			KEY[3] = 1'b0; // 3 -> 2
			#50;
			KEY[3] = 1'b1;
			#2000;
			
			KEY[2] = 1'b0; // 2 -> 1
			#50;
			KEY[2] = 1'b1;
			#2000;
			
			SW[4] = 1'b0; // 1 -> 3
			#50;
			SW[4] = 1'b1;
			#2000;
			
			SW[0] = 1'b1; // 3 -> 4
			#50;
			SW[0] = 1'b0;
			#2000;
			
			SW[2] = 1'b1; // 4 -> 1
			#50;
			SW[2] = 1'b0;
			#2000;
			
			SW[4] = 1'b0; // 1 -> 3
			#50;
			SW[4] = 1'b1;
			#2000;
			
			SW[0] = 1'b1; // 3 -> 4
			#50;
			SW[0] = 1'b0;
			#2000;
			
			SW[1] = 1'b0; // 4 -> 5
			#50;
			SW[1] = 1'b1;
			#2000;
			
			KEY[1] = 1'b0; // 5 -> 1
			#50;
			KEY[1] = 1'b1;
			#2000;
						
		end

endmodule 