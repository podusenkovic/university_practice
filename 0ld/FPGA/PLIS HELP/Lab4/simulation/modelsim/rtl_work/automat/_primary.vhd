library verilog;
use verilog.vl_types.all;
entity automat is
    port(
        clk             : in     vl_logic;
        SW              : in     vl_logic_vector(9 downto 0);
        KEY             : in     vl_logic_vector(3 downto 0);
        state           : out    vl_logic_vector(2 downto 0)
    );
end automat;
