library verilog;
use verilog.vl_types.all;
entity sec_counter is
    generic(
        MAX_SECS        : vl_logic_vector(0 to 13) := (Hi1, Hi0, Hi0, Hi1, Hi1, Hi1, Hi0, Hi0, Hi0, Hi0, Hi1, Hi1, Hi1, Hi1)
    );
    port(
        one_second      : in     vl_logic;
        rst             : in     vl_logic;
        clk             : in     vl_logic;
        count_second    : out    vl_logic_vector(13 downto 0);
        data            : out    vl_logic_vector(13 downto 0)
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of MAX_SECS : constant is 1;
end sec_counter;
