-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

-- DATE "04/10/2018 11:39:28"

-- 
-- Device: Altera EP2C20F484C7 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEII;
LIBRARY IEEE;
USE CYCLONEII.CYCLONEII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	main IS
    PORT (
	CLOCK_50 : IN std_logic;
	SW : IN std_logic_vector(9 DOWNTO 0);
	KEY : IN std_logic_vector(3 DOWNTO 0);
	LEDR : OUT std_logic_vector(4 DOWNTO 0);
	LEDG : OUT std_logic_vector(1 DOWNTO 0);
	HEX0 : OUT std_logic_vector(6 DOWNTO 0);
	HEX1 : OUT std_logic_vector(6 DOWNTO 0);
	HEX2 : OUT std_logic_vector(6 DOWNTO 0);
	HEX3 : OUT std_logic_vector(6 DOWNTO 0)
	);
END main;

-- Design Ports Information
-- SW[5]	=>  Location: PIN_U12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_U11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_M2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[8]	=>  Location: PIN_M1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[1]	=>  Location: PIN_R21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- LEDR[0]	=>  Location: PIN_R20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[1]	=>  Location: PIN_R19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[2]	=>  Location: PIN_U19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[3]	=>  Location: PIN_Y19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[4]	=>  Location: PIN_T18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDG[0]	=>  Location: PIN_U22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDG[1]	=>  Location: PIN_U21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[0]	=>  Location: PIN_J2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[1]	=>  Location: PIN_J1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[2]	=>  Location: PIN_H2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[3]	=>  Location: PIN_H1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[4]	=>  Location: PIN_F2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[5]	=>  Location: PIN_F1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[6]	=>  Location: PIN_E2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[0]	=>  Location: PIN_E1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[1]	=>  Location: PIN_H6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[2]	=>  Location: PIN_H5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[3]	=>  Location: PIN_H4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[4]	=>  Location: PIN_G3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[5]	=>  Location: PIN_D2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[6]	=>  Location: PIN_D1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[0]	=>  Location: PIN_G5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[1]	=>  Location: PIN_G6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[2]	=>  Location: PIN_C2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[3]	=>  Location: PIN_C1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[4]	=>  Location: PIN_E3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[5]	=>  Location: PIN_E4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[6]	=>  Location: PIN_D3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[0]	=>  Location: PIN_F4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[1]	=>  Location: PIN_D5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[2]	=>  Location: PIN_D6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[3]	=>  Location: PIN_J4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[4]	=>  Location: PIN_L8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[5]	=>  Location: PIN_F3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[6]	=>  Location: PIN_D4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- SW[9]	=>  Location: PIN_L2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- CLOCK_50	=>  Location: PIN_L1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_L21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_M22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[0]	=>  Location: PIN_R22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_L22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_V12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_W12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[2]	=>  Location: PIN_T22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[3]	=>  Location: PIN_T21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF main IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLOCK_50 : std_logic;
SIGNAL ww_SW : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_KEY : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_LEDR : std_logic_vector(4 DOWNTO 0);
SIGNAL ww_LEDG : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_HEX0 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX2 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX3 : std_logic_vector(6 DOWNTO 0);
SIGNAL \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTADATAIN_bus\ : std_logic_vector(13 DOWNTO 0);
SIGNAL \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTAADDR_bus\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBADDR_bus\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\ : std_logic_vector(13 DOWNTO 0);
SIGNAL \CLOCK_50~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~0_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~6_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~8_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~6_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~8_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~8_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~6_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~8_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[1]~14_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~6_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[1]~14_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[1]~14_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\ : std_logic;
SIGNAL \count_0|cntr[2]~32_combout\ : std_logic;
SIGNAL \count_0|cntr[23]~74_combout\ : std_logic;
SIGNAL \count_0|cntr[24]~77\ : std_logic;
SIGNAL \count_0|cntr[25]~78_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[18]~100_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[16]~102_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[23]~103_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[22]~104_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[21]~105_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[28]~106_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[27]~107_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[26]~108_combout\ : std_logic;
SIGNAL \toOut[8]~15_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[38]~112_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[36]~114_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[43]~115_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[41]~117_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[48]~118_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[47]~119_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[46]~120_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[52]~122_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[58]~124_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[57]~125_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[63]~127_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[53]~103_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[52]~104_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[60]~109_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[58]~111_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[70]~112_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[69]~113_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[68]~114_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[66]~117_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[78]~118_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[77]~119_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[76]~120_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[74]~123_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[86]~124_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[85]~125_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[82]~129_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[92]~132_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[102]~136_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[101]~137_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[99]~139_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[98]~141_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[110]~142_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[109]~143_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[16]~28_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[21]~32_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[28]~33_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[26]~36_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[108]~104_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[107]~105_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[106]~106_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[105]~107_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[104]~108_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[102]~110_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[118]~112_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[113]~117_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[112]~118_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[127]~122_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[125]~124_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[124]~125_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[123]~127_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[139]~130_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[138]~131_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[137]~132_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[136]~133_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[134]~136_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[151]~138_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[150]~139_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[149]~140_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[53]~41_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[52]~42_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[49]~45_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[61]~47_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[60]~48_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[59]~49_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[58]~50_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[48]~51_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[57]~52_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[67]~56_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[56]~58_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[65]~59_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[108]~82_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[107]~83_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[105]~85_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[103]~87_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[102]~88_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[118]~90_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[113]~95_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[112]~96_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[130]~97_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[129]~98_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[128]~99_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[127]~100_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[125]~102_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[139]~108_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[137]~110_combout\ : std_logic;
SIGNAL \SM|Mux1~1_combout\ : std_logic;
SIGNAL \sec_cnt|Equal0~0_combout\ : std_logic;
SIGNAL \count_0|Equal0~3_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[15]~136_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[20]~142_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[25]~147_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[33]~148_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[31]~150_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[30]~152_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[37]~154_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[35]~156_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[40]~161_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[45]~166_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[53]~168_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[50]~172_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[55]~176_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[60]~179_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[61]~182_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[54]~146_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[51]~149_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[50]~150_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[49]~151_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[62]~153_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[61]~154_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[59]~156_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[57]~158_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[65]~165_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[75]~169_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[73]~171_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[84]~174_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[83]~175_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[94]~178_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[18]~37_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[17]~38_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[23]~42_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[22]~43_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[27]~47_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[96]~195_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[96]~196_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[25]~48_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[103]~151_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[101]~154_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[119]~155_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[117]~157_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[116]~158_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[114]~160_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[130]~165_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[129]~166_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[128]~167_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[141]~176_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[140]~177_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[152]~187_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[148]~191_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[54]~60_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[51]~63_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[50]~64_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[62]~66_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[70]~73_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[69]~74_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[68]~75_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[66]~77_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[101]~123_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[119]~124_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[114]~129_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[100]~132_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[100]~133_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[126]~138_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[124]~140_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[123]~144_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[141]~145_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[140]~146_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[138]~148_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[136]~150_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[135]~151_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[122]~152_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[110]~154_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[134]~155_combout\ : std_logic;
SIGNAL \SM|Mux1~0_combout\ : std_logic;
SIGNAL \SM|Mux1~2_combout\ : std_logic;
SIGNAL \SM|Mux0~0_combout\ : std_logic;
SIGNAL \SM|Mux0~1_combout\ : std_logic;
SIGNAL \SM|Mux0~2_combout\ : std_logic;
SIGNAL \SM|Mux2~0_combout\ : std_logic;
SIGNAL \SM|Mux2~1_combout\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \Equal0~1_combout\ : std_logic;
SIGNAL \Equal0~2_combout\ : std_logic;
SIGNAL \Equal0~3_combout\ : std_logic;
SIGNAL \Equal0~4_combout\ : std_logic;
SIGNAL \myFIFO|addr_wr[0]~7_combout\ : std_logic;
SIGNAL \myFIFO|key3_d0~regout\ : std_logic;
SIGNAL \myFIFO|key3_d1~0_combout\ : std_logic;
SIGNAL \myFIFO|key3_d1~regout\ : std_logic;
SIGNAL \myFIFO|key3_d2~0_combout\ : std_logic;
SIGNAL \myFIFO|key3_d2~regout\ : std_logic;
SIGNAL \myFIFO|addr_wr[2]~11_combout\ : std_logic;
SIGNAL \myFIFO|key2_d0~feeder_combout\ : std_logic;
SIGNAL \myFIFO|key2_d0~regout\ : std_logic;
SIGNAL \myFIFO|key2_d1~0_combout\ : std_logic;
SIGNAL \myFIFO|key2_d1~regout\ : std_logic;
SIGNAL \myFIFO|key2_d2~0_combout\ : std_logic;
SIGNAL \myFIFO|key2_d2~regout\ : std_logic;
SIGNAL \myFIFO|Add2~1\ : std_logic;
SIGNAL \myFIFO|Add2~4\ : std_logic;
SIGNAL \myFIFO|Add2~6_combout\ : std_logic;
SIGNAL \myFIFO|Add2~8_combout\ : std_logic;
SIGNAL \myFIFO|Add2~7\ : std_logic;
SIGNAL \myFIFO|Add2~9_combout\ : std_logic;
SIGNAL \myFIFO|Add2~11_combout\ : std_logic;
SIGNAL \myFIFO|Add2~10\ : std_logic;
SIGNAL \myFIFO|Add2~12_combout\ : std_logic;
SIGNAL \myFIFO|Add2~14_combout\ : std_logic;
SIGNAL \myFIFO|Add0~5\ : std_logic;
SIGNAL \myFIFO|Add0~7\ : std_logic;
SIGNAL \myFIFO|Add0~8_combout\ : std_logic;
SIGNAL \myFIFO|Add0~0_combout\ : std_logic;
SIGNAL \myFIFO|Add0~2_combout\ : std_logic;
SIGNAL \myFIFO|Equal0~0_combout\ : std_logic;
SIGNAL \myFIFO|we~0_combout\ : std_logic;
SIGNAL \myFIFO|we~1_combout\ : std_logic;
SIGNAL \myFIFO|addr_rd~0_combout\ : std_logic;
SIGNAL \myFIFO|Add2~0_combout\ : std_logic;
SIGNAL \myFIFO|Add2~2_combout\ : std_logic;
SIGNAL \myFIFO|Add0~1\ : std_logic;
SIGNAL \myFIFO|Add0~3\ : std_logic;
SIGNAL \myFIFO|Add0~4_combout\ : std_logic;
SIGNAL \myFIFO|Add0~6_combout\ : std_logic;
SIGNAL \myFIFO|Equal0~1_combout\ : std_logic;
SIGNAL \myFIFO|Equal0~2_combout\ : std_logic;
SIGNAL \myFIFO|addr_wr[3]~17_combout\ : std_logic;
SIGNAL \myFIFO|addr_wr[0]~8\ : std_logic;
SIGNAL \myFIFO|addr_wr[1]~9_combout\ : std_logic;
SIGNAL \myFIFO|addr_wr[1]~10\ : std_logic;
SIGNAL \myFIFO|addr_wr[2]~12\ : std_logic;
SIGNAL \myFIFO|addr_wr[3]~13_combout\ : std_logic;
SIGNAL \myFIFO|addr_wr[3]~14\ : std_logic;
SIGNAL \myFIFO|addr_wr[4]~15_combout\ : std_logic;
SIGNAL \myFIFO|Equal1~1_combout\ : std_logic;
SIGNAL \myFIFO|Add2~3_combout\ : std_logic;
SIGNAL \myFIFO|Add2~5_combout\ : std_logic;
SIGNAL \myFIFO|Equal1~0_combout\ : std_logic;
SIGNAL \myFIFO|Equal1~2_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram_rtl_0_bypass[5]~feeder_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram~1_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram~2_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram_rtl_0_bypass[1]~feeder_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram~0_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram~3_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[0]~14_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[1]~19\ : std_logic;
SIGNAL \sec_cnt|count_second[2]~20_combout\ : std_logic;
SIGNAL \count_0|cntr[0]~27\ : std_logic;
SIGNAL \count_0|cntr[1]~30_combout\ : std_logic;
SIGNAL \count_0|cntr[0]~26_combout\ : std_logic;
SIGNAL \count_0|cntr[3]~34_combout\ : std_logic;
SIGNAL \count_0|Equal0~0_combout\ : std_logic;
SIGNAL \count_0|cntr[9]~46_combout\ : std_logic;
SIGNAL \count_0|Equal0~2_combout\ : std_logic;
SIGNAL \count_0|cntr[7]~42_combout\ : std_logic;
SIGNAL \count_0|cntr[5]~38_combout\ : std_logic;
SIGNAL \count_0|Equal0~1_combout\ : std_logic;
SIGNAL \count_0|Equal0~4_combout\ : std_logic;
SIGNAL \count_0|cntr[9]~28_combout\ : std_logic;
SIGNAL \count_0|cntr[9]~29_combout\ : std_logic;
SIGNAL \count_0|cntr[1]~31\ : std_logic;
SIGNAL \count_0|cntr[2]~33\ : std_logic;
SIGNAL \count_0|cntr[3]~35\ : std_logic;
SIGNAL \count_0|cntr[4]~36_combout\ : std_logic;
SIGNAL \count_0|cntr[4]~37\ : std_logic;
SIGNAL \count_0|cntr[5]~39\ : std_logic;
SIGNAL \count_0|cntr[6]~40_combout\ : std_logic;
SIGNAL \count_0|cntr[6]~41\ : std_logic;
SIGNAL \count_0|cntr[7]~43\ : std_logic;
SIGNAL \count_0|cntr[8]~44_combout\ : std_logic;
SIGNAL \count_0|cntr[8]~45\ : std_logic;
SIGNAL \count_0|cntr[9]~47\ : std_logic;
SIGNAL \count_0|cntr[10]~48_combout\ : std_logic;
SIGNAL \count_0|cntr[10]~49\ : std_logic;
SIGNAL \count_0|cntr[11]~50_combout\ : std_logic;
SIGNAL \count_0|cntr[11]~51\ : std_logic;
SIGNAL \count_0|cntr[12]~52_combout\ : std_logic;
SIGNAL \count_0|cntr[12]~53\ : std_logic;
SIGNAL \count_0|cntr[13]~54_combout\ : std_logic;
SIGNAL \count_0|cntr[13]~55\ : std_logic;
SIGNAL \count_0|cntr[14]~56_combout\ : std_logic;
SIGNAL \count_0|cntr[14]~57\ : std_logic;
SIGNAL \count_0|cntr[15]~58_combout\ : std_logic;
SIGNAL \count_0|cntr[15]~59\ : std_logic;
SIGNAL \count_0|cntr[16]~60_combout\ : std_logic;
SIGNAL \count_0|cntr[16]~61\ : std_logic;
SIGNAL \count_0|cntr[17]~62_combout\ : std_logic;
SIGNAL \count_0|cntr[17]~63\ : std_logic;
SIGNAL \count_0|cntr[18]~64_combout\ : std_logic;
SIGNAL \count_0|cntr[18]~65\ : std_logic;
SIGNAL \count_0|cntr[19]~66_combout\ : std_logic;
SIGNAL \count_0|cntr[19]~67\ : std_logic;
SIGNAL \count_0|cntr[20]~68_combout\ : std_logic;
SIGNAL \count_0|cntr[20]~69\ : std_logic;
SIGNAL \count_0|cntr[21]~71\ : std_logic;
SIGNAL \count_0|cntr[22]~72_combout\ : std_logic;
SIGNAL \count_0|cntr[22]~73\ : std_logic;
SIGNAL \count_0|cntr[23]~75\ : std_logic;
SIGNAL \count_0|cntr[24]~76_combout\ : std_logic;
SIGNAL \count_0|cntr[21]~70_combout\ : std_logic;
SIGNAL \count_0|Equal0~6_combout\ : std_logic;
SIGNAL \count_0|Equal0~5_combout\ : std_logic;
SIGNAL \count_0|Equal0~7_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[13]~17_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[2]~21\ : std_logic;
SIGNAL \sec_cnt|count_second[3]~22_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[3]~23\ : std_logic;
SIGNAL \sec_cnt|count_second[4]~25\ : std_logic;
SIGNAL \sec_cnt|count_second[5]~26_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[4]~24_combout\ : std_logic;
SIGNAL \sec_cnt|Equal0~2_combout\ : std_logic;
SIGNAL \sec_cnt|Equal0~3_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[5]~27\ : std_logic;
SIGNAL \sec_cnt|count_second[6]~28_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[6]~29\ : std_logic;
SIGNAL \sec_cnt|count_second[7]~30_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[7]~31\ : std_logic;
SIGNAL \sec_cnt|count_second[8]~32_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[8]~33\ : std_logic;
SIGNAL \sec_cnt|count_second[9]~34_combout\ : std_logic;
SIGNAL \sec_cnt|Equal0~1_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[13]~16_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[0]~15\ : std_logic;
SIGNAL \sec_cnt|count_second[1]~18_combout\ : std_logic;
SIGNAL \CLOCK_50~combout\ : std_logic;
SIGNAL \CLOCK_50~clkctrl_outclk\ : std_logic;
SIGNAL \sec_cnt|count_second[9]~35\ : std_logic;
SIGNAL \sec_cnt|count_second[10]~36_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[10]~37\ : std_logic;
SIGNAL \sec_cnt|count_second[11]~38_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[11]~39\ : std_logic;
SIGNAL \sec_cnt|count_second[12]~40_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[12]~41\ : std_logic;
SIGNAL \sec_cnt|count_second[13]~42_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a1\ : std_logic;
SIGNAL \toOut[1]~2_combout\ : std_logic;
SIGNAL \toOut[1]~3_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a13\ : std_logic;
SIGNAL \toOut[13]~4_combout\ : std_logic;
SIGNAL \toOut[13]~5_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[17]~134_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[23]~138_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a11\ : std_logic;
SIGNAL \toOut[11]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[16]~135_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[22]~139_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[18]~133_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[17]~101_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[15]~137_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a10\ : std_logic;
SIGNAL \toOut[10]~10_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[21]~140_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a9\ : std_logic;
SIGNAL \toOut[9]~12_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[20]~141_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[28]~143_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[27]~144_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[26]~145_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a8\ : std_logic;
SIGNAL \toOut[8]~14_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[25]~146_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[33]~109_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[32]~110_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[31]~111_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a7\ : std_logic;
SIGNAL \toOut[7]~16_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[30]~151_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_7_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[36]~155_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a6\ : std_logic;
SIGNAL \toOut[6]~18_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[35]~157_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[32]~149_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[38]~153_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[37]~113_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_8_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[42]~159_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[48]~163_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[41]~160_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[47]~164_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[43]~158_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[42]~116_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a5\ : std_logic;
SIGNAL \toOut[5]~19_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[40]~162_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_9_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[46]~165_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a4\ : std_logic;
SIGNAL \toOut[4]~20_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[45]~167_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_10_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[51]~123_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a3\ : std_logic;
SIGNAL \toOut[3]~21_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[50]~171_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[53]~121_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[52]~169_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_11_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[51]~170_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[57]~174_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[63]~180_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[56]~175_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[62]~181_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[58]~173_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[56]~126_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a2\ : std_logic;
SIGNAL \toOut[2]~22_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[55]~177_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_12_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[61]~129_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[60]~178_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_13_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[62]~128_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0~portbdataout\ : std_logic;
SIGNAL \toOut[0]~0_combout\ : std_logic;
SIGNAL \toOut[0]~1_combout\ : std_logic;
SIGNAL \dec0|WideOr6~0_combout\ : std_logic;
SIGNAL \dec0|WideOr5~0_combout\ : std_logic;
SIGNAL \dec0|WideOr4~0_combout\ : std_logic;
SIGNAL \dec0|WideOr3~0_combout\ : std_logic;
SIGNAL \dec0|WideOr2~0_combout\ : std_logic;
SIGNAL \dec0|WideOr1~0_combout\ : std_logic;
SIGNAL \dec0|WideOr0~0_combout\ : std_logic;
SIGNAL \toOut[11]~9_combout\ : std_logic;
SIGNAL \toOut[10]~11_combout\ : std_logic;
SIGNAL \toOut[9]~13_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a12\ : std_logic;
SIGNAL \toOut[12]~6_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[53]~147_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[52]~148_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[51]~105_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[50]~106_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[49]~152_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[54]~102_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[62]~107_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[61]~108_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[60]~155_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[59]~110_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[58]~157_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[57]~159_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[67]~115_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[66]~116_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[65]~164_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~1\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~3\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~4_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[70]~160_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[69]~161_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[68]~162_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~5\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~7\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~9\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[67]~163_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[76]~168_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[85]~173_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~2_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[75]~121_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[74]~122_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[73]~170_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~1\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~3\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~4_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[78]~166_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[77]~167_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~5\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~7\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~9\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[7]~11_cout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[84]~126_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~2_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[83]~127_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[82]~128_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[81]~177_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~1\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~3\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~5\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~7\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~8_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[86]~172_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~9\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[7]~11_cout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[94]~130_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~6_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[93]~131_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[92]~180_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~2_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[91]~133_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~0_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[90]~135_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[89]~182_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~1\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~3\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~5\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~7\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~9\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[7]~11_cout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~4_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~0_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[91]~181_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[100]~186_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[109]~191_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[93]~179_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[102]~184_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~4_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[101]~185_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[100]~138_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[81]~176_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[90]~134_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[99]~187_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[89]~183_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[98]~140_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[97]~188_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~1\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~3\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~5\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~7\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~9\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[7]~11_cout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~4_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[108]~144_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[17]~27_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~2_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[108]~192_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[16]~39_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~2_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~0_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[107]~193_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[15]~41_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~6_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[110]~190_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[18]~26_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[23]~29_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[22]~30_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[15]~40_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[21]~31_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~0_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[97]~189_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[106]~145_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[20]~44_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[28]~46_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[27]~34_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[20]~45_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[26]~35_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[105]~194_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[25]~49_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_6_result_int[1]~1_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_6_result_int[2]~3_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_6_result_int[3]~5_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ : std_logic;
SIGNAL \dec1|WideOr6~0_combout\ : std_logic;
SIGNAL \dec1|WideOr5~0_combout\ : std_logic;
SIGNAL \dec1|WideOr4~0_combout\ : std_logic;
SIGNAL \dec1|WideOr3~0_combout\ : std_logic;
SIGNAL \dec1|WideOr2~0_combout\ : std_logic;
SIGNAL \dec1|WideOr1~0_combout\ : std_logic;
SIGNAL \dec1|WideOr0~0_combout\ : std_logic;
SIGNAL \toOut[12]~7_combout\ : std_logic;
SIGNAL \toOut[7]~17_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[107]~147_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[106]~148_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[105]~149_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[104]~150_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[103]~109_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[102]~152_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[101]~153_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[108]~146_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[119]~111_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[118]~156_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[117]~113_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[116]~114_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[115]~115_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[114]~116_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[113]~161_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[112]~162_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[129]~120_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[128]~121_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[115]~159_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[127]~168_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[126]~123_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[125]~170_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[100]~164_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[100]~163_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[124]~171_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[111]~172_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[99]~174_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[99]~173_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[123]~175_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[130]~119_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[141]~128_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[140]~129_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[139]~178_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[126]~169_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[138]~179_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[137]~180_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[136]~181_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[135]~134_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[122]~183_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[110]~184_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[110]~185_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[134]~186_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~1\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~3\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~5\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~7\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~9\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~11\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~13\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~12_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[152]~137_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[151]~188_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[150]~189_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[149]~190_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~4_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[148]~141_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~10_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[53]~61_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~8_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[52]~62_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[51]~43_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[50]~44_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~2_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[111]~126_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[135]~182_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[147]~192_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[49]~65_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[54]~40_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[62]~46_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[61]~67_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[60]~68_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[59]~69_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[147]~142_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[1]~12_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[58]~70_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~0_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[48]~71_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[146]~143_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[122]~135_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[146]~193_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[0]~14_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[57]~72_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[70]~53_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[69]~54_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[68]~55_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[67]~76_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[66]~57_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[121]~196_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[121]~195_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[1]~20_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[133]~194_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[145]~197_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[133]~144_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[2]~18_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[56]~78_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[145]~145_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[0]~16_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[65]~79_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_9_result_int[2]~1_cout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_9_result_int[3]~3_cout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~5_cout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~7_cout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_9_result_int[6]~9_cout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ : std_logic;
SIGNAL \dec2|WideOr6~0_combout\ : std_logic;
SIGNAL \dec2|WideOr5~0_combout\ : std_logic;
SIGNAL \dec2|WideOr4~0_combout\ : std_logic;
SIGNAL \dec2|WideOr3~0_combout\ : std_logic;
SIGNAL \dec2|WideOr2~0_combout\ : std_logic;
SIGNAL \dec2|WideOr1~0_combout\ : std_logic;
SIGNAL \dec2|WideOr0~0_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[108]~115_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[107]~116_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[106]~84_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[105]~118_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[104]~86_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[103]~120_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[102]~121_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[101]~122_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[119]~89_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[106]~117_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[118]~125_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[117]~91_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[116]~92_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[115]~93_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[114]~94_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[113]~130_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[112]~131_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[130]~134_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[117]~126_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[129]~135_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[104]~119_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[116]~127_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[128]~136_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[115]~128_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[127]~137_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[126]~101_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[125]~139_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[124]~103_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[99]~142_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[99]~143_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[111]~104_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[111]~141_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[123]~105_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[141]~106_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[140]~107_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[139]~147_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[138]~109_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[137]~149_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[136]~111_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[135]~112_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[110]~153_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[122]~113_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[134]~114_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_13_result_int[3]~1_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_13_result_int[4]~3_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_13_result_int[5]~5_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_13_result_int[6]~7_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_13_result_int[7]~9_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_13_result_int[8]~11_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_13_result_int[9]~13_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ : std_logic;
SIGNAL \dec3|WideOr6~0_combout\ : std_logic;
SIGNAL \dec3|WideOr5~0_combout\ : std_logic;
SIGNAL \dec3|WideOr4~0_combout\ : std_logic;
SIGNAL \dec3|WideOr3~0_combout\ : std_logic;
SIGNAL \dec3|WideOr2~0_combout\ : std_logic;
SIGNAL \dec3|WideOr1~0_combout\ : std_logic;
SIGNAL \dec3|WideOr0~0_combout\ : std_logic;
SIGNAL \myFIFO|myRam|ram_rtl_0_bypass\ : std_logic_vector(0 TO 24);
SIGNAL \SM|state\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \count_0|cntr\ : std_logic_vector(25 DOWNTO 0);
SIGNAL \sec_cnt|count_second\ : std_logic_vector(13 DOWNTO 0);
SIGNAL \myFIFO|addr_wr\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \myFIFO|addr_rd\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \SW~combout\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \KEY~combout\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \ALT_INV_KEY~combout\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \dec3|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \dec2|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \dec1|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \dec0|ALT_INV_WideOr0~0_combout\ : std_logic;

BEGIN

ww_CLOCK_50 <= CLOCK_50;
ww_SW <= SW;
ww_KEY <= KEY;
LEDR <= ww_LEDR;
LEDG <= ww_LEDG;
HEX0 <= ww_HEX0;
HEX1 <= ww_HEX1;
HEX2 <= ww_HEX2;
HEX3 <= ww_HEX3;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTADATAIN_bus\ <= (\sec_cnt|count_second\(13) & \sec_cnt|count_second\(12) & \sec_cnt|count_second\(11) & \sec_cnt|count_second\(10) & \sec_cnt|count_second\(9) & \sec_cnt|count_second\(8) & 
\sec_cnt|count_second\(7) & \sec_cnt|count_second\(6) & \sec_cnt|count_second\(5) & \sec_cnt|count_second\(4) & \sec_cnt|count_second\(3) & \sec_cnt|count_second\(2) & \sec_cnt|count_second\(1) & \sec_cnt|count_second\(0));

\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTAADDR_bus\ <= (\myFIFO|addr_wr\(4) & \myFIFO|addr_wr\(3) & \myFIFO|addr_wr\(2) & \myFIFO|addr_wr\(1) & \myFIFO|addr_wr\(0));

\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBADDR_bus\ <= (\myFIFO|Add2~14_combout\ & \myFIFO|Add2~11_combout\ & \myFIFO|Add2~8_combout\ & \myFIFO|Add2~5_combout\ & \myFIFO|Add2~2_combout\);

\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0~portbdataout\ <= \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(0);
\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a1\ <= \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(1);
\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a2\ <= \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(2);
\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a3\ <= \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(3);
\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a4\ <= \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(4);
\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a5\ <= \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(5);
\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a6\ <= \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(6);
\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a7\ <= \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(7);
\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a8\ <= \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(8);
\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a9\ <= \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(9);
\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a10\ <= \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(10);
\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a11\ <= \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(11);
\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a12\ <= \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(12);
\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a13\ <= \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(13);

\CLOCK_50~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \CLOCK_50~combout\);
\ALT_INV_KEY~combout\(0) <= NOT \KEY~combout\(0);
\dec3|ALT_INV_WideOr0~0_combout\ <= NOT \dec3|WideOr0~0_combout\;
\dec2|ALT_INV_WideOr0~0_combout\ <= NOT \dec2|WideOr0~0_combout\;
\dec1|ALT_INV_WideOr0~0_combout\ <= NOT \dec1|WideOr0~0_combout\;
\dec0|ALT_INV_WideOr0~0_combout\ <= NOT \dec0|WideOr0~0_combout\;

-- Location: LCCOMB_X39_Y10_N18
\Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\ = (\toOut[13]~5_combout\ & (\Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ $ (GND))) # (!\toOut[13]~5_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ & VCC))
-- \Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\ = CARRY((\toOut[13]~5_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \toOut[13]~5_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\);

-- Location: LCCOMB_X39_Y9_N2
\Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[15]~136_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[15]~137_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[15]~136_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[15]~137_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[15]~136_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[15]~137_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\);

-- Location: LCCOMB_X39_Y9_N4
\Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[16]~102_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[16]~135_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[16]~102_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[16]~135_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[16]~102_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[16]~135_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[16]~102_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[16]~135_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\);

-- Location: LCCOMB_X39_Y9_N6
\Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[17]~134_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[17]~101_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[17]~134_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[17]~101_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[17]~134_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[17]~101_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[17]~134_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[17]~101_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\);

-- Location: LCCOMB_X38_Y9_N12
\Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[21]~105_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[21]~140_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[21]~105_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[21]~140_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[21]~105_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[21]~140_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[21]~105_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[21]~140_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\);

-- Location: LCCOMB_X38_Y9_N14
\Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[22]~104_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[22]~139_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[22]~104_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[22]~139_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[22]~104_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[22]~139_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[22]~104_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[22]~139_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\);

-- Location: LCCOMB_X37_Y9_N14
\Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[26]~108_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[26]~145_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[26]~108_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[26]~145_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[26]~108_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[26]~145_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[26]~108_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[26]~145_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~3\);

-- Location: LCCOMB_X37_Y10_N18
\Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[30]~152_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[30]~151_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[30]~152_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[30]~151_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[30]~152_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[30]~151_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~1\);

-- Location: LCCOMB_X37_Y10_N22
\Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[32]~149_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[32]~110_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[32]~149_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[32]~110_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[32]~149_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[32]~110_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[32]~149_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[32]~110_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~5\);

-- Location: LCCOMB_X36_Y10_N22
\Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[37]~154_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[37]~113_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[37]~154_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[37]~113_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[37]~154_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[37]~113_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[37]~154_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[37]~113_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~5\);

-- Location: LCCOMB_X35_Y10_N2
\Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[41]~117_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[41]~160_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[41]~117_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[41]~160_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[41]~117_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[41]~160_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[41]~117_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[41]~160_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~3\);

-- Location: LCCOMB_X35_Y10_N4
\Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[42]~159_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[42]~116_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[42]~159_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[42]~116_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[42]~159_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[42]~116_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[42]~159_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[42]~116_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~5\);

-- Location: LCCOMB_X30_Y10_N10
\Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[52]~122_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[52]~169_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[52]~122_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[52]~169_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[52]~122_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[52]~169_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[52]~122_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[52]~169_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~5\);

-- Location: LCCOMB_X29_Y10_N4
\Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[57]~125_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[57]~174_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[57]~125_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[57]~174_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[57]~125_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[57]~174_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[57]~125_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[57]~174_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~5\);

-- Location: LCCOMB_X29_Y10_N16
\Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[60]~179_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[60]~178_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[60]~179_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[60]~178_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[60]~179_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[60]~178_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~1\);

-- Location: LCCOMB_X29_Y10_N20
\Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[62]~128_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[62]~181_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[62]~128_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[62]~181_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[62]~128_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[62]~181_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[62]~128_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[62]~181_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~5\);

-- Location: LCCOMB_X38_Y12_N16
\Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\ = (\toOut[11]~9_combout\ & ((GND) # (!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\))) # (!\toOut[11]~9_combout\ & 
-- (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\ $ (GND)))
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\ = CARRY((\toOut[11]~9_combout\) # (!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \toOut[11]~9_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\);

-- Location: LCCOMB_X38_Y12_N18
\Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\ = (\toOut[12]~7_combout\ & (!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\)) # (!\toOut[12]~7_combout\ & 
-- ((\Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\) # (GND)))
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ = CARRY((!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\) # (!\toOut[12]~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[12]~7_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\);

-- Location: LCCOMB_X37_Y12_N20
\Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\ & ((((\Mod1|auto_generated|divider|divider|StageOut[51]~149_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[51]~105_combout\))))) # (!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\ & ((\Mod1|auto_generated|divider|divider|StageOut[51]~149_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|StageOut[51]~105_combout\) # (GND))))
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[51]~149_combout\) # ((\Mod1|auto_generated|divider|divider|StageOut[51]~105_combout\) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[51]~149_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[51]~105_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\);

-- Location: LCCOMB_X36_Y12_N14
\Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\ = (((\Mod1|auto_generated|divider|divider|StageOut[57]~158_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[57]~159_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[57]~158_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[57]~159_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[57]~158_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[57]~159_combout\,
	datad => VCC,
	combout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\);

-- Location: LCCOMB_X36_Y12_N16
\Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ & (((\Mod1|auto_generated|divider|divider|StageOut[58]~111_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[58]~157_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ & (!\Mod1|auto_generated|divider|divider|StageOut[58]~111_combout\ & 
-- (!\Mod1|auto_generated|divider|divider|StageOut[58]~157_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[58]~111_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[58]~157_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[58]~111_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[58]~157_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\);

-- Location: LCCOMB_X36_Y12_N22
\Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ & (((\Mod1|auto_generated|divider|divider|StageOut[61]~154_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[61]~108_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ & ((((\Mod1|auto_generated|divider|divider|StageOut[61]~154_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[61]~108_combout\)))))
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\ = CARRY((!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ & ((\Mod1|auto_generated|divider|divider|StageOut[61]~154_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[61]~108_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[61]~154_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[61]~108_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\);

-- Location: LCCOMB_X35_Y12_N16
\Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~0_combout\ = (((\Mod1|auto_generated|divider|divider|StageOut[65]~165_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[65]~164_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~1\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[65]~165_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[65]~164_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[65]~165_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[65]~164_combout\,
	datad => VCC,
	combout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~0_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~1\);

-- Location: LCCOMB_X35_Y12_N22
\Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~6_combout\ = (\Mod1|auto_generated|divider|divider|StageOut[68]~114_combout\ & (((!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~5\)))) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[68]~114_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[68]~162_combout\ & (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~5\)) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[68]~162_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~5\) # (GND)))))
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~7\ = CARRY(((!\Mod1|auto_generated|divider|divider|StageOut[68]~114_combout\ & !\Mod1|auto_generated|divider|divider|StageOut[68]~162_combout\)) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[68]~114_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[68]~162_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~5\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~6_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~7\);

-- Location: LCCOMB_X35_Y12_N24
\Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~8_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~7\ & (((\Mod1|auto_generated|divider|divider|StageOut[69]~113_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[69]~161_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~7\ & ((((\Mod1|auto_generated|divider|divider|StageOut[69]~113_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[69]~161_combout\)))))
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~9\ = CARRY((!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~7\ & ((\Mod1|auto_generated|divider|divider|StageOut[69]~113_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[69]~161_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[69]~113_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[69]~161_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~7\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~8_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~9\);

-- Location: LCCOMB_X34_Y12_N24
\Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~6_combout\ = (\Mod1|auto_generated|divider|divider|StageOut[76]~120_combout\ & (((!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~5\)))) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[76]~120_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[76]~168_combout\ & (!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~5\)) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[76]~168_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~5\) # (GND)))))
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~7\ = CARRY(((!\Mod1|auto_generated|divider|divider|StageOut[76]~120_combout\ & !\Mod1|auto_generated|divider|divider|StageOut[76]~168_combout\)) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[76]~120_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[76]~168_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~5\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~6_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~7\);

-- Location: LCCOMB_X34_Y12_N26
\Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~8_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~7\ & (((\Mod1|auto_generated|divider|divider|StageOut[77]~119_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[77]~167_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~7\ & ((((\Mod1|auto_generated|divider|divider|StageOut[77]~119_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[77]~167_combout\)))))
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~9\ = CARRY((!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~7\ & ((\Mod1|auto_generated|divider|divider|StageOut[77]~119_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[77]~167_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[77]~119_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[77]~167_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~7\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~8_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~9\);

-- Location: LCCOMB_X30_Y12_N16
\Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~8_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~7\ & (((\Mod1|auto_generated|divider|divider|StageOut[93]~179_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[93]~131_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~7\ & ((((\Mod1|auto_generated|divider|divider|StageOut[93]~179_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[93]~131_combout\)))))
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~9\ = CARRY((!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~7\ & ((\Mod1|auto_generated|divider|divider|StageOut[93]~179_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[93]~131_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[93]~179_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[93]~131_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~7\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~8_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~9\);

-- Location: LCCOMB_X29_Y12_N6
\Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~6_combout\ = (\Mod1|auto_generated|divider|divider|StageOut[100]~186_combout\ & (((!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~5\)))) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[100]~186_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[100]~138_combout\ & (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~5\)) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[100]~138_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~5\) # (GND)))))
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~7\ = CARRY(((!\Mod1|auto_generated|divider|divider|StageOut[100]~186_combout\ & !\Mod1|auto_generated|divider|divider|StageOut[100]~138_combout\)) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[100]~186_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[100]~138_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~5\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~6_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~7\);

-- Location: LCCOMB_X29_Y12_N8
\Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~8_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~7\ & (((\Mod1|auto_generated|divider|divider|StageOut[101]~137_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[101]~185_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~7\ & ((((\Mod1|auto_generated|divider|divider|StageOut[101]~137_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[101]~185_combout\)))))
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~9\ = CARRY((!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~7\ & ((\Mod1|auto_generated|divider|divider|StageOut[101]~137_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[101]~185_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[101]~137_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[101]~185_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~7\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~8_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~9\);

-- Location: LCCOMB_X25_Y13_N8
\Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\ = (((\Mod1|auto_generated|divider|divider|StageOut[108]~192_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[108]~144_combout\)))
-- \Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[108]~192_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[108]~144_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[108]~192_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[108]~144_combout\,
	datad => VCC,
	combout => \Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\);

-- Location: LCCOMB_X26_Y13_N20
\Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\ = (((\Div0|auto_generated|divider|divider|StageOut[15]~40_combout\) # (\Div0|auto_generated|divider|divider|StageOut[15]~41_combout\)))
-- \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\ = CARRY((\Div0|auto_generated|divider|divider|StageOut[15]~40_combout\) # (\Div0|auto_generated|divider|divider|StageOut[15]~41_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[15]~40_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[15]~41_combout\,
	datad => VCC,
	combout => \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\);

-- Location: LCCOMB_X27_Y13_N18
\Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\ = (((\Div0|auto_generated|divider|divider|StageOut[20]~45_combout\) # (\Div0|auto_generated|divider|divider|StageOut[20]~44_combout\)))
-- \Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ = CARRY((\Div0|auto_generated|divider|divider|StageOut[20]~45_combout\) # (\Div0|auto_generated|divider|divider|StageOut[20]~44_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[20]~45_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[20]~44_combout\,
	datad => VCC,
	combout => \Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\);

-- Location: LCCOMB_X27_Y13_N22
\Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ & (((\Div0|auto_generated|divider|divider|StageOut[22]~43_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[22]~30_combout\)))) # (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ & ((((\Div0|auto_generated|divider|divider|StageOut[22]~43_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[22]~30_combout\)))))
-- \Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\ = CARRY((!\Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ & ((\Div0|auto_generated|divider|divider|StageOut[22]~43_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[22]~30_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[22]~43_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[22]~30_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\,
	combout => \Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\);

-- Location: LCCOMB_X29_Y11_N12
\Mod1|auto_generated|divider|divider|add_sub_13_result_int[1]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[1]~14_combout\ = (\Mod1|auto_generated|divider|divider|StageOut[96]~196_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[96]~195_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|StageOut[96]~196_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[96]~195_combout\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[1]~14_combout\);

-- Location: LCCOMB_X36_Y11_N0
\Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\ = \toOut[7]~17_combout\ $ (VCC)
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\ = CARRY(\toOut[7]~17_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \toOut[7]~17_combout\,
	datad => VCC,
	combout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\);

-- Location: LCCOMB_X36_Y11_N4
\Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\ = (\toOut[9]~13_combout\ & (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\ $ (GND))) # (!\toOut[9]~13_combout\ & 
-- (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\ & VCC))
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\ = CARRY((\toOut[9]~13_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \toOut[9]~13_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\);

-- Location: LCCOMB_X36_Y11_N6
\Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\ = (\toOut[10]~11_combout\ & (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\)) # (!\toOut[10]~11_combout\ & 
-- ((\Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\) # (GND)))
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\) # (!\toOut[10]~11_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \toOut[10]~11_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\);

-- Location: LCCOMB_X36_Y11_N8
\Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\ = (\toOut[11]~9_combout\ & (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\ $ (GND))) # (!\toOut[11]~9_combout\ & 
-- (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\ & VCC))
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\ = CARRY((\toOut[11]~9_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \toOut[11]~9_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\);

-- Location: LCCOMB_X36_Y11_N10
\Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\ = (\toOut[12]~7_combout\ & (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\)) # (!\toOut[12]~7_combout\ & 
-- ((\Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\) # (GND)))
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\) # (!\toOut[12]~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \toOut[12]~7_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\);

-- Location: LCCOMB_X36_Y11_N12
\Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\ = (\toOut[13]~5_combout\ & (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\ $ (GND))) # (!\toOut[13]~5_combout\ & 
-- (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\ & VCC))
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\ = CARRY((\toOut[13]~5_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[13]~5_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\);

-- Location: LCCOMB_X34_Y11_N10
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\ = (((\Mod2|auto_generated|divider|divider|StageOut[101]~154_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[101]~153_combout\)))
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\ = CARRY((\Mod2|auto_generated|divider|divider|StageOut[101]~154_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[101]~153_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[101]~154_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[101]~153_combout\,
	datad => VCC,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\);

-- Location: LCCOMB_X34_Y11_N16
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[104]~108_combout\ & (((!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\)))) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[104]~108_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[104]~150_combout\ & (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\)) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[104]~150_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\) # (GND)))))
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ = CARRY(((!\Mod2|auto_generated|divider|divider|StageOut[104]~108_combout\ & !\Mod2|auto_generated|divider|divider|StageOut[104]~150_combout\)) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[104]~108_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[104]~150_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\);

-- Location: LCCOMB_X34_Y11_N18
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ & (((\Mod2|auto_generated|divider|divider|StageOut[105]~107_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[105]~149_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ & ((((\Mod2|auto_generated|divider|divider|StageOut[105]~107_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[105]~149_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ & ((\Mod2|auto_generated|divider|divider|StageOut[105]~107_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[105]~149_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[105]~107_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[105]~149_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\);

-- Location: LCCOMB_X34_Y11_N20
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[106]~106_combout\ & (((!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\)))) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[106]~106_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[106]~148_combout\ & (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\)) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[106]~148_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\) # (GND)))))
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ = CARRY(((!\Mod2|auto_generated|divider|divider|StageOut[106]~106_combout\ & !\Mod2|auto_generated|divider|divider|StageOut[106]~148_combout\)) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[106]~106_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[106]~148_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\);

-- Location: LCCOMB_X35_Y13_N14
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\ = (((\Mod2|auto_generated|divider|divider|StageOut[112]~118_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[112]~162_combout\)))
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\ = CARRY((\Mod2|auto_generated|divider|divider|StageOut[112]~118_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[112]~162_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[112]~118_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[112]~162_combout\,
	datad => VCC,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\);

-- Location: LCCOMB_X35_Y13_N18
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ & (((\Mod2|auto_generated|divider|divider|StageOut[114]~160_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[114]~116_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ & ((((\Mod2|auto_generated|divider|divider|StageOut[114]~160_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[114]~116_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ & ((\Mod2|auto_generated|divider|divider|StageOut[114]~160_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[114]~116_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[114]~160_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[114]~116_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\);

-- Location: LCCOMB_X35_Y13_N20
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[115]~159_combout\ & (((!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\)))) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[115]~159_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[115]~115_combout\ & (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\)) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[115]~115_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\) # (GND)))))
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ = CARRY(((!\Mod2|auto_generated|divider|divider|StageOut[115]~159_combout\ & !\Mod2|auto_generated|divider|divider|StageOut[115]~115_combout\)) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[115]~159_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[115]~115_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\);

-- Location: LCCOMB_X33_Y13_N16
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\ & (((\Mod2|auto_generated|divider|divider|StageOut[124]~125_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[124]~171_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\ & (!\Mod2|auto_generated|divider|divider|StageOut[124]~125_combout\ & 
-- (!\Mod2|auto_generated|divider|divider|StageOut[124]~171_combout\)))
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ = CARRY((!\Mod2|auto_generated|divider|divider|StageOut[124]~125_combout\ & (!\Mod2|auto_generated|divider|divider|StageOut[124]~171_combout\ & 
-- !\Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[124]~125_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[124]~171_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\);

-- Location: LCCOMB_X33_Y13_N18
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ & (((\Mod2|auto_generated|divider|divider|StageOut[125]~124_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[125]~170_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ & ((((\Mod2|auto_generated|divider|divider|StageOut[125]~124_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[125]~170_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ & ((\Mod2|auto_generated|divider|divider|StageOut[125]~124_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[125]~170_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[125]~124_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[125]~170_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\);

-- Location: LCCOMB_X32_Y13_N12
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~6_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[137]~132_combout\ & (((!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~5\)))) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[137]~132_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[137]~180_combout\ & (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~5\)) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[137]~180_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~5\) # (GND)))))
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~7\ = CARRY(((!\Mod2|auto_generated|divider|divider|StageOut[137]~132_combout\ & !\Mod2|auto_generated|divider|divider|StageOut[137]~180_combout\)) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[137]~132_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[137]~180_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~5\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~6_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~7\);

-- Location: LCCOMB_X31_Y13_N24
\Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[151]~138_combout\ & (((!\Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\)))) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[151]~138_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[151]~188_combout\ & (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\)) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[151]~188_combout\ & ((\Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\) # (GND)))))
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ = CARRY(((!\Mod2|auto_generated|divider|divider|StageOut[151]~138_combout\ & !\Mod2|auto_generated|divider|divider|StageOut[151]~188_combout\)) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[151]~138_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[151]~188_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\,
	combout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\);

-- Location: LCCOMB_X31_Y14_N8
\Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\ = (((\Div1|auto_generated|divider|divider|StageOut[49]~45_combout\) # (\Div1|auto_generated|divider|divider|StageOut[49]~65_combout\)))
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\ = CARRY((\Div1|auto_generated|divider|divider|StageOut[49]~45_combout\) # (\Div1|auto_generated|divider|divider|StageOut[49]~65_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[49]~45_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[49]~65_combout\,
	datad => VCC,
	combout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\);

-- Location: LCCOMB_X31_Y14_N10
\Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\ & (((\Div1|auto_generated|divider|divider|StageOut[50]~64_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[50]~44_combout\)))) # (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\ & (!\Div1|auto_generated|divider|divider|StageOut[50]~64_combout\ & 
-- (!\Div1|auto_generated|divider|divider|StageOut[50]~44_combout\)))
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\ = CARRY((!\Div1|auto_generated|divider|divider|StageOut[50]~64_combout\ & (!\Div1|auto_generated|divider|divider|StageOut[50]~44_combout\ & 
-- !\Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[50]~64_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[50]~44_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\,
	combout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\);

-- Location: LCCOMB_X31_Y14_N12
\Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\ & ((((\Div1|auto_generated|divider|divider|StageOut[51]~63_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[51]~43_combout\))))) # (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\ & ((\Div1|auto_generated|divider|divider|StageOut[51]~63_combout\) # 
-- ((\Div1|auto_generated|divider|divider|StageOut[51]~43_combout\) # (GND))))
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\ = CARRY((\Div1|auto_generated|divider|divider|StageOut[51]~63_combout\) # ((\Div1|auto_generated|divider|divider|StageOut[51]~43_combout\) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[51]~63_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[51]~43_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\,
	combout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\);

-- Location: LCCOMB_X31_Y14_N14
\Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\ = (\Div1|auto_generated|divider|divider|StageOut[52]~42_combout\ & (((!\Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\)))) # 
-- (!\Div1|auto_generated|divider|divider|StageOut[52]~42_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[52]~62_combout\ & (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\)) # 
-- (!\Div1|auto_generated|divider|divider|StageOut[52]~62_combout\ & ((\Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\) # (GND)))))
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ = CARRY(((!\Div1|auto_generated|divider|divider|StageOut[52]~42_combout\ & !\Div1|auto_generated|divider|divider|StageOut[52]~62_combout\)) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[52]~42_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[52]~62_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\,
	combout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\);

-- Location: LCCOMB_X29_Y16_N26
\Div1|auto_generated|divider|divider|add_sub_7_result_int[1]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[1]~14_combout\ = (\Div1|auto_generated|divider|divider|StageOut[48]~71_combout\) # (\Div1|auto_generated|divider|divider|StageOut[48]~51_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|StageOut[48]~71_combout\,
	datad => \Div1|auto_generated|divider|divider|StageOut[48]~51_combout\,
	combout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[1]~14_combout\);

-- Location: LCCOMB_X30_Y14_N6
\Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\ = (((\Div1|auto_generated|divider|divider|StageOut[57]~52_combout\) # (\Div1|auto_generated|divider|divider|StageOut[57]~72_combout\)))
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ = CARRY((\Div1|auto_generated|divider|divider|StageOut[57]~52_combout\) # (\Div1|auto_generated|divider|divider|StageOut[57]~72_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[57]~52_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[57]~72_combout\,
	datad => VCC,
	combout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\);

-- Location: LCCOMB_X30_Y14_N8
\Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ & (((\Div1|auto_generated|divider|divider|StageOut[58]~50_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[58]~70_combout\)))) # (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ & (!\Div1|auto_generated|divider|divider|StageOut[58]~50_combout\ & 
-- (!\Div1|auto_generated|divider|divider|StageOut[58]~70_combout\)))
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ = CARRY((!\Div1|auto_generated|divider|divider|StageOut[58]~50_combout\ & (!\Div1|auto_generated|divider|divider|StageOut[58]~70_combout\ & 
-- !\Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[58]~50_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[58]~70_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\,
	combout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\);

-- Location: LCCOMB_X32_Y14_N10
\Div1|auto_generated|divider|divider|add_sub_8_result_int[1]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[1]~14_combout\ = (\Div1|auto_generated|divider|divider|StageOut[56]~78_combout\) # (\Div1|auto_generated|divider|divider|StageOut[56]~58_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|StageOut[56]~78_combout\,
	datad => \Div1|auto_generated|divider|divider|StageOut[56]~58_combout\,
	combout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[1]~14_combout\);

-- Location: LCCOMB_X36_Y11_N16
\Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\ = \toOut[7]~17_combout\ $ (VCC)
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\ = CARRY(\toOut[7]~17_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \toOut[7]~17_combout\,
	datad => VCC,
	combout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\);

-- Location: LCCOMB_X36_Y11_N26
\Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\ = (\toOut[12]~7_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\)) # (!\toOut[12]~7_combout\ & 
-- ((\Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\) # (GND)))
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\) # (!\toOut[12]~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \toOut[12]~7_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\,
	combout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\);

-- Location: LCCOMB_X36_Y11_N28
\Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\ = (\toOut[13]~5_combout\ & (\Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\ $ (GND))) # (!\toOut[13]~5_combout\ & 
-- (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\ & VCC))
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\ = CARRY((\toOut[13]~5_combout\ & !\Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[13]~5_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\,
	combout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\);

-- Location: LCCOMB_X35_Y9_N10
\Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\ = (\Div2|auto_generated|divider|divider|StageOut[106]~117_combout\ & (((!\Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\)))) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[106]~117_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[106]~84_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\)) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[106]~84_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\) # (GND)))))
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ = CARRY(((!\Div2|auto_generated|divider|divider|StageOut[106]~117_combout\ & !\Div2|auto_generated|divider|divider|StageOut[106]~84_combout\)) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[106]~117_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[106]~84_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\);

-- Location: LCCOMB_X35_Y9_N12
\Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ & (((\Div2|auto_generated|divider|divider|StageOut[107]~83_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[107]~116_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ & ((((\Div2|auto_generated|divider|divider|StageOut[107]~83_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[107]~116_combout\)))))
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ & ((\Div2|auto_generated|divider|divider|StageOut[107]~83_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[107]~116_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[107]~83_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[107]~116_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\);

-- Location: LCCOMB_X34_Y9_N24
\Div2|auto_generated|divider|divider|add_sub_10_result_int[2]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\ = (\Div2|auto_generated|divider|divider|StageOut[100]~132_combout\) # (\Div2|auto_generated|divider|divider|StageOut[100]~133_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|StageOut[100]~132_combout\,
	datad => \Div2|auto_generated|divider|divider|StageOut[100]~133_combout\,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\);

-- Location: LCCOMB_X36_Y8_N10
\Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\ = (((\Div2|auto_generated|divider|divider|StageOut[112]~96_combout\) # (\Div2|auto_generated|divider|divider|StageOut[112]~131_combout\)))
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\ = CARRY((\Div2|auto_generated|divider|divider|StageOut[112]~96_combout\) # (\Div2|auto_generated|divider|divider|StageOut[112]~131_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[112]~96_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[112]~131_combout\,
	datad => VCC,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\);

-- Location: LCCOMB_X36_Y8_N14
\Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ & (((\Div2|auto_generated|divider|divider|StageOut[114]~129_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[114]~94_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ & ((((\Div2|auto_generated|divider|divider|StageOut[114]~129_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[114]~94_combout\)))))
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ & ((\Div2|auto_generated|divider|divider|StageOut[114]~129_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[114]~94_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[114]~129_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[114]~94_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\);

-- Location: LCCOMB_X36_Y8_N16
\Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\ = (\Div2|auto_generated|divider|divider|StageOut[115]~128_combout\ & (((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\)))) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[115]~128_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[115]~93_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\)) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[115]~93_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\) # (GND)))))
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ = CARRY(((!\Div2|auto_generated|divider|divider|StageOut[115]~128_combout\ & !\Div2|auto_generated|divider|divider|StageOut[115]~93_combout\)) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[115]~128_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[115]~93_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\);

-- Location: LCCOMB_X36_Y8_N18
\Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ & (((\Div2|auto_generated|divider|divider|StageOut[116]~127_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[116]~92_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ & ((((\Div2|auto_generated|divider|divider|StageOut[116]~127_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[116]~92_combout\)))))
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ & ((\Div2|auto_generated|divider|divider|StageOut[116]~127_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[116]~92_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[116]~127_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[116]~92_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\);

-- Location: LCCOMB_X36_Y8_N20
\Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\ = (\Div2|auto_generated|divider|divider|StageOut[117]~126_combout\ & (((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\)))) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[117]~126_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[117]~91_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\)) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[117]~91_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\) # (GND)))))
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ = CARRY(((!\Div2|auto_generated|divider|divider|StageOut[117]~126_combout\ & !\Div2|auto_generated|divider|divider|StageOut[117]~91_combout\)) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[117]~126_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[117]~91_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\);

-- Location: LCCOMB_X36_Y8_N22
\Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ & (((\Div2|auto_generated|divider|divider|StageOut[118]~90_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[118]~125_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ & ((((\Div2|auto_generated|divider|divider|StageOut[118]~90_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[118]~125_combout\)))))
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ & ((\Div2|auto_generated|divider|divider|StageOut[118]~90_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[118]~125_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[118]~90_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[118]~125_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\);

-- Location: LCCOMB_X35_Y8_N2
\Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\ = (((\Div2|auto_generated|divider|divider|StageOut[123]~144_combout\) # (\Div2|auto_generated|divider|divider|StageOut[123]~105_combout\)))
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\ = CARRY((\Div2|auto_generated|divider|divider|StageOut[123]~144_combout\) # (\Div2|auto_generated|divider|divider|StageOut[123]~105_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[123]~144_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[123]~105_combout\,
	datad => VCC,
	combout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\);

-- Location: LCCOMB_X35_Y8_N4
\Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\ & (((\Div2|auto_generated|divider|divider|StageOut[124]~140_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[124]~103_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\ & (!\Div2|auto_generated|divider|divider|StageOut[124]~140_combout\ & 
-- (!\Div2|auto_generated|divider|divider|StageOut[124]~103_combout\)))
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ = CARRY((!\Div2|auto_generated|divider|divider|StageOut[124]~140_combout\ & (!\Div2|auto_generated|divider|divider|StageOut[124]~103_combout\ & 
-- !\Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[124]~140_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[124]~103_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\,
	combout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\);

-- Location: LCCOMB_X35_Y8_N6
\Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ & (((\Div2|auto_generated|divider|divider|StageOut[125]~102_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[125]~139_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ & ((((\Div2|auto_generated|divider|divider|StageOut[125]~102_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[125]~139_combout\)))))
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ & ((\Div2|auto_generated|divider|divider|StageOut[125]~102_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[125]~139_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[125]~102_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[125]~139_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\,
	combout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\);

-- Location: LCCOMB_X35_Y8_N10
\Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ & (((\Div2|auto_generated|divider|divider|StageOut[127]~100_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[127]~137_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ & ((((\Div2|auto_generated|divider|divider|StageOut[127]~100_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[127]~137_combout\)))))
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ & ((\Div2|auto_generated|divider|divider|StageOut[127]~100_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[127]~137_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[127]~100_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[127]~137_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\,
	combout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\);

-- Location: LCFF_X32_Y9_N11
\count_0|cntr[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[2]~32_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(2));

-- Location: LCFF_X32_Y8_N21
\count_0|cntr[23]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[23]~74_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(23));

-- Location: LCFF_X32_Y8_N25
\count_0|cntr[25]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[25]~78_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(25));

-- Location: LCCOMB_X32_Y9_N10
\count_0|cntr[2]~32\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[2]~32_combout\ = (\count_0|cntr\(2) & (\count_0|cntr[1]~31\ $ (GND))) # (!\count_0|cntr\(2) & (!\count_0|cntr[1]~31\ & VCC))
-- \count_0|cntr[2]~33\ = CARRY((\count_0|cntr\(2) & !\count_0|cntr[1]~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(2),
	datad => VCC,
	cin => \count_0|cntr[1]~31\,
	combout => \count_0|cntr[2]~32_combout\,
	cout => \count_0|cntr[2]~33\);

-- Location: LCCOMB_X32_Y8_N20
\count_0|cntr[23]~74\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[23]~74_combout\ = (\count_0|cntr\(23) & (!\count_0|cntr[22]~73\)) # (!\count_0|cntr\(23) & ((\count_0|cntr[22]~73\) # (GND)))
-- \count_0|cntr[23]~75\ = CARRY((!\count_0|cntr[22]~73\) # (!\count_0|cntr\(23)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(23),
	datad => VCC,
	cin => \count_0|cntr[22]~73\,
	combout => \count_0|cntr[23]~74_combout\,
	cout => \count_0|cntr[23]~75\);

-- Location: LCCOMB_X32_Y8_N22
\count_0|cntr[24]~76\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[24]~76_combout\ = (\count_0|cntr\(24) & (\count_0|cntr[23]~75\ $ (GND))) # (!\count_0|cntr\(24) & (!\count_0|cntr[23]~75\ & VCC))
-- \count_0|cntr[24]~77\ = CARRY((\count_0|cntr\(24) & !\count_0|cntr[23]~75\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(24),
	datad => VCC,
	cin => \count_0|cntr[23]~75\,
	combout => \count_0|cntr[24]~76_combout\,
	cout => \count_0|cntr[24]~77\);

-- Location: LCCOMB_X32_Y8_N24
\count_0|cntr[25]~78\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[25]~78_combout\ = \count_0|cntr\(25) $ (\count_0|cntr[24]~77\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(25),
	cin => \count_0|cntr[24]~77\,
	combout => \count_0|cntr[25]~78_combout\);

-- Location: LCCOMB_X39_Y10_N4
\Mod0|auto_generated|divider|divider|StageOut[18]~100\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[18]~100_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & \Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[18]~100_combout\);

-- Location: LCCOMB_X39_Y9_N14
\Mod0|auto_generated|divider|divider|StageOut[16]~102\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[16]~102_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[16]~102_combout\);

-- Location: LCCOMB_X38_Y9_N20
\Mod0|auto_generated|divider|divider|StageOut[23]~103\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[23]~103_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[23]~103_combout\);

-- Location: LCCOMB_X38_Y9_N22
\Mod0|auto_generated|divider|divider|StageOut[22]~104\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[22]~104_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[22]~104_combout\);

-- Location: LCCOMB_X38_Y9_N24
\Mod0|auto_generated|divider|divider|StageOut[21]~105\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[21]~105_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[21]~105_combout\);

-- Location: LCCOMB_X38_Y9_N26
\Mod0|auto_generated|divider|divider|StageOut[28]~106\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[28]~106_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[28]~106_combout\);

-- Location: LCCOMB_X37_Y9_N24
\Mod0|auto_generated|divider|divider|StageOut[27]~107\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[27]~107_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[27]~107_combout\);

-- Location: LCCOMB_X37_Y9_N10
\Mod0|auto_generated|divider|divider|StageOut[26]~108\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[26]~108_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[26]~108_combout\);

-- Location: LCCOMB_X33_Y11_N18
\toOut[8]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[8]~15_combout\ = (\toOut[8]~14_combout\) # ((\sec_cnt|count_second\(8) & \SW~combout\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(8),
	datac => \SW~combout\(9),
	datad => \toOut[8]~14_combout\,
	combout => \toOut[8]~15_combout\);

-- Location: LCCOMB_X36_Y10_N12
\Mod0|auto_generated|divider|divider|StageOut[38]~112\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[38]~112_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~4_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~4_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[38]~112_combout\);

-- Location: LCCOMB_X36_Y10_N16
\Mod0|auto_generated|divider|divider|StageOut[36]~114\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[36]~114_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~0_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[36]~114_combout\);

-- Location: LCCOMB_X36_Y10_N2
\Mod0|auto_generated|divider|divider|StageOut[43]~115\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[43]~115_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~4_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~4_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[43]~115_combout\);

-- Location: LCCOMB_X35_Y10_N20
\Mod0|auto_generated|divider|divider|StageOut[41]~117\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[41]~117_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~0_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[41]~117_combout\);

-- Location: LCCOMB_X34_Y10_N24
\Mod0|auto_generated|divider|divider|StageOut[48]~118\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[48]~118_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~4_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[48]~118_combout\);

-- Location: LCCOMB_X34_Y10_N30
\Mod0|auto_generated|divider|divider|StageOut[47]~119\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[47]~119_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~2_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[47]~119_combout\);

-- Location: LCCOMB_X34_Y10_N16
\Mod0|auto_generated|divider|divider|StageOut[46]~120\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[46]~120_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~0_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~0_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[46]~120_combout\);

-- Location: LCCOMB_X31_Y10_N26
\Mod0|auto_generated|divider|divider|StageOut[52]~122\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[52]~122_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~2_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[52]~122_combout\);

-- Location: LCCOMB_X30_Y10_N4
\Mod0|auto_generated|divider|divider|StageOut[58]~124\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[58]~124_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~4_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[58]~124_combout\);

-- Location: LCCOMB_X30_Y10_N2
\Mod0|auto_generated|divider|divider|StageOut[57]~125\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[57]~125_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~2_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[57]~125_combout\);

-- Location: LCCOMB_X29_Y10_N10
\Mod0|auto_generated|divider|divider|StageOut[63]~127\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[63]~127_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~4_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~4_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[63]~127_combout\);

-- Location: LCCOMB_X38_Y12_N6
\Mod1|auto_generated|divider|divider|StageOut[53]~103\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[53]~103_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[53]~103_combout\);

-- Location: LCCOMB_X38_Y12_N8
\Mod1|auto_generated|divider|divider|StageOut[52]~104\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[52]~104_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[52]~104_combout\);

-- Location: LCCOMB_X36_Y12_N6
\Mod1|auto_generated|divider|divider|StageOut[60]~109\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[60]~109_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[60]~109_combout\);

-- Location: LCCOMB_X36_Y12_N10
\Mod1|auto_generated|divider|divider|StageOut[58]~111\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[58]~111_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[58]~111_combout\);

-- Location: LCCOMB_X35_Y12_N0
\Mod1|auto_generated|divider|divider|StageOut[70]~112\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[70]~112_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[70]~112_combout\);

-- Location: LCCOMB_X35_Y12_N6
\Mod1|auto_generated|divider|divider|StageOut[69]~113\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[69]~113_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[69]~113_combout\);

-- Location: LCCOMB_X35_Y12_N4
\Mod1|auto_generated|divider|divider|StageOut[68]~114\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[68]~114_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[68]~114_combout\);

-- Location: LCCOMB_X35_Y12_N12
\Mod1|auto_generated|divider|divider|StageOut[66]~117\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[66]~117_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[66]~117_combout\);

-- Location: LCCOMB_X34_Y12_N16
\Mod1|auto_generated|divider|divider|StageOut[78]~118\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[78]~118_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~8_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[78]~118_combout\);

-- Location: LCCOMB_X34_Y12_N10
\Mod1|auto_generated|divider|divider|StageOut[77]~119\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[77]~119_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~6_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~6_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[77]~119_combout\);

-- Location: LCCOMB_X34_Y12_N12
\Mod1|auto_generated|divider|divider|StageOut[76]~120\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[76]~120_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~4_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[76]~120_combout\);

-- Location: LCCOMB_X34_Y12_N0
\Mod1|auto_generated|divider|divider|StageOut[74]~123\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[74]~123_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~0_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[74]~123_combout\);

-- Location: LCCOMB_X32_Y12_N24
\Mod1|auto_generated|divider|divider|StageOut[86]~124\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[86]~124_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~8_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~8_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[86]~124_combout\);

-- Location: LCCOMB_X33_Y12_N6
\Mod1|auto_generated|divider|divider|StageOut[85]~125\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[85]~125_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~6_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~6_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[85]~125_combout\);

-- Location: LCCOMB_X32_Y12_N16
\Mod1|auto_generated|divider|divider|StageOut[82]~129\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[82]~129_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~0_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~0_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[82]~129_combout\);

-- Location: LCCOMB_X30_Y12_N6
\Mod1|auto_generated|divider|divider|StageOut[92]~132\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[92]~132_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~4_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~4_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[92]~132_combout\);

-- Location: LCCOMB_X29_Y12_N14
\Mod1|auto_generated|divider|divider|StageOut[102]~136\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[102]~136_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~8_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~8_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[102]~136_combout\);

-- Location: LCCOMB_X29_Y12_N20
\Mod1|auto_generated|divider|divider|StageOut[101]~137\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[101]~137_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~6_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~6_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[101]~137_combout\);

-- Location: LCCOMB_X29_Y12_N24
\Mod1|auto_generated|divider|divider|StageOut[99]~139\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[99]~139_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~2_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[99]~139_combout\);

-- Location: LCCOMB_X29_Y12_N18
\Mod1|auto_generated|divider|divider|StageOut[98]~141\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[98]~141_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~0_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~0_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[98]~141_combout\);

-- Location: LCCOMB_X25_Y13_N20
\Mod1|auto_generated|divider|divider|StageOut[110]~142\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[110]~142_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~8_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[110]~142_combout\);

-- Location: LCCOMB_X25_Y13_N6
\Mod1|auto_generated|divider|divider|StageOut[109]~143\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[109]~143_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~6_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[109]~143_combout\);

-- Location: LCCOMB_X26_Y13_N6
\Div0|auto_generated|divider|divider|StageOut[16]~28\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[16]~28_combout\ = (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & \Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[16]~28_combout\);

-- Location: LCCOMB_X27_Y13_N12
\Div0|auto_generated|divider|divider|StageOut[21]~32\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[21]~32_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\ & !\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[21]~32_combout\);

-- Location: LCCOMB_X27_Y13_N14
\Div0|auto_generated|divider|divider|StageOut[28]~33\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[28]~33_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\ & !\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[28]~33_combout\);

-- Location: LCCOMB_X27_Y13_N8
\Div0|auto_generated|divider|divider|StageOut[26]~36\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[26]~36_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\ & !\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[26]~36_combout\);

-- Location: LCCOMB_X35_Y11_N26
\Mod2|auto_generated|divider|divider|StageOut[108]~104\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[108]~104_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[108]~104_combout\);

-- Location: LCCOMB_X34_Y11_N4
\Mod2|auto_generated|divider|divider|StageOut[107]~105\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[107]~105_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[107]~105_combout\);

-- Location: LCCOMB_X34_Y11_N6
\Mod2|auto_generated|divider|divider|StageOut[106]~106\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[106]~106_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[106]~106_combout\);

-- Location: LCCOMB_X35_Y11_N20
\Mod2|auto_generated|divider|divider|StageOut[105]~107\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[105]~107_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[105]~107_combout\);

-- Location: LCCOMB_X34_Y11_N8
\Mod2|auto_generated|divider|divider|StageOut[104]~108\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[104]~108_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[104]~108_combout\);

-- Location: LCCOMB_X35_Y11_N22
\Mod2|auto_generated|divider|divider|StageOut[102]~110\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[102]~110_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[102]~110_combout\);

-- Location: LCCOMB_X35_Y13_N6
\Mod2|auto_generated|divider|divider|StageOut[118]~112\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[118]~112_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[118]~112_combout\);

-- Location: LCCOMB_X34_Y13_N12
\Mod2|auto_generated|divider|divider|StageOut[113]~117\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[113]~117_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[113]~117_combout\);

-- Location: LCCOMB_X34_Y13_N30
\Mod2|auto_generated|divider|divider|StageOut[112]~118\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[112]~118_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[112]~118_combout\);

-- Location: LCCOMB_X34_Y13_N26
\Mod2|auto_generated|divider|divider|StageOut[127]~122\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[127]~122_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[127]~122_combout\);

-- Location: LCCOMB_X34_Y13_N16
\Mod2|auto_generated|divider|divider|StageOut[125]~124\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[125]~124_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[125]~124_combout\);

-- Location: LCCOMB_X33_Y13_N6
\Mod2|auto_generated|divider|divider|StageOut[124]~125\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[124]~125_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[124]~125_combout\);

-- Location: LCCOMB_X33_Y11_N10
\Mod2|auto_generated|divider|divider|StageOut[123]~127\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[123]~127_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[123]~127_combout\);

-- Location: LCCOMB_X33_Y13_N2
\Mod2|auto_generated|divider|divider|StageOut[139]~130\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[139]~130_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[139]~130_combout\);

-- Location: LCCOMB_X32_Y13_N24
\Mod2|auto_generated|divider|divider|StageOut[138]~131\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[138]~131_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[138]~131_combout\);

-- Location: LCCOMB_X30_Y13_N26
\Mod2|auto_generated|divider|divider|StageOut[137]~132\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[137]~132_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[137]~132_combout\);

-- Location: LCCOMB_X32_Y13_N26
\Mod2|auto_generated|divider|divider|StageOut[136]~133\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[136]~133_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[136]~133_combout\);

-- Location: LCCOMB_X32_Y14_N8
\Mod2|auto_generated|divider|divider|StageOut[134]~136\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[134]~136_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[134]~136_combout\);

-- Location: LCCOMB_X31_Y13_N16
\Mod2|auto_generated|divider|divider|StageOut[151]~138\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[151]~138_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~10_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[151]~138_combout\);

-- Location: LCCOMB_X31_Y13_N10
\Mod2|auto_generated|divider|divider|StageOut[150]~139\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[150]~139_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~8_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[150]~139_combout\);

-- Location: LCCOMB_X31_Y13_N12
\Mod2|auto_generated|divider|divider|StageOut[149]~140\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[149]~140_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~6_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[149]~140_combout\);

-- Location: LCCOMB_X31_Y13_N6
\Div1|auto_generated|divider|divider|StageOut[53]~41\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[53]~41_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\ & !\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[53]~41_combout\);

-- Location: LCCOMB_X31_Y14_N22
\Div1|auto_generated|divider|divider|StageOut[52]~42\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[52]~42_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\ & !\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[52]~42_combout\);

-- Location: LCCOMB_X31_Y14_N6
\Div1|auto_generated|divider|divider|StageOut[49]~45\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[49]~45_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[1]~12_combout\ & !\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[1]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[49]~45_combout\);

-- Location: LCCOMB_X30_Y14_N24
\Div1|auto_generated|divider|divider|StageOut[61]~47\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[61]~47_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[61]~47_combout\);

-- Location: LCCOMB_X30_Y14_N20
\Div1|auto_generated|divider|divider|StageOut[60]~48\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[60]~48_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[60]~48_combout\);

-- Location: LCCOMB_X30_Y14_N30
\Div1|auto_generated|divider|divider|StageOut[59]~49\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[59]~49_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[59]~49_combout\);

-- Location: LCCOMB_X29_Y14_N0
\Div1|auto_generated|divider|divider|StageOut[58]~50\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[58]~50_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[58]~50_combout\);

-- Location: LCCOMB_X29_Y16_N10
\Div1|auto_generated|divider|divider|StageOut[48]~51\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[48]~51_combout\ = (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \Div1|auto_generated|divider|divider|add_sub_6_result_int[0]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[0]~14_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[48]~51_combout\);

-- Location: LCCOMB_X29_Y14_N20
\Div1|auto_generated|divider|divider|StageOut[57]~52\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[57]~52_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[1]~14_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[1]~14_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[57]~52_combout\);

-- Location: LCCOMB_X29_Y14_N24
\Div1|auto_generated|divider|divider|StageOut[67]~56\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[67]~56_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\ & !\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[67]~56_combout\);

-- Location: LCCOMB_X32_Y14_N6
\Div1|auto_generated|divider|divider|StageOut[56]~58\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[56]~58_combout\ = (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & \Div1|auto_generated|divider|divider|add_sub_7_result_int[0]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[0]~16_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[56]~58_combout\);

-- Location: LCCOMB_X32_Y14_N28
\Div1|auto_generated|divider|divider|StageOut[65]~59\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[65]~59_combout\ = (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & \Div1|auto_generated|divider|divider|add_sub_8_result_int[1]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_8_result_int[1]~14_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[65]~59_combout\);

-- Location: LCCOMB_X36_Y9_N20
\Div2|auto_generated|divider|divider|StageOut[108]~82\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[108]~82_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\ & !\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[108]~82_combout\);

-- Location: LCCOMB_X36_Y9_N6
\Div2|auto_generated|divider|divider|StageOut[107]~83\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[107]~83_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\ & !\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[107]~83_combout\);

-- Location: LCCOMB_X35_Y9_N22
\Div2|auto_generated|divider|divider|StageOut[105]~85\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[105]~85_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\ & !\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[105]~85_combout\);

-- Location: LCCOMB_X36_Y9_N26
\Div2|auto_generated|divider|divider|StageOut[103]~87\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[103]~87_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\ & !\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[103]~87_combout\);

-- Location: LCCOMB_X35_Y9_N26
\Div2|auto_generated|divider|divider|StageOut[102]~88\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[102]~88_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\ & !\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[102]~88_combout\);

-- Location: LCCOMB_X36_Y8_N4
\Div2|auto_generated|divider|divider|StageOut[118]~90\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[118]~90_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\ & !\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[118]~90_combout\);

-- Location: LCCOMB_X35_Y8_N20
\Div2|auto_generated|divider|divider|StageOut[113]~95\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[113]~95_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[113]~95_combout\);

-- Location: LCCOMB_X34_Y9_N10
\Div2|auto_generated|divider|divider|StageOut[112]~96\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[112]~96_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\ & !\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[112]~96_combout\);

-- Location: LCCOMB_X36_Y8_N28
\Div2|auto_generated|divider|divider|StageOut[130]~97\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[130]~97_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[130]~97_combout\);

-- Location: LCCOMB_X35_Y8_N24
\Div2|auto_generated|divider|divider|StageOut[129]~98\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[129]~98_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[129]~98_combout\);

-- Location: LCCOMB_X37_Y8_N12
\Div2|auto_generated|divider|divider|StageOut[128]~99\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[128]~99_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[128]~99_combout\);

-- Location: LCCOMB_X35_Y8_N22
\Div2|auto_generated|divider|divider|StageOut[127]~100\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[127]~100_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[127]~100_combout\);

-- Location: LCCOMB_X36_Y8_N6
\Div2|auto_generated|divider|divider|StageOut[125]~102\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[125]~102_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[125]~102_combout\);

-- Location: LCCOMB_X34_Y8_N22
\Div2|auto_generated|divider|divider|StageOut[139]~108\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[139]~108_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\ & !\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[139]~108_combout\);

-- Location: LCCOMB_X34_Y8_N30
\Div2|auto_generated|divider|divider|StageOut[137]~110\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[137]~110_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\ & !\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[137]~110_combout\);

-- Location: LCCOMB_X31_Y10_N30
\SM|Mux1~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \SM|Mux1~1_combout\ = (\SM|state\(0) & (!\SW~combout\(1) & ((!\SW~combout\(2))))) # (!\SM|state\(0) & (((!\SW~combout\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001101000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(1),
	datab => \SM|state\(0),
	datac => \SW~combout\(3),
	datad => \SW~combout\(2),
	combout => \SM|Mux1~1_combout\);

-- Location: LCCOMB_X30_Y9_N20
\sec_cnt|Equal0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|Equal0~0_combout\ = (\sec_cnt|count_second\(11)) # ((\sec_cnt|count_second\(12)) # ((!\sec_cnt|count_second\(13)) # (!\sec_cnt|count_second\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(11),
	datab => \sec_cnt|count_second\(12),
	datac => \sec_cnt|count_second\(10),
	datad => \sec_cnt|count_second\(13),
	combout => \sec_cnt|Equal0~0_combout\);

-- Location: LCCOMB_X32_Y9_N4
\count_0|Equal0~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|Equal0~3_combout\ = (\count_0|cntr\(14)) # (((\count_0|cntr\(12)) # (!\count_0|cntr\(13))) # (!\count_0|cntr\(15)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(14),
	datab => \count_0|cntr\(15),
	datac => \count_0|cntr\(13),
	datad => \count_0|cntr\(12),
	combout => \count_0|Equal0~3_combout\);

-- Location: LCCOMB_X39_Y9_N20
\Mod0|auto_generated|divider|divider|StageOut[15]~136\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[15]~136_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & ((\toOut[10]~10_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(10)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[10]~10_combout\,
	datab => \SW~combout\(9),
	datac => \sec_cnt|count_second\(10),
	datad => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[15]~136_combout\);

-- Location: LCCOMB_X38_Y9_N6
\Mod0|auto_generated|divider|divider|StageOut[20]~142\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[20]~142_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\toOut[9]~12_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \toOut[9]~12_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(9),
	combout => \Mod0|auto_generated|divider|divider|StageOut[20]~142_combout\);

-- Location: LCCOMB_X37_Y9_N8
\Mod0|auto_generated|divider|divider|StageOut[25]~147\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[25]~147_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\toOut[8]~14_combout\) # ((\sec_cnt|count_second\(8) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(8),
	datab => \SW~combout\(9),
	datac => \toOut[8]~14_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[25]~147_combout\);

-- Location: LCCOMB_X37_Y9_N30
\Mod0|auto_generated|divider|divider|StageOut[33]~148\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[33]~148_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[27]~144_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[27]~144_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[33]~148_combout\);

-- Location: LCCOMB_X37_Y10_N12
\Mod0|auto_generated|divider|divider|StageOut[31]~150\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[31]~150_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ & ((\toOut[8]~14_combout\) # ((\sec_cnt|count_second\(8) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[8]~14_combout\,
	datab => \sec_cnt|count_second\(8),
	datac => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datad => \SW~combout\(9),
	combout => \Mod0|auto_generated|divider|divider|StageOut[31]~150_combout\);

-- Location: LCCOMB_X37_Y10_N16
\Mod0|auto_generated|divider|divider|StageOut[30]~152\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[30]~152_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ & ((\toOut[7]~16_combout\) # ((\sec_cnt|count_second\(7) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(7),
	datab => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datac => \toOut[7]~16_combout\,
	datad => \SW~combout\(9),
	combout => \Mod0|auto_generated|divider|divider|StageOut[30]~152_combout\);

-- Location: LCCOMB_X37_Y10_N4
\Mod0|auto_generated|divider|divider|StageOut[37]~154\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[37]~154_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[31]~150_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~0_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[31]~150_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~0_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[37]~154_combout\);

-- Location: LCCOMB_X36_Y10_N10
\Mod0|auto_generated|divider|divider|StageOut[35]~156\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[35]~156_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\ & ((\toOut[6]~18_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \toOut[6]~18_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(6),
	combout => \Mod0|auto_generated|divider|divider|StageOut[35]~156_combout\);

-- Location: LCCOMB_X35_Y10_N12
\Mod0|auto_generated|divider|divider|StageOut[40]~161\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[40]~161_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ & ((\toOut[5]~19_combout\) # ((\sec_cnt|count_second\(5) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(5),
	datab => \SW~combout\(9),
	datac => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	datad => \toOut[5]~19_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[40]~161_combout\);

-- Location: LCCOMB_X34_Y10_N20
\Mod0|auto_generated|divider|divider|StageOut[45]~166\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[45]~166_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ & ((\toOut[4]~20_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \toOut[4]~20_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(4),
	combout => \Mod0|auto_generated|divider|divider|StageOut[45]~166_combout\);

-- Location: LCCOMB_X34_Y10_N0
\Mod0|auto_generated|divider|divider|StageOut[53]~168\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[53]~168_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[47]~164_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~2_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|StageOut[47]~164_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[53]~168_combout\);

-- Location: LCCOMB_X30_Y10_N16
\Mod0|auto_generated|divider|divider|StageOut[50]~172\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[50]~172_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ & ((\toOut[3]~21_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	datab => \SW~combout\(9),
	datac => \sec_cnt|count_second\(3),
	datad => \toOut[3]~21_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[50]~172_combout\);

-- Location: LCCOMB_X30_Y9_N4
\Mod0|auto_generated|divider|divider|StageOut[55]~176\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[55]~176_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ & ((\toOut[2]~22_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \toOut[2]~22_combout\,
	datac => \sec_cnt|count_second\(2),
	datad => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[55]~176_combout\);

-- Location: LCCOMB_X30_Y9_N22
\Mod0|auto_generated|divider|divider|StageOut[60]~179\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[60]~179_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ & ((\toOut[1]~2_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[1]~2_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\,
	datac => \SW~combout\(9),
	datad => \sec_cnt|count_second\(1),
	combout => \Mod0|auto_generated|divider|divider|StageOut[60]~179_combout\);

-- Location: LCCOMB_X30_Y9_N0
\Mod0|auto_generated|divider|divider|StageOut[61]~182\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[61]~182_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ & ((\toOut[2]~22_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\,
	datac => \sec_cnt|count_second\(2),
	datad => \toOut[2]~22_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[61]~182_combout\);

-- Location: LCCOMB_X37_Y12_N12
\Mod1|auto_generated|divider|divider|StageOut[54]~146\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[54]~146_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\toOut[13]~4_combout\) # ((\sec_cnt|count_second\(13) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(13),
	datab => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \SW~combout\(9),
	datad => \toOut[13]~4_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[54]~146_combout\);

-- Location: LCCOMB_X38_Y12_N2
\Mod1|auto_generated|divider|divider|StageOut[51]~149\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[51]~149_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\toOut[10]~10_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(10)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \sec_cnt|count_second\(10),
	datac => \toOut[10]~10_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[51]~149_combout\);

-- Location: LCCOMB_X38_Y12_N4
\Mod1|auto_generated|divider|divider|StageOut[50]~150\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[50]~150_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\toOut[9]~12_combout\) # ((\sec_cnt|count_second\(9) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datab => \sec_cnt|count_second\(9),
	datac => \SW~combout\(9),
	datad => \toOut[9]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[50]~150_combout\);

-- Location: LCCOMB_X37_Y12_N10
\Mod1|auto_generated|divider|divider|StageOut[49]~151\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[49]~151_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\toOut[8]~14_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(8)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[8]~14_combout\,
	datab => \SW~combout\(9),
	datac => \sec_cnt|count_second\(8),
	datad => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[49]~151_combout\);

-- Location: LCCOMB_X37_Y12_N6
\Mod1|auto_generated|divider|divider|StageOut[62]~153\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[62]~153_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[53]~147_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \Mod1|auto_generated|divider|divider|StageOut[53]~147_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[62]~153_combout\);

-- Location: LCCOMB_X37_Y12_N4
\Mod1|auto_generated|divider|divider|StageOut[61]~154\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[61]~154_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[52]~148_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[52]~148_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[61]~154_combout\);

-- Location: LCCOMB_X37_Y12_N2
\Mod1|auto_generated|divider|divider|StageOut[59]~156\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[59]~156_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[50]~150_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[50]~150_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[59]~156_combout\);

-- Location: LCCOMB_X32_Y12_N14
\Mod1|auto_generated|divider|divider|StageOut[57]~158\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[57]~158_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\toOut[7]~16_combout\) # ((\sec_cnt|count_second\(7) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(7),
	datab => \toOut[7]~16_combout\,
	datac => \SW~combout\(9),
	datad => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[57]~158_combout\);

-- Location: LCCOMB_X32_Y12_N8
\Mod1|auto_generated|divider|divider|StageOut[65]~165\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[65]~165_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\toOut[6]~18_combout\) # ((\sec_cnt|count_second\(6) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(6),
	datab => \toOut[6]~18_combout\,
	datac => \SW~combout\(9),
	datad => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[65]~165_combout\);

-- Location: LCCOMB_X35_Y12_N2
\Mod1|auto_generated|divider|divider|StageOut[75]~169\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[75]~169_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[66]~116_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\,
	datac => \Mod1|auto_generated|divider|divider|StageOut[66]~116_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[75]~169_combout\);

-- Location: LCCOMB_X32_Y12_N0
\Mod1|auto_generated|divider|divider|StageOut[73]~171\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[73]~171_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & ((\toOut[5]~19_combout\) # ((\sec_cnt|count_second\(5) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[5]~19_combout\,
	datab => \sec_cnt|count_second\(5),
	datac => \SW~combout\(9),
	datad => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[73]~171_combout\);

-- Location: LCCOMB_X34_Y12_N2
\Mod1|auto_generated|divider|divider|StageOut[84]~174\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[84]~174_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[75]~169_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[75]~169_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~2_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[84]~174_combout\);

-- Location: LCCOMB_X34_Y12_N8
\Mod1|auto_generated|divider|divider|StageOut[83]~175\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[83]~175_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[74]~122_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[74]~122_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~0_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[83]~175_combout\);

-- Location: LCCOMB_X33_Y12_N4
\Mod1|auto_generated|divider|divider|StageOut[94]~178\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[94]~178_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[85]~173_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~6_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[85]~173_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~6_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[94]~178_combout\);

-- Location: LCCOMB_X25_Y13_N30
\Div0|auto_generated|divider|divider|StageOut[18]~37\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[18]~37_combout\ = (\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[110]~190_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~8_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~8_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[110]~190_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[18]~37_combout\);

-- Location: LCCOMB_X25_Y13_N0
\Div0|auto_generated|divider|divider|StageOut[17]~38\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[17]~38_combout\ = (\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[109]~191_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~6_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~6_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[109]~191_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[17]~38_combout\);

-- Location: LCCOMB_X26_Y13_N12
\Div0|auto_generated|divider|divider|StageOut[23]~42\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[23]~42_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\Div0|auto_generated|divider|divider|StageOut[17]~38_combout\) # 
-- ((!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|StageOut[17]~38_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[23]~42_combout\);

-- Location: LCCOMB_X26_Y13_N14
\Div0|auto_generated|divider|divider|StageOut[22]~43\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[22]~43_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\Div0|auto_generated|divider|divider|StageOut[16]~39_combout\) # 
-- ((\Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\ & !\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[16]~39_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[22]~43_combout\);

-- Location: LCCOMB_X27_Y13_N10
\Div0|auto_generated|divider|divider|StageOut[27]~47\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[27]~47_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\Div0|auto_generated|divider|divider|StageOut[21]~31_combout\) # 
-- ((!\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\,
	datad => \Div0|auto_generated|divider|divider|StageOut[21]~31_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[27]~47_combout\);

-- Location: LCCOMB_X29_Y11_N26
\Mod1|auto_generated|divider|divider|StageOut[96]~195\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[96]~195_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & ((\toOut[1]~2_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datab => \toOut[1]~2_combout\,
	datac => \SW~combout\(9),
	datad => \sec_cnt|count_second\(1),
	combout => \Mod1|auto_generated|divider|divider|StageOut[96]~195_combout\);

-- Location: LCCOMB_X29_Y11_N24
\Mod1|auto_generated|divider|divider|StageOut[96]~196\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[96]~196_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & ((\toOut[1]~2_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datab => \toOut[1]~2_combout\,
	datac => \SW~combout\(9),
	datad => \sec_cnt|count_second\(1),
	combout => \Mod1|auto_generated|divider|divider|StageOut[96]~196_combout\);

-- Location: LCCOMB_X29_Y11_N2
\Div0|auto_generated|divider|divider|StageOut[25]~48\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[25]~48_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[105]~194_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_13_result_int[1]~14_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[1]~14_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[105]~194_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[25]~48_combout\);

-- Location: LCCOMB_X33_Y11_N14
\Mod2|auto_generated|divider|divider|StageOut[103]~151\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[103]~151_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[8]~14_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(8)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datab => \SW~combout\(9),
	datac => \sec_cnt|count_second\(8),
	datad => \toOut[8]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[103]~151_combout\);

-- Location: LCCOMB_X35_Y11_N6
\Mod2|auto_generated|divider|divider|StageOut[101]~154\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[101]~154_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[6]~18_combout\) # ((\sec_cnt|count_second\(6) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(6),
	datab => \toOut[6]~18_combout\,
	datac => \SW~combout\(9),
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[101]~154_combout\);

-- Location: LCCOMB_X34_Y11_N28
\Mod2|auto_generated|divider|divider|StageOut[119]~155\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[119]~155_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[107]~147_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[107]~147_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[119]~155_combout\);

-- Location: LCCOMB_X35_Y11_N4
\Mod2|auto_generated|divider|divider|StageOut[117]~157\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[117]~157_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[105]~149_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datac => \Mod2|auto_generated|divider|divider|StageOut[105]~149_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[117]~157_combout\);

-- Location: LCCOMB_X35_Y11_N10
\Mod2|auto_generated|divider|divider|StageOut[116]~158\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[116]~158_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[104]~150_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datac => \Mod2|auto_generated|divider|divider|StageOut[104]~150_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[116]~158_combout\);

-- Location: LCCOMB_X35_Y11_N24
\Mod2|auto_generated|divider|divider|StageOut[114]~160\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[114]~160_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[102]~152_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[102]~152_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[114]~160_combout\);

-- Location: LCCOMB_X35_Y13_N12
\Mod2|auto_generated|divider|divider|StageOut[130]~165\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[130]~165_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[118]~156_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[118]~156_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[130]~165_combout\);

-- Location: LCCOMB_X35_Y13_N10
\Mod2|auto_generated|divider|divider|StageOut[129]~166\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[129]~166_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[117]~157_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[117]~157_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[129]~166_combout\);

-- Location: LCCOMB_X34_Y13_N6
\Mod2|auto_generated|divider|divider|StageOut[128]~167\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[128]~167_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[116]~158_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[116]~158_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[128]~167_combout\);

-- Location: LCCOMB_X33_Y13_N4
\Mod2|auto_generated|divider|divider|StageOut[141]~176\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[141]~176_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[129]~166_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[129]~166_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[141]~176_combout\);

-- Location: LCCOMB_X34_Y13_N18
\Mod2|auto_generated|divider|divider|StageOut[140]~177\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[140]~177_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[128]~167_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[128]~167_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[140]~177_combout\);

-- Location: LCCOMB_X30_Y13_N14
\Mod2|auto_generated|divider|divider|StageOut[152]~187\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[152]~187_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[140]~177_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[140]~177_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[152]~187_combout\);

-- Location: LCCOMB_X32_Y13_N4
\Mod2|auto_generated|divider|divider|StageOut[148]~191\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[148]~191_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[136]~181_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[136]~181_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[148]~191_combout\);

-- Location: LCCOMB_X30_Y13_N2
\Div1|auto_generated|divider|divider|StageOut[54]~60\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[54]~60_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[152]~187_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[152]~187_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[54]~60_combout\);

-- Location: LCCOMB_X31_Y13_N8
\Div1|auto_generated|divider|divider|StageOut[51]~63\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[51]~63_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[149]~190_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[149]~190_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~6_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[51]~63_combout\);

-- Location: LCCOMB_X31_Y13_N30
\Div1|auto_generated|divider|divider|StageOut[50]~64\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[50]~64_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[148]~191_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~4_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[148]~191_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~4_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[50]~64_combout\);

-- Location: LCCOMB_X31_Y14_N26
\Div1|auto_generated|divider|divider|StageOut[62]~66\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[62]~66_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[53]~61_combout\) # 
-- ((\Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\ & !\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[53]~61_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[62]~66_combout\);

-- Location: LCCOMB_X30_Y14_N0
\Div1|auto_generated|divider|divider|StageOut[70]~73\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[70]~73_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[61]~67_combout\) # 
-- ((\Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[61]~67_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[70]~73_combout\);

-- Location: LCCOMB_X29_Y14_N16
\Div1|auto_generated|divider|divider|StageOut[69]~74\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[69]~74_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[60]~68_combout\) # 
-- ((\Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[60]~68_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[69]~74_combout\);

-- Location: LCCOMB_X30_Y14_N26
\Div1|auto_generated|divider|divider|StageOut[68]~75\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[68]~75_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[59]~69_combout\) # 
-- ((\Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[59]~69_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[68]~75_combout\);

-- Location: LCCOMB_X29_Y16_N22
\Div1|auto_generated|divider|divider|StageOut[66]~77\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[66]~77_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[57]~72_combout\) # 
-- ((\Div1|auto_generated|divider|divider|add_sub_7_result_int[1]~14_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[57]~72_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_7_result_int[1]~14_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[66]~77_combout\);

-- Location: LCCOMB_X35_Y10_N28
\Div2|auto_generated|divider|divider|StageOut[101]~123\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[101]~123_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[6]~18_combout\) # ((\sec_cnt|count_second\(6) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(6),
	datab => \SW~combout\(9),
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \toOut[6]~18_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[101]~123_combout\);

-- Location: LCCOMB_X36_Y9_N0
\Div2|auto_generated|divider|divider|StageOut[119]~124\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[119]~124_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[107]~116_combout\) # 
-- ((!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|StageOut[107]~116_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[119]~124_combout\);

-- Location: LCCOMB_X35_Y9_N30
\Div2|auto_generated|divider|divider|StageOut[114]~129\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[114]~129_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[102]~121_combout\) # 
-- ((\Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\ & !\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[102]~121_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[114]~129_combout\);

-- Location: LCCOMB_X34_Y10_N4
\Div2|auto_generated|divider|divider|StageOut[100]~132\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[100]~132_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[5]~19_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \sec_cnt|count_second\(5),
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \toOut[5]~19_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[100]~132_combout\);

-- Location: LCCOMB_X34_Y9_N18
\Div2|auto_generated|divider|divider|StageOut[100]~133\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[100]~133_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[5]~19_combout\) # ((\sec_cnt|count_second\(5) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datab => \sec_cnt|count_second\(5),
	datac => \SW~combout\(9),
	datad => \toOut[5]~19_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[100]~133_combout\);

-- Location: LCCOMB_X37_Y8_N0
\Div2|auto_generated|divider|divider|StageOut[126]~138\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[126]~138_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[114]~129_combout\) # 
-- ((!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\,
	datac => \Div2|auto_generated|divider|divider|StageOut[114]~129_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[126]~138_combout\);

-- Location: LCCOMB_X34_Y9_N16
\Div2|auto_generated|divider|divider|StageOut[124]~140\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[124]~140_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[112]~131_combout\) # 
-- ((\Div2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\ & !\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[112]~131_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[124]~140_combout\);

-- Location: LCCOMB_X34_Y9_N28
\Div2|auto_generated|divider|divider|StageOut[123]~144\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[123]~144_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[111]~141_combout\) # 
-- ((\Div2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\ & !\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[111]~141_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[123]~144_combout\);

-- Location: LCCOMB_X35_Y8_N26
\Div2|auto_generated|divider|divider|StageOut[141]~145\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[141]~145_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[129]~135_combout\) # 
-- ((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|StageOut[129]~135_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[141]~145_combout\);

-- Location: LCCOMB_X37_Y8_N22
\Div2|auto_generated|divider|divider|StageOut[140]~146\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[140]~146_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[128]~136_combout\) # 
-- ((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[128]~136_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[140]~146_combout\);

-- Location: LCCOMB_X37_Y8_N24
\Div2|auto_generated|divider|divider|StageOut[138]~148\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[138]~148_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[126]~138_combout\) # 
-- ((\Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|StageOut[126]~138_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[138]~148_combout\);

-- Location: LCCOMB_X37_Y8_N18
\Div2|auto_generated|divider|divider|StageOut[136]~150\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[136]~150_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[124]~140_combout\) # 
-- ((\Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|StageOut[124]~140_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[136]~150_combout\);

-- Location: LCCOMB_X34_Y9_N30
\Div2|auto_generated|divider|divider|StageOut[135]~151\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[135]~151_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[123]~144_combout\) # 
-- ((\Div2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[123]~144_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[135]~151_combout\);

-- Location: LCCOMB_X40_Y9_N20
\Div2|auto_generated|divider|divider|StageOut[122]~152\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[122]~152_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\toOut[3]~21_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \sec_cnt|count_second\(3),
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \toOut[3]~21_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[122]~152_combout\);

-- Location: LCCOMB_X40_Y9_N16
\Div2|auto_generated|divider|divider|StageOut[110]~154\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[110]~154_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\toOut[3]~21_combout\) # ((\sec_cnt|count_second\(3) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \sec_cnt|count_second\(3),
	datac => \SW~combout\(9),
	datad => \toOut[3]~21_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[110]~154_combout\);

-- Location: LCCOMB_X40_Y9_N2
\Div2|auto_generated|divider|divider|StageOut[134]~155\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[134]~155_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[122]~152_combout\) # 
-- ((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[122]~152_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[134]~155_combout\);

-- Location: PIN_L21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(1),
	combout => \SW~combout\(1));

-- Location: PIN_M22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(2),
	combout => \SW~combout\(2));

-- Location: PIN_W12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(4),
	combout => \SW~combout\(4));

-- Location: PIN_V12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(3),
	combout => \SW~combout\(3));

-- Location: PIN_L22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(0),
	combout => \SW~combout\(0));

-- Location: LCCOMB_X31_Y10_N16
\SM|Mux1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \SM|Mux1~0_combout\ = (\SW~combout\(4) & (!\SM|state\(2) & (!\SM|state\(0) & !\SM|state\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(4),
	datab => \SM|state\(2),
	datac => \SM|state\(0),
	datad => \SM|state\(1),
	combout => \SM|Mux1~0_combout\);

-- Location: LCCOMB_X32_Y10_N4
\SM|Mux1~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \SM|Mux1~2_combout\ = (\SM|Mux1~0_combout\) # ((\SM|state\(1) & ((\SM|Mux1~1_combout\) # (\SM|state\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SM|Mux1~1_combout\,
	datab => \SM|state\(2),
	datac => \SM|state\(1),
	datad => \SM|Mux1~0_combout\,
	combout => \SM|Mux1~2_combout\);

-- Location: PIN_R22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(0),
	combout => \KEY~combout\(0));

-- Location: LCFF_X32_Y10_N5
\SM|state[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \SM|Mux1~2_combout\,
	sdata => VCC,
	sload => \ALT_INV_KEY~combout\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \SM|state\(1));

-- Location: LCCOMB_X31_Y10_N2
\SM|Mux0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \SM|Mux0~0_combout\ = (\SM|state\(2) & ((\SW~combout\(1)) # ((\SM|state\(0)) # (\SM|state\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(1),
	datab => \SM|state\(2),
	datac => \SM|state\(0),
	datad => \SM|state\(1),
	combout => \SM|Mux0~0_combout\);

-- Location: LCCOMB_X31_Y10_N12
\SM|Mux0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \SM|Mux0~1_combout\ = (\SW~combout\(1) & \SM|state\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(1),
	datad => \SM|state\(1),
	combout => \SM|Mux0~1_combout\);

-- Location: LCCOMB_X31_Y10_N28
\SM|Mux0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \SM|Mux0~2_combout\ = (\SM|Mux0~0_combout\) # ((!\SW~combout\(2) & (\SM|state\(0) & \SM|Mux0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(2),
	datab => \SM|Mux0~0_combout\,
	datac => \SM|state\(0),
	datad => \SM|Mux0~1_combout\,
	combout => \SM|Mux0~2_combout\);

-- Location: LCFF_X31_Y10_N29
\SM|state[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \SM|Mux0~2_combout\,
	sclr => \ALT_INV_KEY~combout\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \SM|state\(2));

-- Location: LCCOMB_X31_Y10_N18
\SM|Mux2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \SM|Mux2~0_combout\ = (\SM|state\(0) & ((\SM|state\(2)) # ((!\SW~combout\(2) & !\SM|Mux0~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(2),
	datab => \SM|state\(2),
	datac => \SM|state\(0),
	datad => \SM|Mux0~1_combout\,
	combout => \SM|Mux2~0_combout\);

-- Location: LCCOMB_X31_Y10_N14
\SM|Mux2~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \SM|Mux2~1_combout\ = (\SM|Mux2~0_combout\) # ((\Equal0~2_combout\ & ((\SW~combout\(3)) # (\SW~combout\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~2_combout\,
	datab => \SW~combout\(3),
	datac => \SW~combout\(0),
	datad => \SM|Mux2~0_combout\,
	combout => \SM|Mux2~1_combout\);

-- Location: LCFF_X31_Y10_N15
\SM|state[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \SM|Mux2~1_combout\,
	sclr => \ALT_INV_KEY~combout\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \SM|state\(0));

-- Location: LCCOMB_X32_Y10_N2
\Equal0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal0~0_combout\ = (!\SM|state\(0) & (!\SM|state\(2) & !\SM|state\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SM|state\(0),
	datab => \SM|state\(2),
	datac => \SM|state\(1),
	combout => \Equal0~0_combout\);

-- Location: LCCOMB_X32_Y10_N8
\Equal0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal0~1_combout\ = (\SM|state\(0) & (!\SM|state\(2) & !\SM|state\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SM|state\(0),
	datab => \SM|state\(2),
	datac => \SM|state\(1),
	combout => \Equal0~1_combout\);

-- Location: LCCOMB_X31_Y10_N20
\Equal0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal0~2_combout\ = (!\SM|state\(2) & (!\SM|state\(0) & \SM|state\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SM|state\(2),
	datac => \SM|state\(0),
	datad => \SM|state\(1),
	combout => \Equal0~2_combout\);

-- Location: LCCOMB_X32_Y10_N10
\Equal0~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal0~3_combout\ = (\SM|state\(0) & (!\SM|state\(2) & \SM|state\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SM|state\(0),
	datab => \SM|state\(2),
	datac => \SM|state\(1),
	combout => \Equal0~3_combout\);

-- Location: LCCOMB_X32_Y10_N12
\Equal0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal0~4_combout\ = (!\SM|state\(0) & (\SM|state\(2) & !\SM|state\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SM|state\(0),
	datab => \SM|state\(2),
	datac => \SM|state\(1),
	combout => \Equal0~4_combout\);

-- Location: LCCOMB_X42_Y9_N2
\myFIFO|addr_wr[0]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|addr_wr[0]~7_combout\ = \myFIFO|addr_wr\(0) $ (VCC)
-- \myFIFO|addr_wr[0]~8\ = CARRY(\myFIFO|addr_wr\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \myFIFO|addr_wr\(0),
	datad => VCC,
	combout => \myFIFO|addr_wr[0]~7_combout\,
	cout => \myFIFO|addr_wr[0]~8\);

-- Location: PIN_T21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(3),
	combout => \KEY~combout\(3));

-- Location: LCFF_X43_Y9_N29
\myFIFO|key3_d0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \KEY~combout\(3),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|key3_d0~regout\);

-- Location: LCCOMB_X42_Y9_N14
\myFIFO|key3_d1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|key3_d1~0_combout\ = (\KEY~combout\(0) & (\myFIFO|key3_d0~regout\)) # (!\KEY~combout\(0) & ((\KEY~combout\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \myFIFO|key3_d0~regout\,
	datac => \KEY~combout\(3),
	datad => \KEY~combout\(0),
	combout => \myFIFO|key3_d1~0_combout\);

-- Location: LCFF_X43_Y9_N27
\myFIFO|key3_d1\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \myFIFO|key3_d1~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|key3_d1~regout\);

-- Location: LCCOMB_X42_Y9_N12
\myFIFO|key3_d2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|key3_d2~0_combout\ = (\KEY~combout\(0) & ((\myFIFO|key3_d1~regout\))) # (!\KEY~combout\(0) & (\KEY~combout\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \KEY~combout\(3),
	datac => \myFIFO|key3_d1~regout\,
	datad => \KEY~combout\(0),
	combout => \myFIFO|key3_d2~0_combout\);

-- Location: LCFF_X43_Y9_N9
\myFIFO|key3_d2\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \myFIFO|key3_d2~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|key3_d2~regout\);

-- Location: LCCOMB_X42_Y9_N6
\myFIFO|addr_wr[2]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|addr_wr[2]~11_combout\ = (\myFIFO|addr_wr\(2) & (\myFIFO|addr_wr[1]~10\ $ (GND))) # (!\myFIFO|addr_wr\(2) & (!\myFIFO|addr_wr[1]~10\ & VCC))
-- \myFIFO|addr_wr[2]~12\ = CARRY((\myFIFO|addr_wr\(2) & !\myFIFO|addr_wr[1]~10\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|addr_wr\(2),
	datad => VCC,
	cin => \myFIFO|addr_wr[1]~10\,
	combout => \myFIFO|addr_wr[2]~11_combout\,
	cout => \myFIFO|addr_wr[2]~12\);

-- Location: LCFF_X42_Y9_N7
\myFIFO|addr_wr[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \myFIFO|addr_wr[2]~11_combout\,
	sclr => \ALT_INV_KEY~combout\(0),
	ena => \myFIFO|addr_wr[3]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|addr_wr\(2));

-- Location: PIN_T22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(2),
	combout => \KEY~combout\(2));

-- Location: LCCOMB_X44_Y9_N16
\myFIFO|key2_d0~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|key2_d0~feeder_combout\ = \KEY~combout\(2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \KEY~combout\(2),
	combout => \myFIFO|key2_d0~feeder_combout\);

-- Location: LCFF_X44_Y9_N17
\myFIFO|key2_d0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \myFIFO|key2_d0~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|key2_d0~regout\);

-- Location: LCCOMB_X44_Y9_N4
\myFIFO|key2_d1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|key2_d1~0_combout\ = (\KEY~combout\(0) & ((\myFIFO|key2_d0~regout\))) # (!\KEY~combout\(0) & (\KEY~combout\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \KEY~combout\(2),
	datac => \myFIFO|key2_d0~regout\,
	datad => \KEY~combout\(0),
	combout => \myFIFO|key2_d1~0_combout\);

-- Location: LCFF_X44_Y9_N5
\myFIFO|key2_d1\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \myFIFO|key2_d1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|key2_d1~regout\);

-- Location: LCCOMB_X44_Y9_N14
\myFIFO|key2_d2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|key2_d2~0_combout\ = (\KEY~combout\(0) & ((\myFIFO|key2_d1~regout\))) # (!\KEY~combout\(0) & (\KEY~combout\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \KEY~combout\(2),
	datac => \myFIFO|key2_d1~regout\,
	datad => \KEY~combout\(0),
	combout => \myFIFO|key2_d2~0_combout\);

-- Location: LCFF_X44_Y9_N15
\myFIFO|key2_d2\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \myFIFO|key2_d2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|key2_d2~regout\);

-- Location: LCCOMB_X43_Y9_N6
\myFIFO|Add2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Add2~0_combout\ = (\myFIFO|addr_rd\(0) & (\myFIFO|addr_rd~0_combout\ $ (GND))) # (!\myFIFO|addr_rd\(0) & (!\myFIFO|addr_rd~0_combout\ & VCC))
-- \myFIFO|Add2~1\ = CARRY((\myFIFO|addr_rd\(0) & !\myFIFO|addr_rd~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100100100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|addr_rd\(0),
	datab => \myFIFO|addr_rd~0_combout\,
	datad => VCC,
	combout => \myFIFO|Add2~0_combout\,
	cout => \myFIFO|Add2~1\);

-- Location: LCCOMB_X43_Y9_N8
\myFIFO|Add2~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Add2~3_combout\ = (\myFIFO|addr_rd\(1) & (!\myFIFO|Add2~1\)) # (!\myFIFO|addr_rd\(1) & ((\myFIFO|Add2~1\) # (GND)))
-- \myFIFO|Add2~4\ = CARRY((!\myFIFO|Add2~1\) # (!\myFIFO|addr_rd\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|addr_rd\(1),
	datad => VCC,
	cin => \myFIFO|Add2~1\,
	combout => \myFIFO|Add2~3_combout\,
	cout => \myFIFO|Add2~4\);

-- Location: LCCOMB_X43_Y9_N10
\myFIFO|Add2~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Add2~6_combout\ = (\myFIFO|addr_rd\(2) & (\myFIFO|Add2~4\ $ (GND))) # (!\myFIFO|addr_rd\(2) & (!\myFIFO|Add2~4\ & VCC))
-- \myFIFO|Add2~7\ = CARRY((\myFIFO|addr_rd\(2) & !\myFIFO|Add2~4\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \myFIFO|addr_rd\(2),
	datad => VCC,
	cin => \myFIFO|Add2~4\,
	combout => \myFIFO|Add2~6_combout\,
	cout => \myFIFO|Add2~7\);

-- Location: LCCOMB_X42_Y9_N30
\myFIFO|Add2~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Add2~8_combout\ = (\myFIFO|Add2~6_combout\ & \KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \myFIFO|Add2~6_combout\,
	datad => \KEY~combout\(0),
	combout => \myFIFO|Add2~8_combout\);

-- Location: LCFF_X43_Y9_N23
\myFIFO|addr_rd[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \myFIFO|Add2~8_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|addr_rd\(2));

-- Location: LCCOMB_X43_Y9_N12
\myFIFO|Add2~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Add2~9_combout\ = (\myFIFO|addr_rd\(3) & (!\myFIFO|Add2~7\)) # (!\myFIFO|addr_rd\(3) & ((\myFIFO|Add2~7\) # (GND)))
-- \myFIFO|Add2~10\ = CARRY((!\myFIFO|Add2~7\) # (!\myFIFO|addr_rd\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \myFIFO|addr_rd\(3),
	datad => VCC,
	cin => \myFIFO|Add2~7\,
	combout => \myFIFO|Add2~9_combout\,
	cout => \myFIFO|Add2~10\);

-- Location: LCCOMB_X42_Y9_N0
\myFIFO|Add2~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Add2~11_combout\ = (\myFIFO|Add2~9_combout\ & \KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \myFIFO|Add2~9_combout\,
	datad => \KEY~combout\(0),
	combout => \myFIFO|Add2~11_combout\);

-- Location: LCFF_X43_Y9_N25
\myFIFO|addr_rd[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \myFIFO|Add2~11_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|addr_rd\(3));

-- Location: LCCOMB_X43_Y9_N14
\myFIFO|Add2~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Add2~12_combout\ = \myFIFO|Add2~10\ $ (!\myFIFO|addr_rd\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \myFIFO|addr_rd\(4),
	cin => \myFIFO|Add2~10\,
	combout => \myFIFO|Add2~12_combout\);

-- Location: LCCOMB_X42_Y9_N22
\myFIFO|Add2~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Add2~14_combout\ = (\myFIFO|Add2~12_combout\ & \KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \myFIFO|Add2~12_combout\,
	datad => \KEY~combout\(0),
	combout => \myFIFO|Add2~14_combout\);

-- Location: LCFF_X42_Y9_N23
\myFIFO|addr_rd[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \myFIFO|Add2~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|addr_rd\(4));

-- Location: LCCOMB_X43_Y9_N24
\myFIFO|Add0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Add0~4_combout\ = (\myFIFO|addr_rd\(2) & ((GND) # (!\myFIFO|Add0~3\))) # (!\myFIFO|addr_rd\(2) & (\myFIFO|Add0~3\ $ (GND)))
-- \myFIFO|Add0~5\ = CARRY((\myFIFO|addr_rd\(2)) # (!\myFIFO|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|addr_rd\(2),
	datad => VCC,
	cin => \myFIFO|Add0~3\,
	combout => \myFIFO|Add0~4_combout\,
	cout => \myFIFO|Add0~5\);

-- Location: LCCOMB_X43_Y9_N26
\myFIFO|Add0~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Add0~6_combout\ = (\myFIFO|addr_rd\(3) & (\myFIFO|Add0~5\ & VCC)) # (!\myFIFO|addr_rd\(3) & (!\myFIFO|Add0~5\))
-- \myFIFO|Add0~7\ = CARRY((!\myFIFO|addr_rd\(3) & !\myFIFO|Add0~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \myFIFO|addr_rd\(3),
	datad => VCC,
	cin => \myFIFO|Add0~5\,
	combout => \myFIFO|Add0~6_combout\,
	cout => \myFIFO|Add0~7\);

-- Location: LCCOMB_X43_Y9_N28
\myFIFO|Add0~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Add0~8_combout\ = \myFIFO|Add0~7\ $ (\myFIFO|addr_rd\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \myFIFO|addr_rd\(4),
	cin => \myFIFO|Add0~7\,
	combout => \myFIFO|Add0~8_combout\);

-- Location: LCCOMB_X43_Y9_N20
\myFIFO|Add0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Add0~0_combout\ = \myFIFO|addr_rd\(0) $ (VCC)
-- \myFIFO|Add0~1\ = CARRY(\myFIFO|addr_rd\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \myFIFO|addr_rd\(0),
	datad => VCC,
	combout => \myFIFO|Add0~0_combout\,
	cout => \myFIFO|Add0~1\);

-- Location: LCCOMB_X43_Y9_N22
\myFIFO|Add0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Add0~2_combout\ = (\myFIFO|addr_rd\(1) & (\myFIFO|Add0~1\ & VCC)) # (!\myFIFO|addr_rd\(1) & (!\myFIFO|Add0~1\))
-- \myFIFO|Add0~3\ = CARRY((!\myFIFO|addr_rd\(1) & !\myFIFO|Add0~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|addr_rd\(1),
	datad => VCC,
	cin => \myFIFO|Add0~1\,
	combout => \myFIFO|Add0~2_combout\,
	cout => \myFIFO|Add0~3\);

-- Location: LCCOMB_X43_Y9_N16
\myFIFO|Equal0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Equal0~0_combout\ = (\myFIFO|addr_wr\(0) & (\myFIFO|Add0~0_combout\ & (\myFIFO|addr_wr\(1) $ (!\myFIFO|Add0~2_combout\)))) # (!\myFIFO|addr_wr\(0) & (!\myFIFO|Add0~0_combout\ & (\myFIFO|addr_wr\(1) $ (!\myFIFO|Add0~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|addr_wr\(0),
	datab => \myFIFO|addr_wr\(1),
	datac => \myFIFO|Add0~0_combout\,
	datad => \myFIFO|Add0~2_combout\,
	combout => \myFIFO|Equal0~0_combout\);

-- Location: LCCOMB_X43_Y9_N2
\myFIFO|we~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|we~0_combout\ = (\myFIFO|Equal0~0_combout\ & (\myFIFO|Equal0~1_combout\ & (\myFIFO|addr_wr\(4) $ (!\myFIFO|Add0~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|addr_wr\(4),
	datab => \myFIFO|Add0~8_combout\,
	datac => \myFIFO|Equal0~0_combout\,
	datad => \myFIFO|Equal0~1_combout\,
	combout => \myFIFO|we~0_combout\);

-- Location: LCCOMB_X43_Y9_N30
\myFIFO|we~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|we~1_combout\ = (!\myFIFO|key3_d1~regout\ & (\myFIFO|key3_d2~regout\ & !\myFIFO|we~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|key3_d1~regout\,
	datab => \myFIFO|key3_d2~regout\,
	datad => \myFIFO|we~0_combout\,
	combout => \myFIFO|we~1_combout\);

-- Location: LCCOMB_X43_Y9_N18
\myFIFO|addr_rd~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|addr_rd~0_combout\ = (\myFIFO|key2_d1~regout\) # ((\myFIFO|Equal1~2_combout\) # ((\myFIFO|we~1_combout\) # (!\myFIFO|key2_d2~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|key2_d1~regout\,
	datab => \myFIFO|Equal1~2_combout\,
	datac => \myFIFO|key2_d2~regout\,
	datad => \myFIFO|we~1_combout\,
	combout => \myFIFO|addr_rd~0_combout\);

-- Location: LCCOMB_X42_Y9_N20
\myFIFO|Add2~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Add2~2_combout\ = (\KEY~combout\(0) & \myFIFO|Add2~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \KEY~combout\(0),
	datad => \myFIFO|Add2~0_combout\,
	combout => \myFIFO|Add2~2_combout\);

-- Location: LCFF_X43_Y9_N5
\myFIFO|addr_rd[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \myFIFO|Add2~2_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|addr_rd\(0));

-- Location: LCCOMB_X43_Y9_N0
\myFIFO|Equal0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Equal0~1_combout\ = (\myFIFO|addr_wr\(3) & (\myFIFO|Add0~6_combout\ & (\myFIFO|addr_wr\(2) $ (!\myFIFO|Add0~4_combout\)))) # (!\myFIFO|addr_wr\(3) & (!\myFIFO|Add0~6_combout\ & (\myFIFO|addr_wr\(2) $ (!\myFIFO|Add0~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|addr_wr\(3),
	datab => \myFIFO|addr_wr\(2),
	datac => \myFIFO|Add0~4_combout\,
	datad => \myFIFO|Add0~6_combout\,
	combout => \myFIFO|Equal0~1_combout\);

-- Location: LCCOMB_X42_Y9_N18
\myFIFO|Equal0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Equal0~2_combout\ = (\myFIFO|Equal0~1_combout\ & (\myFIFO|Equal0~0_combout\ & (\myFIFO|addr_wr\(4) $ (!\myFIFO|Add0~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|addr_wr\(4),
	datab => \myFIFO|Equal0~1_combout\,
	datac => \myFIFO|Equal0~0_combout\,
	datad => \myFIFO|Add0~8_combout\,
	combout => \myFIFO|Equal0~2_combout\);

-- Location: LCCOMB_X42_Y9_N26
\myFIFO|addr_wr[3]~17\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|addr_wr[3]~17_combout\ = ((!\myFIFO|key3_d1~regout\ & (\myFIFO|key3_d2~regout\ & !\myFIFO|Equal0~2_combout\))) # (!\KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001101110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|key3_d1~regout\,
	datab => \KEY~combout\(0),
	datac => \myFIFO|key3_d2~regout\,
	datad => \myFIFO|Equal0~2_combout\,
	combout => \myFIFO|addr_wr[3]~17_combout\);

-- Location: LCFF_X42_Y9_N3
\myFIFO|addr_wr[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \myFIFO|addr_wr[0]~7_combout\,
	sclr => \ALT_INV_KEY~combout\(0),
	ena => \myFIFO|addr_wr[3]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|addr_wr\(0));

-- Location: LCCOMB_X42_Y9_N4
\myFIFO|addr_wr[1]~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|addr_wr[1]~9_combout\ = (\myFIFO|addr_wr\(1) & (!\myFIFO|addr_wr[0]~8\)) # (!\myFIFO|addr_wr\(1) & ((\myFIFO|addr_wr[0]~8\) # (GND)))
-- \myFIFO|addr_wr[1]~10\ = CARRY((!\myFIFO|addr_wr[0]~8\) # (!\myFIFO|addr_wr\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \myFIFO|addr_wr\(1),
	datad => VCC,
	cin => \myFIFO|addr_wr[0]~8\,
	combout => \myFIFO|addr_wr[1]~9_combout\,
	cout => \myFIFO|addr_wr[1]~10\);

-- Location: LCFF_X42_Y9_N5
\myFIFO|addr_wr[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \myFIFO|addr_wr[1]~9_combout\,
	sclr => \ALT_INV_KEY~combout\(0),
	ena => \myFIFO|addr_wr[3]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|addr_wr\(1));

-- Location: LCCOMB_X42_Y9_N8
\myFIFO|addr_wr[3]~13\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|addr_wr[3]~13_combout\ = (\myFIFO|addr_wr\(3) & (!\myFIFO|addr_wr[2]~12\)) # (!\myFIFO|addr_wr\(3) & ((\myFIFO|addr_wr[2]~12\) # (GND)))
-- \myFIFO|addr_wr[3]~14\ = CARRY((!\myFIFO|addr_wr[2]~12\) # (!\myFIFO|addr_wr\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \myFIFO|addr_wr\(3),
	datad => VCC,
	cin => \myFIFO|addr_wr[2]~12\,
	combout => \myFIFO|addr_wr[3]~13_combout\,
	cout => \myFIFO|addr_wr[3]~14\);

-- Location: LCFF_X42_Y9_N9
\myFIFO|addr_wr[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \myFIFO|addr_wr[3]~13_combout\,
	sclr => \ALT_INV_KEY~combout\(0),
	ena => \myFIFO|addr_wr[3]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|addr_wr\(3));

-- Location: LCCOMB_X42_Y9_N10
\myFIFO|addr_wr[4]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|addr_wr[4]~15_combout\ = \myFIFO|addr_wr[3]~14\ $ (!\myFIFO|addr_wr\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \myFIFO|addr_wr\(4),
	cin => \myFIFO|addr_wr[3]~14\,
	combout => \myFIFO|addr_wr[4]~15_combout\);

-- Location: LCFF_X42_Y9_N11
\myFIFO|addr_wr[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \myFIFO|addr_wr[4]~15_combout\,
	sclr => \ALT_INV_KEY~combout\(0),
	ena => \myFIFO|addr_wr[3]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|addr_wr\(4));

-- Location: LCCOMB_X42_Y9_N24
\myFIFO|Equal1~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Equal1~1_combout\ = (\myFIFO|addr_wr\(2) & (\myFIFO|addr_rd\(2) & (\myFIFO|addr_wr\(3) $ (!\myFIFO|addr_rd\(3))))) # (!\myFIFO|addr_wr\(2) & (!\myFIFO|addr_rd\(2) & (\myFIFO|addr_wr\(3) $ (!\myFIFO|addr_rd\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|addr_wr\(2),
	datab => \myFIFO|addr_rd\(2),
	datac => \myFIFO|addr_wr\(3),
	datad => \myFIFO|addr_rd\(3),
	combout => \myFIFO|Equal1~1_combout\);

-- Location: LCCOMB_X43_Y9_N4
\myFIFO|Add2~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Add2~5_combout\ = (\KEY~combout\(0) & \myFIFO|Add2~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \KEY~combout\(0),
	datad => \myFIFO|Add2~3_combout\,
	combout => \myFIFO|Add2~5_combout\);

-- Location: LCFF_X43_Y9_N31
\myFIFO|addr_rd[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \myFIFO|Add2~5_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|addr_rd\(1));

-- Location: LCCOMB_X42_Y9_N28
\myFIFO|Equal1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Equal1~0_combout\ = (\myFIFO|addr_wr\(0) & (\myFIFO|addr_rd\(0) & (\myFIFO|addr_rd\(1) $ (!\myFIFO|addr_wr\(1))))) # (!\myFIFO|addr_wr\(0) & (!\myFIFO|addr_rd\(0) & (\myFIFO|addr_rd\(1) $ (!\myFIFO|addr_wr\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|addr_wr\(0),
	datab => \myFIFO|addr_rd\(1),
	datac => \myFIFO|addr_wr\(1),
	datad => \myFIFO|addr_rd\(0),
	combout => \myFIFO|Equal1~0_combout\);

-- Location: LCCOMB_X42_Y9_N16
\myFIFO|Equal1~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|Equal1~2_combout\ = (\myFIFO|Equal1~1_combout\ & (\myFIFO|Equal1~0_combout\ & (\myFIFO|addr_wr\(4) $ (!\myFIFO|addr_rd\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|addr_wr\(4),
	datab => \myFIFO|addr_rd\(4),
	datac => \myFIFO|Equal1~1_combout\,
	datad => \myFIFO|Equal1~0_combout\,
	combout => \myFIFO|Equal1~2_combout\);

-- Location: LCFF_X40_Y9_N5
\myFIFO|myRam|ram_rtl_0_bypass[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \myFIFO|addr_wr\(3),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(7));

-- Location: LCCOMB_X40_Y9_N30
\myFIFO|myRam|ram_rtl_0_bypass[5]~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|myRam|ram_rtl_0_bypass[5]~feeder_combout\ = \myFIFO|addr_wr\(2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \myFIFO|addr_wr\(2),
	combout => \myFIFO|myRam|ram_rtl_0_bypass[5]~feeder_combout\);

-- Location: LCFF_X40_Y9_N31
\myFIFO|myRam|ram_rtl_0_bypass[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \myFIFO|myRam|ram_rtl_0_bypass[5]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(5));

-- Location: LCCOMB_X40_Y9_N4
\myFIFO|myRam|ram~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|myRam|ram~1_combout\ = (\myFIFO|addr_rd\(3) & (\myFIFO|myRam|ram_rtl_0_bypass\(7) & (\myFIFO|addr_rd\(2) $ (!\myFIFO|myRam|ram_rtl_0_bypass\(5))))) # (!\myFIFO|addr_rd\(3) & (!\myFIFO|myRam|ram_rtl_0_bypass\(7) & (\myFIFO|addr_rd\(2) $ 
-- (!\myFIFO|myRam|ram_rtl_0_bypass\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|addr_rd\(3),
	datab => \myFIFO|addr_rd\(2),
	datac => \myFIFO|myRam|ram_rtl_0_bypass\(7),
	datad => \myFIFO|myRam|ram_rtl_0_bypass\(5),
	combout => \myFIFO|myRam|ram~1_combout\);

-- Location: LCFF_X40_Y9_N25
\myFIFO|myRam|ram_rtl_0_bypass[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \myFIFO|addr_wr\(4),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(9));

-- Location: LCFF_X43_Y9_N13
\myFIFO|myRam|ram_rtl_0_bypass[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \myFIFO|we~1_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(0));

-- Location: LCCOMB_X40_Y9_N24
\myFIFO|myRam|ram~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|myRam|ram~2_combout\ = (\myFIFO|myRam|ram_rtl_0_bypass\(0) & (\myFIFO|addr_rd\(4) $ (!\myFIFO|myRam|ram_rtl_0_bypass\(9))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \myFIFO|addr_rd\(4),
	datac => \myFIFO|myRam|ram_rtl_0_bypass\(9),
	datad => \myFIFO|myRam|ram_rtl_0_bypass\(0),
	combout => \myFIFO|myRam|ram~2_combout\);

-- Location: LCFF_X40_Y9_N1
\myFIFO|myRam|ram_rtl_0_bypass[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \myFIFO|addr_wr\(1),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(3));

-- Location: LCCOMB_X40_Y9_N6
\myFIFO|myRam|ram_rtl_0_bypass[1]~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|myRam|ram_rtl_0_bypass[1]~feeder_combout\ = \myFIFO|addr_wr\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \myFIFO|addr_wr\(0),
	combout => \myFIFO|myRam|ram_rtl_0_bypass[1]~feeder_combout\);

-- Location: LCFF_X40_Y9_N7
\myFIFO|myRam|ram_rtl_0_bypass[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \myFIFO|myRam|ram_rtl_0_bypass[1]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(1));

-- Location: LCCOMB_X40_Y9_N0
\myFIFO|myRam|ram~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|myRam|ram~0_combout\ = (\myFIFO|addr_rd\(1) & (\myFIFO|myRam|ram_rtl_0_bypass\(3) & (\myFIFO|addr_rd\(0) $ (!\myFIFO|myRam|ram_rtl_0_bypass\(1))))) # (!\myFIFO|addr_rd\(1) & (!\myFIFO|myRam|ram_rtl_0_bypass\(3) & (\myFIFO|addr_rd\(0) $ 
-- (!\myFIFO|myRam|ram_rtl_0_bypass\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|addr_rd\(1),
	datab => \myFIFO|addr_rd\(0),
	datac => \myFIFO|myRam|ram_rtl_0_bypass\(3),
	datad => \myFIFO|myRam|ram_rtl_0_bypass\(1),
	combout => \myFIFO|myRam|ram~0_combout\);

-- Location: LCCOMB_X40_Y9_N22
\myFIFO|myRam|ram~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \myFIFO|myRam|ram~3_combout\ = (\myFIFO|myRam|ram~1_combout\ & (\myFIFO|myRam|ram~2_combout\ & \myFIFO|myRam|ram~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \myFIFO|myRam|ram~1_combout\,
	datac => \myFIFO|myRam|ram~2_combout\,
	datad => \myFIFO|myRam|ram~0_combout\,
	combout => \myFIFO|myRam|ram~3_combout\);

-- Location: LCCOMB_X31_Y9_N4
\sec_cnt|count_second[0]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[0]~14_combout\ = \sec_cnt|count_second\(0) $ (VCC)
-- \sec_cnt|count_second[0]~15\ = CARRY(\sec_cnt|count_second\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(0),
	datad => VCC,
	combout => \sec_cnt|count_second[0]~14_combout\,
	cout => \sec_cnt|count_second[0]~15\);

-- Location: LCCOMB_X31_Y9_N6
\sec_cnt|count_second[1]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[1]~18_combout\ = (\sec_cnt|count_second\(1) & (!\sec_cnt|count_second[0]~15\)) # (!\sec_cnt|count_second\(1) & ((\sec_cnt|count_second[0]~15\) # (GND)))
-- \sec_cnt|count_second[1]~19\ = CARRY((!\sec_cnt|count_second[0]~15\) # (!\sec_cnt|count_second\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(1),
	datad => VCC,
	cin => \sec_cnt|count_second[0]~15\,
	combout => \sec_cnt|count_second[1]~18_combout\,
	cout => \sec_cnt|count_second[1]~19\);

-- Location: LCCOMB_X31_Y9_N8
\sec_cnt|count_second[2]~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[2]~20_combout\ = (\sec_cnt|count_second\(2) & (\sec_cnt|count_second[1]~19\ $ (GND))) # (!\sec_cnt|count_second\(2) & (!\sec_cnt|count_second[1]~19\ & VCC))
-- \sec_cnt|count_second[2]~21\ = CARRY((\sec_cnt|count_second\(2) & !\sec_cnt|count_second[1]~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(2),
	datad => VCC,
	cin => \sec_cnt|count_second[1]~19\,
	combout => \sec_cnt|count_second[2]~20_combout\,
	cout => \sec_cnt|count_second[2]~21\);

-- Location: LCCOMB_X32_Y9_N6
\count_0|cntr[0]~26\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[0]~26_combout\ = \count_0|cntr\(0) $ (VCC)
-- \count_0|cntr[0]~27\ = CARRY(\count_0|cntr\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(0),
	datad => VCC,
	combout => \count_0|cntr[0]~26_combout\,
	cout => \count_0|cntr[0]~27\);

-- Location: LCCOMB_X32_Y9_N8
\count_0|cntr[1]~30\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[1]~30_combout\ = (\count_0|cntr\(1) & (!\count_0|cntr[0]~27\)) # (!\count_0|cntr\(1) & ((\count_0|cntr[0]~27\) # (GND)))
-- \count_0|cntr[1]~31\ = CARRY((!\count_0|cntr[0]~27\) # (!\count_0|cntr\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(1),
	datad => VCC,
	cin => \count_0|cntr[0]~27\,
	combout => \count_0|cntr[1]~30_combout\,
	cout => \count_0|cntr[1]~31\);

-- Location: LCFF_X32_Y9_N7
\count_0|cntr[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[0]~26_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(0));

-- Location: LCCOMB_X32_Y9_N12
\count_0|cntr[3]~34\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[3]~34_combout\ = (\count_0|cntr\(3) & (!\count_0|cntr[2]~33\)) # (!\count_0|cntr\(3) & ((\count_0|cntr[2]~33\) # (GND)))
-- \count_0|cntr[3]~35\ = CARRY((!\count_0|cntr[2]~33\) # (!\count_0|cntr\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(3),
	datad => VCC,
	cin => \count_0|cntr[2]~33\,
	combout => \count_0|cntr[3]~34_combout\,
	cout => \count_0|cntr[3]~35\);

-- Location: LCFF_X32_Y9_N13
\count_0|cntr[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[3]~34_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(3));

-- Location: LCCOMB_X33_Y9_N24
\count_0|Equal0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|Equal0~0_combout\ = (((!\count_0|cntr\(3)) # (!\count_0|cntr\(1))) # (!\count_0|cntr\(0))) # (!\count_0|cntr\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(2),
	datab => \count_0|cntr\(0),
	datac => \count_0|cntr\(1),
	datad => \count_0|cntr\(3),
	combout => \count_0|Equal0~0_combout\);

-- Location: LCCOMB_X32_Y9_N24
\count_0|cntr[9]~46\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[9]~46_combout\ = (\count_0|cntr\(9) & (!\count_0|cntr[8]~45\)) # (!\count_0|cntr\(9) & ((\count_0|cntr[8]~45\) # (GND)))
-- \count_0|cntr[9]~47\ = CARRY((!\count_0|cntr[8]~45\) # (!\count_0|cntr\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(9),
	datad => VCC,
	cin => \count_0|cntr[8]~45\,
	combout => \count_0|cntr[9]~46_combout\,
	cout => \count_0|cntr[9]~47\);

-- Location: LCFF_X32_Y9_N25
\count_0|cntr[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[9]~46_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(9));

-- Location: LCCOMB_X33_Y9_N8
\count_0|Equal0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|Equal0~2_combout\ = (\count_0|cntr\(10)) # ((\count_0|cntr\(9)) # ((\count_0|cntr\(11)) # (!\count_0|cntr\(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(10),
	datab => \count_0|cntr\(9),
	datac => \count_0|cntr\(11),
	datad => \count_0|cntr\(8),
	combout => \count_0|Equal0~2_combout\);

-- Location: LCCOMB_X32_Y9_N20
\count_0|cntr[7]~42\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[7]~42_combout\ = (\count_0|cntr\(7) & (!\count_0|cntr[6]~41\)) # (!\count_0|cntr\(7) & ((\count_0|cntr[6]~41\) # (GND)))
-- \count_0|cntr[7]~43\ = CARRY((!\count_0|cntr[6]~41\) # (!\count_0|cntr\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(7),
	datad => VCC,
	cin => \count_0|cntr[6]~41\,
	combout => \count_0|cntr[7]~42_combout\,
	cout => \count_0|cntr[7]~43\);

-- Location: LCFF_X32_Y9_N21
\count_0|cntr[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[7]~42_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(7));

-- Location: LCCOMB_X32_Y9_N16
\count_0|cntr[5]~38\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[5]~38_combout\ = (\count_0|cntr\(5) & (!\count_0|cntr[4]~37\)) # (!\count_0|cntr\(5) & ((\count_0|cntr[4]~37\) # (GND)))
-- \count_0|cntr[5]~39\ = CARRY((!\count_0|cntr[4]~37\) # (!\count_0|cntr\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(5),
	datad => VCC,
	cin => \count_0|cntr[4]~37\,
	combout => \count_0|cntr[5]~38_combout\,
	cout => \count_0|cntr[5]~39\);

-- Location: LCFF_X32_Y9_N17
\count_0|cntr[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[5]~38_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(5));

-- Location: LCCOMB_X33_Y9_N30
\count_0|Equal0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|Equal0~1_combout\ = ((\count_0|cntr\(6)) # ((\count_0|cntr\(7)) # (\count_0|cntr\(5)))) # (!\count_0|cntr\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(4),
	datab => \count_0|cntr\(6),
	datac => \count_0|cntr\(7),
	datad => \count_0|cntr\(5),
	combout => \count_0|Equal0~1_combout\);

-- Location: LCCOMB_X32_Y9_N2
\count_0|Equal0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|Equal0~4_combout\ = (\count_0|Equal0~3_combout\) # ((\count_0|Equal0~0_combout\) # ((\count_0|Equal0~2_combout\) # (\count_0|Equal0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|Equal0~3_combout\,
	datab => \count_0|Equal0~0_combout\,
	datac => \count_0|Equal0~2_combout\,
	datad => \count_0|Equal0~1_combout\,
	combout => \count_0|Equal0~4_combout\);

-- Location: LCCOMB_X31_Y10_N8
\count_0|cntr[9]~28\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[9]~28_combout\ = (!\SM|state\(2) & (\SM|state\(0) $ (!\SM|state\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SM|state\(2),
	datac => \SM|state\(0),
	datad => \SM|state\(1),
	combout => \count_0|cntr[9]~28_combout\);

-- Location: LCCOMB_X32_Y9_N0
\count_0|cntr[9]~29\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[9]~29_combout\ = (\Equal0~2_combout\) # (((!\count_0|Equal0~4_combout\ & !\count_0|Equal0~7_combout\)) # (!\count_0|cntr[9]~28_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~2_combout\,
	datab => \count_0|Equal0~4_combout\,
	datac => \count_0|cntr[9]~28_combout\,
	datad => \count_0|Equal0~7_combout\,
	combout => \count_0|cntr[9]~29_combout\);

-- Location: LCFF_X32_Y9_N9
\count_0|cntr[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[1]~30_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(1));

-- Location: LCCOMB_X32_Y9_N14
\count_0|cntr[4]~36\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[4]~36_combout\ = (\count_0|cntr\(4) & (\count_0|cntr[3]~35\ $ (GND))) # (!\count_0|cntr\(4) & (!\count_0|cntr[3]~35\ & VCC))
-- \count_0|cntr[4]~37\ = CARRY((\count_0|cntr\(4) & !\count_0|cntr[3]~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(4),
	datad => VCC,
	cin => \count_0|cntr[3]~35\,
	combout => \count_0|cntr[4]~36_combout\,
	cout => \count_0|cntr[4]~37\);

-- Location: LCFF_X32_Y9_N15
\count_0|cntr[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[4]~36_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(4));

-- Location: LCCOMB_X32_Y9_N18
\count_0|cntr[6]~40\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[6]~40_combout\ = (\count_0|cntr\(6) & (\count_0|cntr[5]~39\ $ (GND))) # (!\count_0|cntr\(6) & (!\count_0|cntr[5]~39\ & VCC))
-- \count_0|cntr[6]~41\ = CARRY((\count_0|cntr\(6) & !\count_0|cntr[5]~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(6),
	datad => VCC,
	cin => \count_0|cntr[5]~39\,
	combout => \count_0|cntr[6]~40_combout\,
	cout => \count_0|cntr[6]~41\);

-- Location: LCFF_X32_Y9_N19
\count_0|cntr[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[6]~40_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(6));

-- Location: LCCOMB_X32_Y9_N22
\count_0|cntr[8]~44\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[8]~44_combout\ = (\count_0|cntr\(8) & (\count_0|cntr[7]~43\ $ (GND))) # (!\count_0|cntr\(8) & (!\count_0|cntr[7]~43\ & VCC))
-- \count_0|cntr[8]~45\ = CARRY((\count_0|cntr\(8) & !\count_0|cntr[7]~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(8),
	datad => VCC,
	cin => \count_0|cntr[7]~43\,
	combout => \count_0|cntr[8]~44_combout\,
	cout => \count_0|cntr[8]~45\);

-- Location: LCFF_X32_Y9_N23
\count_0|cntr[8]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[8]~44_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(8));

-- Location: LCCOMB_X32_Y9_N26
\count_0|cntr[10]~48\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[10]~48_combout\ = (\count_0|cntr\(10) & (\count_0|cntr[9]~47\ $ (GND))) # (!\count_0|cntr\(10) & (!\count_0|cntr[9]~47\ & VCC))
-- \count_0|cntr[10]~49\ = CARRY((\count_0|cntr\(10) & !\count_0|cntr[9]~47\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(10),
	datad => VCC,
	cin => \count_0|cntr[9]~47\,
	combout => \count_0|cntr[10]~48_combout\,
	cout => \count_0|cntr[10]~49\);

-- Location: LCFF_X32_Y9_N27
\count_0|cntr[10]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[10]~48_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(10));

-- Location: LCCOMB_X32_Y9_N28
\count_0|cntr[11]~50\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[11]~50_combout\ = (\count_0|cntr\(11) & (!\count_0|cntr[10]~49\)) # (!\count_0|cntr\(11) & ((\count_0|cntr[10]~49\) # (GND)))
-- \count_0|cntr[11]~51\ = CARRY((!\count_0|cntr[10]~49\) # (!\count_0|cntr\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(11),
	datad => VCC,
	cin => \count_0|cntr[10]~49\,
	combout => \count_0|cntr[11]~50_combout\,
	cout => \count_0|cntr[11]~51\);

-- Location: LCFF_X32_Y9_N29
\count_0|cntr[11]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[11]~50_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(11));

-- Location: LCCOMB_X32_Y9_N30
\count_0|cntr[12]~52\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[12]~52_combout\ = (\count_0|cntr\(12) & (\count_0|cntr[11]~51\ $ (GND))) # (!\count_0|cntr\(12) & (!\count_0|cntr[11]~51\ & VCC))
-- \count_0|cntr[12]~53\ = CARRY((\count_0|cntr\(12) & !\count_0|cntr[11]~51\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(12),
	datad => VCC,
	cin => \count_0|cntr[11]~51\,
	combout => \count_0|cntr[12]~52_combout\,
	cout => \count_0|cntr[12]~53\);

-- Location: LCFF_X32_Y9_N31
\count_0|cntr[12]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[12]~52_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(12));

-- Location: LCCOMB_X32_Y8_N0
\count_0|cntr[13]~54\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[13]~54_combout\ = (\count_0|cntr\(13) & (!\count_0|cntr[12]~53\)) # (!\count_0|cntr\(13) & ((\count_0|cntr[12]~53\) # (GND)))
-- \count_0|cntr[13]~55\ = CARRY((!\count_0|cntr[12]~53\) # (!\count_0|cntr\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(13),
	datad => VCC,
	cin => \count_0|cntr[12]~53\,
	combout => \count_0|cntr[13]~54_combout\,
	cout => \count_0|cntr[13]~55\);

-- Location: LCFF_X33_Y9_N17
\count_0|cntr[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \count_0|cntr[13]~54_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(13));

-- Location: LCCOMB_X32_Y8_N2
\count_0|cntr[14]~56\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[14]~56_combout\ = (\count_0|cntr\(14) & (\count_0|cntr[13]~55\ $ (GND))) # (!\count_0|cntr\(14) & (!\count_0|cntr[13]~55\ & VCC))
-- \count_0|cntr[14]~57\ = CARRY((\count_0|cntr\(14) & !\count_0|cntr[13]~55\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(14),
	datad => VCC,
	cin => \count_0|cntr[13]~55\,
	combout => \count_0|cntr[14]~56_combout\,
	cout => \count_0|cntr[14]~57\);

-- Location: LCFF_X32_Y8_N3
\count_0|cntr[14]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[14]~56_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(14));

-- Location: LCCOMB_X32_Y8_N4
\count_0|cntr[15]~58\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[15]~58_combout\ = (\count_0|cntr\(15) & (!\count_0|cntr[14]~57\)) # (!\count_0|cntr\(15) & ((\count_0|cntr[14]~57\) # (GND)))
-- \count_0|cntr[15]~59\ = CARRY((!\count_0|cntr[14]~57\) # (!\count_0|cntr\(15)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(15),
	datad => VCC,
	cin => \count_0|cntr[14]~57\,
	combout => \count_0|cntr[15]~58_combout\,
	cout => \count_0|cntr[15]~59\);

-- Location: LCFF_X33_Y9_N11
\count_0|cntr[15]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \count_0|cntr[15]~58_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(15));

-- Location: LCCOMB_X32_Y8_N6
\count_0|cntr[16]~60\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[16]~60_combout\ = (\count_0|cntr\(16) & (\count_0|cntr[15]~59\ $ (GND))) # (!\count_0|cntr\(16) & (!\count_0|cntr[15]~59\ & VCC))
-- \count_0|cntr[16]~61\ = CARRY((\count_0|cntr\(16) & !\count_0|cntr[15]~59\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(16),
	datad => VCC,
	cin => \count_0|cntr[15]~59\,
	combout => \count_0|cntr[16]~60_combout\,
	cout => \count_0|cntr[16]~61\);

-- Location: LCFF_X32_Y8_N7
\count_0|cntr[16]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[16]~60_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(16));

-- Location: LCCOMB_X32_Y8_N8
\count_0|cntr[17]~62\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[17]~62_combout\ = (\count_0|cntr\(17) & (!\count_0|cntr[16]~61\)) # (!\count_0|cntr\(17) & ((\count_0|cntr[16]~61\) # (GND)))
-- \count_0|cntr[17]~63\ = CARRY((!\count_0|cntr[16]~61\) # (!\count_0|cntr\(17)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(17),
	datad => VCC,
	cin => \count_0|cntr[16]~61\,
	combout => \count_0|cntr[17]~62_combout\,
	cout => \count_0|cntr[17]~63\);

-- Location: LCFF_X32_Y8_N9
\count_0|cntr[17]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[17]~62_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(17));

-- Location: LCCOMB_X32_Y8_N10
\count_0|cntr[18]~64\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[18]~64_combout\ = (\count_0|cntr\(18) & (\count_0|cntr[17]~63\ $ (GND))) # (!\count_0|cntr\(18) & (!\count_0|cntr[17]~63\ & VCC))
-- \count_0|cntr[18]~65\ = CARRY((\count_0|cntr\(18) & !\count_0|cntr[17]~63\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(18),
	datad => VCC,
	cin => \count_0|cntr[17]~63\,
	combout => \count_0|cntr[18]~64_combout\,
	cout => \count_0|cntr[18]~65\);

-- Location: LCFF_X32_Y8_N11
\count_0|cntr[18]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[18]~64_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(18));

-- Location: LCCOMB_X32_Y8_N12
\count_0|cntr[19]~66\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[19]~66_combout\ = (\count_0|cntr\(19) & (!\count_0|cntr[18]~65\)) # (!\count_0|cntr\(19) & ((\count_0|cntr[18]~65\) # (GND)))
-- \count_0|cntr[19]~67\ = CARRY((!\count_0|cntr[18]~65\) # (!\count_0|cntr\(19)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(19),
	datad => VCC,
	cin => \count_0|cntr[18]~65\,
	combout => \count_0|cntr[19]~66_combout\,
	cout => \count_0|cntr[19]~67\);

-- Location: LCFF_X32_Y8_N13
\count_0|cntr[19]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[19]~66_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(19));

-- Location: LCCOMB_X32_Y8_N14
\count_0|cntr[20]~68\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[20]~68_combout\ = (\count_0|cntr\(20) & (\count_0|cntr[19]~67\ $ (GND))) # (!\count_0|cntr\(20) & (!\count_0|cntr[19]~67\ & VCC))
-- \count_0|cntr[20]~69\ = CARRY((\count_0|cntr\(20) & !\count_0|cntr[19]~67\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(20),
	datad => VCC,
	cin => \count_0|cntr[19]~67\,
	combout => \count_0|cntr[20]~68_combout\,
	cout => \count_0|cntr[20]~69\);

-- Location: LCFF_X32_Y8_N15
\count_0|cntr[20]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[20]~68_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(20));

-- Location: LCCOMB_X32_Y8_N16
\count_0|cntr[21]~70\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[21]~70_combout\ = (\count_0|cntr\(21) & (!\count_0|cntr[20]~69\)) # (!\count_0|cntr\(21) & ((\count_0|cntr[20]~69\) # (GND)))
-- \count_0|cntr[21]~71\ = CARRY((!\count_0|cntr[20]~69\) # (!\count_0|cntr\(21)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(21),
	datad => VCC,
	cin => \count_0|cntr[20]~69\,
	combout => \count_0|cntr[21]~70_combout\,
	cout => \count_0|cntr[21]~71\);

-- Location: LCCOMB_X32_Y8_N18
\count_0|cntr[22]~72\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[22]~72_combout\ = (\count_0|cntr\(22) & (\count_0|cntr[21]~71\ $ (GND))) # (!\count_0|cntr\(22) & (!\count_0|cntr[21]~71\ & VCC))
-- \count_0|cntr[22]~73\ = CARRY((\count_0|cntr\(22) & !\count_0|cntr[21]~71\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(22),
	datad => VCC,
	cin => \count_0|cntr[21]~71\,
	combout => \count_0|cntr[22]~72_combout\,
	cout => \count_0|cntr[22]~73\);

-- Location: LCFF_X32_Y8_N19
\count_0|cntr[22]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[22]~72_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(22));

-- Location: LCFF_X32_Y8_N23
\count_0|cntr[24]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[24]~76_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(24));

-- Location: LCFF_X32_Y8_N17
\count_0|cntr[21]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[21]~70_combout\,
	sclr => \count_0|cntr[9]~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(21));

-- Location: LCCOMB_X32_Y8_N28
\count_0|Equal0~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|Equal0~6_combout\ = (\count_0|cntr\(23)) # ((\count_0|cntr\(20)) # ((\count_0|cntr\(21)) # (\count_0|cntr\(22))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(23),
	datab => \count_0|cntr\(20),
	datac => \count_0|cntr\(21),
	datad => \count_0|cntr\(22),
	combout => \count_0|Equal0~6_combout\);

-- Location: LCCOMB_X32_Y8_N26
\count_0|Equal0~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|Equal0~5_combout\ = (((\count_0|cntr\(19)) # (!\count_0|cntr\(17))) # (!\count_0|cntr\(18))) # (!\count_0|cntr\(16))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(16),
	datab => \count_0|cntr\(18),
	datac => \count_0|cntr\(17),
	datad => \count_0|cntr\(19),
	combout => \count_0|Equal0~5_combout\);

-- Location: LCCOMB_X32_Y8_N30
\count_0|Equal0~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|Equal0~7_combout\ = (\count_0|cntr\(25)) # ((\count_0|cntr\(24)) # ((\count_0|Equal0~6_combout\) # (\count_0|Equal0~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(25),
	datab => \count_0|cntr\(24),
	datac => \count_0|Equal0~6_combout\,
	datad => \count_0|Equal0~5_combout\,
	combout => \count_0|Equal0~7_combout\);

-- Location: LCCOMB_X31_Y9_N2
\sec_cnt|count_second[13]~17\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[13]~17_combout\ = (\Equal0~2_combout\) # ((!\count_0|Equal0~7_combout\ & !\count_0|Equal0~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal0~2_combout\,
	datac => \count_0|Equal0~7_combout\,
	datad => \count_0|Equal0~4_combout\,
	combout => \sec_cnt|count_second[13]~17_combout\);

-- Location: LCFF_X31_Y9_N9
\sec_cnt|count_second[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[2]~20_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(2));

-- Location: LCCOMB_X31_Y9_N10
\sec_cnt|count_second[3]~22\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[3]~22_combout\ = (\sec_cnt|count_second\(3) & (!\sec_cnt|count_second[2]~21\)) # (!\sec_cnt|count_second\(3) & ((\sec_cnt|count_second[2]~21\) # (GND)))
-- \sec_cnt|count_second[3]~23\ = CARRY((!\sec_cnt|count_second[2]~21\) # (!\sec_cnt|count_second\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(3),
	datad => VCC,
	cin => \sec_cnt|count_second[2]~21\,
	combout => \sec_cnt|count_second[3]~22_combout\,
	cout => \sec_cnt|count_second[3]~23\);

-- Location: LCFF_X31_Y9_N11
\sec_cnt|count_second[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[3]~22_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(3));

-- Location: LCCOMB_X31_Y9_N12
\sec_cnt|count_second[4]~24\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[4]~24_combout\ = (\sec_cnt|count_second\(4) & (\sec_cnt|count_second[3]~23\ $ (GND))) # (!\sec_cnt|count_second\(4) & (!\sec_cnt|count_second[3]~23\ & VCC))
-- \sec_cnt|count_second[4]~25\ = CARRY((\sec_cnt|count_second\(4) & !\sec_cnt|count_second[3]~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(4),
	datad => VCC,
	cin => \sec_cnt|count_second[3]~23\,
	combout => \sec_cnt|count_second[4]~24_combout\,
	cout => \sec_cnt|count_second[4]~25\);

-- Location: LCCOMB_X31_Y9_N14
\sec_cnt|count_second[5]~26\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[5]~26_combout\ = (\sec_cnt|count_second\(5) & (!\sec_cnt|count_second[4]~25\)) # (!\sec_cnt|count_second\(5) & ((\sec_cnt|count_second[4]~25\) # (GND)))
-- \sec_cnt|count_second[5]~27\ = CARRY((!\sec_cnt|count_second[4]~25\) # (!\sec_cnt|count_second\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(5),
	datad => VCC,
	cin => \sec_cnt|count_second[4]~25\,
	combout => \sec_cnt|count_second[5]~26_combout\,
	cout => \sec_cnt|count_second[5]~27\);

-- Location: LCFF_X31_Y9_N15
\sec_cnt|count_second[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[5]~26_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(5));

-- Location: LCFF_X31_Y9_N13
\sec_cnt|count_second[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[4]~24_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(4));

-- Location: LCCOMB_X30_Y9_N8
\sec_cnt|Equal0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|Equal0~2_combout\ = (((\sec_cnt|count_second\(5)) # (\sec_cnt|count_second\(4))) # (!\sec_cnt|count_second\(3))) # (!\sec_cnt|count_second\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(2),
	datab => \sec_cnt|count_second\(3),
	datac => \sec_cnt|count_second\(5),
	datad => \sec_cnt|count_second\(4),
	combout => \sec_cnt|Equal0~2_combout\);

-- Location: LCCOMB_X30_Y9_N30
\sec_cnt|Equal0~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|Equal0~3_combout\ = ((\sec_cnt|Equal0~2_combout\) # (!\sec_cnt|count_second\(0))) # (!\sec_cnt|count_second\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(1),
	datac => \sec_cnt|Equal0~2_combout\,
	datad => \sec_cnt|count_second\(0),
	combout => \sec_cnt|Equal0~3_combout\);

-- Location: LCCOMB_X31_Y9_N16
\sec_cnt|count_second[6]~28\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[6]~28_combout\ = (\sec_cnt|count_second\(6) & (\sec_cnt|count_second[5]~27\ $ (GND))) # (!\sec_cnt|count_second\(6) & (!\sec_cnt|count_second[5]~27\ & VCC))
-- \sec_cnt|count_second[6]~29\ = CARRY((\sec_cnt|count_second\(6) & !\sec_cnt|count_second[5]~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(6),
	datad => VCC,
	cin => \sec_cnt|count_second[5]~27\,
	combout => \sec_cnt|count_second[6]~28_combout\,
	cout => \sec_cnt|count_second[6]~29\);

-- Location: LCFF_X31_Y9_N17
\sec_cnt|count_second[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[6]~28_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(6));

-- Location: LCCOMB_X31_Y9_N18
\sec_cnt|count_second[7]~30\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[7]~30_combout\ = (\sec_cnt|count_second\(7) & (!\sec_cnt|count_second[6]~29\)) # (!\sec_cnt|count_second\(7) & ((\sec_cnt|count_second[6]~29\) # (GND)))
-- \sec_cnt|count_second[7]~31\ = CARRY((!\sec_cnt|count_second[6]~29\) # (!\sec_cnt|count_second\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(7),
	datad => VCC,
	cin => \sec_cnt|count_second[6]~29\,
	combout => \sec_cnt|count_second[7]~30_combout\,
	cout => \sec_cnt|count_second[7]~31\);

-- Location: LCFF_X31_Y9_N19
\sec_cnt|count_second[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[7]~30_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(7));

-- Location: LCCOMB_X31_Y9_N20
\sec_cnt|count_second[8]~32\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[8]~32_combout\ = (\sec_cnt|count_second\(8) & (\sec_cnt|count_second[7]~31\ $ (GND))) # (!\sec_cnt|count_second\(8) & (!\sec_cnt|count_second[7]~31\ & VCC))
-- \sec_cnt|count_second[8]~33\ = CARRY((\sec_cnt|count_second\(8) & !\sec_cnt|count_second[7]~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(8),
	datad => VCC,
	cin => \sec_cnt|count_second[7]~31\,
	combout => \sec_cnt|count_second[8]~32_combout\,
	cout => \sec_cnt|count_second[8]~33\);

-- Location: LCFF_X31_Y9_N21
\sec_cnt|count_second[8]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[8]~32_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(8));

-- Location: LCCOMB_X31_Y9_N22
\sec_cnt|count_second[9]~34\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[9]~34_combout\ = (\sec_cnt|count_second\(9) & (!\sec_cnt|count_second[8]~33\)) # (!\sec_cnt|count_second\(9) & ((\sec_cnt|count_second[8]~33\) # (GND)))
-- \sec_cnt|count_second[9]~35\ = CARRY((!\sec_cnt|count_second[8]~33\) # (!\sec_cnt|count_second\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(9),
	datad => VCC,
	cin => \sec_cnt|count_second[8]~33\,
	combout => \sec_cnt|count_second[9]~34_combout\,
	cout => \sec_cnt|count_second[9]~35\);

-- Location: LCFF_X31_Y9_N23
\sec_cnt|count_second[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[9]~34_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(9));

-- Location: LCCOMB_X30_Y9_N14
\sec_cnt|Equal0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|Equal0~1_combout\ = (\sec_cnt|count_second\(6)) # ((\sec_cnt|count_second\(7)) # ((!\sec_cnt|count_second\(9)) # (!\sec_cnt|count_second\(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(6),
	datab => \sec_cnt|count_second\(7),
	datac => \sec_cnt|count_second\(8),
	datad => \sec_cnt|count_second\(9),
	combout => \sec_cnt|Equal0~1_combout\);

-- Location: LCCOMB_X31_Y9_N0
\sec_cnt|count_second[13]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[13]~16_combout\ = (\Equal0~2_combout\) # ((!\sec_cnt|Equal0~0_combout\ & (!\sec_cnt|Equal0~3_combout\ & !\sec_cnt|Equal0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|Equal0~0_combout\,
	datab => \Equal0~2_combout\,
	datac => \sec_cnt|Equal0~3_combout\,
	datad => \sec_cnt|Equal0~1_combout\,
	combout => \sec_cnt|count_second[13]~16_combout\);

-- Location: LCFF_X31_Y9_N5
\sec_cnt|count_second[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[0]~14_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(0));

-- Location: LCFF_X31_Y9_N7
\sec_cnt|count_second[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[1]~18_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(1));

-- Location: LCFF_X30_Y9_N13
\myFIFO|myRam|ram_rtl_0_bypass[12]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \sec_cnt|count_second\(1),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(12));

-- Location: PIN_L1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\CLOCK_50~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_CLOCK_50,
	combout => \CLOCK_50~combout\);

-- Location: CLKCTRL_G2
\CLOCK_50~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLOCK_50~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLOCK_50~clkctrl_outclk\);

-- Location: LCCOMB_X31_Y9_N24
\sec_cnt|count_second[10]~36\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[10]~36_combout\ = (\sec_cnt|count_second\(10) & (\sec_cnt|count_second[9]~35\ $ (GND))) # (!\sec_cnt|count_second\(10) & (!\sec_cnt|count_second[9]~35\ & VCC))
-- \sec_cnt|count_second[10]~37\ = CARRY((\sec_cnt|count_second\(10) & !\sec_cnt|count_second[9]~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(10),
	datad => VCC,
	cin => \sec_cnt|count_second[9]~35\,
	combout => \sec_cnt|count_second[10]~36_combout\,
	cout => \sec_cnt|count_second[10]~37\);

-- Location: LCFF_X31_Y9_N25
\sec_cnt|count_second[10]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[10]~36_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(10));

-- Location: LCCOMB_X31_Y9_N26
\sec_cnt|count_second[11]~38\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[11]~38_combout\ = (\sec_cnt|count_second\(11) & (!\sec_cnt|count_second[10]~37\)) # (!\sec_cnt|count_second\(11) & ((\sec_cnt|count_second[10]~37\) # (GND)))
-- \sec_cnt|count_second[11]~39\ = CARRY((!\sec_cnt|count_second[10]~37\) # (!\sec_cnt|count_second\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(11),
	datad => VCC,
	cin => \sec_cnt|count_second[10]~37\,
	combout => \sec_cnt|count_second[11]~38_combout\,
	cout => \sec_cnt|count_second[11]~39\);

-- Location: LCFF_X31_Y9_N27
\sec_cnt|count_second[11]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[11]~38_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(11));

-- Location: LCCOMB_X31_Y9_N28
\sec_cnt|count_second[12]~40\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[12]~40_combout\ = (\sec_cnt|count_second\(12) & (\sec_cnt|count_second[11]~39\ $ (GND))) # (!\sec_cnt|count_second\(12) & (!\sec_cnt|count_second[11]~39\ & VCC))
-- \sec_cnt|count_second[12]~41\ = CARRY((\sec_cnt|count_second\(12) & !\sec_cnt|count_second[11]~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(12),
	datad => VCC,
	cin => \sec_cnt|count_second[11]~39\,
	combout => \sec_cnt|count_second[12]~40_combout\,
	cout => \sec_cnt|count_second[12]~41\);

-- Location: LCFF_X31_Y9_N29
\sec_cnt|count_second[12]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[12]~40_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(12));

-- Location: LCCOMB_X31_Y9_N30
\sec_cnt|count_second[13]~42\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[13]~42_combout\ = \sec_cnt|count_second[12]~41\ $ (\sec_cnt|count_second\(13))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \sec_cnt|count_second\(13),
	cin => \sec_cnt|count_second[12]~41\,
	combout => \sec_cnt|count_second[13]~42_combout\);

-- Location: LCFF_X31_Y9_N31
\sec_cnt|count_second[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[13]~42_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(13));

-- Location: M4K_X41_Y9
\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "initFIFO:myFIFO|ram:myRam|altsyncram:ram_rtl_0|altsyncram_48c1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 5,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 14,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 31,
	port_a_logical_ram_depth => 32,
	port_a_logical_ram_width => 14,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 5,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 14,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 31,
	port_b_logical_ram_depth => 32,
	port_b_logical_ram_width => 14,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock0",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => \myFIFO|we~1_combout\,
	portbrewe => VCC,
	clk0 => \CLOCK_50~clkctrl_outclk\,
	portadatain => \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTADATAIN_bus\,
	portaaddr => \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTAADDR_bus\,
	portbaddr => \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X30_Y9_N12
\toOut[1]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[1]~2_combout\ = (!\SW~combout\(9) & ((\myFIFO|myRam|ram~3_combout\ & (\myFIFO|myRam|ram_rtl_0_bypass\(12))) # (!\myFIFO|myRam|ram~3_combout\ & ((\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a1\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \myFIFO|myRam|ram~3_combout\,
	datac => \myFIFO|myRam|ram_rtl_0_bypass\(12),
	datad => \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a1\,
	combout => \toOut[1]~2_combout\);

-- Location: PIN_L2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[9]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(9),
	combout => \SW~combout\(9));

-- Location: LCCOMB_X29_Y11_N22
\toOut[1]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[1]~3_combout\ = (\toOut[1]~2_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \toOut[1]~2_combout\,
	datac => \SW~combout\(9),
	datad => \sec_cnt|count_second\(1),
	combout => \toOut[1]~3_combout\);

-- Location: LCFF_X40_Y9_N13
\myFIFO|myRam|ram_rtl_0_bypass[24]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \sec_cnt|count_second\(13),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(24));

-- Location: LCCOMB_X40_Y9_N12
\toOut[13]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[13]~4_combout\ = (!\SW~combout\(9) & ((\myFIFO|myRam|ram~3_combout\ & (\myFIFO|myRam|ram_rtl_0_bypass\(24))) # (!\myFIFO|myRam|ram~3_combout\ & ((\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a13\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \myFIFO|myRam|ram~3_combout\,
	datac => \myFIFO|myRam|ram_rtl_0_bypass\(24),
	datad => \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a13\,
	combout => \toOut[13]~4_combout\);

-- Location: LCCOMB_X39_Y10_N0
\toOut[13]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[13]~5_combout\ = (\toOut[13]~4_combout\) # ((\sec_cnt|count_second\(13) & \SW~combout\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(13),
	datab => \SW~combout\(9),
	datac => \toOut[13]~4_combout\,
	combout => \toOut[13]~5_combout\);

-- Location: LCCOMB_X39_Y10_N14
\Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\ = \toOut[11]~9_combout\ $ (VCC)
-- \Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\ = CARRY(\toOut[11]~9_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[11]~9_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\);

-- Location: LCCOMB_X39_Y10_N16
\Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\ = (\toOut[12]~7_combout\ & (\Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\ & VCC)) # (!\toOut[12]~7_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\))
-- \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ = CARRY((!\toOut[12]~7_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[12]~7_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\);

-- Location: LCCOMB_X39_Y10_N20
\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ = !\Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\);

-- Location: LCCOMB_X39_Y9_N12
\Mod0|auto_generated|divider|divider|StageOut[17]~134\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[17]~134_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & ((\toOut[12]~6_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(12)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[12]~6_combout\,
	datab => \SW~combout\(9),
	datac => \sec_cnt|count_second\(12),
	datad => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[17]~134_combout\);

-- Location: LCCOMB_X39_Y9_N24
\Mod0|auto_generated|divider|divider|StageOut[23]~138\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[23]~138_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[17]~134_combout\) # 
-- ((!\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datac => \Mod0|auto_generated|divider|divider|StageOut[17]~134_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[23]~138_combout\);

-- Location: LCFF_X37_Y9_N29
\myFIFO|myRam|ram_rtl_0_bypass[22]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \sec_cnt|count_second\(11),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(22));

-- Location: LCCOMB_X37_Y9_N28
\toOut[11]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[11]~8_combout\ = (!\SW~combout\(9) & ((\myFIFO|myRam|ram~3_combout\ & (\myFIFO|myRam|ram_rtl_0_bypass\(22))) # (!\myFIFO|myRam|ram~3_combout\ & ((\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a11\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|myRam|ram~3_combout\,
	datab => \SW~combout\(9),
	datac => \myFIFO|myRam|ram_rtl_0_bypass\(22),
	datad => \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a11\,
	combout => \toOut[11]~8_combout\);

-- Location: LCCOMB_X39_Y9_N26
\Mod0|auto_generated|divider|divider|StageOut[16]~135\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[16]~135_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & ((\toOut[11]~8_combout\) # ((\sec_cnt|count_second\(11) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(11),
	datab => \SW~combout\(9),
	datac => \toOut[11]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[16]~135_combout\);

-- Location: LCCOMB_X39_Y9_N30
\Mod0|auto_generated|divider|divider|StageOut[22]~139\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[22]~139_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[16]~135_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[16]~135_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[22]~139_combout\);

-- Location: LCCOMB_X39_Y9_N22
\Mod0|auto_generated|divider|divider|StageOut[18]~133\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[18]~133_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & ((\toOut[13]~4_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(13)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[13]~4_combout\,
	datab => \SW~combout\(9),
	datac => \sec_cnt|count_second\(13),
	datad => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[18]~133_combout\);

-- Location: LCCOMB_X39_Y9_N16
\Mod0|auto_generated|divider|divider|StageOut[17]~101\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[17]~101_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[17]~101_combout\);

-- Location: LCCOMB_X39_Y9_N18
\Mod0|auto_generated|divider|divider|StageOut[15]~137\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[15]~137_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & ((\toOut[10]~10_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(10)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[10]~10_combout\,
	datab => \SW~combout\(9),
	datac => \sec_cnt|count_second\(10),
	datad => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[15]~137_combout\);

-- Location: LCCOMB_X39_Y9_N8
\Mod0|auto_generated|divider|divider|add_sub_4_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[18]~100_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[18]~133_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[18]~100_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[18]~133_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\);

-- Location: LCCOMB_X39_Y9_N10
\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\);

-- Location: LCFF_X39_Y9_N29
\myFIFO|myRam|ram_rtl_0_bypass[21]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \sec_cnt|count_second\(10),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(21));

-- Location: LCCOMB_X39_Y9_N28
\toOut[10]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[10]~10_combout\ = (!\SW~combout\(9) & ((\myFIFO|myRam|ram~3_combout\ & (\myFIFO|myRam|ram_rtl_0_bypass\(21))) # (!\myFIFO|myRam|ram~3_combout\ & ((\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a10\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|myRam|ram~3_combout\,
	datab => \SW~combout\(9),
	datac => \myFIFO|myRam|ram_rtl_0_bypass\(21),
	datad => \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a10\,
	combout => \toOut[10]~10_combout\);

-- Location: LCCOMB_X38_Y9_N4
\Mod0|auto_generated|divider|divider|StageOut[21]~140\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[21]~140_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\toOut[10]~10_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(10)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \sec_cnt|count_second\(10),
	datac => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datad => \toOut[10]~10_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[21]~140_combout\);

-- Location: LCFF_X37_Y9_N23
\myFIFO|myRam|ram_rtl_0_bypass[20]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \sec_cnt|count_second\(9),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(20));

-- Location: LCCOMB_X37_Y9_N22
\toOut[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[9]~12_combout\ = (!\SW~combout\(9) & ((\myFIFO|myRam|ram~3_combout\ & (\myFIFO|myRam|ram_rtl_0_bypass\(20))) # (!\myFIFO|myRam|ram~3_combout\ & ((\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a9\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|myRam|ram~3_combout\,
	datab => \SW~combout\(9),
	datac => \myFIFO|myRam|ram_rtl_0_bypass\(20),
	datad => \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a9\,
	combout => \toOut[9]~12_combout\);

-- Location: LCCOMB_X38_Y9_N0
\Mod0|auto_generated|divider|divider|StageOut[20]~141\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[20]~141_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\toOut[9]~12_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \toOut[9]~12_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(9),
	combout => \Mod0|auto_generated|divider|divider|StageOut[20]~141_combout\);

-- Location: LCCOMB_X38_Y9_N10
\Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[20]~142_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[20]~141_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[20]~142_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[20]~141_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[20]~142_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[20]~141_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\);

-- Location: LCCOMB_X38_Y9_N16
\Mod0|auto_generated|divider|divider|add_sub_5_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[23]~103_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[23]~138_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[23]~103_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[23]~138_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\);

-- Location: LCCOMB_X38_Y9_N18
\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\);

-- Location: LCCOMB_X38_Y9_N8
\Mod0|auto_generated|divider|divider|StageOut[28]~143\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[28]~143_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[22]~139_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|StageOut[22]~139_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[28]~143_combout\);

-- Location: LCCOMB_X38_Y9_N30
\Mod0|auto_generated|divider|divider|StageOut[27]~144\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[27]~144_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[21]~140_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[21]~140_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[27]~144_combout\);

-- Location: LCCOMB_X37_Y9_N2
\Mod0|auto_generated|divider|divider|StageOut[26]~145\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[26]~145_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\toOut[9]~12_combout\) # ((\sec_cnt|count_second\(9) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datab => \toOut[9]~12_combout\,
	datac => \sec_cnt|count_second\(9),
	datad => \SW~combout\(9),
	combout => \Mod0|auto_generated|divider|divider|StageOut[26]~145_combout\);

-- Location: LCFF_X37_Y9_N5
\myFIFO|myRam|ram_rtl_0_bypass[19]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \sec_cnt|count_second\(8),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(19));

-- Location: LCCOMB_X37_Y9_N4
\toOut[8]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[8]~14_combout\ = (!\SW~combout\(9) & ((\myFIFO|myRam|ram~3_combout\ & (\myFIFO|myRam|ram_rtl_0_bypass\(19))) # (!\myFIFO|myRam|ram~3_combout\ & ((\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a8\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|myRam|ram~3_combout\,
	datab => \SW~combout\(9),
	datac => \myFIFO|myRam|ram_rtl_0_bypass\(19),
	datad => \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a8\,
	combout => \toOut[8]~14_combout\);

-- Location: LCCOMB_X37_Y9_N26
\Mod0|auto_generated|divider|divider|StageOut[25]~146\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[25]~146_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\toOut[8]~14_combout\) # ((\sec_cnt|count_second\(8) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(8),
	datab => \SW~combout\(9),
	datac => \toOut[8]~14_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[25]~146_combout\);

-- Location: LCCOMB_X37_Y9_N12
\Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[25]~147_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[25]~146_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[25]~147_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[25]~146_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[25]~147_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[25]~146_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~1\);

-- Location: LCCOMB_X37_Y9_N16
\Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[27]~107_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[27]~144_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[27]~107_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[27]~144_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[27]~107_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[27]~144_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[27]~107_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[27]~144_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~5\);

-- Location: LCCOMB_X37_Y9_N18
\Mod0|auto_generated|divider|divider|add_sub_6_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[28]~106_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[28]~143_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[28]~106_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[28]~143_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\);

-- Location: LCCOMB_X37_Y9_N20
\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\);

-- Location: LCCOMB_X37_Y10_N2
\Mod0|auto_generated|divider|divider|StageOut[33]~109\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[33]~109_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~4_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[33]~109_combout\);

-- Location: LCCOMB_X37_Y10_N28
\Mod0|auto_generated|divider|divider|StageOut[32]~110\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[32]~110_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~2_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[32]~110_combout\);

-- Location: LCCOMB_X37_Y10_N30
\Mod0|auto_generated|divider|divider|StageOut[31]~111\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[31]~111_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~0_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[31]~111_combout\);

-- Location: LCFF_X36_Y9_N5
\myFIFO|myRam|ram_rtl_0_bypass[18]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \sec_cnt|count_second\(7),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(18));

-- Location: LCCOMB_X36_Y9_N4
\toOut[7]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[7]~16_combout\ = (!\SW~combout\(9) & ((\myFIFO|myRam|ram~3_combout\ & (\myFIFO|myRam|ram_rtl_0_bypass\(18))) # (!\myFIFO|myRam|ram~3_combout\ & ((\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a7\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \myFIFO|myRam|ram~3_combout\,
	datac => \myFIFO|myRam|ram_rtl_0_bypass\(18),
	datad => \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a7\,
	combout => \toOut[7]~16_combout\);

-- Location: LCCOMB_X37_Y10_N14
\Mod0|auto_generated|divider|divider|StageOut[30]~151\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[30]~151_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ & ((\toOut[7]~16_combout\) # ((\sec_cnt|count_second\(7) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(7),
	datab => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datac => \toOut[7]~16_combout\,
	datad => \SW~combout\(9),
	combout => \Mod0|auto_generated|divider|divider|StageOut[30]~151_combout\);

-- Location: LCCOMB_X37_Y10_N20
\Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[31]~150_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[31]~111_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[31]~150_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[31]~111_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[31]~150_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[31]~111_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[31]~150_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[31]~111_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~3\);

-- Location: LCCOMB_X37_Y10_N24
\Mod0|auto_generated|divider|divider|add_sub_7_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_7_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[33]~148_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[33]~109_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[33]~148_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[33]~109_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[4]~7_cout\);

-- Location: LCCOMB_X37_Y10_N26
\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_7_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\);

-- Location: LCCOMB_X36_Y10_N28
\Mod0|auto_generated|divider|divider|StageOut[36]~155\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[36]~155_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\ & ((\toOut[7]~16_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(7)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \sec_cnt|count_second\(7),
	datac => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	datad => \toOut[7]~16_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[36]~155_combout\);

-- Location: LCFF_X36_Y9_N3
\myFIFO|myRam|ram_rtl_0_bypass[17]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \sec_cnt|count_second\(6),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(17));

-- Location: LCCOMB_X36_Y9_N2
\toOut[6]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[6]~18_combout\ = (!\SW~combout\(9) & ((\myFIFO|myRam|ram~3_combout\ & (\myFIFO|myRam|ram_rtl_0_bypass\(17))) # (!\myFIFO|myRam|ram~3_combout\ & ((\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a6\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \myFIFO|myRam|ram~3_combout\,
	datac => \myFIFO|myRam|ram_rtl_0_bypass\(17),
	datad => \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a6\,
	combout => \toOut[6]~18_combout\);

-- Location: LCCOMB_X36_Y10_N8
\Mod0|auto_generated|divider|divider|StageOut[35]~157\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[35]~157_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\ & ((\toOut[6]~18_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \toOut[6]~18_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(6),
	combout => \Mod0|auto_generated|divider|divider|StageOut[35]~157_combout\);

-- Location: LCCOMB_X36_Y10_N18
\Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[35]~156_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[35]~157_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[35]~156_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[35]~157_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[35]~156_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[35]~157_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~1\);

-- Location: LCCOMB_X36_Y10_N20
\Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[36]~114_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[36]~155_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[36]~114_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[36]~155_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[36]~114_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[36]~155_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[36]~114_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[36]~155_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~3\);

-- Location: LCCOMB_X37_Y9_N0
\Mod0|auto_generated|divider|divider|StageOut[32]~149\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[32]~149_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[26]~145_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[26]~145_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[32]~149_combout\);

-- Location: LCCOMB_X37_Y10_N10
\Mod0|auto_generated|divider|divider|StageOut[38]~153\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[38]~153_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[32]~149_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~2_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[32]~149_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[38]~153_combout\);

-- Location: LCCOMB_X36_Y10_N14
\Mod0|auto_generated|divider|divider|StageOut[37]~113\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[37]~113_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~2_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[37]~113_combout\);

-- Location: LCCOMB_X36_Y10_N24
\Mod0|auto_generated|divider|divider|add_sub_8_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_8_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[38]~112_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[38]~153_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[38]~112_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[38]~153_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[4]~7_cout\);

-- Location: LCCOMB_X36_Y10_N26
\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_8_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\);

-- Location: LCCOMB_X36_Y10_N4
\Mod0|auto_generated|divider|divider|StageOut[42]~159\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[42]~159_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[36]~155_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~0_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~0_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|StageOut[36]~155_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[42]~159_combout\);

-- Location: LCCOMB_X35_Y10_N24
\Mod0|auto_generated|divider|divider|StageOut[48]~163\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[48]~163_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[42]~159_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~2_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|StageOut[42]~159_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[48]~163_combout\);

-- Location: LCCOMB_X35_Y10_N22
\Mod0|auto_generated|divider|divider|StageOut[41]~160\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[41]~160_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ & ((\toOut[6]~18_combout\) # ((\sec_cnt|count_second\(6) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(6),
	datab => \SW~combout\(9),
	datac => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	datad => \toOut[6]~18_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[41]~160_combout\);

-- Location: LCCOMB_X35_Y10_N30
\Mod0|auto_generated|divider|divider|StageOut[47]~164\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[47]~164_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[41]~160_combout\) # 
-- ((!\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[41]~160_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~0_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[47]~164_combout\);

-- Location: LCCOMB_X36_Y10_N30
\Mod0|auto_generated|divider|divider|StageOut[43]~158\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[43]~158_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[37]~154_combout\) # 
-- ((!\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[37]~154_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~2_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[43]~158_combout\);

-- Location: LCCOMB_X35_Y10_N26
\Mod0|auto_generated|divider|divider|StageOut[42]~116\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[42]~116_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~2_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[42]~116_combout\);

-- Location: LCFF_X34_Y9_N3
\myFIFO|myRam|ram_rtl_0_bypass[16]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \sec_cnt|count_second\(5),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(16));

-- Location: LCCOMB_X34_Y9_N2
\toOut[5]~19\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[5]~19_combout\ = (!\SW~combout\(9) & ((\myFIFO|myRam|ram~3_combout\ & (\myFIFO|myRam|ram_rtl_0_bypass\(16))) # (!\myFIFO|myRam|ram~3_combout\ & ((\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a5\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \myFIFO|myRam|ram~3_combout\,
	datac => \myFIFO|myRam|ram_rtl_0_bypass\(16),
	datad => \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a5\,
	combout => \toOut[5]~19_combout\);

-- Location: LCCOMB_X35_Y10_N14
\Mod0|auto_generated|divider|divider|StageOut[40]~162\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[40]~162_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ & ((\toOut[5]~19_combout\) # ((\sec_cnt|count_second\(5) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(5),
	datab => \SW~combout\(9),
	datac => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	datad => \toOut[5]~19_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[40]~162_combout\);

-- Location: LCCOMB_X35_Y10_N0
\Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[40]~161_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[40]~162_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[40]~161_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[40]~162_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[40]~161_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[40]~162_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~1\);

-- Location: LCCOMB_X35_Y10_N6
\Mod0|auto_generated|divider|divider|add_sub_9_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_9_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[43]~115_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[43]~158_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[43]~115_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[43]~158_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[4]~7_cout\);

-- Location: LCCOMB_X35_Y10_N8
\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_9_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\);

-- Location: LCCOMB_X34_Y10_N22
\Mod0|auto_generated|divider|divider|StageOut[46]~165\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[46]~165_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ & ((\toOut[5]~19_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \sec_cnt|count_second\(5),
	datac => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	datad => \toOut[5]~19_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[46]~165_combout\);

-- Location: LCFF_X34_Y9_N1
\myFIFO|myRam|ram_rtl_0_bypass[15]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \sec_cnt|count_second\(4),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(15));

-- Location: LCCOMB_X34_Y9_N0
\toOut[4]~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[4]~20_combout\ = (!\SW~combout\(9) & ((\myFIFO|myRam|ram~3_combout\ & (\myFIFO|myRam|ram_rtl_0_bypass\(15))) # (!\myFIFO|myRam|ram~3_combout\ & ((\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a4\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \myFIFO|myRam|ram~3_combout\,
	datac => \myFIFO|myRam|ram_rtl_0_bypass\(15),
	datad => \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a4\,
	combout => \toOut[4]~20_combout\);

-- Location: LCCOMB_X34_Y10_N2
\Mod0|auto_generated|divider|divider|StageOut[45]~167\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[45]~167_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ & ((\toOut[4]~20_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \toOut[4]~20_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(4),
	combout => \Mod0|auto_generated|divider|divider|StageOut[45]~167_combout\);

-- Location: LCCOMB_X34_Y10_N6
\Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[45]~166_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[45]~167_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[45]~166_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[45]~167_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[45]~166_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[45]~167_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~1\);

-- Location: LCCOMB_X34_Y10_N8
\Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[46]~120_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[46]~165_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[46]~120_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[46]~165_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[46]~120_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[46]~165_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[46]~120_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[46]~165_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~3\);

-- Location: LCCOMB_X34_Y10_N10
\Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[47]~119_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[47]~164_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[47]~119_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[47]~164_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[47]~119_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[47]~164_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[47]~119_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[47]~164_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~5\);

-- Location: LCCOMB_X34_Y10_N12
\Mod0|auto_generated|divider|divider|add_sub_10_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_10_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[48]~118_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[48]~163_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[48]~118_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[48]~163_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[4]~7_cout\);

-- Location: LCCOMB_X34_Y10_N14
\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_10_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\);

-- Location: LCCOMB_X31_Y10_N0
\Mod0|auto_generated|divider|divider|StageOut[51]~123\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[51]~123_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~0_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[51]~123_combout\);

-- Location: LCFF_X40_Y9_N11
\myFIFO|myRam|ram_rtl_0_bypass[14]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \sec_cnt|count_second\(3),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(14));

-- Location: LCCOMB_X40_Y9_N10
\toOut[3]~21\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[3]~21_combout\ = (!\SW~combout\(9) & ((\myFIFO|myRam|ram~3_combout\ & (\myFIFO|myRam|ram_rtl_0_bypass\(14))) # (!\myFIFO|myRam|ram~3_combout\ & ((\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a3\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \myFIFO|myRam|ram~3_combout\,
	datac => \myFIFO|myRam|ram_rtl_0_bypass\(14),
	datad => \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a3\,
	combout => \toOut[3]~21_combout\);

-- Location: LCCOMB_X30_Y10_N22
\Mod0|auto_generated|divider|divider|StageOut[50]~171\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[50]~171_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ & ((\toOut[3]~21_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	datab => \SW~combout\(9),
	datac => \sec_cnt|count_second\(3),
	datad => \toOut[3]~21_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[50]~171_combout\);

-- Location: LCCOMB_X30_Y10_N6
\Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[50]~172_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[50]~171_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[50]~172_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[50]~171_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[50]~172_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[50]~171_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~1\);

-- Location: LCCOMB_X30_Y10_N8
\Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[51]~170_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[51]~123_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[51]~170_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[51]~123_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[51]~170_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[51]~123_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[51]~170_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[51]~123_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~3\);

-- Location: LCCOMB_X34_Y10_N28
\Mod0|auto_generated|divider|divider|StageOut[53]~121\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[53]~121_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~4_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[53]~121_combout\);

-- Location: LCCOMB_X34_Y10_N26
\Mod0|auto_generated|divider|divider|StageOut[52]~169\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[52]~169_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[46]~165_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~0_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~0_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|StageOut[46]~165_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[52]~169_combout\);

-- Location: LCCOMB_X30_Y10_N12
\Mod0|auto_generated|divider|divider|add_sub_11_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_11_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[53]~168_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[53]~121_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[53]~168_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[53]~121_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[4]~7_cout\);

-- Location: LCCOMB_X30_Y10_N14
\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_11_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\);

-- Location: LCCOMB_X30_Y10_N20
\Mod0|auto_generated|divider|divider|StageOut[51]~170\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[51]~170_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ & ((\toOut[4]~20_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[4]~20_combout\,
	datab => \SW~combout\(9),
	datac => \sec_cnt|count_second\(4),
	datad => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[51]~170_combout\);

-- Location: LCCOMB_X30_Y10_N24
\Mod0|auto_generated|divider|divider|StageOut[57]~174\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[57]~174_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[51]~170_combout\) # 
-- ((!\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	datac => \Mod0|auto_generated|divider|divider|StageOut[51]~170_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~0_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[57]~174_combout\);

-- Location: LCCOMB_X30_Y10_N0
\Mod0|auto_generated|divider|divider|StageOut[63]~180\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[63]~180_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[57]~174_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~2_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|StageOut[57]~174_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[63]~180_combout\);

-- Location: LCCOMB_X30_Y10_N26
\Mod0|auto_generated|divider|divider|StageOut[56]~175\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[56]~175_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ & ((\toOut[3]~21_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	datac => \sec_cnt|count_second\(3),
	datad => \toOut[3]~21_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[56]~175_combout\);

-- Location: LCCOMB_X30_Y10_N30
\Mod0|auto_generated|divider|divider|StageOut[62]~181\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[62]~181_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[56]~175_combout\) # 
-- ((!\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[56]~175_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~0_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[62]~181_combout\);

-- Location: LCCOMB_X30_Y10_N18
\Mod0|auto_generated|divider|divider|StageOut[58]~173\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[58]~173_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[52]~169_combout\) # 
-- ((!\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[52]~169_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~2_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[58]~173_combout\);

-- Location: LCCOMB_X30_Y10_N28
\Mod0|auto_generated|divider|divider|StageOut[56]~126\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[56]~126_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~0_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[56]~126_combout\);

-- Location: LCFF_X30_Y9_N19
\myFIFO|myRam|ram_rtl_0_bypass[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \sec_cnt|count_second\(2),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(13));

-- Location: LCCOMB_X30_Y9_N18
\toOut[2]~22\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[2]~22_combout\ = (!\SW~combout\(9) & ((\myFIFO|myRam|ram~3_combout\ & (\myFIFO|myRam|ram_rtl_0_bypass\(13))) # (!\myFIFO|myRam|ram~3_combout\ & ((\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a2\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \myFIFO|myRam|ram~3_combout\,
	datac => \myFIFO|myRam|ram_rtl_0_bypass\(13),
	datad => \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a2\,
	combout => \toOut[2]~22_combout\);

-- Location: LCCOMB_X30_Y9_N10
\Mod0|auto_generated|divider|divider|StageOut[55]~177\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[55]~177_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ & ((\toOut[2]~22_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \toOut[2]~22_combout\,
	datac => \sec_cnt|count_second\(2),
	datad => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[55]~177_combout\);

-- Location: LCCOMB_X29_Y10_N0
\Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[55]~176_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[55]~177_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[55]~176_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[55]~177_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[55]~176_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[55]~177_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~1\);

-- Location: LCCOMB_X29_Y10_N2
\Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[56]~175_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[56]~126_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[56]~175_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[56]~126_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[56]~175_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[56]~126_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[56]~175_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[56]~126_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\);

-- Location: LCCOMB_X29_Y10_N6
\Mod0|auto_generated|divider|divider|add_sub_12_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_12_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[58]~124_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[58]~173_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[58]~124_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[58]~173_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[4]~7_cout\);

-- Location: LCCOMB_X29_Y10_N8
\Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_12_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\);

-- Location: LCCOMB_X29_Y10_N30
\Mod0|auto_generated|divider|divider|StageOut[61]~129\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[61]~129_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~0_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[61]~129_combout\);

-- Location: LCCOMB_X30_Y9_N16
\Mod0|auto_generated|divider|divider|StageOut[60]~178\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[60]~178_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ & ((\toOut[1]~2_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[1]~2_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\,
	datac => \SW~combout\(9),
	datad => \sec_cnt|count_second\(1),
	combout => \Mod0|auto_generated|divider|divider|StageOut[60]~178_combout\);

-- Location: LCCOMB_X29_Y10_N18
\Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[61]~182_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[61]~129_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[61]~182_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[61]~129_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[61]~182_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[61]~129_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[61]~182_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[61]~129_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~3\);

-- Location: LCCOMB_X29_Y10_N22
\Mod0|auto_generated|divider|divider|add_sub_13_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_13_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[63]~127_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[63]~180_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[63]~127_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[63]~180_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[4]~7_cout\);

-- Location: LCCOMB_X29_Y10_N24
\Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_13_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\);

-- Location: LCCOMB_X29_Y10_N14
\Mod0|auto_generated|divider|divider|StageOut[66]~130\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\ & ((\toOut[1]~3_combout\))) # (!\Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\ & 
-- (\Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~0_combout\,
	datab => \toOut[1]~3_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\);

-- Location: LCCOMB_X29_Y10_N12
\Mod0|auto_generated|divider|divider|StageOut[62]~128\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[62]~128_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~2_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[62]~128_combout\);

-- Location: LCCOMB_X29_Y10_N26
\Mod0|auto_generated|divider|divider|StageOut[68]~132\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\ & (((\Mod0|auto_generated|divider|divider|StageOut[62]~181_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[62]~128_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\ & (\Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~4_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[62]~181_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|StageOut[62]~128_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\);

-- Location: LCCOMB_X29_Y10_N28
\Mod0|auto_generated|divider|divider|StageOut[67]~131\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[61]~182_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|StageOut[61]~129_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\ & (((\Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[61]~182_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[61]~129_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~2_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\);

-- Location: LCFF_X30_Y9_N29
\myFIFO|myRam|ram_rtl_0_bypass[11]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \sec_cnt|count_second\(0),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(11));

-- Location: LCCOMB_X30_Y9_N28
\toOut[0]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[0]~0_combout\ = (!\SW~combout\(9) & ((\myFIFO|myRam|ram~3_combout\ & (\myFIFO|myRam|ram_rtl_0_bypass\(11))) # (!\myFIFO|myRam|ram~3_combout\ & ((\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \myFIFO|myRam|ram~3_combout\,
	datac => \myFIFO|myRam|ram_rtl_0_bypass\(11),
	datad => \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a0~portbdataout\,
	combout => \toOut[0]~0_combout\);

-- Location: LCCOMB_X30_Y9_N6
\toOut[0]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[0]~1_combout\ = (\toOut[0]~0_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \toOut[0]~0_combout\,
	datac => \SW~combout\(9),
	datad => \sec_cnt|count_second\(0),
	combout => \toOut[0]~1_combout\);

-- Location: LCCOMB_X1_Y18_N20
\dec0|WideOr6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr6~0_combout\ = (\Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\ & (\Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\)) # (!\Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\ & 
-- (\Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\ $ (((!\Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\ & \toOut[0]~1_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100111011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\,
	datac => \Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\,
	datad => \toOut[0]~1_combout\,
	combout => \dec0|WideOr6~0_combout\);

-- Location: LCCOMB_X1_Y18_N10
\dec0|WideOr5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr5~0_combout\ = (\Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\) # ((\Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\)))) # 
-- (!\Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\ & (\Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\ & (\Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\ $ (\toOut[0]~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100011101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\,
	datac => \Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\,
	datad => \toOut[0]~1_combout\,
	combout => \dec0|WideOr5~0_combout\);

-- Location: LCCOMB_X1_Y18_N12
\dec0|WideOr4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr4~0_combout\ = (\Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\ & (((\Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\)))) # (!\Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\ & 
-- (\Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\) # (!\toOut[0]~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\,
	datac => \Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\,
	datad => \toOut[0]~1_combout\,
	combout => \dec0|WideOr4~0_combout\);

-- Location: LCCOMB_X1_Y18_N6
\dec0|WideOr3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr3~0_combout\ = (\Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\) # ((\Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\ & \toOut[0]~1_combout\)))) # 
-- (!\Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\ & (\Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\ $ (((!\Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\ & \toOut[0]~1_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110100111011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\,
	datac => \Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\,
	datad => \toOut[0]~1_combout\,
	combout => \dec0|WideOr3~0_combout\);

-- Location: LCCOMB_X1_Y18_N4
\dec0|WideOr2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr2~0_combout\ = (\toOut[0]~1_combout\) # ((\Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\ & (\Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\)) # (!\Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\ & 
-- ((\Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\,
	datac => \Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\,
	datad => \toOut[0]~1_combout\,
	combout => \dec0|WideOr2~0_combout\);

-- Location: LCCOMB_X1_Y18_N26
\dec0|WideOr1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr1~0_combout\ = (\Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\) # ((\Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\ & \toOut[0]~1_combout\)))) # 
-- (!\Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\) # ((!\Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\ & \toOut[0]~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101111001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\,
	datac => \Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\,
	datad => \toOut[0]~1_combout\,
	combout => \dec0|WideOr1~0_combout\);

-- Location: LCCOMB_X1_Y18_N0
\dec0|WideOr0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr0~0_combout\ = (\Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\ & ((!\toOut[0]~1_combout\) # (!\Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\)))) 
-- # (!\Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\ & (\Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\ $ ((\Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001011000110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[66]~130_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[68]~132_combout\,
	datac => \Mod0|auto_generated|divider|divider|StageOut[67]~131_combout\,
	datad => \toOut[0]~1_combout\,
	combout => \dec0|WideOr0~0_combout\);

-- Location: LCCOMB_X39_Y10_N10
\toOut[11]~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[11]~9_combout\ = (\toOut[11]~8_combout\) # ((\sec_cnt|count_second\(11) & \SW~combout\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(11),
	datab => \SW~combout\(9),
	datad => \toOut[11]~8_combout\,
	combout => \toOut[11]~9_combout\);

-- Location: LCCOMB_X38_Y12_N0
\toOut[10]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[10]~11_combout\ = (\toOut[10]~10_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(10)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[10]~10_combout\,
	datac => \SW~combout\(9),
	datad => \sec_cnt|count_second\(10),
	combout => \toOut[10]~11_combout\);

-- Location: LCCOMB_X38_Y12_N26
\toOut[9]~13\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[9]~13_combout\ = (\toOut[9]~12_combout\) # ((\sec_cnt|count_second\(9) & \SW~combout\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(9),
	datac => \SW~combout\(9),
	datad => \toOut[9]~12_combout\,
	combout => \toOut[9]~13_combout\);

-- Location: LCCOMB_X38_Y12_N12
\Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\ = \toOut[9]~13_combout\ $ (VCC)
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\ = CARRY(\toOut[9]~13_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \toOut[9]~13_combout\,
	datad => VCC,
	combout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\);

-- Location: LCCOMB_X38_Y12_N14
\Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\ = (\toOut[10]~11_combout\ & (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\ & VCC)) # (!\toOut[10]~11_combout\ & 
-- (!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\))
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\ = CARRY((!\toOut[10]~11_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \toOut[10]~11_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\);

-- Location: LCCOMB_X38_Y12_N20
\Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\ = (\toOut[13]~5_combout\ & (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ $ (GND))) # (!\toOut[13]~5_combout\ & 
-- (!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ & VCC))
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\ = CARRY((\toOut[13]~5_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \toOut[13]~5_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\);

-- Location: LCCOMB_X38_Y12_N22
\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ = !\Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\);

-- Location: LCFF_X39_Y9_N1
\myFIFO|myRam|ram_rtl_0_bypass[23]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \sec_cnt|count_second\(12),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myFIFO|myRam|ram_rtl_0_bypass\(23));

-- Location: LCCOMB_X39_Y9_N0
\toOut[12]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[12]~6_combout\ = (!\SW~combout\(9) & ((\myFIFO|myRam|ram~3_combout\ & (\myFIFO|myRam|ram_rtl_0_bypass\(23))) # (!\myFIFO|myRam|ram~3_combout\ & ((\myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a12\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myFIFO|myRam|ram~3_combout\,
	datab => \SW~combout\(9),
	datac => \myFIFO|myRam|ram_rtl_0_bypass\(23),
	datad => \myFIFO|myRam|ram_rtl_0|auto_generated|ram_block1a12\,
	combout => \toOut[12]~6_combout\);

-- Location: LCCOMB_X38_Y12_N10
\Mod1|auto_generated|divider|divider|StageOut[53]~147\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[53]~147_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\toOut[12]~6_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(12)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \sec_cnt|count_second\(12),
	datac => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \toOut[12]~6_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[53]~147_combout\);

-- Location: LCCOMB_X38_Y12_N28
\Mod1|auto_generated|divider|divider|StageOut[52]~148\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[52]~148_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\toOut[11]~8_combout\) # ((\sec_cnt|count_second\(11) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datab => \sec_cnt|count_second\(11),
	datac => \SW~combout\(9),
	datad => \toOut[11]~8_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[52]~148_combout\);

-- Location: LCCOMB_X37_Y12_N8
\Mod1|auto_generated|divider|divider|StageOut[51]~105\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[51]~105_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[51]~105_combout\);

-- Location: LCCOMB_X37_Y12_N14
\Mod1|auto_generated|divider|divider|StageOut[50]~106\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[50]~106_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[50]~106_combout\);

-- Location: LCCOMB_X37_Y12_N0
\Mod1|auto_generated|divider|divider|StageOut[49]~152\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[49]~152_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\toOut[8]~14_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(8)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[8]~14_combout\,
	datab => \SW~combout\(9),
	datac => \sec_cnt|count_second\(8),
	datad => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[49]~152_combout\);

-- Location: LCCOMB_X37_Y12_N16
\Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\ = (((\Mod1|auto_generated|divider|divider|StageOut[49]~151_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[49]~152_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[49]~151_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[49]~152_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[49]~151_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[49]~152_combout\,
	datad => VCC,
	combout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\);

-- Location: LCCOMB_X37_Y12_N18
\Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\ & (((\Mod1|auto_generated|divider|divider|StageOut[50]~150_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[50]~106_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\ & (!\Mod1|auto_generated|divider|divider|StageOut[50]~150_combout\ & 
-- (!\Mod1|auto_generated|divider|divider|StageOut[50]~106_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[50]~150_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[50]~106_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[50]~150_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[50]~106_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\);

-- Location: LCCOMB_X37_Y12_N22
\Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\ = (\Mod1|auto_generated|divider|divider|StageOut[52]~104_combout\ & (((!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\)))) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[52]~104_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[52]~148_combout\ & (!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\)) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[52]~148_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\) # (GND)))))
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ = CARRY(((!\Mod1|auto_generated|divider|divider|StageOut[52]~104_combout\ & !\Mod1|auto_generated|divider|divider|StageOut[52]~148_combout\)) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[52]~104_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[52]~148_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\);

-- Location: LCCOMB_X37_Y12_N24
\Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ & (((\Mod1|auto_generated|divider|divider|StageOut[53]~103_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[53]~147_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ & ((((\Mod1|auto_generated|divider|divider|StageOut[53]~103_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[53]~147_combout\)))))
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\ = CARRY((!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ & ((\Mod1|auto_generated|divider|divider|StageOut[53]~103_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[53]~147_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[53]~103_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[53]~147_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\);

-- Location: LCCOMB_X38_Y12_N24
\Mod1|auto_generated|divider|divider|StageOut[54]~102\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[54]~102_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[54]~102_combout\);

-- Location: LCCOMB_X37_Y12_N26
\Mod1|auto_generated|divider|divider|add_sub_7_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[54]~146_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[54]~102_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[54]~146_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[54]~102_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\);

-- Location: LCCOMB_X37_Y12_N28
\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ = \Mod1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\);

-- Location: LCCOMB_X37_Y11_N0
\Mod1|auto_generated|divider|divider|StageOut[62]~107\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[62]~107_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[62]~107_combout\);

-- Location: LCCOMB_X36_Y12_N4
\Mod1|auto_generated|divider|divider|StageOut[61]~108\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[61]~108_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[61]~108_combout\);

-- Location: LCCOMB_X37_Y12_N30
\Mod1|auto_generated|divider|divider|StageOut[60]~155\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[60]~155_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[51]~149_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[51]~149_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[60]~155_combout\);

-- Location: LCCOMB_X36_Y12_N28
\Mod1|auto_generated|divider|divider|StageOut[59]~110\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[59]~110_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[59]~110_combout\);

-- Location: LCCOMB_X33_Y11_N0
\Mod1|auto_generated|divider|divider|StageOut[58]~157\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[58]~157_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\toOut[8]~14_combout\) # ((\sec_cnt|count_second\(8) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datab => \sec_cnt|count_second\(8),
	datac => \SW~combout\(9),
	datad => \toOut[8]~14_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[58]~157_combout\);

-- Location: LCCOMB_X32_Y12_N12
\Mod1|auto_generated|divider|divider|StageOut[57]~159\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[57]~159_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\toOut[7]~16_combout\) # ((\sec_cnt|count_second\(7) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(7),
	datab => \toOut[7]~16_combout\,
	datac => \SW~combout\(9),
	datad => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[57]~159_combout\);

-- Location: LCCOMB_X36_Y12_N18
\Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ & ((((\Mod1|auto_generated|divider|divider|StageOut[59]~156_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[59]~110_combout\))))) # (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ & ((\Mod1|auto_generated|divider|divider|StageOut[59]~156_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|StageOut[59]~110_combout\) # (GND))))
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[59]~156_combout\) # ((\Mod1|auto_generated|divider|divider|StageOut[59]~110_combout\) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[59]~156_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[59]~110_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\);

-- Location: LCCOMB_X36_Y12_N20
\Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\ = (\Mod1|auto_generated|divider|divider|StageOut[60]~109_combout\ & (((!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\)))) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[60]~109_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[60]~155_combout\ & (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\)) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[60]~155_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\) # (GND)))))
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ = CARRY(((!\Mod1|auto_generated|divider|divider|StageOut[60]~109_combout\ & !\Mod1|auto_generated|divider|divider|StageOut[60]~155_combout\)) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[60]~109_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[60]~155_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\);

-- Location: LCCOMB_X36_Y12_N24
\Mod1|auto_generated|divider|divider|add_sub_8_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[62]~153_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[62]~107_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[62]~153_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[62]~107_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\);

-- Location: LCCOMB_X36_Y12_N26
\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ = \Mod1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\);

-- Location: LCCOMB_X35_Y12_N14
\Mod1|auto_generated|divider|divider|StageOut[67]~115\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[67]~115_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[67]~115_combout\);

-- Location: LCCOMB_X36_Y12_N8
\Mod1|auto_generated|divider|divider|StageOut[66]~116\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[66]~116_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[57]~158_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[57]~159_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[57]~158_combout\,
	datac => \Mod1|auto_generated|divider|divider|StageOut[57]~159_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[66]~116_combout\);

-- Location: LCCOMB_X32_Y12_N22
\Mod1|auto_generated|divider|divider|StageOut[65]~164\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[65]~164_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\toOut[6]~18_combout\) # ((\sec_cnt|count_second\(6) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(6),
	datab => \toOut[6]~18_combout\,
	datac => \SW~combout\(9),
	datad => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[65]~164_combout\);

-- Location: LCCOMB_X35_Y12_N18
\Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~2_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~1\ & (((\Mod1|auto_generated|divider|divider|StageOut[66]~117_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[66]~116_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~1\ & (!\Mod1|auto_generated|divider|divider|StageOut[66]~117_combout\ & 
-- (!\Mod1|auto_generated|divider|divider|StageOut[66]~116_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~3\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[66]~117_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[66]~116_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[66]~117_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[66]~116_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~1\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~2_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~3\);

-- Location: LCCOMB_X35_Y12_N20
\Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~4_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~3\ & ((((\Mod1|auto_generated|divider|divider|StageOut[67]~163_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[67]~115_combout\))))) # (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~3\ & ((\Mod1|auto_generated|divider|divider|StageOut[67]~163_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|StageOut[67]~115_combout\) # (GND))))
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~5\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[67]~163_combout\) # ((\Mod1|auto_generated|divider|divider|StageOut[67]~115_combout\) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[67]~163_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[67]~115_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~3\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~4_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~5\);

-- Location: LCCOMB_X36_Y12_N30
\Mod1|auto_generated|divider|divider|StageOut[70]~160\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[70]~160_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[61]~154_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[61]~154_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[70]~160_combout\);

-- Location: LCCOMB_X36_Y12_N12
\Mod1|auto_generated|divider|divider|StageOut[69]~161\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[69]~161_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[60]~155_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[60]~155_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[69]~161_combout\);

-- Location: LCCOMB_X36_Y12_N0
\Mod1|auto_generated|divider|divider|StageOut[68]~162\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[68]~162_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[59]~156_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[59]~156_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[68]~162_combout\);

-- Location: LCCOMB_X35_Y12_N26
\Mod1|auto_generated|divider|divider|add_sub_9_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[70]~112_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[70]~160_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[70]~112_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[70]~160_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~9\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\);

-- Location: LCCOMB_X35_Y12_N28
\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ = \Mod1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\);

-- Location: LCCOMB_X36_Y12_N2
\Mod1|auto_generated|divider|divider|StageOut[67]~163\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[67]~163_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[58]~157_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[58]~157_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[67]~163_combout\);

-- Location: LCCOMB_X35_Y12_N30
\Mod1|auto_generated|divider|divider|StageOut[76]~168\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[76]~168_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[67]~163_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[67]~163_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[76]~168_combout\);

-- Location: LCCOMB_X34_Y12_N4
\Mod1|auto_generated|divider|divider|StageOut[85]~173\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[85]~173_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[76]~168_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~4_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~4_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[76]~168_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[85]~173_combout\);

-- Location: LCCOMB_X34_Y12_N14
\Mod1|auto_generated|divider|divider|StageOut[75]~121\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[75]~121_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~2_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[75]~121_combout\);

-- Location: LCCOMB_X33_Y12_N16
\Mod1|auto_generated|divider|divider|StageOut[74]~122\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[74]~122_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[65]~165_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[65]~164_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[65]~165_combout\,
	datac => \Mod1|auto_generated|divider|divider|StageOut[65]~164_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[74]~122_combout\);

-- Location: LCCOMB_X32_Y12_N2
\Mod1|auto_generated|divider|divider|StageOut[73]~170\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[73]~170_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & ((\toOut[5]~19_combout\) # ((\sec_cnt|count_second\(5) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[5]~19_combout\,
	datab => \sec_cnt|count_second\(5),
	datac => \SW~combout\(9),
	datad => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[73]~170_combout\);

-- Location: LCCOMB_X34_Y12_N18
\Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~0_combout\ = (((\Mod1|auto_generated|divider|divider|StageOut[73]~171_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[73]~170_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~1\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[73]~171_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[73]~170_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[73]~171_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[73]~170_combout\,
	datad => VCC,
	combout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~0_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~1\);

-- Location: LCCOMB_X34_Y12_N20
\Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~2_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~1\ & (((\Mod1|auto_generated|divider|divider|StageOut[74]~123_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[74]~122_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~1\ & (!\Mod1|auto_generated|divider|divider|StageOut[74]~123_combout\ & 
-- (!\Mod1|auto_generated|divider|divider|StageOut[74]~122_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~3\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[74]~123_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[74]~122_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[74]~123_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[74]~122_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~1\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~2_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~3\);

-- Location: LCCOMB_X34_Y12_N22
\Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~4_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~3\ & ((((\Mod1|auto_generated|divider|divider|StageOut[75]~169_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[75]~121_combout\))))) # (!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~3\ & ((\Mod1|auto_generated|divider|divider|StageOut[75]~169_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|StageOut[75]~121_combout\) # (GND))))
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~5\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[75]~169_combout\) # ((\Mod1|auto_generated|divider|divider|StageOut[75]~121_combout\) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[75]~169_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[75]~121_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~3\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~4_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~5\);

-- Location: LCCOMB_X35_Y12_N10
\Mod1|auto_generated|divider|divider|StageOut[78]~166\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[78]~166_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[69]~161_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|StageOut[69]~161_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[78]~166_combout\);

-- Location: LCCOMB_X35_Y12_N8
\Mod1|auto_generated|divider|divider|StageOut[77]~167\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[77]~167_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[68]~162_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|StageOut[68]~162_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[77]~167_combout\);

-- Location: LCCOMB_X34_Y12_N28
\Mod1|auto_generated|divider|divider|add_sub_10_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[7]~11_cout\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[78]~118_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[78]~166_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[78]~118_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[78]~166_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~9\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[7]~11_cout\);

-- Location: LCCOMB_X34_Y12_N30
\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ = \Mod1|auto_generated|divider|divider|add_sub_10_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[7]~11_cout\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\);

-- Location: LCCOMB_X32_Y12_N10
\Mod1|auto_generated|divider|divider|StageOut[84]~126\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[84]~126_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~4_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~4_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[84]~126_combout\);

-- Location: LCCOMB_X32_Y12_N28
\Mod1|auto_generated|divider|divider|StageOut[83]~127\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[83]~127_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~2_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[83]~127_combout\);

-- Location: LCCOMB_X32_Y12_N30
\Mod1|auto_generated|divider|divider|StageOut[82]~128\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[82]~128_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[73]~171_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[73]~170_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[73]~171_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[73]~170_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[82]~128_combout\);

-- Location: LCCOMB_X31_Y12_N28
\Mod1|auto_generated|divider|divider|StageOut[81]~177\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[81]~177_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & ((\toOut[4]~20_combout\) # ((\sec_cnt|count_second\(4) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(4),
	datab => \SW~combout\(9),
	datac => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datad => \toOut[4]~20_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[81]~177_combout\);

-- Location: LCCOMB_X31_Y12_N10
\Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~0_combout\ = (((\Mod1|auto_generated|divider|divider|StageOut[81]~176_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[81]~177_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~1\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[81]~176_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[81]~177_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[81]~176_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[81]~177_combout\,
	datad => VCC,
	combout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~0_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~1\);

-- Location: LCCOMB_X31_Y12_N12
\Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~2_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~1\ & (((\Mod1|auto_generated|divider|divider|StageOut[82]~129_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[82]~128_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~1\ & (!\Mod1|auto_generated|divider|divider|StageOut[82]~129_combout\ & 
-- (!\Mod1|auto_generated|divider|divider|StageOut[82]~128_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~3\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[82]~129_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[82]~128_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[82]~129_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[82]~128_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~1\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~2_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~3\);

-- Location: LCCOMB_X31_Y12_N14
\Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~4_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~3\ & ((((\Mod1|auto_generated|divider|divider|StageOut[83]~175_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[83]~127_combout\))))) # (!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~3\ & ((\Mod1|auto_generated|divider|divider|StageOut[83]~175_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|StageOut[83]~127_combout\) # (GND))))
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~5\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[83]~175_combout\) # ((\Mod1|auto_generated|divider|divider|StageOut[83]~127_combout\) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[83]~175_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[83]~127_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~3\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~4_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~5\);

-- Location: LCCOMB_X31_Y12_N16
\Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~6_combout\ = (\Mod1|auto_generated|divider|divider|StageOut[84]~174_combout\ & (((!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~5\)))) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[84]~174_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[84]~126_combout\ & (!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~5\)) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[84]~126_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~5\) # (GND)))))
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~7\ = CARRY(((!\Mod1|auto_generated|divider|divider|StageOut[84]~174_combout\ & !\Mod1|auto_generated|divider|divider|StageOut[84]~126_combout\)) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[84]~174_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[84]~126_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~5\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~6_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~7\);

-- Location: LCCOMB_X31_Y12_N18
\Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~8_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~7\ & (((\Mod1|auto_generated|divider|divider|StageOut[85]~125_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[85]~173_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~7\ & ((((\Mod1|auto_generated|divider|divider|StageOut[85]~125_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[85]~173_combout\)))))
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~9\ = CARRY((!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~7\ & ((\Mod1|auto_generated|divider|divider|StageOut[85]~125_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[85]~173_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[85]~125_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[85]~173_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~7\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~8_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~9\);

-- Location: LCCOMB_X34_Y12_N6
\Mod1|auto_generated|divider|divider|StageOut[86]~172\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[86]~172_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[77]~167_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~6_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~6_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|StageOut[77]~167_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[86]~172_combout\);

-- Location: LCCOMB_X31_Y12_N20
\Mod1|auto_generated|divider|divider|add_sub_11_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[7]~11_cout\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[86]~124_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[86]~172_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[86]~124_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[86]~172_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~9\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[7]~11_cout\);

-- Location: LCCOMB_X31_Y12_N22
\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ = \Mod1|auto_generated|divider|divider|add_sub_11_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[7]~11_cout\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\);

-- Location: LCCOMB_X31_Y12_N8
\Mod1|auto_generated|divider|divider|StageOut[94]~130\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[94]~130_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~8_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~8_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[94]~130_combout\);

-- Location: LCCOMB_X30_Y12_N28
\Mod1|auto_generated|divider|divider|StageOut[93]~131\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[93]~131_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~6_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~6_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[93]~131_combout\);

-- Location: LCCOMB_X31_Y12_N0
\Mod1|auto_generated|divider|divider|StageOut[92]~180\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[92]~180_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[83]~175_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[83]~175_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~2_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[92]~180_combout\);

-- Location: LCCOMB_X31_Y12_N30
\Mod1|auto_generated|divider|divider|StageOut[91]~133\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[91]~133_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~2_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[91]~133_combout\);

-- Location: LCCOMB_X31_Y12_N2
\Mod1|auto_generated|divider|divider|StageOut[90]~135\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[90]~135_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~0_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[90]~135_combout\);

-- Location: LCCOMB_X30_Y12_N2
\Mod1|auto_generated|divider|divider|StageOut[89]~182\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[89]~182_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & ((\toOut[3]~21_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	datac => \sec_cnt|count_second\(3),
	datad => \toOut[3]~21_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[89]~182_combout\);

-- Location: LCCOMB_X30_Y12_N8
\Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~0_combout\ = (((\Mod1|auto_generated|divider|divider|StageOut[89]~183_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[89]~182_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~1\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[89]~183_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[89]~182_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[89]~183_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[89]~182_combout\,
	datad => VCC,
	combout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~0_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~1\);

-- Location: LCCOMB_X30_Y12_N10
\Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~2_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~1\ & (((\Mod1|auto_generated|divider|divider|StageOut[90]~134_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[90]~135_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~1\ & (!\Mod1|auto_generated|divider|divider|StageOut[90]~134_combout\ & 
-- (!\Mod1|auto_generated|divider|divider|StageOut[90]~135_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~3\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[90]~134_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[90]~135_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[90]~134_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[90]~135_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~1\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~2_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~3\);

-- Location: LCCOMB_X30_Y12_N12
\Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~4_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~3\ & ((((\Mod1|auto_generated|divider|divider|StageOut[91]~181_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[91]~133_combout\))))) # (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~3\ & ((\Mod1|auto_generated|divider|divider|StageOut[91]~181_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|StageOut[91]~133_combout\) # (GND))))
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~5\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[91]~181_combout\) # ((\Mod1|auto_generated|divider|divider|StageOut[91]~133_combout\) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[91]~181_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[91]~133_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~3\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~4_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~5\);

-- Location: LCCOMB_X30_Y12_N14
\Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~6_combout\ = (\Mod1|auto_generated|divider|divider|StageOut[92]~132_combout\ & (((!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~5\)))) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[92]~132_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[92]~180_combout\ & (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~5\)) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[92]~180_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~5\) # (GND)))))
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~7\ = CARRY(((!\Mod1|auto_generated|divider|divider|StageOut[92]~132_combout\ & !\Mod1|auto_generated|divider|divider|StageOut[92]~180_combout\)) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[92]~132_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[92]~180_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~5\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~6_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~7\);

-- Location: LCCOMB_X30_Y12_N18
\Mod1|auto_generated|divider|divider|add_sub_12_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[7]~11_cout\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[94]~178_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[94]~130_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[94]~178_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[94]~130_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~9\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[7]~11_cout\);

-- Location: LCCOMB_X30_Y12_N20
\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ = \Mod1|auto_generated|divider|divider|add_sub_12_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[7]~11_cout\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\);

-- Location: LCCOMB_X31_Y12_N26
\Mod1|auto_generated|divider|divider|StageOut[91]~181\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[91]~181_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[82]~128_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[82]~128_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~0_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[91]~181_combout\);

-- Location: LCCOMB_X30_Y12_N4
\Mod1|auto_generated|divider|divider|StageOut[100]~186\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[100]~186_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[91]~181_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~2_combout\,
	datac => \Mod1|auto_generated|divider|divider|StageOut[91]~181_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[100]~186_combout\);

-- Location: LCCOMB_X29_Y12_N30
\Mod1|auto_generated|divider|divider|StageOut[109]~191\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[109]~191_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[100]~186_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~4_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[100]~186_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[109]~191_combout\);

-- Location: LCCOMB_X31_Y12_N6
\Mod1|auto_generated|divider|divider|StageOut[93]~179\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[93]~179_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[84]~174_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~4_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[84]~174_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~4_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[93]~179_combout\);

-- Location: LCCOMB_X30_Y12_N24
\Mod1|auto_generated|divider|divider|StageOut[102]~184\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[102]~184_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[93]~179_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~6_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[93]~179_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[102]~184_combout\);

-- Location: LCCOMB_X30_Y12_N30
\Mod1|auto_generated|divider|divider|StageOut[101]~185\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[101]~185_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[92]~180_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~4_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[92]~180_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~4_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[101]~185_combout\);

-- Location: LCCOMB_X29_Y12_N26
\Mod1|auto_generated|divider|divider|StageOut[100]~138\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[100]~138_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~4_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~4_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[100]~138_combout\);

-- Location: LCCOMB_X31_Y12_N24
\Mod1|auto_generated|divider|divider|StageOut[81]~176\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[81]~176_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & ((\toOut[4]~20_combout\) # ((\sec_cnt|count_second\(4) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(4),
	datab => \SW~combout\(9),
	datac => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datad => \toOut[4]~20_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[81]~176_combout\);

-- Location: LCCOMB_X31_Y12_N4
\Mod1|auto_generated|divider|divider|StageOut[90]~134\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[90]~134_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[81]~177_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[81]~176_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|StageOut[81]~177_combout\,
	datac => \Mod1|auto_generated|divider|divider|StageOut[81]~176_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[90]~134_combout\);

-- Location: LCCOMB_X30_Y12_N22
\Mod1|auto_generated|divider|divider|StageOut[99]~187\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[99]~187_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[90]~134_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~0_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[90]~134_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[99]~187_combout\);

-- Location: LCCOMB_X30_Y12_N26
\Mod1|auto_generated|divider|divider|StageOut[89]~183\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[89]~183_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & ((\toOut[3]~21_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	datac => \sec_cnt|count_second\(3),
	datad => \toOut[3]~21_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[89]~183_combout\);

-- Location: LCCOMB_X30_Y12_N0
\Mod1|auto_generated|divider|divider|StageOut[98]~140\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[98]~140_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[89]~182_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[89]~183_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|StageOut[89]~182_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[89]~183_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[98]~140_combout\);

-- Location: LCCOMB_X30_Y9_N26
\Mod1|auto_generated|divider|divider|StageOut[97]~188\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[97]~188_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & ((\toOut[2]~22_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \toOut[2]~22_combout\,
	datac => \sec_cnt|count_second\(2),
	datad => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[97]~188_combout\);

-- Location: LCCOMB_X29_Y12_N0
\Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~0_combout\ = (((\Mod1|auto_generated|divider|divider|StageOut[97]~189_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[97]~188_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~1\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[97]~189_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[97]~188_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[97]~189_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[97]~188_combout\,
	datad => VCC,
	combout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~0_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~1\);

-- Location: LCCOMB_X29_Y12_N2
\Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~2_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~1\ & (((\Mod1|auto_generated|divider|divider|StageOut[98]~141_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[98]~140_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~1\ & (!\Mod1|auto_generated|divider|divider|StageOut[98]~141_combout\ & 
-- (!\Mod1|auto_generated|divider|divider|StageOut[98]~140_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~3\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[98]~141_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[98]~140_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[98]~141_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[98]~140_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~1\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~2_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~3\);

-- Location: LCCOMB_X29_Y12_N4
\Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~4_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~3\ & ((((\Mod1|auto_generated|divider|divider|StageOut[99]~139_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[99]~187_combout\))))) # (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~3\ & ((\Mod1|auto_generated|divider|divider|StageOut[99]~139_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|StageOut[99]~187_combout\) # (GND))))
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~5\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[99]~139_combout\) # ((\Mod1|auto_generated|divider|divider|StageOut[99]~187_combout\) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[99]~139_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[99]~187_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~3\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~4_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~5\);

-- Location: LCCOMB_X29_Y12_N10
\Mod1|auto_generated|divider|divider|add_sub_13_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[7]~11_cout\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[102]~136_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[102]~184_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[102]~136_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[102]~184_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~9\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[7]~11_cout\);

-- Location: LCCOMB_X29_Y12_N12
\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ = \Mod1|auto_generated|divider|divider|add_sub_13_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[7]~11_cout\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\);

-- Location: LCCOMB_X26_Y13_N8
\Mod1|auto_generated|divider|divider|StageOut[108]~144\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[108]~144_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~4_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[108]~144_combout\);

-- Location: LCCOMB_X25_Y13_N10
\Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\ = (\Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\ & (((\Mod1|auto_generated|divider|divider|StageOut[109]~143_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[109]~191_combout\)))) # (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\ & (!\Mod1|auto_generated|divider|divider|StageOut[109]~143_combout\ & 
-- (!\Mod1|auto_generated|divider|divider|StageOut[109]~191_combout\)))
-- \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[109]~143_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[109]~191_combout\ & 
-- !\Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[109]~143_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[109]~191_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\,
	combout => \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\);

-- Location: LCCOMB_X26_Y13_N0
\Div0|auto_generated|divider|divider|StageOut[17]~27\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[17]~27_combout\ = (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[17]~27_combout\);

-- Location: LCCOMB_X29_Y12_N28
\Mod1|auto_generated|divider|divider|StageOut[108]~192\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[108]~192_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[99]~187_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~2_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[99]~187_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[108]~192_combout\);

-- Location: LCCOMB_X26_Y13_N30
\Div0|auto_generated|divider|divider|StageOut[16]~39\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[16]~39_combout\ = (\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[108]~192_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|StageOut[108]~192_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~4_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[16]~39_combout\);

-- Location: LCCOMB_X29_Y12_N22
\Mod1|auto_generated|divider|divider|StageOut[107]~193\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[107]~193_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[98]~140_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~0_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[98]~140_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~0_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[107]~193_combout\);

-- Location: LCCOMB_X26_Y13_N18
\Div0|auto_generated|divider|divider|StageOut[15]~41\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[15]~41_combout\ = (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[107]~193_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~2_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[107]~193_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[15]~41_combout\);

-- Location: LCCOMB_X26_Y13_N22
\Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\ & (((\Div0|auto_generated|divider|divider|StageOut[16]~28_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[16]~39_combout\)))) # (!\Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\ & (!\Div0|auto_generated|divider|divider|StageOut[16]~28_combout\ & 
-- (!\Div0|auto_generated|divider|divider|StageOut[16]~39_combout\)))
-- \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ = CARRY((!\Div0|auto_generated|divider|divider|StageOut[16]~28_combout\ & (!\Div0|auto_generated|divider|divider|StageOut[16]~39_combout\ & 
-- !\Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[16]~28_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[16]~39_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\,
	combout => \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\);

-- Location: LCCOMB_X26_Y13_N24
\Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ & (((\Div0|auto_generated|divider|divider|StageOut[17]~38_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[17]~27_combout\)))) # (!\Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ & ((((\Div0|auto_generated|divider|divider|StageOut[17]~38_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[17]~27_combout\)))))
-- \Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\ = CARRY((!\Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ & ((\Div0|auto_generated|divider|divider|StageOut[17]~38_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[17]~27_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[17]~38_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[17]~27_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\,
	combout => \Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\);

-- Location: LCCOMB_X29_Y12_N16
\Mod1|auto_generated|divider|divider|StageOut[110]~190\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[110]~190_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[101]~185_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~6_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[101]~185_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[110]~190_combout\);

-- Location: LCCOMB_X25_Y13_N12
\Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\ = (\Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ & (((\Mod1|auto_generated|divider|divider|StageOut[110]~142_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[110]~190_combout\)))) # (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ & ((((\Mod1|auto_generated|divider|divider|StageOut[110]~142_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[110]~190_combout\)))))
-- \Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\ = CARRY((!\Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ & ((\Mod1|auto_generated|divider|divider|StageOut[110]~142_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[110]~190_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[110]~142_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[110]~190_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\,
	combout => \Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\);

-- Location: LCCOMB_X26_Y13_N2
\Div0|auto_generated|divider|divider|StageOut[18]~26\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[18]~26_combout\ = (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & \Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[18]~26_combout\);

-- Location: LCCOMB_X26_Y13_N26
\Div0|auto_generated|divider|divider|add_sub_4_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\ = CARRY((!\Div0|auto_generated|divider|divider|StageOut[18]~37_combout\ & (!\Div0|auto_generated|divider|divider|StageOut[18]~26_combout\ & 
-- !\Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[18]~37_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[18]~26_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\,
	cout => \Div0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\);

-- Location: LCCOMB_X26_Y13_N28
\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ = \Div0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\,
	combout => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\);

-- Location: LCCOMB_X27_Y13_N4
\Div0|auto_generated|divider|divider|StageOut[23]~29\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[23]~29_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\ & !\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[23]~29_combout\);

-- Location: LCCOMB_X27_Y13_N30
\Div0|auto_generated|divider|divider|StageOut[22]~30\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[22]~30_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\ & !\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[22]~30_combout\);

-- Location: LCCOMB_X26_Y13_N4
\Div0|auto_generated|divider|divider|StageOut[15]~40\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[15]~40_combout\ = (\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[107]~193_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~2_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[107]~193_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[15]~40_combout\);

-- Location: LCCOMB_X26_Y13_N16
\Div0|auto_generated|divider|divider|StageOut[21]~31\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[21]~31_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\Div0|auto_generated|divider|divider|StageOut[15]~41_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[15]~40_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[15]~41_combout\,
	datad => \Div0|auto_generated|divider|divider|StageOut[15]~40_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[21]~31_combout\);

-- Location: LCCOMB_X30_Y9_N24
\Mod1|auto_generated|divider|divider|StageOut[97]~189\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[97]~189_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & ((\toOut[2]~22_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \toOut[2]~22_combout\,
	datac => \sec_cnt|count_second\(2),
	datad => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[97]~189_combout\);

-- Location: LCCOMB_X29_Y13_N4
\Mod1|auto_generated|divider|divider|StageOut[106]~145\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[106]~145_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[97]~188_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[97]~189_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|StageOut[97]~188_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[97]~189_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[106]~145_combout\);

-- Location: LCCOMB_X27_Y13_N2
\Div0|auto_generated|divider|divider|StageOut[20]~44\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[20]~44_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[106]~145_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~0_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[106]~145_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[20]~44_combout\);

-- Location: LCCOMB_X27_Y13_N20
\Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ & (((\Div0|auto_generated|divider|divider|StageOut[21]~32_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[21]~31_combout\)))) # (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ & (!\Div0|auto_generated|divider|divider|StageOut[21]~32_combout\ & 
-- (!\Div0|auto_generated|divider|divider|StageOut[21]~31_combout\)))
-- \Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ = CARRY((!\Div0|auto_generated|divider|divider|StageOut[21]~32_combout\ & (!\Div0|auto_generated|divider|divider|StageOut[21]~31_combout\ & 
-- !\Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[21]~32_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[21]~31_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\,
	combout => \Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\);

-- Location: LCCOMB_X27_Y13_N24
\Div0|auto_generated|divider|divider|add_sub_5_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\ = CARRY((!\Div0|auto_generated|divider|divider|StageOut[23]~42_combout\ & (!\Div0|auto_generated|divider|divider|StageOut[23]~29_combout\ & 
-- !\Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[23]~42_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[23]~29_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\,
	cout => \Div0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\);

-- Location: LCCOMB_X27_Y13_N26
\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ = \Div0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\,
	combout => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\);

-- Location: LCCOMB_X25_Y13_N14
\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ = !\Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\,
	combout => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\);

-- Location: LCCOMB_X27_Y13_N0
\Div0|auto_generated|divider|divider|StageOut[28]~46\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[28]~46_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\Div0|auto_generated|divider|divider|StageOut[22]~43_combout\) # 
-- ((!\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[22]~43_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[28]~46_combout\);

-- Location: LCCOMB_X27_Y13_N28
\Div0|auto_generated|divider|divider|StageOut[27]~34\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[27]~34_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\ & !\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[27]~34_combout\);

-- Location: LCCOMB_X27_Y13_N16
\Div0|auto_generated|divider|divider|StageOut[20]~45\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[20]~45_combout\ = (!\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[106]~145_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~0_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[106]~145_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[20]~45_combout\);

-- Location: LCCOMB_X27_Y13_N6
\Div0|auto_generated|divider|divider|StageOut[26]~35\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[26]~35_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\Div0|auto_generated|divider|divider|StageOut[20]~44_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[20]~45_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div0|auto_generated|divider|divider|StageOut[20]~44_combout\,
	datac => \Div0|auto_generated|divider|divider|StageOut[20]~45_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[26]~35_combout\);

-- Location: LCCOMB_X29_Y11_N28
\Mod1|auto_generated|divider|divider|StageOut[105]~194\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[105]~194_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & ((\toOut[1]~2_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datab => \toOut[1]~2_combout\,
	datac => \SW~combout\(9),
	datad => \sec_cnt|count_second\(1),
	combout => \Mod1|auto_generated|divider|divider|StageOut[105]~194_combout\);

-- Location: LCCOMB_X29_Y11_N0
\Div0|auto_generated|divider|divider|StageOut[25]~49\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[25]~49_combout\ = (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[105]~194_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_13_result_int[1]~14_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[1]~14_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[105]~194_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[25]~49_combout\);

-- Location: LCCOMB_X29_Y13_N8
\Div0|auto_generated|divider|divider|add_sub_6_result_int[1]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_6_result_int[1]~1_cout\ = CARRY((\Div0|auto_generated|divider|divider|StageOut[25]~48_combout\) # (\Div0|auto_generated|divider|divider|StageOut[25]~49_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[25]~48_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[25]~49_combout\,
	datad => VCC,
	cout => \Div0|auto_generated|divider|divider|add_sub_6_result_int[1]~1_cout\);

-- Location: LCCOMB_X29_Y13_N10
\Div0|auto_generated|divider|divider|add_sub_6_result_int[2]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_6_result_int[2]~3_cout\ = CARRY((!\Div0|auto_generated|divider|divider|StageOut[26]~36_combout\ & (!\Div0|auto_generated|divider|divider|StageOut[26]~35_combout\ & 
-- !\Div0|auto_generated|divider|divider|add_sub_6_result_int[1]~1_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[26]~36_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[26]~35_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_6_result_int[1]~1_cout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_6_result_int[2]~3_cout\);

-- Location: LCCOMB_X29_Y13_N12
\Div0|auto_generated|divider|divider|add_sub_6_result_int[3]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_6_result_int[3]~5_cout\ = CARRY((!\Div0|auto_generated|divider|divider|add_sub_6_result_int[2]~3_cout\ & ((\Div0|auto_generated|divider|divider|StageOut[27]~47_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[27]~34_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[27]~47_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[27]~34_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_6_result_int[2]~3_cout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_6_result_int[3]~5_cout\);

-- Location: LCCOMB_X29_Y13_N14
\Div0|auto_generated|divider|divider|add_sub_6_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\ = CARRY((!\Div0|auto_generated|divider|divider|StageOut[28]~33_combout\ & (!\Div0|auto_generated|divider|divider|StageOut[28]~46_combout\ & 
-- !\Div0|auto_generated|divider|divider|add_sub_6_result_int[3]~5_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[28]~33_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[28]~46_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_6_result_int[3]~5_cout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\);

-- Location: LCCOMB_X29_Y13_N16
\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ = \Div0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\,
	combout => \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\);

-- Location: LCCOMB_X25_Y13_N16
\dec1|WideOr6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr6~0_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & (\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ $ (((\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\) # 
-- (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\))))) # (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & (((!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010011110000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	combout => \dec1|WideOr6~0_combout\);

-- Location: LCCOMB_X25_Y13_N18
\dec1|WideOr5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr5~0_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\))) 
-- # (!\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ $ (\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\)) # 
-- (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001011100100111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	combout => \dec1|WideOr5~0_combout\);

-- Location: LCCOMB_X25_Y13_N24
\dec1|WideOr4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr4~0_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\) # 
-- (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)))) # (!\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & (((!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100011100000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	combout => \dec1|WideOr4~0_combout\);

-- Location: LCCOMB_X25_Y13_N22
\dec1|WideOr3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr3~0_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & (\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ $ (((\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\) # 
-- (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\))))) # (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & (((!\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & 
-- !\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\)) # (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010011110010111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	combout => \dec1|WideOr3~0_combout\);

-- Location: LCCOMB_X25_Y13_N4
\dec1|WideOr2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr2~0_combout\ = ((\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & (!\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\)) # (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ 
-- & ((!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)))) # (!\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010011111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	combout => \dec1|WideOr2~0_combout\);

-- Location: LCCOMB_X25_Y13_N2
\dec1|WideOr1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr1~0_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & (((\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & !\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\)) # 
-- (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\))) # (!\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & (((!\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & 
-- !\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\)) # (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100011111010111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	combout => \dec1|WideOr1~0_combout\);

-- Location: LCCOMB_X25_Y13_N28
\dec1|WideOr0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr0~0_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & (\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ $ ((\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)))) 
-- # (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & (\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & ((\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\) # 
-- (\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100001101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	combout => \dec1|WideOr0~0_combout\);

-- Location: LCCOMB_X37_Y10_N0
\toOut[12]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[12]~7_combout\ = (\toOut[12]~6_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(12)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SW~combout\(9),
	datac => \sec_cnt|count_second\(12),
	datad => \toOut[12]~6_combout\,
	combout => \toOut[12]~7_combout\);

-- Location: LCCOMB_X35_Y11_N16
\toOut[7]~17\ : cycloneii_lcell_comb
-- Equation(s):
-- \toOut[7]~17_combout\ = (\toOut[7]~16_combout\) # ((\sec_cnt|count_second\(7) & \SW~combout\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(7),
	datac => \SW~combout\(9),
	datad => \toOut[7]~16_combout\,
	combout => \toOut[7]~17_combout\);

-- Location: LCCOMB_X36_Y11_N2
\Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\ = (\toOut[8]~15_combout\ & (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\ & VCC)) # (!\toOut[8]~15_combout\ & 
-- (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\))
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\ = CARRY((!\toOut[8]~15_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[8]~15_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\);

-- Location: LCCOMB_X36_Y11_N14
\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ = !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\);

-- Location: LCCOMB_X35_Y11_N30
\Mod2|auto_generated|divider|divider|StageOut[107]~147\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[107]~147_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[12]~6_combout\) # ((\sec_cnt|count_second\(12) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(12),
	datab => \toOut[12]~6_combout\,
	datac => \SW~combout\(9),
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[107]~147_combout\);

-- Location: LCCOMB_X35_Y11_N28
\Mod2|auto_generated|divider|divider|StageOut[106]~148\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[106]~148_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[11]~8_combout\) # ((\sec_cnt|count_second\(11) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(11),
	datab => \toOut[11]~8_combout\,
	datac => \SW~combout\(9),
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[106]~148_combout\);

-- Location: LCCOMB_X35_Y11_N18
\Mod2|auto_generated|divider|divider|StageOut[105]~149\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[105]~149_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[10]~10_combout\) # ((\sec_cnt|count_second\(10) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[10]~10_combout\,
	datab => \sec_cnt|count_second\(10),
	datac => \SW~combout\(9),
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[105]~149_combout\);

-- Location: LCCOMB_X35_Y11_N0
\Mod2|auto_generated|divider|divider|StageOut[104]~150\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[104]~150_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[9]~12_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datac => \sec_cnt|count_second\(9),
	datad => \toOut[9]~12_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[104]~150_combout\);

-- Location: LCCOMB_X34_Y11_N30
\Mod2|auto_generated|divider|divider|StageOut[103]~109\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[103]~109_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[103]~109_combout\);

-- Location: LCCOMB_X35_Y11_N14
\Mod2|auto_generated|divider|divider|StageOut[102]~152\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[102]~152_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[7]~16_combout\) # ((\sec_cnt|count_second\(7) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[7]~16_combout\,
	datab => \sec_cnt|count_second\(7),
	datac => \SW~combout\(9),
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[102]~152_combout\);

-- Location: LCCOMB_X35_Y11_N8
\Mod2|auto_generated|divider|divider|StageOut[101]~153\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[101]~153_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[6]~18_combout\) # ((\sec_cnt|count_second\(6) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(6),
	datab => \toOut[6]~18_combout\,
	datac => \SW~combout\(9),
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[101]~153_combout\);

-- Location: LCCOMB_X34_Y11_N12
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\ & (((\Mod2|auto_generated|divider|divider|StageOut[102]~110_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[102]~152_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\ & (!\Mod2|auto_generated|divider|divider|StageOut[102]~110_combout\ & 
-- (!\Mod2|auto_generated|divider|divider|StageOut[102]~152_combout\)))
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ = CARRY((!\Mod2|auto_generated|divider|divider|StageOut[102]~110_combout\ & (!\Mod2|auto_generated|divider|divider|StageOut[102]~152_combout\ & 
-- !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[102]~110_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[102]~152_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\);

-- Location: LCCOMB_X34_Y11_N14
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ & (((\Mod2|auto_generated|divider|divider|StageOut[103]~151_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[103]~109_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ & ((((\Mod2|auto_generated|divider|divider|StageOut[103]~151_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[103]~109_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ & ((\Mod2|auto_generated|divider|divider|StageOut[103]~151_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[103]~109_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[103]~151_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[103]~109_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\);

-- Location: LCCOMB_X34_Y11_N22
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ & (((\Mod2|auto_generated|divider|divider|StageOut[107]~105_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[107]~147_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ & ((((\Mod2|auto_generated|divider|divider|StageOut[107]~105_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[107]~147_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ & ((\Mod2|auto_generated|divider|divider|StageOut[107]~105_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[107]~147_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[107]~105_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[107]~147_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\);

-- Location: LCCOMB_X35_Y11_N12
\Mod2|auto_generated|divider|divider|StageOut[108]~146\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[108]~146_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[13]~4_combout\) # ((\sec_cnt|count_second\(13) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(13),
	datab => \toOut[13]~4_combout\,
	datac => \SW~combout\(9),
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[108]~146_combout\);

-- Location: LCCOMB_X34_Y11_N24
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[10]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\ = CARRY((!\Mod2|auto_generated|divider|divider|StageOut[108]~104_combout\ & (!\Mod2|auto_generated|divider|divider|StageOut[108]~146_combout\ & 
-- !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[108]~104_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[108]~146_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\);

-- Location: LCCOMB_X34_Y11_N26
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ = \Mod2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\);

-- Location: LCCOMB_X35_Y13_N4
\Mod2|auto_generated|divider|divider|StageOut[119]~111\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[119]~111_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[119]~111_combout\);

-- Location: LCCOMB_X34_Y11_N0
\Mod2|auto_generated|divider|divider|StageOut[118]~156\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[118]~156_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[106]~148_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[106]~148_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[118]~156_combout\);

-- Location: LCCOMB_X35_Y13_N0
\Mod2|auto_generated|divider|divider|StageOut[117]~113\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[117]~113_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[117]~113_combout\);

-- Location: LCCOMB_X34_Y13_N4
\Mod2|auto_generated|divider|divider|StageOut[116]~114\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[116]~114_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[116]~114_combout\);

-- Location: LCCOMB_X34_Y13_N22
\Mod2|auto_generated|divider|divider|StageOut[115]~115\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[115]~115_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[115]~115_combout\);

-- Location: LCCOMB_X35_Y13_N2
\Mod2|auto_generated|divider|divider|StageOut[114]~116\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[114]~116_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[114]~116_combout\);

-- Location: LCCOMB_X35_Y11_N2
\Mod2|auto_generated|divider|divider|StageOut[113]~161\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[113]~161_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\toOut[6]~18_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \toOut[6]~18_combout\,
	datac => \SW~combout\(9),
	datad => \sec_cnt|count_second\(6),
	combout => \Mod2|auto_generated|divider|divider|StageOut[113]~161_combout\);

-- Location: LCCOMB_X33_Y11_N16
\Mod2|auto_generated|divider|divider|StageOut[112]~162\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[112]~162_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\toOut[5]~19_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \SW~combout\(9),
	datac => \sec_cnt|count_second\(5),
	datad => \toOut[5]~19_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[112]~162_combout\);

-- Location: LCCOMB_X35_Y13_N16
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\ & (((\Mod2|auto_generated|divider|divider|StageOut[113]~117_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[113]~161_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\ & (!\Mod2|auto_generated|divider|divider|StageOut[113]~117_combout\ & 
-- (!\Mod2|auto_generated|divider|divider|StageOut[113]~161_combout\)))
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ = CARRY((!\Mod2|auto_generated|divider|divider|StageOut[113]~117_combout\ & (!\Mod2|auto_generated|divider|divider|StageOut[113]~161_combout\ & 
-- !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[113]~117_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[113]~161_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\);

-- Location: LCCOMB_X35_Y13_N22
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ & (((\Mod2|auto_generated|divider|divider|StageOut[116]~158_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[116]~114_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ & ((((\Mod2|auto_generated|divider|divider|StageOut[116]~158_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[116]~114_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ & ((\Mod2|auto_generated|divider|divider|StageOut[116]~158_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[116]~114_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[116]~158_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[116]~114_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\);

-- Location: LCCOMB_X35_Y13_N24
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[117]~157_combout\ & (((!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\)))) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[117]~157_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[117]~113_combout\ & (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\)) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[117]~113_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\) # (GND)))))
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ = CARRY(((!\Mod2|auto_generated|divider|divider|StageOut[117]~157_combout\ & !\Mod2|auto_generated|divider|divider|StageOut[117]~113_combout\)) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[117]~157_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[117]~113_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\);

-- Location: LCCOMB_X35_Y13_N26
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ & (((\Mod2|auto_generated|divider|divider|StageOut[118]~112_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[118]~156_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ & ((((\Mod2|auto_generated|divider|divider|StageOut[118]~112_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[118]~156_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ & ((\Mod2|auto_generated|divider|divider|StageOut[118]~112_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[118]~156_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[118]~112_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[118]~156_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\);

-- Location: LCCOMB_X35_Y13_N28
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[10]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\ = CARRY((!\Mod2|auto_generated|divider|divider|StageOut[119]~155_combout\ & (!\Mod2|auto_generated|divider|divider|StageOut[119]~111_combout\ & 
-- !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[119]~155_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[119]~111_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\);

-- Location: LCCOMB_X35_Y13_N30
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ = \Mod2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\);

-- Location: LCCOMB_X34_Y13_N10
\Mod2|auto_generated|divider|divider|StageOut[129]~120\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[129]~120_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[129]~120_combout\);

-- Location: LCCOMB_X34_Y13_N28
\Mod2|auto_generated|divider|divider|StageOut[128]~121\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[128]~121_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[128]~121_combout\);

-- Location: LCCOMB_X34_Y11_N2
\Mod2|auto_generated|divider|divider|StageOut[115]~159\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[115]~159_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[103]~151_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[103]~151_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[115]~159_combout\);

-- Location: LCCOMB_X34_Y13_N8
\Mod2|auto_generated|divider|divider|StageOut[127]~168\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[127]~168_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[115]~159_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[115]~159_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[127]~168_combout\);

-- Location: LCCOMB_X33_Y13_N0
\Mod2|auto_generated|divider|divider|StageOut[126]~123\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[126]~123_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[126]~123_combout\);

-- Location: LCCOMB_X34_Y13_N14
\Mod2|auto_generated|divider|divider|StageOut[125]~170\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[125]~170_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[113]~161_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[113]~161_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[125]~170_combout\);

-- Location: LCCOMB_X33_Y11_N20
\Mod2|auto_generated|divider|divider|StageOut[100]~164\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[100]~164_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[5]~19_combout\) # ((\sec_cnt|count_second\(5) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[5]~19_combout\,
	datab => \sec_cnt|count_second\(5),
	datac => \SW~combout\(9),
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[100]~164_combout\);

-- Location: LCCOMB_X33_Y11_N6
\Mod2|auto_generated|divider|divider|StageOut[100]~163\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[100]~163_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[5]~19_combout\) # ((\sec_cnt|count_second\(5) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[5]~19_combout\,
	datab => \sec_cnt|count_second\(5),
	datac => \SW~combout\(9),
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[100]~163_combout\);

-- Location: LCCOMB_X33_Y11_N24
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[2]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[100]~164_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[100]~163_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|StageOut[100]~164_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[100]~163_combout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\);

-- Location: LCCOMB_X34_Y13_N24
\Mod2|auto_generated|divider|divider|StageOut[124]~171\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[124]~171_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[112]~162_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[112]~162_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[124]~171_combout\);

-- Location: LCCOMB_X33_Y11_N2
\Mod2|auto_generated|divider|divider|StageOut[111]~172\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[111]~172_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\toOut[4]~20_combout\) # ((\sec_cnt|count_second\(4) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(4),
	datab => \SW~combout\(9),
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \toOut[4]~20_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[111]~172_combout\);

-- Location: LCCOMB_X33_Y11_N30
\Mod2|auto_generated|divider|divider|StageOut[99]~174\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[99]~174_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[4]~20_combout\) # ((\sec_cnt|count_second\(4) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(4),
	datab => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datac => \SW~combout\(9),
	datad => \toOut[4]~20_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[99]~174_combout\);

-- Location: LCCOMB_X33_Y11_N28
\Mod2|auto_generated|divider|divider|StageOut[99]~173\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[99]~173_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[4]~20_combout\) # ((\sec_cnt|count_second\(4) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(4),
	datab => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datac => \SW~combout\(9),
	datad => \toOut[4]~20_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[99]~173_combout\);

-- Location: LCCOMB_X33_Y11_N26
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[1]~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[99]~174_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[99]~173_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|StageOut[99]~174_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[99]~173_combout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\);

-- Location: LCCOMB_X33_Y11_N4
\Mod2|auto_generated|divider|divider|StageOut[123]~175\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[123]~175_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[111]~172_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[111]~172_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[123]~175_combout\);

-- Location: LCCOMB_X33_Y13_N14
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\ = (((\Mod2|auto_generated|divider|divider|StageOut[123]~127_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[123]~175_combout\)))
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\ = CARRY((\Mod2|auto_generated|divider|divider|StageOut[123]~127_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[123]~175_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[123]~127_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[123]~175_combout\,
	datad => VCC,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\);

-- Location: LCCOMB_X33_Y13_N20
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[126]~169_combout\ & (((!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\)))) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[126]~169_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[126]~123_combout\ & (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\)) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[126]~123_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\) # (GND)))))
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ = CARRY(((!\Mod2|auto_generated|divider|divider|StageOut[126]~169_combout\ & !\Mod2|auto_generated|divider|divider|StageOut[126]~123_combout\)) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[126]~169_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[126]~123_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\);

-- Location: LCCOMB_X33_Y13_N22
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ & (((\Mod2|auto_generated|divider|divider|StageOut[127]~122_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[127]~168_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ & ((((\Mod2|auto_generated|divider|divider|StageOut[127]~122_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[127]~168_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ & ((\Mod2|auto_generated|divider|divider|StageOut[127]~122_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[127]~168_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[127]~122_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[127]~168_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\);

-- Location: LCCOMB_X33_Y13_N24
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[128]~167_combout\ & (((!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\)))) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[128]~167_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[128]~121_combout\ & (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\)) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[128]~121_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\) # (GND)))))
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ = CARRY(((!\Mod2|auto_generated|divider|divider|StageOut[128]~167_combout\ & !\Mod2|auto_generated|divider|divider|StageOut[128]~121_combout\)) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[128]~167_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[128]~121_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\);

-- Location: LCCOMB_X33_Y13_N26
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ & (((\Mod2|auto_generated|divider|divider|StageOut[129]~166_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[129]~120_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ & ((((\Mod2|auto_generated|divider|divider|StageOut[129]~166_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[129]~120_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ & ((\Mod2|auto_generated|divider|divider|StageOut[129]~166_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[129]~120_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[129]~166_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[129]~120_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\);

-- Location: LCCOMB_X34_Y13_N0
\Mod2|auto_generated|divider|divider|StageOut[130]~119\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[130]~119_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[130]~119_combout\);

-- Location: LCCOMB_X33_Y13_N28
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[10]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\ = CARRY((!\Mod2|auto_generated|divider|divider|StageOut[130]~165_combout\ & (!\Mod2|auto_generated|divider|divider|StageOut[130]~119_combout\ & 
-- !\Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[130]~165_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[130]~119_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\);

-- Location: LCCOMB_X33_Y13_N30
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ = \Mod2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\);

-- Location: LCCOMB_X33_Y13_N8
\Mod2|auto_generated|divider|divider|StageOut[141]~128\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[141]~128_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[141]~128_combout\);

-- Location: LCCOMB_X30_Y13_N12
\Mod2|auto_generated|divider|divider|StageOut[140]~129\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[140]~129_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[140]~129_combout\);

-- Location: LCCOMB_X34_Y13_N20
\Mod2|auto_generated|divider|divider|StageOut[139]~178\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[139]~178_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[127]~168_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[127]~168_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[139]~178_combout\);

-- Location: LCCOMB_X35_Y13_N8
\Mod2|auto_generated|divider|divider|StageOut[126]~169\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[126]~169_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[114]~160_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[114]~160_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[126]~169_combout\);

-- Location: LCCOMB_X33_Y13_N10
\Mod2|auto_generated|divider|divider|StageOut[138]~179\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[138]~179_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[126]~169_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[126]~169_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[138]~179_combout\);

-- Location: LCCOMB_X34_Y13_N2
\Mod2|auto_generated|divider|divider|StageOut[137]~180\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[137]~180_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[125]~170_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[125]~170_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[137]~180_combout\);

-- Location: LCCOMB_X33_Y13_N12
\Mod2|auto_generated|divider|divider|StageOut[136]~181\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[136]~181_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[124]~171_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[124]~171_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[136]~181_combout\);

-- Location: LCCOMB_X32_Y13_N0
\Mod2|auto_generated|divider|divider|StageOut[135]~134\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[135]~134_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[135]~134_combout\);

-- Location: LCCOMB_X33_Y14_N2
\Mod2|auto_generated|divider|divider|StageOut[122]~183\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[122]~183_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\toOut[3]~21_combout\) # ((\sec_cnt|count_second\(3) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(3),
	datab => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datac => \SW~combout\(9),
	datad => \toOut[3]~21_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[122]~183_combout\);

-- Location: LCCOMB_X33_Y14_N28
\Mod2|auto_generated|divider|divider|StageOut[110]~184\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[110]~184_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\toOut[3]~21_combout\) # ((\sec_cnt|count_second\(3) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(3),
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \SW~combout\(9),
	datad => \toOut[3]~21_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[110]~184_combout\);

-- Location: LCCOMB_X33_Y14_N18
\Mod2|auto_generated|divider|divider|StageOut[110]~185\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[110]~185_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\toOut[3]~21_combout\) # ((\sec_cnt|count_second\(3) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(3),
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \SW~combout\(9),
	datad => \toOut[3]~21_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[110]~185_combout\);

-- Location: LCCOMB_X33_Y14_N4
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[1]~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[110]~184_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[110]~185_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|StageOut[110]~184_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[110]~185_combout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\);

-- Location: LCCOMB_X33_Y14_N12
\Mod2|auto_generated|divider|divider|StageOut[134]~186\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[134]~186_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[122]~183_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[122]~183_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[134]~186_combout\);

-- Location: LCCOMB_X32_Y13_N6
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~0_combout\ = (((\Mod2|auto_generated|divider|divider|StageOut[134]~136_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[134]~186_combout\)))
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~1\ = CARRY((\Mod2|auto_generated|divider|divider|StageOut[134]~136_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[134]~186_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[134]~136_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[134]~186_combout\,
	datad => VCC,
	combout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~0_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~1\);

-- Location: LCCOMB_X32_Y13_N8
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~2_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~1\ & (((\Mod2|auto_generated|divider|divider|StageOut[135]~182_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[135]~134_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~1\ & (!\Mod2|auto_generated|divider|divider|StageOut[135]~182_combout\ & 
-- (!\Mod2|auto_generated|divider|divider|StageOut[135]~134_combout\)))
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~3\ = CARRY((!\Mod2|auto_generated|divider|divider|StageOut[135]~182_combout\ & (!\Mod2|auto_generated|divider|divider|StageOut[135]~134_combout\ & 
-- !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[135]~182_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[135]~134_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~1\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~2_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~3\);

-- Location: LCCOMB_X32_Y13_N10
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~4_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~3\ & (((\Mod2|auto_generated|divider|divider|StageOut[136]~133_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[136]~181_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~3\ & ((((\Mod2|auto_generated|divider|divider|StageOut[136]~133_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[136]~181_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~5\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~3\ & ((\Mod2|auto_generated|divider|divider|StageOut[136]~133_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[136]~181_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[136]~133_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[136]~181_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~3\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~4_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~5\);

-- Location: LCCOMB_X32_Y13_N14
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~8_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~7\ & (((\Mod2|auto_generated|divider|divider|StageOut[138]~131_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[138]~179_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~7\ & ((((\Mod2|auto_generated|divider|divider|StageOut[138]~131_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[138]~179_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~9\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~7\ & ((\Mod2|auto_generated|divider|divider|StageOut[138]~131_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[138]~179_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[138]~131_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[138]~179_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~7\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~8_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~9\);

-- Location: LCCOMB_X32_Y13_N16
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~10_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[139]~130_combout\ & (((!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~9\)))) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[139]~130_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[139]~178_combout\ & (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~9\)) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[139]~178_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~9\) # (GND)))))
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~11\ = CARRY(((!\Mod2|auto_generated|divider|divider|StageOut[139]~130_combout\ & !\Mod2|auto_generated|divider|divider|StageOut[139]~178_combout\)) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[139]~130_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[139]~178_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~9\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~10_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~11\);

-- Location: LCCOMB_X32_Y13_N18
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~12_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~11\ & (((\Mod2|auto_generated|divider|divider|StageOut[140]~177_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[140]~129_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~11\ & ((((\Mod2|auto_generated|divider|divider|StageOut[140]~177_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[140]~129_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~13\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~11\ & ((\Mod2|auto_generated|divider|divider|StageOut[140]~177_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[140]~129_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[140]~177_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[140]~129_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~11\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~12_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~13\);

-- Location: LCCOMB_X32_Y13_N20
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[10]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\ = CARRY((!\Mod2|auto_generated|divider|divider|StageOut[141]~176_combout\ & (!\Mod2|auto_generated|divider|divider|StageOut[141]~128_combout\ & 
-- !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~13\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[141]~176_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[141]~128_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~13\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\);

-- Location: LCCOMB_X32_Y13_N22
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ = \Mod2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\);

-- Location: LCCOMB_X30_Y13_N4
\Mod2|auto_generated|divider|divider|StageOut[152]~137\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[152]~137_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~12_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[152]~137_combout\);

-- Location: LCCOMB_X32_Y13_N28
\Mod2|auto_generated|divider|divider|StageOut[151]~188\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[151]~188_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[139]~178_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[139]~178_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[151]~188_combout\);

-- Location: LCCOMB_X32_Y13_N30
\Mod2|auto_generated|divider|divider|StageOut[150]~189\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[150]~189_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[138]~179_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|StageOut[138]~179_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[150]~189_combout\);

-- Location: LCCOMB_X30_Y13_N16
\Mod2|auto_generated|divider|divider|StageOut[149]~190\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[149]~190_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[137]~180_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[137]~180_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[149]~190_combout\);

-- Location: LCCOMB_X31_Y13_N14
\Mod2|auto_generated|divider|divider|StageOut[148]~141\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[148]~141_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~4_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[148]~141_combout\);

-- Location: LCCOMB_X31_Y13_N18
\Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\ = (((\Mod2|auto_generated|divider|divider|StageOut[148]~191_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[148]~141_combout\)))
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\ = CARRY((\Mod2|auto_generated|divider|divider|StageOut[148]~191_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[148]~141_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[148]~191_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[148]~141_combout\,
	datad => VCC,
	combout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\);

-- Location: LCCOMB_X31_Y13_N20
\Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\ & (((\Mod2|auto_generated|divider|divider|StageOut[149]~140_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[149]~190_combout\)))) # (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\ & (!\Mod2|auto_generated|divider|divider|StageOut[149]~140_combout\ & 
-- (!\Mod2|auto_generated|divider|divider|StageOut[149]~190_combout\)))
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\ = CARRY((!\Mod2|auto_generated|divider|divider|StageOut[149]~140_combout\ & (!\Mod2|auto_generated|divider|divider|StageOut[149]~190_combout\ & 
-- !\Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[149]~140_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[149]~190_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\,
	combout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\);

-- Location: LCCOMB_X31_Y13_N22
\Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\ & ((((\Mod2|auto_generated|divider|divider|StageOut[150]~139_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[150]~189_combout\))))) # (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\ & ((\Mod2|auto_generated|divider|divider|StageOut[150]~139_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|StageOut[150]~189_combout\) # (GND))))
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\ = CARRY((\Mod2|auto_generated|divider|divider|StageOut[150]~139_combout\) # ((\Mod2|auto_generated|divider|divider|StageOut[150]~189_combout\) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[150]~139_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[150]~189_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\,
	combout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\);

-- Location: LCCOMB_X31_Y13_N26
\Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ & (((\Mod2|auto_generated|divider|divider|StageOut[152]~187_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[152]~137_combout\)))) # (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ & ((((\Mod2|auto_generated|divider|divider|StageOut[152]~187_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[152]~137_combout\)))))
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\ = CARRY((!\Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ & ((\Mod2|auto_generated|divider|divider|StageOut[152]~187_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[152]~137_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[152]~187_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[152]~137_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\,
	combout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\);

-- Location: LCCOMB_X31_Y13_N28
\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ = !\Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\,
	combout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\);

-- Location: LCCOMB_X31_Y13_N4
\Div1|auto_generated|divider|divider|StageOut[53]~61\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[53]~61_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[151]~188_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~10_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[151]~188_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[53]~61_combout\);

-- Location: LCCOMB_X31_Y13_N2
\Div1|auto_generated|divider|divider|StageOut[52]~62\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[52]~62_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[150]~189_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~8_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[150]~189_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[52]~62_combout\);

-- Location: LCCOMB_X30_Y14_N4
\Div1|auto_generated|divider|divider|StageOut[51]~43\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[51]~43_combout\ = (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[51]~43_combout\);

-- Location: LCCOMB_X31_Y14_N0
\Div1|auto_generated|divider|divider|StageOut[50]~44\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[50]~44_combout\ = (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[50]~44_combout\);

-- Location: LCCOMB_X33_Y11_N12
\Mod2|auto_generated|divider|divider|StageOut[111]~126\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[111]~126_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[111]~126_combout\);

-- Location: LCCOMB_X33_Y11_N8
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[2]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[111]~172_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[111]~126_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|StageOut[111]~172_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[111]~126_combout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\);

-- Location: LCCOMB_X33_Y11_N22
\Mod2|auto_generated|divider|divider|StageOut[135]~182\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[135]~182_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[123]~175_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[123]~175_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[135]~182_combout\);

-- Location: LCCOMB_X32_Y13_N2
\Mod2|auto_generated|divider|divider|StageOut[147]~192\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[147]~192_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[135]~182_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[135]~182_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[147]~192_combout\);

-- Location: LCCOMB_X31_Y14_N4
\Div1|auto_generated|divider|divider|StageOut[49]~65\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[49]~65_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[147]~192_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~2_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[147]~192_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[49]~65_combout\);

-- Location: LCCOMB_X31_Y14_N16
\Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ & (((\Div1|auto_generated|divider|divider|StageOut[53]~41_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[53]~61_combout\)))) # (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ & ((((\Div1|auto_generated|divider|divider|StageOut[53]~41_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[53]~61_combout\)))))
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\ = CARRY((!\Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ & ((\Div1|auto_generated|divider|divider|StageOut[53]~41_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[53]~61_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[53]~41_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[53]~61_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\,
	combout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\);

-- Location: LCCOMB_X31_Y13_N0
\Div1|auto_generated|divider|divider|StageOut[54]~40\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[54]~40_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\ & !\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[54]~40_combout\);

-- Location: LCCOMB_X31_Y14_N18
\Div1|auto_generated|divider|divider|add_sub_7_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\ = CARRY((!\Div1|auto_generated|divider|divider|StageOut[54]~60_combout\ & (!\Div1|auto_generated|divider|divider|StageOut[54]~40_combout\ & 
-- !\Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[54]~60_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[54]~40_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\,
	cout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\);

-- Location: LCCOMB_X31_Y14_N20
\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ = \Div1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\,
	combout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\);

-- Location: LCCOMB_X30_Y14_N22
\Div1|auto_generated|divider|divider|StageOut[62]~46\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[62]~46_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[62]~46_combout\);

-- Location: LCCOMB_X31_Y14_N28
\Div1|auto_generated|divider|divider|StageOut[61]~67\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[61]~67_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[52]~62_combout\) # 
-- ((!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[52]~62_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[61]~67_combout\);

-- Location: LCCOMB_X30_Y14_N28
\Div1|auto_generated|divider|divider|StageOut[60]~68\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[60]~68_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[51]~63_combout\) # 
-- ((\Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\ & !\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[51]~63_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[60]~68_combout\);

-- Location: LCCOMB_X31_Y14_N30
\Div1|auto_generated|divider|divider|StageOut[59]~69\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[59]~69_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[50]~64_combout\) # 
-- ((\Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\ & !\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[50]~64_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[59]~69_combout\);

-- Location: LCCOMB_X31_Y14_N2
\Mod2|auto_generated|divider|divider|StageOut[147]~142\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[147]~142_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~2_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~2_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[147]~142_combout\);

-- Location: LCCOMB_X31_Y14_N24
\Div1|auto_generated|divider|divider|add_sub_6_result_int[1]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[1]~12_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[147]~142_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[147]~192_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|StageOut[147]~142_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[147]~192_combout\,
	combout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[1]~12_combout\);

-- Location: LCCOMB_X30_Y14_N2
\Div1|auto_generated|divider|divider|StageOut[58]~70\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[58]~70_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[49]~65_combout\) # 
-- ((!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \Div1|auto_generated|divider|divider|add_sub_6_result_int[1]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[49]~65_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[1]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[58]~70_combout\);

-- Location: LCCOMB_X29_Y16_N14
\Div1|auto_generated|divider|divider|StageOut[48]~71\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[48]~71_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[146]~193_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~0_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[146]~193_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~0_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[48]~71_combout\);

-- Location: LCCOMB_X29_Y16_N24
\Mod2|auto_generated|divider|divider|StageOut[146]~143\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[146]~143_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~0_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~0_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[146]~143_combout\);

-- Location: LCCOMB_X33_Y14_N0
\Mod2|auto_generated|divider|divider|StageOut[122]~135\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[122]~135_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[122]~135_combout\);

-- Location: LCCOMB_X33_Y14_N30
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[2]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[122]~135_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[122]~183_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|StageOut[122]~135_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[122]~183_combout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\);

-- Location: LCCOMB_X32_Y14_N2
\Mod2|auto_generated|divider|divider|StageOut[146]~193\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[146]~193_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[134]~186_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|StageOut[134]~186_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[146]~193_combout\);

-- Location: LCCOMB_X29_Y16_N20
\Div1|auto_generated|divider|divider|add_sub_6_result_int[0]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[0]~14_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[146]~143_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[146]~193_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|StageOut[146]~143_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[146]~193_combout\,
	combout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[0]~14_combout\);

-- Location: LCCOMB_X29_Y16_N16
\Div1|auto_generated|divider|divider|StageOut[57]~72\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[57]~72_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[48]~71_combout\) # 
-- ((!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \Div1|auto_generated|divider|divider|add_sub_6_result_int[0]~14_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[48]~71_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[0]~14_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[57]~72_combout\);

-- Location: LCCOMB_X30_Y14_N10
\Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ & ((((\Div1|auto_generated|divider|divider|StageOut[59]~49_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[59]~69_combout\))))) # (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ & ((\Div1|auto_generated|divider|divider|StageOut[59]~49_combout\) # 
-- ((\Div1|auto_generated|divider|divider|StageOut[59]~69_combout\) # (GND))))
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\ = CARRY((\Div1|auto_generated|divider|divider|StageOut[59]~49_combout\) # ((\Div1|auto_generated|divider|divider|StageOut[59]~69_combout\) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[59]~49_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[59]~69_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\,
	combout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\);

-- Location: LCCOMB_X30_Y14_N12
\Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\ = (\Div1|auto_generated|divider|divider|StageOut[60]~48_combout\ & (((!\Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\)))) # 
-- (!\Div1|auto_generated|divider|divider|StageOut[60]~48_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[60]~68_combout\ & (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\)) # 
-- (!\Div1|auto_generated|divider|divider|StageOut[60]~68_combout\ & ((\Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\) # (GND)))))
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ = CARRY(((!\Div1|auto_generated|divider|divider|StageOut[60]~48_combout\ & !\Div1|auto_generated|divider|divider|StageOut[60]~68_combout\)) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[60]~48_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[60]~68_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\,
	combout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\);

-- Location: LCCOMB_X30_Y14_N14
\Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ & (((\Div1|auto_generated|divider|divider|StageOut[61]~47_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[61]~67_combout\)))) # (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ & ((((\Div1|auto_generated|divider|divider|StageOut[61]~47_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[61]~67_combout\)))))
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\ = CARRY((!\Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ & ((\Div1|auto_generated|divider|divider|StageOut[61]~47_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[61]~67_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[61]~47_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[61]~67_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\,
	combout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\);

-- Location: LCCOMB_X30_Y14_N16
\Div1|auto_generated|divider|divider|add_sub_8_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\ = CARRY((!\Div1|auto_generated|divider|divider|StageOut[62]~66_combout\ & (!\Div1|auto_generated|divider|divider|StageOut[62]~46_combout\ & 
-- !\Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[62]~66_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[62]~46_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\,
	cout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\);

-- Location: LCCOMB_X30_Y14_N18
\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ = \Div1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\,
	combout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\);

-- Location: LCCOMB_X29_Y14_N30
\Div1|auto_generated|divider|divider|StageOut[70]~53\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[70]~53_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\ & !\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[70]~53_combout\);

-- Location: LCCOMB_X29_Y14_N28
\Div1|auto_generated|divider|divider|StageOut[69]~54\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[69]~54_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\ & !\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[69]~54_combout\);

-- Location: LCCOMB_X29_Y14_N26
\Div1|auto_generated|divider|divider|StageOut[68]~55\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[68]~55_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\ & !\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[68]~55_combout\);

-- Location: LCCOMB_X29_Y14_N22
\Div1|auto_generated|divider|divider|StageOut[67]~76\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[67]~76_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[58]~70_combout\) # 
-- ((\Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|StageOut[58]~70_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[67]~76_combout\);

-- Location: LCCOMB_X29_Y14_N18
\Div1|auto_generated|divider|divider|StageOut[66]~57\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[66]~57_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\ & !\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[66]~57_combout\);

-- Location: LCCOMB_X32_Y14_N20
\Mod2|auto_generated|divider|divider|StageOut[121]~196\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[121]~196_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\toOut[2]~22_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[2]~22_combout\,
	datab => \SW~combout\(9),
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(2),
	combout => \Mod2|auto_generated|divider|divider|StageOut[121]~196_combout\);

-- Location: LCCOMB_X32_Y14_N30
\Mod2|auto_generated|divider|divider|StageOut[121]~195\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[121]~195_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\toOut[2]~22_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[2]~22_combout\,
	datab => \SW~combout\(9),
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(2),
	combout => \Mod2|auto_generated|divider|divider|StageOut[121]~195_combout\);

-- Location: LCCOMB_X32_Y14_N4
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[1]~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[1]~20_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[121]~196_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[121]~195_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|StageOut[121]~196_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[121]~195_combout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[1]~20_combout\);

-- Location: LCCOMB_X32_Y14_N16
\Mod2|auto_generated|divider|divider|StageOut[133]~194\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[133]~194_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\toOut[2]~22_combout\) # ((\sec_cnt|count_second\(2) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[2]~22_combout\,
	datab => \sec_cnt|count_second\(2),
	datac => \SW~combout\(9),
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[133]~194_combout\);

-- Location: LCCOMB_X32_Y14_N14
\Mod2|auto_generated|divider|divider|StageOut[145]~197\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[145]~197_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[133]~194_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_12_result_int[1]~20_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[1]~20_combout\,
	datac => \Mod2|auto_generated|divider|divider|StageOut[133]~194_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[145]~197_combout\);

-- Location: LCCOMB_X32_Y14_N18
\Mod2|auto_generated|divider|divider|StageOut[133]~144\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[133]~144_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[1]~20_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[1]~20_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[133]~144_combout\);

-- Location: LCCOMB_X32_Y14_N22
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[2]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[2]~18_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[133]~194_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[133]~144_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|StageOut[133]~194_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[133]~144_combout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[2]~18_combout\);

-- Location: LCCOMB_X32_Y14_N24
\Div1|auto_generated|divider|divider|StageOut[56]~78\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[56]~78_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[145]~197_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_13_result_int[2]~18_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[145]~197_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[2]~18_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[56]~78_combout\);

-- Location: LCCOMB_X32_Y14_N12
\Mod2|auto_generated|divider|divider|StageOut[145]~145\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[145]~145_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_13_result_int[2]~18_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[2]~18_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[145]~145_combout\);

-- Location: LCCOMB_X32_Y14_N0
\Div1|auto_generated|divider|divider|add_sub_7_result_int[0]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[0]~16_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[145]~197_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[145]~145_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|StageOut[145]~197_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[145]~145_combout\,
	combout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[0]~16_combout\);

-- Location: LCCOMB_X32_Y14_N26
\Div1|auto_generated|divider|divider|StageOut[65]~79\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[65]~79_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[56]~78_combout\) # 
-- ((!\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & \Div1|auto_generated|divider|divider|add_sub_7_result_int[0]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[56]~78_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[0]~16_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[65]~79_combout\);

-- Location: LCCOMB_X29_Y14_N2
\Div1|auto_generated|divider|divider|add_sub_9_result_int[2]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_9_result_int[2]~1_cout\ = CARRY((\Div1|auto_generated|divider|divider|StageOut[65]~59_combout\) # (\Div1|auto_generated|divider|divider|StageOut[65]~79_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[65]~59_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[65]~79_combout\,
	datad => VCC,
	cout => \Div1|auto_generated|divider|divider|add_sub_9_result_int[2]~1_cout\);

-- Location: LCCOMB_X29_Y14_N4
\Div1|auto_generated|divider|divider|add_sub_9_result_int[3]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_9_result_int[3]~3_cout\ = CARRY((!\Div1|auto_generated|divider|divider|StageOut[66]~77_combout\ & (!\Div1|auto_generated|divider|divider|StageOut[66]~57_combout\ & 
-- !\Div1|auto_generated|divider|divider|add_sub_9_result_int[2]~1_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[66]~77_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[66]~57_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_9_result_int[2]~1_cout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_9_result_int[3]~3_cout\);

-- Location: LCCOMB_X29_Y14_N6
\Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~5_cout\ = CARRY((\Div1|auto_generated|divider|divider|StageOut[67]~56_combout\) # ((\Div1|auto_generated|divider|divider|StageOut[67]~76_combout\) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_9_result_int[3]~3_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[67]~56_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[67]~76_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_9_result_int[3]~3_cout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~5_cout\);

-- Location: LCCOMB_X29_Y14_N8
\Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~7_cout\ = CARRY(((!\Div1|auto_generated|divider|divider|StageOut[68]~75_combout\ & !\Div1|auto_generated|divider|divider|StageOut[68]~55_combout\)) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~5_cout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[68]~75_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[68]~55_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~5_cout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~7_cout\);

-- Location: LCCOMB_X29_Y14_N10
\Div1|auto_generated|divider|divider|add_sub_9_result_int[6]~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_9_result_int[6]~9_cout\ = CARRY((!\Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~7_cout\ & ((\Div1|auto_generated|divider|divider|StageOut[69]~74_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[69]~54_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[69]~74_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[69]~54_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~7_cout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_9_result_int[6]~9_cout\);

-- Location: LCCOMB_X29_Y14_N12
\Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\ = CARRY((!\Div1|auto_generated|divider|divider|StageOut[70]~73_combout\ & (!\Div1|auto_generated|divider|divider|StageOut[70]~53_combout\ & 
-- !\Div1|auto_generated|divider|divider|add_sub_9_result_int[6]~9_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[70]~73_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[70]~53_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_9_result_int[6]~9_cout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\);

-- Location: LCCOMB_X29_Y14_N14
\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ = \Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\,
	combout => \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\);

-- Location: LCCOMB_X29_Y16_N28
\dec2|WideOr6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr6~0_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ $ (((\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\) 
-- # (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\))))) # (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001100111010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \dec2|WideOr6~0_combout\);

-- Location: LCCOMB_X29_Y16_N30
\dec2|WideOr5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr5~0_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & (\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ $ 
-- (\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\)))) # (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & (((!\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000101111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \dec2|WideOr5~0_combout\);

-- Location: LCCOMB_X29_Y16_N8
\dec2|WideOr4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr4~0_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\) 
-- # (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)))) # (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \dec2|WideOr4~0_combout\);

-- Location: LCCOMB_X29_Y16_N18
\dec2|WideOr3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr3~0_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ $ (((\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\) 
-- # (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\))))) # (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & (((!\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & 
-- !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)) # (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001100111010111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \dec2|WideOr3~0_combout\);

-- Location: LCCOMB_X29_Y16_N12
\dec2|WideOr2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr2~0_combout\ = ((\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((!\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\))) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\))) # (!\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111111011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \dec2|WideOr2~0_combout\);

-- Location: LCCOMB_X29_Y16_N2
\dec2|WideOr1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr1~0_combout\ = (\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & 
-- ((!\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\))) # (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)))) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ $ (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011101101010111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \dec2|WideOr1~0_combout\);

-- Location: LCCOMB_X29_Y16_N0
\dec2|WideOr0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr0~0_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & (\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ $ 
-- (((\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\))))) # (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & (\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & 
-- ((\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\) # (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \dec2|WideOr0~0_combout\);

-- Location: LCCOMB_X36_Y9_N22
\Div2|auto_generated|divider|divider|StageOut[108]~115\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[108]~115_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[13]~4_combout\) # ((\sec_cnt|count_second\(13) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datab => \sec_cnt|count_second\(13),
	datac => \SW~combout\(9),
	datad => \toOut[13]~4_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[108]~115_combout\);

-- Location: LCCOMB_X36_Y11_N18
\Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\ = (\toOut[8]~15_combout\ & (\Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\ & VCC)) # (!\toOut[8]~15_combout\ & 
-- (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\))
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\ = CARRY((!\toOut[8]~15_combout\ & !\Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \toOut[8]~15_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\,
	combout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\);

-- Location: LCCOMB_X36_Y11_N20
\Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\ = (\toOut[9]~13_combout\ & (\Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\ $ (GND))) # (!\toOut[9]~13_combout\ & 
-- (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\ & VCC))
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\ = CARRY((\toOut[9]~13_combout\ & !\Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \toOut[9]~13_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\,
	combout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\);

-- Location: LCCOMB_X36_Y11_N22
\Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\ = (\toOut[10]~11_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\)) # (!\toOut[10]~11_combout\ & 
-- ((\Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\) # (GND)))
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\) # (!\toOut[10]~11_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \toOut[10]~11_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\,
	combout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\);

-- Location: LCCOMB_X36_Y11_N24
\Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\ = (\toOut[11]~9_combout\ & (\Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\ $ (GND))) # (!\toOut[11]~9_combout\ & 
-- (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\ & VCC))
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\ = CARRY((\toOut[11]~9_combout\ & !\Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \toOut[11]~9_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\,
	combout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\);

-- Location: LCCOMB_X36_Y11_N30
\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ = !\Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\,
	combout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\);

-- Location: LCCOMB_X36_Y9_N8
\Div2|auto_generated|divider|divider|StageOut[107]~116\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[107]~116_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[12]~6_combout\) # ((\sec_cnt|count_second\(12) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(12),
	datab => \toOut[12]~6_combout\,
	datac => \SW~combout\(9),
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[107]~116_combout\);

-- Location: LCCOMB_X36_Y9_N28
\Div2|auto_generated|divider|divider|StageOut[106]~84\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[106]~84_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\ & !\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[106]~84_combout\);

-- Location: LCCOMB_X36_Y9_N14
\Div2|auto_generated|divider|divider|StageOut[105]~118\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[105]~118_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[10]~10_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(10)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \sec_cnt|count_second\(10),
	datac => \toOut[10]~10_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[105]~118_combout\);

-- Location: LCCOMB_X35_Y9_N28
\Div2|auto_generated|divider|divider|StageOut[104]~86\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[104]~86_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\ & !\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[104]~86_combout\);

-- Location: LCCOMB_X36_Y9_N24
\Div2|auto_generated|divider|divider|StageOut[103]~120\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[103]~120_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[8]~14_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(8)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \toOut[8]~14_combout\,
	datac => \sec_cnt|count_second\(8),
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[103]~120_combout\);

-- Location: LCCOMB_X36_Y9_N30
\Div2|auto_generated|divider|divider|StageOut[102]~121\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[102]~121_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[7]~16_combout\) # ((\sec_cnt|count_second\(7) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(7),
	datab => \toOut[7]~16_combout\,
	datac => \SW~combout\(9),
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[102]~121_combout\);

-- Location: LCCOMB_X35_Y10_N18
\Div2|auto_generated|divider|divider|StageOut[101]~122\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[101]~122_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[6]~18_combout\) # ((\sec_cnt|count_second\(6) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(6),
	datab => \SW~combout\(9),
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \toOut[6]~18_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[101]~122_combout\);

-- Location: LCCOMB_X35_Y9_N0
\Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\ = (((\Div2|auto_generated|divider|divider|StageOut[101]~123_combout\) # (\Div2|auto_generated|divider|divider|StageOut[101]~122_combout\)))
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\ = CARRY((\Div2|auto_generated|divider|divider|StageOut[101]~123_combout\) # (\Div2|auto_generated|divider|divider|StageOut[101]~122_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[101]~123_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[101]~122_combout\,
	datad => VCC,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\);

-- Location: LCCOMB_X35_Y9_N2
\Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\ & (((\Div2|auto_generated|divider|divider|StageOut[102]~88_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[102]~121_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\ & (!\Div2|auto_generated|divider|divider|StageOut[102]~88_combout\ & 
-- (!\Div2|auto_generated|divider|divider|StageOut[102]~121_combout\)))
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ = CARRY((!\Div2|auto_generated|divider|divider|StageOut[102]~88_combout\ & (!\Div2|auto_generated|divider|divider|StageOut[102]~121_combout\ & 
-- !\Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[102]~88_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[102]~121_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\);

-- Location: LCCOMB_X35_Y9_N4
\Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ & (((\Div2|auto_generated|divider|divider|StageOut[103]~87_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[103]~120_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ & ((((\Div2|auto_generated|divider|divider|StageOut[103]~87_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[103]~120_combout\)))))
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ & ((\Div2|auto_generated|divider|divider|StageOut[103]~87_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[103]~120_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[103]~87_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[103]~120_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\);

-- Location: LCCOMB_X35_Y9_N6
\Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\ = (\Div2|auto_generated|divider|divider|StageOut[104]~119_combout\ & (((!\Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\)))) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[104]~119_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[104]~86_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\)) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[104]~86_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\) # (GND)))))
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ = CARRY(((!\Div2|auto_generated|divider|divider|StageOut[104]~119_combout\ & !\Div2|auto_generated|divider|divider|StageOut[104]~86_combout\)) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[104]~119_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[104]~86_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\);

-- Location: LCCOMB_X35_Y9_N8
\Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ & (((\Div2|auto_generated|divider|divider|StageOut[105]~85_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[105]~118_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ & ((((\Div2|auto_generated|divider|divider|StageOut[105]~85_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[105]~118_combout\)))))
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ & ((\Div2|auto_generated|divider|divider|StageOut[105]~85_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[105]~118_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[105]~85_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[105]~118_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\);

-- Location: LCCOMB_X35_Y9_N14
\Div2|auto_generated|divider|divider|add_sub_10_result_int[10]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\ = CARRY((!\Div2|auto_generated|divider|divider|StageOut[108]~82_combout\ & (!\Div2|auto_generated|divider|divider|StageOut[108]~115_combout\ & 
-- !\Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[108]~82_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[108]~115_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\,
	cout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\);

-- Location: LCCOMB_X35_Y9_N16
\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ = \Div2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\);

-- Location: LCCOMB_X36_Y9_N12
\Div2|auto_generated|divider|divider|StageOut[119]~89\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[119]~89_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\ & !\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[119]~89_combout\);

-- Location: LCCOMB_X37_Y9_N6
\Div2|auto_generated|divider|divider|StageOut[106]~117\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[106]~117_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[11]~8_combout\) # ((\sec_cnt|count_second\(11) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(11),
	datab => \SW~combout\(9),
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \toOut[11]~8_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[106]~117_combout\);

-- Location: LCCOMB_X36_Y9_N10
\Div2|auto_generated|divider|divider|StageOut[118]~125\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[118]~125_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[106]~117_combout\) # 
-- ((!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\,
	datac => \Div2|auto_generated|divider|divider|StageOut[106]~117_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[118]~125_combout\);

-- Location: LCCOMB_X35_Y9_N24
\Div2|auto_generated|divider|divider|StageOut[117]~91\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[117]~91_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[117]~91_combout\);

-- Location: LCCOMB_X37_Y8_N8
\Div2|auto_generated|divider|divider|StageOut[116]~92\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[116]~92_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[116]~92_combout\);

-- Location: LCCOMB_X36_Y8_N2
\Div2|auto_generated|divider|divider|StageOut[115]~93\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[115]~93_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\ & !\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[115]~93_combout\);

-- Location: LCCOMB_X37_Y8_N14
\Div2|auto_generated|divider|divider|StageOut[114]~94\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[114]~94_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\ & !\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[114]~94_combout\);

-- Location: LCCOMB_X36_Y9_N18
\Div2|auto_generated|divider|divider|StageOut[113]~130\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[113]~130_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\toOut[6]~18_combout\) # ((\sec_cnt|count_second\(6) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(6),
	datab => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \SW~combout\(9),
	datad => \toOut[6]~18_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[113]~130_combout\);

-- Location: LCCOMB_X34_Y9_N12
\Div2|auto_generated|divider|divider|StageOut[112]~131\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[112]~131_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\toOut[5]~19_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \sec_cnt|count_second\(5),
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \toOut[5]~19_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[112]~131_combout\);

-- Location: LCCOMB_X36_Y8_N12
\Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\ & (((\Div2|auto_generated|divider|divider|StageOut[113]~95_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[113]~130_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\ & (!\Div2|auto_generated|divider|divider|StageOut[113]~95_combout\ & 
-- (!\Div2|auto_generated|divider|divider|StageOut[113]~130_combout\)))
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ = CARRY((!\Div2|auto_generated|divider|divider|StageOut[113]~95_combout\ & (!\Div2|auto_generated|divider|divider|StageOut[113]~130_combout\ & 
-- !\Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[113]~95_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[113]~130_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\);

-- Location: LCCOMB_X36_Y8_N24
\Div2|auto_generated|divider|divider|add_sub_11_result_int[10]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\ = CARRY((!\Div2|auto_generated|divider|divider|StageOut[119]~124_combout\ & (!\Div2|auto_generated|divider|divider|StageOut[119]~89_combout\ & 
-- !\Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[119]~124_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[119]~89_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\,
	cout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\);

-- Location: LCCOMB_X36_Y8_N26
\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ = \Div2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\);

-- Location: LCCOMB_X36_Y8_N8
\Div2|auto_generated|divider|divider|StageOut[130]~134\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[130]~134_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[118]~125_combout\) # 
-- ((\Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\ & !\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|StageOut[118]~125_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[130]~134_combout\);

-- Location: LCCOMB_X35_Y9_N18
\Div2|auto_generated|divider|divider|StageOut[117]~126\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[117]~126_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[105]~118_combout\) # 
-- ((!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[105]~118_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[117]~126_combout\);

-- Location: LCCOMB_X36_Y8_N0
\Div2|auto_generated|divider|divider|StageOut[129]~135\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[129]~135_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[117]~126_combout\) # 
-- ((!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|StageOut[117]~126_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[129]~135_combout\);

-- Location: LCCOMB_X35_Y10_N16
\Div2|auto_generated|divider|divider|StageOut[104]~119\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[104]~119_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[9]~12_combout\) # ((\sec_cnt|count_second\(9) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(9),
	datab => \SW~combout\(9),
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \toOut[9]~12_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[104]~119_combout\);

-- Location: LCCOMB_X35_Y9_N20
\Div2|auto_generated|divider|divider|StageOut[116]~127\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[116]~127_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[104]~119_combout\) # 
-- ((\Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\ & !\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\,
	datac => \Div2|auto_generated|divider|divider|StageOut[104]~119_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[116]~127_combout\);

-- Location: LCCOMB_X37_Y8_N10
\Div2|auto_generated|divider|divider|StageOut[128]~136\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[128]~136_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[116]~127_combout\) # 
-- ((!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\,
	datac => \Div2|auto_generated|divider|divider|StageOut[116]~127_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[128]~136_combout\);

-- Location: LCCOMB_X36_Y9_N16
\Div2|auto_generated|divider|divider|StageOut[115]~128\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[115]~128_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[103]~120_combout\) # 
-- ((!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\,
	datac => \Div2|auto_generated|divider|divider|StageOut[103]~120_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[115]~128_combout\);

-- Location: LCCOMB_X36_Y8_N30
\Div2|auto_generated|divider|divider|StageOut[127]~137\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[127]~137_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[115]~128_combout\) # 
-- ((\Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\ & !\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\,
	datac => \Div2|auto_generated|divider|divider|StageOut[115]~128_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[127]~137_combout\);

-- Location: LCCOMB_X37_Y8_N2
\Div2|auto_generated|divider|divider|StageOut[126]~101\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[126]~101_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[126]~101_combout\);

-- Location: LCCOMB_X35_Y8_N28
\Div2|auto_generated|divider|divider|StageOut[125]~139\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[125]~139_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[113]~130_combout\) # 
-- ((\Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\ & !\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|StageOut[113]~130_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[125]~139_combout\);

-- Location: LCCOMB_X37_Y8_N20
\Div2|auto_generated|divider|divider|StageOut[124]~103\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[124]~103_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[124]~103_combout\);

-- Location: LCCOMB_X34_Y9_N4
\Div2|auto_generated|divider|divider|StageOut[99]~142\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[99]~142_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[4]~20_combout\) # ((\sec_cnt|count_second\(4) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datab => \sec_cnt|count_second\(4),
	datac => \SW~combout\(9),
	datad => \toOut[4]~20_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[99]~142_combout\);

-- Location: LCCOMB_X34_Y9_N14
\Div2|auto_generated|divider|divider|StageOut[99]~143\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[99]~143_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\toOut[4]~20_combout\) # ((\sec_cnt|count_second\(4) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datab => \sec_cnt|count_second\(4),
	datac => \SW~combout\(9),
	datad => \toOut[4]~20_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[99]~143_combout\);

-- Location: LCCOMB_X34_Y9_N22
\Div2|auto_generated|divider|divider|add_sub_10_result_int[1]~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\ = (\Div2|auto_generated|divider|divider|StageOut[99]~142_combout\) # (\Div2|auto_generated|divider|divider|StageOut[99]~143_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|StageOut[99]~142_combout\,
	datac => \Div2|auto_generated|divider|divider|StageOut[99]~143_combout\,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\);

-- Location: LCCOMB_X34_Y9_N20
\Div2|auto_generated|divider|divider|StageOut[111]~104\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[111]~104_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[111]~104_combout\);

-- Location: LCCOMB_X34_Y9_N6
\Div2|auto_generated|divider|divider|StageOut[111]~141\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[111]~141_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\toOut[4]~20_combout\) # ((\SW~combout\(9) & \sec_cnt|count_second\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \sec_cnt|count_second\(4),
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \toOut[4]~20_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[111]~141_combout\);

-- Location: LCCOMB_X34_Y9_N8
\Div2|auto_generated|divider|divider|add_sub_11_result_int[2]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\ = (\Div2|auto_generated|divider|divider|StageOut[111]~104_combout\) # (\Div2|auto_generated|divider|divider|StageOut[111]~141_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|StageOut[111]~104_combout\,
	datad => \Div2|auto_generated|divider|divider|StageOut[111]~141_combout\,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\);

-- Location: LCCOMB_X34_Y9_N26
\Div2|auto_generated|divider|divider|StageOut[123]~105\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[123]~105_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[123]~105_combout\);

-- Location: LCCOMB_X35_Y8_N8
\Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\ = (\Div2|auto_generated|divider|divider|StageOut[126]~138_combout\ & (((!\Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\)))) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[126]~138_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[126]~101_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\)) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[126]~101_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\) # (GND)))))
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ = CARRY(((!\Div2|auto_generated|divider|divider|StageOut[126]~138_combout\ & !\Div2|auto_generated|divider|divider|StageOut[126]~101_combout\)) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[126]~138_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[126]~101_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\,
	combout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\);

-- Location: LCCOMB_X35_Y8_N12
\Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\ = (\Div2|auto_generated|divider|divider|StageOut[128]~99_combout\ & (((!\Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\)))) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[128]~99_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[128]~136_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\)) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[128]~136_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\) # (GND)))))
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ = CARRY(((!\Div2|auto_generated|divider|divider|StageOut[128]~99_combout\ & !\Div2|auto_generated|divider|divider|StageOut[128]~136_combout\)) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[128]~99_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[128]~136_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\,
	combout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\);

-- Location: LCCOMB_X35_Y8_N14
\Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ & (((\Div2|auto_generated|divider|divider|StageOut[129]~98_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[129]~135_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ & ((((\Div2|auto_generated|divider|divider|StageOut[129]~98_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[129]~135_combout\)))))
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ & ((\Div2|auto_generated|divider|divider|StageOut[129]~98_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[129]~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[129]~98_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[129]~135_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\,
	combout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\);

-- Location: LCCOMB_X35_Y8_N16
\Div2|auto_generated|divider|divider|add_sub_12_result_int[10]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\ = CARRY((!\Div2|auto_generated|divider|divider|StageOut[130]~97_combout\ & (!\Div2|auto_generated|divider|divider|StageOut[130]~134_combout\ & 
-- !\Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[130]~97_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[130]~134_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\,
	cout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\);

-- Location: LCCOMB_X35_Y8_N18
\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ = \Div2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\,
	combout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\);

-- Location: LCCOMB_X34_Y8_N24
\Div2|auto_generated|divider|divider|StageOut[141]~106\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[141]~106_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\ & !\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[141]~106_combout\);

-- Location: LCCOMB_X34_Y8_N2
\Div2|auto_generated|divider|divider|StageOut[140]~107\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[140]~107_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\ & !\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[140]~107_combout\);

-- Location: LCCOMB_X35_Y8_N0
\Div2|auto_generated|divider|divider|StageOut[139]~147\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[139]~147_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[127]~137_combout\) # 
-- ((\Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|StageOut[127]~137_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[139]~147_combout\);

-- Location: LCCOMB_X34_Y8_N28
\Div2|auto_generated|divider|divider|StageOut[138]~109\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[138]~109_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\ & !\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[138]~109_combout\);

-- Location: LCCOMB_X35_Y8_N30
\Div2|auto_generated|divider|divider|StageOut[137]~149\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[137]~149_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[125]~139_combout\) # 
-- ((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[125]~139_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[137]~149_combout\);

-- Location: LCCOMB_X34_Y8_N0
\Div2|auto_generated|divider|divider|StageOut[136]~111\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[136]~111_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\ & !\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[136]~111_combout\);

-- Location: LCCOMB_X34_Y8_N26
\Div2|auto_generated|divider|divider|StageOut[135]~112\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[135]~112_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\ & !\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[135]~112_combout\);

-- Location: LCCOMB_X40_Y9_N14
\Div2|auto_generated|divider|divider|StageOut[110]~153\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[110]~153_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\toOut[3]~21_combout\) # ((\sec_cnt|count_second\(3) & \SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \sec_cnt|count_second\(3),
	datac => \SW~combout\(9),
	datad => \toOut[3]~21_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[110]~153_combout\);

-- Location: LCCOMB_X40_Y9_N28
\Div2|auto_generated|divider|divider|add_sub_11_result_int[1]~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\ = (\Div2|auto_generated|divider|divider|StageOut[110]~154_combout\) # (\Div2|auto_generated|divider|divider|StageOut[110]~153_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[110]~154_combout\,
	datac => \Div2|auto_generated|divider|divider|StageOut[110]~153_combout\,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\);

-- Location: LCCOMB_X40_Y9_N8
\Div2|auto_generated|divider|divider|StageOut[122]~113\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[122]~113_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[122]~113_combout\);

-- Location: LCCOMB_X40_Y9_N18
\Div2|auto_generated|divider|divider|add_sub_12_result_int[2]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\ = (\Div2|auto_generated|divider|divider|StageOut[122]~152_combout\) # (\Div2|auto_generated|divider|divider|StageOut[122]~113_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[122]~152_combout\,
	datac => \Div2|auto_generated|divider|divider|StageOut[122]~113_combout\,
	combout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\);

-- Location: LCCOMB_X40_Y9_N26
\Div2|auto_generated|divider|divider|StageOut[134]~114\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[134]~114_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[134]~114_combout\);

-- Location: LCCOMB_X34_Y8_N4
\Div2|auto_generated|divider|divider|add_sub_13_result_int[3]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_13_result_int[3]~1_cout\ = CARRY((\Div2|auto_generated|divider|divider|StageOut[134]~155_combout\) # (\Div2|auto_generated|divider|divider|StageOut[134]~114_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[134]~155_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[134]~114_combout\,
	datad => VCC,
	cout => \Div2|auto_generated|divider|divider|add_sub_13_result_int[3]~1_cout\);

-- Location: LCCOMB_X34_Y8_N6
\Div2|auto_generated|divider|divider|add_sub_13_result_int[4]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_13_result_int[4]~3_cout\ = CARRY((!\Div2|auto_generated|divider|divider|StageOut[135]~151_combout\ & (!\Div2|auto_generated|divider|divider|StageOut[135]~112_combout\ & 
-- !\Div2|auto_generated|divider|divider|add_sub_13_result_int[3]~1_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[135]~151_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[135]~112_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_13_result_int[3]~1_cout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_13_result_int[4]~3_cout\);

-- Location: LCCOMB_X34_Y8_N8
\Div2|auto_generated|divider|divider|add_sub_13_result_int[5]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_13_result_int[5]~5_cout\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_13_result_int[4]~3_cout\ & ((\Div2|auto_generated|divider|divider|StageOut[136]~150_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[136]~111_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[136]~150_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[136]~111_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_13_result_int[4]~3_cout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_13_result_int[5]~5_cout\);

-- Location: LCCOMB_X34_Y8_N10
\Div2|auto_generated|divider|divider|add_sub_13_result_int[6]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_13_result_int[6]~7_cout\ = CARRY(((!\Div2|auto_generated|divider|divider|StageOut[137]~110_combout\ & !\Div2|auto_generated|divider|divider|StageOut[137]~149_combout\)) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_13_result_int[5]~5_cout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[137]~110_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[137]~149_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_13_result_int[5]~5_cout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_13_result_int[6]~7_cout\);

-- Location: LCCOMB_X34_Y8_N12
\Div2|auto_generated|divider|divider|add_sub_13_result_int[7]~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_13_result_int[7]~9_cout\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_13_result_int[6]~7_cout\ & ((\Div2|auto_generated|divider|divider|StageOut[138]~148_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[138]~109_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[138]~148_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[138]~109_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_13_result_int[6]~7_cout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_13_result_int[7]~9_cout\);

-- Location: LCCOMB_X34_Y8_N14
\Div2|auto_generated|divider|divider|add_sub_13_result_int[8]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_13_result_int[8]~11_cout\ = CARRY(((!\Div2|auto_generated|divider|divider|StageOut[139]~108_combout\ & !\Div2|auto_generated|divider|divider|StageOut[139]~147_combout\)) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_13_result_int[7]~9_cout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[139]~108_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[139]~147_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_13_result_int[7]~9_cout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_13_result_int[8]~11_cout\);

-- Location: LCCOMB_X34_Y8_N16
\Div2|auto_generated|divider|divider|add_sub_13_result_int[9]~13\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_13_result_int[9]~13_cout\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_13_result_int[8]~11_cout\ & ((\Div2|auto_generated|divider|divider|StageOut[140]~146_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[140]~107_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[140]~146_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[140]~107_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_13_result_int[8]~11_cout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_13_result_int[9]~13_cout\);

-- Location: LCCOMB_X34_Y8_N18
\Div2|auto_generated|divider|divider|add_sub_13_result_int[10]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\ = CARRY((!\Div2|auto_generated|divider|divider|StageOut[141]~145_combout\ & (!\Div2|auto_generated|divider|divider|StageOut[141]~106_combout\ & 
-- !\Div2|auto_generated|divider|divider|add_sub_13_result_int[9]~13_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[141]~145_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[141]~106_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_13_result_int[9]~13_cout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\);

-- Location: LCCOMB_X34_Y8_N20
\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ = \Div2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\,
	combout => \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\);

-- Location: LCCOMB_X32_Y11_N4
\dec3|WideOr6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr6~0_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ $ 
-- (((\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\) # (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))) # (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & 
-- (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001100111010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \dec3|WideOr6~0_combout\);

-- Location: LCCOMB_X32_Y11_N10
\dec3|WideOr5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr5~0_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & 
-- (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ $ (\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & 
-- (((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)) # (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000101111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \dec3|WideOr5~0_combout\);

-- Location: LCCOMB_X32_Y11_N0
\dec3|WideOr4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr4~0_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & 
-- ((\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\) # (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & 
-- (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \dec3|WideOr4~0_combout\);

-- Location: LCCOMB_X32_Y11_N18
\dec3|WideOr3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr3~0_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ $ 
-- (((\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\) # (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))) # (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & 
-- (((!\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)) # (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001100111010111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \dec3|WideOr3~0_combout\);

-- Location: LCCOMB_X32_Y11_N8
\dec3|WideOr2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr2~0_combout\ = ((\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))) # (!\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111111011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \dec3|WideOr2~0_combout\);

-- Location: LCCOMB_X32_Y11_N26
\dec3|WideOr1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr1~0_combout\ = (\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & 
-- ((!\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\))) # (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)))) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ $ (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011101101010111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \dec3|WideOr1~0_combout\);

-- Location: LCCOMB_X32_Y11_N16
\dec3|WideOr0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr0~0_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ $ 
-- (((\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))) # (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & 
-- ((\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\) # (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \dec3|WideOr0~0_combout\);

-- Location: PIN_U12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(5));

-- Location: PIN_U11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(6));

-- Location: PIN_M2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[7]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(7));

-- Location: PIN_M1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[8]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(8));

-- Location: PIN_R21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(1));

-- Location: PIN_R20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Equal0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(0));

-- Location: PIN_R19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Equal0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(1));

-- Location: PIN_U19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(2));

-- Location: PIN_Y19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Equal0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(3));

-- Location: PIN_T18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(4));

-- Location: PIN_U22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDG[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \myFIFO|Equal1~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDG(0));

-- Location: PIN_U21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDG[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \myFIFO|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDG(1));

-- Location: PIN_J2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(0));

-- Location: PIN_J1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(1));

-- Location: PIN_H2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(2));

-- Location: PIN_H1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(3));

-- Location: PIN_F2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(4));

-- Location: PIN_F1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(5));

-- Location: PIN_E2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|ALT_INV_WideOr0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(6));

-- Location: PIN_E1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(0));

-- Location: PIN_H6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(1));

-- Location: PIN_H5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(2));

-- Location: PIN_H4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(3));

-- Location: PIN_G3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(4));

-- Location: PIN_D2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(5));

-- Location: PIN_D1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|ALT_INV_WideOr0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(6));

-- Location: PIN_G5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(0));

-- Location: PIN_G6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(1));

-- Location: PIN_C2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|WideOr4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(2));

-- Location: PIN_C1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(3));

-- Location: PIN_E3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(4));

-- Location: PIN_E4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(5));

-- Location: PIN_D3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|ALT_INV_WideOr0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(6));

-- Location: PIN_F4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(0));

-- Location: PIN_D5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(1));

-- Location: PIN_D6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|WideOr4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(2));

-- Location: PIN_J4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(3));

-- Location: PIN_L8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(4));

-- Location: PIN_F3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(5));

-- Location: PIN_D4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|ALT_INV_WideOr0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(6));
END structure;


