module one_sec_counter(input clk,
							  input rst,
							  input start,
							  output one_sec);
	parameter MAX_TICKS = 26'd499999;
	reg[25:0] cntr;
	assign one_sec = (cntr == MAX_TICKS)? 1'b1 : 1'b0;

	always@(posedge clk)
		if(rst) cntr <= 26'b0;
		else if (start)
					if(cntr == MAX_TICKS) cntr <= 26'b0;
					else cntr <= cntr + 1;
			  else cntr <= 26'b0;
			  
endmodule