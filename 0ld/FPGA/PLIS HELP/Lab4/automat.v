module automat(input clk,
					input  [9:0]    SW,
					input  [3:0]	 KEY,
					output reg[2:0] state);
					
	localparam STATE1 = 3'b000;
	localparam STATE2 = 3'b001;
	localparam STATE3 = 3'b010;
	localparam STATE4 = 3'b011;
	localparam STATE5 = 3'b100;
	
	assign rst = ~KEY[0];
	
	always @(posedge clk)
		if (rst)
			state <= STATE3;
		else case (state)
		STATE1: if (SW[4]) state <= STATE3;
				  else state <= STATE1;
		STATE2: if (SW[2]) state <= STATE1;
				  else state <= STATE2;
		STATE3: if (SW[3]) state <= STATE2;
				  else if (SW[0]) state <=STATE4;
				  else state <= STATE3;
		STATE4: if (SW[2]) state <= STATE1;
				  else if (SW[1]) state <= STATE5;
				  else state <= STATE4;
		STATE5: if (~SW[1]) state <= STATE1;
				  else state <= STATE5;
		endcase
					
endmodule