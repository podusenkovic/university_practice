module variant3(input [9:0]SW,					 
				    output reg[9:0]LEDR,
				    output[6:0]HEX0,
					 output[6:0]HEX1,
					 output[6:0]HEX2,
				    output[6:0]HEX3);
	
	integer bitsNum = 0;
	
	always @(SW) begin : loop_block
		casex (SW)
			10'bxxxxxxxxx0 : begin LEDR = 10'b0000000001; bitsNum = 0; end
			10'bxxxxxxxx01 : begin LEDR = 10'b0000000010; bitsNum = 1; end
			10'bxxxxxxx011 : begin LEDR = 10'b0000000100; bitsNum = 2; end
			10'bxxxxxx0111 : begin LEDR = 10'b0000001000; bitsNum = 3; end
			10'bxxxxx01111 : begin LEDR = 10'b0000010000; bitsNum = 4; end
			10'bxxxx011111 : begin LEDR = 10'b0000100000; bitsNum = 5; end
			10'bxxx0111111 : begin LEDR = 10'b0001000000; bitsNum = 6; end
			10'bxx01111111 : begin LEDR = 10'b0010000000; bitsNum = 7; end
			10'bx011111111 : begin LEDR = 10'b0100000000; bitsNum = 8; end
			10'b0111111111 : begin LEDR = 10'b1000000000; bitsNum = 9; end
			default : begin LEDR = 10'b0000000000; bitsNum = 10; end
		endcase
	end


	dec dec0(.in(bitsNum),
				.out(HEX0));
				
	dec dec1(.in(4'd10),
				.out(HEX1));
				
	dec dec2(.in(4'd10),
				.out(HEX2));	
				
	dec dec3(.in(4'd10),
				.out(HEX3));

endmodule