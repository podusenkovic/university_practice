`timescale 1ns/1ps

module tb_variant3();
	
	reg [9:0]SW;
	wire [9:0]LEDR;
	wire [6:0]HEX0;
	wire [6:0]HEX1;
	wire [6:0]HEX2;
	wire [6:0]HEX3;
	
	
	variant3 DUT(.SW(SW),
				    .LEDR(LEDR),
					 .HEX0(HEX0),
					 .HEX1(HEX1),
					 .HEX2(HEX2),
					 .HEX3(HEX3));
					 
	initial begin
		SW = 10'b0001111111;
		#100
		SW = 10'b0001100010;
		#100
		SW = 10'b0111111101;
		#100
		SW = 10'b0111110111;
		#100
		SW = 10'b0000000000;
		#100
		SW = 10'b1111111111;
		#100
		SW = 10'b1000011111;
		#100
		SW = 10'b0111111111;
		#100
		SW = 10'b0100011111;
		end
	
endmodule