module main(input           CLOCK_50,
				input  [9:0]    SW,
				input  [3:0]	 KEY,
				output [6:0]    HEX0,
				output [6:0]    HEX1,
				output [6:0]    HEX2,
				output [6:0]    HEX3);
	
	wire secChanged;
	
	localparam END_COUNT = 26'd49999;
	localparam MAX_SECONDS = 14'd9999;
	wire[15:0] decs;
	wire[13:0] timer;
	
	one_sec_counter count_0(.clk(CLOCK_50),
									.rst(~KEY[0]),
									.start(SW[0]),
									.one_sec(secChanged));
	
	sec_counter sec_cnt(.one_second(secChanged),
					.rst(~KEY[0]),
					.clk(CLOCK_50),
					.count_second(timer));
	
	assign decs[3:0] = timer%10;
	assign decs[7:4] = (timer%100)/10;
	assign decs[11:8] = (timer%1000)/100;
	assign decs[15:12] = timer/1000;
	
	
	dec dec0(.in(decs[3:0]),
				.out(HEX0));
				
	dec dec1(.in(decs[7:4]),
				.out(HEX1));
				
	dec dec2(.in(decs[11:8]),
				.out(HEX2));		
				
	dec dec3(.in(decs[15:12]),
				.out(HEX3));
	

endmodule