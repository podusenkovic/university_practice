library verilog;
use verilog.vl_types.all;
entity sec_counter is
    port(
        one_second      : in     vl_logic;
        rst             : in     vl_logic;
        clk             : in     vl_logic;
        MAX_SECONDS     : in     vl_logic;
        count_second    : out    vl_logic_vector(13 downto 0)
    );
end sec_counter;
