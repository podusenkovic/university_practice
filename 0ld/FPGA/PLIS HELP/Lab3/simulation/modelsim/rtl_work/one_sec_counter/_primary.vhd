library verilog;
use verilog.vl_types.all;
entity one_sec_counter is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        start           : in     vl_logic;
        END_COUNT       : in     vl_logic;
        one_sec         : out    vl_logic
    );
end one_sec_counter;
