-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

-- DATE "03/13/2018 11:56:40"

-- 
-- Device: Altera EP2C20F484C7 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEII;
LIBRARY IEEE;
USE CYCLONEII.CYCLONEII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	main IS
    PORT (
	CLOCK_50 : IN std_logic;
	SW : IN std_logic_vector(9 DOWNTO 0);
	KEY : IN std_logic_vector(3 DOWNTO 0);
	HEX0 : OUT std_logic_vector(6 DOWNTO 0);
	HEX1 : OUT std_logic_vector(6 DOWNTO 0);
	HEX2 : OUT std_logic_vector(6 DOWNTO 0);
	HEX3 : OUT std_logic_vector(6 DOWNTO 0)
	);
END main;

-- Design Ports Information
-- SW[1]	=>  Location: PIN_L21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_M22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_V12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_W12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[5]	=>  Location: PIN_U12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_U11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_M2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[8]	=>  Location: PIN_M1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[9]	=>  Location: PIN_L2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[1]	=>  Location: PIN_R21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[2]	=>  Location: PIN_T22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[3]	=>  Location: PIN_T21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- HEX0[0]	=>  Location: PIN_J2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[1]	=>  Location: PIN_J1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[2]	=>  Location: PIN_H2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[3]	=>  Location: PIN_H1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[4]	=>  Location: PIN_F2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[5]	=>  Location: PIN_F1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[6]	=>  Location: PIN_E2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[0]	=>  Location: PIN_E1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[1]	=>  Location: PIN_H6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[2]	=>  Location: PIN_H5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[3]	=>  Location: PIN_H4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[4]	=>  Location: PIN_G3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[5]	=>  Location: PIN_D2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[6]	=>  Location: PIN_D1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[0]	=>  Location: PIN_G5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[1]	=>  Location: PIN_G6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[2]	=>  Location: PIN_C2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[3]	=>  Location: PIN_C1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[4]	=>  Location: PIN_E3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[5]	=>  Location: PIN_E4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[6]	=>  Location: PIN_D3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[0]	=>  Location: PIN_F4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[1]	=>  Location: PIN_D5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[2]	=>  Location: PIN_D6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[3]	=>  Location: PIN_J4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[4]	=>  Location: PIN_L8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[5]	=>  Location: PIN_F3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[6]	=>  Location: PIN_D4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- CLOCK_50	=>  Location: PIN_L1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[0]	=>  Location: PIN_R22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_L22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF main IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLOCK_50 : std_logic;
SIGNAL ww_SW : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_KEY : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_HEX0 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX2 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX3 : std_logic_vector(6 DOWNTO 0);
SIGNAL \CLOCK_50~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~4_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~6_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~8_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~2_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~8_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~0_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~6_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~8_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~2_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~0_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~8_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[0]~14_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[1]~14_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\ : std_logic;
SIGNAL \count_0|cntr[0]~26_combout\ : std_logic;
SIGNAL \count_0|cntr[5]~37_combout\ : std_logic;
SIGNAL \count_0|cntr[9]~45_combout\ : std_logic;
SIGNAL \count_0|cntr[19]~65_combout\ : std_logic;
SIGNAL \count_0|cntr[23]~73_combout\ : std_logic;
SIGNAL \count_0|cntr[24]~76\ : std_logic;
SIGNAL \count_0|cntr[25]~77_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[18]~97_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[17]~98_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[16]~101_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[15]~102_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[23]~104_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[21]~107_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[20]~108_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[27]~111_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[26]~113_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[25]~115_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[32]~117_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[31]~119_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[30]~120_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[36]~125_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[35]~127_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[41]~131_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[40]~132_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[46]~137_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[45]~139_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[51]~143_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[50]~145_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[56]~148_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[55]~151_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[60]~153_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[61]~156_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[54]~144_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[53]~147_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[52]~148_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[51]~150_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[50]~152_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[49]~154_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[61]~157_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[60]~158_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[58]~161_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[57]~163_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[70]~164_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[69]~165_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[68]~166_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[66]~168_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[65]~169_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[78]~171_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[77]~172_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[75]~174_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[73]~176_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[86]~178_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[84]~180_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[83]~181_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[81]~183_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[94]~185_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[93]~186_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[91]~188_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[90]~189_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[89]~190_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[101]~193_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[100]~194_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[99]~195_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[97]~198_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[18]~42_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[17]~43_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[22]~46_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[28]~49_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[27]~50_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[26]~52_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[108]~127_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[107]~128_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[106]~131_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[105]~133_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[104]~134_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[103]~137_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[102]~139_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[101]~141_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[119]~142_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[116]~145_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[114]~147_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[113]~148_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[100]~151_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[100]~152_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[112]~153_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[128]~156_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[127]~157_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[126]~158_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[125]~159_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[141]~166_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[139]~168_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[138]~169_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[122]~173_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[134]~177_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[151]~179_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[150]~180_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[54]~40_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[53]~41_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[51]~43_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[49]~45_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[62]~46_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[58]~50_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[146]~184_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[57]~52_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[70]~53_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[69]~54_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[68]~55_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[66]~57_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[56]~58_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[65]~59_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[108]~104_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[107]~107_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[106]~108_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[105]~111_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[104]~112_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[103]~115_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[102]~117_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[101]~119_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[118]~121_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[114]~125_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[113]~127_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[100]~129_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[100]~130_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[112]~131_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[130]~132_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[127]~135_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[125]~137_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[124]~138_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[111]~139_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[99]~140_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[99]~141_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[111]~142_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[138]~147_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[136]~149_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[122]~151_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[122]~154_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[134]~155_combout\ : std_logic;
SIGNAL \sec_cnt|Equal0~1_combout\ : std_logic;
SIGNAL \count_0|Equal0~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[28]~161_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[33]~162_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[38]~163_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[43]~164_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[48]~165_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[53]~166_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[58]~167_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[63]~168_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[76]~209_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[85]~211_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[102]~216_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[23]~58_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[130]~190_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[140]~196_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[137]~199_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[136]~200_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[135]~201_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[149]~205_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[148]~206_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[52]~62_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[61]~67_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[67]~76_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[56]~78_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[129]~157_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[128]~158_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[141]~161_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[140]~162_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[139]~163_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[137]~165_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[135]~167_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[22]~170_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[37]~173_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[42]~174_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[47]~175_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[52]~176_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[57]~177_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[62]~222_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[67]~226_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[74]~228_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[82]~229_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[98]~231_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[25]~63_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[118]~210_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[124]~216_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[123]~217_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[119]~168_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[117]~170_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[116]~171_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[115]~172_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[123]~176_combout\ : std_logic;
SIGNAL \CLOCK_50~combout\ : std_logic;
SIGNAL \CLOCK_50~clkctrl_outclk\ : std_logic;
SIGNAL \sec_cnt|count_second[0]~14_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[2]~21\ : std_logic;
SIGNAL \sec_cnt|count_second[3]~22_combout\ : std_logic;
SIGNAL \count_0|cntr[0]~27\ : std_logic;
SIGNAL \count_0|cntr[1]~29_combout\ : std_logic;
SIGNAL \count_0|cntr[7]~41_combout\ : std_logic;
SIGNAL \count_0|Equal0~1_combout\ : std_logic;
SIGNAL \count_0|cntr[2]~31_combout\ : std_logic;
SIGNAL \count_0|cntr[3]~33_combout\ : std_logic;
SIGNAL \count_0|Equal0~0_combout\ : std_logic;
SIGNAL \count_0|cntr[15]~57_combout\ : std_logic;
SIGNAL \count_0|Equal0~3_combout\ : std_logic;
SIGNAL \count_0|Equal0~4_combout\ : std_logic;
SIGNAL \count_0|cntr[21]~28_combout\ : std_logic;
SIGNAL \count_0|cntr[1]~30\ : std_logic;
SIGNAL \count_0|cntr[2]~32\ : std_logic;
SIGNAL \count_0|cntr[3]~34\ : std_logic;
SIGNAL \count_0|cntr[4]~35_combout\ : std_logic;
SIGNAL \count_0|cntr[4]~36\ : std_logic;
SIGNAL \count_0|cntr[5]~38\ : std_logic;
SIGNAL \count_0|cntr[6]~39_combout\ : std_logic;
SIGNAL \count_0|cntr[6]~40\ : std_logic;
SIGNAL \count_0|cntr[7]~42\ : std_logic;
SIGNAL \count_0|cntr[8]~43_combout\ : std_logic;
SIGNAL \count_0|cntr[8]~44\ : std_logic;
SIGNAL \count_0|cntr[9]~46\ : std_logic;
SIGNAL \count_0|cntr[10]~47_combout\ : std_logic;
SIGNAL \count_0|cntr[10]~48\ : std_logic;
SIGNAL \count_0|cntr[11]~49_combout\ : std_logic;
SIGNAL \count_0|cntr[11]~50\ : std_logic;
SIGNAL \count_0|cntr[12]~51_combout\ : std_logic;
SIGNAL \count_0|cntr[12]~52\ : std_logic;
SIGNAL \count_0|cntr[13]~53_combout\ : std_logic;
SIGNAL \count_0|cntr[13]~54\ : std_logic;
SIGNAL \count_0|cntr[14]~55_combout\ : std_logic;
SIGNAL \count_0|cntr[14]~56\ : std_logic;
SIGNAL \count_0|cntr[15]~58\ : std_logic;
SIGNAL \count_0|cntr[16]~60\ : std_logic;
SIGNAL \count_0|cntr[17]~61_combout\ : std_logic;
SIGNAL \count_0|cntr[17]~62\ : std_logic;
SIGNAL \count_0|cntr[18]~64\ : std_logic;
SIGNAL \count_0|cntr[19]~66\ : std_logic;
SIGNAL \count_0|cntr[20]~67_combout\ : std_logic;
SIGNAL \count_0|cntr[20]~68\ : std_logic;
SIGNAL \count_0|cntr[21]~69_combout\ : std_logic;
SIGNAL \count_0|cntr[21]~70\ : std_logic;
SIGNAL \count_0|cntr[22]~71_combout\ : std_logic;
SIGNAL \count_0|Equal0~6_combout\ : std_logic;
SIGNAL \count_0|cntr[22]~72\ : std_logic;
SIGNAL \count_0|cntr[23]~74\ : std_logic;
SIGNAL \count_0|cntr[24]~75_combout\ : std_logic;
SIGNAL \count_0|cntr[18]~63_combout\ : std_logic;
SIGNAL \count_0|cntr[16]~59_combout\ : std_logic;
SIGNAL \count_0|Equal0~5_combout\ : std_logic;
SIGNAL \count_0|Equal0~7_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[13]~17_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[3]~23\ : std_logic;
SIGNAL \sec_cnt|count_second[4]~24_combout\ : std_logic;
SIGNAL \sec_cnt|Equal0~2_combout\ : std_logic;
SIGNAL \sec_cnt|Equal0~3_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[4]~25\ : std_logic;
SIGNAL \sec_cnt|count_second[5]~27\ : std_logic;
SIGNAL \sec_cnt|count_second[6]~29\ : std_logic;
SIGNAL \sec_cnt|count_second[7]~30_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[7]~31\ : std_logic;
SIGNAL \sec_cnt|count_second[8]~33\ : std_logic;
SIGNAL \sec_cnt|count_second[9]~34_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[9]~35\ : std_logic;
SIGNAL \sec_cnt|count_second[10]~36_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[10]~37\ : std_logic;
SIGNAL \sec_cnt|count_second[11]~38_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[11]~39\ : std_logic;
SIGNAL \sec_cnt|count_second[12]~40_combout\ : std_logic;
SIGNAL \sec_cnt|Equal0~0_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[13]~16_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[0]~15\ : std_logic;
SIGNAL \sec_cnt|count_second[1]~18_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[1]~19\ : std_logic;
SIGNAL \sec_cnt|count_second[2]~20_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[55]~150_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~0_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[5]~26_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[46]~136_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[12]~41\ : std_logic;
SIGNAL \sec_cnt|count_second[13]~42_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[18]~96_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[17]~99_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[16]~100_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[15]~103_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[22]~105_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[21]~106_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[20]~109_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[23]~169_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[28]~110_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[27]~171_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[26]~112_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[8]~32_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[25]~114_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[33]~116_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[32]~172_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[31]~118_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[30]~121_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_7_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[38]~122_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[37]~123_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[36]~124_combout\ : std_logic;
SIGNAL \sec_cnt|count_second[6]~28_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[35]~126_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_8_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[43]~128_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[42]~129_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[41]~130_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[40]~133_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_9_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[45]~138_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[48]~134_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[47]~135_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_10_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[52]~141_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[51]~142_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[50]~144_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[53]~140_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_11_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[58]~146_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[57]~147_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[56]~149_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_12_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[61]~157_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[60]~152_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~1\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[63]~154_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~2_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[62]~155_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~3\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~5\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_13_result_int[4]~7_cout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[62]~178_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~4_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~0_combout\ : std_logic;
SIGNAL \Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\ : std_logic;
SIGNAL \dec0|WideOr6~0_combout\ : std_logic;
SIGNAL \dec0|WideOr5~0_combout\ : std_logic;
SIGNAL \dec0|WideOr4~0_combout\ : std_logic;
SIGNAL \dec0|WideOr3~0_combout\ : std_logic;
SIGNAL \dec0|WideOr2~0_combout\ : std_logic;
SIGNAL \dec0|WideOr1~0_combout\ : std_logic;
SIGNAL \dec0|WideOr0~0_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[54]~145_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[53]~146_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[52]~149_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[51]~151_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[50]~153_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[49]~155_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[61]~223_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[62]~156_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[60]~224_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[59]~159_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[58]~160_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[57]~162_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[70]~204_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[69]~205_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[59]~225_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[68]~206_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[67]~167_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[66]~227_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[65]~170_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~1\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~3\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~5\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~7\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~9\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~0_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[83]~233_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[78]~207_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[77]~208_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[76]~173_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[75]~232_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[74]~175_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[73]~177_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~1\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~3\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~5\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~7\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~9\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[7]~11_cout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~0_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[82]~182_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[81]~184_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~1\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~3\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~4_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[86]~210_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~6_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[85]~179_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~2_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[84]~212_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~5\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~7\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~9\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[7]~11_cout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[92]~215_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[101]~217_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[91]~234_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~2_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[100]~218_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[94]~213_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~4_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[93]~214_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[92]~187_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[90]~230_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[89]~191_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~1\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~3\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~5\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~7\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~9\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[7]~11_cout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[99]~235_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~0_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[98]~196_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[97]~197_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~1\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~3\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~5\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~7\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~8_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~6_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[110]~219_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~8_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[102]~192_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~9\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[7]~11_cout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[18]~53_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~4_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[109]~220_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~6_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[17]~54_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[110]~199_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[109]~200_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~4_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[108]~201_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[16]~44_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[107]~236_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[15]~57_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~2_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[108]~221_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[16]~55_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[22]~59_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[28]~60_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[15]~56_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[21]~47_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[27]~61_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~0_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[20]~64_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[23]~45_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[21]~48_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[20]~65_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[26]~51_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[96]~202_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|StageOut[96]~203_combout\ : std_logic;
SIGNAL \Mod1|auto_generated|divider|divider|add_sub_13_result_int[1]~14_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|StageOut[25]~62_combout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_6_result_int[1]~1_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_6_result_int[2]~3_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_6_result_int[3]~5_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\ : std_logic;
SIGNAL \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ : std_logic;
SIGNAL \dec1|WideOr6~0_combout\ : std_logic;
SIGNAL \dec1|WideOr5~0_combout\ : std_logic;
SIGNAL \dec1|WideOr4~0_combout\ : std_logic;
SIGNAL \dec1|WideOr3~0_combout\ : std_logic;
SIGNAL \dec1|WideOr2~0_combout\ : std_logic;
SIGNAL \dec1|WideOr1~0_combout\ : std_logic;
SIGNAL \dec1|WideOr0~0_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[103]~136_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[102]~138_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[101]~140_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[108]~126_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[107]~129_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[106]~130_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[105]~132_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[104]~135_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[119]~209_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[118]~143_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[117]~144_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[116]~212_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[115]~146_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[114]~214_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[113]~149_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[112]~150_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[115]~213_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[127]~193_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[130]~154_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[129]~155_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[128]~192_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[126]~194_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[125]~215_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[124]~160_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[111]~161_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[99]~163_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[99]~162_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[111]~164_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[123]~165_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[139]~197_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[138]~198_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[137]~170_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[136]~171_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[135]~172_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[110]~175_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[110]~174_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[134]~218_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~1\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~3\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~5\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~7\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~9\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~10_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[117]~211_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[129]~191_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[141]~195_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[140]~167_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~11\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~13\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[151]~203_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[53]~61_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[152]~202_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~12_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[152]~178_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[150]~204_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~6_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[149]~181_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~4_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[148]~182_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[54]~60_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[52]~42_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[51]~63_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[50]~44_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[147]~207_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[49]~65_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[62]~66_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[61]~47_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[60]~48_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[59]~49_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~2_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[147]~183_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[1]~12_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[58]~70_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[122]~176_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[146]~208_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[48]~71_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[57]~72_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[70]~73_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[60]~68_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[69]~74_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[50]~64_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[59]~69_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[68]~75_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[67]~56_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[48]~51_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[1]~14_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[66]~77_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[133]~185_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[121]~186_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[121]~187_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_12_result_int[1]~20_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[133]~188_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|add_sub_13_result_int[2]~18_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[145]~189_combout\ : std_logic;
SIGNAL \Mod2|auto_generated|divider|divider|StageOut[145]~219_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_7_result_int[0]~16_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|StageOut[65]~79_combout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_9_result_int[2]~1_cout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_9_result_int[3]~3_cout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~5_cout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~7_cout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_9_result_int[6]~9_cout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\ : std_logic;
SIGNAL \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ : std_logic;
SIGNAL \dec2|WideOr6~0_combout\ : std_logic;
SIGNAL \dec2|WideOr5~0_combout\ : std_logic;
SIGNAL \dec2|WideOr4~0_combout\ : std_logic;
SIGNAL \dec2|WideOr3~0_combout\ : std_logic;
SIGNAL \dec2|WideOr2~0_combout\ : std_logic;
SIGNAL \dec2|WideOr1~0_combout\ : std_logic;
SIGNAL \dec2|WideOr0~0_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[107]~106_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[106]~109_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[105]~110_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[104]~113_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[103]~114_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[102]~116_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[101]~118_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[108]~105_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[119]~120_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[118]~169_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[117]~122_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[116]~123_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[115]~124_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[114]~173_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[113]~126_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[112]~128_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[129]~133_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[128]~134_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[127]~159_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[126]~136_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[125]~174_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[124]~175_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[123]~143_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[130]~156_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[141]~144_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[140]~145_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[139]~146_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[126]~160_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[138]~164_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[137]~148_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[136]~166_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[135]~150_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[110]~153_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[110]~152_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|StageOut[134]~177_combout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_13_result_int[3]~1_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_13_result_int[4]~3_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_13_result_int[5]~5_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_13_result_int[6]~7_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_13_result_int[7]~9_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_13_result_int[8]~11_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_13_result_int[9]~13_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\ : std_logic;
SIGNAL \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ : std_logic;
SIGNAL \dec3|WideOr6~0_combout\ : std_logic;
SIGNAL \dec3|WideOr5~0_combout\ : std_logic;
SIGNAL \dec3|WideOr4~0_combout\ : std_logic;
SIGNAL \dec3|WideOr3~0_combout\ : std_logic;
SIGNAL \dec3|WideOr2~0_combout\ : std_logic;
SIGNAL \dec3|WideOr1~0_combout\ : std_logic;
SIGNAL \dec3|WideOr0~0_combout\ : std_logic;
SIGNAL \count_0|cntr\ : std_logic_vector(25 DOWNTO 0);
SIGNAL \sec_cnt|count_second\ : std_logic_vector(13 DOWNTO 0);
SIGNAL \KEY~combout\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \SW~combout\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \dec3|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \dec2|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \dec1|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \dec0|ALT_INV_WideOr0~0_combout\ : std_logic;

BEGIN

ww_CLOCK_50 <= CLOCK_50;
ww_SW <= SW;
ww_KEY <= KEY;
HEX0 <= ww_HEX0;
HEX1 <= ww_HEX1;
HEX2 <= ww_HEX2;
HEX3 <= ww_HEX3;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\CLOCK_50~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \CLOCK_50~combout\);
\dec3|ALT_INV_WideOr0~0_combout\ <= NOT \dec3|WideOr0~0_combout\;
\dec2|ALT_INV_WideOr0~0_combout\ <= NOT \dec2|WideOr0~0_combout\;
\dec1|ALT_INV_WideOr0~0_combout\ <= NOT \dec1|WideOr0~0_combout\;
\dec0|ALT_INV_WideOr0~0_combout\ <= NOT \dec0|WideOr0~0_combout\;

-- Location: LCCOMB_X23_Y21_N0
\Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\ = \sec_cnt|count_second\(11) $ (VCC)
-- \Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\ = CARRY(\sec_cnt|count_second\(11))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(11),
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\);

-- Location: LCCOMB_X23_Y21_N4
\Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\ = (\sec_cnt|count_second\(13) & (\Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ $ (GND))) # (!\sec_cnt|count_second\(13) & 
-- (!\Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ & VCC))
-- \Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\ = CARRY((\sec_cnt|count_second\(13) & !\Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(13),
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\);

-- Location: LCCOMB_X24_Y21_N8
\Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[15]~102_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[15]~103_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[15]~102_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[15]~103_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[15]~102_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[15]~103_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\);

-- Location: LCCOMB_X24_Y21_N10
\Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[16]~101_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[16]~100_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[16]~101_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[16]~100_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[16]~101_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[16]~100_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[16]~101_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[16]~100_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\);

-- Location: LCCOMB_X24_Y21_N12
\Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[17]~98_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[17]~99_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[17]~98_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[17]~99_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[17]~98_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[17]~99_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[17]~98_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[17]~99_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\);

-- Location: LCCOMB_X25_Y21_N14
\Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[20]~108_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[20]~109_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[20]~108_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[20]~109_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[20]~108_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[20]~109_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\);

-- Location: LCCOMB_X25_Y21_N16
\Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[21]~107_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[21]~106_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[21]~107_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[21]~106_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[21]~107_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[21]~106_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[21]~107_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[21]~106_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\);

-- Location: LCCOMB_X26_Y21_N6
\Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[25]~115_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[25]~114_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[25]~115_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[25]~114_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[25]~115_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[25]~114_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~1\);

-- Location: LCCOMB_X26_Y21_N8
\Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[26]~113_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[26]~112_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[26]~113_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[26]~112_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[26]~113_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[26]~112_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[26]~113_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[26]~112_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~3\);

-- Location: LCCOMB_X25_Y20_N4
\Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[30]~120_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[30]~121_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[30]~120_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[30]~121_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[30]~120_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[30]~121_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~1\);

-- Location: LCCOMB_X25_Y20_N8
\Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[32]~117_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[32]~172_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[32]~117_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[32]~172_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[32]~117_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[32]~172_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[32]~117_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[32]~172_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~5\);

-- Location: LCCOMB_X26_Y20_N22
\Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[35]~127_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[35]~126_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[35]~127_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[35]~126_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[35]~127_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[35]~126_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~1\);

-- Location: LCCOMB_X27_Y20_N22
\Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[40]~132_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[40]~133_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[40]~132_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[40]~133_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[40]~132_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[40]~133_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~1\);

-- Location: LCCOMB_X29_Y20_N20
\Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[45]~139_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[45]~138_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[45]~139_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[45]~138_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[45]~139_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[45]~138_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~1\);

-- Location: LCCOMB_X25_Y19_N12
\Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\ = \sec_cnt|count_second\(9) $ (VCC)
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\ = CARRY(\sec_cnt|count_second\(9))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(9),
	datad => VCC,
	combout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\);

-- Location: LCCOMB_X25_Y19_N18
\Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\ = (\sec_cnt|count_second\(12) & (!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\)) # (!\sec_cnt|count_second\(12) & 
-- ((\Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\) # (GND)))
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ = CARRY((!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\) # (!\sec_cnt|count_second\(12)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(12),
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\);

-- Location: LCCOMB_X24_Y19_N0
\Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\ = (((\Mod1|auto_generated|divider|divider|StageOut[49]~154_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[49]~155_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[49]~154_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[49]~155_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[49]~154_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[49]~155_combout\,
	datad => VCC,
	combout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\);

-- Location: LCCOMB_X24_Y19_N6
\Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\ = (\Mod1|auto_generated|divider|divider|StageOut[52]~148_combout\ & (((!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\)))) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[52]~148_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[52]~149_combout\ & (!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\)) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[52]~149_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\) # (GND)))))
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ = CARRY(((!\Mod1|auto_generated|divider|divider|StageOut[52]~148_combout\ & !\Mod1|auto_generated|divider|divider|StageOut[52]~149_combout\)) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[52]~148_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[52]~149_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\);

-- Location: LCCOMB_X24_Y20_N8
\Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\ = (((\Mod1|auto_generated|divider|divider|StageOut[57]~163_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[57]~162_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[57]~163_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[57]~162_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[57]~163_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[57]~162_combout\,
	datad => VCC,
	combout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\);

-- Location: LCCOMB_X24_Y20_N16
\Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ & (((\Mod1|auto_generated|divider|divider|StageOut[61]~157_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[61]~223_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ & ((((\Mod1|auto_generated|divider|divider|StageOut[61]~157_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[61]~223_combout\)))))
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\ = CARRY((!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ & ((\Mod1|auto_generated|divider|divider|StageOut[61]~157_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[61]~223_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[61]~157_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[61]~223_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\);

-- Location: LCCOMB_X23_Y20_N14
\Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~4_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~3\ & ((((\Mod1|auto_generated|divider|divider|StageOut[67]~226_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[67]~167_combout\))))) # (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~3\ & ((\Mod1|auto_generated|divider|divider|StageOut[67]~226_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|StageOut[67]~167_combout\) # (GND))))
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~5\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[67]~226_combout\) # ((\Mod1|auto_generated|divider|divider|StageOut[67]~167_combout\) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[67]~226_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[67]~167_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~3\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~4_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~5\);

-- Location: LCCOMB_X23_Y20_N16
\Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~6_combout\ = (\Mod1|auto_generated|divider|divider|StageOut[68]~166_combout\ & (((!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~5\)))) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[68]~166_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[68]~206_combout\ & (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~5\)) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[68]~206_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~5\) # (GND)))))
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~7\ = CARRY(((!\Mod1|auto_generated|divider|divider|StageOut[68]~166_combout\ & !\Mod1|auto_generated|divider|divider|StageOut[68]~206_combout\)) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[68]~166_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[68]~206_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~5\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~6_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~7\);

-- Location: LCCOMB_X23_Y20_N18
\Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~8_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~7\ & (((\Mod1|auto_generated|divider|divider|StageOut[69]~165_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[69]~205_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~7\ & ((((\Mod1|auto_generated|divider|divider|StageOut[69]~165_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[69]~205_combout\)))))
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~9\ = CARRY((!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~7\ & ((\Mod1|auto_generated|divider|divider|StageOut[69]~165_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[69]~205_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[69]~165_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[69]~205_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~7\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~8_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~9\);

-- Location: LCCOMB_X16_Y20_N18
\Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~2_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~1\ & (((\Mod1|auto_generated|divider|divider|StageOut[74]~228_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[74]~175_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~1\ & (!\Mod1|auto_generated|divider|divider|StageOut[74]~228_combout\ & 
-- (!\Mod1|auto_generated|divider|divider|StageOut[74]~175_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~3\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[74]~228_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[74]~175_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[74]~228_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[74]~175_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~1\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~2_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~3\);

-- Location: LCCOMB_X16_Y20_N24
\Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~8_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~7\ & (((\Mod1|auto_generated|divider|divider|StageOut[77]~172_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[77]~208_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~7\ & ((((\Mod1|auto_generated|divider|divider|StageOut[77]~172_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[77]~208_combout\)))))
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~9\ = CARRY((!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~7\ & ((\Mod1|auto_generated|divider|divider|StageOut[77]~172_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[77]~208_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[77]~172_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[77]~208_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~7\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~8_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~9\);

-- Location: LCCOMB_X15_Y20_N16
\Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~0_combout\ = (((\Mod1|auto_generated|divider|divider|StageOut[81]~183_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[81]~184_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~1\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[81]~183_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[81]~184_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[81]~183_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[81]~184_combout\,
	datad => VCC,
	combout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~0_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~1\);

-- Location: LCCOMB_X15_Y20_N22
\Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~6_combout\ = (\Mod1|auto_generated|divider|divider|StageOut[84]~180_combout\ & (((!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~5\)))) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[84]~180_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[84]~212_combout\ & (!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~5\)) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[84]~212_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~5\) # (GND)))))
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~7\ = CARRY(((!\Mod1|auto_generated|divider|divider|StageOut[84]~180_combout\ & !\Mod1|auto_generated|divider|divider|StageOut[84]~212_combout\)) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[84]~180_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[84]~212_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~5\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~6_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~7\);

-- Location: LCCOMB_X15_Y20_N24
\Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~8_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~7\ & (((\Mod1|auto_generated|divider|divider|StageOut[85]~211_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[85]~179_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~7\ & ((((\Mod1|auto_generated|divider|divider|StageOut[85]~211_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[85]~179_combout\)))))
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~9\ = CARRY((!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~7\ & ((\Mod1|auto_generated|divider|divider|StageOut[85]~211_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[85]~179_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[85]~211_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[85]~179_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~7\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~8_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~9\);

-- Location: LCCOMB_X13_Y22_N6
\Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~2_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~1\ & (((\Mod1|auto_generated|divider|divider|StageOut[98]~231_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[98]~196_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~1\ & (!\Mod1|auto_generated|divider|divider|StageOut[98]~231_combout\ & 
-- (!\Mod1|auto_generated|divider|divider|StageOut[98]~196_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~3\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[98]~231_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[98]~196_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[98]~231_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[98]~196_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~1\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~2_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~3\);

-- Location: LCCOMB_X11_Y22_N6
\Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\ = (((\Mod1|auto_generated|divider|divider|StageOut[108]~221_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[108]~201_combout\)))
-- \Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[108]~221_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[108]~201_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[108]~221_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[108]~201_combout\,
	datad => VCC,
	combout => \Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\);

-- Location: LCCOMB_X11_Y22_N8
\Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\ = (\Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\ & (((\Mod1|auto_generated|divider|divider|StageOut[109]~220_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[109]~200_combout\)))) # (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\ & (!\Mod1|auto_generated|divider|divider|StageOut[109]~220_combout\ & 
-- (!\Mod1|auto_generated|divider|divider|StageOut[109]~200_combout\)))
-- \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[109]~220_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[109]~200_combout\ & 
-- !\Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[109]~220_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[109]~200_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\,
	combout => \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\);

-- Location: LCCOMB_X11_Y22_N10
\Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\ = (\Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ & (((\Mod1|auto_generated|divider|divider|StageOut[110]~219_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[110]~199_combout\)))) # (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ & ((((\Mod1|auto_generated|divider|divider|StageOut[110]~219_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[110]~199_combout\)))))
-- \Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\ = CARRY((!\Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ & ((\Mod1|auto_generated|divider|divider|StageOut[110]~219_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[110]~199_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[110]~219_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[110]~199_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\,
	combout => \Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\);

-- Location: LCCOMB_X8_Y22_N12
\Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\ = (((\Div0|auto_generated|divider|divider|StageOut[20]~65_combout\) # (\Div0|auto_generated|divider|divider|StageOut[20]~64_combout\)))
-- \Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ = CARRY((\Div0|auto_generated|divider|divider|StageOut[20]~65_combout\) # (\Div0|auto_generated|divider|divider|StageOut[20]~64_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[20]~65_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[20]~64_combout\,
	datad => VCC,
	combout => \Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\);

-- Location: LCCOMB_X8_Y22_N14
\Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ & (((\Div0|auto_generated|divider|divider|StageOut[21]~47_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[21]~48_combout\)))) # (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ & (!\Div0|auto_generated|divider|divider|StageOut[21]~47_combout\ & 
-- (!\Div0|auto_generated|divider|divider|StageOut[21]~48_combout\)))
-- \Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ = CARRY((!\Div0|auto_generated|divider|divider|StageOut[21]~47_combout\ & (!\Div0|auto_generated|divider|divider|StageOut[21]~48_combout\ & 
-- !\Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[21]~47_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[21]~48_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~1\,
	combout => \Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\);

-- Location: LCCOMB_X8_Y22_N16
\Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ & (((\Div0|auto_generated|divider|divider|StageOut[22]~46_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[22]~59_combout\)))) # (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ & ((((\Div0|auto_generated|divider|divider|StageOut[22]~46_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[22]~59_combout\)))))
-- \Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\ = CARRY((!\Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ & ((\Div0|auto_generated|divider|divider|StageOut[22]~46_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[22]~59_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[22]~46_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[22]~59_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\,
	combout => \Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\);

-- Location: LCCOMB_X21_Y19_N20
\Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\ = (\sec_cnt|count_second\(11) & (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\ $ (GND))) # (!\sec_cnt|count_second\(11) & 
-- (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\ & VCC))
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\ = CARRY((\sec_cnt|count_second\(11) & !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(11),
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\);

-- Location: LCCOMB_X21_Y19_N24
\Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\ = (\sec_cnt|count_second\(13) & (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\ $ (GND))) # (!\sec_cnt|count_second\(13) & 
-- (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\ & VCC))
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\ = CARRY((\sec_cnt|count_second\(13) & !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(13),
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\);

-- Location: LCCOMB_X19_Y19_N24
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ & (((\Mod2|auto_generated|divider|divider|StageOut[107]~128_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[107]~129_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ & ((((\Mod2|auto_generated|divider|divider|StageOut[107]~128_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[107]~129_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ & ((\Mod2|auto_generated|divider|divider|StageOut[107]~128_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[107]~129_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[107]~128_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[107]~129_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\);

-- Location: LCCOMB_X15_Y19_N16
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[2]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[100]~151_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[100]~152_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|StageOut[100]~151_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[100]~152_combout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\);

-- Location: LCCOMB_X16_Y19_N10
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\ & (((\Mod2|auto_generated|divider|divider|StageOut[113]~148_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[113]~149_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\ & (!\Mod2|auto_generated|divider|divider|StageOut[113]~148_combout\ & 
-- (!\Mod2|auto_generated|divider|divider|StageOut[113]~149_combout\)))
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ = CARRY((!\Mod2|auto_generated|divider|divider|StageOut[113]~148_combout\ & (!\Mod2|auto_generated|divider|divider|StageOut[113]~149_combout\ & 
-- !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[113]~148_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[113]~149_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\);

-- Location: LCCOMB_X16_Y19_N14
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[115]~213_combout\ & (((!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\)))) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[115]~213_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[115]~146_combout\ & (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\)) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[115]~146_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\) # (GND)))))
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ = CARRY(((!\Mod2|auto_generated|divider|divider|StageOut[115]~213_combout\ & !\Mod2|auto_generated|divider|divider|StageOut[115]~146_combout\)) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[115]~213_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[115]~146_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\);

-- Location: LCCOMB_X16_Y19_N16
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ & (((\Mod2|auto_generated|divider|divider|StageOut[116]~145_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[116]~212_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ & ((((\Mod2|auto_generated|divider|divider|StageOut[116]~145_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[116]~212_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ & ((\Mod2|auto_generated|divider|divider|StageOut[116]~145_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[116]~212_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[116]~145_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[116]~212_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\);

-- Location: LCCOMB_X13_Y19_N8
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ & (((\Mod2|auto_generated|divider|divider|StageOut[127]~157_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[127]~193_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ & ((((\Mod2|auto_generated|divider|divider|StageOut[127]~157_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[127]~193_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ & ((\Mod2|auto_generated|divider|divider|StageOut[127]~157_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[127]~193_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[127]~157_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[127]~193_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\);

-- Location: LCCOMB_X13_Y19_N12
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ & (((\Mod2|auto_generated|divider|divider|StageOut[129]~191_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[129]~155_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ & ((((\Mod2|auto_generated|divider|divider|StageOut[129]~191_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[129]~155_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ & ((\Mod2|auto_generated|divider|divider|StageOut[129]~191_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[129]~155_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[129]~191_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[129]~155_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\);

-- Location: LCCOMB_X13_Y20_N4
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~0_combout\ = (((\Mod2|auto_generated|divider|divider|StageOut[134]~177_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[134]~218_combout\)))
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~1\ = CARRY((\Mod2|auto_generated|divider|divider|StageOut[134]~177_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[134]~218_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[134]~177_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[134]~218_combout\,
	datad => VCC,
	combout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~0_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~1\);

-- Location: LCCOMB_X13_Y20_N12
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~8_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~7\ & (((\Mod2|auto_generated|divider|divider|StageOut[138]~169_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[138]~198_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~7\ & ((((\Mod2|auto_generated|divider|divider|StageOut[138]~169_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[138]~198_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~9\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~7\ & ((\Mod2|auto_generated|divider|divider|StageOut[138]~169_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[138]~198_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[138]~169_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[138]~198_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~7\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~8_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~9\);

-- Location: LCCOMB_X12_Y20_N24
\Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[151]~179_combout\ & (((!\Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\)))) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[151]~179_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[151]~203_combout\ & (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\)) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[151]~203_combout\ & ((\Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\) # (GND)))))
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ = CARRY(((!\Mod2|auto_generated|divider|divider|StageOut[151]~179_combout\ & !\Mod2|auto_generated|divider|divider|StageOut[151]~203_combout\)) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[151]~179_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[151]~203_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\,
	combout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\);

-- Location: LCCOMB_X12_Y20_N26
\Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ & (((\Mod2|auto_generated|divider|divider|StageOut[152]~202_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[152]~178_combout\)))) # (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ & ((((\Mod2|auto_generated|divider|divider|StageOut[152]~202_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[152]~178_combout\)))))
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\ = CARRY((!\Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ & ((\Mod2|auto_generated|divider|divider|StageOut[152]~202_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[152]~178_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[152]~202_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[152]~178_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\,
	combout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\);

-- Location: LCCOMB_X11_Y21_N2
\Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\ = (((\Div1|auto_generated|divider|divider|StageOut[49]~45_combout\) # (\Div1|auto_generated|divider|divider|StageOut[49]~65_combout\)))
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\ = CARRY((\Div1|auto_generated|divider|divider|StageOut[49]~45_combout\) # (\Div1|auto_generated|divider|divider|StageOut[49]~65_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[49]~45_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[49]~65_combout\,
	datad => VCC,
	combout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\);

-- Location: LCCOMB_X11_Y21_N4
\Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\ & (((\Div1|auto_generated|divider|divider|StageOut[50]~64_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[50]~44_combout\)))) # (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\ & (!\Div1|auto_generated|divider|divider|StageOut[50]~64_combout\ & 
-- (!\Div1|auto_generated|divider|divider|StageOut[50]~44_combout\)))
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\ = CARRY((!\Div1|auto_generated|divider|divider|StageOut[50]~64_combout\ & (!\Div1|auto_generated|divider|divider|StageOut[50]~44_combout\ & 
-- !\Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[50]~64_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[50]~44_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\,
	combout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\);

-- Location: LCCOMB_X11_Y21_N6
\Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\ & ((((\Div1|auto_generated|divider|divider|StageOut[51]~43_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[51]~63_combout\))))) # (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\ & ((\Div1|auto_generated|divider|divider|StageOut[51]~43_combout\) # 
-- ((\Div1|auto_generated|divider|divider|StageOut[51]~63_combout\) # (GND))))
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\ = CARRY((\Div1|auto_generated|divider|divider|StageOut[51]~43_combout\) # ((\Div1|auto_generated|divider|divider|StageOut[51]~63_combout\) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[51]~43_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[51]~63_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\,
	combout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\);

-- Location: LCCOMB_X11_Y21_N10
\Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ & (((\Div1|auto_generated|divider|divider|StageOut[53]~41_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[53]~61_combout\)))) # (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ & ((((\Div1|auto_generated|divider|divider|StageOut[53]~41_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[53]~61_combout\)))))
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\ = CARRY((!\Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ & ((\Div1|auto_generated|divider|divider|StageOut[53]~41_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[53]~61_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[53]~41_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[53]~61_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\,
	combout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\);

-- Location: LCCOMB_X12_Y19_N10
\Div1|auto_generated|divider|divider|add_sub_6_result_int[0]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[0]~14_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[146]~184_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[146]~208_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|StageOut[146]~184_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[146]~208_combout\,
	combout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[0]~14_combout\);

-- Location: LCCOMB_X9_Y21_N6
\Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\ = (((\Div1|auto_generated|divider|divider|StageOut[57]~52_combout\) # (\Div1|auto_generated|divider|divider|StageOut[57]~72_combout\)))
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ = CARRY((\Div1|auto_generated|divider|divider|StageOut[57]~52_combout\) # (\Div1|auto_generated|divider|divider|StageOut[57]~72_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[57]~52_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[57]~72_combout\,
	datad => VCC,
	combout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\);

-- Location: LCCOMB_X9_Y21_N10
\Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ & ((((\Div1|auto_generated|divider|divider|StageOut[59]~69_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[59]~49_combout\))))) # (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ & ((\Div1|auto_generated|divider|divider|StageOut[59]~69_combout\) # 
-- ((\Div1|auto_generated|divider|divider|StageOut[59]~49_combout\) # (GND))))
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\ = CARRY((\Div1|auto_generated|divider|divider|StageOut[59]~69_combout\) # ((\Div1|auto_generated|divider|divider|StageOut[59]~49_combout\) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[59]~69_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[59]~49_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\,
	combout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\);

-- Location: LCCOMB_X9_Y21_N12
\Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\ = (\Div1|auto_generated|divider|divider|StageOut[60]~68_combout\ & (((!\Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\)))) # 
-- (!\Div1|auto_generated|divider|divider|StageOut[60]~68_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[60]~48_combout\ & (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\)) # 
-- (!\Div1|auto_generated|divider|divider|StageOut[60]~48_combout\ & ((\Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\) # (GND)))))
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ = CARRY(((!\Div1|auto_generated|divider|divider|StageOut[60]~68_combout\ & !\Div1|auto_generated|divider|divider|StageOut[60]~48_combout\)) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[60]~68_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[60]~48_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\,
	combout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\);

-- Location: LCCOMB_X9_Y21_N14
\Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ & (((\Div1|auto_generated|divider|divider|StageOut[61]~67_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[61]~47_combout\)))) # (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ & ((((\Div1|auto_generated|divider|divider|StageOut[61]~67_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[61]~47_combout\)))))
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\ = CARRY((!\Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ & ((\Div1|auto_generated|divider|divider|StageOut[61]~67_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[61]~47_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[61]~67_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[61]~47_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\,
	combout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\);

-- Location: LCCOMB_X8_Y21_N2
\Div1|auto_generated|divider|divider|add_sub_8_result_int[1]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[1]~14_combout\ = (\Div1|auto_generated|divider|divider|StageOut[56]~58_combout\) # (\Div1|auto_generated|divider|divider|StageOut[56]~78_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|StageOut[56]~58_combout\,
	datad => \Div1|auto_generated|divider|divider|StageOut[56]~78_combout\,
	combout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[1]~14_combout\);

-- Location: LCCOMB_X21_Y21_N18
\Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\ = (\sec_cnt|count_second\(8) & (\Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\ & VCC)) # (!\sec_cnt|count_second\(8) & 
-- (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\))
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\ = CARRY((!\sec_cnt|count_second\(8) & !\Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(8),
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\,
	combout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\);

-- Location: LCCOMB_X21_Y21_N22
\Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\ = (\sec_cnt|count_second\(10) & (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\)) # (!\sec_cnt|count_second\(10) & 
-- ((\Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\) # (GND)))
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\) # (!\sec_cnt|count_second\(10)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(10),
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\,
	combout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\);

-- Location: LCCOMB_X21_Y21_N26
\Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\ = (\sec_cnt|count_second\(12) & (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\)) # (!\sec_cnt|count_second\(12) & 
-- ((\Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\) # (GND)))
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\) # (!\sec_cnt|count_second\(12)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(12),
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\,
	combout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\);

-- Location: LCCOMB_X20_Y21_N24
\Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\ = (\Div2|auto_generated|divider|divider|StageOut[106]~108_combout\ & (((!\Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\)))) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[106]~108_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[106]~109_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\)) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[106]~109_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\) # (GND)))))
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ = CARRY(((!\Div2|auto_generated|divider|divider|StageOut[106]~108_combout\ & !\Div2|auto_generated|divider|divider|StageOut[106]~109_combout\)) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[106]~108_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[106]~109_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\);

-- Location: LCCOMB_X18_Y20_N24
\Div2|auto_generated|divider|divider|add_sub_10_result_int[2]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\ = (\Div2|auto_generated|divider|divider|StageOut[100]~130_combout\) # (\Div2|auto_generated|divider|divider|StageOut[100]~129_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|StageOut[100]~130_combout\,
	datad => \Div2|auto_generated|divider|divider|StageOut[100]~129_combout\,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\);

-- Location: LCCOMB_X18_Y21_N8
\Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\ & (((\Div2|auto_generated|divider|divider|StageOut[113]~127_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[113]~126_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\ & (!\Div2|auto_generated|divider|divider|StageOut[113]~127_combout\ & 
-- (!\Div2|auto_generated|divider|divider|StageOut[113]~126_combout\)))
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ = CARRY((!\Div2|auto_generated|divider|divider|StageOut[113]~127_combout\ & (!\Div2|auto_generated|divider|divider|StageOut[113]~126_combout\ & 
-- !\Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[113]~127_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[113]~126_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\);

-- Location: LCCOMB_X18_Y21_N12
\Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\ = (\Div2|auto_generated|divider|divider|StageOut[115]~172_combout\ & (((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\)))) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[115]~172_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[115]~124_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\)) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[115]~124_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\) # (GND)))))
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ = CARRY(((!\Div2|auto_generated|divider|divider|StageOut[115]~172_combout\ & !\Div2|auto_generated|divider|divider|StageOut[115]~124_combout\)) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[115]~172_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[115]~124_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\);

-- Location: LCCOMB_X18_Y21_N16
\Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\ = (\Div2|auto_generated|divider|divider|StageOut[117]~170_combout\ & (((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\)))) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[117]~170_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[117]~122_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\)) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[117]~122_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\) # (GND)))))
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ = CARRY(((!\Div2|auto_generated|divider|divider|StageOut[117]~170_combout\ & !\Div2|auto_generated|divider|divider|StageOut[117]~122_combout\)) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[117]~170_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[117]~122_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\);

-- Location: LCCOMB_X18_Y21_N18
\Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ & (((\Div2|auto_generated|divider|divider|StageOut[118]~121_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[118]~169_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ & ((((\Div2|auto_generated|divider|divider|StageOut[118]~121_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[118]~169_combout\)))))
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ & ((\Div2|auto_generated|divider|divider|StageOut[118]~121_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[118]~169_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[118]~121_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[118]~169_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\);

-- Location: LCCOMB_X18_Y20_N30
\Div2|auto_generated|divider|divider|add_sub_10_result_int[1]~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\ = (\Div2|auto_generated|divider|divider|StageOut[99]~141_combout\) # (\Div2|auto_generated|divider|divider|StageOut[99]~140_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|StageOut[99]~141_combout\,
	datad => \Div2|auto_generated|divider|divider|StageOut[99]~140_combout\,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\);

-- Location: LCCOMB_X18_Y20_N12
\Div2|auto_generated|divider|divider|add_sub_11_result_int[2]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\ = (\Div2|auto_generated|divider|divider|StageOut[111]~142_combout\) # (\Div2|auto_generated|divider|divider|StageOut[111]~139_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|StageOut[111]~142_combout\,
	datad => \Div2|auto_generated|divider|divider|StageOut[111]~139_combout\,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\);

-- Location: LCCOMB_X18_Y22_N12
\Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\ = (((\Div2|auto_generated|divider|divider|StageOut[123]~176_combout\) # (\Div2|auto_generated|divider|divider|StageOut[123]~143_combout\)))
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\ = CARRY((\Div2|auto_generated|divider|divider|StageOut[123]~176_combout\) # (\Div2|auto_generated|divider|divider|StageOut[123]~143_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[123]~176_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[123]~143_combout\,
	datad => VCC,
	combout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\);

-- Location: LCCOMB_X18_Y22_N14
\Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\ & (((\Div2|auto_generated|divider|divider|StageOut[124]~138_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[124]~175_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\ & (!\Div2|auto_generated|divider|divider|StageOut[124]~138_combout\ & 
-- (!\Div2|auto_generated|divider|divider|StageOut[124]~175_combout\)))
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ = CARRY((!\Div2|auto_generated|divider|divider|StageOut[124]~138_combout\ & (!\Div2|auto_generated|divider|divider|StageOut[124]~175_combout\ & 
-- !\Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[124]~138_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[124]~175_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\,
	combout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\);

-- Location: LCCOMB_X18_Y22_N18
\Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\ = (\Div2|auto_generated|divider|divider|StageOut[126]~160_combout\ & (((!\Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\)))) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[126]~160_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[126]~136_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\)) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[126]~136_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\) # (GND)))))
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ = CARRY(((!\Div2|auto_generated|divider|divider|StageOut[126]~160_combout\ & !\Div2|auto_generated|divider|divider|StageOut[126]~136_combout\)) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[126]~160_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[126]~136_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\,
	combout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\);

-- Location: LCCOMB_X15_Y22_N22
\Div2|auto_generated|divider|divider|add_sub_12_result_int[2]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\ = (\Div2|auto_generated|divider|divider|StageOut[122]~151_combout\) # (\Div2|auto_generated|divider|divider|StageOut[122]~154_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|StageOut[122]~151_combout\,
	datad => \Div2|auto_generated|divider|divider|StageOut[122]~154_combout\,
	combout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\);

-- Location: LCFF_X30_Y19_N7
\count_0|cntr[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[0]~26_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(0));

-- Location: LCFF_X30_Y19_N17
\count_0|cntr[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[5]~37_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(5));

-- Location: LCFF_X30_Y19_N25
\count_0|cntr[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[9]~45_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(9));

-- Location: LCFF_X30_Y18_N13
\count_0|cntr[19]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[19]~65_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(19));

-- Location: LCFF_X30_Y18_N21
\count_0|cntr[23]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[23]~73_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(23));

-- Location: LCFF_X30_Y18_N25
\count_0|cntr[25]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[25]~77_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(25));

-- Location: LCCOMB_X30_Y19_N6
\count_0|cntr[0]~26\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[0]~26_combout\ = \count_0|cntr\(0) $ (VCC)
-- \count_0|cntr[0]~27\ = CARRY(\count_0|cntr\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(0),
	datad => VCC,
	combout => \count_0|cntr[0]~26_combout\,
	cout => \count_0|cntr[0]~27\);

-- Location: LCCOMB_X30_Y19_N16
\count_0|cntr[5]~37\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[5]~37_combout\ = (\count_0|cntr\(5) & (!\count_0|cntr[4]~36\)) # (!\count_0|cntr\(5) & ((\count_0|cntr[4]~36\) # (GND)))
-- \count_0|cntr[5]~38\ = CARRY((!\count_0|cntr[4]~36\) # (!\count_0|cntr\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(5),
	datad => VCC,
	cin => \count_0|cntr[4]~36\,
	combout => \count_0|cntr[5]~37_combout\,
	cout => \count_0|cntr[5]~38\);

-- Location: LCCOMB_X30_Y19_N24
\count_0|cntr[9]~45\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[9]~45_combout\ = (\count_0|cntr\(9) & (!\count_0|cntr[8]~44\)) # (!\count_0|cntr\(9) & ((\count_0|cntr[8]~44\) # (GND)))
-- \count_0|cntr[9]~46\ = CARRY((!\count_0|cntr[8]~44\) # (!\count_0|cntr\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(9),
	datad => VCC,
	cin => \count_0|cntr[8]~44\,
	combout => \count_0|cntr[9]~45_combout\,
	cout => \count_0|cntr[9]~46\);

-- Location: LCCOMB_X30_Y18_N12
\count_0|cntr[19]~65\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[19]~65_combout\ = (\count_0|cntr\(19) & (!\count_0|cntr[18]~64\)) # (!\count_0|cntr\(19) & ((\count_0|cntr[18]~64\) # (GND)))
-- \count_0|cntr[19]~66\ = CARRY((!\count_0|cntr[18]~64\) # (!\count_0|cntr\(19)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(19),
	datad => VCC,
	cin => \count_0|cntr[18]~64\,
	combout => \count_0|cntr[19]~65_combout\,
	cout => \count_0|cntr[19]~66\);

-- Location: LCCOMB_X30_Y18_N20
\count_0|cntr[23]~73\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[23]~73_combout\ = (\count_0|cntr\(23) & (!\count_0|cntr[22]~72\)) # (!\count_0|cntr\(23) & ((\count_0|cntr[22]~72\) # (GND)))
-- \count_0|cntr[23]~74\ = CARRY((!\count_0|cntr[22]~72\) # (!\count_0|cntr\(23)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(23),
	datad => VCC,
	cin => \count_0|cntr[22]~72\,
	combout => \count_0|cntr[23]~73_combout\,
	cout => \count_0|cntr[23]~74\);

-- Location: LCCOMB_X30_Y18_N22
\count_0|cntr[24]~75\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[24]~75_combout\ = (\count_0|cntr\(24) & (\count_0|cntr[23]~74\ $ (GND))) # (!\count_0|cntr\(24) & (!\count_0|cntr[23]~74\ & VCC))
-- \count_0|cntr[24]~76\ = CARRY((\count_0|cntr\(24) & !\count_0|cntr[23]~74\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(24),
	datad => VCC,
	cin => \count_0|cntr[23]~74\,
	combout => \count_0|cntr[24]~75_combout\,
	cout => \count_0|cntr[24]~76\);

-- Location: LCCOMB_X30_Y18_N24
\count_0|cntr[25]~77\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[25]~77_combout\ = \count_0|cntr\(25) $ (\count_0|cntr[24]~76\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(25),
	cin => \count_0|cntr[24]~76\,
	combout => \count_0|cntr[25]~77_combout\);

-- Location: LCCOMB_X24_Y21_N24
\Mod0|auto_generated|divider|divider|StageOut[18]~97\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[18]~97_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[18]~97_combout\);

-- Location: LCCOMB_X24_Y21_N6
\Mod0|auto_generated|divider|divider|StageOut[17]~98\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[17]~98_combout\ = (\sec_cnt|count_second\(12) & \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \sec_cnt|count_second\(12),
	datad => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[17]~98_combout\);

-- Location: LCCOMB_X24_Y21_N18
\Mod0|auto_generated|divider|divider|StageOut[16]~101\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[16]~101_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[16]~101_combout\);

-- Location: LCCOMB_X24_Y21_N20
\Mod0|auto_generated|divider|divider|StageOut[15]~102\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[15]~102_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & \sec_cnt|count_second\(10))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datad => \sec_cnt|count_second\(10),
	combout => \Mod0|auto_generated|divider|divider|StageOut[15]~102_combout\);

-- Location: LCCOMB_X25_Y21_N12
\Mod0|auto_generated|divider|divider|StageOut[23]~104\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[23]~104_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[23]~104_combout\);

-- Location: LCCOMB_X25_Y21_N10
\Mod0|auto_generated|divider|divider|StageOut[21]~107\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[21]~107_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[21]~107_combout\);

-- Location: LCCOMB_X25_Y21_N24
\Mod0|auto_generated|divider|divider|StageOut[20]~108\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[20]~108_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & \sec_cnt|count_second\(9))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \sec_cnt|count_second\(9),
	combout => \Mod0|auto_generated|divider|divider|StageOut[20]~108_combout\);

-- Location: LCCOMB_X26_Y21_N30
\Mod0|auto_generated|divider|divider|StageOut[27]~111\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[27]~111_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[27]~111_combout\);

-- Location: LCCOMB_X26_Y21_N20
\Mod0|auto_generated|divider|divider|StageOut[26]~113\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[26]~113_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[26]~113_combout\);

-- Location: LCCOMB_X26_Y21_N24
\Mod0|auto_generated|divider|divider|StageOut[25]~115\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[25]~115_combout\ = (\sec_cnt|count_second\(8) & !\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \sec_cnt|count_second\(8),
	datad => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[25]~115_combout\);

-- Location: LCCOMB_X25_Y20_N24
\Mod0|auto_generated|divider|divider|StageOut[32]~117\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[32]~117_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~2_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[32]~117_combout\);

-- Location: LCCOMB_X25_Y20_N30
\Mod0|auto_generated|divider|divider|StageOut[31]~119\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[31]~119_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~0_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~0_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[31]~119_combout\);

-- Location: LCCOMB_X25_Y20_N20
\Mod0|auto_generated|divider|divider|StageOut[30]~120\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[30]~120_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ & \sec_cnt|count_second\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(7),
	combout => \Mod0|auto_generated|divider|divider|StageOut[30]~120_combout\);

-- Location: LCCOMB_X26_Y20_N10
\Mod0|auto_generated|divider|divider|StageOut[36]~125\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[36]~125_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~0_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~0_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[36]~125_combout\);

-- Location: LCCOMB_X26_Y20_N6
\Mod0|auto_generated|divider|divider|StageOut[35]~127\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[35]~127_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\ & \sec_cnt|count_second\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	datac => \sec_cnt|count_second\(6),
	combout => \Mod0|auto_generated|divider|divider|StageOut[35]~127_combout\);

-- Location: LCCOMB_X27_Y20_N10
\Mod0|auto_generated|divider|divider|StageOut[41]~131\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[41]~131_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~0_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[41]~131_combout\);

-- Location: LCCOMB_X27_Y20_N20
\Mod0|auto_generated|divider|divider|StageOut[40]~132\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[40]~132_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ & \sec_cnt|count_second\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(5),
	combout => \Mod0|auto_generated|divider|divider|StageOut[40]~132_combout\);

-- Location: LCCOMB_X30_Y20_N16
\Mod0|auto_generated|divider|divider|StageOut[46]~137\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[46]~137_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~0_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[46]~137_combout\);

-- Location: LCCOMB_X29_Y20_N12
\Mod0|auto_generated|divider|divider|StageOut[45]~139\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[45]~139_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ & \sec_cnt|count_second\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(4),
	combout => \Mod0|auto_generated|divider|divider|StageOut[45]~139_combout\);

-- Location: LCCOMB_X30_Y20_N24
\Mod0|auto_generated|divider|divider|StageOut[51]~143\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[51]~143_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~0_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~0_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[51]~143_combout\);

-- Location: LCCOMB_X30_Y20_N20
\Mod0|auto_generated|divider|divider|StageOut[50]~145\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[50]~145_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ & \sec_cnt|count_second\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(3),
	combout => \Mod0|auto_generated|divider|divider|StageOut[50]~145_combout\);

-- Location: LCCOMB_X31_Y20_N16
\Mod0|auto_generated|divider|divider|StageOut[56]~148\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[56]~148_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ & \sec_cnt|count_second\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(3),
	combout => \Mod0|auto_generated|divider|divider|StageOut[56]~148_combout\);

-- Location: LCCOMB_X31_Y20_N6
\Mod0|auto_generated|divider|divider|StageOut[55]~151\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[55]~151_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ & \sec_cnt|count_second\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	datac => \sec_cnt|count_second\(2),
	combout => \Mod0|auto_generated|divider|divider|StageOut[55]~151_combout\);

-- Location: LCCOMB_X32_Y19_N10
\Mod0|auto_generated|divider|divider|StageOut[60]~153\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[60]~153_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ & \sec_cnt|count_second\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(1),
	combout => \Mod0|auto_generated|divider|divider|StageOut[60]~153_combout\);

-- Location: LCCOMB_X32_Y19_N6
\Mod0|auto_generated|divider|divider|StageOut[61]~156\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[61]~156_combout\ = (\sec_cnt|count_second\(2) & \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(2),
	datac => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[61]~156_combout\);

-- Location: LCCOMB_X25_Y19_N24
\Mod1|auto_generated|divider|divider|StageOut[54]~144\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[54]~144_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \sec_cnt|count_second\(13))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \sec_cnt|count_second\(13),
	combout => \Mod1|auto_generated|divider|divider|StageOut[54]~144_combout\);

-- Location: LCCOMB_X25_Y19_N4
\Mod1|auto_generated|divider|divider|StageOut[53]~147\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[53]~147_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[53]~147_combout\);

-- Location: LCCOMB_X24_Y19_N20
\Mod1|auto_generated|divider|divider|StageOut[52]~148\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[52]~148_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \sec_cnt|count_second\(11))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \sec_cnt|count_second\(11),
	combout => \Mod1|auto_generated|divider|divider|StageOut[52]~148_combout\);

-- Location: LCCOMB_X24_Y19_N24
\Mod1|auto_generated|divider|divider|StageOut[51]~150\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[51]~150_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \sec_cnt|count_second\(10))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \sec_cnt|count_second\(10),
	combout => \Mod1|auto_generated|divider|divider|StageOut[51]~150_combout\);

-- Location: LCCOMB_X25_Y19_N2
\Mod1|auto_generated|divider|divider|StageOut[50]~152\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[50]~152_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \sec_cnt|count_second\(9))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \sec_cnt|count_second\(9),
	combout => \Mod1|auto_generated|divider|divider|StageOut[50]~152_combout\);

-- Location: LCCOMB_X24_Y19_N16
\Mod1|auto_generated|divider|divider|StageOut[49]~154\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[49]~154_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \sec_cnt|count_second\(8))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \sec_cnt|count_second\(8),
	combout => \Mod1|auto_generated|divider|divider|StageOut[49]~154_combout\);

-- Location: LCCOMB_X24_Y20_N28
\Mod1|auto_generated|divider|divider|StageOut[61]~157\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[61]~157_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[61]~157_combout\);

-- Location: LCCOMB_X23_Y20_N0
\Mod1|auto_generated|divider|divider|StageOut[60]~158\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[60]~158_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[60]~158_combout\);

-- Location: LCCOMB_X24_Y20_N6
\Mod1|auto_generated|divider|divider|StageOut[58]~161\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[58]~161_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[58]~161_combout\);

-- Location: LCCOMB_X24_Y20_N24
\Mod1|auto_generated|divider|divider|StageOut[57]~163\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[57]~163_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & \sec_cnt|count_second\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datad => \sec_cnt|count_second\(7),
	combout => \Mod1|auto_generated|divider|divider|StageOut[57]~163_combout\);

-- Location: LCCOMB_X23_Y20_N6
\Mod1|auto_generated|divider|divider|StageOut[70]~164\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[70]~164_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[70]~164_combout\);

-- Location: LCCOMB_X23_Y20_N24
\Mod1|auto_generated|divider|divider|StageOut[69]~165\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[69]~165_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[69]~165_combout\);

-- Location: LCCOMB_X23_Y20_N30
\Mod1|auto_generated|divider|divider|StageOut[68]~166\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[68]~166_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[68]~166_combout\);

-- Location: LCCOMB_X22_Y20_N12
\Mod1|auto_generated|divider|divider|StageOut[66]~168\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[66]~168_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[66]~168_combout\);

-- Location: LCCOMB_X22_Y20_N14
\Mod1|auto_generated|divider|divider|StageOut[65]~169\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[65]~169_combout\ = (\sec_cnt|count_second\(6) & \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(6),
	datad => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[65]~169_combout\);

-- Location: LCCOMB_X22_Y20_N10
\Mod1|auto_generated|divider|divider|StageOut[78]~171\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[78]~171_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~8_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~8_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[78]~171_combout\);

-- Location: LCCOMB_X16_Y20_N12
\Mod1|auto_generated|divider|divider|StageOut[77]~172\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[77]~172_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~6_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[77]~172_combout\);

-- Location: LCCOMB_X16_Y20_N8
\Mod1|auto_generated|divider|divider|StageOut[75]~174\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[75]~174_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~2_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[75]~174_combout\);

-- Location: LCCOMB_X16_Y20_N6
\Mod1|auto_generated|divider|divider|StageOut[73]~176\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[73]~176_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & \sec_cnt|count_second\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \sec_cnt|count_second\(5),
	combout => \Mod1|auto_generated|divider|divider|StageOut[73]~176_combout\);

-- Location: LCCOMB_X14_Y20_N0
\Mod1|auto_generated|divider|divider|StageOut[86]~178\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[86]~178_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~8_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[86]~178_combout\);

-- Location: LCCOMB_X15_Y20_N8
\Mod1|auto_generated|divider|divider|StageOut[84]~180\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[84]~180_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~4_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[84]~180_combout\);

-- Location: LCCOMB_X15_Y20_N10
\Mod1|auto_generated|divider|divider|StageOut[83]~181\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[83]~181_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~2_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[83]~181_combout\);

-- Location: LCCOMB_X14_Y20_N14
\Mod1|auto_generated|divider|divider|StageOut[81]~183\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[81]~183_combout\ = (\sec_cnt|count_second\(4) & \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(4),
	datac => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[81]~183_combout\);

-- Location: LCCOMB_X14_Y21_N0
\Mod1|auto_generated|divider|divider|StageOut[94]~185\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[94]~185_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~8_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~8_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[94]~185_combout\);

-- Location: LCCOMB_X14_Y22_N14
\Mod1|auto_generated|divider|divider|StageOut[93]~186\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[93]~186_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~6_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[93]~186_combout\);

-- Location: LCCOMB_X14_Y22_N22
\Mod1|auto_generated|divider|divider|StageOut[91]~188\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[91]~188_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~2_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[91]~188_combout\);

-- Location: LCCOMB_X14_Y20_N10
\Mod1|auto_generated|divider|divider|StageOut[90]~189\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[90]~189_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~0_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~0_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[90]~189_combout\);

-- Location: LCCOMB_X14_Y22_N16
\Mod1|auto_generated|divider|divider|StageOut[89]~190\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[89]~190_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & \sec_cnt|count_second\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	datad => \sec_cnt|count_second\(3),
	combout => \Mod1|auto_generated|divider|divider|StageOut[89]~190_combout\);

-- Location: LCCOMB_X13_Y22_N20
\Mod1|auto_generated|divider|divider|StageOut[101]~193\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[101]~193_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~6_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[101]~193_combout\);

-- Location: LCCOMB_X13_Y22_N18
\Mod1|auto_generated|divider|divider|StageOut[100]~194\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[100]~194_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~4_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[100]~194_combout\);

-- Location: LCCOMB_X13_Y22_N30
\Mod1|auto_generated|divider|divider|StageOut[99]~195\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[99]~195_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~2_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[99]~195_combout\);

-- Location: LCCOMB_X13_Y22_N24
\Mod1|auto_generated|divider|divider|StageOut[97]~198\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[97]~198_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & \sec_cnt|count_second\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datad => \sec_cnt|count_second\(2),
	combout => \Mod1|auto_generated|divider|divider|StageOut[97]~198_combout\);

-- Location: LCCOMB_X11_Y22_N26
\Div0|auto_generated|divider|divider|StageOut[18]~42\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[18]~42_combout\ = (\Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\ & !\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[18]~42_combout\);

-- Location: LCCOMB_X11_Y22_N4
\Div0|auto_generated|divider|divider|StageOut[17]~43\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[17]~43_combout\ = (\Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\ & !\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[17]~43_combout\);

-- Location: LCCOMB_X9_Y22_N8
\Div0|auto_generated|divider|divider|StageOut[22]~46\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[22]~46_combout\ = (!\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[22]~46_combout\);

-- Location: LCCOMB_X8_Y22_N0
\Div0|auto_generated|divider|divider|StageOut[28]~49\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[28]~49_combout\ = (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & \Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[28]~49_combout\);

-- Location: LCCOMB_X8_Y22_N10
\Div0|auto_generated|divider|divider|StageOut[27]~50\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[27]~50_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\ & !\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[27]~50_combout\);

-- Location: LCCOMB_X8_Y22_N6
\Div0|auto_generated|divider|divider|StageOut[26]~52\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[26]~52_combout\ = (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & \Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[26]~52_combout\);

-- Location: LCCOMB_X20_Y19_N16
\Mod2|auto_generated|divider|divider|StageOut[108]~127\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[108]~127_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[108]~127_combout\);

-- Location: LCCOMB_X20_Y19_N26
\Mod2|auto_generated|divider|divider|StageOut[107]~128\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[107]~128_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(12))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \sec_cnt|count_second\(12),
	combout => \Mod2|auto_generated|divider|divider|StageOut[107]~128_combout\);

-- Location: LCCOMB_X20_Y19_N12
\Mod2|auto_generated|divider|divider|StageOut[106]~131\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[106]~131_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[106]~131_combout\);

-- Location: LCCOMB_X19_Y19_N0
\Mod2|auto_generated|divider|divider|StageOut[105]~133\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[105]~133_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[105]~133_combout\);

-- Location: LCCOMB_X19_Y19_N6
\Mod2|auto_generated|divider|divider|StageOut[104]~134\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[104]~134_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(9))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datac => \sec_cnt|count_second\(9),
	combout => \Mod2|auto_generated|divider|divider|StageOut[104]~134_combout\);

-- Location: LCCOMB_X20_Y19_N8
\Mod2|auto_generated|divider|divider|StageOut[103]~137\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[103]~137_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[103]~137_combout\);

-- Location: LCCOMB_X20_Y19_N24
\Mod2|auto_generated|divider|divider|StageOut[102]~139\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[102]~139_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[102]~139_combout\);

-- Location: LCCOMB_X19_Y19_N10
\Mod2|auto_generated|divider|divider|StageOut[101]~141\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[101]~141_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datac => \sec_cnt|count_second\(6),
	combout => \Mod2|auto_generated|divider|divider|StageOut[101]~141_combout\);

-- Location: LCCOMB_X19_Y19_N4
\Mod2|auto_generated|divider|divider|StageOut[119]~142\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[119]~142_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[119]~142_combout\);

-- Location: LCCOMB_X15_Y19_N0
\Mod2|auto_generated|divider|divider|StageOut[116]~145\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[116]~145_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[116]~145_combout\);

-- Location: LCCOMB_X16_Y19_N6
\Mod2|auto_generated|divider|divider|StageOut[114]~147\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[114]~147_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[114]~147_combout\);

-- Location: LCCOMB_X19_Y19_N2
\Mod2|auto_generated|divider|divider|StageOut[113]~148\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[113]~148_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \sec_cnt|count_second\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \sec_cnt|count_second\(6),
	combout => \Mod2|auto_generated|divider|divider|StageOut[113]~148_combout\);

-- Location: LCCOMB_X15_Y19_N2
\Mod2|auto_generated|divider|divider|StageOut[100]~151\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[100]~151_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \sec_cnt|count_second\(5),
	combout => \Mod2|auto_generated|divider|divider|StageOut[100]~151_combout\);

-- Location: LCCOMB_X15_Y19_N12
\Mod2|auto_generated|divider|divider|StageOut[100]~152\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[100]~152_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \sec_cnt|count_second\(5),
	combout => \Mod2|auto_generated|divider|divider|StageOut[100]~152_combout\);

-- Location: LCCOMB_X15_Y19_N14
\Mod2|auto_generated|divider|divider|StageOut[112]~153\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[112]~153_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[112]~153_combout\);

-- Location: LCCOMB_X12_Y19_N0
\Mod2|auto_generated|divider|divider|StageOut[128]~156\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[128]~156_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[128]~156_combout\);

-- Location: LCCOMB_X13_Y19_N20
\Mod2|auto_generated|divider|divider|StageOut[127]~157\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[127]~157_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[127]~157_combout\);

-- Location: LCCOMB_X14_Y19_N16
\Mod2|auto_generated|divider|divider|StageOut[126]~158\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[126]~158_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[126]~158_combout\);

-- Location: LCCOMB_X14_Y19_N2
\Mod2|auto_generated|divider|divider|StageOut[125]~159\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[125]~159_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[125]~159_combout\);

-- Location: LCCOMB_X13_Y19_N18
\Mod2|auto_generated|divider|divider|StageOut[141]~166\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[141]~166_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[141]~166_combout\);

-- Location: LCCOMB_X12_Y20_N0
\Mod2|auto_generated|divider|divider|StageOut[139]~168\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[139]~168_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[139]~168_combout\);

-- Location: LCCOMB_X13_Y20_N22
\Mod2|auto_generated|divider|divider|StageOut[138]~169\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[138]~169_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[138]~169_combout\);

-- Location: LCCOMB_X14_Y19_N6
\Mod2|auto_generated|divider|divider|StageOut[122]~173\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[122]~173_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \sec_cnt|count_second\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(3),
	combout => \Mod2|auto_generated|divider|divider|StageOut[122]~173_combout\);

-- Location: LCCOMB_X13_Y19_N28
\Mod2|auto_generated|divider|divider|StageOut[134]~177\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[134]~177_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[134]~177_combout\);

-- Location: LCCOMB_X12_Y20_N10
\Mod2|auto_generated|divider|divider|StageOut[151]~179\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[151]~179_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~10_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~10_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[151]~179_combout\);

-- Location: LCCOMB_X12_Y20_N16
\Mod2|auto_generated|divider|divider|StageOut[150]~180\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[150]~180_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~8_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~8_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[150]~180_combout\);

-- Location: LCCOMB_X11_Y20_N18
\Div1|auto_generated|divider|divider|StageOut[54]~40\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[54]~40_combout\ = (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[54]~40_combout\);

-- Location: LCCOMB_X10_Y21_N0
\Div1|auto_generated|divider|divider|StageOut[53]~41\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[53]~41_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\ & !\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[53]~41_combout\);

-- Location: LCCOMB_X11_Y21_N20
\Div1|auto_generated|divider|divider|StageOut[51]~43\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[51]~43_combout\ = (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[51]~43_combout\);

-- Location: LCCOMB_X11_Y21_N24
\Div1|auto_generated|divider|divider|StageOut[49]~45\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[49]~45_combout\ = (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \Div1|auto_generated|divider|divider|add_sub_6_result_int[1]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[1]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[49]~45_combout\);

-- Location: LCCOMB_X10_Y21_N26
\Div1|auto_generated|divider|divider|StageOut[62]~46\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[62]~46_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[62]~46_combout\);

-- Location: LCCOMB_X10_Y21_N2
\Div1|auto_generated|divider|divider|StageOut[58]~50\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[58]~50_combout\ = (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[58]~50_combout\);

-- Location: LCCOMB_X12_Y19_N28
\Mod2|auto_generated|divider|divider|StageOut[146]~184\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[146]~184_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~0_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~0_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[146]~184_combout\);

-- Location: LCCOMB_X8_Y23_N28
\Div1|auto_generated|divider|divider|StageOut[57]~52\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[57]~52_combout\ = (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & \Div1|auto_generated|divider|divider|add_sub_7_result_int[1]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[1]~14_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[57]~52_combout\);

-- Location: LCCOMB_X8_Y21_N12
\Div1|auto_generated|divider|divider|StageOut[70]~53\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[70]~53_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\ & !\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[70]~53_combout\);

-- Location: LCCOMB_X9_Y21_N20
\Div1|auto_generated|divider|divider|StageOut[69]~54\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[69]~54_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\ & !\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[69]~54_combout\);

-- Location: LCCOMB_X9_Y21_N4
\Div1|auto_generated|divider|divider|StageOut[68]~55\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[68]~55_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\ & !\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[68]~55_combout\);

-- Location: LCCOMB_X8_Y21_N10
\Div1|auto_generated|divider|divider|StageOut[66]~57\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[66]~57_combout\ = (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & \Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[66]~57_combout\);

-- Location: LCCOMB_X8_Y21_N14
\Div1|auto_generated|divider|divider|StageOut[56]~58\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[56]~58_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[0]~16_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[0]~16_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[56]~58_combout\);

-- Location: LCCOMB_X8_Y21_N16
\Div1|auto_generated|divider|divider|StageOut[65]~59\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[65]~59_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[1]~14_combout\ & !\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div1|auto_generated|divider|divider|add_sub_8_result_int[1]~14_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[65]~59_combout\);

-- Location: LCCOMB_X21_Y21_N4
\Div2|auto_generated|divider|divider|StageOut[108]~104\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[108]~104_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(13))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datac => \sec_cnt|count_second\(13),
	combout => \Div2|auto_generated|divider|divider|StageOut[108]~104_combout\);

-- Location: LCCOMB_X20_Y21_N10
\Div2|auto_generated|divider|divider|StageOut[107]~107\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[107]~107_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[107]~107_combout\);

-- Location: LCCOMB_X19_Y21_N8
\Div2|auto_generated|divider|divider|StageOut[106]~108\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[106]~108_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(11))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \sec_cnt|count_second\(11),
	combout => \Div2|auto_generated|divider|divider|StageOut[106]~108_combout\);

-- Location: LCCOMB_X20_Y21_N6
\Div2|auto_generated|divider|divider|StageOut[105]~111\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[105]~111_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[105]~111_combout\);

-- Location: LCCOMB_X21_Y21_N6
\Div2|auto_generated|divider|divider|StageOut[104]~112\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[104]~112_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(9))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datac => \sec_cnt|count_second\(9),
	combout => \Div2|auto_generated|divider|divider|StageOut[104]~112_combout\);

-- Location: LCCOMB_X22_Y21_N10
\Div2|auto_generated|divider|divider|StageOut[103]~115\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[103]~115_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[103]~115_combout\);

-- Location: LCCOMB_X19_Y21_N20
\Div2|auto_generated|divider|divider|StageOut[102]~117\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[102]~117_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\ & !\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[102]~117_combout\);

-- Location: LCCOMB_X20_Y21_N12
\Div2|auto_generated|divider|divider|StageOut[101]~119\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[101]~119_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \sec_cnt|count_second\(6),
	combout => \Div2|auto_generated|divider|divider|StageOut[101]~119_combout\);

-- Location: LCCOMB_X19_Y21_N4
\Div2|auto_generated|divider|divider|StageOut[118]~121\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[118]~121_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[118]~121_combout\);

-- Location: LCCOMB_X19_Y21_N16
\Div2|auto_generated|divider|divider|StageOut[114]~125\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[114]~125_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\ & !\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[114]~125_combout\);

-- Location: LCCOMB_X19_Y21_N12
\Div2|auto_generated|divider|divider|StageOut[113]~127\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[113]~127_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\ & !\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[113]~127_combout\);

-- Location: LCCOMB_X18_Y20_N26
\Div2|auto_generated|divider|divider|StageOut[100]~129\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[100]~129_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \sec_cnt|count_second\(5),
	combout => \Div2|auto_generated|divider|divider|StageOut[100]~129_combout\);

-- Location: LCCOMB_X18_Y20_N4
\Div2|auto_generated|divider|divider|StageOut[100]~130\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[100]~130_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \sec_cnt|count_second\(5),
	combout => \Div2|auto_generated|divider|divider|StageOut[100]~130_combout\);

-- Location: LCCOMB_X18_Y20_N10
\Div2|auto_generated|divider|divider|StageOut[112]~131\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[112]~131_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[112]~131_combout\);

-- Location: LCCOMB_X18_Y21_N4
\Div2|auto_generated|divider|divider|StageOut[130]~132\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[130]~132_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[130]~132_combout\);

-- Location: LCCOMB_X18_Y21_N28
\Div2|auto_generated|divider|divider|StageOut[127]~135\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[127]~135_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[127]~135_combout\);

-- Location: LCCOMB_X18_Y22_N8
\Div2|auto_generated|divider|divider|StageOut[125]~137\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[125]~137_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[125]~137_combout\);

-- Location: LCCOMB_X18_Y20_N8
\Div2|auto_generated|divider|divider|StageOut[124]~138\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[124]~138_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[124]~138_combout\);

-- Location: LCCOMB_X18_Y20_N22
\Div2|auto_generated|divider|divider|StageOut[111]~139\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[111]~139_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \sec_cnt|count_second\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(4),
	combout => \Div2|auto_generated|divider|divider|StageOut[111]~139_combout\);

-- Location: LCCOMB_X18_Y20_N28
\Div2|auto_generated|divider|divider|StageOut[99]~140\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[99]~140_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \sec_cnt|count_second\(4),
	combout => \Div2|auto_generated|divider|divider|StageOut[99]~140_combout\);

-- Location: LCCOMB_X18_Y20_N14
\Div2|auto_generated|divider|divider|StageOut[99]~141\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[99]~141_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \sec_cnt|count_second\(4),
	combout => \Div2|auto_generated|divider|divider|StageOut[99]~141_combout\);

-- Location: LCCOMB_X18_Y20_N20
\Div2|auto_generated|divider|divider|StageOut[111]~142\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[111]~142_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\ & !\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[111]~142_combout\);

-- Location: LCCOMB_X19_Y22_N20
\Div2|auto_generated|divider|divider|StageOut[138]~147\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[138]~147_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\ & !\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[138]~147_combout\);

-- Location: LCCOMB_X19_Y22_N24
\Div2|auto_generated|divider|divider|StageOut[136]~149\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[136]~149_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\ & !\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[136]~149_combout\);

-- Location: LCCOMB_X15_Y22_N16
\Div2|auto_generated|divider|divider|StageOut[122]~151\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[122]~151_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \sec_cnt|count_second\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(3),
	combout => \Div2|auto_generated|divider|divider|StageOut[122]~151_combout\);

-- Location: LCCOMB_X15_Y22_N10
\Div2|auto_generated|divider|divider|StageOut[122]~154\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[122]~154_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[122]~154_combout\);

-- Location: LCCOMB_X15_Y22_N12
\Div2|auto_generated|divider|divider|StageOut[134]~155\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[134]~155_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\ & !\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[134]~155_combout\);

-- Location: LCCOMB_X32_Y19_N26
\sec_cnt|Equal0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|Equal0~1_combout\ = (\sec_cnt|count_second\(7)) # (((\sec_cnt|count_second\(6)) # (!\sec_cnt|count_second\(9))) # (!\sec_cnt|count_second\(8)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(7),
	datab => \sec_cnt|count_second\(8),
	datac => \sec_cnt|count_second\(6),
	datad => \sec_cnt|count_second\(9),
	combout => \sec_cnt|Equal0~1_combout\);

-- Location: LCCOMB_X29_Y19_N14
\count_0|Equal0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|Equal0~2_combout\ = (\count_0|cntr\(10)) # ((\count_0|cntr\(11)) # ((\count_0|cntr\(9)) # (!\count_0|cntr\(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(10),
	datab => \count_0|cntr\(11),
	datac => \count_0|cntr\(8),
	datad => \count_0|cntr\(9),
	combout => \count_0|Equal0~2_combout\);

-- Location: LCCOMB_X25_Y21_N0
\Mod0|auto_generated|divider|divider|StageOut[28]~161\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[28]~161_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[22]~170_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Mod0|auto_generated|divider|divider|StageOut[22]~170_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[28]~161_combout\);

-- Location: LCCOMB_X26_Y21_N4
\Mod0|auto_generated|divider|divider|StageOut[33]~162\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[33]~162_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[27]~171_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[27]~171_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[33]~162_combout\);

-- Location: LCCOMB_X25_Y20_N0
\Mod0|auto_generated|divider|divider|StageOut[38]~163\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[38]~163_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[32]~172_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~2_combout\,
	datac => \Mod0|auto_generated|divider|divider|StageOut[32]~172_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[38]~163_combout\);

-- Location: LCCOMB_X26_Y20_N12
\Mod0|auto_generated|divider|divider|StageOut[43]~164\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[43]~164_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[37]~173_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[37]~173_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~2_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[43]~164_combout\);

-- Location: LCCOMB_X27_Y20_N2
\Mod0|auto_generated|divider|divider|StageOut[48]~165\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[48]~165_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[42]~174_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[42]~174_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~2_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[48]~165_combout\);

-- Location: LCCOMB_X29_Y20_N14
\Mod0|auto_generated|divider|divider|StageOut[53]~166\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[53]~166_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[47]~175_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[47]~175_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~2_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[53]~166_combout\);

-- Location: LCCOMB_X30_Y20_N30
\Mod0|auto_generated|divider|divider|StageOut[58]~167\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[58]~167_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[52]~176_combout\) # 
-- ((!\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~2_combout\,
	datad => \Mod0|auto_generated|divider|divider|StageOut[52]~176_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[58]~167_combout\);

-- Location: LCCOMB_X31_Y20_N8
\Mod0|auto_generated|divider|divider|StageOut[63]~168\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[63]~168_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[57]~177_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[57]~177_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~2_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[63]~168_combout\);

-- Location: LCCOMB_X23_Y20_N26
\Mod1|auto_generated|divider|divider|StageOut[76]~209\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[76]~209_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[67]~226_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[67]~226_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[76]~209_combout\);

-- Location: LCCOMB_X16_Y20_N4
\Mod1|auto_generated|divider|divider|StageOut[85]~211\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[85]~211_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[76]~209_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[76]~209_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~4_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[85]~211_combout\);

-- Location: LCCOMB_X14_Y22_N26
\Mod1|auto_generated|divider|divider|StageOut[102]~216\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[102]~216_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[93]~214_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~6_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[5]~6_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[93]~214_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[102]~216_combout\);

-- Location: LCCOMB_X11_Y22_N22
\Div0|auto_generated|divider|divider|StageOut[23]~58\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[23]~58_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\Div0|auto_generated|divider|divider|StageOut[17]~54_combout\) # 
-- ((\Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\ & !\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[17]~54_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[23]~58_combout\);

-- Location: LCCOMB_X16_Y19_N2
\Mod2|auto_generated|divider|divider|StageOut[130]~190\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[130]~190_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[118]~210_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[118]~210_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[130]~190_combout\);

-- Location: LCCOMB_X12_Y19_N2
\Mod2|auto_generated|divider|divider|StageOut[140]~196\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[140]~196_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[128]~192_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[128]~192_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[140]~196_combout\);

-- Location: LCCOMB_X14_Y19_N22
\Mod2|auto_generated|divider|divider|StageOut[137]~199\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[137]~199_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[125]~215_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[125]~215_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[137]~199_combout\);

-- Location: LCCOMB_X14_Y19_N20
\Mod2|auto_generated|divider|divider|StageOut[136]~200\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[136]~200_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[124]~216_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[124]~216_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[136]~200_combout\);

-- Location: LCCOMB_X12_Y19_N12
\Mod2|auto_generated|divider|divider|StageOut[135]~201\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[135]~201_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[123]~217_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[123]~217_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[135]~201_combout\);

-- Location: LCCOMB_X13_Y20_N24
\Mod2|auto_generated|divider|divider|StageOut[149]~205\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[149]~205_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[137]~199_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[137]~199_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[149]~205_combout\);

-- Location: LCCOMB_X13_Y20_N26
\Mod2|auto_generated|divider|divider|StageOut[148]~206\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[148]~206_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[136]~200_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[136]~200_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[148]~206_combout\);

-- Location: LCCOMB_X12_Y20_N6
\Div1|auto_generated|divider|divider|StageOut[52]~62\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[52]~62_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[150]~204_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~8_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[150]~204_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~8_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[52]~62_combout\);

-- Location: LCCOMB_X11_Y21_N26
\Div1|auto_generated|divider|divider|StageOut[61]~67\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[61]~67_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[52]~62_combout\) # 
-- ((!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\,
	datad => \Div1|auto_generated|divider|divider|StageOut[52]~62_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[61]~67_combout\);

-- Location: LCCOMB_X9_Y21_N2
\Div1|auto_generated|divider|divider|StageOut[67]~76\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[67]~76_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[58]~70_combout\) # 
-- ((!\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[58]~70_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[67]~76_combout\);

-- Location: LCCOMB_X8_Y21_N6
\Div1|auto_generated|divider|divider|StageOut[56]~78\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[56]~78_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[145]~219_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_13_result_int[2]~18_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[145]~219_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[2]~18_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[56]~78_combout\);

-- Location: LCCOMB_X18_Y21_N0
\Div2|auto_generated|divider|divider|StageOut[129]~157\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[129]~157_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[117]~170_combout\) # 
-- ((!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\,
	datad => \Div2|auto_generated|divider|divider|StageOut[117]~170_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[129]~157_combout\);

-- Location: LCCOMB_X18_Y21_N26
\Div2|auto_generated|divider|divider|StageOut[128]~158\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[128]~158_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[116]~171_combout\) # 
-- ((\Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\ & !\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|StageOut[116]~171_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[128]~158_combout\);

-- Location: LCCOMB_X18_Y22_N10
\Div2|auto_generated|divider|divider|StageOut[141]~161\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[141]~161_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[129]~157_combout\) # 
-- ((\Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|StageOut[129]~157_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[141]~161_combout\);

-- Location: LCCOMB_X18_Y22_N0
\Div2|auto_generated|divider|divider|StageOut[140]~162\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[140]~162_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[128]~158_combout\) # 
-- ((\Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|StageOut[128]~158_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[140]~162_combout\);

-- Location: LCCOMB_X19_Y21_N24
\Div2|auto_generated|divider|divider|StageOut[139]~163\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[139]~163_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[127]~159_combout\) # 
-- ((\Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|StageOut[127]~159_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[139]~163_combout\);

-- Location: LCCOMB_X18_Y22_N30
\Div2|auto_generated|divider|divider|StageOut[137]~165\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[137]~165_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[125]~174_combout\) # 
-- ((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~2_combout\,
	datad => \Div2|auto_generated|divider|divider|StageOut[125]~174_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[137]~165_combout\);

-- Location: LCCOMB_X18_Y20_N6
\Div2|auto_generated|divider|divider|StageOut[135]~167\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[135]~167_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[123]~176_combout\) # 
-- ((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|StageOut[123]~176_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[135]~167_combout\);

-- Location: LCCOMB_X24_Y21_N30
\Mod0|auto_generated|divider|divider|StageOut[22]~170\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[22]~170_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & (\sec_cnt|count_second\(11))) # 
-- (!\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & ((\Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datab => \sec_cnt|count_second\(11),
	datac => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[22]~170_combout\);

-- Location: LCCOMB_X25_Y20_N14
\Mod0|auto_generated|divider|divider|StageOut[37]~173\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[37]~173_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ & ((\sec_cnt|count_second\(8)))) # 
-- (!\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ & (\Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[1]~0_combout\,
	datad => \sec_cnt|count_second\(8),
	combout => \Mod0|auto_generated|divider|divider|StageOut[37]~173_combout\);

-- Location: LCCOMB_X26_Y20_N14
\Mod0|auto_generated|divider|divider|StageOut[42]~174\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[42]~174_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\ & ((\sec_cnt|count_second\(7)))) # 
-- (!\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\ & (\Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~0_combout\,
	datad => \sec_cnt|count_second\(7),
	combout => \Mod0|auto_generated|divider|divider|StageOut[42]~174_combout\);

-- Location: LCCOMB_X27_Y20_N8
\Mod0|auto_generated|divider|divider|StageOut[47]~175\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[47]~175_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ & (\sec_cnt|count_second\(6))) # 
-- (!\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(6),
	datab => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~0_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[47]~175_combout\);

-- Location: LCCOMB_X30_Y20_N12
\Mod0|auto_generated|divider|divider|StageOut[52]~176\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[52]~176_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ & ((\sec_cnt|count_second\(5)))) # 
-- (!\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ & (\Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~0_combout\,
	datac => \sec_cnt|count_second\(5),
	datad => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[52]~176_combout\);

-- Location: LCCOMB_X30_Y20_N14
\Mod0|auto_generated|divider|divider|StageOut[57]~177\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[57]~177_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ & ((\sec_cnt|count_second\(4)))) # 
-- (!\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ & (\Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~0_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	datac => \sec_cnt|count_second\(4),
	datad => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[57]~177_combout\);

-- Location: LCCOMB_X25_Y19_N26
\Mod1|auto_generated|divider|divider|StageOut[62]~222\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[62]~222_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\sec_cnt|count_second\(12)))) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\,
	datac => \sec_cnt|count_second\(12),
	datad => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[62]~222_combout\);

-- Location: LCCOMB_X24_Y20_N26
\Mod1|auto_generated|divider|divider|StageOut[67]~226\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[67]~226_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\sec_cnt|count_second\(8)))) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datad => \sec_cnt|count_second\(8),
	combout => \Mod1|auto_generated|divider|divider|StageOut[67]~226_combout\);

-- Location: LCCOMB_X15_Y20_N14
\Mod1|auto_generated|divider|divider|StageOut[74]~228\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[74]~228_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & \sec_cnt|count_second\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \sec_cnt|count_second\(6),
	combout => \Mod1|auto_generated|divider|divider|StageOut[74]~228_combout\);

-- Location: LCCOMB_X16_Y20_N2
\Mod1|auto_generated|divider|divider|StageOut[82]~229\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[82]~229_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & \sec_cnt|count_second\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datad => \sec_cnt|count_second\(5),
	combout => \Mod1|auto_generated|divider|divider|StageOut[82]~229_combout\);

-- Location: LCCOMB_X12_Y22_N0
\Mod1|auto_generated|divider|divider|StageOut[98]~231\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[98]~231_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & \sec_cnt|count_second\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datad => \sec_cnt|count_second\(3),
	combout => \Mod1|auto_generated|divider|divider|StageOut[98]~231_combout\);

-- Location: LCCOMB_X9_Y22_N16
\Div0|auto_generated|divider|divider|StageOut[25]~63\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[25]~63_combout\ = (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & ((\sec_cnt|count_second\(1)))) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & (\Mod1|auto_generated|divider|divider|add_sub_13_result_int[1]~14_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[1]~14_combout\,
	datad => \sec_cnt|count_second\(1),
	combout => \Div0|auto_generated|divider|divider|StageOut[25]~63_combout\);

-- Location: LCCOMB_X20_Y19_N4
\Mod2|auto_generated|divider|divider|StageOut[118]~210\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[118]~210_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\sec_cnt|count_second\(11)))) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\,
	datab => \sec_cnt|count_second\(11),
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[118]~210_combout\);

-- Location: LCCOMB_X15_Y19_N28
\Mod2|auto_generated|divider|divider|StageOut[124]~216\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[124]~216_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\sec_cnt|count_second\(5)))) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(5),
	combout => \Mod2|auto_generated|divider|divider|StageOut[124]~216_combout\);

-- Location: LCCOMB_X15_Y19_N10
\Mod2|auto_generated|divider|divider|StageOut[123]~217\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[123]~217_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & (\sec_cnt|count_second\(4))) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(4),
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[123]~217_combout\);

-- Location: LCCOMB_X21_Y21_N0
\Div2|auto_generated|divider|divider|StageOut[119]~168\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[119]~168_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\sec_cnt|count_second\(12)))) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & (\Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(12),
	combout => \Div2|auto_generated|divider|divider|StageOut[119]~168_combout\);

-- Location: LCCOMB_X21_Y21_N10
\Div2|auto_generated|divider|divider|StageOut[117]~170\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[117]~170_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & (\sec_cnt|count_second\(10))) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datac => \sec_cnt|count_second\(10),
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[117]~170_combout\);

-- Location: LCCOMB_X21_Y21_N12
\Div2|auto_generated|divider|divider|StageOut[116]~171\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[116]~171_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & (\sec_cnt|count_second\(9))) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datac => \sec_cnt|count_second\(9),
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[116]~171_combout\);

-- Location: LCCOMB_X21_Y21_N2
\Div2|auto_generated|divider|divider|StageOut[115]~172\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[115]~172_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & (\sec_cnt|count_second\(8))) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datac => \sec_cnt|count_second\(8),
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[115]~172_combout\);

-- Location: LCCOMB_X18_Y20_N2
\Div2|auto_generated|divider|divider|StageOut[123]~176\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[123]~176_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\sec_cnt|count_second\(4)))) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & (\Div2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(4),
	combout => \Div2|auto_generated|divider|divider|StageOut[123]~176_combout\);

-- Location: PIN_L1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\CLOCK_50~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_CLOCK_50,
	combout => \CLOCK_50~combout\);

-- Location: PIN_L22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(0),
	combout => \SW~combout\(0));

-- Location: CLKCTRL_G2
\CLOCK_50~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLOCK_50~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLOCK_50~clkctrl_outclk\);

-- Location: LCCOMB_X31_Y19_N0
\sec_cnt|count_second[0]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[0]~14_combout\ = \sec_cnt|count_second\(0) $ (VCC)
-- \sec_cnt|count_second[0]~15\ = CARRY(\sec_cnt|count_second\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(0),
	datad => VCC,
	combout => \sec_cnt|count_second[0]~14_combout\,
	cout => \sec_cnt|count_second[0]~15\);

-- Location: PIN_R22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(0),
	combout => \KEY~combout\(0));

-- Location: LCCOMB_X31_Y19_N4
\sec_cnt|count_second[2]~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[2]~20_combout\ = (\sec_cnt|count_second\(2) & (\sec_cnt|count_second[1]~19\ $ (GND))) # (!\sec_cnt|count_second\(2) & (!\sec_cnt|count_second[1]~19\ & VCC))
-- \sec_cnt|count_second[2]~21\ = CARRY((\sec_cnt|count_second\(2) & !\sec_cnt|count_second[1]~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(2),
	datad => VCC,
	cin => \sec_cnt|count_second[1]~19\,
	combout => \sec_cnt|count_second[2]~20_combout\,
	cout => \sec_cnt|count_second[2]~21\);

-- Location: LCCOMB_X31_Y19_N6
\sec_cnt|count_second[3]~22\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[3]~22_combout\ = (\sec_cnt|count_second\(3) & (!\sec_cnt|count_second[2]~21\)) # (!\sec_cnt|count_second\(3) & ((\sec_cnt|count_second[2]~21\) # (GND)))
-- \sec_cnt|count_second[3]~23\ = CARRY((!\sec_cnt|count_second[2]~21\) # (!\sec_cnt|count_second\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(3),
	datad => VCC,
	cin => \sec_cnt|count_second[2]~21\,
	combout => \sec_cnt|count_second[3]~22_combout\,
	cout => \sec_cnt|count_second[3]~23\);

-- Location: LCCOMB_X30_Y19_N8
\count_0|cntr[1]~29\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[1]~29_combout\ = (\count_0|cntr\(1) & (!\count_0|cntr[0]~27\)) # (!\count_0|cntr\(1) & ((\count_0|cntr[0]~27\) # (GND)))
-- \count_0|cntr[1]~30\ = CARRY((!\count_0|cntr[0]~27\) # (!\count_0|cntr\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(1),
	datad => VCC,
	cin => \count_0|cntr[0]~27\,
	combout => \count_0|cntr[1]~29_combout\,
	cout => \count_0|cntr[1]~30\);

-- Location: LCCOMB_X30_Y19_N20
\count_0|cntr[7]~41\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[7]~41_combout\ = (\count_0|cntr\(7) & (!\count_0|cntr[6]~40\)) # (!\count_0|cntr\(7) & ((\count_0|cntr[6]~40\) # (GND)))
-- \count_0|cntr[7]~42\ = CARRY((!\count_0|cntr[6]~40\) # (!\count_0|cntr\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(7),
	datad => VCC,
	cin => \count_0|cntr[6]~40\,
	combout => \count_0|cntr[7]~41_combout\,
	cout => \count_0|cntr[7]~42\);

-- Location: LCFF_X30_Y19_N21
\count_0|cntr[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[7]~41_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(7));

-- Location: LCCOMB_X29_Y19_N28
\count_0|Equal0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|Equal0~1_combout\ = (\count_0|cntr\(5)) # ((\count_0|cntr\(6)) # ((\count_0|cntr\(7)) # (!\count_0|cntr\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(5),
	datab => \count_0|cntr\(6),
	datac => \count_0|cntr\(4),
	datad => \count_0|cntr\(7),
	combout => \count_0|Equal0~1_combout\);

-- Location: LCCOMB_X30_Y19_N10
\count_0|cntr[2]~31\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[2]~31_combout\ = (\count_0|cntr\(2) & (\count_0|cntr[1]~30\ $ (GND))) # (!\count_0|cntr\(2) & (!\count_0|cntr[1]~30\ & VCC))
-- \count_0|cntr[2]~32\ = CARRY((\count_0|cntr\(2) & !\count_0|cntr[1]~30\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(2),
	datad => VCC,
	cin => \count_0|cntr[1]~30\,
	combout => \count_0|cntr[2]~31_combout\,
	cout => \count_0|cntr[2]~32\);

-- Location: LCFF_X30_Y19_N11
\count_0|cntr[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[2]~31_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(2));

-- Location: LCCOMB_X30_Y19_N12
\count_0|cntr[3]~33\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[3]~33_combout\ = (\count_0|cntr\(3) & (!\count_0|cntr[2]~32\)) # (!\count_0|cntr\(3) & ((\count_0|cntr[2]~32\) # (GND)))
-- \count_0|cntr[3]~34\ = CARRY((!\count_0|cntr[2]~32\) # (!\count_0|cntr\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(3),
	datad => VCC,
	cin => \count_0|cntr[2]~32\,
	combout => \count_0|cntr[3]~33_combout\,
	cout => \count_0|cntr[3]~34\);

-- Location: LCFF_X30_Y19_N13
\count_0|cntr[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[3]~33_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(3));

-- Location: LCCOMB_X29_Y19_N30
\count_0|Equal0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|Equal0~0_combout\ = (((!\count_0|cntr\(3)) # (!\count_0|cntr\(2))) # (!\count_0|cntr\(1))) # (!\count_0|cntr\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(0),
	datab => \count_0|cntr\(1),
	datac => \count_0|cntr\(2),
	datad => \count_0|cntr\(3),
	combout => \count_0|Equal0~0_combout\);

-- Location: LCCOMB_X30_Y18_N4
\count_0|cntr[15]~57\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[15]~57_combout\ = (\count_0|cntr\(15) & (!\count_0|cntr[14]~56\)) # (!\count_0|cntr\(15) & ((\count_0|cntr[14]~56\) # (GND)))
-- \count_0|cntr[15]~58\ = CARRY((!\count_0|cntr[14]~56\) # (!\count_0|cntr\(15)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(15),
	datad => VCC,
	cin => \count_0|cntr[14]~56\,
	combout => \count_0|cntr[15]~57_combout\,
	cout => \count_0|cntr[15]~58\);

-- Location: LCFF_X30_Y18_N5
\count_0|cntr[15]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[15]~57_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(15));

-- Location: LCCOMB_X30_Y19_N0
\count_0|Equal0~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|Equal0~3_combout\ = (\count_0|cntr\(14)) # ((\count_0|cntr\(12)) # ((!\count_0|cntr\(13)) # (!\count_0|cntr\(15))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(14),
	datab => \count_0|cntr\(12),
	datac => \count_0|cntr\(15),
	datad => \count_0|cntr\(13),
	combout => \count_0|Equal0~3_combout\);

-- Location: LCCOMB_X30_Y19_N2
\count_0|Equal0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|Equal0~4_combout\ = (\count_0|Equal0~2_combout\) # ((\count_0|Equal0~1_combout\) # ((\count_0|Equal0~0_combout\) # (\count_0|Equal0~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|Equal0~2_combout\,
	datab => \count_0|Equal0~1_combout\,
	datac => \count_0|Equal0~0_combout\,
	datad => \count_0|Equal0~3_combout\,
	combout => \count_0|Equal0~4_combout\);

-- Location: LCCOMB_X30_Y19_N4
\count_0|cntr[21]~28\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[21]~28_combout\ = (((!\count_0|Equal0~7_combout\ & !\count_0|Equal0~4_combout\)) # (!\KEY~combout\(0))) # (!\SW~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011101111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(0),
	datab => \KEY~combout\(0),
	datac => \count_0|Equal0~7_combout\,
	datad => \count_0|Equal0~4_combout\,
	combout => \count_0|cntr[21]~28_combout\);

-- Location: LCFF_X30_Y19_N9
\count_0|cntr[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[1]~29_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(1));

-- Location: LCCOMB_X30_Y19_N14
\count_0|cntr[4]~35\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[4]~35_combout\ = (\count_0|cntr\(4) & (\count_0|cntr[3]~34\ $ (GND))) # (!\count_0|cntr\(4) & (!\count_0|cntr[3]~34\ & VCC))
-- \count_0|cntr[4]~36\ = CARRY((\count_0|cntr\(4) & !\count_0|cntr[3]~34\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(4),
	datad => VCC,
	cin => \count_0|cntr[3]~34\,
	combout => \count_0|cntr[4]~35_combout\,
	cout => \count_0|cntr[4]~36\);

-- Location: LCFF_X30_Y19_N15
\count_0|cntr[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[4]~35_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(4));

-- Location: LCCOMB_X30_Y19_N18
\count_0|cntr[6]~39\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[6]~39_combout\ = (\count_0|cntr\(6) & (\count_0|cntr[5]~38\ $ (GND))) # (!\count_0|cntr\(6) & (!\count_0|cntr[5]~38\ & VCC))
-- \count_0|cntr[6]~40\ = CARRY((\count_0|cntr\(6) & !\count_0|cntr[5]~38\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(6),
	datad => VCC,
	cin => \count_0|cntr[5]~38\,
	combout => \count_0|cntr[6]~39_combout\,
	cout => \count_0|cntr[6]~40\);

-- Location: LCFF_X30_Y19_N19
\count_0|cntr[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[6]~39_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(6));

-- Location: LCCOMB_X30_Y19_N22
\count_0|cntr[8]~43\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[8]~43_combout\ = (\count_0|cntr\(8) & (\count_0|cntr[7]~42\ $ (GND))) # (!\count_0|cntr\(8) & (!\count_0|cntr[7]~42\ & VCC))
-- \count_0|cntr[8]~44\ = CARRY((\count_0|cntr\(8) & !\count_0|cntr[7]~42\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(8),
	datad => VCC,
	cin => \count_0|cntr[7]~42\,
	combout => \count_0|cntr[8]~43_combout\,
	cout => \count_0|cntr[8]~44\);

-- Location: LCFF_X30_Y19_N23
\count_0|cntr[8]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[8]~43_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(8));

-- Location: LCCOMB_X30_Y19_N26
\count_0|cntr[10]~47\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[10]~47_combout\ = (\count_0|cntr\(10) & (\count_0|cntr[9]~46\ $ (GND))) # (!\count_0|cntr\(10) & (!\count_0|cntr[9]~46\ & VCC))
-- \count_0|cntr[10]~48\ = CARRY((\count_0|cntr\(10) & !\count_0|cntr[9]~46\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(10),
	datad => VCC,
	cin => \count_0|cntr[9]~46\,
	combout => \count_0|cntr[10]~47_combout\,
	cout => \count_0|cntr[10]~48\);

-- Location: LCFF_X30_Y19_N27
\count_0|cntr[10]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[10]~47_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(10));

-- Location: LCCOMB_X30_Y19_N28
\count_0|cntr[11]~49\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[11]~49_combout\ = (\count_0|cntr\(11) & (!\count_0|cntr[10]~48\)) # (!\count_0|cntr\(11) & ((\count_0|cntr[10]~48\) # (GND)))
-- \count_0|cntr[11]~50\ = CARRY((!\count_0|cntr[10]~48\) # (!\count_0|cntr\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(11),
	datad => VCC,
	cin => \count_0|cntr[10]~48\,
	combout => \count_0|cntr[11]~49_combout\,
	cout => \count_0|cntr[11]~50\);

-- Location: LCFF_X30_Y19_N29
\count_0|cntr[11]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[11]~49_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(11));

-- Location: LCCOMB_X30_Y19_N30
\count_0|cntr[12]~51\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[12]~51_combout\ = (\count_0|cntr\(12) & (\count_0|cntr[11]~50\ $ (GND))) # (!\count_0|cntr\(12) & (!\count_0|cntr[11]~50\ & VCC))
-- \count_0|cntr[12]~52\ = CARRY((\count_0|cntr\(12) & !\count_0|cntr[11]~50\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(12),
	datad => VCC,
	cin => \count_0|cntr[11]~50\,
	combout => \count_0|cntr[12]~51_combout\,
	cout => \count_0|cntr[12]~52\);

-- Location: LCFF_X30_Y19_N31
\count_0|cntr[12]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[12]~51_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(12));

-- Location: LCCOMB_X30_Y18_N0
\count_0|cntr[13]~53\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[13]~53_combout\ = (\count_0|cntr\(13) & (!\count_0|cntr[12]~52\)) # (!\count_0|cntr\(13) & ((\count_0|cntr[12]~52\) # (GND)))
-- \count_0|cntr[13]~54\ = CARRY((!\count_0|cntr[12]~52\) # (!\count_0|cntr\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(13),
	datad => VCC,
	cin => \count_0|cntr[12]~52\,
	combout => \count_0|cntr[13]~53_combout\,
	cout => \count_0|cntr[13]~54\);

-- Location: LCFF_X29_Y19_N17
\count_0|cntr[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \count_0|cntr[13]~53_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(13));

-- Location: LCCOMB_X30_Y18_N2
\count_0|cntr[14]~55\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[14]~55_combout\ = (\count_0|cntr\(14) & (\count_0|cntr[13]~54\ $ (GND))) # (!\count_0|cntr\(14) & (!\count_0|cntr[13]~54\ & VCC))
-- \count_0|cntr[14]~56\ = CARRY((\count_0|cntr\(14) & !\count_0|cntr[13]~54\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(14),
	datad => VCC,
	cin => \count_0|cntr[13]~54\,
	combout => \count_0|cntr[14]~55_combout\,
	cout => \count_0|cntr[14]~56\);

-- Location: LCFF_X30_Y18_N3
\count_0|cntr[14]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[14]~55_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(14));

-- Location: LCCOMB_X30_Y18_N6
\count_0|cntr[16]~59\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[16]~59_combout\ = (\count_0|cntr\(16) & (\count_0|cntr[15]~58\ $ (GND))) # (!\count_0|cntr\(16) & (!\count_0|cntr[15]~58\ & VCC))
-- \count_0|cntr[16]~60\ = CARRY((\count_0|cntr\(16) & !\count_0|cntr[15]~58\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(16),
	datad => VCC,
	cin => \count_0|cntr[15]~58\,
	combout => \count_0|cntr[16]~59_combout\,
	cout => \count_0|cntr[16]~60\);

-- Location: LCCOMB_X30_Y18_N8
\count_0|cntr[17]~61\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[17]~61_combout\ = (\count_0|cntr\(17) & (!\count_0|cntr[16]~60\)) # (!\count_0|cntr\(17) & ((\count_0|cntr[16]~60\) # (GND)))
-- \count_0|cntr[17]~62\ = CARRY((!\count_0|cntr[16]~60\) # (!\count_0|cntr\(17)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(17),
	datad => VCC,
	cin => \count_0|cntr[16]~60\,
	combout => \count_0|cntr[17]~61_combout\,
	cout => \count_0|cntr[17]~62\);

-- Location: LCFF_X30_Y18_N9
\count_0|cntr[17]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[17]~61_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(17));

-- Location: LCCOMB_X30_Y18_N10
\count_0|cntr[18]~63\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[18]~63_combout\ = (\count_0|cntr\(18) & (\count_0|cntr[17]~62\ $ (GND))) # (!\count_0|cntr\(18) & (!\count_0|cntr[17]~62\ & VCC))
-- \count_0|cntr[18]~64\ = CARRY((\count_0|cntr\(18) & !\count_0|cntr[17]~62\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(18),
	datad => VCC,
	cin => \count_0|cntr[17]~62\,
	combout => \count_0|cntr[18]~63_combout\,
	cout => \count_0|cntr[18]~64\);

-- Location: LCCOMB_X30_Y18_N14
\count_0|cntr[20]~67\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[20]~67_combout\ = (\count_0|cntr\(20) & (\count_0|cntr[19]~66\ $ (GND))) # (!\count_0|cntr\(20) & (!\count_0|cntr[19]~66\ & VCC))
-- \count_0|cntr[20]~68\ = CARRY((\count_0|cntr\(20) & !\count_0|cntr[19]~66\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(20),
	datad => VCC,
	cin => \count_0|cntr[19]~66\,
	combout => \count_0|cntr[20]~67_combout\,
	cout => \count_0|cntr[20]~68\);

-- Location: LCFF_X30_Y18_N15
\count_0|cntr[20]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[20]~67_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(20));

-- Location: LCCOMB_X30_Y18_N16
\count_0|cntr[21]~69\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[21]~69_combout\ = (\count_0|cntr\(21) & (!\count_0|cntr[20]~68\)) # (!\count_0|cntr\(21) & ((\count_0|cntr[20]~68\) # (GND)))
-- \count_0|cntr[21]~70\ = CARRY((!\count_0|cntr[20]~68\) # (!\count_0|cntr\(21)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(21),
	datad => VCC,
	cin => \count_0|cntr[20]~68\,
	combout => \count_0|cntr[21]~69_combout\,
	cout => \count_0|cntr[21]~70\);

-- Location: LCFF_X30_Y18_N17
\count_0|cntr[21]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[21]~69_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(21));

-- Location: LCCOMB_X30_Y18_N18
\count_0|cntr[22]~71\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|cntr[22]~71_combout\ = (\count_0|cntr\(22) & (\count_0|cntr[21]~70\ $ (GND))) # (!\count_0|cntr\(22) & (!\count_0|cntr[21]~70\ & VCC))
-- \count_0|cntr[22]~72\ = CARRY((\count_0|cntr\(22) & !\count_0|cntr[21]~70\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \count_0|cntr\(22),
	datad => VCC,
	cin => \count_0|cntr[21]~70\,
	combout => \count_0|cntr[22]~71_combout\,
	cout => \count_0|cntr[22]~72\);

-- Location: LCFF_X30_Y18_N19
\count_0|cntr[22]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[22]~71_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(22));

-- Location: LCCOMB_X30_Y18_N28
\count_0|Equal0~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|Equal0~6_combout\ = (\count_0|cntr\(23)) # ((\count_0|cntr\(20)) # ((\count_0|cntr\(21)) # (\count_0|cntr\(22))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(23),
	datab => \count_0|cntr\(20),
	datac => \count_0|cntr\(21),
	datad => \count_0|cntr\(22),
	combout => \count_0|Equal0~6_combout\);

-- Location: LCFF_X30_Y18_N23
\count_0|cntr[24]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[24]~75_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(24));

-- Location: LCFF_X30_Y18_N11
\count_0|cntr[18]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[18]~63_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(18));

-- Location: LCFF_X30_Y18_N7
\count_0|cntr[16]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \count_0|cntr[16]~59_combout\,
	sclr => \count_0|cntr[21]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \count_0|cntr\(16));

-- Location: LCCOMB_X30_Y18_N26
\count_0|Equal0~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|Equal0~5_combout\ = (\count_0|cntr\(19)) # (((!\count_0|cntr\(16)) # (!\count_0|cntr\(18))) # (!\count_0|cntr\(17)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(19),
	datab => \count_0|cntr\(17),
	datac => \count_0|cntr\(18),
	datad => \count_0|cntr\(16),
	combout => \count_0|Equal0~5_combout\);

-- Location: LCCOMB_X30_Y18_N30
\count_0|Equal0~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \count_0|Equal0~7_combout\ = (\count_0|cntr\(25)) # ((\count_0|Equal0~6_combout\) # ((\count_0|cntr\(24)) # (\count_0|Equal0~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \count_0|cntr\(25),
	datab => \count_0|Equal0~6_combout\,
	datac => \count_0|cntr\(24),
	datad => \count_0|Equal0~5_combout\,
	combout => \count_0|Equal0~7_combout\);

-- Location: LCCOMB_X31_Y19_N30
\sec_cnt|count_second[13]~17\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[13]~17_combout\ = ((!\count_0|Equal0~7_combout\ & !\count_0|Equal0~4_combout\)) # (!\KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \KEY~combout\(0),
	datac => \count_0|Equal0~7_combout\,
	datad => \count_0|Equal0~4_combout\,
	combout => \sec_cnt|count_second[13]~17_combout\);

-- Location: LCFF_X31_Y19_N7
\sec_cnt|count_second[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[3]~22_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(3));

-- Location: LCCOMB_X31_Y19_N8
\sec_cnt|count_second[4]~24\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[4]~24_combout\ = (\sec_cnt|count_second\(4) & (\sec_cnt|count_second[3]~23\ $ (GND))) # (!\sec_cnt|count_second\(4) & (!\sec_cnt|count_second[3]~23\ & VCC))
-- \sec_cnt|count_second[4]~25\ = CARRY((\sec_cnt|count_second\(4) & !\sec_cnt|count_second[3]~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(4),
	datad => VCC,
	cin => \sec_cnt|count_second[3]~23\,
	combout => \sec_cnt|count_second[4]~24_combout\,
	cout => \sec_cnt|count_second[4]~25\);

-- Location: LCFF_X31_Y19_N9
\sec_cnt|count_second[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[4]~24_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(4));

-- Location: LCCOMB_X32_Y19_N28
\sec_cnt|Equal0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|Equal0~2_combout\ = (\sec_cnt|count_second\(5)) # (((\sec_cnt|count_second\(4)) # (!\sec_cnt|count_second\(3))) # (!\sec_cnt|count_second\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(5),
	datab => \sec_cnt|count_second\(2),
	datac => \sec_cnt|count_second\(3),
	datad => \sec_cnt|count_second\(4),
	combout => \sec_cnt|Equal0~2_combout\);

-- Location: LCCOMB_X32_Y19_N14
\sec_cnt|Equal0~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|Equal0~3_combout\ = ((\sec_cnt|Equal0~2_combout\) # (!\sec_cnt|count_second\(0))) # (!\sec_cnt|count_second\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(1),
	datac => \sec_cnt|count_second\(0),
	datad => \sec_cnt|Equal0~2_combout\,
	combout => \sec_cnt|Equal0~3_combout\);

-- Location: LCCOMB_X31_Y19_N10
\sec_cnt|count_second[5]~26\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[5]~26_combout\ = (\sec_cnt|count_second\(5) & (!\sec_cnt|count_second[4]~25\)) # (!\sec_cnt|count_second\(5) & ((\sec_cnt|count_second[4]~25\) # (GND)))
-- \sec_cnt|count_second[5]~27\ = CARRY((!\sec_cnt|count_second[4]~25\) # (!\sec_cnt|count_second\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(5),
	datad => VCC,
	cin => \sec_cnt|count_second[4]~25\,
	combout => \sec_cnt|count_second[5]~26_combout\,
	cout => \sec_cnt|count_second[5]~27\);

-- Location: LCCOMB_X31_Y19_N12
\sec_cnt|count_second[6]~28\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[6]~28_combout\ = (\sec_cnt|count_second\(6) & (\sec_cnt|count_second[5]~27\ $ (GND))) # (!\sec_cnt|count_second\(6) & (!\sec_cnt|count_second[5]~27\ & VCC))
-- \sec_cnt|count_second[6]~29\ = CARRY((\sec_cnt|count_second\(6) & !\sec_cnt|count_second[5]~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(6),
	datad => VCC,
	cin => \sec_cnt|count_second[5]~27\,
	combout => \sec_cnt|count_second[6]~28_combout\,
	cout => \sec_cnt|count_second[6]~29\);

-- Location: LCCOMB_X31_Y19_N14
\sec_cnt|count_second[7]~30\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[7]~30_combout\ = (\sec_cnt|count_second\(7) & (!\sec_cnt|count_second[6]~29\)) # (!\sec_cnt|count_second\(7) & ((\sec_cnt|count_second[6]~29\) # (GND)))
-- \sec_cnt|count_second[7]~31\ = CARRY((!\sec_cnt|count_second[6]~29\) # (!\sec_cnt|count_second\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(7),
	datad => VCC,
	cin => \sec_cnt|count_second[6]~29\,
	combout => \sec_cnt|count_second[7]~30_combout\,
	cout => \sec_cnt|count_second[7]~31\);

-- Location: LCFF_X31_Y19_N15
\sec_cnt|count_second[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[7]~30_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(7));

-- Location: LCCOMB_X31_Y19_N16
\sec_cnt|count_second[8]~32\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[8]~32_combout\ = (\sec_cnt|count_second\(8) & (\sec_cnt|count_second[7]~31\ $ (GND))) # (!\sec_cnt|count_second\(8) & (!\sec_cnt|count_second[7]~31\ & VCC))
-- \sec_cnt|count_second[8]~33\ = CARRY((\sec_cnt|count_second\(8) & !\sec_cnt|count_second[7]~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(8),
	datad => VCC,
	cin => \sec_cnt|count_second[7]~31\,
	combout => \sec_cnt|count_second[8]~32_combout\,
	cout => \sec_cnt|count_second[8]~33\);

-- Location: LCCOMB_X31_Y19_N18
\sec_cnt|count_second[9]~34\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[9]~34_combout\ = (\sec_cnt|count_second\(9) & (!\sec_cnt|count_second[8]~33\)) # (!\sec_cnt|count_second\(9) & ((\sec_cnt|count_second[8]~33\) # (GND)))
-- \sec_cnt|count_second[9]~35\ = CARRY((!\sec_cnt|count_second[8]~33\) # (!\sec_cnt|count_second\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(9),
	datad => VCC,
	cin => \sec_cnt|count_second[8]~33\,
	combout => \sec_cnt|count_second[9]~34_combout\,
	cout => \sec_cnt|count_second[9]~35\);

-- Location: LCFF_X31_Y19_N19
\sec_cnt|count_second[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[9]~34_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(9));

-- Location: LCCOMB_X31_Y19_N20
\sec_cnt|count_second[10]~36\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[10]~36_combout\ = (\sec_cnt|count_second\(10) & (\sec_cnt|count_second[9]~35\ $ (GND))) # (!\sec_cnt|count_second\(10) & (!\sec_cnt|count_second[9]~35\ & VCC))
-- \sec_cnt|count_second[10]~37\ = CARRY((\sec_cnt|count_second\(10) & !\sec_cnt|count_second[9]~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(10),
	datad => VCC,
	cin => \sec_cnt|count_second[9]~35\,
	combout => \sec_cnt|count_second[10]~36_combout\,
	cout => \sec_cnt|count_second[10]~37\);

-- Location: LCFF_X31_Y19_N21
\sec_cnt|count_second[10]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[10]~36_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(10));

-- Location: LCCOMB_X31_Y19_N22
\sec_cnt|count_second[11]~38\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[11]~38_combout\ = (\sec_cnt|count_second\(11) & (!\sec_cnt|count_second[10]~37\)) # (!\sec_cnt|count_second\(11) & ((\sec_cnt|count_second[10]~37\) # (GND)))
-- \sec_cnt|count_second[11]~39\ = CARRY((!\sec_cnt|count_second[10]~37\) # (!\sec_cnt|count_second\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(11),
	datad => VCC,
	cin => \sec_cnt|count_second[10]~37\,
	combout => \sec_cnt|count_second[11]~38_combout\,
	cout => \sec_cnt|count_second[11]~39\);

-- Location: LCFF_X31_Y19_N23
\sec_cnt|count_second[11]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[11]~38_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(11));

-- Location: LCCOMB_X31_Y19_N24
\sec_cnt|count_second[12]~40\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[12]~40_combout\ = (\sec_cnt|count_second\(12) & (\sec_cnt|count_second[11]~39\ $ (GND))) # (!\sec_cnt|count_second\(12) & (!\sec_cnt|count_second[11]~39\ & VCC))
-- \sec_cnt|count_second[12]~41\ = CARRY((\sec_cnt|count_second\(12) & !\sec_cnt|count_second[11]~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(12),
	datad => VCC,
	cin => \sec_cnt|count_second[11]~39\,
	combout => \sec_cnt|count_second[12]~40_combout\,
	cout => \sec_cnt|count_second[12]~41\);

-- Location: LCFF_X31_Y19_N25
\sec_cnt|count_second[12]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[12]~40_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(12));

-- Location: LCCOMB_X32_Y19_N30
\sec_cnt|Equal0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|Equal0~0_combout\ = ((\sec_cnt|count_second\(12)) # ((\sec_cnt|count_second\(11)) # (!\sec_cnt|count_second\(10)))) # (!\sec_cnt|count_second\(13))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(13),
	datab => \sec_cnt|count_second\(12),
	datac => \sec_cnt|count_second\(10),
	datad => \sec_cnt|count_second\(11),
	combout => \sec_cnt|Equal0~0_combout\);

-- Location: LCCOMB_X31_Y19_N28
\sec_cnt|count_second[13]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[13]~16_combout\ = ((!\sec_cnt|Equal0~1_combout\ & (!\sec_cnt|Equal0~3_combout\ & !\sec_cnt|Equal0~0_combout\))) # (!\KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|Equal0~1_combout\,
	datab => \KEY~combout\(0),
	datac => \sec_cnt|Equal0~3_combout\,
	datad => \sec_cnt|Equal0~0_combout\,
	combout => \sec_cnt|count_second[13]~16_combout\);

-- Location: LCFF_X31_Y19_N1
\sec_cnt|count_second[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[0]~14_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(0));

-- Location: LCCOMB_X31_Y19_N2
\sec_cnt|count_second[1]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[1]~18_combout\ = (\sec_cnt|count_second\(1) & (!\sec_cnt|count_second[0]~15\)) # (!\sec_cnt|count_second\(1) & ((\sec_cnt|count_second[0]~15\) # (GND)))
-- \sec_cnt|count_second[1]~19\ = CARRY((!\sec_cnt|count_second[0]~15\) # (!\sec_cnt|count_second\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(1),
	datad => VCC,
	cin => \sec_cnt|count_second[0]~15\,
	combout => \sec_cnt|count_second[1]~18_combout\,
	cout => \sec_cnt|count_second[1]~19\);

-- Location: LCFF_X31_Y19_N3
\sec_cnt|count_second[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[1]~18_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(1));

-- Location: LCFF_X31_Y19_N5
\sec_cnt|count_second[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[2]~20_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(2));

-- Location: LCCOMB_X31_Y20_N28
\Mod0|auto_generated|divider|divider|StageOut[55]~150\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[55]~150_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ & \sec_cnt|count_second\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	datac => \sec_cnt|count_second\(2),
	combout => \Mod0|auto_generated|divider|divider|StageOut[55]~150_combout\);

-- Location: LCCOMB_X31_Y20_N18
\Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[55]~151_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[55]~150_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[55]~151_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[55]~150_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[55]~151_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[55]~150_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~1\);

-- Location: LCFF_X31_Y19_N11
\sec_cnt|count_second[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[5]~26_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(5));

-- Location: LCCOMB_X30_Y20_N0
\Mod0|auto_generated|divider|divider|StageOut[46]~136\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[46]~136_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ & \sec_cnt|count_second\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	datac => \sec_cnt|count_second\(5),
	combout => \Mod0|auto_generated|divider|divider|StageOut[46]~136_combout\);

-- Location: LCCOMB_X31_Y19_N26
\sec_cnt|count_second[13]~42\ : cycloneii_lcell_comb
-- Equation(s):
-- \sec_cnt|count_second[13]~42_combout\ = \sec_cnt|count_second[12]~41\ $ (\sec_cnt|count_second\(13))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \sec_cnt|count_second\(13),
	cin => \sec_cnt|count_second[12]~41\,
	combout => \sec_cnt|count_second[13]~42_combout\);

-- Location: LCFF_X31_Y19_N27
\sec_cnt|count_second[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[13]~42_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(13));

-- Location: LCCOMB_X23_Y21_N16
\Mod0|auto_generated|divider|divider|StageOut[18]~96\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[18]~96_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & \sec_cnt|count_second\(13))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datac => \sec_cnt|count_second\(13),
	combout => \Mod0|auto_generated|divider|divider|StageOut[18]~96_combout\);

-- Location: LCCOMB_X23_Y21_N2
\Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\ = (\sec_cnt|count_second\(12) & (\Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\ & VCC)) # (!\sec_cnt|count_second\(12) & 
-- (!\Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\))
-- \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ = CARRY((!\sec_cnt|count_second\(12) & !\Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(12),
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~3\);

-- Location: LCCOMB_X23_Y21_N6
\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ = !\Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\);

-- Location: LCCOMB_X24_Y21_N0
\Mod0|auto_generated|divider|divider|StageOut[17]~99\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[17]~99_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[17]~99_combout\);

-- Location: LCCOMB_X24_Y21_N22
\Mod0|auto_generated|divider|divider|StageOut[16]~100\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[16]~100_combout\ = (\sec_cnt|count_second\(11) & \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(11),
	datad => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[16]~100_combout\);

-- Location: LCCOMB_X24_Y21_N26
\Mod0|auto_generated|divider|divider|StageOut[15]~103\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[15]~103_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & \sec_cnt|count_second\(10))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datad => \sec_cnt|count_second\(10),
	combout => \Mod0|auto_generated|divider|divider|StageOut[15]~103_combout\);

-- Location: LCCOMB_X24_Y21_N14
\Mod0|auto_generated|divider|divider|add_sub_4_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[18]~97_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[18]~96_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[18]~97_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[18]~96_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\);

-- Location: LCCOMB_X24_Y21_N16
\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\);

-- Location: LCCOMB_X25_Y21_N26
\Mod0|auto_generated|divider|divider|StageOut[22]~105\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[22]~105_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[22]~105_combout\);

-- Location: LCCOMB_X25_Y21_N8
\Mod0|auto_generated|divider|divider|StageOut[21]~106\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[21]~106_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & \sec_cnt|count_second\(10))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \sec_cnt|count_second\(10),
	combout => \Mod0|auto_generated|divider|divider|StageOut[21]~106_combout\);

-- Location: LCCOMB_X25_Y21_N2
\Mod0|auto_generated|divider|divider|StageOut[20]~109\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[20]~109_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & \sec_cnt|count_second\(9))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \sec_cnt|count_second\(9),
	combout => \Mod0|auto_generated|divider|divider|StageOut[20]~109_combout\);

-- Location: LCCOMB_X25_Y21_N18
\Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[22]~170_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[22]~105_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[22]~170_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[22]~105_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[22]~170_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[22]~105_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[22]~170_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[22]~105_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\);

-- Location: LCCOMB_X24_Y21_N4
\Mod0|auto_generated|divider|divider|StageOut[23]~169\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[23]~169_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & (\sec_cnt|count_second\(12))) # 
-- (!\Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & ((\Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datac => \sec_cnt|count_second\(12),
	datad => \Mod0|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[23]~169_combout\);

-- Location: LCCOMB_X25_Y21_N20
\Mod0|auto_generated|divider|divider|add_sub_5_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[23]~104_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[23]~169_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[23]~104_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[23]~169_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\);

-- Location: LCCOMB_X25_Y21_N22
\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\);

-- Location: LCCOMB_X26_Y21_N0
\Mod0|auto_generated|divider|divider|StageOut[28]~110\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[28]~110_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[28]~110_combout\);

-- Location: LCCOMB_X25_Y21_N28
\Mod0|auto_generated|divider|divider|StageOut[27]~171\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[27]~171_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\sec_cnt|count_second\(10)))) # 
-- (!\Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & (\Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \sec_cnt|count_second\(10),
	datad => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[27]~171_combout\);

-- Location: LCCOMB_X26_Y21_N28
\Mod0|auto_generated|divider|divider|StageOut[26]~112\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[26]~112_combout\ = (\sec_cnt|count_second\(9) & \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \sec_cnt|count_second\(9),
	datad => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[26]~112_combout\);

-- Location: LCFF_X31_Y19_N17
\sec_cnt|count_second[8]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[8]~32_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(8));

-- Location: LCCOMB_X26_Y21_N2
\Mod0|auto_generated|divider|divider|StageOut[25]~114\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[25]~114_combout\ = (\sec_cnt|count_second\(8) & \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \sec_cnt|count_second\(8),
	datad => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[25]~114_combout\);

-- Location: LCCOMB_X26_Y21_N10
\Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[27]~111_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[27]~171_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[27]~111_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[27]~171_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[27]~111_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[27]~171_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[27]~111_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[27]~171_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~5\);

-- Location: LCCOMB_X26_Y21_N12
\Mod0|auto_generated|divider|divider|add_sub_6_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[28]~161_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[28]~110_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[28]~161_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[28]~110_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\);

-- Location: LCCOMB_X26_Y21_N14
\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\);

-- Location: LCCOMB_X26_Y21_N22
\Mod0|auto_generated|divider|divider|StageOut[33]~116\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[33]~116_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[3]~4_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[33]~116_combout\);

-- Location: LCCOMB_X26_Y21_N26
\Mod0|auto_generated|divider|divider|StageOut[32]~172\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[32]~172_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\sec_cnt|count_second\(9)))) # 
-- (!\Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & (\Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datac => \sec_cnt|count_second\(9),
	datad => \Mod0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[32]~172_combout\);

-- Location: LCCOMB_X25_Y20_N22
\Mod0|auto_generated|divider|divider|StageOut[31]~118\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[31]~118_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ & \sec_cnt|count_second\(8))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(8),
	combout => \Mod0|auto_generated|divider|divider|StageOut[31]~118_combout\);

-- Location: LCCOMB_X25_Y20_N26
\Mod0|auto_generated|divider|divider|StageOut[30]~121\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[30]~121_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ & \sec_cnt|count_second\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(7),
	combout => \Mod0|auto_generated|divider|divider|StageOut[30]~121_combout\);

-- Location: LCCOMB_X25_Y20_N6
\Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[31]~119_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[31]~118_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[31]~119_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[31]~118_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[31]~119_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[31]~118_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[31]~119_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[31]~118_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~3\);

-- Location: LCCOMB_X25_Y20_N10
\Mod0|auto_generated|divider|divider|add_sub_7_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_7_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[33]~162_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[33]~116_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[33]~162_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[33]~116_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[4]~7_cout\);

-- Location: LCCOMB_X25_Y20_N12
\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_7_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\);

-- Location: LCCOMB_X26_Y20_N0
\Mod0|auto_generated|divider|divider|StageOut[38]~122\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[38]~122_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~4_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[3]~4_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[38]~122_combout\);

-- Location: LCCOMB_X26_Y20_N2
\Mod0|auto_generated|divider|divider|StageOut[37]~123\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[37]~123_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[2]~2_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[37]~123_combout\);

-- Location: LCCOMB_X26_Y20_N8
\Mod0|auto_generated|divider|divider|StageOut[36]~124\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[36]~124_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\ & \sec_cnt|count_second\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(7),
	combout => \Mod0|auto_generated|divider|divider|StageOut[36]~124_combout\);

-- Location: LCFF_X31_Y19_N13
\sec_cnt|count_second[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \sec_cnt|count_second[6]~28_combout\,
	sclr => \sec_cnt|count_second[13]~16_combout\,
	ena => \sec_cnt|count_second[13]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \sec_cnt|count_second\(6));

-- Location: LCCOMB_X26_Y20_N4
\Mod0|auto_generated|divider|divider|StageOut[35]~126\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[35]~126_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\ & \sec_cnt|count_second\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_7_result_int[5]~8_combout\,
	datac => \sec_cnt|count_second\(6),
	combout => \Mod0|auto_generated|divider|divider|StageOut[35]~126_combout\);

-- Location: LCCOMB_X26_Y20_N24
\Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[36]~125_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[36]~124_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[36]~125_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[36]~124_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[36]~125_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[36]~124_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[36]~125_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[36]~124_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~3\);

-- Location: LCCOMB_X26_Y20_N26
\Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[37]~173_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[37]~123_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[37]~173_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[37]~123_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[37]~173_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[37]~123_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[37]~173_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[37]~123_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~5\);

-- Location: LCCOMB_X26_Y20_N28
\Mod0|auto_generated|divider|divider|add_sub_8_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_8_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[38]~163_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[38]~122_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[38]~163_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[38]~122_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[4]~7_cout\);

-- Location: LCCOMB_X26_Y20_N30
\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_8_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\);

-- Location: LCCOMB_X27_Y20_N0
\Mod0|auto_generated|divider|divider|StageOut[43]~128\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[43]~128_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[3]~4_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[43]~128_combout\);

-- Location: LCCOMB_X27_Y20_N14
\Mod0|auto_generated|divider|divider|StageOut[42]~129\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[42]~129_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[2]~2_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[42]~129_combout\);

-- Location: LCCOMB_X27_Y20_N4
\Mod0|auto_generated|divider|divider|StageOut[41]~130\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[41]~130_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ & \sec_cnt|count_second\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(6),
	combout => \Mod0|auto_generated|divider|divider|StageOut[41]~130_combout\);

-- Location: LCCOMB_X27_Y20_N18
\Mod0|auto_generated|divider|divider|StageOut[40]~133\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[40]~133_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\ & \sec_cnt|count_second\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_8_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(5),
	combout => \Mod0|auto_generated|divider|divider|StageOut[40]~133_combout\);

-- Location: LCCOMB_X27_Y20_N24
\Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[41]~131_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[41]~130_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[41]~131_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[41]~130_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[41]~131_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[41]~130_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[41]~131_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[41]~130_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~3\);

-- Location: LCCOMB_X27_Y20_N26
\Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[42]~174_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[42]~129_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[42]~174_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[42]~129_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[42]~174_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[42]~129_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[42]~174_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[42]~129_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~5\);

-- Location: LCCOMB_X27_Y20_N28
\Mod0|auto_generated|divider|divider|add_sub_9_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_9_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[43]~164_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[43]~128_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[43]~164_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[43]~128_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[4]~7_cout\);

-- Location: LCCOMB_X27_Y20_N30
\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_9_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\);

-- Location: LCCOMB_X29_Y20_N2
\Mod0|auto_generated|divider|divider|StageOut[45]~138\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[45]~138_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ & \sec_cnt|count_second\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(4),
	combout => \Mod0|auto_generated|divider|divider|StageOut[45]~138_combout\);

-- Location: LCCOMB_X29_Y20_N22
\Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[46]~137_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[46]~136_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[46]~137_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[46]~136_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[46]~137_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[46]~136_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[46]~137_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[46]~136_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~3\);

-- Location: LCCOMB_X27_Y20_N12
\Mod0|auto_generated|divider|divider|StageOut[48]~134\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[48]~134_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~4_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[3]~4_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[48]~134_combout\);

-- Location: LCCOMB_X29_Y20_N0
\Mod0|auto_generated|divider|divider|StageOut[47]~135\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[47]~135_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_9_result_int[2]~2_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[47]~135_combout\);

-- Location: LCCOMB_X29_Y20_N24
\Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[47]~175_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[47]~135_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[47]~175_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[47]~135_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[47]~175_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[47]~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[47]~175_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[47]~135_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~5\);

-- Location: LCCOMB_X29_Y20_N26
\Mod0|auto_generated|divider|divider|add_sub_10_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_10_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[48]~165_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[48]~134_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[48]~165_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[48]~134_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[4]~7_cout\);

-- Location: LCCOMB_X29_Y20_N28
\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_10_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\);

-- Location: LCCOMB_X30_Y20_N28
\Mod0|auto_generated|divider|divider|StageOut[52]~141\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[52]~141_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[2]~2_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[52]~141_combout\);

-- Location: LCCOMB_X30_Y20_N26
\Mod0|auto_generated|divider|divider|StageOut[51]~142\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[51]~142_combout\ = (\sec_cnt|count_second\(4) & \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \sec_cnt|count_second\(4),
	datad => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[51]~142_combout\);

-- Location: LCCOMB_X30_Y20_N22
\Mod0|auto_generated|divider|divider|StageOut[50]~144\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[50]~144_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ & \sec_cnt|count_second\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(3),
	combout => \Mod0|auto_generated|divider|divider|StageOut[50]~144_combout\);

-- Location: LCCOMB_X30_Y20_N2
\Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[50]~145_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[50]~144_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[50]~145_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[50]~144_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[50]~145_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[50]~144_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~1\);

-- Location: LCCOMB_X30_Y20_N4
\Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[51]~143_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[51]~142_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[51]~143_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[51]~142_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[51]~143_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[51]~142_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[51]~143_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[51]~142_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~3\);

-- Location: LCCOMB_X30_Y20_N6
\Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[52]~176_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[52]~141_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[52]~176_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[52]~141_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[52]~176_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[52]~141_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[52]~176_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[52]~141_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~5\);

-- Location: LCCOMB_X30_Y20_N18
\Mod0|auto_generated|divider|divider|StageOut[53]~140\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[53]~140_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~4_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[3]~4_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[53]~140_combout\);

-- Location: LCCOMB_X30_Y20_N8
\Mod0|auto_generated|divider|divider|add_sub_11_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_11_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[53]~166_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[53]~140_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[53]~166_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[53]~140_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[4]~7_cout\);

-- Location: LCCOMB_X30_Y20_N10
\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_11_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\);

-- Location: LCCOMB_X31_Y20_N0
\Mod0|auto_generated|divider|divider|StageOut[58]~146\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[58]~146_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~4_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[3]~4_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[58]~146_combout\);

-- Location: LCCOMB_X31_Y20_N14
\Mod0|auto_generated|divider|divider|StageOut[57]~147\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[57]~147_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[2]~2_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[57]~147_combout\);

-- Location: LCCOMB_X31_Y20_N30
\Mod0|auto_generated|divider|divider|StageOut[56]~149\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[56]~149_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~0_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~0_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[56]~149_combout\);

-- Location: LCCOMB_X31_Y20_N20
\Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[56]~148_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[56]~149_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[56]~148_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[56]~149_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[56]~148_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[56]~149_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[56]~148_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[56]~149_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\);

-- Location: LCCOMB_X31_Y20_N22
\Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[57]~177_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[57]~147_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[57]~177_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[57]~147_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[57]~177_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[57]~147_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[57]~177_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[57]~147_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~5\);

-- Location: LCCOMB_X31_Y20_N24
\Mod0|auto_generated|divider|divider|add_sub_12_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_12_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[58]~167_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[58]~146_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[58]~167_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[58]~146_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[4]~7_cout\);

-- Location: LCCOMB_X31_Y20_N26
\Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_12_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\);

-- Location: LCCOMB_X31_Y20_N10
\Mod0|auto_generated|divider|divider|StageOut[61]~157\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[61]~157_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~0_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~0_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[61]~157_combout\);

-- Location: LCCOMB_X32_Y19_N8
\Mod0|auto_generated|divider|divider|StageOut[60]~152\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[60]~152_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ & \sec_cnt|count_second\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\,
	datad => \sec_cnt|count_second\(1),
	combout => \Mod0|auto_generated|divider|divider|StageOut[60]~152_combout\);

-- Location: LCCOMB_X32_Y19_N16
\Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~0_combout\ = (((\Mod0|auto_generated|divider|divider|StageOut[60]~153_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[60]~152_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~1\ = CARRY((\Mod0|auto_generated|divider|divider|StageOut[60]~153_combout\) # (\Mod0|auto_generated|divider|divider|StageOut[60]~152_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[60]~153_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[60]~152_combout\,
	datad => VCC,
	combout => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~0_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~1\);

-- Location: LCCOMB_X32_Y19_N18
\Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~2_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~1\ & (((\Mod0|auto_generated|divider|divider|StageOut[61]~156_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[61]~157_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~1\ & (!\Mod0|auto_generated|divider|divider|StageOut[61]~156_combout\ & 
-- (!\Mod0|auto_generated|divider|divider|StageOut[61]~157_combout\)))
-- \Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~3\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[61]~156_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[61]~157_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[61]~156_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[61]~157_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~1\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~2_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~3\);

-- Location: LCCOMB_X32_Y19_N0
\Mod0|auto_generated|divider|divider|StageOut[63]~154\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[63]~154_combout\ = (!\Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ & \Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~4_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[63]~154_combout\);

-- Location: LCCOMB_X31_Y20_N4
\Mod0|auto_generated|divider|divider|StageOut[62]~155\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[62]~155_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~2_combout\ & !\Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~2_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[62]~155_combout\);

-- Location: LCCOMB_X32_Y19_N20
\Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~4_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~3\ & (((\Mod0|auto_generated|divider|divider|StageOut[62]~178_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[62]~155_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~3\ & ((((\Mod0|auto_generated|divider|divider|StageOut[62]~178_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[62]~155_combout\)))))
-- \Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~5\ = CARRY((!\Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~3\ & ((\Mod0|auto_generated|divider|divider|StageOut[62]~178_combout\) # 
-- (\Mod0|auto_generated|divider|divider|StageOut[62]~155_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[62]~178_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[62]~155_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~3\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~4_combout\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~5\);

-- Location: LCCOMB_X32_Y19_N22
\Mod0|auto_generated|divider|divider|add_sub_13_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_13_result_int[4]~7_cout\ = CARRY((!\Mod0|auto_generated|divider|divider|StageOut[63]~168_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[63]~154_combout\ & 
-- !\Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[63]~168_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[63]~154_combout\,
	datad => VCC,
	cin => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~5\,
	cout => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[4]~7_cout\);

-- Location: LCCOMB_X32_Y19_N24
\Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\ = \Mod0|auto_generated|divider|divider|add_sub_13_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[4]~7_cout\,
	combout => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\);

-- Location: LCCOMB_X32_Y19_N2
\Mod0|auto_generated|divider|divider|StageOut[67]~159\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[61]~156_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|StageOut[61]~157_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\ & (((\Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[61]~156_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~2_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\,
	datad => \Mod0|auto_generated|divider|divider|StageOut[61]~157_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\);

-- Location: LCCOMB_X31_Y20_N12
\Mod0|auto_generated|divider|divider|StageOut[62]~178\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[62]~178_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ & ((\sec_cnt|count_second\(3)))) # 
-- (!\Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ & (\Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\,
	datab => \Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_11_result_int[1]~0_combout\,
	datad => \sec_cnt|count_second\(3),
	combout => \Mod0|auto_generated|divider|divider|StageOut[62]~178_combout\);

-- Location: LCCOMB_X32_Y19_N4
\Mod0|auto_generated|divider|divider|StageOut[68]~160\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[62]~155_combout\) # 
-- ((\Mod0|auto_generated|divider|divider|StageOut[62]~178_combout\)))) # (!\Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\ & (((\Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[62]~155_combout\,
	datac => \Mod0|auto_generated|divider|divider|StageOut[62]~178_combout\,
	datad => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~4_combout\,
	combout => \Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\);

-- Location: LCCOMB_X32_Y19_N12
\Mod0|auto_generated|divider|divider|StageOut[66]~158\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\ = (\Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\ & ((\sec_cnt|count_second\(1)))) # (!\Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\ & 
-- (\Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\,
	datac => \Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~0_combout\,
	datad => \sec_cnt|count_second\(1),
	combout => \Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\);

-- Location: LCCOMB_X1_Y19_N16
\dec0|WideOr6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr6~0_combout\ = (\Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\ & (((\Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\)))) # (!\Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\ & 
-- (\Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\ $ (((!\Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\ & \sec_cnt|count_second\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\,
	datac => \sec_cnt|count_second\(0),
	datad => \Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\,
	combout => \dec0|WideOr6~0_combout\);

-- Location: LCCOMB_X1_Y19_N10
\dec0|WideOr5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr5~0_combout\ = (\Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\) # (\sec_cnt|count_second\(0) $ 
-- (\Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\)))) # (!\Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\ & (\Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\ & 
-- ((\Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\,
	datac => \sec_cnt|count_second\(0),
	datad => \Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\,
	combout => \dec0|WideOr5~0_combout\);

-- Location: LCCOMB_X1_Y19_N28
\dec0|WideOr4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr4~0_combout\ = (\Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\ & (\Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\)) # (!\Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\ & 
-- (\Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\) # (!\sec_cnt|count_second\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\,
	datac => \sec_cnt|count_second\(0),
	datad => \Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\,
	combout => \dec0|WideOr4~0_combout\);

-- Location: LCCOMB_X1_Y19_N26
\dec0|WideOr3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr3~0_combout\ = (\Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\) # ((\Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\ & 
-- \sec_cnt|count_second\(0))))) # (!\Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\ & (\Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\ $ (((!\Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\ & 
-- \sec_cnt|count_second\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\,
	datac => \sec_cnt|count_second\(0),
	datad => \Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\,
	combout => \dec0|WideOr3~0_combout\);

-- Location: LCCOMB_X1_Y19_N0
\dec0|WideOr2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr2~0_combout\ = (\sec_cnt|count_second\(0)) # ((\Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\))) # 
-- (!\Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\ & (\Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\,
	datac => \sec_cnt|count_second\(0),
	datad => \Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\,
	combout => \dec0|WideOr2~0_combout\);

-- Location: LCCOMB_X1_Y19_N22
\dec0|WideOr1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr1~0_combout\ = (\Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\) # ((\sec_cnt|count_second\(0) & 
-- \Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\)))) # (!\Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\ & ((\Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\) # 
-- ((!\Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\ & \sec_cnt|count_second\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110110011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\,
	datac => \sec_cnt|count_second\(0),
	datad => \Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\,
	combout => \dec0|WideOr1~0_combout\);

-- Location: LCCOMB_X1_Y19_N24
\dec0|WideOr0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr0~0_combout\ = (\Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\ & (!\Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\ & ((!\Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\) # 
-- (!\sec_cnt|count_second\(0))))) # (!\Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\ & (\Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\ $ (((\Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod0|auto_generated|divider|divider|StageOut[67]~159_combout\,
	datab => \Mod0|auto_generated|divider|divider|StageOut[68]~160_combout\,
	datac => \sec_cnt|count_second\(0),
	datad => \Mod0|auto_generated|divider|divider|StageOut[66]~158_combout\,
	combout => \dec0|WideOr0~0_combout\);

-- Location: LCCOMB_X25_Y19_N14
\Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\ = (\sec_cnt|count_second\(10) & (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\ & VCC)) # (!\sec_cnt|count_second\(10) & 
-- (!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\))
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\ = CARRY((!\sec_cnt|count_second\(10) & !\Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(10),
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\);

-- Location: LCCOMB_X25_Y19_N16
\Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\ = (\sec_cnt|count_second\(11) & ((GND) # (!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\))) # (!\sec_cnt|count_second\(11) & 
-- (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\ $ (GND)))
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\ = CARRY((\sec_cnt|count_second\(11)) # (!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(11),
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\);

-- Location: LCCOMB_X25_Y19_N20
\Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\ = (\sec_cnt|count_second\(13) & (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ $ (GND))) # (!\sec_cnt|count_second\(13) & 
-- (!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ & VCC))
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\ = CARRY((\sec_cnt|count_second\(13) & !\Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(13),
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[5]~7\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\);

-- Location: LCCOMB_X25_Y19_N22
\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ = !\Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\);

-- Location: LCCOMB_X24_Y19_N30
\Mod1|auto_generated|divider|divider|StageOut[54]~145\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[54]~145_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[54]~145_combout\);

-- Location: LCCOMB_X25_Y19_N30
\Mod1|auto_generated|divider|divider|StageOut[53]~146\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[53]~146_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \sec_cnt|count_second\(12))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \sec_cnt|count_second\(12),
	combout => \Mod1|auto_generated|divider|divider|StageOut[53]~146_combout\);

-- Location: LCCOMB_X24_Y19_N18
\Mod1|auto_generated|divider|divider|StageOut[52]~149\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[52]~149_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[52]~149_combout\);

-- Location: LCCOMB_X24_Y19_N14
\Mod1|auto_generated|divider|divider|StageOut[51]~151\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[51]~151_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[51]~151_combout\);

-- Location: LCCOMB_X25_Y19_N8
\Mod1|auto_generated|divider|divider|StageOut[50]~153\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[50]~153_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[50]~153_combout\);

-- Location: LCCOMB_X24_Y19_N26
\Mod1|auto_generated|divider|divider|StageOut[49]~155\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[49]~155_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \sec_cnt|count_second\(8))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \sec_cnt|count_second\(8),
	combout => \Mod1|auto_generated|divider|divider|StageOut[49]~155_combout\);

-- Location: LCCOMB_X24_Y19_N2
\Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\ & (((\Mod1|auto_generated|divider|divider|StageOut[50]~152_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[50]~153_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\ & (!\Mod1|auto_generated|divider|divider|StageOut[50]~152_combout\ & 
-- (!\Mod1|auto_generated|divider|divider|StageOut[50]~153_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[50]~152_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[50]~153_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[50]~152_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[50]~153_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[2]~1\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\);

-- Location: LCCOMB_X24_Y19_N4
\Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\ & ((((\Mod1|auto_generated|divider|divider|StageOut[51]~150_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[51]~151_combout\))))) # (!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\ & ((\Mod1|auto_generated|divider|divider|StageOut[51]~150_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|StageOut[51]~151_combout\) # (GND))))
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[51]~150_combout\) # ((\Mod1|auto_generated|divider|divider|StageOut[51]~151_combout\) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[51]~150_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[51]~151_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~3\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\);

-- Location: LCCOMB_X24_Y19_N8
\Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ & (((\Mod1|auto_generated|divider|divider|StageOut[53]~147_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[53]~146_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ & ((((\Mod1|auto_generated|divider|divider|StageOut[53]~147_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[53]~146_combout\)))))
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\ = CARRY((!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ & ((\Mod1|auto_generated|divider|divider|StageOut[53]~147_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[53]~146_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[53]~147_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[53]~146_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\);

-- Location: LCCOMB_X24_Y19_N10
\Mod1|auto_generated|divider|divider|add_sub_7_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[54]~144_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[54]~145_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[54]~144_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[54]~145_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\);

-- Location: LCCOMB_X24_Y19_N12
\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ = \Mod1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\);

-- Location: LCCOMB_X24_Y19_N28
\Mod1|auto_generated|divider|divider|StageOut[61]~223\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[61]~223_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\sec_cnt|count_second\(11)))) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \sec_cnt|count_second\(11),
	datad => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[61]~223_combout\);

-- Location: LCCOMB_X23_Y19_N0
\Mod1|auto_generated|divider|divider|StageOut[62]~156\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[62]~156_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[62]~156_combout\);

-- Location: LCCOMB_X24_Y19_N22
\Mod1|auto_generated|divider|divider|StageOut[60]~224\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[60]~224_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\sec_cnt|count_second\(10)))) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\,
	datad => \sec_cnt|count_second\(10),
	combout => \Mod1|auto_generated|divider|divider|StageOut[60]~224_combout\);

-- Location: LCCOMB_X24_Y20_N2
\Mod1|auto_generated|divider|divider|StageOut[59]~159\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[59]~159_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[59]~159_combout\);

-- Location: LCCOMB_X24_Y20_N4
\Mod1|auto_generated|divider|divider|StageOut[58]~160\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[58]~160_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & \sec_cnt|count_second\(8))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datad => \sec_cnt|count_second\(8),
	combout => \Mod1|auto_generated|divider|divider|StageOut[58]~160_combout\);

-- Location: LCCOMB_X24_Y20_N22
\Mod1|auto_generated|divider|divider|StageOut[57]~162\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[57]~162_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & \sec_cnt|count_second\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datad => \sec_cnt|count_second\(7),
	combout => \Mod1|auto_generated|divider|divider|StageOut[57]~162_combout\);

-- Location: LCCOMB_X24_Y20_N10
\Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ & (((\Mod1|auto_generated|divider|divider|StageOut[58]~161_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[58]~160_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ & (!\Mod1|auto_generated|divider|divider|StageOut[58]~161_combout\ & 
-- (!\Mod1|auto_generated|divider|divider|StageOut[58]~160_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[58]~161_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[58]~160_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[58]~161_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[58]~160_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\);

-- Location: LCCOMB_X24_Y20_N12
\Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ & ((((\Mod1|auto_generated|divider|divider|StageOut[59]~225_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[59]~159_combout\))))) # (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ & ((\Mod1|auto_generated|divider|divider|StageOut[59]~225_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|StageOut[59]~159_combout\) # (GND))))
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[59]~225_combout\) # ((\Mod1|auto_generated|divider|divider|StageOut[59]~159_combout\) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[59]~225_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[59]~159_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\);

-- Location: LCCOMB_X24_Y20_N14
\Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\ = (\Mod1|auto_generated|divider|divider|StageOut[60]~158_combout\ & (((!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\)))) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[60]~158_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[60]~224_combout\ & (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\)) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[60]~224_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\) # (GND)))))
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ = CARRY(((!\Mod1|auto_generated|divider|divider|StageOut[60]~158_combout\ & !\Mod1|auto_generated|divider|divider|StageOut[60]~224_combout\)) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[60]~158_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[60]~224_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~5\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~7\);

-- Location: LCCOMB_X24_Y20_N18
\Mod1|auto_generated|divider|divider|add_sub_8_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[62]~222_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[62]~156_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[62]~222_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[62]~156_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\);

-- Location: LCCOMB_X24_Y20_N20
\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ = \Mod1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\);

-- Location: LCCOMB_X24_Y20_N30
\Mod1|auto_generated|divider|divider|StageOut[70]~204\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[70]~204_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[61]~223_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[61]~223_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[70]~204_combout\);

-- Location: LCCOMB_X23_Y20_N8
\Mod1|auto_generated|divider|divider|StageOut[69]~205\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[69]~205_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[60]~224_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[60]~224_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[69]~205_combout\);

-- Location: LCCOMB_X25_Y19_N0
\Mod1|auto_generated|divider|divider|StageOut[59]~225\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[59]~225_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\sec_cnt|count_second\(9)))) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & (\Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\,
	datab => \sec_cnt|count_second\(9),
	datac => \Mod1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[59]~225_combout\);

-- Location: LCCOMB_X24_Y20_N0
\Mod1|auto_generated|divider|divider|StageOut[68]~206\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[68]~206_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[59]~225_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[59]~225_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[68]~206_combout\);

-- Location: LCCOMB_X23_Y20_N28
\Mod1|auto_generated|divider|divider|StageOut[67]~167\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[67]~167_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[67]~167_combout\);

-- Location: LCCOMB_X22_Y20_N16
\Mod1|auto_generated|divider|divider|StageOut[66]~227\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[66]~227_combout\ = (\sec_cnt|count_second\(7) & \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \sec_cnt|count_second\(7),
	datad => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[66]~227_combout\);

-- Location: LCCOMB_X22_Y20_N4
\Mod1|auto_generated|divider|divider|StageOut[65]~170\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[65]~170_combout\ = (\sec_cnt|count_second\(6) & !\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(6),
	datad => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[65]~170_combout\);

-- Location: LCCOMB_X23_Y20_N10
\Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~0_combout\ = (((\Mod1|auto_generated|divider|divider|StageOut[65]~169_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[65]~170_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~1\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[65]~169_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[65]~170_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[65]~169_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[65]~170_combout\,
	datad => VCC,
	combout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~0_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~1\);

-- Location: LCCOMB_X23_Y20_N12
\Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~2_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~1\ & (((\Mod1|auto_generated|divider|divider|StageOut[66]~168_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[66]~227_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~1\ & (!\Mod1|auto_generated|divider|divider|StageOut[66]~168_combout\ & 
-- (!\Mod1|auto_generated|divider|divider|StageOut[66]~227_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~3\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[66]~168_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[66]~227_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[66]~168_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[66]~227_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~1\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~2_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~3\);

-- Location: LCCOMB_X23_Y20_N20
\Mod1|auto_generated|divider|divider|add_sub_9_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[70]~164_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[70]~204_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[70]~164_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[70]~204_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[6]~9\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\);

-- Location: LCCOMB_X23_Y20_N22
\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ = \Mod1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\);

-- Location: LCCOMB_X15_Y20_N2
\Mod1|auto_generated|divider|divider|StageOut[83]~233\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[83]~233_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & ((\sec_cnt|count_second\(6)))) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~0_combout\,
	datad => \sec_cnt|count_second\(6),
	combout => \Mod1|auto_generated|divider|divider|StageOut[83]~233_combout\);

-- Location: LCCOMB_X23_Y20_N2
\Mod1|auto_generated|divider|divider|StageOut[78]~207\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[78]~207_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[69]~205_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[69]~205_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[78]~207_combout\);

-- Location: LCCOMB_X23_Y20_N4
\Mod1|auto_generated|divider|divider|StageOut[77]~208\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[77]~208_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[68]~206_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[68]~206_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[77]~208_combout\);

-- Location: LCCOMB_X16_Y20_N14
\Mod1|auto_generated|divider|divider|StageOut[76]~173\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[76]~173_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~4_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[4]~4_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[76]~173_combout\);

-- Location: LCCOMB_X22_Y20_N6
\Mod1|auto_generated|divider|divider|StageOut[75]~232\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[75]~232_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\sec_cnt|count_second\(7)))) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & (\Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datac => \sec_cnt|count_second\(7),
	datad => \Mod1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[75]~232_combout\);

-- Location: LCCOMB_X15_Y20_N4
\Mod1|auto_generated|divider|divider|StageOut[74]~175\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[74]~175_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~0_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[2]~0_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[74]~175_combout\);

-- Location: LCCOMB_X16_Y20_N0
\Mod1|auto_generated|divider|divider|StageOut[73]~177\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[73]~177_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & \sec_cnt|count_second\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \sec_cnt|count_second\(5),
	combout => \Mod1|auto_generated|divider|divider|StageOut[73]~177_combout\);

-- Location: LCCOMB_X16_Y20_N16
\Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~0_combout\ = (((\Mod1|auto_generated|divider|divider|StageOut[73]~176_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[73]~177_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~1\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[73]~176_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[73]~177_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[73]~176_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[73]~177_combout\,
	datad => VCC,
	combout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~0_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~1\);

-- Location: LCCOMB_X16_Y20_N20
\Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~4_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~3\ & ((((\Mod1|auto_generated|divider|divider|StageOut[75]~174_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[75]~232_combout\))))) # (!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~3\ & ((\Mod1|auto_generated|divider|divider|StageOut[75]~174_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|StageOut[75]~232_combout\) # (GND))))
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~5\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[75]~174_combout\) # ((\Mod1|auto_generated|divider|divider|StageOut[75]~232_combout\) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[75]~174_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[75]~232_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~3\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~4_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~5\);

-- Location: LCCOMB_X16_Y20_N22
\Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~6_combout\ = (\Mod1|auto_generated|divider|divider|StageOut[76]~209_combout\ & (((!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~5\)))) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[76]~209_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[76]~173_combout\ & (!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~5\)) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[76]~173_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~5\) # (GND)))))
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~7\ = CARRY(((!\Mod1|auto_generated|divider|divider|StageOut[76]~209_combout\ & !\Mod1|auto_generated|divider|divider|StageOut[76]~173_combout\)) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[76]~209_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[76]~173_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~5\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~6_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~7\);

-- Location: LCCOMB_X16_Y20_N26
\Mod1|auto_generated|divider|divider|add_sub_10_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[7]~11_cout\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[78]~171_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[78]~207_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[78]~171_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[78]~207_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[6]~9\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[7]~11_cout\);

-- Location: LCCOMB_X16_Y20_N28
\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ = \Mod1|auto_generated|divider|divider|add_sub_10_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[7]~11_cout\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\);

-- Location: LCCOMB_X15_Y21_N16
\Mod1|auto_generated|divider|divider|StageOut[82]~182\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[82]~182_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~0_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[82]~182_combout\);

-- Location: LCCOMB_X14_Y20_N16
\Mod1|auto_generated|divider|divider|StageOut[81]~184\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[81]~184_combout\ = (\sec_cnt|count_second\(4) & !\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(4),
	datac => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[81]~184_combout\);

-- Location: LCCOMB_X15_Y20_N18
\Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~2_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~1\ & (((\Mod1|auto_generated|divider|divider|StageOut[82]~229_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[82]~182_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~1\ & (!\Mod1|auto_generated|divider|divider|StageOut[82]~229_combout\ & 
-- (!\Mod1|auto_generated|divider|divider|StageOut[82]~182_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~3\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[82]~229_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[82]~182_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[82]~229_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[82]~182_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~1\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~2_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~3\);

-- Location: LCCOMB_X15_Y20_N20
\Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~4_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~3\ & ((((\Mod1|auto_generated|divider|divider|StageOut[83]~181_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[83]~233_combout\))))) # (!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~3\ & ((\Mod1|auto_generated|divider|divider|StageOut[83]~181_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|StageOut[83]~233_combout\) # (GND))))
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~5\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[83]~181_combout\) # ((\Mod1|auto_generated|divider|divider|StageOut[83]~233_combout\) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[83]~181_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[83]~233_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~3\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~4_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~5\);

-- Location: LCCOMB_X16_Y20_N10
\Mod1|auto_generated|divider|divider|StageOut[86]~210\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[86]~210_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[77]~208_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~6_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[5]~6_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[77]~208_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[86]~210_combout\);

-- Location: LCCOMB_X15_Y20_N30
\Mod1|auto_generated|divider|divider|StageOut[85]~179\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[85]~179_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~6_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[85]~179_combout\);

-- Location: LCCOMB_X16_Y20_N30
\Mod1|auto_generated|divider|divider|StageOut[84]~212\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[84]~212_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[75]~232_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|StageOut[75]~232_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_9_result_int[3]~2_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[84]~212_combout\);

-- Location: LCCOMB_X15_Y20_N26
\Mod1|auto_generated|divider|divider|add_sub_11_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[7]~11_cout\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[86]~178_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[86]~210_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[86]~178_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[86]~210_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[6]~9\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[7]~11_cout\);

-- Location: LCCOMB_X15_Y20_N28
\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ = \Mod1|auto_generated|divider|divider|add_sub_11_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[7]~11_cout\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\);

-- Location: LCCOMB_X15_Y20_N12
\Mod1|auto_generated|divider|divider|StageOut[92]~215\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[92]~215_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[83]~233_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[3]~2_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[83]~233_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[92]~215_combout\);

-- Location: LCCOMB_X14_Y22_N20
\Mod1|auto_generated|divider|divider|StageOut[101]~217\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[101]~217_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[92]~215_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~4_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~4_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[92]~215_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[101]~217_combout\);

-- Location: LCCOMB_X15_Y21_N26
\Mod1|auto_generated|divider|divider|StageOut[91]~234\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[91]~234_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & (\sec_cnt|count_second\(5))) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(5),
	datab => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[2]~0_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[91]~234_combout\);

-- Location: LCCOMB_X14_Y22_N30
\Mod1|auto_generated|divider|divider|StageOut[100]~218\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[100]~218_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[91]~234_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[91]~234_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[3]~2_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[100]~218_combout\);

-- Location: LCCOMB_X15_Y20_N0
\Mod1|auto_generated|divider|divider|StageOut[94]~213\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[94]~213_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[85]~211_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[85]~211_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[5]~6_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[94]~213_combout\);

-- Location: LCCOMB_X15_Y20_N6
\Mod1|auto_generated|divider|divider|StageOut[93]~214\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[93]~214_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[84]~212_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~4_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[4]~4_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_10_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[84]~212_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[93]~214_combout\);

-- Location: LCCOMB_X14_Y22_N28
\Mod1|auto_generated|divider|divider|StageOut[92]~187\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[92]~187_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~4_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[4]~4_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[92]~187_combout\);

-- Location: LCCOMB_X14_Y20_N4
\Mod1|auto_generated|divider|divider|StageOut[90]~230\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[90]~230_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & \sec_cnt|count_second\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	datac => \sec_cnt|count_second\(4),
	combout => \Mod1|auto_generated|divider|divider|StageOut[90]~230_combout\);

-- Location: LCCOMB_X14_Y22_N18
\Mod1|auto_generated|divider|divider|StageOut[89]~191\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[89]~191_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & \sec_cnt|count_second\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	datad => \sec_cnt|count_second\(3),
	combout => \Mod1|auto_generated|divider|divider|StageOut[89]~191_combout\);

-- Location: LCCOMB_X14_Y22_N0
\Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~0_combout\ = (((\Mod1|auto_generated|divider|divider|StageOut[89]~190_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[89]~191_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~1\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[89]~190_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[89]~191_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[89]~190_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[89]~191_combout\,
	datad => VCC,
	combout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~0_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~1\);

-- Location: LCCOMB_X14_Y22_N2
\Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~2_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~1\ & (((\Mod1|auto_generated|divider|divider|StageOut[90]~189_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[90]~230_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~1\ & (!\Mod1|auto_generated|divider|divider|StageOut[90]~189_combout\ & 
-- (!\Mod1|auto_generated|divider|divider|StageOut[90]~230_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~3\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[90]~189_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[90]~230_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[90]~189_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[90]~230_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~1\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~2_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~3\);

-- Location: LCCOMB_X14_Y22_N4
\Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~4_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~3\ & ((((\Mod1|auto_generated|divider|divider|StageOut[91]~188_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[91]~234_combout\))))) # (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~3\ & ((\Mod1|auto_generated|divider|divider|StageOut[91]~188_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|StageOut[91]~234_combout\) # (GND))))
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~5\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[91]~188_combout\) # ((\Mod1|auto_generated|divider|divider|StageOut[91]~234_combout\) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[91]~188_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[91]~234_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~3\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~4_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~5\);

-- Location: LCCOMB_X14_Y22_N6
\Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~6_combout\ = (\Mod1|auto_generated|divider|divider|StageOut[92]~215_combout\ & (((!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~5\)))) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[92]~215_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[92]~187_combout\ & (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~5\)) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[92]~187_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~5\) # (GND)))))
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~7\ = CARRY(((!\Mod1|auto_generated|divider|divider|StageOut[92]~215_combout\ & !\Mod1|auto_generated|divider|divider|StageOut[92]~187_combout\)) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[92]~215_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[92]~187_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~5\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~6_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~7\);

-- Location: LCCOMB_X14_Y22_N8
\Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~8_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~7\ & (((\Mod1|auto_generated|divider|divider|StageOut[93]~186_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[93]~214_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~7\ & ((((\Mod1|auto_generated|divider|divider|StageOut[93]~186_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[93]~214_combout\)))))
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~9\ = CARRY((!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~7\ & ((\Mod1|auto_generated|divider|divider|StageOut[93]~186_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[93]~214_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[93]~186_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[93]~214_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~7\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~8_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~9\);

-- Location: LCCOMB_X14_Y22_N10
\Mod1|auto_generated|divider|divider|add_sub_12_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[7]~11_cout\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[94]~185_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[94]~213_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[94]~185_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[94]~213_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~9\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[7]~11_cout\);

-- Location: LCCOMB_X14_Y22_N12
\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ = \Mod1|auto_generated|divider|divider|add_sub_12_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[7]~11_cout\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\);

-- Location: LCCOMB_X14_Y20_N26
\Mod1|auto_generated|divider|divider|StageOut[99]~235\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[99]~235_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & ((\sec_cnt|count_second\(4)))) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\ & (\Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[2]~0_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datac => \sec_cnt|count_second\(4),
	datad => \Mod1|auto_generated|divider|divider|add_sub_11_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[99]~235_combout\);

-- Location: LCCOMB_X13_Y22_N0
\Mod1|auto_generated|divider|divider|StageOut[98]~196\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[98]~196_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~0_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[98]~196_combout\);

-- Location: LCCOMB_X13_Y22_N2
\Mod1|auto_generated|divider|divider|StageOut[97]~197\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[97]~197_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & \sec_cnt|count_second\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datad => \sec_cnt|count_second\(2),
	combout => \Mod1|auto_generated|divider|divider|StageOut[97]~197_combout\);

-- Location: LCCOMB_X13_Y22_N4
\Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~0_combout\ = (((\Mod1|auto_generated|divider|divider|StageOut[97]~198_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[97]~197_combout\)))
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~1\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[97]~198_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[97]~197_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[97]~198_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[97]~197_combout\,
	datad => VCC,
	combout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~0_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~1\);

-- Location: LCCOMB_X13_Y22_N8
\Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~4_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~3\ & ((((\Mod1|auto_generated|divider|divider|StageOut[99]~195_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[99]~235_combout\))))) # (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~3\ & ((\Mod1|auto_generated|divider|divider|StageOut[99]~195_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|StageOut[99]~235_combout\) # (GND))))
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~5\ = CARRY((\Mod1|auto_generated|divider|divider|StageOut[99]~195_combout\) # ((\Mod1|auto_generated|divider|divider|StageOut[99]~235_combout\) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[99]~195_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[99]~235_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~3\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~4_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~5\);

-- Location: LCCOMB_X13_Y22_N10
\Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~6_combout\ = (\Mod1|auto_generated|divider|divider|StageOut[100]~194_combout\ & (((!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~5\)))) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[100]~194_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[100]~218_combout\ & (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~5\)) # 
-- (!\Mod1|auto_generated|divider|divider|StageOut[100]~218_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~5\) # (GND)))))
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~7\ = CARRY(((!\Mod1|auto_generated|divider|divider|StageOut[100]~194_combout\ & !\Mod1|auto_generated|divider|divider|StageOut[100]~218_combout\)) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[100]~194_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[100]~218_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~5\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~6_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~7\);

-- Location: LCCOMB_X13_Y22_N12
\Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~8_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~7\ & (((\Mod1|auto_generated|divider|divider|StageOut[101]~193_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[101]~217_combout\)))) # (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~7\ & ((((\Mod1|auto_generated|divider|divider|StageOut[101]~193_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[101]~217_combout\)))))
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~9\ = CARRY((!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~7\ & ((\Mod1|auto_generated|divider|divider|StageOut[101]~193_combout\) # 
-- (\Mod1|auto_generated|divider|divider|StageOut[101]~217_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[101]~193_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[101]~217_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~7\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~8_combout\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~9\);

-- Location: LCCOMB_X13_Y22_N26
\Mod1|auto_generated|divider|divider|StageOut[110]~219\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[110]~219_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[101]~217_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[101]~217_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[5]~6_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[110]~219_combout\);

-- Location: LCCOMB_X14_Y22_N24
\Mod1|auto_generated|divider|divider|StageOut[102]~192\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[102]~192_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~8_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[6]~8_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[102]~192_combout\);

-- Location: LCCOMB_X13_Y22_N14
\Mod1|auto_generated|divider|divider|add_sub_13_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[7]~11_cout\ = CARRY((!\Mod1|auto_generated|divider|divider|StageOut[102]~216_combout\ & (!\Mod1|auto_generated|divider|divider|StageOut[102]~192_combout\ & 
-- !\Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|StageOut[102]~216_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[102]~192_combout\,
	datad => VCC,
	cin => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~9\,
	cout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[7]~11_cout\);

-- Location: LCCOMB_X13_Y22_N16
\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ = \Mod1|auto_generated|divider|divider|add_sub_13_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[7]~11_cout\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\);

-- Location: LCCOMB_X11_Y22_N24
\Div0|auto_generated|divider|divider|StageOut[18]~53\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[18]~53_combout\ = (\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[110]~219_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~8_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~8_combout\,
	datac => \Mod1|auto_generated|divider|divider|StageOut[110]~219_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[18]~53_combout\);

-- Location: LCCOMB_X13_Y22_N28
\Mod1|auto_generated|divider|divider|StageOut[109]~220\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[109]~220_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[100]~218_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[100]~218_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[4]~4_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[109]~220_combout\);

-- Location: LCCOMB_X11_Y22_N14
\Div0|auto_generated|divider|divider|StageOut[17]~54\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[17]~54_combout\ = (\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[109]~220_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|StageOut[109]~220_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~6_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[17]~54_combout\);

-- Location: LCCOMB_X11_Y22_N0
\Mod1|auto_generated|divider|divider|StageOut[110]~199\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[110]~199_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~8_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[6]~8_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[110]~199_combout\);

-- Location: LCCOMB_X11_Y22_N18
\Mod1|auto_generated|divider|divider|StageOut[109]~200\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[109]~200_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[5]~6_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[109]~200_combout\);

-- Location: LCCOMB_X11_Y22_N16
\Mod1|auto_generated|divider|divider|StageOut[108]~201\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[108]~201_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~4_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[108]~201_combout\);

-- Location: LCCOMB_X11_Y22_N12
\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ = !\Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div0|auto_generated|divider|divider|add_sub_3_result_int[3]~5\,
	combout => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\);

-- Location: LCCOMB_X11_Y22_N30
\Div0|auto_generated|divider|divider|StageOut[16]~44\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[16]~44_combout\ = (\Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\ & !\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[16]~44_combout\);

-- Location: LCCOMB_X12_Y22_N2
\Mod1|auto_generated|divider|divider|StageOut[107]~236\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[107]~236_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & ((\sec_cnt|count_second\(3)))) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[2]~0_combout\,
	datad => \sec_cnt|count_second\(3),
	combout => \Mod1|auto_generated|divider|divider|StageOut[107]~236_combout\);

-- Location: LCCOMB_X12_Y22_N18
\Div0|auto_generated|divider|divider|StageOut[15]~57\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[15]~57_combout\ = (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[107]~236_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~2_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[107]~236_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[15]~57_combout\);

-- Location: LCCOMB_X10_Y22_N0
\Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\ = (((\Div0|auto_generated|divider|divider|StageOut[15]~56_combout\) # (\Div0|auto_generated|divider|divider|StageOut[15]~57_combout\)))
-- \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\ = CARRY((\Div0|auto_generated|divider|divider|StageOut[15]~56_combout\) # (\Div0|auto_generated|divider|divider|StageOut[15]~57_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[15]~56_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[15]~57_combout\,
	datad => VCC,
	combout => \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\);

-- Location: LCCOMB_X10_Y22_N2
\Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\ & (((\Div0|auto_generated|divider|divider|StageOut[16]~55_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[16]~44_combout\)))) # (!\Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\ & (!\Div0|auto_generated|divider|divider|StageOut[16]~55_combout\ & 
-- (!\Div0|auto_generated|divider|divider|StageOut[16]~44_combout\)))
-- \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ = CARRY((!\Div0|auto_generated|divider|divider|StageOut[16]~55_combout\ & (!\Div0|auto_generated|divider|divider|StageOut[16]~44_combout\ & 
-- !\Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[16]~55_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[16]~44_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~1\,
	combout => \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\);

-- Location: LCCOMB_X10_Y22_N4
\Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ & (((\Div0|auto_generated|divider|divider|StageOut[17]~43_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[17]~54_combout\)))) # (!\Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ & ((((\Div0|auto_generated|divider|divider|StageOut[17]~43_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[17]~54_combout\)))))
-- \Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\ = CARRY((!\Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ & ((\Div0|auto_generated|divider|divider|StageOut[17]~43_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[17]~54_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[17]~43_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[17]~54_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~3\,
	combout => \Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\);

-- Location: LCCOMB_X10_Y22_N6
\Div0|auto_generated|divider|divider|add_sub_4_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\ = CARRY((!\Div0|auto_generated|divider|divider|StageOut[18]~42_combout\ & (!\Div0|auto_generated|divider|divider|StageOut[18]~53_combout\ & 
-- !\Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[18]~42_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[18]~53_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~5\,
	cout => \Div0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\);

-- Location: LCCOMB_X10_Y22_N8
\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ = \Div0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div0|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\,
	combout => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\);

-- Location: LCCOMB_X13_Y22_N22
\Mod1|auto_generated|divider|divider|StageOut[108]~221\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[108]~221_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[99]~235_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datab => \Mod1|auto_generated|divider|divider|StageOut[99]~235_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[3]~2_combout\,
	combout => \Mod1|auto_generated|divider|divider|StageOut[108]~221_combout\);

-- Location: LCCOMB_X11_Y22_N28
\Div0|auto_generated|divider|divider|StageOut[16]~55\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[16]~55_combout\ = (\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[108]~221_combout\) # 
-- ((!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|StageOut[108]~221_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[4]~4_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[16]~55_combout\);

-- Location: LCCOMB_X11_Y22_N20
\Div0|auto_generated|divider|divider|StageOut[22]~59\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[22]~59_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\Div0|auto_generated|divider|divider|StageOut[16]~55_combout\) # 
-- ((\Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\ & !\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[16]~55_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[22]~59_combout\);

-- Location: LCCOMB_X9_Y22_N18
\Div0|auto_generated|divider|divider|StageOut[28]~60\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[28]~60_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\Div0|auto_generated|divider|divider|StageOut[22]~59_combout\) # 
-- ((!\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|StageOut[22]~59_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[28]~60_combout\);

-- Location: LCCOMB_X12_Y22_N20
\Div0|auto_generated|divider|divider|StageOut[15]~56\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[15]~56_combout\ = (\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & ((\Mod1|auto_generated|divider|divider|StageOut[107]~236_combout\) # 
-- ((\Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~2_combout\ & !\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[3]~2_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[107]~236_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[15]~56_combout\);

-- Location: LCCOMB_X9_Y22_N2
\Div0|auto_generated|divider|divider|StageOut[21]~47\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[21]~47_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\Div0|auto_generated|divider|divider|StageOut[15]~57_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[15]~56_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div0|auto_generated|divider|divider|StageOut[15]~57_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datad => \Div0|auto_generated|divider|divider|StageOut[15]~56_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[21]~47_combout\);

-- Location: LCCOMB_X9_Y22_N28
\Div0|auto_generated|divider|divider|StageOut[27]~61\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[27]~61_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\Div0|auto_generated|divider|divider|StageOut[21]~47_combout\) # 
-- ((!\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\,
	datad => \Div0|auto_generated|divider|divider|StageOut[21]~47_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[27]~61_combout\);

-- Location: LCCOMB_X9_Y22_N10
\Div0|auto_generated|divider|divider|StageOut[20]~64\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[20]~64_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & (\sec_cnt|count_second\(2))) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(2),
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~0_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[20]~64_combout\);

-- Location: LCCOMB_X9_Y22_N30
\Div0|auto_generated|divider|divider|StageOut[23]~45\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[23]~45_combout\ = (!\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & \Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[23]~45_combout\);

-- Location: LCCOMB_X9_Y22_N24
\Div0|auto_generated|divider|divider|StageOut[21]~48\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[21]~48_combout\ = (!\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[21]~48_combout\);

-- Location: LCCOMB_X8_Y22_N18
\Div0|auto_generated|divider|divider|add_sub_5_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\ = CARRY((!\Div0|auto_generated|divider|divider|StageOut[23]~58_combout\ & (!\Div0|auto_generated|divider|divider|StageOut[23]~45_combout\ & 
-- !\Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[23]~58_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[23]~45_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_5_result_int[3]~5\,
	cout => \Div0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\);

-- Location: LCCOMB_X8_Y22_N20
\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ = \Div0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div0|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\,
	combout => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\);

-- Location: LCCOMB_X9_Y22_N4
\Div0|auto_generated|divider|divider|StageOut[20]~65\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[20]~65_combout\ = (!\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & (\sec_cnt|count_second\(2))) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(2),
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[2]~0_combout\,
	datad => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[20]~65_combout\);

-- Location: LCCOMB_X8_Y22_N4
\Div0|auto_generated|divider|divider|StageOut[26]~51\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[26]~51_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\Div0|auto_generated|divider|divider|StageOut[20]~64_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[20]~65_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div0|auto_generated|divider|divider|StageOut[20]~64_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datad => \Div0|auto_generated|divider|divider|StageOut[20]~65_combout\,
	combout => \Div0|auto_generated|divider|divider|StageOut[26]~51_combout\);

-- Location: LCCOMB_X9_Y22_N26
\Mod1|auto_generated|divider|divider|StageOut[96]~202\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[96]~202_combout\ = (\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & \sec_cnt|count_second\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datad => \sec_cnt|count_second\(1),
	combout => \Mod1|auto_generated|divider|divider|StageOut[96]~202_combout\);

-- Location: LCCOMB_X9_Y22_N12
\Mod1|auto_generated|divider|divider|StageOut[96]~203\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|StageOut[96]~203_combout\ = (!\Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\ & \sec_cnt|count_second\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod1|auto_generated|divider|divider|add_sub_12_result_int[8]~12_combout\,
	datad => \sec_cnt|count_second\(1),
	combout => \Mod1|auto_generated|divider|divider|StageOut[96]~203_combout\);

-- Location: LCCOMB_X9_Y22_N20
\Mod1|auto_generated|divider|divider|add_sub_13_result_int[1]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod1|auto_generated|divider|divider|add_sub_13_result_int[1]~14_combout\ = (\Mod1|auto_generated|divider|divider|StageOut[96]~202_combout\) # (\Mod1|auto_generated|divider|divider|StageOut[96]~203_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod1|auto_generated|divider|divider|StageOut[96]~202_combout\,
	datad => \Mod1|auto_generated|divider|divider|StageOut[96]~203_combout\,
	combout => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[1]~14_combout\);

-- Location: LCCOMB_X9_Y22_N14
\Div0|auto_generated|divider|divider|StageOut[25]~62\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|StageOut[25]~62_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & ((\sec_cnt|count_second\(1)))) # 
-- (!\Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\ & (\Mod1|auto_generated|divider|divider|add_sub_13_result_int[1]~14_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datab => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[8]~12_combout\,
	datac => \Mod1|auto_generated|divider|divider|add_sub_13_result_int[1]~14_combout\,
	datad => \sec_cnt|count_second\(1),
	combout => \Div0|auto_generated|divider|divider|StageOut[25]~62_combout\);

-- Location: LCCOMB_X8_Y22_N22
\Div0|auto_generated|divider|divider|add_sub_6_result_int[1]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_6_result_int[1]~1_cout\ = CARRY((\Div0|auto_generated|divider|divider|StageOut[25]~63_combout\) # (\Div0|auto_generated|divider|divider|StageOut[25]~62_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[25]~63_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[25]~62_combout\,
	datad => VCC,
	cout => \Div0|auto_generated|divider|divider|add_sub_6_result_int[1]~1_cout\);

-- Location: LCCOMB_X8_Y22_N24
\Div0|auto_generated|divider|divider|add_sub_6_result_int[2]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_6_result_int[2]~3_cout\ = CARRY((!\Div0|auto_generated|divider|divider|StageOut[26]~52_combout\ & (!\Div0|auto_generated|divider|divider|StageOut[26]~51_combout\ & 
-- !\Div0|auto_generated|divider|divider|add_sub_6_result_int[1]~1_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[26]~52_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[26]~51_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_6_result_int[1]~1_cout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_6_result_int[2]~3_cout\);

-- Location: LCCOMB_X8_Y22_N26
\Div0|auto_generated|divider|divider|add_sub_6_result_int[3]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_6_result_int[3]~5_cout\ = CARRY((!\Div0|auto_generated|divider|divider|add_sub_6_result_int[2]~3_cout\ & ((\Div0|auto_generated|divider|divider|StageOut[27]~50_combout\) # 
-- (\Div0|auto_generated|divider|divider|StageOut[27]~61_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[27]~50_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[27]~61_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_6_result_int[2]~3_cout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_6_result_int[3]~5_cout\);

-- Location: LCCOMB_X8_Y22_N28
\Div0|auto_generated|divider|divider|add_sub_6_result_int[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\ = CARRY((!\Div0|auto_generated|divider|divider|StageOut[28]~49_combout\ & (!\Div0|auto_generated|divider|divider|StageOut[28]~60_combout\ & 
-- !\Div0|auto_generated|divider|divider|add_sub_6_result_int[3]~5_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|StageOut[28]~49_combout\,
	datab => \Div0|auto_generated|divider|divider|StageOut[28]~60_combout\,
	datad => VCC,
	cin => \Div0|auto_generated|divider|divider|add_sub_6_result_int[3]~5_cout\,
	cout => \Div0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\);

-- Location: LCCOMB_X8_Y22_N30
\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ = \Div0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div0|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\,
	combout => \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\);

-- Location: LCCOMB_X1_Y22_N12
\dec1|WideOr6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr6~0_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & (\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ $ (((\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\) # 
-- (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\))))) # (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & (((!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000000111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \dec1|WideOr6~0_combout\);

-- Location: LCCOMB_X1_Y22_N6
\dec1|WideOr5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr5~0_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & (((!\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & 
-- !\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)))) # (!\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ $ 
-- (\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\)) # (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001000111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \dec1|WideOr5~0_combout\);

-- Location: LCCOMB_X1_Y22_N24
\dec1|WideOr4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr4~0_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & ((\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\) # 
-- (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)))) # (!\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & (((!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \dec1|WideOr4~0_combout\);

-- Location: LCCOMB_X1_Y22_N22
\dec1|WideOr3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr3~0_combout\ = (\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & (\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ $ (((\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\) # 
-- (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\))))) # (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & (((!\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ & 
-- !\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\)) # (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000100111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \dec1|WideOr3~0_combout\);

-- Location: LCCOMB_X1_Y22_N16
\dec1|WideOr2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr2~0_combout\ = ((\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ & (!\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\)) # (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ 
-- & ((!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)))) # (!\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010101111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \dec1|WideOr2~0_combout\);

-- Location: LCCOMB_X1_Y22_N10
\dec1|WideOr1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr1~0_combout\ = (\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ & ((\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\)) # 
-- (!\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\))))) # (!\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ & 
-- ((\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ $ (!\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)) # (!\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100110100111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \dec1|WideOr1~0_combout\);

-- Location: LCCOMB_X1_Y22_N28
\dec1|WideOr0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr0~0_combout\ = (\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & ((\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ $ (\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\)))) 
-- # (!\Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ & (\Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ & ((\Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\) # 
-- (\Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div0|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\,
	datab => \Div0|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\,
	datac => \Div0|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\,
	datad => \Div0|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\,
	combout => \dec1|WideOr0~0_combout\);

-- Location: LCCOMB_X21_Y19_N12
\Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\ = \sec_cnt|count_second\(7) $ (VCC)
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\ = CARRY(\sec_cnt|count_second\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(7),
	datad => VCC,
	combout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\);

-- Location: LCCOMB_X21_Y19_N14
\Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\ = (\sec_cnt|count_second\(8) & (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\ & VCC)) # (!\sec_cnt|count_second\(8) & 
-- (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\))
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\ = CARRY((!\sec_cnt|count_second\(8) & !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(8),
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\);

-- Location: LCCOMB_X21_Y19_N16
\Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\ = (\sec_cnt|count_second\(9) & (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\ $ (GND))) # (!\sec_cnt|count_second\(9) & 
-- (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\ & VCC))
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\ = CARRY((\sec_cnt|count_second\(9) & !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(9),
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\);

-- Location: LCCOMB_X21_Y19_N18
\Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\ = (\sec_cnt|count_second\(10) & (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\)) # (!\sec_cnt|count_second\(10) & 
-- ((\Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\) # (GND)))
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\) # (!\sec_cnt|count_second\(10)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(10),
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\);

-- Location: LCCOMB_X21_Y19_N22
\Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\ = (\sec_cnt|count_second\(12) & (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\)) # (!\sec_cnt|count_second\(12) & 
-- ((\Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\) # (GND)))
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\) # (!\sec_cnt|count_second\(12)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(12),
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\);

-- Location: LCCOMB_X21_Y19_N26
\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ = !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\);

-- Location: LCCOMB_X20_Y19_N30
\Mod2|auto_generated|divider|divider|StageOut[103]~136\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[103]~136_combout\ = (\sec_cnt|count_second\(8) & \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(8),
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[103]~136_combout\);

-- Location: LCCOMB_X20_Y19_N14
\Mod2|auto_generated|divider|divider|StageOut[102]~138\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[102]~138_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \sec_cnt|count_second\(7),
	combout => \Mod2|auto_generated|divider|divider|StageOut[102]~138_combout\);

-- Location: LCCOMB_X19_Y19_N8
\Mod2|auto_generated|divider|divider|StageOut[101]~140\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[101]~140_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datac => \sec_cnt|count_second\(6),
	combout => \Mod2|auto_generated|divider|divider|StageOut[101]~140_combout\);

-- Location: LCCOMB_X19_Y19_N12
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\ = (((\Mod2|auto_generated|divider|divider|StageOut[101]~141_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[101]~140_combout\)))
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\ = CARRY((\Mod2|auto_generated|divider|divider|StageOut[101]~141_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[101]~140_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[101]~141_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[101]~140_combout\,
	datad => VCC,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\);

-- Location: LCCOMB_X19_Y19_N14
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\ & (((\Mod2|auto_generated|divider|divider|StageOut[102]~139_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[102]~138_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\ & (!\Mod2|auto_generated|divider|divider|StageOut[102]~139_combout\ & 
-- (!\Mod2|auto_generated|divider|divider|StageOut[102]~138_combout\)))
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ = CARRY((!\Mod2|auto_generated|divider|divider|StageOut[102]~139_combout\ & (!\Mod2|auto_generated|divider|divider|StageOut[102]~138_combout\ & 
-- !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[102]~139_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[102]~138_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\);

-- Location: LCCOMB_X19_Y19_N16
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ & (((\Mod2|auto_generated|divider|divider|StageOut[103]~137_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[103]~136_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ & ((((\Mod2|auto_generated|divider|divider|StageOut[103]~137_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[103]~136_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ & ((\Mod2|auto_generated|divider|divider|StageOut[103]~137_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[103]~136_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[103]~137_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[103]~136_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\);

-- Location: LCCOMB_X21_Y19_N0
\Mod2|auto_generated|divider|divider|StageOut[108]~126\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[108]~126_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(13))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datac => \sec_cnt|count_second\(13),
	combout => \Mod2|auto_generated|divider|divider|StageOut[108]~126_combout\);

-- Location: LCCOMB_X20_Y19_N0
\Mod2|auto_generated|divider|divider|StageOut[107]~129\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[107]~129_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[107]~129_combout\);

-- Location: LCCOMB_X20_Y19_N10
\Mod2|auto_generated|divider|divider|StageOut[106]~130\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[106]~130_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(11))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \sec_cnt|count_second\(11),
	combout => \Mod2|auto_generated|divider|divider|StageOut[106]~130_combout\);

-- Location: LCCOMB_X20_Y19_N22
\Mod2|auto_generated|divider|divider|StageOut[105]~132\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[105]~132_combout\ = (\sec_cnt|count_second\(10) & \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(10),
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[105]~132_combout\);

-- Location: LCCOMB_X20_Y19_N28
\Mod2|auto_generated|divider|divider|StageOut[104]~135\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[104]~135_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[104]~135_combout\);

-- Location: LCCOMB_X19_Y19_N18
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[104]~134_combout\ & (((!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\)))) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[104]~134_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[104]~135_combout\ & (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\)) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[104]~135_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\) # (GND)))))
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ = CARRY(((!\Mod2|auto_generated|divider|divider|StageOut[104]~134_combout\ & !\Mod2|auto_generated|divider|divider|StageOut[104]~135_combout\)) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[104]~134_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[104]~135_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\);

-- Location: LCCOMB_X19_Y19_N20
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ & (((\Mod2|auto_generated|divider|divider|StageOut[105]~133_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[105]~132_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ & ((((\Mod2|auto_generated|divider|divider|StageOut[105]~133_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[105]~132_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ & ((\Mod2|auto_generated|divider|divider|StageOut[105]~133_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[105]~132_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[105]~133_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[105]~132_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\);

-- Location: LCCOMB_X19_Y19_N22
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[106]~131_combout\ & (((!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\)))) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[106]~131_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[106]~130_combout\ & (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\)) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[106]~130_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\) # (GND)))))
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ = CARRY(((!\Mod2|auto_generated|divider|divider|StageOut[106]~131_combout\ & !\Mod2|auto_generated|divider|divider|StageOut[106]~130_combout\)) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[106]~131_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[106]~130_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\);

-- Location: LCCOMB_X19_Y19_N26
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[10]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\ = CARRY((!\Mod2|auto_generated|divider|divider|StageOut[108]~127_combout\ & (!\Mod2|auto_generated|divider|divider|StageOut[108]~126_combout\ & 
-- !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[108]~127_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[108]~126_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\);

-- Location: LCCOMB_X19_Y19_N28
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ = \Mod2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\);

-- Location: LCCOMB_X20_Y19_N18
\Mod2|auto_generated|divider|divider|StageOut[119]~209\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[119]~209_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & (\sec_cnt|count_second\(12))) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(12),
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[8]~10_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[119]~209_combout\);

-- Location: LCCOMB_X16_Y19_N4
\Mod2|auto_generated|divider|divider|StageOut[118]~143\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[118]~143_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[118]~143_combout\);

-- Location: LCCOMB_X16_Y19_N26
\Mod2|auto_generated|divider|divider|StageOut[117]~144\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[117]~144_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[117]~144_combout\);

-- Location: LCCOMB_X19_Y19_N30
\Mod2|auto_generated|divider|divider|StageOut[116]~212\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[116]~212_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & (\sec_cnt|count_second\(9))) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(9),
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[116]~212_combout\);

-- Location: LCCOMB_X16_Y19_N0
\Mod2|auto_generated|divider|divider|StageOut[115]~146\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[115]~146_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[115]~146_combout\);

-- Location: LCCOMB_X20_Y19_N2
\Mod2|auto_generated|divider|divider|StageOut[114]~214\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[114]~214_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\sec_cnt|count_second\(7)))) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\,
	datad => \sec_cnt|count_second\(7),
	combout => \Mod2|auto_generated|divider|divider|StageOut[114]~214_combout\);

-- Location: LCCOMB_X15_Y19_N22
\Mod2|auto_generated|divider|divider|StageOut[113]~149\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[113]~149_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[113]~149_combout\);

-- Location: LCCOMB_X15_Y19_N24
\Mod2|auto_generated|divider|divider|StageOut[112]~150\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[112]~150_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \sec_cnt|count_second\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(5),
	combout => \Mod2|auto_generated|divider|divider|StageOut[112]~150_combout\);

-- Location: LCCOMB_X16_Y19_N8
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\ = (((\Mod2|auto_generated|divider|divider|StageOut[112]~153_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[112]~150_combout\)))
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\ = CARRY((\Mod2|auto_generated|divider|divider|StageOut[112]~153_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[112]~150_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[112]~153_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[112]~150_combout\,
	datad => VCC,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\);

-- Location: LCCOMB_X16_Y19_N12
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ & (((\Mod2|auto_generated|divider|divider|StageOut[114]~147_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[114]~214_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ & ((((\Mod2|auto_generated|divider|divider|StageOut[114]~147_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[114]~214_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ & ((\Mod2|auto_generated|divider|divider|StageOut[114]~147_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[114]~214_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[114]~147_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[114]~214_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\);

-- Location: LCCOMB_X16_Y19_N18
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[117]~211_combout\ & (((!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\)))) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[117]~211_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[117]~144_combout\ & (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\)) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[117]~144_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\) # (GND)))))
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ = CARRY(((!\Mod2|auto_generated|divider|divider|StageOut[117]~211_combout\ & !\Mod2|auto_generated|divider|divider|StageOut[117]~144_combout\)) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[117]~211_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[117]~144_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\);

-- Location: LCCOMB_X16_Y19_N20
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ & (((\Mod2|auto_generated|divider|divider|StageOut[118]~210_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[118]~143_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ & ((((\Mod2|auto_generated|divider|divider|StageOut[118]~210_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[118]~143_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\ & ((\Mod2|auto_generated|divider|divider|StageOut[118]~210_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[118]~143_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[118]~210_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[118]~143_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~11\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\);

-- Location: LCCOMB_X16_Y19_N22
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[10]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\ = CARRY((!\Mod2|auto_generated|divider|divider|StageOut[119]~142_combout\ & (!\Mod2|auto_generated|divider|divider|StageOut[119]~209_combout\ & 
-- !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[119]~142_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[119]~209_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\);

-- Location: LCCOMB_X16_Y19_N24
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ = \Mod2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\);

-- Location: LCCOMB_X20_Y19_N20
\Mod2|auto_generated|divider|divider|StageOut[115]~213\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[115]~213_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\sec_cnt|count_second\(8)))) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[4]~2_combout\,
	datac => \sec_cnt|count_second\(8),
	datad => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[115]~213_combout\);

-- Location: LCCOMB_X16_Y19_N30
\Mod2|auto_generated|divider|divider|StageOut[127]~193\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[127]~193_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[115]~213_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[115]~213_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[127]~193_combout\);

-- Location: LCCOMB_X14_Y19_N18
\Mod2|auto_generated|divider|divider|StageOut[130]~154\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[130]~154_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[9]~12_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[130]~154_combout\);

-- Location: LCCOMB_X13_Y19_N22
\Mod2|auto_generated|divider|divider|StageOut[129]~155\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[129]~155_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[129]~155_combout\);

-- Location: LCCOMB_X15_Y19_N4
\Mod2|auto_generated|divider|divider|StageOut[128]~192\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[128]~192_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[116]~212_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\,
	datac => \Mod2|auto_generated|divider|divider|StageOut[116]~212_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[128]~192_combout\);

-- Location: LCCOMB_X14_Y19_N14
\Mod2|auto_generated|divider|divider|StageOut[126]~194\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[126]~194_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[114]~214_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[114]~214_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[126]~194_combout\);

-- Location: LCCOMB_X15_Y19_N6
\Mod2|auto_generated|divider|divider|StageOut[125]~215\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[125]~215_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & (\sec_cnt|count_second\(6))) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(6),
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[125]~215_combout\);

-- Location: LCCOMB_X14_Y19_N8
\Mod2|auto_generated|divider|divider|StageOut[124]~160\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[124]~160_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[124]~160_combout\);

-- Location: LCCOMB_X15_Y19_N8
\Mod2|auto_generated|divider|divider|StageOut[111]~161\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[111]~161_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \sec_cnt|count_second\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(4),
	combout => \Mod2|auto_generated|divider|divider|StageOut[111]~161_combout\);

-- Location: LCCOMB_X15_Y19_N20
\Mod2|auto_generated|divider|divider|StageOut[99]~163\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[99]~163_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \sec_cnt|count_second\(4),
	combout => \Mod2|auto_generated|divider|divider|StageOut[99]~163_combout\);

-- Location: LCCOMB_X15_Y19_N18
\Mod2|auto_generated|divider|divider|StageOut[99]~162\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[99]~162_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \sec_cnt|count_second\(4),
	combout => \Mod2|auto_generated|divider|divider|StageOut[99]~162_combout\);

-- Location: LCCOMB_X15_Y19_N26
\Mod2|auto_generated|divider|divider|add_sub_10_result_int[1]~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[99]~163_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[99]~162_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|StageOut[99]~163_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[99]~162_combout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\);

-- Location: LCCOMB_X15_Y19_N30
\Mod2|auto_generated|divider|divider|StageOut[111]~164\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[111]~164_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[1]~20_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[111]~164_combout\);

-- Location: LCCOMB_X14_Y19_N28
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[2]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[111]~161_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[111]~164_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|StageOut[111]~161_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[111]~164_combout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\);

-- Location: LCCOMB_X12_Y19_N6
\Mod2|auto_generated|divider|divider|StageOut[123]~165\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[123]~165_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[123]~165_combout\);

-- Location: LCCOMB_X13_Y19_N0
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\ = (((\Mod2|auto_generated|divider|divider|StageOut[123]~217_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[123]~165_combout\)))
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\ = CARRY((\Mod2|auto_generated|divider|divider|StageOut[123]~217_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[123]~165_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[123]~217_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[123]~165_combout\,
	datad => VCC,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\);

-- Location: LCCOMB_X13_Y19_N2
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\ & (((\Mod2|auto_generated|divider|divider|StageOut[124]~216_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[124]~160_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\ & (!\Mod2|auto_generated|divider|divider|StageOut[124]~216_combout\ & 
-- (!\Mod2|auto_generated|divider|divider|StageOut[124]~160_combout\)))
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ = CARRY((!\Mod2|auto_generated|divider|divider|StageOut[124]~216_combout\ & (!\Mod2|auto_generated|divider|divider|StageOut[124]~160_combout\ & 
-- !\Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[124]~216_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[124]~160_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~1\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\);

-- Location: LCCOMB_X13_Y19_N4
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ & (((\Mod2|auto_generated|divider|divider|StageOut[125]~159_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[125]~215_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ & ((((\Mod2|auto_generated|divider|divider|StageOut[125]~159_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[125]~215_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ & ((\Mod2|auto_generated|divider|divider|StageOut[125]~159_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[125]~215_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[125]~159_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[125]~215_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\);

-- Location: LCCOMB_X13_Y19_N6
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[126]~158_combout\ & (((!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\)))) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[126]~158_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[126]~194_combout\ & (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\)) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[126]~194_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\) # (GND)))))
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ = CARRY(((!\Mod2|auto_generated|divider|divider|StageOut[126]~158_combout\ & !\Mod2|auto_generated|divider|divider|StageOut[126]~194_combout\)) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[126]~158_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[126]~194_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\);

-- Location: LCCOMB_X13_Y19_N10
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[128]~156_combout\ & (((!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\)))) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[128]~156_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[128]~192_combout\ & (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\)) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[128]~192_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\) # (GND)))))
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ = CARRY(((!\Mod2|auto_generated|divider|divider|StageOut[128]~156_combout\ & !\Mod2|auto_generated|divider|divider|StageOut[128]~192_combout\)) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[128]~156_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[128]~192_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\);

-- Location: LCCOMB_X13_Y19_N14
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[10]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\ = CARRY((!\Mod2|auto_generated|divider|divider|StageOut[130]~190_combout\ & (!\Mod2|auto_generated|divider|divider|StageOut[130]~154_combout\ & 
-- !\Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[130]~190_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[130]~154_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\);

-- Location: LCCOMB_X13_Y19_N16
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ = \Mod2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\);

-- Location: LCCOMB_X13_Y19_N24
\Mod2|auto_generated|divider|divider|StageOut[139]~197\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[139]~197_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[127]~193_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[6]~6_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[127]~193_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[139]~197_combout\);

-- Location: LCCOMB_X14_Y19_N0
\Mod2|auto_generated|divider|divider|StageOut[138]~198\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[138]~198_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[126]~194_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[126]~194_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[138]~198_combout\);

-- Location: LCCOMB_X13_Y20_N2
\Mod2|auto_generated|divider|divider|StageOut[137]~170\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[137]~170_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[137]~170_combout\);

-- Location: LCCOMB_X13_Y20_N0
\Mod2|auto_generated|divider|divider|StageOut[136]~171\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[136]~171_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[4]~2_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[136]~171_combout\);

-- Location: LCCOMB_X12_Y19_N24
\Mod2|auto_generated|divider|divider|StageOut[135]~172\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[135]~172_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[135]~172_combout\);

-- Location: LCCOMB_X14_Y19_N26
\Mod2|auto_generated|divider|divider|StageOut[110]~175\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[110]~175_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \sec_cnt|count_second\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(3),
	combout => \Mod2|auto_generated|divider|divider|StageOut[110]~175_combout\);

-- Location: LCCOMB_X14_Y19_N12
\Mod2|auto_generated|divider|divider|StageOut[110]~174\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[110]~174_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \sec_cnt|count_second\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(3),
	combout => \Mod2|auto_generated|divider|divider|StageOut[110]~174_combout\);

-- Location: LCCOMB_X14_Y19_N30
\Mod2|auto_generated|divider|divider|add_sub_11_result_int[1]~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[110]~175_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[110]~174_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|StageOut[110]~175_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[110]~174_combout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\);

-- Location: LCCOMB_X14_Y19_N10
\Mod2|auto_generated|divider|divider|StageOut[134]~218\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[134]~218_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\sec_cnt|count_second\(3)))) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(3),
	combout => \Mod2|auto_generated|divider|divider|StageOut[134]~218_combout\);

-- Location: LCCOMB_X13_Y20_N6
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~2_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~1\ & (((\Mod2|auto_generated|divider|divider|StageOut[135]~201_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[135]~172_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~1\ & (!\Mod2|auto_generated|divider|divider|StageOut[135]~201_combout\ & 
-- (!\Mod2|auto_generated|divider|divider|StageOut[135]~172_combout\)))
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~3\ = CARRY((!\Mod2|auto_generated|divider|divider|StageOut[135]~201_combout\ & (!\Mod2|auto_generated|divider|divider|StageOut[135]~172_combout\ & 
-- !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[135]~201_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[135]~172_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~1\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~2_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~3\);

-- Location: LCCOMB_X13_Y20_N8
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~4_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~3\ & (((\Mod2|auto_generated|divider|divider|StageOut[136]~200_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[136]~171_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~3\ & ((((\Mod2|auto_generated|divider|divider|StageOut[136]~200_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[136]~171_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~5\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~3\ & ((\Mod2|auto_generated|divider|divider|StageOut[136]~200_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[136]~171_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[136]~200_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[136]~171_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~3\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~4_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~5\);

-- Location: LCCOMB_X13_Y20_N10
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~6_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[137]~199_combout\ & (((!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~5\)))) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[137]~199_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[137]~170_combout\ & (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~5\)) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[137]~170_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~5\) # (GND)))))
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~7\ = CARRY(((!\Mod2|auto_generated|divider|divider|StageOut[137]~199_combout\ & !\Mod2|auto_generated|divider|divider|StageOut[137]~170_combout\)) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[137]~199_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[137]~170_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~5\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~6_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~7\);

-- Location: LCCOMB_X13_Y20_N14
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~10_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[139]~168_combout\ & (((!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~9\)))) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[139]~168_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[139]~197_combout\ & (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~9\)) # 
-- (!\Mod2|auto_generated|divider|divider|StageOut[139]~197_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~9\) # (GND)))))
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~11\ = CARRY(((!\Mod2|auto_generated|divider|divider|StageOut[139]~168_combout\ & !\Mod2|auto_generated|divider|divider|StageOut[139]~197_combout\)) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[139]~168_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[139]~197_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[7]~9\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~10_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~11\);

-- Location: LCCOMB_X20_Y19_N6
\Mod2|auto_generated|divider|divider|StageOut[117]~211\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[117]~211_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & (\sec_cnt|count_second\(10))) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \sec_cnt|count_second\(10),
	datad => \Mod2|auto_generated|divider|divider|add_sub_9_result_int[6]~6_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[117]~211_combout\);

-- Location: LCCOMB_X16_Y19_N28
\Mod2|auto_generated|divider|divider|StageOut[129]~191\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[129]~191_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[117]~211_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[117]~211_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[129]~191_combout\);

-- Location: LCCOMB_X13_Y19_N26
\Mod2|auto_generated|divider|divider|StageOut[141]~195\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[141]~195_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[129]~191_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[129]~191_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[141]~195_combout\);

-- Location: LCCOMB_X13_Y20_N28
\Mod2|auto_generated|divider|divider|StageOut[140]~167\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[140]~167_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[140]~167_combout\);

-- Location: LCCOMB_X13_Y20_N16
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~12_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~11\ & (((\Mod2|auto_generated|divider|divider|StageOut[140]~196_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[140]~167_combout\)))) # (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~11\ & ((((\Mod2|auto_generated|divider|divider|StageOut[140]~196_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[140]~167_combout\)))))
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~13\ = CARRY((!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~11\ & ((\Mod2|auto_generated|divider|divider|StageOut[140]~196_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[140]~167_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[140]~196_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[140]~167_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~11\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~12_combout\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~13\);

-- Location: LCCOMB_X13_Y20_N18
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[10]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\ = CARRY((!\Mod2|auto_generated|divider|divider|StageOut[141]~166_combout\ & (!\Mod2|auto_generated|divider|divider|StageOut[141]~195_combout\ & 
-- !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~13\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[141]~166_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[141]~195_combout\,
	datad => VCC,
	cin => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~13\,
	cout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\);

-- Location: LCCOMB_X13_Y20_N20
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ = \Mod2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\);

-- Location: LCCOMB_X12_Y20_N4
\Mod2|auto_generated|divider|divider|StageOut[151]~203\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[151]~203_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[139]~197_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[139]~197_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[151]~203_combout\);

-- Location: LCCOMB_X11_Y20_N6
\Div1|auto_generated|divider|divider|StageOut[53]~61\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[53]~61_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[151]~203_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~10_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[8]~10_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[151]~203_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[53]~61_combout\);

-- Location: LCCOMB_X12_Y20_N30
\Mod2|auto_generated|divider|divider|StageOut[152]~202\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[152]~202_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[140]~196_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[140]~196_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[152]~202_combout\);

-- Location: LCCOMB_X11_Y20_N16
\Mod2|auto_generated|divider|divider|StageOut[152]~178\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[152]~178_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~12_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[152]~178_combout\);

-- Location: LCCOMB_X13_Y20_N30
\Mod2|auto_generated|divider|divider|StageOut[150]~204\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[150]~204_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[138]~198_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[6]~6_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[138]~198_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[150]~204_combout\);

-- Location: LCCOMB_X12_Y20_N14
\Mod2|auto_generated|divider|divider|StageOut[149]~181\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[149]~181_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~6_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~6_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[149]~181_combout\);

-- Location: LCCOMB_X12_Y20_N8
\Mod2|auto_generated|divider|divider|StageOut[148]~182\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[148]~182_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~4_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~4_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[148]~182_combout\);

-- Location: LCCOMB_X12_Y20_N18
\Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\ = (((\Mod2|auto_generated|divider|divider|StageOut[148]~206_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[148]~182_combout\)))
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\ = CARRY((\Mod2|auto_generated|divider|divider|StageOut[148]~206_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[148]~182_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[148]~206_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[148]~182_combout\,
	datad => VCC,
	combout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\);

-- Location: LCCOMB_X12_Y20_N20
\Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\ & (((\Mod2|auto_generated|divider|divider|StageOut[149]~205_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[149]~181_combout\)))) # (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\ & (!\Mod2|auto_generated|divider|divider|StageOut[149]~205_combout\ & 
-- (!\Mod2|auto_generated|divider|divider|StageOut[149]~181_combout\)))
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\ = CARRY((!\Mod2|auto_generated|divider|divider|StageOut[149]~205_combout\ & (!\Mod2|auto_generated|divider|divider|StageOut[149]~181_combout\ & 
-- !\Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[149]~205_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[149]~181_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~1\,
	combout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\);

-- Location: LCCOMB_X12_Y20_N22
\Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\ & ((((\Mod2|auto_generated|divider|divider|StageOut[150]~180_combout\) # 
-- (\Mod2|auto_generated|divider|divider|StageOut[150]~204_combout\))))) # (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\ & ((\Mod2|auto_generated|divider|divider|StageOut[150]~180_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|StageOut[150]~204_combout\) # (GND))))
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\ = CARRY((\Mod2|auto_generated|divider|divider|StageOut[150]~180_combout\) # ((\Mod2|auto_generated|divider|divider|StageOut[150]~204_combout\) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[150]~180_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[150]~204_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~3\,
	combout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~5\);

-- Location: LCCOMB_X12_Y20_N28
\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ = !\Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div1|auto_generated|divider|divider|add_sub_6_result_int[6]~9\,
	combout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\);

-- Location: LCCOMB_X11_Y20_N0
\Div1|auto_generated|divider|divider|StageOut[54]~60\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[54]~60_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[152]~202_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[152]~202_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[9]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[54]~60_combout\);

-- Location: LCCOMB_X11_Y21_N0
\Div1|auto_generated|divider|divider|StageOut[52]~42\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[52]~42_combout\ = (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[52]~42_combout\);

-- Location: LCCOMB_X12_Y20_N12
\Div1|auto_generated|divider|divider|StageOut[51]~63\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[51]~63_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[149]~205_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[149]~205_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[6]~6_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[51]~63_combout\);

-- Location: LCCOMB_X11_Y21_N30
\Div1|auto_generated|divider|divider|StageOut[50]~44\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[50]~44_combout\ = (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[50]~44_combout\);

-- Location: LCCOMB_X12_Y19_N26
\Mod2|auto_generated|divider|divider|StageOut[147]~207\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[147]~207_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[135]~201_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[135]~201_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[147]~207_combout\);

-- Location: LCCOMB_X12_Y19_N16
\Div1|auto_generated|divider|divider|StageOut[49]~65\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[49]~65_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[147]~207_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~2_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~2_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[147]~207_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[49]~65_combout\);

-- Location: LCCOMB_X11_Y21_N8
\Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\ = (\Div1|auto_generated|divider|divider|StageOut[52]~62_combout\ & (((!\Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\)))) # 
-- (!\Div1|auto_generated|divider|divider|StageOut[52]~62_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[52]~42_combout\ & (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\)) # 
-- (!\Div1|auto_generated|divider|divider|StageOut[52]~42_combout\ & ((\Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\) # (GND)))))
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ = CARRY(((!\Div1|auto_generated|divider|divider|StageOut[52]~62_combout\ & !\Div1|auto_generated|divider|divider|StageOut[52]~42_combout\)) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[52]~62_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[52]~42_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~5\,
	combout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~7\);

-- Location: LCCOMB_X11_Y21_N12
\Div1|auto_generated|divider|divider|add_sub_7_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\ = CARRY((!\Div1|auto_generated|divider|divider|StageOut[54]~40_combout\ & (!\Div1|auto_generated|divider|divider|StageOut[54]~60_combout\ & 
-- !\Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[54]~40_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[54]~60_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_7_result_int[6]~9\,
	cout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\);

-- Location: LCCOMB_X11_Y21_N14
\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ = \Div1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div1|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\,
	combout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\);

-- Location: LCCOMB_X10_Y21_N24
\Div1|auto_generated|divider|divider|StageOut[62]~66\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[62]~66_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[53]~61_combout\) # 
-- ((\Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\ & !\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[53]~61_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[62]~66_combout\);

-- Location: LCCOMB_X10_Y21_N8
\Div1|auto_generated|divider|divider|StageOut[61]~47\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[61]~47_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[61]~47_combout\);

-- Location: LCCOMB_X9_Y21_N28
\Div1|auto_generated|divider|divider|StageOut[60]~48\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[60]~48_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[60]~48_combout\);

-- Location: LCCOMB_X9_Y21_N26
\Div1|auto_generated|divider|divider|StageOut[59]~49\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[59]~49_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[59]~49_combout\);

-- Location: LCCOMB_X12_Y19_N18
\Mod2|auto_generated|divider|divider|StageOut[147]~183\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[147]~183_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~2_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[4]~2_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[147]~183_combout\);

-- Location: LCCOMB_X12_Y19_N4
\Div1|auto_generated|divider|divider|add_sub_6_result_int[1]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[1]~12_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[147]~183_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[147]~207_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|StageOut[147]~183_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[147]~207_combout\,
	combout => \Div1|auto_generated|divider|divider|add_sub_6_result_int[1]~12_combout\);

-- Location: LCCOMB_X11_Y21_N16
\Div1|auto_generated|divider|divider|StageOut[58]~70\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[58]~70_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[49]~65_combout\) # 
-- ((!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \Div1|auto_generated|divider|divider|add_sub_6_result_int[1]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|StageOut[49]~65_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[1]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[58]~70_combout\);

-- Location: LCCOMB_X14_Y19_N4
\Mod2|auto_generated|divider|divider|StageOut[122]~176\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[122]~176_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[122]~176_combout\);

-- Location: LCCOMB_X14_Y19_N24
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[2]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[122]~173_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[122]~176_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[122]~173_combout\,
	datac => \Mod2|auto_generated|divider|divider|StageOut[122]~176_combout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\);

-- Location: LCCOMB_X13_Y19_N30
\Mod2|auto_generated|divider|divider|StageOut[146]~208\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[146]~208_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[134]~218_combout\) # 
-- ((!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \Mod2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datac => \Mod2|auto_generated|divider|divider|StageOut[134]~218_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[2]~18_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[146]~208_combout\);

-- Location: LCCOMB_X12_Y19_N22
\Div1|auto_generated|divider|divider|StageOut[48]~71\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[48]~71_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[146]~208_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~0_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[3]~0_combout\,
	datab => \Mod2|auto_generated|divider|divider|StageOut[146]~208_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[48]~71_combout\);

-- Location: LCCOMB_X8_Y23_N12
\Div1|auto_generated|divider|divider|StageOut[57]~72\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[57]~72_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[48]~71_combout\) # 
-- ((\Div1|auto_generated|divider|divider|add_sub_6_result_int[0]~14_combout\ & !\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[0]~14_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[48]~71_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[57]~72_combout\);

-- Location: LCCOMB_X9_Y21_N8
\Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ & (((\Div1|auto_generated|divider|divider|StageOut[58]~50_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[58]~70_combout\)))) # (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ & (!\Div1|auto_generated|divider|divider|StageOut[58]~50_combout\ & 
-- (!\Div1|auto_generated|divider|divider|StageOut[58]~70_combout\)))
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ = CARRY((!\Div1|auto_generated|divider|divider|StageOut[58]~50_combout\ & (!\Div1|auto_generated|divider|divider|StageOut[58]~70_combout\ & 
-- !\Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[58]~50_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[58]~70_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_8_result_int[2]~1\,
	combout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~3\);

-- Location: LCCOMB_X9_Y21_N16
\Div1|auto_generated|divider|divider|add_sub_8_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\ = CARRY((!\Div1|auto_generated|divider|divider|StageOut[62]~46_combout\ & (!\Div1|auto_generated|divider|divider|StageOut[62]~66_combout\ & 
-- !\Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[62]~46_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[62]~66_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_8_result_int[6]~9\,
	cout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\);

-- Location: LCCOMB_X9_Y21_N18
\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ = \Div1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div1|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\,
	combout => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\);

-- Location: LCCOMB_X9_Y21_N0
\Div1|auto_generated|divider|divider|StageOut[70]~73\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[70]~73_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[61]~67_combout\) # 
-- ((\Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[61]~67_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[70]~73_combout\);

-- Location: LCCOMB_X11_Y21_N28
\Div1|auto_generated|divider|divider|StageOut[60]~68\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[60]~68_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[51]~63_combout\) # 
-- ((!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|StageOut[51]~63_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[60]~68_combout\);

-- Location: LCCOMB_X9_Y21_N30
\Div1|auto_generated|divider|divider|StageOut[69]~74\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[69]~74_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[60]~68_combout\) # 
-- ((\Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|StageOut[60]~68_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[69]~74_combout\);

-- Location: LCCOMB_X12_Y20_N2
\Div1|auto_generated|divider|divider|StageOut[50]~64\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[50]~64_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & ((\Mod2|auto_generated|divider|divider|StageOut[148]~206_combout\) # 
-- ((\Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~4_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|StageOut[148]~206_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[5]~4_combout\,
	datad => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[50]~64_combout\);

-- Location: LCCOMB_X11_Y21_N22
\Div1|auto_generated|divider|divider|StageOut[59]~69\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[59]~69_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[50]~64_combout\) # 
-- ((!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\,
	datad => \Div1|auto_generated|divider|divider|StageOut[50]~64_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[59]~69_combout\);

-- Location: LCCOMB_X9_Y21_N24
\Div1|auto_generated|divider|divider|StageOut[68]~75\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[68]~75_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[59]~69_combout\) # 
-- ((\Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|StageOut[59]~69_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[68]~75_combout\);

-- Location: LCCOMB_X9_Y21_N22
\Div1|auto_generated|divider|divider|StageOut[67]~56\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[67]~56_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\ & !\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div1|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[67]~56_combout\);

-- Location: LCCOMB_X8_Y23_N22
\Div1|auto_generated|divider|divider|StageOut[48]~51\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[48]~51_combout\ = (\Div1|auto_generated|divider|divider|add_sub_6_result_int[0]~14_combout\ & !\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_6_result_int[0]~14_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[48]~51_combout\);

-- Location: LCCOMB_X8_Y23_N24
\Div1|auto_generated|divider|divider|add_sub_7_result_int[1]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[1]~14_combout\ = (\Div1|auto_generated|divider|divider|StageOut[48]~51_combout\) # (\Div1|auto_generated|divider|divider|StageOut[48]~71_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div1|auto_generated|divider|divider|StageOut[48]~51_combout\,
	datad => \Div1|auto_generated|divider|divider|StageOut[48]~71_combout\,
	combout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[1]~14_combout\);

-- Location: LCCOMB_X8_Y23_N26
\Div1|auto_generated|divider|divider|StageOut[66]~77\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[66]~77_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[57]~72_combout\) # 
-- ((!\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & \Div1|auto_generated|divider|divider|add_sub_7_result_int[1]~14_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[57]~72_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[1]~14_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[66]~77_combout\);

-- Location: LCCOMB_X11_Y20_N24
\Mod2|auto_generated|divider|divider|StageOut[133]~185\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[133]~185_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & \sec_cnt|count_second\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(2),
	combout => \Mod2|auto_generated|divider|divider|StageOut[133]~185_combout\);

-- Location: LCCOMB_X11_Y20_N26
\Mod2|auto_generated|divider|divider|StageOut[121]~186\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[121]~186_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \sec_cnt|count_second\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(2),
	combout => \Mod2|auto_generated|divider|divider|StageOut[121]~186_combout\);

-- Location: LCCOMB_X11_Y20_N12
\Mod2|auto_generated|divider|divider|StageOut[121]~187\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[121]~187_combout\ = (!\Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \sec_cnt|count_second\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(2),
	combout => \Mod2|auto_generated|divider|divider|StageOut[121]~187_combout\);

-- Location: LCCOMB_X11_Y20_N28
\Mod2|auto_generated|divider|divider|add_sub_12_result_int[1]~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_12_result_int[1]~20_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[121]~186_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[121]~187_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|StageOut[121]~186_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[121]~187_combout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[1]~20_combout\);

-- Location: LCCOMB_X11_Y20_N2
\Mod2|auto_generated|divider|divider|StageOut[133]~188\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[133]~188_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[1]~20_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[1]~20_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[133]~188_combout\);

-- Location: LCCOMB_X11_Y20_N22
\Mod2|auto_generated|divider|divider|add_sub_13_result_int[2]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|add_sub_13_result_int[2]~18_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[133]~185_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[133]~188_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|StageOut[133]~185_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[133]~188_combout\,
	combout => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[2]~18_combout\);

-- Location: LCCOMB_X8_Y21_N8
\Mod2|auto_generated|divider|divider|StageOut[145]~189\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[145]~189_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[2]~18_combout\ & !\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[2]~18_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	combout => \Mod2|auto_generated|divider|divider|StageOut[145]~189_combout\);

-- Location: LCCOMB_X11_Y20_N20
\Mod2|auto_generated|divider|divider|StageOut[145]~219\ : cycloneii_lcell_comb
-- Equation(s):
-- \Mod2|auto_generated|divider|divider|StageOut[145]~219_combout\ = (\Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\sec_cnt|count_second\(2)))) # 
-- (!\Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & (\Mod2|auto_generated|divider|divider|add_sub_12_result_int[1]~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mod2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datab => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[1]~20_combout\,
	datac => \Mod2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(2),
	combout => \Mod2|auto_generated|divider|divider|StageOut[145]~219_combout\);

-- Location: LCCOMB_X8_Y21_N4
\Div1|auto_generated|divider|divider|add_sub_7_result_int[0]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_7_result_int[0]~16_combout\ = (\Mod2|auto_generated|divider|divider|StageOut[145]~189_combout\) # (\Mod2|auto_generated|divider|divider|StageOut[145]~219_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Mod2|auto_generated|divider|divider|StageOut[145]~189_combout\,
	datad => \Mod2|auto_generated|divider|divider|StageOut[145]~219_combout\,
	combout => \Div1|auto_generated|divider|divider|add_sub_7_result_int[0]~16_combout\);

-- Location: LCCOMB_X8_Y21_N0
\Div1|auto_generated|divider|divider|StageOut[65]~79\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|StageOut[65]~79_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|StageOut[56]~78_combout\) # 
-- ((\Div1|auto_generated|divider|divider|add_sub_7_result_int[0]~16_combout\ & !\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[56]~78_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_7_result_int[0]~16_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \Div1|auto_generated|divider|divider|StageOut[65]~79_combout\);

-- Location: LCCOMB_X8_Y21_N18
\Div1|auto_generated|divider|divider|add_sub_9_result_int[2]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_9_result_int[2]~1_cout\ = CARRY((\Div1|auto_generated|divider|divider|StageOut[65]~59_combout\) # (\Div1|auto_generated|divider|divider|StageOut[65]~79_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[65]~59_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[65]~79_combout\,
	datad => VCC,
	cout => \Div1|auto_generated|divider|divider|add_sub_9_result_int[2]~1_cout\);

-- Location: LCCOMB_X8_Y21_N20
\Div1|auto_generated|divider|divider|add_sub_9_result_int[3]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_9_result_int[3]~3_cout\ = CARRY((!\Div1|auto_generated|divider|divider|StageOut[66]~57_combout\ & (!\Div1|auto_generated|divider|divider|StageOut[66]~77_combout\ & 
-- !\Div1|auto_generated|divider|divider|add_sub_9_result_int[2]~1_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[66]~57_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[66]~77_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_9_result_int[2]~1_cout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_9_result_int[3]~3_cout\);

-- Location: LCCOMB_X8_Y21_N22
\Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~5_cout\ = CARRY((\Div1|auto_generated|divider|divider|StageOut[67]~76_combout\) # ((\Div1|auto_generated|divider|divider|StageOut[67]~56_combout\) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_9_result_int[3]~3_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[67]~76_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[67]~56_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_9_result_int[3]~3_cout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~5_cout\);

-- Location: LCCOMB_X8_Y21_N24
\Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~7_cout\ = CARRY(((!\Div1|auto_generated|divider|divider|StageOut[68]~55_combout\ & !\Div1|auto_generated|divider|divider|StageOut[68]~75_combout\)) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~5_cout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[68]~55_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[68]~75_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~5_cout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~7_cout\);

-- Location: LCCOMB_X8_Y21_N26
\Div1|auto_generated|divider|divider|add_sub_9_result_int[6]~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_9_result_int[6]~9_cout\ = CARRY((!\Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~7_cout\ & ((\Div1|auto_generated|divider|divider|StageOut[69]~54_combout\) # 
-- (\Div1|auto_generated|divider|divider|StageOut[69]~74_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[69]~54_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[69]~74_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~7_cout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_9_result_int[6]~9_cout\);

-- Location: LCCOMB_X8_Y21_N28
\Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\ = CARRY((!\Div1|auto_generated|divider|divider|StageOut[70]~53_combout\ & (!\Div1|auto_generated|divider|divider|StageOut[70]~73_combout\ & 
-- !\Div1|auto_generated|divider|divider|add_sub_9_result_int[6]~9_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|StageOut[70]~53_combout\,
	datab => \Div1|auto_generated|divider|divider|StageOut[70]~73_combout\,
	datad => VCC,
	cin => \Div1|auto_generated|divider|divider|add_sub_9_result_int[6]~9_cout\,
	cout => \Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\);

-- Location: LCCOMB_X8_Y21_N30
\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ = \Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\,
	combout => \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\);

-- Location: LCCOMB_X8_Y23_N6
\dec2|WideOr6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr6~0_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ $ (((\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\) 
-- # (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\))))) # (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & (((!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100001110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \dec2|WideOr6~0_combout\);

-- Location: LCCOMB_X8_Y23_N8
\dec2|WideOr5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr5~0_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & 
-- ((!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)))) # (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ $ 
-- (\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\)) # (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001001110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \dec2|WideOr5~0_combout\);

-- Location: LCCOMB_X8_Y23_N18
\dec2|WideOr4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr4~0_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\) 
-- # (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)))) # (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & (((!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000001110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \dec2|WideOr4~0_combout\);

-- Location: LCCOMB_X8_Y23_N20
\dec2|WideOr3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr3~0_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ $ (((\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\) 
-- # (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\))))) # (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & (((!\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & 
-- !\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\)) # (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100101110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \dec2|WideOr3~0_combout\);

-- Location: LCCOMB_X8_Y23_N2
\dec2|WideOr2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr2~0_combout\ = ((\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)) # 
-- (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & ((!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)))) # (!\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010111101111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \dec2|WideOr2~0_combout\);

-- Location: LCCOMB_X8_Y23_N0
\dec2|WideOr1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr1~0_combout\ = (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & (((!\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ & 
-- \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)) # (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\))) # (!\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & 
-- (((!\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & !\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\)) # (!\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100110101110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \dec2|WideOr1~0_combout\);

-- Location: LCCOMB_X8_Y23_N10
\dec2|WideOr0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr0~0_combout\ = (\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & (\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ $ 
-- (((\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\))))) # (!\Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ & (\Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & 
-- ((\Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\) # (\Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div1|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\,
	datab => \Div1|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datac => \Div1|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\,
	datad => \Div1|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \dec2|WideOr0~0_combout\);

-- Location: LCCOMB_X21_Y21_N16
\Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\ = \sec_cnt|count_second\(7) $ (VCC)
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\ = CARRY(\sec_cnt|count_second\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(7),
	datad => VCC,
	combout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~1\);

-- Location: LCCOMB_X21_Y21_N20
\Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\ = (\sec_cnt|count_second\(9) & (\Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\ $ (GND))) # (!\sec_cnt|count_second\(9) & 
-- (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\ & VCC))
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\ = CARRY((\sec_cnt|count_second\(9) & !\Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(9),
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_9_result_int[4]~3\,
	combout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~5\);

-- Location: LCCOMB_X21_Y21_N24
\Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\ = (\sec_cnt|count_second\(11) & (\Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\ $ (GND))) # (!\sec_cnt|count_second\(11) & 
-- (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\ & VCC))
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\ = CARRY((\sec_cnt|count_second\(11) & !\Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(11),
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_9_result_int[6]~7\,
	combout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~9\);

-- Location: LCCOMB_X21_Y21_N28
\Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\ = (\sec_cnt|count_second\(13) & (\Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\ $ (GND))) # (!\sec_cnt|count_second\(13) & 
-- (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\ & VCC))
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\ = CARRY((\sec_cnt|count_second\(13) & !\Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(13),
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_9_result_int[8]~11\,
	combout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\);

-- Location: LCCOMB_X21_Y21_N30
\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ = !\Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~13\,
	combout => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\);

-- Location: LCCOMB_X21_Y21_N14
\Div2|auto_generated|divider|divider|StageOut[107]~106\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[107]~106_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(12))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \sec_cnt|count_second\(12),
	combout => \Div2|auto_generated|divider|divider|StageOut[107]~106_combout\);

-- Location: LCCOMB_X20_Y21_N0
\Div2|auto_generated|divider|divider|StageOut[106]~109\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[106]~109_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[106]~109_combout\);

-- Location: LCCOMB_X21_Y21_N8
\Div2|auto_generated|divider|divider|StageOut[105]~110\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[105]~110_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(10))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datac => \sec_cnt|count_second\(10),
	combout => \Div2|auto_generated|divider|divider|StageOut[105]~110_combout\);

-- Location: LCCOMB_X20_Y21_N4
\Div2|auto_generated|divider|divider|StageOut[104]~113\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[104]~113_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_9_result_int[5]~4_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[104]~113_combout\);

-- Location: LCCOMB_X22_Y21_N28
\Div2|auto_generated|divider|divider|StageOut[103]~114\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[103]~114_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(8))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \sec_cnt|count_second\(8),
	combout => \Div2|auto_generated|divider|divider|StageOut[103]~114_combout\);

-- Location: LCCOMB_X19_Y21_N18
\Div2|auto_generated|divider|divider|StageOut[102]~116\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[102]~116_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \sec_cnt|count_second\(7),
	combout => \Div2|auto_generated|divider|divider|StageOut[102]~116_combout\);

-- Location: LCCOMB_X20_Y21_N2
\Div2|auto_generated|divider|divider|StageOut[101]~118\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[101]~118_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & \sec_cnt|count_second\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \sec_cnt|count_second\(6),
	combout => \Div2|auto_generated|divider|divider|StageOut[101]~118_combout\);

-- Location: LCCOMB_X20_Y21_N14
\Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\ = (((\Div2|auto_generated|divider|divider|StageOut[101]~119_combout\) # (\Div2|auto_generated|divider|divider|StageOut[101]~118_combout\)))
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\ = CARRY((\Div2|auto_generated|divider|divider|StageOut[101]~119_combout\) # (\Div2|auto_generated|divider|divider|StageOut[101]~118_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[101]~119_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[101]~118_combout\,
	datad => VCC,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\);

-- Location: LCCOMB_X20_Y21_N16
\Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\ & (((\Div2|auto_generated|divider|divider|StageOut[102]~117_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[102]~116_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\ & (!\Div2|auto_generated|divider|divider|StageOut[102]~117_combout\ & 
-- (!\Div2|auto_generated|divider|divider|StageOut[102]~116_combout\)))
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ = CARRY((!\Div2|auto_generated|divider|divider|StageOut[102]~117_combout\ & (!\Div2|auto_generated|divider|divider|StageOut[102]~116_combout\ & 
-- !\Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[102]~117_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[102]~116_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~1\,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\);

-- Location: LCCOMB_X20_Y21_N18
\Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ & (((\Div2|auto_generated|divider|divider|StageOut[103]~115_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[103]~114_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ & ((((\Div2|auto_generated|divider|divider|StageOut[103]~115_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[103]~114_combout\)))))
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\ & ((\Div2|auto_generated|divider|divider|StageOut[103]~115_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[103]~114_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[103]~115_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[103]~114_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~3\,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\);

-- Location: LCCOMB_X20_Y21_N20
\Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\ = (\Div2|auto_generated|divider|divider|StageOut[104]~112_combout\ & (((!\Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\)))) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[104]~112_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[104]~113_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\)) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[104]~113_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\) # (GND)))))
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ = CARRY(((!\Div2|auto_generated|divider|divider|StageOut[104]~112_combout\ & !\Div2|auto_generated|divider|divider|StageOut[104]~113_combout\)) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[104]~112_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[104]~113_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~5\,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\);

-- Location: LCCOMB_X20_Y21_N22
\Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ & (((\Div2|auto_generated|divider|divider|StageOut[105]~111_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[105]~110_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ & ((((\Div2|auto_generated|divider|divider|StageOut[105]~111_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[105]~110_combout\)))))
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\ & ((\Div2|auto_generated|divider|divider|StageOut[105]~111_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[105]~110_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[105]~111_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[105]~110_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~7\,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~9\);

-- Location: LCCOMB_X20_Y21_N26
\Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ & (((\Div2|auto_generated|divider|divider|StageOut[107]~107_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[107]~106_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ & ((((\Div2|auto_generated|divider|divider|StageOut[107]~107_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[107]~106_combout\)))))
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\ & ((\Div2|auto_generated|divider|divider|StageOut[107]~107_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[107]~106_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[107]~107_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[107]~106_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~11\,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\);

-- Location: LCCOMB_X20_Y21_N8
\Div2|auto_generated|divider|divider|StageOut[108]~105\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[108]~105_combout\ = (\Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\ & !\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_9_result_int[9]~12_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[108]~105_combout\);

-- Location: LCCOMB_X20_Y21_N28
\Div2|auto_generated|divider|divider|add_sub_10_result_int[10]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\ = CARRY((!\Div2|auto_generated|divider|divider|StageOut[108]~104_combout\ & (!\Div2|auto_generated|divider|divider|StageOut[108]~105_combout\ & 
-- !\Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[108]~104_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[108]~105_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~13\,
	cout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\);

-- Location: LCCOMB_X20_Y21_N30
\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ = \Div2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div2|auto_generated|divider|divider|add_sub_10_result_int[10]~15_cout\,
	combout => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\);

-- Location: LCCOMB_X19_Y21_N22
\Div2|auto_generated|divider|divider|StageOut[119]~120\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[119]~120_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\ & !\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[9]~12_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[119]~120_combout\);

-- Location: LCCOMB_X19_Y21_N30
\Div2|auto_generated|divider|divider|StageOut[118]~169\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[118]~169_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & (\sec_cnt|count_second\(11))) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(11),
	datab => \Div2|auto_generated|divider|divider|add_sub_9_result_int[7]~8_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[118]~169_combout\);

-- Location: LCCOMB_X18_Y21_N24
\Div2|auto_generated|divider|divider|StageOut[117]~122\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[117]~122_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[7]~8_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[117]~122_combout\);

-- Location: LCCOMB_X16_Y21_N12
\Div2|auto_generated|divider|divider|StageOut[116]~123\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[116]~123_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[6]~6_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[116]~123_combout\);

-- Location: LCCOMB_X19_Y21_N10
\Div2|auto_generated|divider|divider|StageOut[115]~124\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[115]~124_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[115]~124_combout\);

-- Location: LCCOMB_X19_Y21_N28
\Div2|auto_generated|divider|divider|StageOut[114]~173\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[114]~173_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & ((\sec_cnt|count_second\(7)))) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\ & (\Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_9_result_int[10]~14_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_9_result_int[3]~0_combout\,
	datad => \sec_cnt|count_second\(7),
	combout => \Div2|auto_generated|divider|divider|StageOut[114]~173_combout\);

-- Location: LCCOMB_X19_Y21_N14
\Div2|auto_generated|divider|divider|StageOut[113]~126\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[113]~126_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \sec_cnt|count_second\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(6),
	combout => \Div2|auto_generated|divider|divider|StageOut[113]~126_combout\);

-- Location: LCCOMB_X18_Y21_N30
\Div2|auto_generated|divider|divider|StageOut[112]~128\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[112]~128_combout\ = (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \sec_cnt|count_second\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(5),
	combout => \Div2|auto_generated|divider|divider|StageOut[112]~128_combout\);

-- Location: LCCOMB_X18_Y21_N6
\Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\ = (((\Div2|auto_generated|divider|divider|StageOut[112]~131_combout\) # (\Div2|auto_generated|divider|divider|StageOut[112]~128_combout\)))
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\ = CARRY((\Div2|auto_generated|divider|divider|StageOut[112]~131_combout\) # (\Div2|auto_generated|divider|divider|StageOut[112]~128_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[112]~131_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[112]~128_combout\,
	datad => VCC,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~1\);

-- Location: LCCOMB_X18_Y21_N10
\Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ & (((\Div2|auto_generated|divider|divider|StageOut[114]~125_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[114]~173_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ & ((((\Div2|auto_generated|divider|divider|StageOut[114]~125_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[114]~173_combout\)))))
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\ & ((\Div2|auto_generated|divider|divider|StageOut[114]~125_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[114]~173_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[114]~125_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[114]~173_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_11_result_int[4]~3\,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~5\);

-- Location: LCCOMB_X18_Y21_N14
\Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ & (((\Div2|auto_generated|divider|divider|StageOut[116]~171_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[116]~123_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ & ((((\Div2|auto_generated|divider|divider|StageOut[116]~171_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[116]~123_combout\)))))
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\ & ((\Div2|auto_generated|divider|divider|StageOut[116]~171_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[116]~123_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[116]~171_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[116]~123_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_11_result_int[6]~7\,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~9\);

-- Location: LCCOMB_X18_Y21_N20
\Div2|auto_generated|divider|divider|add_sub_11_result_int[10]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\ = CARRY((!\Div2|auto_generated|divider|divider|StageOut[119]~168_combout\ & (!\Div2|auto_generated|divider|divider|StageOut[119]~120_combout\ & 
-- !\Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[119]~168_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[119]~120_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_11_result_int[9]~13\,
	cout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\);

-- Location: LCCOMB_X18_Y21_N22
\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ = \Div2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div2|auto_generated|divider|divider|add_sub_11_result_int[10]~15_cout\,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\);

-- Location: LCCOMB_X18_Y22_N4
\Div2|auto_generated|divider|divider|StageOut[129]~133\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[129]~133_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[8]~10_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[129]~133_combout\);

-- Location: LCCOMB_X18_Y22_N2
\Div2|auto_generated|divider|divider|StageOut[128]~134\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[128]~134_combout\ = (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[7]~8_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[128]~134_combout\);

-- Location: LCCOMB_X19_Y21_N0
\Div2|auto_generated|divider|divider|StageOut[127]~159\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[127]~159_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[115]~172_combout\) # 
-- ((!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[115]~172_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[5]~4_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[127]~159_combout\);

-- Location: LCCOMB_X18_Y21_N2
\Div2|auto_generated|divider|divider|StageOut[126]~136\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[126]~136_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[126]~136_combout\);

-- Location: LCCOMB_X19_Y21_N6
\Div2|auto_generated|divider|divider|StageOut[125]~174\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[125]~174_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & (\sec_cnt|count_second\(6))) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sec_cnt|count_second\(6),
	datab => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[3]~0_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[125]~174_combout\);

-- Location: LCCOMB_X18_Y20_N0
\Div2|auto_generated|divider|divider|StageOut[124]~175\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[124]~175_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & ((\sec_cnt|count_second\(5)))) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & (\Div2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[2]~18_combout\,
	datab => \sec_cnt|count_second\(5),
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[124]~175_combout\);

-- Location: LCCOMB_X18_Y20_N18
\Div2|auto_generated|divider|divider|StageOut[123]~143\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[123]~143_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[2]~18_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[123]~143_combout\);

-- Location: LCCOMB_X18_Y22_N16
\Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ & (((\Div2|auto_generated|divider|divider|StageOut[125]~137_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[125]~174_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ & ((((\Div2|auto_generated|divider|divider|StageOut[125]~137_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[125]~174_combout\)))))
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\ & ((\Div2|auto_generated|divider|divider|StageOut[125]~137_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[125]~174_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[125]~137_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[125]~174_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_12_result_int[4]~3\,
	combout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~5\);

-- Location: LCCOMB_X18_Y22_N20
\Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ & (((\Div2|auto_generated|divider|divider|StageOut[127]~135_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[127]~159_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ & ((((\Div2|auto_generated|divider|divider|StageOut[127]~135_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[127]~159_combout\)))))
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\ & ((\Div2|auto_generated|divider|divider|StageOut[127]~135_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[127]~159_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[127]~135_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[127]~159_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_12_result_int[6]~7\,
	combout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\);

-- Location: LCCOMB_X18_Y22_N22
\Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\ = (\Div2|auto_generated|divider|divider|StageOut[128]~158_combout\ & (((!\Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\)))) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[128]~158_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[128]~134_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\)) # 
-- (!\Div2|auto_generated|divider|divider|StageOut[128]~134_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\) # (GND)))))
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ = CARRY(((!\Div2|auto_generated|divider|divider|StageOut[128]~158_combout\ & !\Div2|auto_generated|divider|divider|StageOut[128]~134_combout\)) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[128]~158_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[128]~134_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~9\,
	combout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\);

-- Location: LCCOMB_X18_Y22_N24
\Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ & (((\Div2|auto_generated|divider|divider|StageOut[129]~157_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[129]~133_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ & ((((\Div2|auto_generated|divider|divider|StageOut[129]~157_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[129]~133_combout\)))))
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\ & ((\Div2|auto_generated|divider|divider|StageOut[129]~157_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[129]~133_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[129]~157_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[129]~133_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~11\,
	combout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\);

-- Location: LCCOMB_X19_Y21_N26
\Div2|auto_generated|divider|divider|StageOut[130]~156\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[130]~156_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[118]~169_combout\) # 
-- ((\Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\ & !\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_10_result_int[8]~10_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|StageOut[118]~169_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[130]~156_combout\);

-- Location: LCCOMB_X18_Y22_N26
\Div2|auto_generated|divider|divider|add_sub_12_result_int[10]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\ = CARRY((!\Div2|auto_generated|divider|divider|StageOut[130]~132_combout\ & (!\Div2|auto_generated|divider|divider|StageOut[130]~156_combout\ & 
-- !\Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[130]~132_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[130]~156_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~13\,
	cout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\);

-- Location: LCCOMB_X18_Y22_N28
\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ = \Div2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div2|auto_generated|divider|divider|add_sub_12_result_int[10]~15_cout\,
	combout => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\);

-- Location: LCCOMB_X19_Y22_N30
\Div2|auto_generated|divider|divider|StageOut[141]~144\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[141]~144_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\ & !\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[9]~12_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[141]~144_combout\);

-- Location: LCCOMB_X19_Y22_N28
\Div2|auto_generated|divider|divider|StageOut[140]~145\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[140]~145_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\ & !\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[8]~10_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[140]~145_combout\);

-- Location: LCCOMB_X19_Y22_N18
\Div2|auto_generated|divider|divider|StageOut[139]~146\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[139]~146_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\ & !\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|add_sub_12_result_int[7]~8_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[139]~146_combout\);

-- Location: LCCOMB_X19_Y21_N2
\Div2|auto_generated|divider|divider|StageOut[126]~160\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[126]~160_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[114]~173_combout\) # 
-- ((\Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\ & !\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[114]~173_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_10_result_int[4]~2_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[126]~160_combout\);

-- Location: LCCOMB_X18_Y22_N6
\Div2|auto_generated|divider|divider|StageOut[138]~164\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[138]~164_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[126]~160_combout\) # 
-- ((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[5]~4_combout\,
	datad => \Div2|auto_generated|divider|divider|StageOut[126]~160_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[138]~164_combout\);

-- Location: LCCOMB_X19_Y22_N22
\Div2|auto_generated|divider|divider|StageOut[137]~148\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[137]~148_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\ & !\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[5]~4_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[137]~148_combout\);

-- Location: LCCOMB_X18_Y20_N16
\Div2|auto_generated|divider|divider|StageOut[136]~166\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[136]~166_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|StageOut[124]~175_combout\) # 
-- ((!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & \Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[124]~175_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[3]~0_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[136]~166_combout\);

-- Location: LCCOMB_X19_Y22_N26
\Div2|auto_generated|divider|divider|StageOut[135]~150\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[135]~150_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\ & !\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_12_result_int[3]~0_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[135]~150_combout\);

-- Location: LCCOMB_X15_Y22_N24
\Div2|auto_generated|divider|divider|StageOut[110]~153\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[110]~153_combout\ = (\sec_cnt|count_second\(3) & !\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(3),
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[110]~153_combout\);

-- Location: LCCOMB_X15_Y22_N26
\Div2|auto_generated|divider|divider|StageOut[110]~152\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[110]~152_combout\ = (\sec_cnt|count_second\(3) & \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sec_cnt|count_second\(3),
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \Div2|auto_generated|divider|divider|StageOut[110]~152_combout\);

-- Location: LCCOMB_X15_Y22_N0
\Div2|auto_generated|divider|divider|add_sub_11_result_int[1]~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\ = (\Div2|auto_generated|divider|divider|StageOut[110]~153_combout\) # (\Div2|auto_generated|divider|divider|StageOut[110]~152_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Div2|auto_generated|divider|divider|StageOut[110]~153_combout\,
	datad => \Div2|auto_generated|divider|divider|StageOut[110]~152_combout\,
	combout => \Div2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\);

-- Location: LCCOMB_X15_Y22_N4
\Div2|auto_generated|divider|divider|StageOut[134]~177\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|StageOut[134]~177_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\sec_cnt|count_second\(3)))) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & (\Div2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_11_result_int[1]~20_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \sec_cnt|count_second\(3),
	combout => \Div2|auto_generated|divider|divider|StageOut[134]~177_combout\);

-- Location: LCCOMB_X19_Y22_N0
\Div2|auto_generated|divider|divider|add_sub_13_result_int[3]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_13_result_int[3]~1_cout\ = CARRY((\Div2|auto_generated|divider|divider|StageOut[134]~155_combout\) # (\Div2|auto_generated|divider|divider|StageOut[134]~177_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[134]~155_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[134]~177_combout\,
	datad => VCC,
	cout => \Div2|auto_generated|divider|divider|add_sub_13_result_int[3]~1_cout\);

-- Location: LCCOMB_X19_Y22_N2
\Div2|auto_generated|divider|divider|add_sub_13_result_int[4]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_13_result_int[4]~3_cout\ = CARRY((!\Div2|auto_generated|divider|divider|StageOut[135]~167_combout\ & (!\Div2|auto_generated|divider|divider|StageOut[135]~150_combout\ & 
-- !\Div2|auto_generated|divider|divider|add_sub_13_result_int[3]~1_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[135]~167_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[135]~150_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_13_result_int[3]~1_cout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_13_result_int[4]~3_cout\);

-- Location: LCCOMB_X19_Y22_N4
\Div2|auto_generated|divider|divider|add_sub_13_result_int[5]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_13_result_int[5]~5_cout\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_13_result_int[4]~3_cout\ & ((\Div2|auto_generated|divider|divider|StageOut[136]~149_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[136]~166_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[136]~149_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[136]~166_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_13_result_int[4]~3_cout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_13_result_int[5]~5_cout\);

-- Location: LCCOMB_X19_Y22_N6
\Div2|auto_generated|divider|divider|add_sub_13_result_int[6]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_13_result_int[6]~7_cout\ = CARRY(((!\Div2|auto_generated|divider|divider|StageOut[137]~165_combout\ & !\Div2|auto_generated|divider|divider|StageOut[137]~148_combout\)) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_13_result_int[5]~5_cout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[137]~165_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[137]~148_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_13_result_int[5]~5_cout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_13_result_int[6]~7_cout\);

-- Location: LCCOMB_X19_Y22_N8
\Div2|auto_generated|divider|divider|add_sub_13_result_int[7]~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_13_result_int[7]~9_cout\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_13_result_int[6]~7_cout\ & ((\Div2|auto_generated|divider|divider|StageOut[138]~147_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[138]~164_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[138]~147_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[138]~164_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_13_result_int[6]~7_cout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_13_result_int[7]~9_cout\);

-- Location: LCCOMB_X19_Y22_N10
\Div2|auto_generated|divider|divider|add_sub_13_result_int[8]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_13_result_int[8]~11_cout\ = CARRY(((!\Div2|auto_generated|divider|divider|StageOut[139]~163_combout\ & !\Div2|auto_generated|divider|divider|StageOut[139]~146_combout\)) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_13_result_int[7]~9_cout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[139]~163_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[139]~146_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_13_result_int[7]~9_cout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_13_result_int[8]~11_cout\);

-- Location: LCCOMB_X19_Y22_N12
\Div2|auto_generated|divider|divider|add_sub_13_result_int[9]~13\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_13_result_int[9]~13_cout\ = CARRY((!\Div2|auto_generated|divider|divider|add_sub_13_result_int[8]~11_cout\ & ((\Div2|auto_generated|divider|divider|StageOut[140]~162_combout\) # 
-- (\Div2|auto_generated|divider|divider|StageOut[140]~145_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[140]~162_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[140]~145_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_13_result_int[8]~11_cout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_13_result_int[9]~13_cout\);

-- Location: LCCOMB_X19_Y22_N14
\Div2|auto_generated|divider|divider|add_sub_13_result_int[10]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\ = CARRY((!\Div2|auto_generated|divider|divider|StageOut[141]~161_combout\ & (!\Div2|auto_generated|divider|divider|StageOut[141]~144_combout\ & 
-- !\Div2|auto_generated|divider|divider|add_sub_13_result_int[9]~13_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|StageOut[141]~161_combout\,
	datab => \Div2|auto_generated|divider|divider|StageOut[141]~144_combout\,
	datad => VCC,
	cin => \Div2|auto_generated|divider|divider|add_sub_13_result_int[9]~13_cout\,
	cout => \Div2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\);

-- Location: LCCOMB_X19_Y22_N16
\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ = \Div2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Div2|auto_generated|divider|divider|add_sub_13_result_int[10]~15_cout\,
	combout => \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\);

-- Location: LCCOMB_X15_Y22_N30
\dec3|WideOr6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr6~0_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ $ 
-- (((\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\) # (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))) # (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & 
-- (((!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100000111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \dec3|WideOr6~0_combout\);

-- Location: LCCOMB_X15_Y22_N8
\dec3|WideOr5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr5~0_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & (((!\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & 
-- !\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ $ 
-- (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)) # (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011000111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \dec3|WideOr5~0_combout\);

-- Location: LCCOMB_X15_Y22_N18
\dec3|WideOr4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr4~0_combout\ = (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & 
-- ((\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\) # (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & 
-- (((!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \dec3|WideOr4~0_combout\);

-- Location: LCCOMB_X15_Y22_N28
\dec3|WideOr3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr3~0_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ $ 
-- (((\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\) # (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))) # (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & 
-- (((!\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & !\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)) # (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100100111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \dec3|WideOr3~0_combout\);

-- Location: LCCOMB_X15_Y22_N6
\dec3|WideOr2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr2~0_combout\ = ((\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\)) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101110101111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \dec3|WideOr2~0_combout\);

-- Location: LCCOMB_X15_Y22_N20
\dec3|WideOr1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr1~0_combout\ = (\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & 
-- (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)) # (!\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ & ((!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\))))) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ $ (!\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)) # 
-- (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111000100111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \dec3|WideOr1~0_combout\);

-- Location: LCCOMB_X15_Y22_N2
\dec3|WideOr0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr0~0_combout\ = (\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & ((\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\ $ 
-- (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\)))) # (!\Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\ & (\Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\ & 
-- ((\Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\) # (\Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Div2|auto_generated|divider|divider|add_sub_13_result_int[11]~16_combout\,
	datab => \Div2|auto_generated|divider|divider|add_sub_12_result_int[11]~16_combout\,
	datac => \Div2|auto_generated|divider|divider|add_sub_11_result_int[11]~16_combout\,
	datad => \Div2|auto_generated|divider|divider|add_sub_10_result_int[11]~16_combout\,
	combout => \dec3|WideOr0~0_combout\);

-- Location: PIN_L21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(1));

-- Location: PIN_M22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(2));

-- Location: PIN_V12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(3));

-- Location: PIN_W12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(4));

-- Location: PIN_U12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(5));

-- Location: PIN_U11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(6));

-- Location: PIN_M2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[7]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(7));

-- Location: PIN_M1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[8]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(8));

-- Location: PIN_L2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[9]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(9));

-- Location: PIN_R21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(1));

-- Location: PIN_T22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(2));

-- Location: PIN_T21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(3));

-- Location: PIN_J2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(0));

-- Location: PIN_J1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(1));

-- Location: PIN_H2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(2));

-- Location: PIN_H1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(3));

-- Location: PIN_F2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(4));

-- Location: PIN_F1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(5));

-- Location: PIN_E2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|ALT_INV_WideOr0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(6));

-- Location: PIN_E1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(0));

-- Location: PIN_H6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(1));

-- Location: PIN_H5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(2));

-- Location: PIN_H4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(3));

-- Location: PIN_G3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(4));

-- Location: PIN_D2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(5));

-- Location: PIN_D1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|ALT_INV_WideOr0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(6));

-- Location: PIN_G5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(0));

-- Location: PIN_G6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(1));

-- Location: PIN_C2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|WideOr4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(2));

-- Location: PIN_C1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(3));

-- Location: PIN_E3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(4));

-- Location: PIN_E4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(5));

-- Location: PIN_D3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|ALT_INV_WideOr0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(6));

-- Location: PIN_F4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(0));

-- Location: PIN_D5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(1));

-- Location: PIN_D6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|WideOr4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(2));

-- Location: PIN_J4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(3));

-- Location: PIN_L8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(4));

-- Location: PIN_F3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(5));

-- Location: PIN_D4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|ALT_INV_WideOr0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(6));
END structure;


