module sec_counter(input one_second,
						 input rst,
						 input clk,
						 output reg[13:0] count_second,
						 output reg[13:0] data);
						 
	always@(posedge clk)
		if(rst)
			count_second <= 14'b0;
		else if(one_second)
			if (count_second == 14'd9999)
				count_second <= 14'b0;
			else count_second <= count_second + 1'b1;
	
endmodule