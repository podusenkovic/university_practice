module sinus_maker(input 		   clk,
						 input  		   rst,
						 input [15:0]  amp,
						 input [15:0]  freq,
						 output [15:0] data,
						 output reg 	valid);
						 
	localparam const = 32'd562210; // 2*3.14159/48000*2^32
	wire signed [15:0] CosOut, SinOut;
	reg [15:0] x_in = 32000/1.647, y_in = 0;
	reg [31:0] angle = 0;
	reg [15:0] t  = 0;
	reg [10:0] dt = 0;
	
	cordic cordMdl(.clock(clk),
						.cosine(CosOut),
						.sine(SinOut),
						.x_start(x_in),
						.y_start(y_in),
						.angle(angle));		
				
	always@(posedge clk)
		if (rst) begin
			angle <= 0;
			t <= 0;
			dt <= 0;
			valid <= 0;
		end 
		else begin
			if (dt == 10'd1042) begin
				 t <= t + 1'b1;
				 dt <= 0;
				 valid <= 1;
			end 
			else begin 
				dt <= dt + 1'b1;
				valid <= 0;
			end
			angle <= freq * const * t;
		end
	
	assign data = (SinOut * amp) >> 8 + 16'h7FF;
						 
endmodule 