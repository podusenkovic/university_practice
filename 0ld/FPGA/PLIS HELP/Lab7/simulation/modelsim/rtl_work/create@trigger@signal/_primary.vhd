library verilog;
use verilog.vl_types.all;
entity createTriggerSignal is
    generic(
        MAX_TICKS       : vl_logic_vector(0 to 9) := (Hi1, Hi1, Hi1, Hi0, Hi0, Hi0, Hi0, Hi1, Hi1, Hi1)
    );
    port(
        one_sec         : in     vl_logic;
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        trigger         : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of MAX_TICKS : constant is 1;
end createTriggerSignal;
