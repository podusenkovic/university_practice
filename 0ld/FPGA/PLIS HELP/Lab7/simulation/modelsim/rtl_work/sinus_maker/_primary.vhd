library verilog;
use verilog.vl_types.all;
entity sinus_maker is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        amp             : in     vl_logic_vector(15 downto 0);
        freq            : in     vl_logic_vector(15 downto 0);
        data            : out    vl_logic_vector(15 downto 0);
        valid           : out    vl_logic
    );
end sinus_maker;
