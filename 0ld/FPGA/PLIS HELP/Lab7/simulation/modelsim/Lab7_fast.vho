-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 32-bit"
-- VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

-- DATE "06/05/2018 11:37:00"

-- 
-- Device: Altera EP2C20F484C7 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEII;
LIBRARY IEEE;
USE CYCLONEII.CYCLONEII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	main IS
    PORT (
	CLOCK_50 : IN std_logic;
	echo : IN std_logic;
	echo_2 : IN std_logic;
	SW : IN std_logic_vector(9 DOWNTO 0);
	KEY : IN std_logic_vector(3 DOWNTO 0);
	trigger : OUT std_logic;
	trigger_2 : OUT std_logic;
	LEDR : OUT std_logic_vector(9 DOWNTO 0);
	HEX0 : OUT std_logic_vector(6 DOWNTO 0);
	HEX1 : OUT std_logic_vector(6 DOWNTO 0);
	HEX2 : OUT std_logic_vector(6 DOWNTO 0);
	HEX3 : OUT std_logic_vector(6 DOWNTO 0)
	);
END main;

-- Design Ports Information
-- echo_2	=>  Location: PIN_D19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_L22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_L21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_M22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_V12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_W12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[5]	=>  Location: PIN_U12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_U11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_M2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[8]	=>  Location: PIN_M1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[9]	=>  Location: PIN_L2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[1]	=>  Location: PIN_R21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[2]	=>  Location: PIN_T22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[3]	=>  Location: PIN_T21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- trigger	=>  Location: PIN_A13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- trigger_2	=>  Location: PIN_H12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[0]	=>  Location: PIN_R20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[1]	=>  Location: PIN_R19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[2]	=>  Location: PIN_U19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[3]	=>  Location: PIN_Y19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[4]	=>  Location: PIN_T18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[5]	=>  Location: PIN_V19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[6]	=>  Location: PIN_Y18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[7]	=>  Location: PIN_U18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[8]	=>  Location: PIN_R18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[9]	=>  Location: PIN_R17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[0]	=>  Location: PIN_J2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[1]	=>  Location: PIN_J1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[2]	=>  Location: PIN_H2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[3]	=>  Location: PIN_H1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[4]	=>  Location: PIN_F2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[5]	=>  Location: PIN_F1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[6]	=>  Location: PIN_E2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[0]	=>  Location: PIN_E1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[1]	=>  Location: PIN_H6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[2]	=>  Location: PIN_H5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[3]	=>  Location: PIN_H4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[4]	=>  Location: PIN_G3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[5]	=>  Location: PIN_D2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[6]	=>  Location: PIN_D1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[0]	=>  Location: PIN_G5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[1]	=>  Location: PIN_G6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[2]	=>  Location: PIN_C2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[3]	=>  Location: PIN_C1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[4]	=>  Location: PIN_E3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[5]	=>  Location: PIN_E4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[6]	=>  Location: PIN_D3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[0]	=>  Location: PIN_F4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[1]	=>  Location: PIN_D5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[2]	=>  Location: PIN_D6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[3]	=>  Location: PIN_J4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[4]	=>  Location: PIN_L8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[5]	=>  Location: PIN_F3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[6]	=>  Location: PIN_D4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- KEY[0]	=>  Location: PIN_R22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- CLOCK_50	=>  Location: PIN_L1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- echo	=>  Location: PIN_D21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF main IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLOCK_50 : std_logic;
SIGNAL ww_echo : std_logic;
SIGNAL ww_echo_2 : std_logic;
SIGNAL ww_SW : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_KEY : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_trigger : std_logic;
SIGNAL ww_trigger_2 : std_logic;
SIGNAL ww_LEDR : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_HEX0 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX2 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX3 : std_logic_vector(6 DOWNTO 0);
SIGNAL \CLOCK_50~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_14~2_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_14~6_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_15~0_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_15~4_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_16~0_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_16~2_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_16~4_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_16~6_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_18~4_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_19~0_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_19~4_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_1~4_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_1~8_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_2~0_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_2~6_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_2~8_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_3~0_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_3~2_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[4]~6_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_4~6_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_4~8_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[4]~6_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_5~0_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_5~2_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_5~6_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_5~8_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[4]~6_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[4]~2_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[9]~12_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_6~0_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_6~6_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_6~8_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[4]~2_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[6]~6_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[9]~12_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[1]~12_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[1]~14_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[4]~2_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[8]~10_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[2]~18_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_7~8_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_8~0_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_8~4_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_8~6_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[4]~6_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[0]~14_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[0]~10_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_15_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_16_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_17_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_18_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[0]~16_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[1]~14_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[6]~8_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[3]~2_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[4]~4_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[5]~6_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[6]~8_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[0]~16_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[1]~14_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[6]~8_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_18_result_int[2]~0_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\ : std_logic;
SIGNAL \cTS2|cntr[3]~17_combout\ : std_logic;
SIGNAL \cTS2|cntr[6]~24_combout\ : std_logic;
SIGNAL \one_sec|cntr[0]~26_combout\ : std_logic;
SIGNAL \one_sec|cntr[2]~31_combout\ : std_logic;
SIGNAL \one_sec|cntr[3]~33_combout\ : std_logic;
SIGNAL \one_sec|cntr[5]~37_combout\ : std_logic;
SIGNAL \one_sec|cntr[9]~45_combout\ : std_logic;
SIGNAL \one_sec|cntr[18]~63_combout\ : std_logic;
SIGNAL \one_sec|cntr[23]~73_combout\ : std_logic;
SIGNAL \one_sec|cntr[24]~76\ : std_logic;
SIGNAL \one_sec|cntr[25]~77_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[28]~200_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[27]~203_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[26]~204_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[25]~207_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[24]~209_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[32]~212_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[30]~215_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[39]~217_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[38]~218_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[37]~219_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[36]~220_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[42]~226_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[68]~185_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[67]~187_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[66]~188_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[65]~191_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[64]~193_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[50]~230_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[48]~232_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[63]~195_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[75]~196_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[73]~198_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[72]~199_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[56]~236_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[55]~237_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[54]~239_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[70]~202_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[80]~205_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[79]~206_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[78]~207_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[64]~240_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[62]~242_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[60]~245_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[77]~209_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[89]~210_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[70]~246_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[69]~247_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[67]~249_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[66]~251_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[84]~216_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[95]~218_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[93]~220_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[75]~253_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[74]~254_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[73]~255_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[72]~257_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[91]~222_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[103]~224_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[102]~225_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[101]~226_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[99]~228_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[82]~258_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[81]~259_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[80]~260_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[79]~261_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[78]~262_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[98]~230_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[207]~59_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[206]~61_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[205]~62_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[204]~64_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[203]~67_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[202]~69_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[201]~71_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[110]~231_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[109]~232_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[107]~234_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[106]~235_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[88]~264_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[87]~265_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[86]~266_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[85]~267_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[84]~269_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[105]~237_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[200]~73_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[218]~74_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[215]~77_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[54]~40_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[50]~44_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[213]~79_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[49]~45_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[59]~49_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[48]~51_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[57]~52_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|StageOut[207]~0_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|StageOut[206]~3_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|StageOut[205]~4_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|StageOut[204]~6_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|StageOut[203]~8_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|StageOut[202]~11_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|StageOut[201]~12_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|StageOut[200]~15_combout\ : std_logic;
SIGNAL \oTH|LessThan0~2_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[116]~239_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[114]~241_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[94]~270_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[93]~271_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[90]~275_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[112]~243_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[124]~245_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[123]~246_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[122]~247_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[120]~249_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[96]~280_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[119]~251_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[131]~252_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[127]~256_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[102]~286_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[126]~258_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[63]~85_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[62]~87_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[61]~89_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[60]~90_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[67]~93_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[65]~95_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[73]~97_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[72]~98_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[71]~99_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[70]~101_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[75]~105_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[81]~109_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[80]~111_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[88]~112_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[86]~114_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[85]~115_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[90]~119_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[93]~121_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[69]~54_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[68]~55_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[67]~56_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[66]~57_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[211]~83_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[56]~58_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[65]~59_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[126]~127_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[125]~128_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[124]~131_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[123]~133_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[122]~134_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[121]~136_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[134]~138_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[133]~139_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[132]~140_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[130]~142_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[120]~143_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[120]~144_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[129]~145_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[148]~153_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[23]~33_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[22]~34_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[21]~36_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[28]~37_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[26]~40_combout\ : std_logic;
SIGNAL \cTS2|Equal0~0_combout\ : std_logic;
SIGNAL \one_sec|Equal0~0_combout\ : std_logic;
SIGNAL \cET|tacts[20]~0_combout\ : std_logic;
SIGNAL \cET|Equal0~3_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[40]~288_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[46]~291_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[45]~292_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[44]~293_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[52]~294_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[51]~295_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[58]~297_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[57]~298_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[82]~259_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[81]~260_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[63]~301_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[88]~263_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[68]~305_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[96]~265_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[94]~267_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[76]~306_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[53]~61_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[52]~62_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[51]~63_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[62]~66_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[61]~67_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[60]~68_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[48]~71_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[117]~274_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[92]~317_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[100]~318_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[99]~319_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[98]~320_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[130]~281_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[129]~282_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[106]~321_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[105]~322_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[104]~323_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[78]~126_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[77]~127_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[83]~128_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[87]~131_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[70]~73_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[142]~162_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[141]~163_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[140]~164_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[138]~166_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[150]~167_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[149]~168_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[147]~170_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[158]~171_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[18]~41_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[27]~49_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[25]~51_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[34]~324_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[33]~325_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[74]~284_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[71]~287_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[61]~332_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[85]~289_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[92]~290_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[217]~86_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[216]~87_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[214]~89_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[113]~293_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[91]~337_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[97]~338_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[103]~339_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[68]~134_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[66]~136_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[76]~138_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[131]~177_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[137]~179_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[145]~181_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[86]~297_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[100]~299_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[121]~302_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[128]~303_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[146]~184_combout\ : std_logic;
SIGNAL \CLOCK_50~combout\ : std_logic;
SIGNAL \CLOCK_50~clkctrl_outclk\ : std_logic;
SIGNAL \cTS2|cntr[0]~11_combout\ : std_logic;
SIGNAL \cTS2|cntr[0]~19_combout\ : std_logic;
SIGNAL \one_sec|cntr[0]~27\ : std_logic;
SIGNAL \one_sec|cntr[1]~29_combout\ : std_logic;
SIGNAL \one_sec|cntr[13]~54\ : std_logic;
SIGNAL \one_sec|cntr[14]~55_combout\ : std_logic;
SIGNAL \one_sec|cntr[14]~56\ : std_logic;
SIGNAL \one_sec|cntr[15]~58\ : std_logic;
SIGNAL \one_sec|cntr[16]~60\ : std_logic;
SIGNAL \one_sec|cntr[17]~61_combout\ : std_logic;
SIGNAL \one_sec|cntr[17]~62\ : std_logic;
SIGNAL \one_sec|cntr[18]~64\ : std_logic;
SIGNAL \one_sec|cntr[19]~65_combout\ : std_logic;
SIGNAL \one_sec|cntr[16]~59_combout\ : std_logic;
SIGNAL \one_sec|Equal0~5_combout\ : std_logic;
SIGNAL \one_sec|cntr[19]~66\ : std_logic;
SIGNAL \one_sec|cntr[20]~67_combout\ : std_logic;
SIGNAL \one_sec|cntr[20]~68\ : std_logic;
SIGNAL \one_sec|cntr[21]~70\ : std_logic;
SIGNAL \one_sec|cntr[22]~71_combout\ : std_logic;
SIGNAL \one_sec|cntr[22]~72\ : std_logic;
SIGNAL \one_sec|cntr[23]~74\ : std_logic;
SIGNAL \one_sec|cntr[24]~75_combout\ : std_logic;
SIGNAL \one_sec|cntr[21]~69_combout\ : std_logic;
SIGNAL \one_sec|Equal0~6_combout\ : std_logic;
SIGNAL \one_sec|Equal0~7_combout\ : std_logic;
SIGNAL \one_sec|cntr[10]~28_combout\ : std_logic;
SIGNAL \one_sec|cntr[1]~30\ : std_logic;
SIGNAL \one_sec|cntr[2]~32\ : std_logic;
SIGNAL \one_sec|cntr[3]~34\ : std_logic;
SIGNAL \one_sec|cntr[4]~35_combout\ : std_logic;
SIGNAL \one_sec|cntr[4]~36\ : std_logic;
SIGNAL \one_sec|cntr[5]~38\ : std_logic;
SIGNAL \one_sec|cntr[6]~39_combout\ : std_logic;
SIGNAL \one_sec|cntr[6]~40\ : std_logic;
SIGNAL \one_sec|cntr[7]~42\ : std_logic;
SIGNAL \one_sec|cntr[8]~43_combout\ : std_logic;
SIGNAL \one_sec|cntr[8]~44\ : std_logic;
SIGNAL \one_sec|cntr[9]~46\ : std_logic;
SIGNAL \one_sec|cntr[10]~47_combout\ : std_logic;
SIGNAL \one_sec|cntr[10]~48\ : std_logic;
SIGNAL \one_sec|cntr[11]~49_combout\ : std_logic;
SIGNAL \one_sec|cntr[11]~50\ : std_logic;
SIGNAL \one_sec|cntr[12]~51_combout\ : std_logic;
SIGNAL \one_sec|cntr[12]~52\ : std_logic;
SIGNAL \one_sec|cntr[13]~53_combout\ : std_logic;
SIGNAL \one_sec|cntr[15]~57_combout\ : std_logic;
SIGNAL \one_sec|Equal0~3_combout\ : std_logic;
SIGNAL \one_sec|Equal0~2_combout\ : std_logic;
SIGNAL \one_sec|cntr[7]~41_combout\ : std_logic;
SIGNAL \one_sec|Equal0~1_combout\ : std_logic;
SIGNAL \one_sec|Equal0~4_combout\ : std_logic;
SIGNAL \cTS2|cntr[0]~10_combout\ : std_logic;
SIGNAL \cTS2|cntr[0]~12\ : std_logic;
SIGNAL \cTS2|cntr[1]~13_combout\ : std_logic;
SIGNAL \cTS2|cntr[1]~14\ : std_logic;
SIGNAL \cTS2|cntr[2]~15_combout\ : std_logic;
SIGNAL \cTS2|cntr[2]~16\ : std_logic;
SIGNAL \cTS2|cntr[3]~18\ : std_logic;
SIGNAL \cTS2|cntr[4]~20_combout\ : std_logic;
SIGNAL \cTS2|cntr[4]~21\ : std_logic;
SIGNAL \cTS2|cntr[5]~23\ : std_logic;
SIGNAL \cTS2|cntr[6]~25\ : std_logic;
SIGNAL \cTS2|cntr[7]~26_combout\ : std_logic;
SIGNAL \cTS2|cntr[7]~27\ : std_logic;
SIGNAL \cTS2|cntr[8]~29\ : std_logic;
SIGNAL \cTS2|cntr[9]~30_combout\ : std_logic;
SIGNAL \cTS2|cntr[8]~28_combout\ : std_logic;
SIGNAL \cTS2|cntr[5]~22_combout\ : std_logic;
SIGNAL \cTS2|Equal0~1_combout\ : std_logic;
SIGNAL \cTS2|Equal0~2_combout\ : std_logic;
SIGNAL \cTS2|check~0_combout\ : std_logic;
SIGNAL \cTS2|check~regout\ : std_logic;
SIGNAL \cET|cntr[0]~22\ : std_logic;
SIGNAL \cET|cntr[1]~23_combout\ : std_logic;
SIGNAL \echo~combout\ : std_logic;
SIGNAL \cET|cntr[10]~63_combout\ : std_logic;
SIGNAL \cET|cntr[18]~58\ : std_logic;
SIGNAL \cET|cntr[19]~59_combout\ : std_logic;
SIGNAL \cET|cntr[16]~53_combout\ : std_logic;
SIGNAL \cET|cntr[15]~51_combout\ : std_logic;
SIGNAL \cET|Equal0~5_combout\ : std_logic;
SIGNAL \cET|Equal0~2_combout\ : std_logic;
SIGNAL \cET|cntr[4]~29_combout\ : std_logic;
SIGNAL \cET|Equal0~1_combout\ : std_logic;
SIGNAL \cET|cntr[19]~60\ : std_logic;
SIGNAL \cET|cntr[20]~61_combout\ : std_logic;
SIGNAL \cET|cntr[0]~21_combout\ : std_logic;
SIGNAL \cET|Equal0~0_combout\ : std_logic;
SIGNAL \cET|Equal0~4_combout\ : std_logic;
SIGNAL \cET|cntr[10]~64_combout\ : std_logic;
SIGNAL \cET|cntr[1]~24\ : std_logic;
SIGNAL \cET|cntr[2]~26\ : std_logic;
SIGNAL \cET|cntr[3]~27_combout\ : std_logic;
SIGNAL \cET|cntr[3]~28\ : std_logic;
SIGNAL \cET|cntr[4]~30\ : std_logic;
SIGNAL \cET|cntr[5]~31_combout\ : std_logic;
SIGNAL \cET|cntr[5]~32\ : std_logic;
SIGNAL \cET|cntr[6]~34\ : std_logic;
SIGNAL \cET|cntr[7]~35_combout\ : std_logic;
SIGNAL \cET|cntr[7]~36\ : std_logic;
SIGNAL \cET|cntr[8]~37_combout\ : std_logic;
SIGNAL \cET|cntr[8]~38\ : std_logic;
SIGNAL \cET|cntr[9]~39_combout\ : std_logic;
SIGNAL \cET|cntr[9]~40\ : std_logic;
SIGNAL \cET|cntr[10]~41_combout\ : std_logic;
SIGNAL \cET|cntr[10]~42\ : std_logic;
SIGNAL \cET|cntr[11]~43_combout\ : std_logic;
SIGNAL \cET|cntr[11]~44\ : std_logic;
SIGNAL \cET|cntr[12]~45_combout\ : std_logic;
SIGNAL \cET|cntr[12]~46\ : std_logic;
SIGNAL \cET|cntr[13]~48\ : std_logic;
SIGNAL \cET|cntr[14]~49_combout\ : std_logic;
SIGNAL \cET|cntr[14]~50\ : std_logic;
SIGNAL \cET|cntr[15]~52\ : std_logic;
SIGNAL \cET|cntr[16]~54\ : std_logic;
SIGNAL \cET|cntr[17]~55_combout\ : std_logic;
SIGNAL \cET|cntr[17]~56\ : std_logic;
SIGNAL \cET|cntr[18]~57_combout\ : std_logic;
SIGNAL \cET|tacts[20]~1_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_14~1\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_14~3\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_14~5\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_14~7\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_14~9\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_14~10_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[27]~202_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_14~4_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[26]~205_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[25]~206_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[24]~208_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_15~1\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_15~3\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_15~5\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_15~7\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_15~8_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_14~8_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[28]~201_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_15~9\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_15~11_cout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_15~12_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[34]~210_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_15~6_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[33]~211_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[32]~326_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_15~2_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[31]~213_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[30]~214_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_16~1\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_16~3\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_16~5\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_16~7\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_16~9\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_16~11_cout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_16~12_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_16~8_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[40]~216_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[39]~289_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_14~0_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[31]~327_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[38]~290_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[37]~328_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[36]~221_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_17~1\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_17~3\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_17~5\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_17~7\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_17~9\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_17~11_cout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_17~12_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_17~8_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[46]~222_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_17~6_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[45]~223_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_17~4_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[44]~224_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_17~2_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[43]~225_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_17~0_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[42]~227_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_18~1\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_18~3\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_18~5\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_18~7\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_18~9\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_18~11_cout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_18~12_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~7\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~9\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[6]~11_cout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[66]~189_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[65]~190_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[64]~192_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[63]~194_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[4]~6_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[68]~184_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[67]~186_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[4]~7\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[5]~9\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[6]~11_cout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[7]~12_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[74]~197_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[73]~285_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[72]~286_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[71]~200_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_18~6_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[51]~229_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[43]~329_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[50]~296_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_18~2_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[49]~231_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_18~0_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[48]~233_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_19~1\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_19~3\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_19~5\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_19~7\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_19~8_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_18~8_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[52]~228_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_19~9\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_19~11_cout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_19~12_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[58]~234_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_19~6_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[57]~235_combout\ : std_logic;
SIGNAL \cET|cntr[13]~47_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[49]~330_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[56]~299_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[55]~331_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[54]~238_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_1~1\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_1~3\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_1~5\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_1~7\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_1~9\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_1~11_cout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_1~12_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[70]~201_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[4]~7\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~6_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[75]~283_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[5]~9\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[6]~11_cout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[7]~12_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[82]~203_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[4]~6_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[81]~204_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[80]~261_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[79]~296_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[78]~288_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[64]~300_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_1~6_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[63]~241_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_19~2_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[62]~302_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_1~2_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[61]~243_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[60]~244_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_2~1\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_2~3\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_2~5\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_2~7\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_2~9\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_2~11_cout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_2~12_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[77]~208_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[4]~7\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[5]~9\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[6]~11_cout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[7]~12_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[89]~262_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[4]~6_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[88]~211_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[87]~212_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[86]~213_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[85]~214_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[70]~303_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[69]~304_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_2~4_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[68]~248_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_1~0_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[67]~333_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[66]~250_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_3~1\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_3~3\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_3~5\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_3~7\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_3~9\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_3~11_cout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_3~12_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[84]~215_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[4]~7\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[5]~9\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[6]~11_cout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[7]~12_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[2]~1\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[3]~2_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[3]~3\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[4]~5\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[5]~7\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[6]~9\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[7]~11_cout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[8]~12_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[132]~176_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[126]~126_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[5]~6_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[125]~129_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[124]~130_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[123]~132_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[2]~0_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[122]~135_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[96]~217_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[87]~264_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[95]~266_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[94]~219_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[93]~298_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[92]~221_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_3~8_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[76]~252_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[75]~307_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_2~2_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[74]~308_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[73]~334_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[72]~256_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_4~1\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_4~3\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_4~5\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_4~7\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_4~9\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_4~11_cout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_4~12_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[91]~223_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[4]~7\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[5]~9\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[6]~11_cout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[1]~14_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[121]~137_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[2]~1\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[3]~3\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[4]~5\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[5]~7\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[6]~9\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[7]~11_cout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[8]~12_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[131]~141_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[130]~178_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[103]~268_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[102]~269_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[101]~270_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[100]~227_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[99]~291_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_3~6_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[82]~309_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_3~4_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[81]~310_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[80]~311_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[79]~335_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_4~0_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[78]~263_combout\ : std_logic;
SIGNAL \cET|cntr[6]~33_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_5~1\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_5~3\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_5~5\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_5~7\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_5~9\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_5~11_cout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_5~12_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[98]~229_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[4]~7\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[5]~9\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[6]~11_cout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[7]~12_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[129]~161_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[2]~1\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[3]~3\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[4]~5\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[5]~6_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[134]~174_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[4]~4_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[133]~175_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[5]~7\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[6]~9\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[7]~11_cout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[8]~12_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[141]~147_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[4]~4_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[140]~148_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[3]~2_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[139]~149_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[2]~0_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[138]~150_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[110]~271_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[109]~272_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[108]~233_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[107]~300_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[106]~292_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[88]~312_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_4~4_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[87]~313_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_4~2_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[86]~314_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[85]~336_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[84]~268_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_6~1\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_6~3\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_6~5\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_6~7\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_6~9\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_6~11_cout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_6~12_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[105]~236_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[4]~7\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[5]~9\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[6]~11_cout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[7]~12_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[137]~180_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_18_result_int[2]~1\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_18_result_int[3]~3\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_18_result_int[4]~5\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_18_result_int[5]~7\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_18_result_int[6]~8_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[142]~146_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_18_result_int[6]~9\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_18_result_int[7]~11_cout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_18_result_int[8]~12_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[150]~151_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_18_result_int[5]~6_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[149]~152_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[2]~0_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[139]~165_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[148]~169_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_18_result_int[3]~2_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[147]~154_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[146]~155_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[117]~238_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[108]~273_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[116]~275_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[115]~240_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[114]~301_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[113]~242_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[94]~315_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_5~4_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[93]~316_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_6~4_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[92]~272_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_6~2_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[91]~273_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[90]~274_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_7~1\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_7~3\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_7~5\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_7~7\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_7~9\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_7~11_cout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_7~12_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[112]~244_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[4]~7\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[5]~9\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[6]~11_cout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[7]~12_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[145]~182_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_19_result_int[2]~1\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_19_result_int[3]~3\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_19_result_int[4]~5\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_19_result_int[5]~7\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_19_result_int[6]~9\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_19_result_int[7]~11_cout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_19_result_int[8]~12_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_19_result_int[6]~8_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[158]~156_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_19_result_int[5]~6_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[157]~157_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_19_result_int[4]~4_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[156]~158_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_3_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_3_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_3_result_int[4]~6_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_3_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[18]~30_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[17]~31_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_3_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[16]~32_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_19_result_int[3]~2_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[155]~185_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[15]~44_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[4]~7_cout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|LessThan0~5_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[3]~1\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[4]~3\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[5]~5\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[6]~7\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[7]~9\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[8]~11\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[9]~13\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[10]~15_cout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[8]~10_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[218]~85_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[207]~58_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[206]~60_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[7]~8_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[205]~63_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[6]~6_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[204]~65_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[203]~66_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[202]~68_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[201]~70_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[200]~72_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[3]~1\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[4]~3\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[5]~5\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[6]~7\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[7]~9\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[8]~11\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[9]~13\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[10]~15_cout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[11]~16_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[8]~10_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[217]~75_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[7]~8_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[216]~76_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[5]~4_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[215]~88_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[5]~4_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[214]~78_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[2]~1\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[3]~3\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[4]~5\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[6]~9\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[54]~60_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[5]~6_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[53]~41_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[4]~4_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[52]~42_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[3]~2_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[51]~43_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[50]~64_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[3]~0_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[213]~90_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[49]~65_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[2]~1\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[3]~3\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[4]~5\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[5]~7\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[6]~9\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[7]~11_cout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[5]~6_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[61]~47_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[4]~4_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[60]~48_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[59]~69_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[2]~0_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[58]~50_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[3]~0_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[212]~80_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[2]~18_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[212]~91_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[0]~14_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[57]~72_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[4]~5\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[6]~8_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[6]~8_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[62]~46_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[6]~9\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[7]~11_cout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[8]~12_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[70]~53_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[69]~74_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[68]~75_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[58]~70_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[67]~76_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[66]~77_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[199]~81_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[1]~20_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[199]~82_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[2]~18_combout\ : std_logic;
SIGNAL \oTH|Mod2|auto_generated|divider|divider|StageOut[211]~84_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[56]~78_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|StageOut[65]~79_combout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_9_result_int[2]~1_cout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_9_result_int[3]~3_cout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_9_result_int[4]~5_cout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_9_result_int[5]~7_cout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_9_result_int[6]~9_cout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_9_result_int[7]~11_cout\ : std_logic;
SIGNAL \oTH|Div7|auto_generated|divider|divider|add_sub_9_result_int[8]~12_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_18_result_int[4]~4_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[157]~172_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[17]~42_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[23]~46_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[156]~173_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[16]~43_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[22]~47_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[15]~45_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[21]~35_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_19_result_int[2]~0_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[20]~53_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[4]~7_cout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[28]~48_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[27]~38_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[20]~52_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[26]~39_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[4]~6_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[124]~277_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[115]~276_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[123]~278_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[122]~279_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[121]~248_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[120]~294_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[100]~276_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_7~6_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[99]~277_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_7~4_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[98]~278_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_7~2_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[97]~279_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_7~0_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[96]~281_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_8~1\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_8~3\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_8~5\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_8~7\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_8~9\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_8~11_cout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_8~12_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[119]~250_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[4]~7\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[5]~9\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[6]~11_cout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[7]~12_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[144]~159_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_18_result_int[0]~14_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[144]~160_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|add_sub_19_result_int[1]~14_combout\ : std_logic;
SIGNAL \oTH|Mod1|auto_generated|divider|divider|StageOut[153]~183_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|StageOut[25]~50_combout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_6_result_int[1]~1_cout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_6_result_int[2]~3_cout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_6_result_int[3]~5_cout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_6_result_int[4]~7_cout\ : std_logic;
SIGNAL \oTH|Div4|auto_generated|divider|divider|add_sub_6_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|outter[0]~4_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[3]~1\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[4]~3\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[5]~5\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[6]~7\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[7]~9\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[8]~11\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[9]~12_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[9]~13\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[10]~15_cout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|StageOut[207]~1_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|StageOut[206]~2_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[7]~8_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|StageOut[205]~5_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[6]~6_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|StageOut[204]~7_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[5]~4_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|StageOut[203]~9_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|StageOut[202]~10_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[3]~0_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|StageOut[201]~13_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|StageOut[200]~14_combout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_19_result_int[3]~1_cout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_19_result_int[4]~3_cout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_19_result_int[5]~5_cout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_19_result_int[6]~7_cout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_19_result_int[7]~9_cout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_19_result_int[8]~11_cout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_19_result_int[9]~13_cout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_19_result_int[10]~15_cout\ : std_logic;
SIGNAL \oTH|Div10|auto_generated|divider|divider|add_sub_19_result_int[11]~16_combout\ : std_logic;
SIGNAL \oTH|LessThan0~3_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[4]~7_cout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[61]~88_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[0]~10_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[60]~91_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[67]~135_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[63]~84_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[62]~86_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_13_result_int[4]~7_cout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_13_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[73]~124_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_13_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[68]~92_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[66]~94_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_13_result_int[0]~10_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[65]~96_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[4]~7_cout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[72]~125_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[71]~137_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[70]~100_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_15_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_15_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_15_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_15_result_int[4]~7_cout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_15_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[78]~102_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_15_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[77]~103_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_15_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[76]~104_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_15_result_int[0]~10_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[75]~106_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_16_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_16_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_16_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_16_result_int[4]~7_cout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_16_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[81]~139_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[80]~110_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_17_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_17_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_16_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[82]~129_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[88]~130_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_16_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[83]~107_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[82]~108_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_17_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_17_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_17_result_int[4]~7_cout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_17_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[87]~113_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_16_result_int[0]~10_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[86]~140_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_17_result_int[0]~10_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[85]~116_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_18_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_18_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_18_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_18_result_int[4]~7_cout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_18_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[93]~133_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_18_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[92]~117_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[91]~141_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_18_result_int[0]~10_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[90]~120_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_19_result_int[1]~1\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_19_result_int[2]~3\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_19_result_int[3]~5\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_19_result_int[4]~7_cout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_19_result_int[5]~8_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_19_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|outter[1]~6_combout\ : std_logic;
SIGNAL \oTH|outter[1]~7_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_19_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_18_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[91]~118_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[97]~123_combout\ : std_logic;
SIGNAL \oTH|outter[3]~15_combout\ : std_logic;
SIGNAL \oTH|outter[2]~14_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[4]~6_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[131]~280_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[130]~253_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[129]~254_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[2]~2_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[128]~255_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[0]~14_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[127]~295_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_8~8_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[106]~282_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[105]~283_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[104]~284_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_8~2_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[103]~285_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|StageOut[102]~287_combout\ : std_logic;
SIGNAL \cET|cntr[2]~25_combout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_9~1_cout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_9~3_cout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_9~5_cout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_9~7_cout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_9~9_cout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_9~11_cout\ : std_logic;
SIGNAL \oTH|Div0|auto_generated|divider|divider|op_9~12_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|StageOut[126]~257_combout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_19_result_int[1]~1_cout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_19_result_int[2]~3_cout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_19_result_int[3]~5_cout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_19_result_int[4]~7_cout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_19_result_int[5]~9_cout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_19_result_int[6]~11_cout\ : std_logic;
SIGNAL \oTH|Div1|auto_generated|divider|divider|add_sub_19_result_int[7]~12_combout\ : std_logic;
SIGNAL \oTH|outter[0]~5_combout\ : std_logic;
SIGNAL \oTH|dec0|WideOr6~0_combout\ : std_logic;
SIGNAL \oTH|dec0|WideOr5~0_combout\ : std_logic;
SIGNAL \oTH|dec0|WideOr4~0_combout\ : std_logic;
SIGNAL \oTH|dec0|WideOr3~0_combout\ : std_logic;
SIGNAL \oTH|dec0|WideOr2~0_combout\ : std_logic;
SIGNAL \oTH|dec0|WideOr1~0_combout\ : std_logic;
SIGNAL \oTH|dec0|WideOr0~0_combout\ : std_logic;
SIGNAL \oTH|outter[5]~9_combout\ : std_logic;
SIGNAL \oTH|outter[4]~8_combout\ : std_logic;
SIGNAL \oTH|outter[7]~11_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_17_result_int[1]~0_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[92]~132_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|add_sub_19_result_int[3]~4_combout\ : std_logic;
SIGNAL \oTH|Mod0|auto_generated|divider|divider|StageOut[98]~122_combout\ : std_logic;
SIGNAL \oTH|LessThan0~4_combout\ : std_logic;
SIGNAL \oTH|outter[6]~10_combout\ : std_logic;
SIGNAL \oTH|dec1|WideOr6~0_combout\ : std_logic;
SIGNAL \oTH|dec1|WideOr5~0_combout\ : std_logic;
SIGNAL \oTH|dec1|WideOr4~0_combout\ : std_logic;
SIGNAL \oTH|dec1|WideOr3~0_combout\ : std_logic;
SIGNAL \oTH|dec1|WideOr2~0_combout\ : std_logic;
SIGNAL \oTH|dec1|WideOr1~0_combout\ : std_logic;
SIGNAL \oTH|dec1|WideOr0~0_combout\ : std_logic;
SIGNAL \oTH|outter[8]~12_combout\ : std_logic;
SIGNAL \oTH|dec2|WideOr2~0_combout\ : std_logic;
SIGNAL \oTH|outter[9]~13_combout\ : std_logic;
SIGNAL \oTH|dec2|WideOr4~2_combout\ : std_logic;
SIGNAL \oTH|dec2|WideOr2~1_combout\ : std_logic;
SIGNAL \oTH|dec2|WideOr1~2_combout\ : std_logic;
SIGNAL \oTH|LessThan0~6_combout\ : std_logic;
SIGNAL \cET|cntr\ : std_logic_vector(20 DOWNTO 0);
SIGNAL \cET|tacts\ : std_logic_vector(20 DOWNTO 0);
SIGNAL \KEY~combout\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \cTS2|cntr\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \one_sec|cntr\ : std_logic_vector(25 DOWNTO 0);
SIGNAL \oTH|ALT_INV_LessThan0~6_combout\ : std_logic;
SIGNAL \oTH|dec2|ALT_INV_WideOr2~1_combout\ : std_logic;
SIGNAL \oTH|ALT_INV_outter[9]~13_combout\ : std_logic;
SIGNAL \oTH|dec2|ALT_INV_WideOr2~0_combout\ : std_logic;
SIGNAL \oTH|ALT_INV_outter[8]~12_combout\ : std_logic;
SIGNAL \oTH|dec1|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \oTH|dec0|ALT_INV_WideOr0~0_combout\ : std_logic;

BEGIN

ww_CLOCK_50 <= CLOCK_50;
ww_echo <= echo;
ww_echo_2 <= echo_2;
ww_SW <= SW;
ww_KEY <= KEY;
trigger <= ww_trigger;
trigger_2 <= ww_trigger_2;
LEDR <= ww_LEDR;
HEX0 <= ww_HEX0;
HEX1 <= ww_HEX1;
HEX2 <= ww_HEX2;
HEX3 <= ww_HEX3;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\CLOCK_50~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \CLOCK_50~combout\);
\oTH|ALT_INV_LessThan0~6_combout\ <= NOT \oTH|LessThan0~6_combout\;
\oTH|dec2|ALT_INV_WideOr2~1_combout\ <= NOT \oTH|dec2|WideOr2~1_combout\;
\oTH|ALT_INV_outter[9]~13_combout\ <= NOT \oTH|outter[9]~13_combout\;
\oTH|dec2|ALT_INV_WideOr2~0_combout\ <= NOT \oTH|dec2|WideOr2~0_combout\;
\oTH|ALT_INV_outter[8]~12_combout\ <= NOT \oTH|outter[8]~12_combout\;
\oTH|dec1|ALT_INV_WideOr0~0_combout\ <= NOT \oTH|dec1|WideOr0~0_combout\;
\oTH|dec0|ALT_INV_WideOr0~0_combout\ <= NOT \oTH|dec0|WideOr0~0_combout\;

-- Location: LCCOMB_X31_Y19_N6
\oTH|Div0|auto_generated|divider|divider|op_14~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_14~2_combout\ = (\cET|tacts\(17) & (\oTH|Div0|auto_generated|divider|divider|op_14~1\ & VCC)) # (!\cET|tacts\(17) & (!\oTH|Div0|auto_generated|divider|divider|op_14~1\))
-- \oTH|Div0|auto_generated|divider|divider|op_14~3\ = CARRY((!\cET|tacts\(17) & !\oTH|Div0|auto_generated|divider|divider|op_14~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \cET|tacts\(17),
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_14~1\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_14~2_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_14~3\);

-- Location: LCCOMB_X31_Y19_N10
\oTH|Div0|auto_generated|divider|divider|op_14~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_14~6_combout\ = (\cET|tacts\(19) & (!\oTH|Div0|auto_generated|divider|divider|op_14~5\)) # (!\cET|tacts\(19) & ((\oTH|Div0|auto_generated|divider|divider|op_14~5\) # (GND)))
-- \oTH|Div0|auto_generated|divider|divider|op_14~7\ = CARRY((!\oTH|Div0|auto_generated|divider|divider|op_14~5\) # (!\cET|tacts\(19)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \cET|tacts\(19),
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_14~5\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_14~6_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_14~7\);

-- Location: LCCOMB_X29_Y19_N10
\oTH|Div0|auto_generated|divider|divider|op_15~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_15~0_combout\ = \cET|tacts\(15) $ (VCC)
-- \oTH|Div0|auto_generated|divider|divider|op_15~1\ = CARRY(\cET|tacts\(15))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \cET|tacts\(15),
	datad => VCC,
	combout => \oTH|Div0|auto_generated|divider|divider|op_15~0_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_15~1\);

-- Location: LCCOMB_X29_Y19_N14
\oTH|Div0|auto_generated|divider|divider|op_15~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_15~4_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_15~3\ & ((((\oTH|Div0|auto_generated|divider|divider|StageOut[25]~207_combout\) # 
-- (\oTH|Div0|auto_generated|divider|divider|StageOut[25]~206_combout\))))) # (!\oTH|Div0|auto_generated|divider|divider|op_15~3\ & ((\oTH|Div0|auto_generated|divider|divider|StageOut[25]~207_combout\) # 
-- ((\oTH|Div0|auto_generated|divider|divider|StageOut[25]~206_combout\) # (GND))))
-- \oTH|Div0|auto_generated|divider|divider|op_15~5\ = CARRY((\oTH|Div0|auto_generated|divider|divider|StageOut[25]~207_combout\) # ((\oTH|Div0|auto_generated|divider|divider|StageOut[25]~206_combout\) # (!\oTH|Div0|auto_generated|divider|divider|op_15~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[25]~207_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[25]~206_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_15~3\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_15~4_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_15~5\);

-- Location: LCCOMB_X30_Y19_N16
\oTH|Div0|auto_generated|divider|divider|op_16~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_16~0_combout\ = \cET|tacts\(14) $ (VCC)
-- \oTH|Div0|auto_generated|divider|divider|op_16~1\ = CARRY(\cET|tacts\(14))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \cET|tacts\(14),
	datad => VCC,
	combout => \oTH|Div0|auto_generated|divider|divider|op_16~0_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_16~1\);

-- Location: LCCOMB_X30_Y19_N18
\oTH|Div0|auto_generated|divider|divider|op_16~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_16~2_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_16~1\ & (((\oTH|Div0|auto_generated|divider|divider|StageOut[30]~215_combout\) # 
-- (\oTH|Div0|auto_generated|divider|divider|StageOut[30]~214_combout\)))) # (!\oTH|Div0|auto_generated|divider|divider|op_16~1\ & (!\oTH|Div0|auto_generated|divider|divider|StageOut[30]~215_combout\ & 
-- (!\oTH|Div0|auto_generated|divider|divider|StageOut[30]~214_combout\)))
-- \oTH|Div0|auto_generated|divider|divider|op_16~3\ = CARRY((!\oTH|Div0|auto_generated|divider|divider|StageOut[30]~215_combout\ & (!\oTH|Div0|auto_generated|divider|divider|StageOut[30]~214_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_16~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[30]~215_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[30]~214_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_16~1\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_16~2_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_16~3\);

-- Location: LCCOMB_X30_Y19_N20
\oTH|Div0|auto_generated|divider|divider|op_16~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_16~4_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_16~3\ & ((((\oTH|Div0|auto_generated|divider|divider|StageOut[31]~327_combout\) # 
-- (\oTH|Div0|auto_generated|divider|divider|StageOut[31]~213_combout\))))) # (!\oTH|Div0|auto_generated|divider|divider|op_16~3\ & ((\oTH|Div0|auto_generated|divider|divider|StageOut[31]~327_combout\) # 
-- ((\oTH|Div0|auto_generated|divider|divider|StageOut[31]~213_combout\) # (GND))))
-- \oTH|Div0|auto_generated|divider|divider|op_16~5\ = CARRY((\oTH|Div0|auto_generated|divider|divider|StageOut[31]~327_combout\) # ((\oTH|Div0|auto_generated|divider|divider|StageOut[31]~213_combout\) # (!\oTH|Div0|auto_generated|divider|divider|op_16~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[31]~327_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[31]~213_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_16~3\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_16~4_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_16~5\);

-- Location: LCCOMB_X30_Y19_N22
\oTH|Div0|auto_generated|divider|divider|op_16~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_16~6_combout\ = (\oTH|Div0|auto_generated|divider|divider|StageOut[32]~212_combout\ & (((!\oTH|Div0|auto_generated|divider|divider|op_16~5\)))) # 
-- (!\oTH|Div0|auto_generated|divider|divider|StageOut[32]~212_combout\ & ((\oTH|Div0|auto_generated|divider|divider|StageOut[32]~326_combout\ & (!\oTH|Div0|auto_generated|divider|divider|op_16~5\)) # 
-- (!\oTH|Div0|auto_generated|divider|divider|StageOut[32]~326_combout\ & ((\oTH|Div0|auto_generated|divider|divider|op_16~5\) # (GND)))))
-- \oTH|Div0|auto_generated|divider|divider|op_16~7\ = CARRY(((!\oTH|Div0|auto_generated|divider|divider|StageOut[32]~212_combout\ & !\oTH|Div0|auto_generated|divider|divider|StageOut[32]~326_combout\)) # (!\oTH|Div0|auto_generated|divider|divider|op_16~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[32]~212_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[32]~326_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_16~5\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_16~6_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_16~7\);

-- Location: LCCOMB_X31_Y20_N4
\oTH|Div0|auto_generated|divider|divider|op_18~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_18~4_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_18~3\ & ((((\oTH|Div0|auto_generated|divider|divider|StageOut[43]~329_combout\) # 
-- (\oTH|Div0|auto_generated|divider|divider|StageOut[43]~225_combout\))))) # (!\oTH|Div0|auto_generated|divider|divider|op_18~3\ & ((\oTH|Div0|auto_generated|divider|divider|StageOut[43]~329_combout\) # 
-- ((\oTH|Div0|auto_generated|divider|divider|StageOut[43]~225_combout\) # (GND))))
-- \oTH|Div0|auto_generated|divider|divider|op_18~5\ = CARRY((\oTH|Div0|auto_generated|divider|divider|StageOut[43]~329_combout\) # ((\oTH|Div0|auto_generated|divider|divider|StageOut[43]~225_combout\) # (!\oTH|Div0|auto_generated|divider|divider|op_18~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[43]~329_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[43]~225_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_18~3\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_18~4_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_18~5\);

-- Location: LCCOMB_X27_Y19_N12
\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_14~10_combout\ & (!\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~7\ & VCC)) # 
-- (!\oTH|Div0|auto_generated|divider|divider|op_14~10_combout\ & (\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~7\ $ (GND)))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~9\ = CARRY((!\oTH|Div0|auto_generated|divider|divider|op_14~10_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101000000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|op_14~10_combout\,
	datad => VCC,
	cin => \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~7\,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~9\);

-- Location: LCCOMB_X31_Y21_N4
\oTH|Div0|auto_generated|divider|divider|op_19~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_19~0_combout\ = \cET|tacts\(11) $ (VCC)
-- \oTH|Div0|auto_generated|divider|divider|op_19~1\ = CARRY(\cET|tacts\(11))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \cET|tacts\(11),
	datad => VCC,
	combout => \oTH|Div0|auto_generated|divider|divider|op_19~0_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_19~1\);

-- Location: LCCOMB_X31_Y21_N8
\oTH|Div0|auto_generated|divider|divider|op_19~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_19~4_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_19~3\ & ((((\oTH|Div0|auto_generated|divider|divider|StageOut[49]~330_combout\) # 
-- (\oTH|Div0|auto_generated|divider|divider|StageOut[49]~231_combout\))))) # (!\oTH|Div0|auto_generated|divider|divider|op_19~3\ & ((\oTH|Div0|auto_generated|divider|divider|StageOut[49]~330_combout\) # 
-- ((\oTH|Div0|auto_generated|divider|divider|StageOut[49]~231_combout\) # (GND))))
-- \oTH|Div0|auto_generated|divider|divider|op_19~5\ = CARRY((\oTH|Div0|auto_generated|divider|divider|StageOut[49]~330_combout\) # ((\oTH|Div0|auto_generated|divider|divider|StageOut[49]~231_combout\) # (!\oTH|Div0|auto_generated|divider|divider|op_19~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[49]~330_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[49]~231_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_19~3\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_19~4_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_19~5\);

-- Location: LCCOMB_X26_Y19_N4
\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[3]~4_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[2]~3\ & (((\oTH|Div1|auto_generated|divider|divider|StageOut[65]~191_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[65]~190_combout\)))) # (!\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[2]~3\ & ((((\oTH|Div1|auto_generated|divider|divider|StageOut[65]~191_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[65]~190_combout\)))))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[3]~5\ = CARRY((!\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[2]~3\ & ((\oTH|Div1|auto_generated|divider|divider|StageOut[65]~191_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[65]~190_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[65]~191_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[65]~190_combout\,
	datad => VCC,
	cin => \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[2]~3\,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[3]~4_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[3]~5\);

-- Location: LCCOMB_X26_Y19_N8
\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[4]~7\ & (((\oTH|Div1|auto_generated|divider|divider|StageOut[67]~187_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[67]~186_combout\)))) # (!\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[4]~7\ & ((((\oTH|Div1|auto_generated|divider|divider|StageOut[67]~187_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[67]~186_combout\)))))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[5]~9\ = CARRY((!\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[4]~7\ & ((\oTH|Div1|auto_generated|divider|divider|StageOut[67]~187_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[67]~186_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[67]~187_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[67]~186_combout\,
	datad => VCC,
	cin => \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[4]~7\,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[5]~9\);

-- Location: LCCOMB_X29_Y21_N14
\oTH|Div0|auto_generated|divider|divider|op_1~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_1~4_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_1~3\ & ((((\oTH|Div0|auto_generated|divider|divider|StageOut[55]~237_combout\) # 
-- (\oTH|Div0|auto_generated|divider|divider|StageOut[55]~331_combout\))))) # (!\oTH|Div0|auto_generated|divider|divider|op_1~3\ & ((\oTH|Div0|auto_generated|divider|divider|StageOut[55]~237_combout\) # 
-- ((\oTH|Div0|auto_generated|divider|divider|StageOut[55]~331_combout\) # (GND))))
-- \oTH|Div0|auto_generated|divider|divider|op_1~5\ = CARRY((\oTH|Div0|auto_generated|divider|divider|StageOut[55]~237_combout\) # ((\oTH|Div0|auto_generated|divider|divider|StageOut[55]~331_combout\) # (!\oTH|Div0|auto_generated|divider|divider|op_1~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[55]~237_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[55]~331_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_1~3\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_1~4_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_1~5\);

-- Location: LCCOMB_X29_Y21_N18
\oTH|Div0|auto_generated|divider|divider|op_1~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_1~8_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_1~7\ & (((\oTH|Div0|auto_generated|divider|divider|StageOut[57]~298_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[57]~235_combout\)))) 
-- # (!\oTH|Div0|auto_generated|divider|divider|op_1~7\ & ((((\oTH|Div0|auto_generated|divider|divider|StageOut[57]~298_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[57]~235_combout\)))))
-- \oTH|Div0|auto_generated|divider|divider|op_1~9\ = CARRY((!\oTH|Div0|auto_generated|divider|divider|op_1~7\ & ((\oTH|Div0|auto_generated|divider|divider|StageOut[57]~298_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[57]~235_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[57]~298_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[57]~235_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_1~7\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_1~8_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_1~9\);

-- Location: LCCOMB_X26_Y18_N16
\oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[1]~0_combout\ = (((\oTH|Div1|auto_generated|divider|divider|StageOut[70]~202_combout\) # (\oTH|Div1|auto_generated|divider|divider|StageOut[70]~201_combout\)))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[1]~1\ = CARRY((\oTH|Div1|auto_generated|divider|divider|StageOut[70]~202_combout\) # (\oTH|Div1|auto_generated|divider|divider|StageOut[70]~201_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[70]~202_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[70]~201_combout\,
	datad => VCC,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[1]~0_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[1]~1\);

-- Location: LCCOMB_X26_Y18_N18
\oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[2]~2_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[1]~1\ & (((\oTH|Div1|auto_generated|divider|divider|StageOut[71]~287_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[71]~200_combout\)))) # (!\oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[1]~1\ & (!\oTH|Div1|auto_generated|divider|divider|StageOut[71]~287_combout\ & 
-- (!\oTH|Div1|auto_generated|divider|divider|StageOut[71]~200_combout\)))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[2]~3\ = CARRY((!\oTH|Div1|auto_generated|divider|divider|StageOut[71]~287_combout\ & (!\oTH|Div1|auto_generated|divider|divider|StageOut[71]~200_combout\ & 
-- !\oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[71]~287_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[71]~200_combout\,
	datad => VCC,
	cin => \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[1]~1\,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[2]~2_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[2]~3\);

-- Location: LCCOMB_X26_Y18_N20
\oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[3]~4_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[2]~3\ & (((\oTH|Div1|auto_generated|divider|divider|StageOut[72]~199_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[72]~286_combout\)))) # (!\oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[2]~3\ & ((((\oTH|Div1|auto_generated|divider|divider|StageOut[72]~199_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[72]~286_combout\)))))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[3]~5\ = CARRY((!\oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[2]~3\ & ((\oTH|Div1|auto_generated|divider|divider|StageOut[72]~199_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[72]~286_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[72]~199_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[72]~286_combout\,
	datad => VCC,
	cin => \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[2]~3\,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[3]~4_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[3]~5\);

-- Location: LCCOMB_X30_Y18_N14
\oTH|Div0|auto_generated|divider|divider|op_2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_2~0_combout\ = \cET|tacts\(9) $ (VCC)
-- \oTH|Div0|auto_generated|divider|divider|op_2~1\ = CARRY(\cET|tacts\(9))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \cET|tacts\(9),
	datad => VCC,
	combout => \oTH|Div0|auto_generated|divider|divider|op_2~0_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_2~1\);

-- Location: LCCOMB_X30_Y18_N20
\oTH|Div0|auto_generated|divider|divider|op_2~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_2~6_combout\ = (\oTH|Div0|auto_generated|divider|divider|StageOut[62]~242_combout\ & (((!\oTH|Div0|auto_generated|divider|divider|op_2~5\)))) # 
-- (!\oTH|Div0|auto_generated|divider|divider|StageOut[62]~242_combout\ & ((\oTH|Div0|auto_generated|divider|divider|StageOut[62]~302_combout\ & (!\oTH|Div0|auto_generated|divider|divider|op_2~5\)) # 
-- (!\oTH|Div0|auto_generated|divider|divider|StageOut[62]~302_combout\ & ((\oTH|Div0|auto_generated|divider|divider|op_2~5\) # (GND)))))
-- \oTH|Div0|auto_generated|divider|divider|op_2~7\ = CARRY(((!\oTH|Div0|auto_generated|divider|divider|StageOut[62]~242_combout\ & !\oTH|Div0|auto_generated|divider|divider|StageOut[62]~302_combout\)) # (!\oTH|Div0|auto_generated|divider|divider|op_2~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[62]~242_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[62]~302_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_2~5\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_2~6_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_2~7\);

-- Location: LCCOMB_X30_Y18_N22
\oTH|Div0|auto_generated|divider|divider|op_2~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_2~8_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_2~7\ & (((\oTH|Div0|auto_generated|divider|divider|StageOut[63]~301_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[63]~241_combout\)))) 
-- # (!\oTH|Div0|auto_generated|divider|divider|op_2~7\ & ((((\oTH|Div0|auto_generated|divider|divider|StageOut[63]~301_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[63]~241_combout\)))))
-- \oTH|Div0|auto_generated|divider|divider|op_2~9\ = CARRY((!\oTH|Div0|auto_generated|divider|divider|op_2~7\ & ((\oTH|Div0|auto_generated|divider|divider|StageOut[63]~301_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[63]~241_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[63]~301_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[63]~241_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_2~7\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_2~8_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_2~9\);

-- Location: LCCOMB_X25_Y18_N24
\oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[4]~7\ & (((\oTH|Div1|auto_generated|divider|divider|StageOut[81]~260_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[81]~204_combout\)))) # (!\oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[4]~7\ & ((((\oTH|Div1|auto_generated|divider|divider|StageOut[81]~260_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[81]~204_combout\)))))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[5]~9\ = CARRY((!\oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[4]~7\ & ((\oTH|Div1|auto_generated|divider|divider|StageOut[81]~260_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[81]~204_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[81]~260_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[81]~204_combout\,
	datad => VCC,
	cin => \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[4]~7\,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[5]~9\);

-- Location: LCCOMB_X31_Y18_N18
\oTH|Div0|auto_generated|divider|divider|op_3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_3~0_combout\ = \cET|tacts\(8) $ (VCC)
-- \oTH|Div0|auto_generated|divider|divider|op_3~1\ = CARRY(\cET|tacts\(8))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \cET|tacts\(8),
	datad => VCC,
	combout => \oTH|Div0|auto_generated|divider|divider|op_3~0_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_3~1\);

-- Location: LCCOMB_X31_Y18_N20
\oTH|Div0|auto_generated|divider|divider|op_3~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_3~2_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_3~1\ & (((\oTH|Div0|auto_generated|divider|divider|StageOut[66]~251_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[66]~250_combout\)))) 
-- # (!\oTH|Div0|auto_generated|divider|divider|op_3~1\ & (!\oTH|Div0|auto_generated|divider|divider|StageOut[66]~251_combout\ & (!\oTH|Div0|auto_generated|divider|divider|StageOut[66]~250_combout\)))
-- \oTH|Div0|auto_generated|divider|divider|op_3~3\ = CARRY((!\oTH|Div0|auto_generated|divider|divider|StageOut[66]~251_combout\ & (!\oTH|Div0|auto_generated|divider|divider|StageOut[66]~250_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_3~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[66]~251_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[66]~250_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_3~1\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_3~2_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_3~3\);

-- Location: LCCOMB_X25_Y21_N12
\oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[2]~2_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[1]~1\ & (((\oTH|Div1|auto_generated|divider|divider|StageOut[85]~289_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[85]~214_combout\)))) # (!\oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[1]~1\ & (!\oTH|Div1|auto_generated|divider|divider|StageOut[85]~289_combout\ & 
-- (!\oTH|Div1|auto_generated|divider|divider|StageOut[85]~214_combout\)))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[2]~3\ = CARRY((!\oTH|Div1|auto_generated|divider|divider|StageOut[85]~289_combout\ & (!\oTH|Div1|auto_generated|divider|divider|StageOut[85]~214_combout\ & 
-- !\oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[85]~289_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[85]~214_combout\,
	datad => VCC,
	cin => \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[1]~1\,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[2]~2_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[2]~3\);

-- Location: LCCOMB_X25_Y21_N16
\oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[4]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[4]~6_combout\ = (\oTH|Div1|auto_generated|divider|divider|StageOut[87]~264_combout\ & (((!\oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[3]~5\)))) # 
-- (!\oTH|Div1|auto_generated|divider|divider|StageOut[87]~264_combout\ & ((\oTH|Div1|auto_generated|divider|divider|StageOut[87]~212_combout\ & (!\oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[3]~5\)) # 
-- (!\oTH|Div1|auto_generated|divider|divider|StageOut[87]~212_combout\ & ((\oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[3]~5\) # (GND)))))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[4]~7\ = CARRY(((!\oTH|Div1|auto_generated|divider|divider|StageOut[87]~264_combout\ & !\oTH|Div1|auto_generated|divider|divider|StageOut[87]~212_combout\)) # 
-- (!\oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[3]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[87]~264_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[87]~212_combout\,
	datad => VCC,
	cin => \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[3]~5\,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[4]~6_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[4]~7\);

-- Location: LCCOMB_X32_Y18_N14
\oTH|Div0|auto_generated|divider|divider|op_4~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_4~6_combout\ = (\oTH|Div0|auto_generated|divider|divider|StageOut[74]~254_combout\ & (((!\oTH|Div0|auto_generated|divider|divider|op_4~5\)))) # 
-- (!\oTH|Div0|auto_generated|divider|divider|StageOut[74]~254_combout\ & ((\oTH|Div0|auto_generated|divider|divider|StageOut[74]~308_combout\ & (!\oTH|Div0|auto_generated|divider|divider|op_4~5\)) # 
-- (!\oTH|Div0|auto_generated|divider|divider|StageOut[74]~308_combout\ & ((\oTH|Div0|auto_generated|divider|divider|op_4~5\) # (GND)))))
-- \oTH|Div0|auto_generated|divider|divider|op_4~7\ = CARRY(((!\oTH|Div0|auto_generated|divider|divider|StageOut[74]~254_combout\ & !\oTH|Div0|auto_generated|divider|divider|StageOut[74]~308_combout\)) # (!\oTH|Div0|auto_generated|divider|divider|op_4~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[74]~254_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[74]~308_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_4~5\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_4~6_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_4~7\);

-- Location: LCCOMB_X32_Y18_N16
\oTH|Div0|auto_generated|divider|divider|op_4~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_4~8_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_4~7\ & (((\oTH|Div0|auto_generated|divider|divider|StageOut[75]~253_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[75]~307_combout\)))) 
-- # (!\oTH|Div0|auto_generated|divider|divider|op_4~7\ & ((((\oTH|Div0|auto_generated|divider|divider|StageOut[75]~253_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[75]~307_combout\)))))
-- \oTH|Div0|auto_generated|divider|divider|op_4~9\ = CARRY((!\oTH|Div0|auto_generated|divider|divider|op_4~7\ & ((\oTH|Div0|auto_generated|divider|divider|StageOut[75]~253_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[75]~307_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[75]~253_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[75]~307_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_4~7\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_4~8_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_4~9\);

-- Location: LCCOMB_X26_Y20_N0
\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[1]~0_combout\ = (((\oTH|Div1|auto_generated|divider|divider|StageOut[91]~222_combout\) # (\oTH|Div1|auto_generated|divider|divider|StageOut[91]~223_combout\)))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[1]~1\ = CARRY((\oTH|Div1|auto_generated|divider|divider|StageOut[91]~222_combout\) # (\oTH|Div1|auto_generated|divider|divider|StageOut[91]~223_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[91]~222_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[91]~223_combout\,
	datad => VCC,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[1]~0_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[1]~1\);

-- Location: LCCOMB_X26_Y20_N6
\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[4]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[4]~6_combout\ = (\oTH|Div1|auto_generated|divider|divider|StageOut[94]~267_combout\ & (((!\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[3]~5\)))) # 
-- (!\oTH|Div1|auto_generated|divider|divider|StageOut[94]~267_combout\ & ((\oTH|Div1|auto_generated|divider|divider|StageOut[94]~219_combout\ & (!\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[3]~5\)) # 
-- (!\oTH|Div1|auto_generated|divider|divider|StageOut[94]~219_combout\ & ((\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[3]~5\) # (GND)))))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[4]~7\ = CARRY(((!\oTH|Div1|auto_generated|divider|divider|StageOut[94]~267_combout\ & !\oTH|Div1|auto_generated|divider|divider|StageOut[94]~219_combout\)) # 
-- (!\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[3]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[94]~267_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[94]~219_combout\,
	datad => VCC,
	cin => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[3]~5\,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[4]~6_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[4]~7\);

-- Location: LCCOMB_X26_Y20_N8
\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[5]~8_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[4]~7\ & (((\oTH|Div1|auto_generated|divider|divider|StageOut[95]~218_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[95]~266_combout\)))) # (!\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[4]~7\ & ((((\oTH|Div1|auto_generated|divider|divider|StageOut[95]~218_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[95]~266_combout\)))))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[5]~9\ = CARRY((!\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[4]~7\ & ((\oTH|Div1|auto_generated|divider|divider|StageOut[95]~218_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[95]~266_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[95]~218_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[95]~266_combout\,
	datad => VCC,
	cin => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[4]~7\,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[5]~8_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[5]~9\);

-- Location: LCCOMB_X34_Y18_N6
\oTH|Div0|auto_generated|divider|divider|op_5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_5~0_combout\ = \cET|tacts\(6) $ (VCC)
-- \oTH|Div0|auto_generated|divider|divider|op_5~1\ = CARRY(\cET|tacts\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \cET|tacts\(6),
	datad => VCC,
	combout => \oTH|Div0|auto_generated|divider|divider|op_5~0_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_5~1\);

-- Location: LCCOMB_X34_Y18_N8
\oTH|Div0|auto_generated|divider|divider|op_5~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_5~2_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_5~1\ & (((\oTH|Div0|auto_generated|divider|divider|StageOut[78]~262_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[78]~263_combout\)))) 
-- # (!\oTH|Div0|auto_generated|divider|divider|op_5~1\ & (!\oTH|Div0|auto_generated|divider|divider|StageOut[78]~262_combout\ & (!\oTH|Div0|auto_generated|divider|divider|StageOut[78]~263_combout\)))
-- \oTH|Div0|auto_generated|divider|divider|op_5~3\ = CARRY((!\oTH|Div0|auto_generated|divider|divider|StageOut[78]~262_combout\ & (!\oTH|Div0|auto_generated|divider|divider|StageOut[78]~263_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_5~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[78]~262_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[78]~263_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_5~1\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_5~2_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_5~3\);

-- Location: LCCOMB_X34_Y18_N12
\oTH|Div0|auto_generated|divider|divider|op_5~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_5~6_combout\ = (\oTH|Div0|auto_generated|divider|divider|StageOut[80]~260_combout\ & (((!\oTH|Div0|auto_generated|divider|divider|op_5~5\)))) # 
-- (!\oTH|Div0|auto_generated|divider|divider|StageOut[80]~260_combout\ & ((\oTH|Div0|auto_generated|divider|divider|StageOut[80]~311_combout\ & (!\oTH|Div0|auto_generated|divider|divider|op_5~5\)) # 
-- (!\oTH|Div0|auto_generated|divider|divider|StageOut[80]~311_combout\ & ((\oTH|Div0|auto_generated|divider|divider|op_5~5\) # (GND)))))
-- \oTH|Div0|auto_generated|divider|divider|op_5~7\ = CARRY(((!\oTH|Div0|auto_generated|divider|divider|StageOut[80]~260_combout\ & !\oTH|Div0|auto_generated|divider|divider|StageOut[80]~311_combout\)) # (!\oTH|Div0|auto_generated|divider|divider|op_5~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[80]~260_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[80]~311_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_5~5\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_5~6_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_5~7\);

-- Location: LCCOMB_X34_Y18_N14
\oTH|Div0|auto_generated|divider|divider|op_5~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_5~8_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_5~7\ & (((\oTH|Div0|auto_generated|divider|divider|StageOut[81]~259_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[81]~310_combout\)))) 
-- # (!\oTH|Div0|auto_generated|divider|divider|op_5~7\ & ((((\oTH|Div0|auto_generated|divider|divider|StageOut[81]~259_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[81]~310_combout\)))))
-- \oTH|Div0|auto_generated|divider|divider|op_5~9\ = CARRY((!\oTH|Div0|auto_generated|divider|divider|op_5~7\ & ((\oTH|Div0|auto_generated|divider|divider|StageOut[81]~259_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[81]~310_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[81]~259_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[81]~310_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_5~7\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_5~8_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_5~9\);

-- Location: LCCOMB_X25_Y20_N18
\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[2]~2_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[1]~1\ & (((\oTH|Div1|auto_generated|divider|divider|StageOut[99]~228_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[99]~291_combout\)))) # (!\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[1]~1\ & (!\oTH|Div1|auto_generated|divider|divider|StageOut[99]~228_combout\ & 
-- (!\oTH|Div1|auto_generated|divider|divider|StageOut[99]~291_combout\)))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[2]~3\ = CARRY((!\oTH|Div1|auto_generated|divider|divider|StageOut[99]~228_combout\ & (!\oTH|Div1|auto_generated|divider|divider|StageOut[99]~291_combout\ & 
-- !\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[99]~228_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[99]~291_combout\,
	datad => VCC,
	cin => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[1]~1\,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[2]~2_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[2]~3\);

-- Location: LCCOMB_X25_Y20_N22
\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[4]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[4]~6_combout\ = (\oTH|Div1|auto_generated|divider|divider|StageOut[101]~226_combout\ & (((!\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[3]~5\)))) # 
-- (!\oTH|Div1|auto_generated|divider|divider|StageOut[101]~226_combout\ & ((\oTH|Div1|auto_generated|divider|divider|StageOut[101]~270_combout\ & (!\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[3]~5\)) # 
-- (!\oTH|Div1|auto_generated|divider|divider|StageOut[101]~270_combout\ & ((\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[3]~5\) # (GND)))))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[4]~7\ = CARRY(((!\oTH|Div1|auto_generated|divider|divider|StageOut[101]~226_combout\ & !\oTH|Div1|auto_generated|divider|divider|StageOut[101]~270_combout\)) # 
-- (!\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[3]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[101]~226_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[101]~270_combout\,
	datad => VCC,
	cin => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[3]~5\,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[4]~6_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[4]~7\);

-- Location: LCCOMB_X25_Y20_N24
\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[5]~8_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[4]~7\ & (((\oTH|Div1|auto_generated|divider|divider|StageOut[102]~225_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[102]~269_combout\)))) # (!\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[4]~7\ & ((((\oTH|Div1|auto_generated|divider|divider|StageOut[102]~225_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[102]~269_combout\)))))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[5]~9\ = CARRY((!\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[4]~7\ & ((\oTH|Div1|auto_generated|divider|divider|StageOut[102]~225_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[102]~269_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[102]~225_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[102]~269_combout\,
	datad => VCC,
	cin => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[4]~7\,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[5]~8_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[5]~9\);

-- Location: LCCOMB_X12_Y19_N14
\oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[4]~2_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\ & (!\oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[3]~1\)) # 
-- (!\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\ & (\oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[3]~1\ & VCC))
-- \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[4]~3\ = CARRY((\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\ & !\oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[3]~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\,
	datad => VCC,
	cin => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[3]~1\,
	combout => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[4]~2_combout\,
	cout => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[4]~3\);

-- Location: LCCOMB_X12_Y19_N24
\oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[9]~12_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\ & (!\oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[8]~11\ & VCC)) # 
-- (!\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\ & (\oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[8]~11\ $ (GND)))
-- \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[9]~13\ = CARRY((!\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\ & !\oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[8]~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101000000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\,
	datad => VCC,
	cin => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[8]~11\,
	combout => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[9]~12_combout\,
	cout => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[9]~13\);

-- Location: LCCOMB_X33_Y19_N2
\oTH|Div0|auto_generated|divider|divider|op_6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_6~0_combout\ = \cET|tacts\(5) $ (VCC)
-- \oTH|Div0|auto_generated|divider|divider|op_6~1\ = CARRY(\cET|tacts\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \cET|tacts\(5),
	datad => VCC,
	combout => \oTH|Div0|auto_generated|divider|divider|op_6~0_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_6~1\);

-- Location: LCCOMB_X33_Y19_N8
\oTH|Div0|auto_generated|divider|divider|op_6~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_6~6_combout\ = (\oTH|Div0|auto_generated|divider|divider|StageOut[86]~266_combout\ & (((!\oTH|Div0|auto_generated|divider|divider|op_6~5\)))) # 
-- (!\oTH|Div0|auto_generated|divider|divider|StageOut[86]~266_combout\ & ((\oTH|Div0|auto_generated|divider|divider|StageOut[86]~314_combout\ & (!\oTH|Div0|auto_generated|divider|divider|op_6~5\)) # 
-- (!\oTH|Div0|auto_generated|divider|divider|StageOut[86]~314_combout\ & ((\oTH|Div0|auto_generated|divider|divider|op_6~5\) # (GND)))))
-- \oTH|Div0|auto_generated|divider|divider|op_6~7\ = CARRY(((!\oTH|Div0|auto_generated|divider|divider|StageOut[86]~266_combout\ & !\oTH|Div0|auto_generated|divider|divider|StageOut[86]~314_combout\)) # (!\oTH|Div0|auto_generated|divider|divider|op_6~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[86]~266_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[86]~314_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_6~5\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_6~6_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_6~7\);

-- Location: LCCOMB_X33_Y19_N10
\oTH|Div0|auto_generated|divider|divider|op_6~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_6~8_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_6~7\ & (((\oTH|Div0|auto_generated|divider|divider|StageOut[87]~265_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[87]~313_combout\)))) 
-- # (!\oTH|Div0|auto_generated|divider|divider|op_6~7\ & ((((\oTH|Div0|auto_generated|divider|divider|StageOut[87]~265_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[87]~313_combout\)))))
-- \oTH|Div0|auto_generated|divider|divider|op_6~9\ = CARRY((!\oTH|Div0|auto_generated|divider|divider|op_6~7\ & ((\oTH|Div0|auto_generated|divider|divider|StageOut[87]~265_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[87]~313_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[87]~265_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[87]~313_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_6~7\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_6~8_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_6~9\);

-- Location: LCCOMB_X21_Y20_N10
\oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[3]~4_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[2]~3\ & (((\oTH|Div1|auto_generated|divider|divider|StageOut[107]~234_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[107]~300_combout\)))) # (!\oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[2]~3\ & ((((\oTH|Div1|auto_generated|divider|divider|StageOut[107]~234_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[107]~300_combout\)))))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[3]~5\ = CARRY((!\oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[2]~3\ & ((\oTH|Div1|auto_generated|divider|divider|StageOut[107]~234_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[107]~300_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[107]~234_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[107]~300_combout\,
	datad => VCC,
	cin => \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[2]~3\,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[3]~4_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[3]~5\);

-- Location: LCCOMB_X11_Y19_N12
\oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[4]~2_combout\ = (\oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[3]~1\ & (((\oTH|Mod2|auto_generated|divider|divider|StageOut[201]~71_combout\) # 
-- (\oTH|Mod2|auto_generated|divider|divider|StageOut[201]~70_combout\)))) # (!\oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[3]~1\ & (!\oTH|Mod2|auto_generated|divider|divider|StageOut[201]~71_combout\ & 
-- (!\oTH|Mod2|auto_generated|divider|divider|StageOut[201]~70_combout\)))
-- \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[4]~3\ = CARRY((!\oTH|Mod2|auto_generated|divider|divider|StageOut[201]~71_combout\ & (!\oTH|Mod2|auto_generated|divider|divider|StageOut[201]~70_combout\ & 
-- !\oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[3]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod2|auto_generated|divider|divider|StageOut[201]~71_combout\,
	datab => \oTH|Mod2|auto_generated|divider|divider|StageOut[201]~70_combout\,
	datad => VCC,
	cin => \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[3]~1\,
	combout => \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[4]~2_combout\,
	cout => \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[4]~3\);

-- Location: LCCOMB_X11_Y19_N16
\oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[6]~6_combout\ = (\oTH|Mod2|auto_generated|divider|divider|StageOut[203]~67_combout\ & (((!\oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[5]~5\)))) # 
-- (!\oTH|Mod2|auto_generated|divider|divider|StageOut[203]~67_combout\ & ((\oTH|Mod2|auto_generated|divider|divider|StageOut[203]~66_combout\ & (!\oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[5]~5\)) # 
-- (!\oTH|Mod2|auto_generated|divider|divider|StageOut[203]~66_combout\ & ((\oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[5]~5\) # (GND)))))
-- \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[6]~7\ = CARRY(((!\oTH|Mod2|auto_generated|divider|divider|StageOut[203]~67_combout\ & !\oTH|Mod2|auto_generated|divider|divider|StageOut[203]~66_combout\)) # 
-- (!\oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[5]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod2|auto_generated|divider|divider|StageOut[203]~67_combout\,
	datab => \oTH|Mod2|auto_generated|divider|divider|StageOut[203]~66_combout\,
	datad => VCC,
	cin => \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[5]~5\,
	combout => \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[6]~6_combout\,
	cout => \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[6]~7\);

-- Location: LCCOMB_X11_Y19_N22
\oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[9]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[9]~12_combout\ = (\oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[8]~11\ & (((\oTH|Mod2|auto_generated|divider|divider|StageOut[206]~61_combout\) # 
-- (\oTH|Mod2|auto_generated|divider|divider|StageOut[206]~60_combout\)))) # (!\oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[8]~11\ & ((((\oTH|Mod2|auto_generated|divider|divider|StageOut[206]~61_combout\) # 
-- (\oTH|Mod2|auto_generated|divider|divider|StageOut[206]~60_combout\)))))
-- \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[9]~13\ = CARRY((!\oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[8]~11\ & ((\oTH|Mod2|auto_generated|divider|divider|StageOut[206]~61_combout\) # 
-- (\oTH|Mod2|auto_generated|divider|divider|StageOut[206]~60_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod2|auto_generated|divider|divider|StageOut[206]~61_combout\,
	datab => \oTH|Mod2|auto_generated|divider|divider|StageOut[206]~60_combout\,
	datad => VCC,
	cin => \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[8]~11\,
	combout => \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[9]~12_combout\,
	cout => \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[9]~13\);

-- Location: LCCOMB_X10_Y19_N20
\oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\ = (((\oTH|Mod2|auto_generated|divider|divider|StageOut[214]~89_combout\) # (\oTH|Mod2|auto_generated|divider|divider|StageOut[214]~78_combout\)))
-- \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[2]~1\ = CARRY((\oTH|Mod2|auto_generated|divider|divider|StageOut[214]~89_combout\) # (\oTH|Mod2|auto_generated|divider|divider|StageOut[214]~78_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod2|auto_generated|divider|divider|StageOut[214]~89_combout\,
	datab => \oTH|Mod2|auto_generated|divider|divider|StageOut[214]~78_combout\,
	datad => VCC,
	combout => \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\,
	cout => \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[2]~1\);

-- Location: LCCOMB_X10_Y19_N28
\oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\ = (\oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ & (((\oTH|Mod2|auto_generated|divider|divider|StageOut[218]~74_combout\) # 
-- (\oTH|Mod2|auto_generated|divider|divider|StageOut[218]~85_combout\)))) # (!\oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ & ((((\oTH|Mod2|auto_generated|divider|divider|StageOut[218]~74_combout\) # 
-- (\oTH|Mod2|auto_generated|divider|divider|StageOut[218]~85_combout\)))))
-- \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[6]~9\ = CARRY((!\oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[5]~7\ & ((\oTH|Mod2|auto_generated|divider|divider|StageOut[218]~74_combout\) # 
-- (\oTH|Mod2|auto_generated|divider|divider|StageOut[218]~85_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod2|auto_generated|divider|divider|StageOut[218]~74_combout\,
	datab => \oTH|Mod2|auto_generated|divider|divider|StageOut[218]~85_combout\,
	datad => VCC,
	cin => \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[5]~7\,
	combout => \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\,
	cout => \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[6]~9\);

-- Location: LCCOMB_X10_Y20_N10
\oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[1]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[1]~12_combout\ = (\oTH|Mod2|auto_generated|divider|divider|StageOut[213]~79_combout\) # (\oTH|Mod2|auto_generated|divider|divider|StageOut[213]~90_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Mod2|auto_generated|divider|divider|StageOut[213]~79_combout\,
	datad => \oTH|Mod2|auto_generated|divider|divider|StageOut[213]~90_combout\,
	combout => \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[1]~12_combout\);

-- Location: LCCOMB_X10_Y20_N26
\oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[1]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[1]~14_combout\ = (\oTH|Div7|auto_generated|divider|divider|StageOut[48]~71_combout\) # (\oTH|Div7|auto_generated|divider|divider|StageOut[48]~51_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div7|auto_generated|divider|divider|StageOut[48]~71_combout\,
	datad => \oTH|Div7|auto_generated|divider|divider|StageOut[48]~51_combout\,
	combout => \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[1]~14_combout\);

-- Location: LCCOMB_X9_Y20_N16
\oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\ = (((\oTH|Div7|auto_generated|divider|divider|StageOut[57]~52_combout\) # (\oTH|Div7|auto_generated|divider|divider|StageOut[57]~72_combout\)))
-- \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ = CARRY((\oTH|Div7|auto_generated|divider|divider|StageOut[57]~52_combout\) # (\oTH|Div7|auto_generated|divider|divider|StageOut[57]~72_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div7|auto_generated|divider|divider|StageOut[57]~52_combout\,
	datab => \oTH|Div7|auto_generated|divider|divider|StageOut[57]~72_combout\,
	datad => VCC,
	combout => \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[2]~0_combout\,
	cout => \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[2]~1\);

-- Location: LCCOMB_X9_Y20_N18
\oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\ = (\oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ & (((\oTH|Div7|auto_generated|divider|divider|StageOut[58]~70_combout\) # 
-- (\oTH|Div7|auto_generated|divider|divider|StageOut[58]~50_combout\)))) # (!\oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[2]~1\ & (!\oTH|Div7|auto_generated|divider|divider|StageOut[58]~70_combout\ & 
-- (!\oTH|Div7|auto_generated|divider|divider|StageOut[58]~50_combout\)))
-- \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ = CARRY((!\oTH|Div7|auto_generated|divider|divider|StageOut[58]~70_combout\ & (!\oTH|Div7|auto_generated|divider|divider|StageOut[58]~50_combout\ & 
-- !\oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div7|auto_generated|divider|divider|StageOut[58]~70_combout\,
	datab => \oTH|Div7|auto_generated|divider|divider|StageOut[58]~50_combout\,
	datad => VCC,
	cin => \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[2]~1\,
	combout => \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[3]~2_combout\,
	cout => \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[3]~3\);

-- Location: LCCOMB_X9_Y20_N20
\oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\ = (\oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ & ((((\oTH|Div7|auto_generated|divider|divider|StageOut[59]~49_combout\) # 
-- (\oTH|Div7|auto_generated|divider|divider|StageOut[59]~69_combout\))))) # (!\oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[3]~3\ & ((\oTH|Div7|auto_generated|divider|divider|StageOut[59]~49_combout\) # 
-- ((\oTH|Div7|auto_generated|divider|divider|StageOut[59]~69_combout\) # (GND))))
-- \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[4]~5\ = CARRY((\oTH|Div7|auto_generated|divider|divider|StageOut[59]~49_combout\) # ((\oTH|Div7|auto_generated|divider|divider|StageOut[59]~69_combout\) # 
-- (!\oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div7|auto_generated|divider|divider|StageOut[59]~49_combout\,
	datab => \oTH|Div7|auto_generated|divider|divider|StageOut[59]~69_combout\,
	datad => VCC,
	cin => \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[3]~3\,
	combout => \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[4]~4_combout\,
	cout => \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[4]~5\);

-- Location: LCCOMB_X9_Y20_N22
\oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\ = (\oTH|Div7|auto_generated|divider|divider|StageOut[60]~68_combout\ & (((!\oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[4]~5\)))) # 
-- (!\oTH|Div7|auto_generated|divider|divider|StageOut[60]~68_combout\ & ((\oTH|Div7|auto_generated|divider|divider|StageOut[60]~48_combout\ & (!\oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[4]~5\)) # 
-- (!\oTH|Div7|auto_generated|divider|divider|StageOut[60]~48_combout\ & ((\oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[4]~5\) # (GND)))))
-- \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[5]~7\ = CARRY(((!\oTH|Div7|auto_generated|divider|divider|StageOut[60]~68_combout\ & !\oTH|Div7|auto_generated|divider|divider|StageOut[60]~48_combout\)) # 
-- (!\oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div7|auto_generated|divider|divider|StageOut[60]~68_combout\,
	datab => \oTH|Div7|auto_generated|divider|divider|StageOut[60]~48_combout\,
	datad => VCC,
	cin => \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[4]~5\,
	combout => \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[5]~6_combout\,
	cout => \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[5]~7\);

-- Location: LCCOMB_X24_Y20_N10
\oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[4]~2_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\ & (!\oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[3]~1\)) # 
-- (!\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\ & (\oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[3]~1\ & VCC))
-- \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[4]~3\ = CARRY((\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\ & !\oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[3]~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101000001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\,
	datad => VCC,
	cin => \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[3]~1\,
	combout => \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[4]~2_combout\,
	cout => \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[4]~3\);

-- Location: LCCOMB_X24_Y20_N18
\oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[8]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[8]~10_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[7]~12_combout\ & ((\oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[7]~9\) # (GND))) # 
-- (!\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[7]~12_combout\ & (!\oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[7]~9\))
-- \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[8]~11\ = CARRY((\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[7]~12_combout\) # (!\oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[7]~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010110101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[7]~12_combout\,
	datad => VCC,
	cin => \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[7]~9\,
	combout => \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[8]~10_combout\,
	cout => \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[8]~11\);

-- Location: LCCOMB_X22_Y20_N20
\oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[2]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[2]~18_combout\ = !\oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[7]~12_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[7]~12_combout\,
	combout => \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[2]~18_combout\);

-- Location: LCCOMB_X34_Y19_N16
\oTH|Div0|auto_generated|divider|divider|op_7~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_7~8_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_7~7\ & (((\oTH|Div0|auto_generated|divider|divider|StageOut[93]~271_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[93]~316_combout\)))) 
-- # (!\oTH|Div0|auto_generated|divider|divider|op_7~7\ & ((((\oTH|Div0|auto_generated|divider|divider|StageOut[93]~271_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[93]~316_combout\)))))
-- \oTH|Div0|auto_generated|divider|divider|op_7~9\ = CARRY((!\oTH|Div0|auto_generated|divider|divider|op_7~7\ & ((\oTH|Div0|auto_generated|divider|divider|StageOut[93]~271_combout\) # (\oTH|Div0|auto_generated|divider|divider|StageOut[93]~316_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[93]~271_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[93]~316_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_7~7\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_7~8_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_7~9\);

-- Location: LCCOMB_X21_Y19_N0
\oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[1]~0_combout\ = (((\oTH|Div1|auto_generated|divider|divider|StageOut[112]~243_combout\) # (\oTH|Div1|auto_generated|divider|divider|StageOut[112]~244_combout\)))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[1]~1\ = CARRY((\oTH|Div1|auto_generated|divider|divider|StageOut[112]~243_combout\) # (\oTH|Div1|auto_generated|divider|divider|StageOut[112]~244_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[112]~243_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[112]~244_combout\,
	datad => VCC,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[1]~0_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[1]~1\);

-- Location: LCCOMB_X21_Y19_N4
\oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[3]~4_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[2]~3\ & (((\oTH|Div1|auto_generated|divider|divider|StageOut[114]~241_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[114]~301_combout\)))) # (!\oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[2]~3\ & ((((\oTH|Div1|auto_generated|divider|divider|StageOut[114]~241_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[114]~301_combout\)))))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[3]~5\ = CARRY((!\oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[2]~3\ & ((\oTH|Div1|auto_generated|divider|divider|StageOut[114]~241_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[114]~301_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[114]~241_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[114]~301_combout\,
	datad => VCC,
	cin => \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[2]~3\,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[3]~4_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[3]~5\);

-- Location: LCCOMB_X21_Y19_N8
\oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[5]~8_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[4]~7\ & (((\oTH|Div1|auto_generated|divider|divider|StageOut[116]~239_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[116]~275_combout\)))) # (!\oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[4]~7\ & ((((\oTH|Div1|auto_generated|divider|divider|StageOut[116]~239_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[116]~275_combout\)))))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[5]~9\ = CARRY((!\oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[4]~7\ & ((\oTH|Div1|auto_generated|divider|divider|StageOut[116]~239_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[116]~275_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[116]~239_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[116]~275_combout\,
	datad => VCC,
	cin => \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[4]~7\,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[5]~8_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[5]~9\);

-- Location: LCCOMB_X35_Y19_N6
\oTH|Div0|auto_generated|divider|divider|op_8~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_8~0_combout\ = \cET|tacts\(3) $ (VCC)
-- \oTH|Div0|auto_generated|divider|divider|op_8~1\ = CARRY(\cET|tacts\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \cET|tacts\(3),
	datad => VCC,
	combout => \oTH|Div0|auto_generated|divider|divider|op_8~0_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_8~1\);

-- Location: LCCOMB_X35_Y19_N10
\oTH|Div0|auto_generated|divider|divider|op_8~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_8~4_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_8~3\ & ((((\oTH|Div0|auto_generated|divider|divider|StageOut[97]~338_combout\) # 
-- (\oTH|Div0|auto_generated|divider|divider|StageOut[97]~279_combout\))))) # (!\oTH|Div0|auto_generated|divider|divider|op_8~3\ & ((\oTH|Div0|auto_generated|divider|divider|StageOut[97]~338_combout\) # 
-- ((\oTH|Div0|auto_generated|divider|divider|StageOut[97]~279_combout\) # (GND))))
-- \oTH|Div0|auto_generated|divider|divider|op_8~5\ = CARRY((\oTH|Div0|auto_generated|divider|divider|StageOut[97]~338_combout\) # ((\oTH|Div0|auto_generated|divider|divider|StageOut[97]~279_combout\) # (!\oTH|Div0|auto_generated|divider|divider|op_8~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[97]~338_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[97]~279_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_8~3\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_8~4_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_8~5\);

-- Location: LCCOMB_X35_Y19_N12
\oTH|Div0|auto_generated|divider|divider|op_8~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|op_8~6_combout\ = (\oTH|Div0|auto_generated|divider|divider|StageOut[98]~320_combout\ & (((!\oTH|Div0|auto_generated|divider|divider|op_8~5\)))) # 
-- (!\oTH|Div0|auto_generated|divider|divider|StageOut[98]~320_combout\ & ((\oTH|Div0|auto_generated|divider|divider|StageOut[98]~278_combout\ & (!\oTH|Div0|auto_generated|divider|divider|op_8~5\)) # 
-- (!\oTH|Div0|auto_generated|divider|divider|StageOut[98]~278_combout\ & ((\oTH|Div0|auto_generated|divider|divider|op_8~5\) # (GND)))))
-- \oTH|Div0|auto_generated|divider|divider|op_8~7\ = CARRY(((!\oTH|Div0|auto_generated|divider|divider|StageOut[98]~320_combout\ & !\oTH|Div0|auto_generated|divider|divider|StageOut[98]~278_combout\)) # (!\oTH|Div0|auto_generated|divider|divider|op_8~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|StageOut[98]~320_combout\,
	datab => \oTH|Div0|auto_generated|divider|divider|StageOut[98]~278_combout\,
	datad => VCC,
	cin => \oTH|Div0|auto_generated|divider|divider|op_8~5\,
	combout => \oTH|Div0|auto_generated|divider|divider|op_8~6_combout\,
	cout => \oTH|Div0|auto_generated|divider|divider|op_8~7\);

-- Location: LCCOMB_X22_Y19_N2
\oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[1]~0_combout\ = (((\oTH|Div1|auto_generated|divider|divider|StageOut[119]~251_combout\) # (\oTH|Div1|auto_generated|divider|divider|StageOut[119]~250_combout\)))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[1]~1\ = CARRY((\oTH|Div1|auto_generated|divider|divider|StageOut[119]~251_combout\) # (\oTH|Div1|auto_generated|divider|divider|StageOut[119]~250_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[119]~251_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[119]~250_combout\,
	datad => VCC,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[1]~0_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[1]~1\);

-- Location: LCCOMB_X22_Y19_N8
\oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[4]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[4]~6_combout\ = (\oTH|Div1|auto_generated|divider|divider|StageOut[122]~247_combout\ & (((!\oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[3]~5\)))) # 
-- (!\oTH|Div1|auto_generated|divider|divider|StageOut[122]~247_combout\ & ((\oTH|Div1|auto_generated|divider|divider|StageOut[122]~279_combout\ & (!\oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[3]~5\)) # 
-- (!\oTH|Div1|auto_generated|divider|divider|StageOut[122]~279_combout\ & ((\oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[3]~5\) # (GND)))))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[4]~7\ = CARRY(((!\oTH|Div1|auto_generated|divider|divider|StageOut[122]~247_combout\ & !\oTH|Div1|auto_generated|divider|divider|StageOut[122]~279_combout\)) # 
-- (!\oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[3]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[122]~247_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[122]~279_combout\,
	datad => VCC,
	cin => \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[3]~5\,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[4]~6_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[4]~7\);

-- Location: LCCOMB_X22_Y19_N10
\oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[5]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[5]~8_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[4]~7\ & (((\oTH|Div1|auto_generated|divider|divider|StageOut[123]~246_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[123]~278_combout\)))) # (!\oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[4]~7\ & ((((\oTH|Div1|auto_generated|divider|divider|StageOut[123]~246_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[123]~278_combout\)))))
-- \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[5]~9\ = CARRY((!\oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[4]~7\ & ((\oTH|Div1|auto_generated|divider|divider|StageOut[123]~246_combout\) # 
-- (\oTH|Div1|auto_generated|divider|divider|StageOut[123]~278_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|StageOut[123]~246_combout\,
	datab => \oTH|Div1|auto_generated|divider|divider|StageOut[123]~278_combout\,
	datad => VCC,
	cin => \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[4]~7\,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[5]~8_combout\,
	cout => \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[5]~9\);

-- Location: LCCOMB_X23_Y19_N28
\oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[0]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[0]~14_combout\ = !\oTH|Div0|auto_generated|divider|divider|op_9~12_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \oTH|Div0|auto_generated|divider|divider|op_9~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[0]~14_combout\);

-- Location: LCCOMB_X22_Y18_N8
\oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~2_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[7]~12_combout\ & (!\oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~1\)) # 
-- (!\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[7]~12_combout\ & (\oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~1\ & VCC))
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\ = CARRY((\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[7]~12_combout\ & !\oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101000001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[7]~12_combout\,
	datad => VCC,
	cin => \oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[1]~1\,
	combout => \oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~2_combout\,
	cout => \oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\);

-- Location: LCCOMB_X22_Y18_N10
\oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~4_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\ & (!\oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\ & VCC)) # 
-- (!\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\ & (\oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\ $ (GND)))
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~5\ = CARRY((!\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\ & !\oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\,
	datad => VCC,
	cin => \oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[2]~3\,
	combout => \oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~4_combout\,
	cout => \oTH|Mod0|auto_generated|divider|divider|add_sub_12_result_int[3]~5\);

-- Location: LCCOMB_X21_Y18_N20
\oTH|Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~4_combout\ = (\oTH|Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~3\ & (((\oTH|Mod0|auto_generated|divider|divider|StageOut[62]~87_combout\) # 
-- (\oTH|Mod0|auto_generated|divider|divider|StageOut[62]~86_combout\)))) # (!\oTH|Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~3\ & ((((\oTH|Mod0|auto_generated|divider|divider|StageOut[62]~87_combout\) # 
-- (\oTH|Mod0|auto_generated|divider|divider|StageOut[62]~86_combout\)))))
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~5\ = CARRY((!\oTH|Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~3\ & ((\oTH|Mod0|auto_generated|divider|divider|StageOut[62]~87_combout\) # 
-- (\oTH|Mod0|auto_generated|divider|divider|StageOut[62]~86_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod0|auto_generated|divider|divider|StageOut[62]~87_combout\,
	datab => \oTH|Mod0|auto_generated|divider|divider|StageOut[62]~86_combout\,
	datad => VCC,
	cin => \oTH|Mod0|auto_generated|divider|divider|add_sub_13_result_int[2]~3\,
	combout => \oTH|Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~4_combout\,
	cout => \oTH|Mod0|auto_generated|divider|divider|add_sub_13_result_int[3]~5\);

-- Location: LCCOMB_X20_Y18_N4
\oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[1]~0_combout\ = (((\oTH|Mod0|auto_generated|divider|divider|StageOut[65]~95_combout\) # (\oTH|Mod0|auto_generated|divider|divider|StageOut[65]~96_combout\)))
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[1]~1\ = CARRY((\oTH|Mod0|auto_generated|divider|divider|StageOut[65]~95_combout\) # (\oTH|Mod0|auto_generated|divider|divider|StageOut[65]~96_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod0|auto_generated|divider|divider|StageOut[65]~95_combout\,
	datab => \oTH|Mod0|auto_generated|divider|divider|StageOut[65]~96_combout\,
	datad => VCC,
	combout => \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[1]~0_combout\,
	cout => \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[1]~1\);

-- Location: LCCOMB_X20_Y18_N6
\oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[2]~2_combout\ = (\oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[1]~1\ & (((\oTH|Mod0|auto_generated|divider|divider|StageOut[66]~136_combout\) # 
-- (\oTH|Mod0|auto_generated|divider|divider|StageOut[66]~94_combout\)))) # (!\oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[1]~1\ & (!\oTH|Mod0|auto_generated|divider|divider|StageOut[66]~136_combout\ & 
-- (!\oTH|Mod0|auto_generated|divider|divider|StageOut[66]~94_combout\)))
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[2]~3\ = CARRY((!\oTH|Mod0|auto_generated|divider|divider|StageOut[66]~136_combout\ & (!\oTH|Mod0|auto_generated|divider|divider|StageOut[66]~94_combout\ & 
-- !\oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod0|auto_generated|divider|divider|StageOut[66]~136_combout\,
	datab => \oTH|Mod0|auto_generated|divider|divider|StageOut[66]~94_combout\,
	datad => VCC,
	cin => \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[1]~1\,
	combout => \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[2]~2_combout\,
	cout => \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[2]~3\);

-- Location: LCCOMB_X20_Y18_N8
\oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[3]~4_combout\ = (\oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[2]~3\ & (((\oTH|Mod0|auto_generated|divider|divider|StageOut[67]~93_combout\) # 
-- (\oTH|Mod0|auto_generated|divider|divider|StageOut[67]~135_combout\)))) # (!\oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[2]~3\ & ((((\oTH|Mod0|auto_generated|divider|divider|StageOut[67]~93_combout\) # 
-- (\oTH|Mod0|auto_generated|divider|divider|StageOut[67]~135_combout\)))))
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[3]~5\ = CARRY((!\oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[2]~3\ & ((\oTH|Mod0|auto_generated|divider|divider|StageOut[67]~93_combout\) # 
-- (\oTH|Mod0|auto_generated|divider|divider|StageOut[67]~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod0|auto_generated|divider|divider|StageOut[67]~93_combout\,
	datab => \oTH|Mod0|auto_generated|divider|divider|StageOut[67]~135_combout\,
	datad => VCC,
	cin => \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[2]~3\,
	combout => \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[3]~4_combout\,
	cout => \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[3]~5\);

-- Location: LCCOMB_X19_Y18_N0
\oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[0]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[0]~10_combout\ = !\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\,
	combout => \oTH|Mod0|auto_generated|divider|divider|add_sub_14_result_int[0]~10_combout\);

-- Location: LCCOMB_X19_Y18_N6
\oTH|Mod0|auto_generated|divider|divider|add_sub_15_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_15_result_int[3]~4_combout\ = (\oTH|Mod0|auto_generated|divider|divider|add_sub_15_result_int[2]~3\ & (((\oTH|Mod0|auto_generated|divider|divider|StageOut[72]~98_combout\) # 
-- (\oTH|Mod0|auto_generated|divider|divider|StageOut[72]~125_combout\)))) # (!\oTH|Mod0|auto_generated|divider|divider|add_sub_15_result_int[2]~3\ & ((((\oTH|Mod0|auto_generated|divider|divider|StageOut[72]~98_combout\) # 
-- (\oTH|Mod0|auto_generated|divider|divider|StageOut[72]~125_combout\)))))
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_15_result_int[3]~5\ = CARRY((!\oTH|Mod0|auto_generated|divider|divider|add_sub_15_result_int[2]~3\ & ((\oTH|Mod0|auto_generated|divider|divider|StageOut[72]~98_combout\) # 
-- (\oTH|Mod0|auto_generated|divider|divider|StageOut[72]~125_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod0|auto_generated|divider|divider|StageOut[72]~98_combout\,
	datab => \oTH|Mod0|auto_generated|divider|divider|StageOut[72]~125_combout\,
	datad => VCC,
	cin => \oTH|Mod0|auto_generated|divider|divider|add_sub_15_result_int[2]~3\,
	combout => \oTH|Mod0|auto_generated|divider|divider|add_sub_15_result_int[3]~4_combout\,
	cout => \oTH|Mod0|auto_generated|divider|divider|add_sub_15_result_int[3]~5\);

-- Location: LCCOMB_X20_Y20_N6
\oTH|Mod0|auto_generated|divider|divider|add_sub_16_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_16_result_int[1]~0_combout\ = (((\oTH|Mod0|auto_generated|divider|divider|StageOut[75]~105_combout\) # (\oTH|Mod0|auto_generated|divider|divider|StageOut[75]~106_combout\)))
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_16_result_int[1]~1\ = CARRY((\oTH|Mod0|auto_generated|divider|divider|StageOut[75]~105_combout\) # (\oTH|Mod0|auto_generated|divider|divider|StageOut[75]~106_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod0|auto_generated|divider|divider|StageOut[75]~105_combout\,
	datab => \oTH|Mod0|auto_generated|divider|divider|StageOut[75]~106_combout\,
	datad => VCC,
	combout => \oTH|Mod0|auto_generated|divider|divider|add_sub_16_result_int[1]~0_combout\,
	cout => \oTH|Mod0|auto_generated|divider|divider|add_sub_16_result_int[1]~1\);

-- Location: LCCOMB_X19_Y20_N20
\oTH|Mod0|auto_generated|divider|divider|add_sub_17_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_17_result_int[3]~4_combout\ = (\oTH|Mod0|auto_generated|divider|divider|add_sub_17_result_int[2]~3\ & (((\oTH|Mod0|auto_generated|divider|divider|StageOut[82]~129_combout\) # 
-- (\oTH|Mod0|auto_generated|divider|divider|StageOut[82]~108_combout\)))) # (!\oTH|Mod0|auto_generated|divider|divider|add_sub_17_result_int[2]~3\ & ((((\oTH|Mod0|auto_generated|divider|divider|StageOut[82]~129_combout\) # 
-- (\oTH|Mod0|auto_generated|divider|divider|StageOut[82]~108_combout\)))))
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_17_result_int[3]~5\ = CARRY((!\oTH|Mod0|auto_generated|divider|divider|add_sub_17_result_int[2]~3\ & ((\oTH|Mod0|auto_generated|divider|divider|StageOut[82]~129_combout\) # 
-- (\oTH|Mod0|auto_generated|divider|divider|StageOut[82]~108_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod0|auto_generated|divider|divider|StageOut[82]~129_combout\,
	datab => \oTH|Mod0|auto_generated|divider|divider|StageOut[82]~108_combout\,
	datad => VCC,
	cin => \oTH|Mod0|auto_generated|divider|divider|add_sub_17_result_int[2]~3\,
	combout => \oTH|Mod0|auto_generated|divider|divider|add_sub_17_result_int[3]~4_combout\,
	cout => \oTH|Mod0|auto_generated|divider|divider|add_sub_17_result_int[3]~5\);

-- Location: LCCOMB_X18_Y20_N24
\oTH|Mod0|auto_generated|divider|divider|add_sub_18_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_18_result_int[3]~4_combout\ = (\oTH|Mod0|auto_generated|divider|divider|add_sub_18_result_int[2]~3\ & (((\oTH|Mod0|auto_generated|divider|divider|StageOut[87]~131_combout\) # 
-- (\oTH|Mod0|auto_generated|divider|divider|StageOut[87]~113_combout\)))) # (!\oTH|Mod0|auto_generated|divider|divider|add_sub_18_result_int[2]~3\ & ((((\oTH|Mod0|auto_generated|divider|divider|StageOut[87]~131_combout\) # 
-- (\oTH|Mod0|auto_generated|divider|divider|StageOut[87]~113_combout\)))))
-- \oTH|Mod0|auto_generated|divider|divider|add_sub_18_result_int[3]~5\ = CARRY((!\oTH|Mod0|auto_generated|divider|divider|add_sub_18_result_int[2]~3\ & ((\oTH|Mod0|auto_generated|divider|divider|StageOut[87]~131_combout\) # 
-- (\oTH|Mod0|auto_generated|divider|divider|StageOut[87]~113_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod0|auto_generated|divider|divider|StageOut[87]~131_combout\,
	datab => \oTH|Mod0|auto_generated|divider|divider|StageOut[87]~113_combout\,
	datad => VCC,
	cin => \oTH|Mod0|auto_generated|divider|divider|add_sub_18_result_int[2]~3\,
	combout => \oTH|Mod0|auto_generated|divider|divider|add_sub_18_result_int[3]~4_combout\,
	cout => \oTH|Mod0|auto_generated|divider|divider|add_sub_18_result_int[3]~5\);

-- Location: LCCOMB_X10_Y20_N4
\oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[0]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[0]~16_combout\ = (\oTH|Mod2|auto_generated|divider|divider|StageOut[211]~83_combout\) # (\oTH|Mod2|auto_generated|divider|divider|StageOut[211]~84_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Mod2|auto_generated|divider|divider|StageOut[211]~83_combout\,
	datad => \oTH|Mod2|auto_generated|divider|divider|StageOut[211]~84_combout\,
	combout => \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[0]~16_combout\);

-- Location: LCCOMB_X9_Y20_N12
\oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[1]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[1]~14_combout\ = (\oTH|Div7|auto_generated|divider|divider|StageOut[56]~58_combout\) # (\oTH|Div7|auto_generated|divider|divider|StageOut[56]~78_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div7|auto_generated|divider|divider|StageOut[56]~58_combout\,
	datad => \oTH|Div7|auto_generated|divider|divider|StageOut[56]~78_combout\,
	combout => \oTH|Div7|auto_generated|divider|divider|add_sub_8_result_int[1]~14_combout\);

-- Location: LCCOMB_X21_Y21_N24
\oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[6]~8_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\ & (!\oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[5]~7\ & VCC)) # 
-- (!\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\ & (\oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[5]~7\ $ (GND)))
-- \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[6]~9\ = CARRY((!\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\ & !\oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[5]~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101000000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\,
	datad => VCC,
	cin => \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[5]~7\,
	combout => \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[6]~8_combout\,
	cout => \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[6]~9\);

-- Location: LCCOMB_X20_Y21_N20
\oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[3]~2_combout\ = (\oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[2]~1\ & (((\oTH|Mod1|auto_generated|divider|divider|StageOut[122]~134_combout\) # 
-- (\oTH|Mod1|auto_generated|divider|divider|StageOut[122]~135_combout\)))) # (!\oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[2]~1\ & (!\oTH|Mod1|auto_generated|divider|divider|StageOut[122]~134_combout\ & 
-- (!\oTH|Mod1|auto_generated|divider|divider|StageOut[122]~135_combout\)))
-- \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[3]~3\ = CARRY((!\oTH|Mod1|auto_generated|divider|divider|StageOut[122]~134_combout\ & (!\oTH|Mod1|auto_generated|divider|divider|StageOut[122]~135_combout\ & 
-- !\oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[2]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod1|auto_generated|divider|divider|StageOut[122]~134_combout\,
	datab => \oTH|Mod1|auto_generated|divider|divider|StageOut[122]~135_combout\,
	datad => VCC,
	cin => \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[2]~1\,
	combout => \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[3]~2_combout\,
	cout => \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[3]~3\);

-- Location: LCCOMB_X20_Y21_N22
\oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[4]~4_combout\ = (\oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[3]~3\ & ((((\oTH|Mod1|auto_generated|divider|divider|StageOut[123]~133_combout\) # 
-- (\oTH|Mod1|auto_generated|divider|divider|StageOut[123]~132_combout\))))) # (!\oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[3]~3\ & ((\oTH|Mod1|auto_generated|divider|divider|StageOut[123]~133_combout\) # 
-- ((\oTH|Mod1|auto_generated|divider|divider|StageOut[123]~132_combout\) # (GND))))
-- \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[4]~5\ = CARRY((\oTH|Mod1|auto_generated|divider|divider|StageOut[123]~133_combout\) # ((\oTH|Mod1|auto_generated|divider|divider|StageOut[123]~132_combout\) # 
-- (!\oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[3]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod1|auto_generated|divider|divider|StageOut[123]~133_combout\,
	datab => \oTH|Mod1|auto_generated|divider|divider|StageOut[123]~132_combout\,
	datad => VCC,
	cin => \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[3]~3\,
	combout => \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[4]~4_combout\,
	cout => \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[4]~5\);

-- Location: LCCOMB_X20_Y21_N24
\oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[5]~6_combout\ = (\oTH|Mod1|auto_generated|divider|divider|StageOut[124]~131_combout\ & (((!\oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[4]~5\)))) # 
-- (!\oTH|Mod1|auto_generated|divider|divider|StageOut[124]~131_combout\ & ((\oTH|Mod1|auto_generated|divider|divider|StageOut[124]~130_combout\ & (!\oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[4]~5\)) # 
-- (!\oTH|Mod1|auto_generated|divider|divider|StageOut[124]~130_combout\ & ((\oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[4]~5\) # (GND)))))
-- \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[5]~7\ = CARRY(((!\oTH|Mod1|auto_generated|divider|divider|StageOut[124]~131_combout\ & !\oTH|Mod1|auto_generated|divider|divider|StageOut[124]~130_combout\)) # 
-- (!\oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[4]~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod1|auto_generated|divider|divider|StageOut[124]~131_combout\,
	datab => \oTH|Mod1|auto_generated|divider|divider|StageOut[124]~130_combout\,
	datad => VCC,
	cin => \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[4]~5\,
	combout => \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[5]~6_combout\,
	cout => \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[5]~7\);

-- Location: LCCOMB_X20_Y21_N26
\oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[6]~8_combout\ = (\oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[5]~7\ & (((\oTH|Mod1|auto_generated|divider|divider|StageOut[125]~128_combout\) # 
-- (\oTH|Mod1|auto_generated|divider|divider|StageOut[125]~129_combout\)))) # (!\oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[5]~7\ & ((((\oTH|Mod1|auto_generated|divider|divider|StageOut[125]~128_combout\) # 
-- (\oTH|Mod1|auto_generated|divider|divider|StageOut[125]~129_combout\)))))
-- \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[6]~9\ = CARRY((!\oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[5]~7\ & ((\oTH|Mod1|auto_generated|divider|divider|StageOut[125]~128_combout\) # 
-- (\oTH|Mod1|auto_generated|divider|divider|StageOut[125]~129_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod1|auto_generated|divider|divider|StageOut[125]~128_combout\,
	datab => \oTH|Mod1|auto_generated|divider|divider|StageOut[125]~129_combout\,
	datad => VCC,
	cin => \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[5]~7\,
	combout => \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[6]~8_combout\,
	cout => \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[6]~9\);

-- Location: LCCOMB_X22_Y21_N6
\oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[0]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[0]~16_combout\ = !\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[7]~12_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[7]~12_combout\,
	combout => \oTH|Mod1|auto_generated|divider|divider|add_sub_15_result_int[0]~16_combout\);

-- Location: LCCOMB_X22_Y21_N0
\oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[1]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[1]~14_combout\ = (\oTH|Mod1|auto_generated|divider|divider|StageOut[120]~144_combout\) # (\oTH|Mod1|auto_generated|divider|divider|StageOut[120]~143_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Mod1|auto_generated|divider|divider|StageOut[120]~144_combout\,
	datac => \oTH|Mod1|auto_generated|divider|divider|StageOut[120]~143_combout\,
	combout => \oTH|Mod1|auto_generated|divider|divider|add_sub_16_result_int[1]~14_combout\);

-- Location: LCCOMB_X19_Y21_N20
\oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[6]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[6]~8_combout\ = (\oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[5]~7\ & (((\oTH|Mod1|auto_generated|divider|divider|StageOut[133]~139_combout\) # 
-- (\oTH|Mod1|auto_generated|divider|divider|StageOut[133]~175_combout\)))) # (!\oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[5]~7\ & ((((\oTH|Mod1|auto_generated|divider|divider|StageOut[133]~139_combout\) # 
-- (\oTH|Mod1|auto_generated|divider|divider|StageOut[133]~175_combout\)))))
-- \oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[6]~9\ = CARRY((!\oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[5]~7\ & ((\oTH|Mod1|auto_generated|divider|divider|StageOut[133]~139_combout\) # 
-- (\oTH|Mod1|auto_generated|divider|divider|StageOut[133]~175_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod1|auto_generated|divider|divider|StageOut[133]~139_combout\,
	datab => \oTH|Mod1|auto_generated|divider|divider|StageOut[133]~175_combout\,
	datad => VCC,
	cin => \oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[5]~7\,
	combout => \oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[6]~8_combout\,
	cout => \oTH|Mod1|auto_generated|divider|divider|add_sub_17_result_int[6]~9\);

-- Location: LCCOMB_X18_Y21_N2
\oTH|Mod1|auto_generated|divider|divider|add_sub_18_result_int[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod1|auto_generated|divider|divider|add_sub_18_result_int[2]~0_combout\ = (((\oTH|Mod1|auto_generated|divider|divider|StageOut[137]~179_combout\) # (\oTH|Mod1|auto_generated|divider|divider|StageOut[137]~180_combout\)))
-- \oTH|Mod1|auto_generated|divider|divider|add_sub_18_result_int[2]~1\ = CARRY((\oTH|Mod1|auto_generated|divider|divider|StageOut[137]~179_combout\) # (\oTH|Mod1|auto_generated|divider|divider|StageOut[137]~180_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod1|auto_generated|divider|divider|StageOut[137]~179_combout\,
	datab => \oTH|Mod1|auto_generated|divider|divider|StageOut[137]~180_combout\,
	datad => VCC,
	combout => \oTH|Mod1|auto_generated|divider|divider|add_sub_18_result_int[2]~0_combout\,
	cout => \oTH|Mod1|auto_generated|divider|divider|add_sub_18_result_int[2]~1\);

-- Location: LCCOMB_X16_Y19_N16
\oTH|Div4|auto_generated|divider|divider|add_sub_3_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div4|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\ = (\oTH|Div4|auto_generated|divider|divider|add_sub_3_result_int[1]~1\ & (((\oTH|Mod1|auto_generated|divider|divider|StageOut[157]~172_combout\) # 
-- (\oTH|Mod1|auto_generated|divider|divider|StageOut[157]~157_combout\)))) # (!\oTH|Div4|auto_generated|divider|divider|add_sub_3_result_int[1]~1\ & (!\oTH|Mod1|auto_generated|divider|divider|StageOut[157]~172_combout\ & 
-- (!\oTH|Mod1|auto_generated|divider|divider|StageOut[157]~157_combout\)))
-- \oTH|Div4|auto_generated|divider|divider|add_sub_3_result_int[2]~3\ = CARRY((!\oTH|Mod1|auto_generated|divider|divider|StageOut[157]~172_combout\ & (!\oTH|Mod1|auto_generated|divider|divider|StageOut[157]~157_combout\ & 
-- !\oTH|Div4|auto_generated|divider|divider|add_sub_3_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod1|auto_generated|divider|divider|StageOut[157]~172_combout\,
	datab => \oTH|Mod1|auto_generated|divider|divider|StageOut[157]~157_combout\,
	datad => VCC,
	cin => \oTH|Div4|auto_generated|divider|divider|add_sub_3_result_int[1]~1\,
	combout => \oTH|Div4|auto_generated|divider|divider|add_sub_3_result_int[2]~2_combout\,
	cout => \oTH|Div4|auto_generated|divider|divider|add_sub_3_result_int[2]~3\);

-- Location: LCCOMB_X15_Y19_N2
\oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\ = (((\oTH|Div4|auto_generated|divider|divider|StageOut[15]~45_combout\) # (\oTH|Div4|auto_generated|divider|divider|StageOut[15]~44_combout\)))
-- \oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[1]~1\ = CARRY((\oTH|Div4|auto_generated|divider|divider|StageOut[15]~45_combout\) # (\oTH|Div4|auto_generated|divider|divider|StageOut[15]~44_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div4|auto_generated|divider|divider|StageOut[15]~45_combout\,
	datab => \oTH|Div4|auto_generated|divider|divider|StageOut[15]~44_combout\,
	datad => VCC,
	combout => \oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[1]~0_combout\,
	cout => \oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[1]~1\);

-- Location: LCCOMB_X15_Y19_N6
\oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\ = (\oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ & (((\oTH|Div4|auto_generated|divider|divider|StageOut[17]~42_combout\) # 
-- (\oTH|Div4|auto_generated|divider|divider|StageOut[17]~31_combout\)))) # (!\oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ & ((((\oTH|Div4|auto_generated|divider|divider|StageOut[17]~42_combout\) # 
-- (\oTH|Div4|auto_generated|divider|divider|StageOut[17]~31_combout\)))))
-- \oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[3]~5\ = CARRY((!\oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[2]~3\ & ((\oTH|Div4|auto_generated|divider|divider|StageOut[17]~42_combout\) # 
-- (\oTH|Div4|auto_generated|divider|divider|StageOut[17]~31_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div4|auto_generated|divider|divider|StageOut[17]~42_combout\,
	datab => \oTH|Div4|auto_generated|divider|divider|StageOut[17]~31_combout\,
	datad => VCC,
	cin => \oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[2]~3\,
	combout => \oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[3]~4_combout\,
	cout => \oTH|Div4|auto_generated|divider|divider|add_sub_4_result_int[3]~5\);

-- Location: LCCOMB_X15_Y19_N20
\oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\ = (((\oTH|Div4|auto_generated|divider|divider|StageOut[20]~52_combout\) # (\oTH|Div4|auto_generated|divider|divider|StageOut[20]~53_combout\)))
-- \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ = CARRY((\oTH|Div4|auto_generated|divider|divider|StageOut[20]~52_combout\) # (\oTH|Div4|auto_generated|divider|divider|StageOut[20]~53_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div4|auto_generated|divider|divider|StageOut[20]~52_combout\,
	datab => \oTH|Div4|auto_generated|divider|divider|StageOut[20]~53_combout\,
	datad => VCC,
	combout => \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[1]~0_combout\,
	cout => \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[1]~1\);

-- Location: LCCOMB_X15_Y19_N22
\oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\ = (\oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ & (((\oTH|Div4|auto_generated|divider|divider|StageOut[21]~36_combout\) # 
-- (\oTH|Div4|auto_generated|divider|divider|StageOut[21]~35_combout\)))) # (!\oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[1]~1\ & (!\oTH|Div4|auto_generated|divider|divider|StageOut[21]~36_combout\ & 
-- (!\oTH|Div4|auto_generated|divider|divider|StageOut[21]~35_combout\)))
-- \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ = CARRY((!\oTH|Div4|auto_generated|divider|divider|StageOut[21]~36_combout\ & (!\oTH|Div4|auto_generated|divider|divider|StageOut[21]~35_combout\ & 
-- !\oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[1]~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100000001",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div4|auto_generated|divider|divider|StageOut[21]~36_combout\,
	datab => \oTH|Div4|auto_generated|divider|divider|StageOut[21]~35_combout\,
	datad => VCC,
	cin => \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[1]~1\,
	combout => \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[2]~2_combout\,
	cout => \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[2]~3\);

-- Location: LCCOMB_X15_Y19_N24
\oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[3]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\ = (\oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ & (((\oTH|Div4|auto_generated|divider|divider|StageOut[22]~34_combout\) # 
-- (\oTH|Div4|auto_generated|divider|divider|StageOut[22]~47_combout\)))) # (!\oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ & ((((\oTH|Div4|auto_generated|divider|divider|StageOut[22]~34_combout\) # 
-- (\oTH|Div4|auto_generated|divider|divider|StageOut[22]~47_combout\)))))
-- \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[3]~5\ = CARRY((!\oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[2]~3\ & ((\oTH|Div4|auto_generated|divider|divider|StageOut[22]~34_combout\) # 
-- (\oTH|Div4|auto_generated|divider|divider|StageOut[22]~47_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000100001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div4|auto_generated|divider|divider|StageOut[22]~34_combout\,
	datab => \oTH|Div4|auto_generated|divider|divider|StageOut[22]~47_combout\,
	datad => VCC,
	cin => \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[2]~3\,
	combout => \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[3]~4_combout\,
	cout => \oTH|Div4|auto_generated|divider|divider|add_sub_5_result_int[3]~5\);

-- Location: LCFF_X25_Y8_N7
\cTS2|cntr[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \cTS2|cntr[3]~17_combout\,
	sclr => \cTS2|cntr[0]~19_combout\,
	ena => \cTS2|cntr[0]~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \cTS2|cntr\(3));

-- Location: LCFF_X25_Y8_N13
\cTS2|cntr[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \cTS2|cntr[6]~24_combout\,
	sclr => \cTS2|cntr[0]~19_combout\,
	ena => \cTS2|cntr[0]~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \cTS2|cntr\(6));

-- Location: LCFF_X23_Y8_N7
\one_sec|cntr[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \one_sec|cntr[0]~26_combout\,
	sclr => \one_sec|cntr[10]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \one_sec|cntr\(0));

-- Location: LCFF_X23_Y8_N11
\one_sec|cntr[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \one_sec|cntr[2]~31_combout\,
	sclr => \one_sec|cntr[10]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \one_sec|cntr\(2));

-- Location: LCFF_X23_Y8_N13
\one_sec|cntr[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \one_sec|cntr[3]~33_combout\,
	sclr => \one_sec|cntr[10]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \one_sec|cntr\(3));

-- Location: LCFF_X23_Y8_N17
\one_sec|cntr[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \one_sec|cntr[5]~37_combout\,
	sclr => \one_sec|cntr[10]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \one_sec|cntr\(5));

-- Location: LCFF_X23_Y8_N25
\one_sec|cntr[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \one_sec|cntr[9]~45_combout\,
	sclr => \one_sec|cntr[10]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \one_sec|cntr\(9));

-- Location: LCFF_X23_Y7_N11
\one_sec|cntr[18]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \one_sec|cntr[18]~63_combout\,
	sclr => \one_sec|cntr[10]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \one_sec|cntr\(18));

-- Location: LCFF_X23_Y7_N21
\one_sec|cntr[23]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \one_sec|cntr[23]~73_combout\,
	sclr => \one_sec|cntr[10]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \one_sec|cntr\(23));

-- Location: LCFF_X23_Y7_N25
\one_sec|cntr[25]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \one_sec|cntr[25]~77_combout\,
	sclr => \one_sec|cntr[10]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \one_sec|cntr\(25));

-- Location: LCCOMB_X25_Y8_N6
\cTS2|cntr[3]~17\ : cycloneii_lcell_comb
-- Equation(s):
-- \cTS2|cntr[3]~17_combout\ = (\cTS2|cntr\(3) & (!\cTS2|cntr[2]~16\)) # (!\cTS2|cntr\(3) & ((\cTS2|cntr[2]~16\) # (GND)))
-- \cTS2|cntr[3]~18\ = CARRY((!\cTS2|cntr[2]~16\) # (!\cTS2|cntr\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \cTS2|cntr\(3),
	datad => VCC,
	cin => \cTS2|cntr[2]~16\,
	combout => \cTS2|cntr[3]~17_combout\,
	cout => \cTS2|cntr[3]~18\);

-- Location: LCCOMB_X25_Y8_N12
\cTS2|cntr[6]~24\ : cycloneii_lcell_comb
-- Equation(s):
-- \cTS2|cntr[6]~24_combout\ = (\cTS2|cntr\(6) & (\cTS2|cntr[5]~23\ $ (GND))) # (!\cTS2|cntr\(6) & (!\cTS2|cntr[5]~23\ & VCC))
-- \cTS2|cntr[6]~25\ = CARRY((\cTS2|cntr\(6) & !\cTS2|cntr[5]~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \cTS2|cntr\(6),
	datad => VCC,
	cin => \cTS2|cntr[5]~23\,
	combout => \cTS2|cntr[6]~24_combout\,
	cout => \cTS2|cntr[6]~25\);

-- Location: LCCOMB_X23_Y8_N6
\one_sec|cntr[0]~26\ : cycloneii_lcell_comb
-- Equation(s):
-- \one_sec|cntr[0]~26_combout\ = \one_sec|cntr\(0) $ (VCC)
-- \one_sec|cntr[0]~27\ = CARRY(\one_sec|cntr\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \one_sec|cntr\(0),
	datad => VCC,
	combout => \one_sec|cntr[0]~26_combout\,
	cout => \one_sec|cntr[0]~27\);

-- Location: LCCOMB_X23_Y8_N10
\one_sec|cntr[2]~31\ : cycloneii_lcell_comb
-- Equation(s):
-- \one_sec|cntr[2]~31_combout\ = (\one_sec|cntr\(2) & (\one_sec|cntr[1]~30\ $ (GND))) # (!\one_sec|cntr\(2) & (!\one_sec|cntr[1]~30\ & VCC))
-- \one_sec|cntr[2]~32\ = CARRY((\one_sec|cntr\(2) & !\one_sec|cntr[1]~30\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \one_sec|cntr\(2),
	datad => VCC,
	cin => \one_sec|cntr[1]~30\,
	combout => \one_sec|cntr[2]~31_combout\,
	cout => \one_sec|cntr[2]~32\);

-- Location: LCCOMB_X23_Y8_N12
\one_sec|cntr[3]~33\ : cycloneii_lcell_comb
-- Equation(s):
-- \one_sec|cntr[3]~33_combout\ = (\one_sec|cntr\(3) & (!\one_sec|cntr[2]~32\)) # (!\one_sec|cntr\(3) & ((\one_sec|cntr[2]~32\) # (GND)))
-- \one_sec|cntr[3]~34\ = CARRY((!\one_sec|cntr[2]~32\) # (!\one_sec|cntr\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \one_sec|cntr\(3),
	datad => VCC,
	cin => \one_sec|cntr[2]~32\,
	combout => \one_sec|cntr[3]~33_combout\,
	cout => \one_sec|cntr[3]~34\);

-- Location: LCCOMB_X23_Y8_N16
\one_sec|cntr[5]~37\ : cycloneii_lcell_comb
-- Equation(s):
-- \one_sec|cntr[5]~37_combout\ = (\one_sec|cntr\(5) & (!\one_sec|cntr[4]~36\)) # (!\one_sec|cntr\(5) & ((\one_sec|cntr[4]~36\) # (GND)))
-- \one_sec|cntr[5]~38\ = CARRY((!\one_sec|cntr[4]~36\) # (!\one_sec|cntr\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \one_sec|cntr\(5),
	datad => VCC,
	cin => \one_sec|cntr[4]~36\,
	combout => \one_sec|cntr[5]~37_combout\,
	cout => \one_sec|cntr[5]~38\);

-- Location: LCCOMB_X23_Y8_N24
\one_sec|cntr[9]~45\ : cycloneii_lcell_comb
-- Equation(s):
-- \one_sec|cntr[9]~45_combout\ = (\one_sec|cntr\(9) & (!\one_sec|cntr[8]~44\)) # (!\one_sec|cntr\(9) & ((\one_sec|cntr[8]~44\) # (GND)))
-- \one_sec|cntr[9]~46\ = CARRY((!\one_sec|cntr[8]~44\) # (!\one_sec|cntr\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \one_sec|cntr\(9),
	datad => VCC,
	cin => \one_sec|cntr[8]~44\,
	combout => \one_sec|cntr[9]~45_combout\,
	cout => \one_sec|cntr[9]~46\);

-- Location: LCCOMB_X23_Y7_N10
\one_sec|cntr[18]~63\ : cycloneii_lcell_comb
-- Equation(s):
-- \one_sec|cntr[18]~63_combout\ = (\one_sec|cntr\(18) & (\one_sec|cntr[17]~62\ $ (GND))) # (!\one_sec|cntr\(18) & (!\one_sec|cntr[17]~62\ & VCC))
-- \one_sec|cntr[18]~64\ = CARRY((\one_sec|cntr\(18) & !\one_sec|cntr[17]~62\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \one_sec|cntr\(18),
	datad => VCC,
	cin => \one_sec|cntr[17]~62\,
	combout => \one_sec|cntr[18]~63_combout\,
	cout => \one_sec|cntr[18]~64\);

-- Location: LCCOMB_X23_Y7_N20
\one_sec|cntr[23]~73\ : cycloneii_lcell_comb
-- Equation(s):
-- \one_sec|cntr[23]~73_combout\ = (\one_sec|cntr\(23) & (!\one_sec|cntr[22]~72\)) # (!\one_sec|cntr\(23) & ((\one_sec|cntr[22]~72\) # (GND)))
-- \one_sec|cntr[23]~74\ = CARRY((!\one_sec|cntr[22]~72\) # (!\one_sec|cntr\(23)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \one_sec|cntr\(23),
	datad => VCC,
	cin => \one_sec|cntr[22]~72\,
	combout => \one_sec|cntr[23]~73_combout\,
	cout => \one_sec|cntr[23]~74\);

-- Location: LCCOMB_X23_Y7_N22
\one_sec|cntr[24]~75\ : cycloneii_lcell_comb
-- Equation(s):
-- \one_sec|cntr[24]~75_combout\ = (\one_sec|cntr\(24) & (\one_sec|cntr[23]~74\ $ (GND))) # (!\one_sec|cntr\(24) & (!\one_sec|cntr[23]~74\ & VCC))
-- \one_sec|cntr[24]~76\ = CARRY((\one_sec|cntr\(24) & !\one_sec|cntr[23]~74\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \one_sec|cntr\(24),
	datad => VCC,
	cin => \one_sec|cntr[23]~74\,
	combout => \one_sec|cntr[24]~75_combout\,
	cout => \one_sec|cntr[24]~76\);

-- Location: LCCOMB_X23_Y7_N24
\one_sec|cntr[25]~77\ : cycloneii_lcell_comb
-- Equation(s):
-- \one_sec|cntr[25]~77_combout\ = \one_sec|cntr\(25) $ (\one_sec|cntr[24]~76\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \one_sec|cntr\(25),
	cin => \one_sec|cntr[24]~76\,
	combout => \one_sec|cntr[25]~77_combout\);

-- Location: LCFF_X31_Y19_N13
\cET|tacts[20]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \cET|cntr\(20),
	sload => VCC,
	ena => \cET|tacts[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \cET|tacts\(20));

-- Location: LCCOMB_X30_Y19_N4
\oTH|Div0|auto_generated|divider|divider|StageOut[28]~200\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[28]~200_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_14~10_combout\ & \cET|tacts\(20))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div0|auto_generated|divider|divider|op_14~10_combout\,
	datac => \cET|tacts\(20),
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[28]~200_combout\);

-- Location: LCCOMB_X30_Y19_N12
\oTH|Div0|auto_generated|divider|divider|StageOut[27]~203\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[27]~203_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_14~6_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_14~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div0|auto_generated|divider|divider|op_14~6_combout\,
	datac => \oTH|Div0|auto_generated|divider|divider|op_14~10_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[27]~203_combout\);

-- Location: LCCOMB_X29_Y19_N24
\oTH|Div0|auto_generated|divider|divider|StageOut[26]~204\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[26]~204_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_14~10_combout\ & \cET|tacts\(18))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_14~10_combout\,
	datad => \cET|tacts\(18),
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[26]~204_combout\);

-- Location: LCCOMB_X27_Y19_N20
\oTH|Div0|auto_generated|divider|divider|StageOut[25]~207\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[25]~207_combout\ = (!\oTH|Div0|auto_generated|divider|divider|op_14~10_combout\ & \oTH|Div0|auto_generated|divider|divider|op_14~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|op_14~10_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_14~2_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[25]~207_combout\);

-- Location: LCCOMB_X29_Y18_N2
\oTH|Div0|auto_generated|divider|divider|StageOut[24]~209\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[24]~209_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_14~0_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_14~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_14~0_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_14~10_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[24]~209_combout\);

-- Location: LCCOMB_X29_Y19_N8
\oTH|Div0|auto_generated|divider|divider|StageOut[32]~212\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[32]~212_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_15~4_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_15~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_15~4_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_15~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[32]~212_combout\);

-- Location: LCCOMB_X29_Y19_N0
\oTH|Div0|auto_generated|divider|divider|StageOut[30]~215\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[30]~215_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_15~0_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_15~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|op_15~0_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_15~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[30]~215_combout\);

-- Location: LCCOMB_X30_Y20_N12
\oTH|Div0|auto_generated|divider|divider|StageOut[39]~217\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[39]~217_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_16~6_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_16~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_16~6_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_16~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[39]~217_combout\);

-- Location: LCCOMB_X30_Y20_N10
\oTH|Div0|auto_generated|divider|divider|StageOut[38]~218\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[38]~218_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_16~4_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_16~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_16~4_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_16~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[38]~218_combout\);

-- Location: LCCOMB_X30_Y20_N0
\oTH|Div0|auto_generated|divider|divider|StageOut[37]~219\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[37]~219_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_16~2_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_16~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_16~2_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_16~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[37]~219_combout\);

-- Location: LCCOMB_X30_Y20_N6
\oTH|Div0|auto_generated|divider|divider|StageOut[36]~220\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[36]~220_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_16~12_combout\ & \cET|tacts\(14))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|op_16~12_combout\,
	datad => \cET|tacts\(14),
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[36]~220_combout\);

-- Location: LCCOMB_X31_Y20_N20
\oTH|Div0|auto_generated|divider|divider|StageOut[42]~226\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[42]~226_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_17~12_combout\ & \cET|tacts\(13))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div0|auto_generated|divider|divider|op_17~12_combout\,
	datac => \cET|tacts\(13),
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[42]~226_combout\);

-- Location: LCCOMB_X27_Y19_N22
\oTH|Div1|auto_generated|divider|divider|StageOut[68]~185\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[68]~185_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\ & \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[5]~8_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[68]~185_combout\);

-- Location: LCCOMB_X27_Y19_N2
\oTH|Div1|auto_generated|divider|divider|StageOut[67]~187\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[67]~187_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\ & \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[4]~6_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[67]~187_combout\);

-- Location: LCCOMB_X26_Y19_N30
\oTH|Div1|auto_generated|divider|divider|StageOut[66]~188\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[66]~188_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_16~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\,
	datac => \oTH|Div0|auto_generated|divider|divider|op_16~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[66]~188_combout\);

-- Location: LCCOMB_X26_Y19_N24
\oTH|Div1|auto_generated|divider|divider|StageOut[65]~191\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[65]~191_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\ & \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[2]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[2]~2_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[65]~191_combout\);

-- Location: LCCOMB_X26_Y19_N20
\oTH|Div1|auto_generated|divider|divider|StageOut[64]~193\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[64]~193_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\ & \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[1]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[1]~0_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[64]~193_combout\);

-- Location: LCCOMB_X31_Y21_N2
\oTH|Div0|auto_generated|divider|divider|StageOut[50]~230\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[50]~230_combout\ = (!\oTH|Div0|auto_generated|divider|divider|op_18~12_combout\ & \oTH|Div0|auto_generated|divider|divider|op_18~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|op_18~12_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_18~4_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[50]~230_combout\);

-- Location: LCCOMB_X30_Y21_N30
\oTH|Div0|auto_generated|divider|divider|StageOut[48]~232\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[48]~232_combout\ = (\cET|tacts\(12) & \oTH|Div0|auto_generated|divider|divider|op_18~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \cET|tacts\(12),
	datad => \oTH|Div0|auto_generated|divider|divider|op_18~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[48]~232_combout\);

-- Location: LCCOMB_X27_Y19_N26
\oTH|Div1|auto_generated|divider|divider|StageOut[63]~195\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[63]~195_combout\ = (!\oTH|Div0|auto_generated|divider|divider|op_19~12_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010100000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|op_19~12_combout\,
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[63]~195_combout\);

-- Location: LCCOMB_X26_Y19_N26
\oTH|Div1|auto_generated|divider|divider|StageOut[75]~196\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[75]~196_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[5]~8_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[7]~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[75]~196_combout\);

-- Location: LCCOMB_X26_Y18_N10
\oTH|Div1|auto_generated|divider|divider|StageOut[73]~198\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[73]~198_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[3]~4_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[3]~4_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[7]~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[73]~198_combout\);

-- Location: LCCOMB_X26_Y18_N12
\oTH|Div1|auto_generated|divider|divider|StageOut[72]~199\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[72]~199_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[2]~2_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[2]~2_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[7]~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[72]~199_combout\);

-- Location: LCCOMB_X30_Y21_N22
\oTH|Div0|auto_generated|divider|divider|StageOut[56]~236\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[56]~236_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_19~4_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_19~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div0|auto_generated|divider|divider|op_19~4_combout\,
	datac => \oTH|Div0|auto_generated|divider|divider|op_19~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[56]~236_combout\);

-- Location: LCCOMB_X30_Y21_N12
\oTH|Div0|auto_generated|divider|divider|StageOut[55]~237\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[55]~237_combout\ = (!\oTH|Div0|auto_generated|divider|divider|op_19~12_combout\ & \oTH|Div0|auto_generated|divider|divider|op_19~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_19~12_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_19~2_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[55]~237_combout\);

-- Location: LCCOMB_X30_Y21_N4
\oTH|Div0|auto_generated|divider|divider|StageOut[54]~239\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[54]~239_combout\ = (!\oTH|Div0|auto_generated|divider|divider|op_19~12_combout\ & \oTH|Div0|auto_generated|divider|divider|op_19~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_19~12_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_19~0_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[54]~239_combout\);

-- Location: LCCOMB_X27_Y18_N4
\oTH|Div1|auto_generated|divider|divider|StageOut[70]~202\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[70]~202_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[7]~12_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_1~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div1|auto_generated|divider|divider|add_sub_10_result_int[7]~12_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_1~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[70]~202_combout\);

-- Location: LCCOMB_X26_Y18_N8
\oTH|Div1|auto_generated|divider|divider|StageOut[80]~205\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[80]~205_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[3]~4_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[3]~4_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[7]~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[80]~205_combout\);

-- Location: LCCOMB_X26_Y18_N6
\oTH|Div1|auto_generated|divider|divider|StageOut[79]~206\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[79]~206_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[7]~12_combout\ & \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[2]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[7]~12_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[2]~2_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[79]~206_combout\);

-- Location: LCCOMB_X25_Y18_N10
\oTH|Div1|auto_generated|divider|divider|StageOut[78]~207\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[78]~207_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[1]~0_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[1]~0_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[7]~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[78]~207_combout\);

-- Location: LCCOMB_X30_Y21_N18
\oTH|Div0|auto_generated|divider|divider|StageOut[64]~240\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[64]~240_combout\ = (!\oTH|Div0|auto_generated|divider|divider|op_1~12_combout\ & \oTH|Div0|auto_generated|divider|divider|op_1~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div0|auto_generated|divider|divider|op_1~12_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_1~8_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[64]~240_combout\);

-- Location: LCCOMB_X30_Y18_N6
\oTH|Div0|auto_generated|divider|divider|StageOut[62]~242\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[62]~242_combout\ = (!\oTH|Div0|auto_generated|divider|divider|op_1~12_combout\ & \oTH|Div0|auto_generated|divider|divider|op_1~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_1~12_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_1~4_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[62]~242_combout\);

-- Location: LCCOMB_X31_Y18_N6
\oTH|Div0|auto_generated|divider|divider|StageOut[60]~245\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[60]~245_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_1~0_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_1~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div0|auto_generated|divider|divider|op_1~0_combout\,
	datac => \oTH|Div0|auto_generated|divider|divider|op_1~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[60]~245_combout\);

-- Location: LCCOMB_X25_Y18_N6
\oTH|Div1|auto_generated|divider|divider|StageOut[77]~209\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[77]~209_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[7]~12_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_2~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[7]~12_combout\,
	datac => \oTH|Div0|auto_generated|divider|divider|op_2~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[77]~209_combout\);

-- Location: LCCOMB_X25_Y21_N24
\oTH|Div1|auto_generated|divider|divider|StageOut[89]~210\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[89]~210_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[5]~8_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[7]~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[89]~210_combout\);

-- Location: LCCOMB_X30_Y18_N30
\oTH|Div0|auto_generated|divider|divider|StageOut[70]~246\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[70]~246_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_2~8_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_2~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div0|auto_generated|divider|divider|op_2~8_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_2~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[70]~246_combout\);

-- Location: LCCOMB_X30_Y18_N12
\oTH|Div0|auto_generated|divider|divider|StageOut[69]~247\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[69]~247_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_2~6_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_2~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_2~6_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_2~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[69]~247_combout\);

-- Location: LCCOMB_X30_Y18_N10
\oTH|Div0|auto_generated|divider|divider|StageOut[67]~249\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[67]~249_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_2~2_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_2~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_2~2_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_2~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[67]~249_combout\);

-- Location: LCCOMB_X31_Y18_N12
\oTH|Div0|auto_generated|divider|divider|StageOut[66]~251\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[66]~251_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_2~0_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_2~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|op_2~0_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_2~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[66]~251_combout\);

-- Location: LCCOMB_X25_Y21_N4
\oTH|Div1|auto_generated|divider|divider|StageOut[84]~216\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[84]~216_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[7]~12_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_3~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[7]~12_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_3~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[84]~216_combout\);

-- Location: LCCOMB_X26_Y20_N22
\oTH|Div1|auto_generated|divider|divider|StageOut[95]~218\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[95]~218_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[4]~6_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[4]~6_combout\,
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[7]~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[95]~218_combout\);

-- Location: LCCOMB_X25_Y20_N8
\oTH|Div1|auto_generated|divider|divider|StageOut[93]~220\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[93]~220_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[2]~2_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[2]~2_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[7]~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[93]~220_combout\);

-- Location: LCCOMB_X32_Y18_N22
\oTH|Div0|auto_generated|divider|divider|StageOut[75]~253\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[75]~253_combout\ = (!\oTH|Div0|auto_generated|divider|divider|op_3~12_combout\ & \oTH|Div0|auto_generated|divider|divider|op_3~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_3~12_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_3~6_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[75]~253_combout\);

-- Location: LCCOMB_X32_Y18_N24
\oTH|Div0|auto_generated|divider|divider|StageOut[74]~254\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[74]~254_combout\ = (!\oTH|Div0|auto_generated|divider|divider|op_3~12_combout\ & \oTH|Div0|auto_generated|divider|divider|op_3~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_3~12_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_3~4_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[74]~254_combout\);

-- Location: LCCOMB_X32_Y18_N26
\oTH|Div0|auto_generated|divider|divider|StageOut[73]~255\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[73]~255_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_3~2_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_3~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|op_3~2_combout\,
	datac => \oTH|Div0|auto_generated|divider|divider|op_3~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[73]~255_combout\);

-- Location: LCCOMB_X32_Y18_N6
\oTH|Div0|auto_generated|divider|divider|StageOut[72]~257\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[72]~257_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_3~0_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_3~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|op_3~0_combout\,
	datac => \oTH|Div0|auto_generated|divider|divider|op_3~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[72]~257_combout\);

-- Location: LCCOMB_X26_Y20_N20
\oTH|Div1|auto_generated|divider|divider|StageOut[91]~222\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[91]~222_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[7]~12_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_4~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[7]~12_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_4~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[91]~222_combout\);

-- Location: LCCOMB_X25_Y20_N10
\oTH|Div1|auto_generated|divider|divider|StageOut[103]~224\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[103]~224_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[5]~8_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[5]~8_combout\,
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[103]~224_combout\);

-- Location: LCCOMB_X25_Y20_N12
\oTH|Div1|auto_generated|divider|divider|StageOut[102]~225\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[102]~225_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\ & \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[4]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[4]~6_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[102]~225_combout\);

-- Location: LCCOMB_X22_Y20_N2
\oTH|Div1|auto_generated|divider|divider|StageOut[101]~226\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[101]~226_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[3]~4_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[3]~4_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[101]~226_combout\);

-- Location: LCCOMB_X22_Y20_N4
\oTH|Div1|auto_generated|divider|divider|StageOut[99]~228\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[99]~228_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[1]~0_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[1]~0_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[99]~228_combout\);

-- Location: LCCOMB_X33_Y18_N10
\oTH|Div0|auto_generated|divider|divider|StageOut[82]~258\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[82]~258_combout\ = (!\oTH|Div0|auto_generated|divider|divider|op_4~12_combout\ & \oTH|Div0|auto_generated|divider|divider|op_4~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_4~12_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_4~8_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[82]~258_combout\);

-- Location: LCCOMB_X33_Y18_N8
\oTH|Div0|auto_generated|divider|divider|StageOut[81]~259\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[81]~259_combout\ = (!\oTH|Div0|auto_generated|divider|divider|op_4~12_combout\ & \oTH|Div0|auto_generated|divider|divider|op_4~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_4~12_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_4~6_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[81]~259_combout\);

-- Location: LCCOMB_X33_Y18_N30
\oTH|Div0|auto_generated|divider|divider|StageOut[80]~260\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[80]~260_combout\ = (!\oTH|Div0|auto_generated|divider|divider|op_4~12_combout\ & \oTH|Div0|auto_generated|divider|divider|op_4~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_4~12_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_4~4_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[80]~260_combout\);

-- Location: LCCOMB_X33_Y18_N20
\oTH|Div0|auto_generated|divider|divider|StageOut[79]~261\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[79]~261_combout\ = (!\oTH|Div0|auto_generated|divider|divider|op_4~12_combout\ & \oTH|Div0|auto_generated|divider|divider|op_4~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_4~12_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_4~2_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[79]~261_combout\);

-- Location: LCCOMB_X33_Y18_N14
\oTH|Div0|auto_generated|divider|divider|StageOut[78]~262\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[78]~262_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_4~12_combout\ & \cET|tacts\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_4~12_combout\,
	datad => \cET|tacts\(7),
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[78]~262_combout\);

-- Location: LCCOMB_X25_Y20_N6
\oTH|Div1|auto_generated|divider|divider|StageOut[98]~230\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[98]~230_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_5~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_14_result_int[7]~12_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_5~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[98]~230_combout\);

-- Location: LCCOMB_X13_Y19_N0
\oTH|Mod2|auto_generated|divider|divider|StageOut[207]~59\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod2|auto_generated|divider|divider|StageOut[207]~59_combout\ = (\oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[9]~12_combout\ & !\oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[9]~12_combout\,
	datad => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\,
	combout => \oTH|Mod2|auto_generated|divider|divider|StageOut[207]~59_combout\);

-- Location: LCCOMB_X11_Y19_N6
\oTH|Mod2|auto_generated|divider|divider|StageOut[206]~61\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod2|auto_generated|divider|divider|StageOut[206]~61_combout\ = (!\oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\ & \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[8]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\,
	datad => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[8]~10_combout\,
	combout => \oTH|Mod2|auto_generated|divider|divider|StageOut[206]~61_combout\);

-- Location: LCCOMB_X12_Y19_N10
\oTH|Mod2|auto_generated|divider|divider|StageOut[205]~62\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod2|auto_generated|divider|divider|StageOut[205]~62_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[7]~12_combout\ & \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[7]~12_combout\,
	datad => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\,
	combout => \oTH|Mod2|auto_generated|divider|divider|StageOut[205]~62_combout\);

-- Location: LCCOMB_X11_Y20_N28
\oTH|Mod2|auto_generated|divider|divider|StageOut[204]~64\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod2|auto_generated|divider|divider|StageOut[204]~64_combout\ = (\oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[7]~12_combout\,
	combout => \oTH|Mod2|auto_generated|divider|divider|StageOut[204]~64_combout\);

-- Location: LCCOMB_X11_Y19_N2
\oTH|Mod2|auto_generated|divider|divider|StageOut[203]~67\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod2|auto_generated|divider|divider|StageOut[203]~67_combout\ = (\oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[5]~4_combout\ & !\oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[5]~4_combout\,
	datad => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\,
	combout => \oTH|Mod2|auto_generated|divider|divider|StageOut[203]~67_combout\);

-- Location: LCCOMB_X12_Y19_N4
\oTH|Mod2|auto_generated|divider|divider|StageOut[202]~69\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod2|auto_generated|divider|divider|StageOut[202]~69_combout\ = (\oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[4]~2_combout\ & !\oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[4]~2_combout\,
	datad => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\,
	combout => \oTH|Mod2|auto_generated|divider|divider|StageOut[202]~69_combout\);

-- Location: LCCOMB_X11_Y20_N10
\oTH|Mod2|auto_generated|divider|divider|StageOut[201]~71\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod2|auto_generated|divider|divider|StageOut[201]~71_combout\ = (!\oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\ & \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[3]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\,
	datac => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[3]~0_combout\,
	combout => \oTH|Mod2|auto_generated|divider|divider|StageOut[201]~71_combout\);

-- Location: LCCOMB_X21_Y20_N20
\oTH|Div1|auto_generated|divider|divider|StageOut[110]~231\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[110]~231_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[7]~12_combout\ & \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[5]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[7]~12_combout\,
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[5]~8_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[110]~231_combout\);

-- Location: LCCOMB_X22_Y20_N6
\oTH|Div1|auto_generated|divider|divider|StageOut[109]~232\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[109]~232_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[7]~12_combout\ & \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[4]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[7]~12_combout\,
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[4]~6_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[109]~232_combout\);

-- Location: LCCOMB_X22_Y20_N10
\oTH|Div1|auto_generated|divider|divider|StageOut[107]~234\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[107]~234_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[7]~12_combout\ & \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[2]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[7]~12_combout\,
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[2]~2_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[107]~234_combout\);

-- Location: LCCOMB_X21_Y20_N30
\oTH|Div1|auto_generated|divider|divider|StageOut[106]~235\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[106]~235_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[7]~12_combout\ & \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[1]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[7]~12_combout\,
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[1]~0_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[106]~235_combout\);

-- Location: LCCOMB_X33_Y18_N6
\oTH|Div0|auto_generated|divider|divider|StageOut[88]~264\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[88]~264_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_5~8_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_5~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_5~8_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_5~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[88]~264_combout\);

-- Location: LCCOMB_X33_Y18_N24
\oTH|Div0|auto_generated|divider|divider|StageOut[87]~265\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[87]~265_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_5~6_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_5~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_5~6_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_5~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[87]~265_combout\);

-- Location: LCCOMB_X33_Y18_N18
\oTH|Div0|auto_generated|divider|divider|StageOut[86]~266\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[86]~266_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_5~4_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_5~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_5~4_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_5~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[86]~266_combout\);

-- Location: LCCOMB_X33_Y19_N24
\oTH|Div0|auto_generated|divider|divider|StageOut[85]~267\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[85]~267_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_5~2_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_5~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div0|auto_generated|divider|divider|op_5~2_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_5~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[85]~267_combout\);

-- Location: LCCOMB_X33_Y18_N12
\oTH|Div0|auto_generated|divider|divider|StageOut[84]~269\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[84]~269_combout\ = (!\oTH|Div0|auto_generated|divider|divider|op_5~12_combout\ & \oTH|Div0|auto_generated|divider|divider|op_5~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div0|auto_generated|divider|divider|op_5~12_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_5~0_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[84]~269_combout\);

-- Location: LCCOMB_X21_Y20_N24
\oTH|Div1|auto_generated|divider|divider|StageOut[105]~237\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[105]~237_combout\ = (!\oTH|Div0|auto_generated|divider|divider|op_6~12_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div0|auto_generated|divider|divider|op_6~12_combout\,
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[7]~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[105]~237_combout\);

-- Location: LCCOMB_X11_Y20_N16
\oTH|Mod2|auto_generated|divider|divider|StageOut[200]~73\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod2|auto_generated|divider|divider|StageOut[200]~73_combout\ = (!\oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\ & \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[2]~18_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\,
	datad => \oTH|Mod2|auto_generated|divider|divider|add_sub_18_result_int[2]~18_combout\,
	combout => \oTH|Mod2|auto_generated|divider|divider|StageOut[200]~73_combout\);

-- Location: LCCOMB_X11_Y19_N8
\oTH|Mod2|auto_generated|divider|divider|StageOut[218]~74\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod2|auto_generated|divider|divider|StageOut[218]~74_combout\ = (\oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[9]~12_combout\ & !\oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[9]~12_combout\,
	datad => \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[11]~16_combout\,
	combout => \oTH|Mod2|auto_generated|divider|divider|StageOut[218]~74_combout\);

-- Location: LCCOMB_X10_Y19_N12
\oTH|Mod2|auto_generated|divider|divider|StageOut[215]~77\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod2|auto_generated|divider|divider|StageOut[215]~77_combout\ = (\oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[6]~6_combout\ & !\oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[6]~6_combout\,
	datac => \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[11]~16_combout\,
	combout => \oTH|Mod2|auto_generated|divider|divider|StageOut[215]~77_combout\);

-- Location: LCCOMB_X10_Y19_N8
\oTH|Div7|auto_generated|divider|divider|StageOut[54]~40\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div7|auto_generated|divider|divider|StageOut[54]~40_combout\ = (\oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\ & !\oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[6]~8_combout\,
	datad => \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \oTH|Div7|auto_generated|divider|divider|StageOut[54]~40_combout\);

-- Location: LCCOMB_X9_Y19_N6
\oTH|Div7|auto_generated|divider|divider|StageOut[50]~44\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div7|auto_generated|divider|divider|StageOut[50]~44_combout\ = (\oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\ & !\oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[2]~0_combout\,
	datad => \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	combout => \oTH|Div7|auto_generated|divider|divider|StageOut[50]~44_combout\);

-- Location: LCCOMB_X10_Y20_N18
\oTH|Mod2|auto_generated|divider|divider|StageOut[213]~79\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Mod2|auto_generated|divider|divider|StageOut[213]~79_combout\ = (!\oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[11]~16_combout\ & \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[4]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[11]~16_combout\,
	datad => \oTH|Mod2|auto_generated|divider|divider|add_sub_19_result_int[4]~2_combout\,
	combout => \oTH|Mod2|auto_generated|divider|divider|StageOut[213]~79_combout\);

-- Location: LCCOMB_X10_Y20_N0
\oTH|Div7|auto_generated|divider|divider|StageOut[49]~45\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div7|auto_generated|divider|divider|StageOut[49]~45_combout\ = (!\oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[1]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[1]~12_combout\,
	combout => \oTH|Div7|auto_generated|divider|divider|StageOut[49]~45_combout\);

-- Location: LCCOMB_X9_Y20_N6
\oTH|Div7|auto_generated|divider|divider|StageOut[59]~49\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div7|auto_generated|divider|divider|StageOut[59]~49_combout\ = (!\oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\ & \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	datac => \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[3]~2_combout\,
	combout => \oTH|Div7|auto_generated|divider|divider|StageOut[59]~49_combout\);

-- Location: LCCOMB_X11_Y20_N30
\oTH|Div7|auto_generated|divider|divider|StageOut[48]~51\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div7|auto_generated|divider|divider|StageOut[48]~51_combout\ = (!\oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[0]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datad => \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[0]~14_combout\,
	combout => \oTH|Div7|auto_generated|divider|divider|StageOut[48]~51_combout\);

-- Location: LCCOMB_X10_Y20_N24
\oTH|Div7|auto_generated|divider|divider|StageOut[57]~52\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div7|auto_generated|divider|divider|StageOut[57]~52_combout\ = (\oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[1]~14_combout\ & !\oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[1]~14_combout\,
	datac => \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \oTH|Div7|auto_generated|divider|divider|StageOut[57]~52_combout\);

-- Location: LCCOMB_X24_Y20_N0
\oTH|Div10|auto_generated|divider|divider|StageOut[207]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div10|auto_generated|divider|divider|StageOut[207]~0_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\ & \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div1|auto_generated|divider|divider|add_sub_9_result_int[7]~12_combout\,
	datac => \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\,
	combout => \oTH|Div10|auto_generated|divider|divider|StageOut[207]~0_combout\);

-- Location: LCCOMB_X23_Y20_N6
\oTH|Div10|auto_generated|divider|divider|StageOut[206]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div10|auto_generated|divider|divider|StageOut[206]~3_combout\ = (\oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[8]~10_combout\ & !\oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[8]~10_combout\,
	datad => \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\,
	combout => \oTH|Div10|auto_generated|divider|divider|StageOut[206]~3_combout\);

-- Location: LCCOMB_X22_Y20_N12
\oTH|Div10|auto_generated|divider|divider|StageOut[205]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div10|auto_generated|divider|divider|StageOut[205]~4_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[7]~12_combout\ & \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div1|auto_generated|divider|divider|add_sub_11_result_int[7]~12_combout\,
	datad => \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\,
	combout => \oTH|Div10|auto_generated|divider|divider|StageOut[205]~4_combout\);

-- Location: LCCOMB_X24_Y20_N4
\oTH|Div10|auto_generated|divider|divider|StageOut[204]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div10|auto_generated|divider|divider|StageOut[204]~6_combout\ = (\oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_12_result_int[7]~12_combout\,
	combout => \oTH|Div10|auto_generated|divider|divider|StageOut[204]~6_combout\);

-- Location: LCCOMB_X24_Y20_N6
\oTH|Div10|auto_generated|divider|divider|StageOut[203]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div10|auto_generated|divider|divider|StageOut[203]~8_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[7]~12_combout\ & \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div1|auto_generated|divider|divider|add_sub_13_result_int[7]~12_combout\,
	datac => \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\,
	combout => \oTH|Div10|auto_generated|divider|divider|StageOut[203]~8_combout\);

-- Location: LCCOMB_X23_Y20_N10
\oTH|Div10|auto_generated|divider|divider|StageOut[202]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div10|auto_generated|divider|divider|StageOut[202]~11_combout\ = (\oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[4]~2_combout\ & !\oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[4]~2_combout\,
	datad => \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\,
	combout => \oTH|Div10|auto_generated|divider|divider|StageOut[202]~11_combout\);

-- Location: LCCOMB_X22_Y20_N8
\oTH|Div10|auto_generated|divider|divider|StageOut[201]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div10|auto_generated|divider|divider|StageOut[201]~12_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[7]~12_combout\ & \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|add_sub_15_result_int[7]~12_combout\,
	datad => \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\,
	combout => \oTH|Div10|auto_generated|divider|divider|StageOut[201]~12_combout\);

-- Location: LCCOMB_X22_Y20_N0
\oTH|Div10|auto_generated|divider|divider|StageOut[200]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div10|auto_generated|divider|divider|StageOut[200]~15_combout\ = (\oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[2]~18_combout\ & !\oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[2]~18_combout\,
	datad => \oTH|Div10|auto_generated|divider|divider|add_sub_18_result_int[11]~16_combout\,
	combout => \oTH|Div10|auto_generated|divider|divider|StageOut[200]~15_combout\);

-- Location: LCCOMB_X11_Y20_N12
\oTH|LessThan0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|LessThan0~2_combout\ = (\oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\ & \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div7|auto_generated|divider|divider|add_sub_6_result_int[7]~10_combout\,
	datac => \oTH|Div7|auto_generated|divider|divider|add_sub_7_result_int[8]~12_combout\,
	combout => \oTH|LessThan0~2_combout\);

-- Location: LCCOMB_X21_Y19_N26
\oTH|Div1|auto_generated|divider|divider|StageOut[116]~239\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[116]~239_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[7]~12_combout\ & \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[4]~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[7]~12_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[4]~6_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[116]~239_combout\);

-- Location: LCCOMB_X21_Y20_N28
\oTH|Div1|auto_generated|divider|divider|StageOut[114]~241\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[114]~241_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[2]~2_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[2]~2_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[7]~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[114]~241_combout\);

-- Location: LCCOMB_X33_Y19_N18
\oTH|Div0|auto_generated|divider|divider|StageOut[94]~270\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[94]~270_combout\ = (!\oTH|Div0|auto_generated|divider|divider|op_6~12_combout\ & \oTH|Div0|auto_generated|divider|divider|op_6~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div0|auto_generated|divider|divider|op_6~12_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_6~8_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[94]~270_combout\);

-- Location: LCCOMB_X33_Y19_N0
\oTH|Div0|auto_generated|divider|divider|StageOut[93]~271\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[93]~271_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_6~6_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_6~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \oTH|Div0|auto_generated|divider|divider|op_6~6_combout\,
	datac => \oTH|Div0|auto_generated|divider|divider|op_6~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[93]~271_combout\);

-- Location: LCCOMB_X33_Y20_N16
\oTH|Div0|auto_generated|divider|divider|StageOut[90]~275\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[90]~275_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_6~0_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_6~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_6~0_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_6~12_combout\,
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[90]~275_combout\);

-- Location: LCCOMB_X21_Y19_N24
\oTH|Div1|auto_generated|divider|divider|StageOut[112]~243\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[112]~243_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[7]~12_combout\ & !\oTH|Div0|auto_generated|divider|divider|op_7~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_16_result_int[7]~12_combout\,
	datad => \oTH|Div0|auto_generated|divider|divider|op_7~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[112]~243_combout\);

-- Location: LCCOMB_X20_Y19_N26
\oTH|Div1|auto_generated|divider|divider|StageOut[124]~245\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[124]~245_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[5]~8_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[5]~8_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[7]~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[124]~245_combout\);

-- Location: LCCOMB_X22_Y19_N20
\oTH|Div1|auto_generated|divider|divider|StageOut[123]~246\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[123]~246_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[4]~6_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[4]~6_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[7]~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[123]~246_combout\);

-- Location: LCCOMB_X22_Y19_N16
\oTH|Div1|auto_generated|divider|divider|StageOut[122]~247\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[122]~247_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[3]~4_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[3]~4_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[7]~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[122]~247_combout\);

-- Location: LCCOMB_X22_Y19_N24
\oTH|Div1|auto_generated|divider|divider|StageOut[120]~249\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[120]~249_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[1]~0_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[1]~0_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[7]~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[120]~249_combout\);

-- Location: LCCOMB_X34_Y19_N26
\oTH|Div0|auto_generated|divider|divider|StageOut[96]~280\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div0|auto_generated|divider|divider|StageOut[96]~280_combout\ = (\oTH|Div0|auto_generated|divider|divider|op_7~12_combout\ & \cET|tacts\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \oTH|Div0|auto_generated|divider|divider|op_7~12_combout\,
	datad => \cET|tacts\(4),
	combout => \oTH|Div0|auto_generated|divider|divider|StageOut[96]~280_combout\);

-- Location: LCCOMB_X20_Y19_N22
\oTH|Div1|auto_generated|divider|divider|StageOut[119]~251\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[119]~251_combout\ = (!\oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[7]~12_combout\ & \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[0]~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[7]~12_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_17_result_int[0]~14_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[119]~251_combout\);

-- Location: LCCOMB_X23_Y19_N20
\oTH|Div1|auto_generated|divider|divider|StageOut[131]~252\ : cycloneii_lcell_comb
-- Equation(s):
-- \oTH|Div1|auto_generated|divider|divider|StageOut[131]~252_combout\ = (\oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[5]~8_combout\ & !\oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[7]~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[5]~8_combout\,
	datad => \oTH|Div1|auto_generated|divider|divider|add_sub_18_result_int[7]~12_combout\,
	combout => \oTH|Div1|auto_generated|divider|divider|StageOut[131]~252_combout\);

-- Location: LCCOMB_X23_Y19_N16
\oTH|Div1|auto_generated|divider|divider|Stag