module main(input 		  		CLOCK_50,
				input 	   		echo,
				input 				echo_2,
				input 	  [9:0]  SW,
				input      [3:0]  KEY,
				output     			trigger,
				output     			trigger_2,
				output     [9:0]  LEDR,
				output     [6:0]  HEX0, HEX1, HEX2, HEX3);
	
	wire reset = ~KEY[0];
	// connecting distance counter 
	// to GPIO_0 
	// trigger to pin1 on gpio   GPIO_0[0] 
	// echo    to pin21 on gpio  GPIO_0[18]
	
	// connecting distance counter 
	// to GPIO_1 
	// trigger to pin1 on gpio   GPIO_1[0] 
	// echo    to pin21 on gpio  GPIO_1[18]
	
	wire oneSecGone;
	one_sec_counter one_sec(.clk(CLOCK_50),
									.rst(reset),
									.start(1'b1),
									.one_sec(oneSecGone));
									
	createTriggerSignal cTS(.clk(CLOCK_50),
									.rst(reset),
									.one_sec(oneSecGone),
									.trigger(trigger));
									
	createTriggerSignal cTS2(.clk(CLOCK_50),
								 	 .rst(reset),
							 		 .one_sec(oneSecGone),
									 .trigger(trigger_2));
	
	wire[20:0] tacts;
	countEchoTime cET(.clk(CLOCK_50),
							.rst(reset),
							.echo(echo),
							.tacts(tacts));	

	wire[20:0] tacts_2;
	countEchoTime cET_2(.clk(CLOCK_50),
							  .rst(reset),
							  .echo(echo_