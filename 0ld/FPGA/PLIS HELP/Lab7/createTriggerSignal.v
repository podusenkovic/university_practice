module createTriggerSignal(input one_sec,
									input clk,
									input rst,
									output trigger);
		
	parameter MAX_TICKS = 10'd4999;
	reg[9:0] cntr;
	reg check;
	
	assign trigger = (check) ? 1 : 0;
	
	always@(posedge clk)
		if (rst) cntr <= 10'b0;
		else if (one_sec)
					check <= 1'b1;
			  else if (cntr == MAX_TICKS)
					 begin 
						 check <= 1'b0;
						 cntr <= 1'b0;
					 end 
			  else cntr <= cntr + 1;
endmodule 