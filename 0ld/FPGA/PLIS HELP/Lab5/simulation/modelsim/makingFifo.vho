-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

-- DATE "04/10/2018 10:58:51"

-- 
-- Device: Altera EP2C20F484C7 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEII;
LIBRARY IEEE;
USE CYCLONEII.CYCLONEII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	main IS
    PORT (
	CLOCK_50 : IN std_logic;
	SW : IN std_logic_vector(9 DOWNTO 0);
	KEY : IN std_logic_vector(3 DOWNTO 0);
	LEDG : OUT std_logic_vector(1 DOWNTO 0);
	HEX0 : OUT std_logic_vector(6 DOWNTO 0);
	HEX1 : OUT std_logic_vector(6 DOWNTO 0);
	HEX2 : OUT std_logic_vector(6 DOWNTO 0);
	HEX3 : OUT std_logic_vector(6 DOWNTO 0)
	);
END main;

-- Design Ports Information
-- KEY[1]	=>  Location: PIN_R21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- LEDG[0]	=>  Location: PIN_U22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDG[1]	=>  Location: PIN_U21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[0]	=>  Location: PIN_J2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[1]	=>  Location: PIN_J1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[2]	=>  Location: PIN_H2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[3]	=>  Location: PIN_H1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[4]	=>  Location: PIN_F2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[5]	=>  Location: PIN_F1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[6]	=>  Location: PIN_E2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[0]	=>  Location: PIN_E1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[1]	=>  Location: PIN_H6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[2]	=>  Location: PIN_H5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[3]	=>  Location: PIN_H4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[4]	=>  Location: PIN_G3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[5]	=>  Location: PIN_D2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[6]	=>  Location: PIN_D1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[0]	=>  Location: PIN_G5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[1]	=>  Location: PIN_G6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[2]	=>  Location: PIN_C2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[3]	=>  Location: PIN_C1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[4]	=>  Location: PIN_E3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[5]	=>  Location: PIN_E4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[6]	=>  Location: PIN_D3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[0]	=>  Location: PIN_F4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[1]	=>  Location: PIN_D5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[2]	=>  Location: PIN_D6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[3]	=>  Location: PIN_J4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[4]	=>  Location: PIN_L8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[5]	=>  Location: PIN_F3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[6]	=>  Location: PIN_D4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- KEY[0]	=>  Location: PIN_R22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- CLOCK_50	=>  Location: PIN_L1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_L22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_L21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_M22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_V12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_W12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[5]	=>  Location: PIN_U12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_U11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_M2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[8]	=>  Location: PIN_M1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[9]	=>  Location: PIN_L2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[2]	=>  Location: PIN_T22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[3]	=>  Location: PIN_T21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF main IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLOCK_50 : std_logic;
SIGNAL ww_SW : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_KEY : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_LEDG : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_HEX0 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX2 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX3 : std_logic_vector(6 DOWNTO 0);
SIGNAL \myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTADATAIN_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTAADDR_bus\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBADDR_bus\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \CLOCK_50~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \Add0~4_combout\ : std_logic;
SIGNAL \myRam|ram~2_combout\ : std_logic;
SIGNAL \key2_d2~0_combout\ : std_logic;
SIGNAL \key2_d2~regout\ : std_logic;
SIGNAL \key2_d0~feeder_combout\ : std_logic;
SIGNAL \key2_d0~regout\ : std_logic;
SIGNAL \key2_d1~0_combout\ : std_logic;
SIGNAL \key2_d1~regout\ : std_logic;
SIGNAL \key3_d0~regout\ : std_logic;
SIGNAL \key3_d1~0_combout\ : std_logic;
SIGNAL \key3_d1~regout\ : std_logic;
SIGNAL \key3_d2~0_combout\ : std_logic;
SIGNAL \key3_d2~regout\ : std_logic;
SIGNAL \Add2~1\ : std_logic;
SIGNAL \Add2~3_combout\ : std_logic;
SIGNAL \Add2~5_combout\ : std_logic;
SIGNAL \Add2~4\ : std_logic;
SIGNAL \Add2~6_combout\ : std_logic;
SIGNAL \Add2~8_combout\ : std_logic;
SIGNAL \Add2~7\ : std_logic;
SIGNAL \Add2~9_combout\ : std_logic;
SIGNAL \Add2~11_combout\ : std_logic;
SIGNAL \Add2~10\ : std_logic;
SIGNAL \Add2~12_combout\ : std_logic;
SIGNAL \Add2~14_combout\ : std_logic;
SIGNAL \Add0~1\ : std_logic;
SIGNAL \Add0~3\ : std_logic;
SIGNAL \Add0~5\ : std_logic;
SIGNAL \Add0~7\ : std_logic;
SIGNAL \Add0~8_combout\ : std_logic;
SIGNAL \addr_wr[0]~7_combout\ : std_logic;
SIGNAL \addr_wr[0]~8\ : std_logic;
SIGNAL \addr_wr[1]~10\ : std_logic;
SIGNAL \addr_wr[2]~11_combout\ : std_logic;
SIGNAL \addr_wr[2]~12\ : std_logic;
SIGNAL \addr_wr[3]~13_combout\ : std_logic;
SIGNAL \Add0~6_combout\ : std_logic;
SIGNAL \Equal0~1_combout\ : std_logic;
SIGNAL \Equal0~2_combout\ : std_logic;
SIGNAL \addr_wr[4]~17_combout\ : std_logic;
SIGNAL \Add0~2_combout\ : std_logic;
SIGNAL \Add0~0_combout\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \we~0_combout\ : std_logic;
SIGNAL \we~1_combout\ : std_logic;
SIGNAL \addr_rd~0_combout\ : std_logic;
SIGNAL \Add2~0_combout\ : std_logic;
SIGNAL \Add2~2_combout\ : std_logic;
SIGNAL \addr_rd[0]~feeder_combout\ : std_logic;
SIGNAL \Equal1~0_combout\ : std_logic;
SIGNAL \addr_wr[3]~14\ : std_logic;
SIGNAL \addr_wr[4]~15_combout\ : std_logic;
SIGNAL \Equal1~1_combout\ : std_logic;
SIGNAL \Equal1~2_combout\ : std_logic;
SIGNAL \myRam|ram~1_combout\ : std_logic;
SIGNAL \addr_wr[1]~9_combout\ : std_logic;
SIGNAL \myRam|ram~0_combout\ : std_logic;
SIGNAL \myRam|ram~3_combout\ : std_logic;
SIGNAL \CLOCK_50~combout\ : std_logic;
SIGNAL \CLOCK_50~clkctrl_outclk\ : std_logic;
SIGNAL \myRam|ram_rtl_0|auto_generated|ram_block1a0~portbdataout\ : std_logic;
SIGNAL \myRam|ram~4_combout\ : std_logic;
SIGNAL \myRam|ram_rtl_0|auto_generated|ram_block1a1\ : std_logic;
SIGNAL \myRam|ram~5_combout\ : std_logic;
SIGNAL \myRam|ram_rtl_0|auto_generated|ram_block1a2\ : std_logic;
SIGNAL \myRam|ram~6_combout\ : std_logic;
SIGNAL \myRam|ram_rtl_0|auto_generated|ram_block1a3\ : std_logic;
SIGNAL \myRam|ram~7_combout\ : std_logic;
SIGNAL \dec0|WideOr6~0_combout\ : std_logic;
SIGNAL \dec0|WideOr5~0_combout\ : std_logic;
SIGNAL \dec0|WideOr4~0_combout\ : std_logic;
SIGNAL \dec0|WideOr3~0_combout\ : std_logic;
SIGNAL \dec0|WideOr2~0_combout\ : std_logic;
SIGNAL \dec0|WideOr1~0_combout\ : std_logic;
SIGNAL \dec0|WideOr0~0_combout\ : std_logic;
SIGNAL \myRam|ram_rtl_0|auto_generated|ram_block1a7\ : std_logic;
SIGNAL \myRam|ram~11_combout\ : std_logic;
SIGNAL \myRam|ram_rtl_0|auto_generated|ram_block1a6\ : std_logic;
SIGNAL \myRam|ram~10_combout\ : std_logic;
SIGNAL \myRam|ram_rtl_0|auto_generated|ram_block1a4\ : std_logic;
SIGNAL \myRam|ram~8_combout\ : std_logic;
SIGNAL \myRam|ram_rtl_0|auto_generated|ram_block1a5\ : std_logic;
SIGNAL \myRam|ram~9_combout\ : std_logic;
SIGNAL \dec1|WideOr6~0_combout\ : std_logic;
SIGNAL \dec1|WideOr5~0_combout\ : std_logic;
SIGNAL \dec1|WideOr4~0_combout\ : std_logic;
SIGNAL \dec1|WideOr3~0_combout\ : std_logic;
SIGNAL \dec1|WideOr2~0_combout\ : std_logic;
SIGNAL \dec1|WideOr1~0_combout\ : std_logic;
SIGNAL \dec1|WideOr0~0_combout\ : std_logic;
SIGNAL \myRam|ram_rtl_0|auto_generated|ram_block1a8\ : std_logic;
SIGNAL \myRam|ram~13_combout\ : std_logic;
SIGNAL \dec2|Decoder0~12_combout\ : std_logic;
SIGNAL \myRam|ram_rtl_0|auto_generated|ram_block1a9\ : std_logic;
SIGNAL \myRam|ram~12_combout\ : std_logic;
SIGNAL \dec2|Decoder0~13_combout\ : std_logic;
SIGNAL \dec2|Decoder0~14_combout\ : std_logic;
SIGNAL \SW~combout\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \KEY~combout\ : std_logic_vector(3 DOWNTO 0);
SIGNAL addr_wr : std_logic_vector(4 DOWNTO 0);
SIGNAL \myRam|ram_rtl_0_bypass\ : std_logic_vector(0 TO 20);
SIGNAL addr_rd : std_logic_vector(4 DOWNTO 0);
SIGNAL \ALT_INV_KEY~combout\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \myRam|ALT_INV_ram~13_combout\ : std_logic;
SIGNAL \dec1|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \dec0|ALT_INV_WideOr0~0_combout\ : std_logic;

BEGIN

ww_CLOCK_50 <= CLOCK_50;
ww_SW <= SW;
ww_KEY <= KEY;
LEDG <= ww_LEDG;
HEX0 <= ww_HEX0;
HEX1 <= ww_HEX1;
HEX2 <= ww_HEX2;
HEX3 <= ww_HEX3;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTADATAIN_bus\ <= (\SW~combout\(9) & \SW~combout\(8) & \SW~combout\(7) & \SW~combout\(6) & \SW~combout\(5) & \SW~combout\(4) & \SW~combout\(3) & \SW~combout\(2) & \SW~combout\(1) & 
\SW~combout\(0));

\myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTAADDR_bus\ <= (addr_wr(4) & addr_wr(3) & addr_wr(2) & addr_wr(1) & addr_wr(0));

\myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBADDR_bus\ <= (\Add2~14_combout\ & \Add2~11_combout\ & \Add2~8_combout\ & \Add2~5_combout\ & \Add2~2_combout\);

\myRam|ram_rtl_0|auto_generated|ram_block1a0~portbdataout\ <= \myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(0);
\myRam|ram_rtl_0|auto_generated|ram_block1a1\ <= \myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(1);
\myRam|ram_rtl_0|auto_generated|ram_block1a2\ <= \myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(2);
\myRam|ram_rtl_0|auto_generated|ram_block1a3\ <= \myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(3);
\myRam|ram_rtl_0|auto_generated|ram_block1a4\ <= \myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(4);
\myRam|ram_rtl_0|auto_generated|ram_block1a5\ <= \myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(5);
\myRam|ram_rtl_0|auto_generated|ram_block1a6\ <= \myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(6);
\myRam|ram_rtl_0|auto_generated|ram_block1a7\ <= \myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(7);
\myRam|ram_rtl_0|auto_generated|ram_block1a8\ <= \myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(8);
\myRam|ram_rtl_0|auto_generated|ram_block1a9\ <= \myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(9);

\CLOCK_50~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \CLOCK_50~combout\);
\ALT_INV_KEY~combout\(0) <= NOT \KEY~combout\(0);
\myRam|ALT_INV_ram~13_combout\ <= NOT \myRam|ram~13_combout\;
\dec1|ALT_INV_WideOr0~0_combout\ <= NOT \dec1|WideOr0~0_combout\;
\dec0|ALT_INV_WideOr0~0_combout\ <= NOT \dec0|WideOr0~0_combout\;

-- Location: LCCOMB_X15_Y18_N10
\Add0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~4_combout\ = (addr_rd(2) & ((GND) # (!\Add0~3\))) # (!addr_rd(2) & (\Add0~3\ $ (GND)))
-- \Add0~5\ = CARRY((addr_rd(2)) # (!\Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => addr_rd(2),
	datad => VCC,
	cin => \Add0~3\,
	combout => \Add0~4_combout\,
	cout => \Add0~5\);

-- Location: LCFF_X16_Y18_N13
\myRam|ram_rtl_0_bypass[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \we~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myRam|ram_rtl_0_bypass\(0));

-- Location: LCFF_X14_Y18_N21
\myRam|ram_rtl_0_bypass[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => addr_wr(4),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myRam|ram_rtl_0_bypass\(9));

-- Location: LCCOMB_X14_Y18_N20
\myRam|ram~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \myRam|ram~2_combout\ = (\myRam|ram_rtl_0_bypass\(0) & (addr_rd(4) $ (!\myRam|ram_rtl_0_bypass\(9))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => addr_rd(4),
	datac => \myRam|ram_rtl_0_bypass\(9),
	datad => \myRam|ram_rtl_0_bypass\(0),
	combout => \myRam|ram~2_combout\);

-- Location: PIN_R22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(0),
	combout => \KEY~combout\(0));

-- Location: PIN_T22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(2),
	combout => \KEY~combout\(2));

-- Location: LCCOMB_X18_Y18_N16
\key2_d2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \key2_d2~0_combout\ = (\KEY~combout\(0) & (\key2_d1~regout\)) # (!\KEY~combout\(0) & ((\KEY~combout\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key2_d1~regout\,
	datac => \KEY~combout\(0),
	datad => \KEY~combout\(2),
	combout => \key2_d2~0_combout\);

-- Location: LCFF_X18_Y18_N17
key2_d2 : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \key2_d2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \key2_d2~regout\);

-- Location: LCCOMB_X18_Y18_N22
\key2_d0~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \key2_d0~feeder_combout\ = \KEY~combout\(2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \KEY~combout\(2),
	combout => \key2_d0~feeder_combout\);

-- Location: LCFF_X18_Y18_N23
key2_d0 : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \key2_d0~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \key2_d0~regout\);

-- Location: LCCOMB_X18_Y18_N6
\key2_d1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \key2_d1~0_combout\ = (\KEY~combout\(0) & ((\key2_d0~regout\))) # (!\KEY~combout\(0) & (\KEY~combout\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \KEY~combout\(2),
	datab => \key2_d0~regout\,
	datac => \KEY~combout\(0),
	combout => \key2_d1~0_combout\);

-- Location: LCFF_X18_Y18_N7
key2_d1 : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \key2_d1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \key2_d1~regout\);

-- Location: PIN_T21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(3),
	combout => \KEY~combout\(3));

-- Location: LCFF_X16_Y18_N3
key3_d0 : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \KEY~combout\(3),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \key3_d0~regout\);

-- Location: LCCOMB_X16_Y18_N26
\key3_d1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \key3_d1~0_combout\ = (\KEY~combout\(0) & (\key3_d0~regout\)) # (!\KEY~combout\(0) & ((\KEY~combout\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \key3_d0~regout\,
	datac => \KEY~combout\(3),
	datad => \KEY~combout\(0),
	combout => \key3_d1~0_combout\);

-- Location: LCFF_X16_Y18_N27
key3_d1 : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \key3_d1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \key3_d1~regout\);

-- Location: LCCOMB_X16_Y18_N24
\key3_d2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \key3_d2~0_combout\ = (\KEY~combout\(0) & (\key3_d1~regout\)) # (!\KEY~combout\(0) & ((\KEY~combout\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \key3_d1~regout\,
	datac => \KEY~combout\(3),
	datad => \KEY~combout\(0),
	combout => \key3_d2~0_combout\);

-- Location: LCFF_X16_Y18_N25
key3_d2 : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \key3_d2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \key3_d2~regout\);

-- Location: LCCOMB_X16_Y18_N14
\Add2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add2~0_combout\ = (addr_rd(0) & (\addr_rd~0_combout\ $ (GND))) # (!addr_rd(0) & (!\addr_rd~0_combout\ & VCC))
-- \Add2~1\ = CARRY((addr_rd(0) & !\addr_rd~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100100100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => addr_rd(0),
	datab => \addr_rd~0_combout\,
	datad => VCC,
	combout => \Add2~0_combout\,
	cout => \Add2~1\);

-- Location: LCCOMB_X16_Y18_N16
\Add2~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add2~3_combout\ = (addr_rd(1) & (!\Add2~1\)) # (!addr_rd(1) & ((\Add2~1\) # (GND)))
-- \Add2~4\ = CARRY((!\Add2~1\) # (!addr_rd(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => addr_rd(1),
	datad => VCC,
	cin => \Add2~1\,
	combout => \Add2~3_combout\,
	cout => \Add2~4\);

-- Location: LCCOMB_X16_Y18_N2
\Add2~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add2~5_combout\ = (\KEY~combout\(0) & \Add2~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \KEY~combout\(0),
	datad => \Add2~3_combout\,
	combout => \Add2~5_combout\);

-- Location: LCFF_X16_Y18_N9
\addr_rd[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \Add2~5_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => addr_rd(1));

-- Location: LCCOMB_X16_Y18_N18
\Add2~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add2~6_combout\ = (addr_rd(2) & (\Add2~4\ $ (GND))) # (!addr_rd(2) & (!\Add2~4\ & VCC))
-- \Add2~7\ = CARRY((addr_rd(2) & !\Add2~4\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => addr_rd(2),
	datad => VCC,
	cin => \Add2~4\,
	combout => \Add2~6_combout\,
	cout => \Add2~7\);

-- Location: LCCOMB_X16_Y18_N4
\Add2~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add2~8_combout\ = (\KEY~combout\(0) & \Add2~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \KEY~combout\(0),
	datad => \Add2~6_combout\,
	combout => \Add2~8_combout\);

-- Location: LCFF_X16_Y18_N11
\addr_rd[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \Add2~8_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => addr_rd(2));

-- Location: LCCOMB_X16_Y18_N20
\Add2~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add2~9_combout\ = (addr_rd(3) & (!\Add2~7\)) # (!addr_rd(3) & ((\Add2~7\) # (GND)))
-- \Add2~10\ = CARRY((!\Add2~7\) # (!addr_rd(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => addr_rd(3),
	datad => VCC,
	cin => \Add2~7\,
	combout => \Add2~9_combout\,
	cout => \Add2~10\);

-- Location: LCCOMB_X16_Y18_N0
\Add2~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add2~11_combout\ = (\Add2~9_combout\ & \KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add2~9_combout\,
	datad => \KEY~combout\(0),
	combout => \Add2~11_combout\);

-- Location: LCFF_X16_Y18_N1
\addr_rd[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \Add2~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => addr_rd(3));

-- Location: LCCOMB_X16_Y18_N22
\Add2~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add2~12_combout\ = addr_rd(4) $ (!\Add2~10\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001111000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => addr_rd(4),
	cin => \Add2~10\,
	combout => \Add2~12_combout\);

-- Location: LCCOMB_X16_Y18_N30
\Add2~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add2~14_combout\ = (\KEY~combout\(0) & \Add2~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \KEY~combout\(0),
	datad => \Add2~12_combout\,
	combout => \Add2~14_combout\);

-- Location: LCFF_X16_Y18_N7
\addr_rd[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \Add2~14_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => addr_rd(4));

-- Location: LCCOMB_X15_Y18_N6
\Add0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~0_combout\ = addr_rd(0) $ (VCC)
-- \Add0~1\ = CARRY(addr_rd(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => addr_rd(0),
	datad => VCC,
	combout => \Add0~0_combout\,
	cout => \Add0~1\);

-- Location: LCCOMB_X15_Y18_N8
\Add0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~2_combout\ = (addr_rd(1) & (\Add0~1\ & VCC)) # (!addr_rd(1) & (!\Add0~1\))
-- \Add0~3\ = CARRY((!addr_rd(1) & !\Add0~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => addr_rd(1),
	datad => VCC,
	cin => \Add0~1\,
	combout => \Add0~2_combout\,
	cout => \Add0~3\);

-- Location: LCCOMB_X15_Y18_N12
\Add0~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~6_combout\ = (addr_rd(3) & (\Add0~5\ & VCC)) # (!addr_rd(3) & (!\Add0~5\))
-- \Add0~7\ = CARRY((!addr_rd(3) & !\Add0~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => addr_rd(3),
	datad => VCC,
	cin => \Add0~5\,
	combout => \Add0~6_combout\,
	cout => \Add0~7\);

-- Location: LCCOMB_X15_Y18_N14
\Add0~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~8_combout\ = \Add0~7\ $ (addr_rd(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => addr_rd(4),
	cin => \Add0~7\,
	combout => \Add0~8_combout\);

-- Location: LCCOMB_X15_Y18_N18
\addr_wr[0]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \addr_wr[0]~7_combout\ = addr_wr(0) $ (VCC)
-- \addr_wr[0]~8\ = CARRY(addr_wr(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => addr_wr(0),
	datad => VCC,
	combout => \addr_wr[0]~7_combout\,
	cout => \addr_wr[0]~8\);

-- Location: LCCOMB_X15_Y18_N20
\addr_wr[1]~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \addr_wr[1]~9_combout\ = (addr_wr(1) & (!\addr_wr[0]~8\)) # (!addr_wr(1) & ((\addr_wr[0]~8\) # (GND)))
-- \addr_wr[1]~10\ = CARRY((!\addr_wr[0]~8\) # (!addr_wr(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => addr_wr(1),
	datad => VCC,
	cin => \addr_wr[0]~8\,
	combout => \addr_wr[1]~9_combout\,
	cout => \addr_wr[1]~10\);

-- Location: LCCOMB_X15_Y18_N22
\addr_wr[2]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \addr_wr[2]~11_combout\ = (addr_wr(2) & (\addr_wr[1]~10\ $ (GND))) # (!addr_wr(2) & (!\addr_wr[1]~10\ & VCC))
-- \addr_wr[2]~12\ = CARRY((addr_wr(2) & !\addr_wr[1]~10\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => addr_wr(2),
	datad => VCC,
	cin => \addr_wr[1]~10\,
	combout => \addr_wr[2]~11_combout\,
	cout => \addr_wr[2]~12\);

-- Location: LCFF_X15_Y18_N23
\addr_wr[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \addr_wr[2]~11_combout\,
	sclr => \ALT_INV_KEY~combout\(0),
	ena => \addr_wr[4]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => addr_wr(2));

-- Location: LCCOMB_X15_Y18_N24
\addr_wr[3]~13\ : cycloneii_lcell_comb
-- Equation(s):
-- \addr_wr[3]~13_combout\ = (addr_wr(3) & (!\addr_wr[2]~12\)) # (!addr_wr(3) & ((\addr_wr[2]~12\) # (GND)))
-- \addr_wr[3]~14\ = CARRY((!\addr_wr[2]~12\) # (!addr_wr(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => addr_wr(3),
	datad => VCC,
	cin => \addr_wr[2]~12\,
	combout => \addr_wr[3]~13_combout\,
	cout => \addr_wr[3]~14\);

-- Location: LCFF_X15_Y18_N25
\addr_wr[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \addr_wr[3]~13_combout\,
	sclr => \ALT_INV_KEY~combout\(0),
	ena => \addr_wr[4]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => addr_wr(3));

-- Location: LCCOMB_X15_Y18_N28
\Equal0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal0~1_combout\ = (\Add0~4_combout\ & (addr_wr(2) & (addr_wr(3) $ (!\Add0~6_combout\)))) # (!\Add0~4_combout\ & (!addr_wr(2) & (addr_wr(3) $ (!\Add0~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Add0~4_combout\,
	datab => addr_wr(2),
	datac => addr_wr(3),
	datad => \Add0~6_combout\,
	combout => \Equal0~1_combout\);

-- Location: LCCOMB_X14_Y18_N16
\Equal0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal0~2_combout\ = (\Equal0~1_combout\ & (\Equal0~0_combout\ & (addr_wr(4) $ (!\Add0~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => addr_wr(4),
	datab => \Equal0~1_combout\,
	datac => \Add0~8_combout\,
	datad => \Equal0~0_combout\,
	combout => \Equal0~2_combout\);

-- Location: LCCOMB_X15_Y18_N16
\addr_wr[4]~17\ : cycloneii_lcell_comb
-- Equation(s):
-- \addr_wr[4]~17_combout\ = ((\key3_d2~regout\ & (!\Equal0~2_combout\ & !\key3_d1~regout\))) # (!\KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key3_d2~regout\,
	datab => \Equal0~2_combout\,
	datac => \key3_d1~regout\,
	datad => \KEY~combout\(0),
	combout => \addr_wr[4]~17_combout\);

-- Location: LCFF_X15_Y18_N19
\addr_wr[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \addr_wr[0]~7_combout\,
	sclr => \ALT_INV_KEY~combout\(0),
	ena => \addr_wr[4]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => addr_wr(0));

-- Location: LCCOMB_X15_Y18_N4
\Equal0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal0~0_combout\ = (addr_wr(1) & (\Add0~2_combout\ & (addr_wr(0) $ (!\Add0~0_combout\)))) # (!addr_wr(1) & (!\Add0~2_combout\ & (addr_wr(0) $ (!\Add0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => addr_wr(1),
	datab => addr_wr(0),
	datac => \Add0~2_combout\,
	datad => \Add0~0_combout\,
	combout => \Equal0~0_combout\);

-- Location: LCCOMB_X15_Y18_N2
\we~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \we~0_combout\ = (\Equal0~0_combout\ & (\Equal0~1_combout\ & (addr_wr(4) $ (!\Add0~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => addr_wr(4),
	datab => \Add0~8_combout\,
	datac => \Equal0~0_combout\,
	datad => \Equal0~1_combout\,
	combout => \we~0_combout\);

-- Location: LCCOMB_X16_Y18_N12
\we~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \we~1_combout\ = (!\key3_d1~regout\ & (\key3_d2~regout\ & !\we~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \key3_d1~regout\,
	datac => \key3_d2~regout\,
	datad => \we~0_combout\,
	combout => \we~1_combout\);

-- Location: LCCOMB_X16_Y18_N28
\addr_rd~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \addr_rd~0_combout\ = (\Equal1~2_combout\) # (((\key2_d1~regout\) # (\we~1_combout\)) # (!\key2_d2~regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal1~2_combout\,
	datab => \key2_d2~regout\,
	datac => \key2_d1~regout\,
	datad => \we~1_combout\,
	combout => \addr_rd~0_combout\);

-- Location: LCCOMB_X16_Y18_N8
\Add2~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add2~2_combout\ = (\Add2~0_combout\ & \KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Add2~0_combout\,
	datad => \KEY~combout\(0),
	combout => \Add2~2_combout\);

-- Location: LCCOMB_X15_Y18_N0
\addr_rd[0]~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \addr_rd[0]~feeder_combout\ = \Add2~2_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \Add2~2_combout\,
	combout => \addr_rd[0]~feeder_combout\);

-- Location: LCFF_X15_Y18_N1
\addr_rd[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \addr_rd[0]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => addr_rd(0));

-- Location: LCCOMB_X15_Y18_N30
\Equal1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal1~0_combout\ = (addr_wr(1) & (addr_rd(1) & (addr_rd(0) $ (!addr_wr(0))))) # (!addr_wr(1) & (!addr_rd(1) & (addr_rd(0) $ (!addr_wr(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => addr_wr(1),
	datab => addr_rd(0),
	datac => addr_rd(1),
	datad => addr_wr(0),
	combout => \Equal1~0_combout\);

-- Location: LCCOMB_X15_Y18_N26
\addr_wr[4]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \addr_wr[4]~15_combout\ = \addr_wr[3]~14\ $ (!addr_wr(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => addr_wr(4),
	cin => \addr_wr[3]~14\,
	combout => \addr_wr[4]~15_combout\);

-- Location: LCFF_X15_Y18_N27
\addr_wr[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \addr_wr[4]~15_combout\,
	sclr => \ALT_INV_KEY~combout\(0),
	ena => \addr_wr[4]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => addr_wr(4));

-- Location: LCCOMB_X16_Y18_N10
\Equal1~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal1~1_combout\ = (addr_wr(3) & (addr_rd(3) & (addr_rd(2) $ (!addr_wr(2))))) # (!addr_wr(3) & (!addr_rd(3) & (addr_rd(2) $ (!addr_wr(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => addr_wr(3),
	datab => addr_rd(3),
	datac => addr_rd(2),
	datad => addr_wr(2),
	combout => \Equal1~1_combout\);

-- Location: LCCOMB_X16_Y18_N6
\Equal1~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal1~2_combout\ = (\Equal1~0_combout\ & (\Equal1~1_combout\ & (addr_wr(4) $ (!addr_rd(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal1~0_combout\,
	datab => addr_wr(4),
	datac => addr_rd(4),
	datad => \Equal1~1_combout\,
	combout => \Equal1~2_combout\);

-- Location: LCFF_X14_Y18_N19
\myRam|ram_rtl_0_bypass[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => addr_wr(2),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myRam|ram_rtl_0_bypass\(5));

-- Location: LCFF_X16_Y18_N19
\myRam|ram_rtl_0_bypass[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => addr_wr(3),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myRam|ram_rtl_0_bypass\(7));

-- Location: LCCOMB_X14_Y18_N18
\myRam|ram~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \myRam|ram~1_combout\ = (addr_rd(3) & (\myRam|ram_rtl_0_bypass\(7) & (addr_rd(2) $ (!\myRam|ram_rtl_0_bypass\(5))))) # (!addr_rd(3) & (!\myRam|ram_rtl_0_bypass\(7) & (addr_rd(2) $ (!\myRam|ram_rtl_0_bypass\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => addr_rd(3),
	datab => addr_rd(2),
	datac => \myRam|ram_rtl_0_bypass\(5),
	datad => \myRam|ram_rtl_0_bypass\(7),
	combout => \myRam|ram~1_combout\);

-- Location: LCFF_X15_Y18_N21
\addr_wr[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \addr_wr[1]~9_combout\,
	sclr => \ALT_INV_KEY~combout\(0),
	ena => \addr_wr[4]~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => addr_wr(1));

-- Location: LCFF_X14_Y18_N15
\myRam|ram_rtl_0_bypass[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => addr_wr(1),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myRam|ram_rtl_0_bypass\(3));

-- Location: LCFF_X14_Y18_N13
\myRam|ram_rtl_0_bypass[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => addr_wr(0),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myRam|ram_rtl_0_bypass\(1));

-- Location: LCCOMB_X14_Y18_N12
\myRam|ram~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \myRam|ram~0_combout\ = (addr_rd(0) & (\myRam|ram_rtl_0_bypass\(1) & (\myRam|ram_rtl_0_bypass\(3) $ (!addr_rd(1))))) # (!addr_rd(0) & (!\myRam|ram_rtl_0_bypass\(1) & (\myRam|ram_rtl_0_bypass\(3) $ (!addr_rd(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => addr_rd(0),
	datab => \myRam|ram_rtl_0_bypass\(3),
	datac => \myRam|ram_rtl_0_bypass\(1),
	datad => addr_rd(1),
	combout => \myRam|ram~0_combout\);

-- Location: LCCOMB_X14_Y18_N14
\myRam|ram~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \myRam|ram~3_combout\ = (\myRam|ram~2_combout\ & (\myRam|ram~1_combout\ & \myRam|ram~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram~2_combout\,
	datab => \myRam|ram~1_combout\,
	datad => \myRam|ram~0_combout\,
	combout => \myRam|ram~3_combout\);

-- Location: PIN_L22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(0),
	combout => \SW~combout\(0));

-- Location: LCFF_X18_Y18_N1
\myRam|ram_rtl_0_bypass[11]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \SW~combout\(0),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myRam|ram_rtl_0_bypass\(11));

-- Location: PIN_L1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\CLOCK_50~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_CLOCK_50,
	combout => \CLOCK_50~combout\);

-- Location: CLKCTRL_G2
\CLOCK_50~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLOCK_50~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLOCK_50~clkctrl_outclk\);

-- Location: PIN_L21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(1),
	combout => \SW~combout\(1));

-- Location: PIN_M22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(2),
	combout => \SW~combout\(2));

-- Location: PIN_V12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(3),
	combout => \SW~combout\(3));

-- Location: PIN_W12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(4),
	combout => \SW~combout\(4));

-- Location: PIN_U12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(5),
	combout => \SW~combout\(5));

-- Location: PIN_U11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(6),
	combout => \SW~combout\(6));

-- Location: PIN_M2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[7]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(7),
	combout => \SW~combout\(7));

-- Location: PIN_M1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[8]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(8),
	combout => \SW~combout\(8));

-- Location: PIN_L2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[9]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(9),
	combout => \SW~combout\(9));

-- Location: M4K_X17_Y18
\myRam|ram_rtl_0|auto_generated|ram_block1a0\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "ram:myRam|altsyncram:ram_rtl_0|altsyncram_s7c1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 5,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 10,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 31,
	port_a_logical_ram_depth => 32,
	port_a_logical_ram_width => 10,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 5,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 10,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 31,
	port_b_logical_ram_depth => 32,
	port_b_logical_ram_width => 10,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock0",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => \we~1_combout\,
	portbrewe => VCC,
	clk0 => \CLOCK_50~clkctrl_outclk\,
	portadatain => \myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTADATAIN_bus\,
	portaaddr => \myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTAADDR_bus\,
	portbaddr => \myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \myRam|ram_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X18_Y18_N0
\myRam|ram~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \myRam|ram~4_combout\ = (\myRam|ram~3_combout\ & (\myRam|ram_rtl_0_bypass\(11))) # (!\myRam|ram~3_combout\ & ((\myRam|ram_rtl_0|auto_generated|ram_block1a0~portbdataout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \myRam|ram~3_combout\,
	datac => \myRam|ram_rtl_0_bypass\(11),
	datad => \myRam|ram_rtl_0|auto_generated|ram_block1a0~portbdataout\,
	combout => \myRam|ram~4_combout\);

-- Location: LCFF_X18_Y18_N3
\myRam|ram_rtl_0_bypass[12]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \SW~combout\(1),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myRam|ram_rtl_0_bypass\(12));

-- Location: LCCOMB_X18_Y18_N2
\myRam|ram~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \myRam|ram~5_combout\ = (\myRam|ram~3_combout\ & (\myRam|ram_rtl_0_bypass\(12))) # (!\myRam|ram~3_combout\ & ((\myRam|ram_rtl_0|auto_generated|ram_block1a1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \myRam|ram~3_combout\,
	datac => \myRam|ram_rtl_0_bypass\(12),
	datad => \myRam|ram_rtl_0|auto_generated|ram_block1a1\,
	combout => \myRam|ram~5_combout\);

-- Location: LCFF_X18_Y18_N5
\myRam|ram_rtl_0_bypass[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \SW~combout\(2),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myRam|ram_rtl_0_bypass\(13));

-- Location: LCCOMB_X18_Y18_N4
\myRam|ram~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \myRam|ram~6_combout\ = (\myRam|ram~3_combout\ & (\myRam|ram_rtl_0_bypass\(13))) # (!\myRam|ram~3_combout\ & ((\myRam|ram_rtl_0|auto_generated|ram_block1a2\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \myRam|ram~3_combout\,
	datac => \myRam|ram_rtl_0_bypass\(13),
	datad => \myRam|ram_rtl_0|auto_generated|ram_block1a2\,
	combout => \myRam|ram~6_combout\);

-- Location: LCFF_X18_Y18_N27
\myRam|ram_rtl_0_bypass[14]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \SW~combout\(3),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myRam|ram_rtl_0_bypass\(14));

-- Location: LCCOMB_X18_Y18_N26
\myRam|ram~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \myRam|ram~7_combout\ = (\myRam|ram~3_combout\ & (\myRam|ram_rtl_0_bypass\(14))) # (!\myRam|ram~3_combout\ & ((\myRam|ram_rtl_0|auto_generated|ram_block1a3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \myRam|ram~3_combout\,
	datac => \myRam|ram_rtl_0_bypass\(14),
	datad => \myRam|ram_rtl_0|auto_generated|ram_block1a3\,
	combout => \myRam|ram~7_combout\);

-- Location: LCCOMB_X1_Y18_N28
\dec0|WideOr6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr6~0_combout\ = (\myRam|ram~6_combout\ & (!\myRam|ram~5_combout\ & (\myRam|ram~4_combout\ $ (!\myRam|ram~7_combout\)))) # (!\myRam|ram~6_combout\ & (\myRam|ram~4_combout\ & (\myRam|ram~5_combout\ $ (!\myRam|ram~7_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100000010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram~4_combout\,
	datab => \myRam|ram~5_combout\,
	datac => \myRam|ram~6_combout\,
	datad => \myRam|ram~7_combout\,
	combout => \dec0|WideOr6~0_combout\);

-- Location: LCCOMB_X1_Y18_N10
\dec0|WideOr5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr5~0_combout\ = (\myRam|ram~5_combout\ & ((\myRam|ram~4_combout\ & ((\myRam|ram~7_combout\))) # (!\myRam|ram~4_combout\ & (\myRam|ram~6_combout\)))) # (!\myRam|ram~5_combout\ & (\myRam|ram~6_combout\ & (\myRam|ram~4_combout\ $ 
-- (\myRam|ram~7_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100001100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram~4_combout\,
	datab => \myRam|ram~5_combout\,
	datac => \myRam|ram~6_combout\,
	datad => \myRam|ram~7_combout\,
	combout => \dec0|WideOr5~0_combout\);

-- Location: LCCOMB_X1_Y18_N20
\dec0|WideOr4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr4~0_combout\ = (\myRam|ram~6_combout\ & (\myRam|ram~7_combout\ & ((\myRam|ram~5_combout\) # (!\myRam|ram~4_combout\)))) # (!\myRam|ram~6_combout\ & (!\myRam|ram~4_combout\ & (\myRam|ram~5_combout\ & !\myRam|ram~7_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram~4_combout\,
	datab => \myRam|ram~5_combout\,
	datac => \myRam|ram~6_combout\,
	datad => \myRam|ram~7_combout\,
	combout => \dec0|WideOr4~0_combout\);

-- Location: LCCOMB_X1_Y18_N22
\dec0|WideOr3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr3~0_combout\ = (\myRam|ram~5_combout\ & ((\myRam|ram~4_combout\ & (\myRam|ram~6_combout\)) # (!\myRam|ram~4_combout\ & (!\myRam|ram~6_combout\ & \myRam|ram~7_combout\)))) # (!\myRam|ram~5_combout\ & (!\myRam|ram~7_combout\ & 
-- (\myRam|ram~4_combout\ $ (\myRam|ram~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010010010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram~4_combout\,
	datab => \myRam|ram~5_combout\,
	datac => \myRam|ram~6_combout\,
	datad => \myRam|ram~7_combout\,
	combout => \dec0|WideOr3~0_combout\);

-- Location: LCCOMB_X1_Y18_N4
\dec0|WideOr2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr2~0_combout\ = (\myRam|ram~5_combout\ & (\myRam|ram~4_combout\ & ((!\myRam|ram~7_combout\)))) # (!\myRam|ram~5_combout\ & ((\myRam|ram~6_combout\ & ((!\myRam|ram~7_combout\))) # (!\myRam|ram~6_combout\ & (\myRam|ram~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001010111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram~4_combout\,
	datab => \myRam|ram~5_combout\,
	datac => \myRam|ram~6_combout\,
	datad => \myRam|ram~7_combout\,
	combout => \dec0|WideOr2~0_combout\);

-- Location: LCCOMB_X1_Y18_N6
\dec0|WideOr1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr1~0_combout\ = (\myRam|ram~4_combout\ & (\myRam|ram~7_combout\ $ (((\myRam|ram~5_combout\) # (!\myRam|ram~6_combout\))))) # (!\myRam|ram~4_combout\ & (\myRam|ram~5_combout\ & (!\myRam|ram~6_combout\ & !\myRam|ram~7_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000010001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram~4_combout\,
	datab => \myRam|ram~5_combout\,
	datac => \myRam|ram~6_combout\,
	datad => \myRam|ram~7_combout\,
	combout => \dec0|WideOr1~0_combout\);

-- Location: LCCOMB_X1_Y18_N0
\dec0|WideOr0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr0~0_combout\ = (\myRam|ram~4_combout\ & ((\myRam|ram~7_combout\) # (\myRam|ram~5_combout\ $ (\myRam|ram~6_combout\)))) # (!\myRam|ram~4_combout\ & ((\myRam|ram~5_combout\) # (\myRam|ram~6_combout\ $ (\myRam|ram~7_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111101111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram~4_combout\,
	datab => \myRam|ram~5_combout\,
	datac => \myRam|ram~6_combout\,
	datad => \myRam|ram~7_combout\,
	combout => \dec0|WideOr0~0_combout\);

-- Location: LCFF_X14_Y18_N31
\myRam|ram_rtl_0_bypass[18]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \SW~combout\(7),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myRam|ram_rtl_0_bypass\(18));

-- Location: LCCOMB_X14_Y18_N30
\myRam|ram~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \myRam|ram~11_combout\ = (\myRam|ram~3_combout\ & (\myRam|ram_rtl_0_bypass\(18))) # (!\myRam|ram~3_combout\ & ((\myRam|ram_rtl_0|auto_generated|ram_block1a7\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram~3_combout\,
	datac => \myRam|ram_rtl_0_bypass\(18),
	datad => \myRam|ram_rtl_0|auto_generated|ram_block1a7\,
	combout => \myRam|ram~11_combout\);

-- Location: LCFF_X18_Y18_N25
\myRam|ram_rtl_0_bypass[17]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \SW~combout\(6),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myRam|ram_rtl_0_bypass\(17));

-- Location: LCCOMB_X18_Y18_N24
\myRam|ram~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \myRam|ram~10_combout\ = (\myRam|ram~3_combout\ & (\myRam|ram_rtl_0_bypass\(17))) # (!\myRam|ram~3_combout\ & ((\myRam|ram_rtl_0|auto_generated|ram_block1a6\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \myRam|ram~3_combout\,
	datac => \myRam|ram_rtl_0_bypass\(17),
	datad => \myRam|ram_rtl_0|auto_generated|ram_block1a6\,
	combout => \myRam|ram~10_combout\);

-- Location: LCFF_X18_Y18_N13
\myRam|ram_rtl_0_bypass[15]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \SW~combout\(4),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myRam|ram_rtl_0_bypass\(15));

-- Location: LCCOMB_X18_Y18_N12
\myRam|ram~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \myRam|ram~8_combout\ = (\myRam|ram~3_combout\ & (\myRam|ram_rtl_0_bypass\(15))) # (!\myRam|ram~3_combout\ & ((\myRam|ram_rtl_0|auto_generated|ram_block1a4\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \myRam|ram~3_combout\,
	datac => \myRam|ram_rtl_0_bypass\(15),
	datad => \myRam|ram_rtl_0|auto_generated|ram_block1a4\,
	combout => \myRam|ram~8_combout\);

-- Location: LCFF_X18_Y18_N11
\myRam|ram_rtl_0_bypass[16]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \SW~combout\(5),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myRam|ram_rtl_0_bypass\(16));

-- Location: LCCOMB_X18_Y18_N10
\myRam|ram~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \myRam|ram~9_combout\ = (\myRam|ram~3_combout\ & (\myRam|ram_rtl_0_bypass\(16))) # (!\myRam|ram~3_combout\ & ((\myRam|ram_rtl_0|auto_generated|ram_block1a5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \myRam|ram~3_combout\,
	datac => \myRam|ram_rtl_0_bypass\(16),
	datad => \myRam|ram_rtl_0|auto_generated|ram_block1a5\,
	combout => \myRam|ram~9_combout\);

-- Location: LCCOMB_X1_Y21_N8
\dec1|WideOr6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr6~0_combout\ = (\myRam|ram~11_combout\ & (\myRam|ram~8_combout\ & (\myRam|ram~10_combout\ $ (\myRam|ram~9_combout\)))) # (!\myRam|ram~11_combout\ & (!\myRam|ram~9_combout\ & (\myRam|ram~10_combout\ $ (\myRam|ram~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000010010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram~11_combout\,
	datab => \myRam|ram~10_combout\,
	datac => \myRam|ram~8_combout\,
	datad => \myRam|ram~9_combout\,
	combout => \dec1|WideOr6~0_combout\);

-- Location: LCCOMB_X1_Y21_N26
\dec1|WideOr5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr5~0_combout\ = (\myRam|ram~11_combout\ & ((\myRam|ram~8_combout\ & ((\myRam|ram~9_combout\))) # (!\myRam|ram~8_combout\ & (\myRam|ram~10_combout\)))) # (!\myRam|ram~11_combout\ & (\myRam|ram~10_combout\ & (\myRam|ram~8_combout\ $ 
-- (\myRam|ram~9_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110001001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram~11_combout\,
	datab => \myRam|ram~10_combout\,
	datac => \myRam|ram~8_combout\,
	datad => \myRam|ram~9_combout\,
	combout => \dec1|WideOr5~0_combout\);

-- Location: LCCOMB_X1_Y21_N28
\dec1|WideOr4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr4~0_combout\ = (\myRam|ram~11_combout\ & (\myRam|ram~10_combout\ & ((\myRam|ram~9_combout\) # (!\myRam|ram~8_combout\)))) # (!\myRam|ram~11_combout\ & (!\myRam|ram~10_combout\ & (!\myRam|ram~8_combout\ & \myRam|ram~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram~11_combout\,
	datab => \myRam|ram~10_combout\,
	datac => \myRam|ram~8_combout\,
	datad => \myRam|ram~9_combout\,
	combout => \dec1|WideOr4~0_combout\);

-- Location: LCCOMB_X1_Y21_N10
\dec1|WideOr3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr3~0_combout\ = (\myRam|ram~9_combout\ & ((\myRam|ram~10_combout\ & ((\myRam|ram~8_combout\))) # (!\myRam|ram~10_combout\ & (\myRam|ram~11_combout\ & !\myRam|ram~8_combout\)))) # (!\myRam|ram~9_combout\ & (!\myRam|ram~11_combout\ & 
-- (\myRam|ram~10_combout\ $ (\myRam|ram~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram~11_combout\,
	datab => \myRam|ram~10_combout\,
	datac => \myRam|ram~8_combout\,
	datad => \myRam|ram~9_combout\,
	combout => \dec1|WideOr3~0_combout\);

-- Location: LCCOMB_X1_Y21_N16
\dec1|WideOr2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr2~0_combout\ = (\myRam|ram~9_combout\ & (!\myRam|ram~11_combout\ & ((\myRam|ram~8_combout\)))) # (!\myRam|ram~9_combout\ & ((\myRam|ram~10_combout\ & (!\myRam|ram~11_combout\)) # (!\myRam|ram~10_combout\ & ((\myRam|ram~8_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram~11_combout\,
	datab => \myRam|ram~10_combout\,
	datac => \myRam|ram~8_combout\,
	datad => \myRam|ram~9_combout\,
	combout => \dec1|WideOr2~0_combout\);

-- Location: LCCOMB_X1_Y21_N18
\dec1|WideOr1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr1~0_combout\ = (\myRam|ram~10_combout\ & (\myRam|ram~8_combout\ & (\myRam|ram~11_combout\ $ (\myRam|ram~9_combout\)))) # (!\myRam|ram~10_combout\ & (!\myRam|ram~11_combout\ & ((\myRam|ram~8_combout\) # (\myRam|ram~9_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram~11_combout\,
	datab => \myRam|ram~10_combout\,
	datac => \myRam|ram~8_combout\,
	datad => \myRam|ram~9_combout\,
	combout => \dec1|WideOr1~0_combout\);

-- Location: LCCOMB_X1_Y21_N0
\dec1|WideOr0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr0~0_combout\ = (\myRam|ram~8_combout\ & ((\myRam|ram~11_combout\) # (\myRam|ram~10_combout\ $ (\myRam|ram~9_combout\)))) # (!\myRam|ram~8_combout\ & ((\myRam|ram~9_combout\) # (\myRam|ram~11_combout\ $ (\myRam|ram~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111111100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram~11_combout\,
	datab => \myRam|ram~10_combout\,
	datac => \myRam|ram~8_combout\,
	datad => \myRam|ram~9_combout\,
	combout => \dec1|WideOr0~0_combout\);

-- Location: LCFF_X14_Y18_N9
\myRam|ram_rtl_0_bypass[19]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \SW~combout\(8),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myRam|ram_rtl_0_bypass\(19));

-- Location: LCFF_X14_Y18_N29
\myRam|ram_rtl_0_bypass[20]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	sdata => \SW~combout\(9),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \myRam|ram_rtl_0_bypass\(20));

-- Location: LCCOMB_X14_Y18_N10
\myRam|ram~13\ : cycloneii_lcell_comb
-- Equation(s):
-- \myRam|ram~13_combout\ = (\myRam|ram~3_combout\ & ((\myRam|ram_rtl_0_bypass\(20)))) # (!\myRam|ram~3_combout\ & (\myRam|ram_rtl_0|auto_generated|ram_block1a9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram_rtl_0|auto_generated|ram_block1a9\,
	datab => \myRam|ram_rtl_0_bypass\(20),
	datad => \myRam|ram~3_combout\,
	combout => \myRam|ram~13_combout\);

-- Location: LCCOMB_X14_Y18_N0
\dec2|Decoder0~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|Decoder0~12_combout\ = (!\myRam|ram~13_combout\ & ((\myRam|ram~3_combout\ & ((\myRam|ram_rtl_0_bypass\(19)))) # (!\myRam|ram~3_combout\ & (\myRam|ram_rtl_0|auto_generated|ram_block1a8\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram~3_combout\,
	datab => \myRam|ram_rtl_0|auto_generated|ram_block1a8\,
	datac => \myRam|ram_rtl_0_bypass\(19),
	datad => \myRam|ram~13_combout\,
	combout => \dec2|Decoder0~12_combout\);

-- Location: LCCOMB_X14_Y18_N22
\myRam|ram~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \myRam|ram~12_combout\ = (\myRam|ram~3_combout\ & (\myRam|ram_rtl_0_bypass\(19))) # (!\myRam|ram~3_combout\ & ((\myRam|ram_rtl_0|auto_generated|ram_block1a8\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \myRam|ram_rtl_0_bypass\(19),
	datac => \myRam|ram_rtl_0|auto_generated|ram_block1a8\,
	datad => \myRam|ram~3_combout\,
	combout => \myRam|ram~12_combout\);

-- Location: LCCOMB_X14_Y18_N26
\dec2|Decoder0~13\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|Decoder0~13_combout\ = (!\myRam|ram~12_combout\ & ((\myRam|ram~3_combout\ & ((\myRam|ram_rtl_0_bypass\(20)))) # (!\myRam|ram~3_combout\ & (\myRam|ram_rtl_0|auto_generated|ram_block1a9\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram_rtl_0|auto_generated|ram_block1a9\,
	datab => \myRam|ram_rtl_0_bypass\(20),
	datac => \myRam|ram~12_combout\,
	datad => \myRam|ram~3_combout\,
	combout => \dec2|Decoder0~13_combout\);

-- Location: LCCOMB_X14_Y18_N24
\dec2|Decoder0~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|Decoder0~14_combout\ = (\myRam|ram~13_combout\) # ((\myRam|ram~3_combout\ & ((\myRam|ram_rtl_0_bypass\(19)))) # (!\myRam|ram~3_combout\ & (\myRam|ram_rtl_0|auto_generated|ram_block1a8\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \myRam|ram~3_combout\,
	datab => \myRam|ram_rtl_0|auto_generated|ram_block1a8\,
	datac => \myRam|ram_rtl_0_bypass\(19),
	datad => \myRam|ram~13_combout\,
	combout => \dec2|Decoder0~14_combout\);

-- Location: PIN_R21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(1));

-- Location: PIN_U22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDG[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Equal1~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDG(0));

-- Location: PIN_U21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDG[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDG(1));

-- Location: PIN_J2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(0));

-- Location: PIN_J1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(1));

-- Location: PIN_H2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(2));

-- Location: PIN_H1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(3));

-- Location: PIN_F2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(4));

-- Location: PIN_F1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(5));

-- Location: PIN_E2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|ALT_INV_WideOr0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(6));

-- Location: PIN_E1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(0));

-- Location: PIN_H6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(1));

-- Location: PIN_H5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(2));

-- Location: PIN_H4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(3));

-- Location: PIN_G3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(4));

-- Location: PIN_D2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(5));

-- Location: PIN_D1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|ALT_INV_WideOr0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(6));

-- Location: PIN_G5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|Decoder0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(0));

-- Location: PIN_G6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(1));

-- Location: PIN_C2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|Decoder0~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(2));

-- Location: PIN_C1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|Decoder0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(3));

-- Location: PIN_E3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \myRam|ram~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(4));

-- Location: PIN_E4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|Decoder0~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(5));

-- Location: PIN_D3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \myRam|ALT_INV_ram~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(6));

-- Location: PIN_F4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(0));

-- Location: PIN_D5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(1));

-- Location: PIN_D6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(2));

-- Location: PIN_J4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(3));

-- Location: PIN_L8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(4));

-- Location: PIN_F3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(5));

-- Location: PIN_D4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(6));
END structure;


