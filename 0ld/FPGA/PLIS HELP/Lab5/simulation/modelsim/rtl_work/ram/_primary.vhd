library verilog;
use verilog.vl_types.all;
entity ram is
    port(
        clk             : in     vl_logic;
        addr_rd         : in     vl_logic_vector(4 downto 0);
        addr_wr         : in     vl_logic_vector(4 downto 0);
        we              : in     vl_logic;
        data_in         : in     vl_logic_vector(9 downto 0);
        data_out        : out    vl_logic_vector(9 downto 0)
    );
end ram;
