module secondsTo7Seg(input seconds,
							output[3:0] HEX0,
							output[3:0] HEX1,
							output[3:0] HEX2,
							output[3:0] HEX3);
			
	wire[15:0] decs;
	assign decs[3:0] = timer%10;
	assign decs[7:4] = (timer%100)/10;
	assign decs[11:8] = (timer%1000)/100;
	assign decs[15:12] = timer/1000;
	
	
	dec dec0(.in(decs[3:0]),
				.out(HEX0));
				
	dec dec1(.in(decs[7:4]),
				.out(HEX1));
				
	dec dec2(.in(decs[11:8]),
				.out(HEX2));		
				
	dec dec3(.in(decs[15:12]),
				.out(HEX3));
	
							
							
endmodule 
							