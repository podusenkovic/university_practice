`timescale 10ps/1ps
module tb_main();

		reg  CLOCK_50;
		reg  [9:0]    SW;
		reg  [3:0]	  KEY;
		wire [6:0]    HEX0;
		wire [6:0]    HEX1;
		wire [6:0]    HEX2;
		wire [6:0]    HEX3;
		
		main DUT(.CLOCK_50(CLOCK_50),
					.SW(SW),
					.KEY(KEY),
					.LEDG(LEDG),
					.HEX0(HEX0),
					.HEX1(HEX1),
					.HEX2(HEX2),
					.HEX3(HEX3));
		
		initial begin
			SW = 10'b0;
			KEY[3] = 1'b1;
			KEY[2] = 1'b1;
			KEY[0] = 1'b1;
		end
		
		initial begin
			CLOCK_50 = 1'b1;
			forever #10 CLOCK_50 = !CLOCK_50;
		end
		
		initial begin
			SW = 10'b1;
			#100;
			KEY[3] = 1'b0;
			#50;
			KEY[3] = 1'b1;
			#100;
			SW = 10'b0101010101;
			#100;
			KEY[3] = 1'b0;
			#50;
			KEY[3] = 1'b1;
			#100;
			
			SW = 10'b1010101010;
			#100;
			KEY[3] = 1'b0;
			#50;
			KEY[3] = 1'b1;
			#100;
			
			KEY[2] = 1'b0;
			#50;
			KEY[2] = 1'b1;
			#100;
			KEY[2] = 1'b0;
			#50;
			KEY[2] = 1'b1;
			#100;
			KEY[2] = 1'b0;
			#50;
			KEY[2] = 1'b1;
			#100;
			KEY[2] = 1'b0;
			#50;
			KEY[2] = 1'b1;
			#100;
			KEY[2] = 1'b0;
			#50;
			KEY[2] = 1'b1;
			#100;			
			
		end


endmodule 