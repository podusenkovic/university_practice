module ram(input 			clk,
			  input  [4:0]	addr_rd,
			  input  [4:0]	addr_wr,
			  input 			we,
			  input  [9:0]	data_in,
			  output [9:0] data_out);
			  
			  
	reg [9:0] ram [31:0];
	always @(posedge clk)
		if (we) ram[addr_wr] <= data_in;
	
	assign data_out = ram[addr_rd];
	
endmodule 