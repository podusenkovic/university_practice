module pr1(input[7:0]SW,
			  output reg[9:0] LEDR,
			  output[6:0]HEX0,
			  output[6:0]HEX1,
			  output[6:0]HEX2,
			  output[6:0]HEX3);
			  
	/*assign LEDR[3:0] = SW % 10;
	assign LEDR[7:4] = (SW / 10) % 10;
	assign LEDR[9:8] = SW / 100;*/
	
	
	
	dec dec0(.in(LEDR[3:0]),
				.out(HEX0));
				
	dec dec1(.in(LEDR[7:4]),
				.out(HEX1));
				
	dec dec2(.in(LEDR[9:8]),
				.out(HEX2));	
				
	dec dec3(.in(4'b0000),
				.out(HEX3));
	
	

endmodule
	