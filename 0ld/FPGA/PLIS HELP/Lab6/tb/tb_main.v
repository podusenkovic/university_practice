`timescale 1ns/1ps
module tb_main();

		reg  PS2_CLK = 1;
		reg  PS2_DAT;
		reg  CLOCK_50;
		reg  [9:0]    SW;
		reg  [3:0]	  KEY;
		wire [9:0] 	  LEDR;
		wire [1:0] 	  LEDG;
		wire [6:0]    HEX0;
		wire [6:0]    HEX1;
		wire [6:0]    HEX2;
		wire [6:0]    HEX3;
		
		task simKPress;
			input [7:0] button;
			begin 
				PS2_CLK = 1'b1;
				#3000;
				PS2_CLK = ~PS2_CLK;
				PS2_DAT = 1'b0;
				#500;
				PS2_CLK = ~PS2_CLK;
				#500;
				PS2_CLK = ~PS2_CLK;
				PS2_DAT = button[0];
				#500;
				PS2_CLK = ~PS2_CLK;
				#500;
				PS2_CLK = ~PS2_CLK;
				PS2_DAT = button[1];
				#500;
				PS2_CLK = ~PS2_CLK;
				#500;
				PS2_CLK = ~PS2_CLK;
				PS2_DAT = button[2];
				#500;
				PS2_CLK = ~PS2_CLK;
				#500;
				PS2_CLK = ~PS2_CLK;
				PS2_DAT = button[3];
				#500;
				PS2_CLK = ~PS2_CLK;
				#500;
				PS2_CLK = ~PS2_CLK;
				PS2_DAT = button[4];
				#500;
				PS2_CLK = ~PS2_CLK;
				#500;
				PS2_CLK = ~PS2_CLK;
				PS2_DAT = button[5];
				#500;
				PS2_CLK = ~PS2_CLK;
				#500;
				PS2_CLK = ~PS2_CLK;
				PS2_DAT = button[6];
				#500;
				PS2_CLK = ~PS2_CLK;
				#500;
				PS2_CLK = ~PS2_CLK;
				PS2_DAT = button[7];
				#500;
				PS2_CLK = ~PS2_CLK;
				#500;
				PS2_CLK = ~PS2_CLK;
				PS2_DAT = 1'b1;
				#500;
				PS2_CLK = ~PS2_CLK;
				#500;
				PS2_CLK = ~PS2_CLK;
				#500;
				PS2_CLK = ~PS2_CLK;
				#3000;				
			end
		endtask
		
		main DUT(.PS2_CLK(PS2_CLK),
					.PS2_DAT(PS2_DAT),					
					.CLOCK_50(CLOCK_50),
					.SW(SW),
					.KEY(KEY),
					.LEDG(LEDG),
					.LEDR(LEDR),
					.HEX0(HEX0),
					.HEX1(HEX1),
					.HEX2(HEX2),
					.HEX3(HEX3));
		
		initial begin
			SW = 10'b0;
			KEY[3] = 1'b1;
			KEY[2] = 1'b1;
			KEY[0] = 1'b1;
		end
		
		initial begin
			CLOCK_50 = 1'b1;
			forever #10 CLOCK_50 = ~CLOCK_50;
		end
		
		initial begin
			KEY[0] = 1'b0;
			PS2_CLK = 1;
			#50;
			KEY[0] = 1'b1;
			#100;
			
			SW = 10'b0000001111;
			
			simKPress(8'h1C);
			simKPress(8'h1C);
			simKPress(8'h1C);
			simKPress(8'h32);
			simKPress(8'h32);
			simKPress(8'h2B);
			simKPress(8'h21);
			
		end


endmodule 