module main(input PS2_CLK,
				input PS2_DAT,
				input CLOCK_50,
				input [9:0]SW,
				input [3:0]KEY,
				output[9:0]LEDR,
				output[1:0]LEDG,
				output[6:0]HEX0,
				output[6:0]HEX1,
				output[6:0]HEX2,
				output[6:0]HEX3);
	
	wire [3:0] dataFromKB;
	wire [3:0] dataOut = dataFromKB;
	
	wire [15:0] counter;	
	
	reg press_d0, press_d1, press_d2;

	wire rst = ~KEY[0];
	
	wire pressInfo;

		
	dec dec0(.in(counter[3:0]),
				.out(HEX0));
	dec dec1(.in(counter[7:4]),
				.out(HEX1));
	dec dec2(.in(counter[11:8]),
				.out(HEX2));
	dec dec3(.in(counter[15:12]),
				.out(HEX3));
				
	keyboard kb0 (.ps2clk (PS2_CLK),
					  .ps2dat (PS2_DAT),
					  .KP (pressInfo),
					  .KB (dataFromKB));
	
	slave slave0 (.clk(CLOCK_50),
					  .rst(rst),
					  .SW(SW),
					  .KButton(dataFromKB),
					  .pressed(signalPressed),
					  .counter(counter));
	
	assign LEDR[9:0] = dataFromKB[3:0];
	
	always@(posedge CLOCK_50)
		if (~KEY[0]) begin
			press_d0 <= pressInfo;
			press_d1 <= pressInfo;
			press_d2 <= pressInfo;
		end
		else begin
			press_d0 <= pressInfo;
			press_d1 <= press_d0;
			press_d2 <= press_d1;
		end
	
	assign signalPressed = (press_d1 && !press_d0) ? 1'b1 : 1'b0;
	assign LEDG[0] = signalPressed;

endmodule 