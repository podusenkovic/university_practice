module slave(input clk,
				 input rst,
				 input [9:0]SW,
				 input [3:0]KButton,
				 input pressed,
				 output reg [15:0]counter);
				 
	always@(posedge clk)
		if (rst)
			counter <= 16'b0;
		else if (pressed)
			case (KButton)
			4'd10 : begin 
						if (counter == 16'hFFFF)
							counter <= 16'h0;
						else counter <= counter + 16'b1;
					  end 
			4'd11 : begin
						if (counter == 16'h0)
							counter <= 16'hFFFF;
						else counter <= counter - 16'b1;
					  end
			4'd12 : counter <= SW;
			4'd15 : counter <= 16'h0;
			endcase			
endmodule 