-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

-- DATE "04/24/2018 12:04:07"

-- 
-- Device: Altera EP2C20F484C7 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEII;
LIBRARY IEEE;
USE CYCLONEII.CYCLONEII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	main IS
    PORT (
	PS2_CLK : IN std_logic;
	PS2_DAT : IN std_logic;
	CLOCK_50 : IN std_logic;
	SW : IN std_logic_vector(9 DOWNTO 0);
	KEY : IN std_logic_vector(3 DOWNTO 0);
	LEDR : OUT std_logic_vector(9 DOWNTO 0);
	LEDG : OUT std_logic_vector(1 DOWNTO 0);
	HEX0 : OUT std_logic_vector(6 DOWNTO 0);
	HEX1 : OUT std_logic_vector(6 DOWNTO 0);
	HEX2 : OUT std_logic_vector(6 DOWNTO 0);
	HEX3 : OUT std_logic_vector(6 DOWNTO 0)
	);
END main;

-- Design Ports Information
-- KEY[1]	=>  Location: PIN_R21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[2]	=>  Location: PIN_T22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[3]	=>  Location: PIN_T21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- LEDR[0]	=>  Location: PIN_R20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[1]	=>  Location: PIN_R19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[2]	=>  Location: PIN_U19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[3]	=>  Location: PIN_Y19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[4]	=>  Location: PIN_T18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[5]	=>  Location: PIN_V19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[6]	=>  Location: PIN_Y18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[7]	=>  Location: PIN_U18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[8]	=>  Location: PIN_R18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[9]	=>  Location: PIN_R17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDG[0]	=>  Location: PIN_U22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDG[1]	=>  Location: PIN_U21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[0]	=>  Location: PIN_J2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[1]	=>  Location: PIN_J1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[2]	=>  Location: PIN_H2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[3]	=>  Location: PIN_H1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[4]	=>  Location: PIN_F2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[5]	=>  Location: PIN_F1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[6]	=>  Location: PIN_E2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[0]	=>  Location: PIN_E1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[1]	=>  Location: PIN_H6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[2]	=>  Location: PIN_H5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[3]	=>  Location: PIN_H4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[4]	=>  Location: PIN_G3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[5]	=>  Location: PIN_D2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[6]	=>  Location: PIN_D1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[0]	=>  Location: PIN_G5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[1]	=>  Location: PIN_G6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[2]	=>  Location: PIN_C2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[3]	=>  Location: PIN_C1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[4]	=>  Location: PIN_E3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[5]	=>  Location: PIN_E4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[6]	=>  Location: PIN_D3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[0]	=>  Location: PIN_F4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[1]	=>  Location: PIN_D5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[2]	=>  Location: PIN_D6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[3]	=>  Location: PIN_J4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[4]	=>  Location: PIN_L8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[5]	=>  Location: PIN_F3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[6]	=>  Location: PIN_D4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- KEY[0]	=>  Location: PIN_R22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- CLOCK_50	=>  Location: PIN_L1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_L22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_L21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_M22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_V12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_W12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[5]	=>  Location: PIN_U12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_U11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_M2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[8]	=>  Location: PIN_M1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[9]	=>  Location: PIN_L2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- PS2_DAT	=>  Location: PIN_J14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- PS2_CLK	=>  Location: PIN_H15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF main IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_PS2_CLK : std_logic;
SIGNAL ww_PS2_DAT : std_logic;
SIGNAL ww_CLOCK_50 : std_logic;
SIGNAL ww_SW : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_KEY : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_LEDR : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_LEDG : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_HEX0 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX2 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX3 : std_logic_vector(6 DOWNTO 0);
SIGNAL \kb0|KP~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \CLOCK_50~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \slave0|Add1~6_combout\ : std_logic;
SIGNAL \kb0|KB[0]~2_combout\ : std_logic;
SIGNAL \kb0|Selector5~1_combout\ : std_logic;
SIGNAL \slave0|counter[9]~1_combout\ : std_logic;
SIGNAL \slave0|counter[11]~22_combout\ : std_logic;
SIGNAL \kb0|dub~regout\ : std_logic;
SIGNAL \kb0|Decoder0~6_combout\ : std_logic;
SIGNAL \kb0|num[5]~6_combout\ : std_logic;
SIGNAL \kb0|dub~0_combout\ : std_logic;
SIGNAL \kb0|dub~1_combout\ : std_logic;
SIGNAL \kb0|always0~0_combout\ : std_logic;
SIGNAL \kb0|always0~1_combout\ : std_logic;
SIGNAL \kb0|always0~2_combout\ : std_logic;
SIGNAL \kb0|always0~3_combout\ : std_logic;
SIGNAL \kb0|always0~4_combout\ : std_logic;
SIGNAL \kb0|dub~2_combout\ : std_logic;
SIGNAL \kb0|dub~3_combout\ : std_logic;
SIGNAL \kb0|dub~4_combout\ : std_logic;
SIGNAL \CLOCK_50~combout\ : std_logic;
SIGNAL \PS2_CLK~combout\ : std_logic;
SIGNAL \CLOCK_50~clkctrl_outclk\ : std_logic;
SIGNAL \kb0|KP~0_combout\ : std_logic;
SIGNAL \kb0|KP~regout\ : std_logic;
SIGNAL \kb0|KP~clkctrl_outclk\ : std_logic;
SIGNAL \PS2_DAT~combout\ : std_logic;
SIGNAL \kb0|i~0_combout\ : std_logic;
SIGNAL \kb0|i~2_combout\ : std_logic;
SIGNAL \kb0|i~1_combout\ : std_logic;
SIGNAL \kb0|i~3_combout\ : std_logic;
SIGNAL \kb0|in[2]~0_combout\ : std_logic;
SIGNAL \kb0|Selector1~0_combout\ : std_logic;
SIGNAL \kb0|in[2]~1_combout\ : std_logic;
SIGNAL \kb0|Selector2~0_combout\ : std_logic;
SIGNAL \kb0|WideOr3~0_combout\ : std_logic;
SIGNAL \kb0|Decoder0~3_combout\ : std_logic;
SIGNAL \kb0|num[0]~3_combout\ : std_logic;
SIGNAL \kb0|Decoder0~0_combout\ : std_logic;
SIGNAL \kb0|num[4]~0_combout\ : std_logic;
SIGNAL \kb0|Selector7~0_combout\ : std_logic;
SIGNAL \kb0|KB[0]~0_combout\ : std_logic;
SIGNAL \kb0|Decoder0~2_combout\ : std_logic;
SIGNAL \kb0|num[3]~2_combout\ : std_logic;
SIGNAL \kb0|Selector0~0_combout\ : std_logic;
SIGNAL \kb0|Decoder0~4_combout\ : std_logic;
SIGNAL \kb0|num[1]~4_combout\ : std_logic;
SIGNAL \kb0|Selector7~1_combout\ : std_logic;
SIGNAL \kb0|Selector7~2_combout\ : std_logic;
SIGNAL \kb0|Decoder0~5_combout\ : std_logic;
SIGNAL \kb0|num[2]~5_combout\ : std_logic;
SIGNAL \kb0|Decoder0~7_combout\ : std_logic;
SIGNAL \kb0|num[7]~7_combout\ : std_logic;
SIGNAL \kb0|KB[0]~3_combout\ : std_logic;
SIGNAL \kb0|KB[0]~4_combout\ : std_logic;
SIGNAL \kb0|Decoder0~1_combout\ : std_logic;
SIGNAL \kb0|num[6]~1_combout\ : std_logic;
SIGNAL \kb0|KB[0]~5_combout\ : std_logic;
SIGNAL \kb0|KB[0]~7_combout\ : std_logic;
SIGNAL \kb0|KB[0]~6_combout\ : std_logic;
SIGNAL \kb0|KB[0]~8_combout\ : std_logic;
SIGNAL \kb0|Selector6~0_combout\ : std_logic;
SIGNAL \kb0|Selector6~1_combout\ : std_logic;
SIGNAL \kb0|KB[1]~feeder_combout\ : std_logic;
SIGNAL \kb0|num[0]~_wirecell_combout\ : std_logic;
SIGNAL \kb0|Selector5~0_combout\ : std_logic;
SIGNAL \kb0|KB[2]~1_combout\ : std_logic;
SIGNAL \kb0|Selector5~2_combout\ : std_logic;
SIGNAL \kb0|Selector4~0_combout\ : std_logic;
SIGNAL \kb0|Selector4~1_combout\ : std_logic;
SIGNAL \press_d0~feeder_combout\ : std_logic;
SIGNAL \press_d0~regout\ : std_logic;
SIGNAL \press_d1~0_combout\ : std_logic;
SIGNAL \press_d1~regout\ : std_logic;
SIGNAL \signalPressed~0_combout\ : std_logic;
SIGNAL \slave0|Add1~0_combout\ : std_logic;
SIGNAL \slave0|counter~6_combout\ : std_logic;
SIGNAL \slave0|counter[9]~0_combout\ : std_logic;
SIGNAL \slave0|counter~7_combout\ : std_logic;
SIGNAL \slave0|counter[9]~8_combout\ : std_logic;
SIGNAL \slave0|counter[9]~9_combout\ : std_logic;
SIGNAL \slave0|Add1~1\ : std_logic;
SIGNAL \slave0|Add1~3\ : std_logic;
SIGNAL \slave0|Add1~5\ : std_logic;
SIGNAL \slave0|Add1~7\ : std_logic;
SIGNAL \slave0|Add1~8_combout\ : std_logic;
SIGNAL \slave0|counter~16_combout\ : std_logic;
SIGNAL \slave0|Add1~9\ : std_logic;
SIGNAL \slave0|Add1~11\ : std_logic;
SIGNAL \slave0|Add1~12_combout\ : std_logic;
SIGNAL \slave0|counter~18_combout\ : std_logic;
SIGNAL \slave0|Add1~10_combout\ : std_logic;
SIGNAL \slave0|counter~17_combout\ : std_logic;
SIGNAL \slave0|counter[9]~2_combout\ : std_logic;
SIGNAL \slave0|Add1~13\ : std_logic;
SIGNAL \slave0|Add1~15\ : std_logic;
SIGNAL \slave0|Add1~16_combout\ : std_logic;
SIGNAL \slave0|counter~20_combout\ : std_logic;
SIGNAL \slave0|Add1~17\ : std_logic;
SIGNAL \slave0|Add1~18_combout\ : std_logic;
SIGNAL \slave0|counter~21_combout\ : std_logic;
SIGNAL \slave0|Add1~19\ : std_logic;
SIGNAL \slave0|Add1~20_combout\ : std_logic;
SIGNAL \slave0|counter~23_combout\ : std_logic;
SIGNAL \slave0|counter[9]~3_combout\ : std_logic;
SIGNAL \slave0|Add1~21\ : std_logic;
SIGNAL \slave0|Add1~22_combout\ : std_logic;
SIGNAL \slave0|counter~24_combout\ : std_logic;
SIGNAL \slave0|Add1~23\ : std_logic;
SIGNAL \slave0|Add1~24_combout\ : std_logic;
SIGNAL \slave0|counter~25_combout\ : std_logic;
SIGNAL \slave0|Add1~25\ : std_logic;
SIGNAL \slave0|Add1~26_combout\ : std_logic;
SIGNAL \slave0|counter~26_combout\ : std_logic;
SIGNAL \slave0|Add1~27\ : std_logic;
SIGNAL \slave0|Add1~28_combout\ : std_logic;
SIGNAL \slave0|counter~27_combout\ : std_logic;
SIGNAL \slave0|counter[9]~4_combout\ : std_logic;
SIGNAL \slave0|counter[9]~5_combout\ : std_logic;
SIGNAL \slave0|counter[9]~14_combout\ : std_logic;
SIGNAL \slave0|counter~15_combout\ : std_logic;
SIGNAL \slave0|Add1~4_combout\ : std_logic;
SIGNAL \slave0|counter~12_combout\ : std_logic;
SIGNAL \slave0|counter~13_combout\ : std_logic;
SIGNAL \slave0|Add1~2_combout\ : std_logic;
SIGNAL \slave0|counter~10_combout\ : std_logic;
SIGNAL \slave0|counter~11_combout\ : std_logic;
SIGNAL \dec0|WideOr6~0_combout\ : std_logic;
SIGNAL \dec0|WideOr5~0_combout\ : std_logic;
SIGNAL \dec0|WideOr4~0_combout\ : std_logic;
SIGNAL \dec0|WideOr3~0_combout\ : std_logic;
SIGNAL \dec0|WideOr2~0_combout\ : std_logic;
SIGNAL \dec0|WideOr1~0_combout\ : std_logic;
SIGNAL \dec0|WideOr0~0_combout\ : std_logic;
SIGNAL \slave0|Add1~14_combout\ : std_logic;
SIGNAL \slave0|counter~19_combout\ : std_logic;
SIGNAL \dec1|WideOr6~0_combout\ : std_logic;
SIGNAL \dec1|WideOr5~0_combout\ : std_logic;
SIGNAL \dec1|WideOr4~0_combout\ : std_logic;
SIGNAL \dec1|WideOr3~0_combout\ : std_logic;
SIGNAL \dec1|WideOr2~0_combout\ : std_logic;
SIGNAL \dec1|WideOr1~0_combout\ : std_logic;
SIGNAL \dec1|WideOr0~0_combout\ : std_logic;
SIGNAL \dec2|WideOr6~0_combout\ : std_logic;
SIGNAL \dec2|WideOr5~0_combout\ : std_logic;
SIGNAL \dec2|WideOr4~0_combout\ : std_logic;
SIGNAL \dec2|WideOr3~0_combout\ : std_logic;
SIGNAL \dec2|WideOr2~0_combout\ : std_logic;
SIGNAL \dec2|WideOr1~0_combout\ : std_logic;
SIGNAL \dec2|WideOr0~0_combout\ : std_logic;
SIGNAL \slave0|Add1~29\ : std_logic;
SIGNAL \slave0|Add1~30_combout\ : std_logic;
SIGNAL \slave0|counter~28_combout\ : std_logic;
SIGNAL \dec3|WideOr6~0_combout\ : std_logic;
SIGNAL \dec3|WideOr5~0_combout\ : std_logic;
SIGNAL \dec3|WideOr4~0_combout\ : std_logic;
SIGNAL \dec3|WideOr3~0_combout\ : std_logic;
SIGNAL \dec3|WideOr2~0_combout\ : std_logic;
SIGNAL \dec3|WideOr1~0_combout\ : std_logic;
SIGNAL \dec3|WideOr0~0_combout\ : std_logic;
SIGNAL \SW~combout\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \KEY~combout\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \kb0|i\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \kb0|in\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \kb0|num\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \slave0|counter\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \kb0|KB\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \kb0|ALT_INV_num\ : std_logic_vector(2 DOWNTO 2);
SIGNAL \dec3|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \dec2|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \dec1|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \dec0|ALT_INV_WideOr0~0_combout\ : std_logic;

BEGIN

ww_PS2_CLK <= PS2_CLK;
ww_PS2_DAT <= PS2_DAT;
ww_CLOCK_50 <= CLOCK_50;
ww_SW <= SW;
ww_KEY <= KEY;
LEDR <= ww_LEDR;
LEDG <= ww_LEDG;
HEX0 <= ww_HEX0;
HEX1 <= ww_HEX1;
HEX2 <= ww_HEX2;
HEX3 <= ww_HEX3;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\kb0|KP~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \kb0|KP~regout\);

\CLOCK_50~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \CLOCK_50~combout\);
\kb0|ALT_INV_num\(2) <= NOT \kb0|num\(2);
\dec3|ALT_INV_WideOr0~0_combout\ <= NOT \dec3|WideOr0~0_combout\;
\dec2|ALT_INV_WideOr0~0_combout\ <= NOT \dec2|WideOr0~0_combout\;
\dec1|ALT_INV_WideOr0~0_combout\ <= NOT \dec1|WideOr0~0_combout\;
\dec0|ALT_INV_WideOr0~0_combout\ <= NOT \dec0|WideOr0~0_combout\;

-- Location: LCCOMB_X6_Y22_N6
\slave0|Add1~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|Add1~6_combout\ = (\slave0|counter\(3) & ((\kb0|KB\(0) & (\slave0|Add1~5\ & VCC)) # (!\kb0|KB\(0) & (!\slave0|Add1~5\)))) # (!\slave0|counter\(3) & ((\kb0|KB\(0) & (!\slave0|Add1~5\)) # (!\kb0|KB\(0) & ((\slave0|Add1~5\) # (GND)))))
-- \slave0|Add1~7\ = CARRY((\slave0|counter\(3) & (!\kb0|KB\(0) & !\slave0|Add1~5\)) # (!\slave0|counter\(3) & ((!\slave0|Add1~5\) # (!\kb0|KB\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(3),
	datab => \kb0|KB\(0),
	datad => VCC,
	cin => \slave0|Add1~5\,
	combout => \slave0|Add1~6_combout\,
	cout => \slave0|Add1~7\);

-- Location: LCFF_X9_Y22_N7
\kb0|num[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \PS2_CLK~combout\,
	datain => \kb0|num[5]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|num\(5));

-- Location: LCCOMB_X8_Y22_N26
\kb0|KB[0]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|KB[0]~2_combout\ = (!\kb0|num\(7) & \kb0|num\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(7),
	datad => \kb0|num\(5),
	combout => \kb0|KB[0]~2_combout\);

-- Location: LCCOMB_X9_Y22_N26
\kb0|Selector5~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Selector5~1_combout\ = (\kb0|num\(6) & (\kb0|num\(0))) # (!\kb0|num\(6) & ((!\kb0|num\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110110001101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(6),
	datab => \kb0|num\(0),
	datac => \kb0|num\(4),
	combout => \kb0|Selector5~1_combout\);

-- Location: LCCOMB_X4_Y22_N24
\slave0|counter[9]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter[9]~1_combout\ = (\slave0|counter\(0) & (\slave0|counter\(1) & (\slave0|counter\(3) & \slave0|counter\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(0),
	datab => \slave0|counter\(1),
	datac => \slave0|counter\(3),
	datad => \slave0|counter\(2),
	combout => \slave0|counter[9]~1_combout\);

-- Location: LCCOMB_X4_Y22_N10
\slave0|counter[11]~22\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter[11]~22_combout\ = (\kb0|KB\(2)) # (!\KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \KEY~combout\(0),
	datad => \kb0|KB\(2),
	combout => \slave0|counter[11]~22_combout\);

-- Location: LCFF_X7_Y22_N23
\kb0|dub\ : cycloneii_lcell_ff
PORT MAP (
	clk => \PS2_CLK~combout\,
	datain => \kb0|dub~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|dub~regout\);

-- Location: LCCOMB_X9_Y22_N0
\kb0|Decoder0~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Decoder0~6_combout\ = (!\kb0|in\(1) & (\kb0|in\(2) & (\kb0|in\(0) & \kb0|WideOr3~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|in\(1),
	datab => \kb0|in\(2),
	datac => \kb0|in\(0),
	datad => \kb0|WideOr3~0_combout\,
	combout => \kb0|Decoder0~6_combout\);

-- Location: LCCOMB_X9_Y22_N6
\kb0|num[5]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|num[5]~6_combout\ = (\kb0|Decoder0~6_combout\ & (\PS2_DAT~combout\)) # (!\kb0|Decoder0~6_combout\ & ((\kb0|num\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \PS2_DAT~combout\,
	datac => \kb0|num\(5),
	datad => \kb0|Decoder0~6_combout\,
	combout => \kb0|num[5]~6_combout\);

-- Location: LCCOMB_X10_Y22_N26
\kb0|dub~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|dub~0_combout\ = (!\kb0|i\(1) & (\kb0|i\(0) & \kb0|i\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kb0|i\(1),
	datac => \kb0|i\(0),
	datad => \kb0|i\(2),
	combout => \kb0|dub~0_combout\);

-- Location: LCCOMB_X7_Y22_N28
\kb0|dub~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|dub~1_combout\ = (\kb0|dub~regout\ & ((\kb0|i\(3)) # (!\kb0|dub~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kb0|dub~regout\,
	datac => \kb0|i\(3),
	datad => \kb0|dub~0_combout\,
	combout => \kb0|dub~1_combout\);

-- Location: LCCOMB_X7_Y22_N18
\kb0|always0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|always0~0_combout\ = (\kb0|num\(4) & (((\kb0|num\(2)) # (!\kb0|num\(3))) # (!\kb0|num\(0)))) # (!\kb0|num\(4) & (\kb0|num\(3) & (\kb0|num\(0) $ (\kb0|num\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101001101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(4),
	datab => \kb0|num\(0),
	datac => \kb0|num\(3),
	datad => \kb0|num\(2),
	combout => \kb0|always0~0_combout\);

-- Location: LCCOMB_X7_Y22_N12
\kb0|always0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|always0~1_combout\ = (\kb0|num\(0) & ((\kb0|num\(2) & (!\kb0|num\(4))) # (!\kb0|num\(2) & ((!\kb0|num\(3)))))) # (!\kb0|num\(0) & (\kb0|num\(4) & (\kb0|num\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101100000111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(4),
	datab => \kb0|num\(3),
	datac => \kb0|num\(0),
	datad => \kb0|num\(2),
	combout => \kb0|always0~1_combout\);

-- Location: LCCOMB_X7_Y22_N2
\kb0|always0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|always0~2_combout\ = (\kb0|num\(1) & (!\kb0|num\(2) & ((\kb0|num\(6)) # (!\kb0|always0~1_combout\)))) # (!\kb0|num\(1) & ((\kb0|num\(6) & ((!\kb0|always0~1_combout\))) # (!\kb0|num\(6) & (\kb0|num\(2) & \kb0|always0~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010010001110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(1),
	datab => \kb0|num\(2),
	datac => \kb0|num\(6),
	datad => \kb0|always0~1_combout\,
	combout => \kb0|always0~2_combout\);

-- Location: LCCOMB_X7_Y22_N16
\kb0|always0~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|always0~3_combout\ = (!\kb0|num\(6) & ((\kb0|num\(2) & (!\kb0|num\(1))) # (!\kb0|num\(2) & ((\kb0|always0~1_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(1),
	datab => \kb0|num\(2),
	datac => \kb0|num\(6),
	datad => \kb0|always0~1_combout\,
	combout => \kb0|always0~3_combout\);

-- Location: LCCOMB_X7_Y22_N20
\kb0|always0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|always0~4_combout\ = (\kb0|always0~0_combout\ & (\kb0|always0~2_combout\ & (\kb0|num\(5) $ (\kb0|always0~3_combout\)))) # (!\kb0|always0~0_combout\ & (\kb0|num\(5) & (\kb0|always0~3_combout\ & !\kb0|always0~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(5),
	datab => \kb0|always0~0_combout\,
	datac => \kb0|always0~3_combout\,
	datad => \kb0|always0~2_combout\,
	combout => \kb0|always0~4_combout\);

-- Location: LCCOMB_X10_Y22_N20
\kb0|dub~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|dub~2_combout\ = (!\kb0|i\(0) & (!\kb0|i\(3) & \kb0|i\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kb0|i\(0),
	datac => \kb0|i\(3),
	datad => \kb0|i\(2),
	combout => \kb0|dub~2_combout\);

-- Location: LCCOMB_X7_Y22_N26
\kb0|dub~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|dub~3_combout\ = (!\kb0|num\(7) & (\kb0|i\(1) & \kb0|dub~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(7),
	datac => \kb0|i\(1),
	datad => \kb0|dub~2_combout\,
	combout => \kb0|dub~3_combout\);

-- Location: LCCOMB_X7_Y22_N22
\kb0|dub~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|dub~4_combout\ = (\kb0|dub~1_combout\) # ((\kb0|always0~4_combout\ & \kb0|dub~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kb0|dub~1_combout\,
	datac => \kb0|always0~4_combout\,
	datad => \kb0|dub~3_combout\,
	combout => \kb0|dub~4_combout\);

-- Location: PIN_L1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\CLOCK_50~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_CLOCK_50,
	combout => \CLOCK_50~combout\);

-- Location: PIN_L22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(0),
	combout => \SW~combout\(0));

-- Location: PIN_L21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(1),
	combout => \SW~combout\(1));

-- Location: PIN_W12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(4),
	combout => \SW~combout\(4));

-- Location: PIN_U12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(5),
	combout => \SW~combout\(5));

-- Location: PIN_U11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(6),
	combout => \SW~combout\(6));

-- Location: PIN_M2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[7]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(7),
	combout => \SW~combout\(7));

-- Location: PIN_M1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[8]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(8),
	combout => \SW~combout\(8));

-- Location: PIN_H15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\PS2_CLK~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_PS2_CLK,
	combout => \PS2_CLK~combout\);

-- Location: CLKCTRL_G2
\CLOCK_50~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLOCK_50~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLOCK_50~clkctrl_outclk\);

-- Location: LCCOMB_X3_Y22_N16
\kb0|KP~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|KP~0_combout\ = !\kb0|KP~regout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kb0|KP~regout\,
	combout => \kb0|KP~0_combout\);

-- Location: LCFF_X3_Y22_N17
\kb0|KP\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kb0|dub~regout\,
	datain => \kb0|KP~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|KP~regout\);

-- Location: CLKCTRL_G1
\kb0|KP~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \kb0|KP~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \kb0|KP~clkctrl_outclk\);

-- Location: PIN_J14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\PS2_DAT~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_PS2_DAT,
	combout => \PS2_DAT~combout\);

-- Location: LCCOMB_X10_Y22_N14
\kb0|i~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|i~0_combout\ = ((!\kb0|i\(3) & (\kb0|i\(2) & !\kb0|i\(1)))) # (!\kb0|i\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111101001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|i\(3),
	datab => \kb0|i\(2),
	datac => \kb0|i\(0),
	datad => \kb0|i\(1),
	combout => \kb0|i~0_combout\);

-- Location: LCFF_X10_Y22_N15
\kb0|i[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \PS2_CLK~combout\,
	datain => \kb0|i~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|i\(0));

-- Location: LCCOMB_X10_Y22_N10
\kb0|i~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|i~2_combout\ = (\kb0|i\(1) & (((\kb0|i\(2))))) # (!\kb0|i\(1) & ((\kb0|i\(2) & ((\kb0|i\(0)) # (!\kb0|i\(3)))) # (!\kb0|i\(2) & ((!\kb0|i\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011010011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|i\(3),
	datab => \kb0|i\(1),
	datac => \kb0|i\(2),
	datad => \kb0|i\(0),
	combout => \kb0|i~2_combout\);

-- Location: LCFF_X10_Y22_N11
\kb0|i[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \PS2_CLK~combout\,
	datain => \kb0|i~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|i\(2));

-- Location: LCCOMB_X10_Y22_N28
\kb0|i~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|i~1_combout\ = (\kb0|i\(1) & (((\kb0|i\(0))))) # (!\kb0|i\(1) & (((!\kb0|i\(3) & \kb0|i\(2))) # (!\kb0|i\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|i\(3),
	datab => \kb0|i\(2),
	datac => \kb0|i\(1),
	datad => \kb0|i\(0),
	combout => \kb0|i~1_combout\);

-- Location: LCFF_X10_Y22_N29
\kb0|i[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \PS2_CLK~combout\,
	datain => \kb0|i~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|i\(1));

-- Location: LCCOMB_X10_Y22_N8
\kb0|i~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|i~3_combout\ = (\kb0|i\(1) & (((\kb0|i\(3))))) # (!\kb0|i\(1) & ((\kb0|i\(2)) # (\kb0|i\(0) $ (!\kb0|i\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|i\(0),
	datab => \kb0|i\(1),
	datac => \kb0|i\(3),
	datad => \kb0|i\(2),
	combout => \kb0|i~3_combout\);

-- Location: LCFF_X10_Y22_N9
\kb0|i[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \PS2_CLK~combout\,
	datain => \kb0|i~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|i\(3));

-- Location: LCCOMB_X10_Y22_N30
\kb0|in[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|in[2]~0_combout\ = ((\kb0|i\(1) $ (\kb0|i\(3))) # (!\kb0|i\(0))) # (!\kb0|i\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111110111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|i\(2),
	datab => \kb0|i\(1),
	datac => \kb0|i\(3),
	datad => \kb0|i\(0),
	combout => \kb0|in[2]~0_combout\);

-- Location: LCCOMB_X9_Y22_N10
\kb0|Selector1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Selector1~0_combout\ = (\kb0|in[2]~0_combout\ & (\kb0|in\(0) $ (\kb0|in\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|in\(0),
	datac => \kb0|in\(1),
	datad => \kb0|in[2]~0_combout\,
	combout => \kb0|Selector1~0_combout\);

-- Location: LCCOMB_X10_Y22_N4
\kb0|in[2]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|in[2]~1_combout\ = (\kb0|i\(3)) # ((\kb0|i\(2) & \kb0|i\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|i\(2),
	datac => \kb0|i\(3),
	datad => \kb0|i\(0),
	combout => \kb0|in[2]~1_combout\);

-- Location: LCFF_X9_Y22_N11
\kb0|in[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \PS2_CLK~combout\,
	datain => \kb0|Selector1~0_combout\,
	ena => \kb0|in[2]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|in\(1));

-- Location: LCCOMB_X10_Y22_N16
\kb0|Selector2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Selector2~0_combout\ = (!\kb0|in\(0) & \kb0|in[2]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kb0|in\(0),
	datad => \kb0|in[2]~0_combout\,
	combout => \kb0|Selector2~0_combout\);

-- Location: LCFF_X10_Y22_N17
\kb0|in[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \PS2_CLK~combout\,
	datain => \kb0|Selector2~0_combout\,
	ena => \kb0|in[2]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|in\(0));

-- Location: LCCOMB_X9_Y22_N30
\kb0|WideOr3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|WideOr3~0_combout\ = \kb0|i\(3) $ (((\kb0|i\(0) & (\kb0|i\(2) & \kb0|i\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|i\(3),
	datab => \kb0|i\(0),
	datac => \kb0|i\(2),
	datad => \kb0|i\(1),
	combout => \kb0|WideOr3~0_combout\);

-- Location: LCCOMB_X8_Y22_N6
\kb0|Decoder0~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Decoder0~3_combout\ = (!\kb0|in\(2) & (!\kb0|in\(1) & (!\kb0|in\(0) & \kb0|WideOr3~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|in\(2),
	datab => \kb0|in\(1),
	datac => \kb0|in\(0),
	datad => \kb0|WideOr3~0_combout\,
	combout => \kb0|Decoder0~3_combout\);

-- Location: LCCOMB_X7_Y22_N4
\kb0|num[0]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|num[0]~3_combout\ = (\kb0|Decoder0~3_combout\ & (\PS2_DAT~combout\)) # (!\kb0|Decoder0~3_combout\ & ((\kb0|num\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_DAT~combout\,
	datac => \kb0|num\(0),
	datad => \kb0|Decoder0~3_combout\,
	combout => \kb0|num[0]~3_combout\);

-- Location: LCFF_X7_Y22_N5
\kb0|num[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \PS2_CLK~combout\,
	datain => \kb0|num[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|num\(0));

-- Location: LCCOMB_X8_Y22_N2
\kb0|Decoder0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Decoder0~0_combout\ = (\kb0|in\(2) & (!\kb0|in\(1) & (!\kb0|in\(0) & \kb0|WideOr3~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|in\(2),
	datab => \kb0|in\(1),
	datac => \kb0|in\(0),
	datad => \kb0|WideOr3~0_combout\,
	combout => \kb0|Decoder0~0_combout\);

-- Location: LCCOMB_X7_Y22_N24
\kb0|num[4]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|num[4]~0_combout\ = (\kb0|Decoder0~0_combout\ & (\PS2_DAT~combout\)) # (!\kb0|Decoder0~0_combout\ & ((\kb0|num\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_DAT~combout\,
	datac => \kb0|num\(4),
	datad => \kb0|Decoder0~0_combout\,
	combout => \kb0|num[4]~0_combout\);

-- Location: LCFF_X7_Y22_N25
\kb0|num[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \PS2_CLK~combout\,
	datain => \kb0|num[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|num\(4));

-- Location: LCCOMB_X9_Y22_N12
\kb0|Selector7~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Selector7~0_combout\ = (\kb0|num\(4)) # (!\kb0|num\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(6),
	datad => \kb0|num\(4),
	combout => \kb0|Selector7~0_combout\);

-- Location: LCCOMB_X7_Y22_N30
\kb0|KB[0]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|KB[0]~0_combout\ = (\kb0|num\(0) & (\kb0|num\(3))) # (!\kb0|num\(0) & ((!\kb0|Selector7~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(3),
	datab => \kb0|num\(0),
	datad => \kb0|Selector7~0_combout\,
	combout => \kb0|KB[0]~0_combout\);

-- Location: LCCOMB_X8_Y22_N20
\kb0|Decoder0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Decoder0~2_combout\ = (!\kb0|in\(2) & (\kb0|in\(1) & (\kb0|in\(0) & \kb0|WideOr3~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|in\(2),
	datab => \kb0|in\(1),
	datac => \kb0|in\(0),
	datad => \kb0|WideOr3~0_combout\,
	combout => \kb0|Decoder0~2_combout\);

-- Location: LCCOMB_X7_Y22_N0
\kb0|num[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|num[3]~2_combout\ = (\kb0|Decoder0~2_combout\ & (\PS2_DAT~combout\)) # (!\kb0|Decoder0~2_combout\ & ((\kb0|num\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_DAT~combout\,
	datac => \kb0|num\(3),
	datad => \kb0|Decoder0~2_combout\,
	combout => \kb0|num[3]~2_combout\);

-- Location: LCFF_X7_Y22_N1
\kb0|num[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \PS2_CLK~combout\,
	datain => \kb0|num[3]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|num\(3));

-- Location: LCCOMB_X9_Y22_N4
\kb0|Selector0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Selector0~0_combout\ = (\kb0|in[2]~0_combout\ & (\kb0|in\(2) $ (((\kb0|in\(0) & \kb0|in\(1))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|in\(0),
	datab => \kb0|in[2]~0_combout\,
	datac => \kb0|in\(2),
	datad => \kb0|in\(1),
	combout => \kb0|Selector0~0_combout\);

-- Location: LCFF_X9_Y22_N5
\kb0|in[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \PS2_CLK~combout\,
	datain => \kb0|Selector0~0_combout\,
	ena => \kb0|in[2]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|in\(2));

-- Location: LCCOMB_X9_Y22_N18
\kb0|Decoder0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Decoder0~4_combout\ = (!\kb0|in\(1) & (!\kb0|in\(2) & (\kb0|in\(0) & \kb0|WideOr3~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|in\(1),
	datab => \kb0|in\(2),
	datac => \kb0|in\(0),
	datad => \kb0|WideOr3~0_combout\,
	combout => \kb0|Decoder0~4_combout\);

-- Location: LCCOMB_X8_Y22_N16
\kb0|num[1]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|num[1]~4_combout\ = (\kb0|Decoder0~4_combout\ & (\PS2_DAT~combout\)) # (!\kb0|Decoder0~4_combout\ & ((\kb0|num\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \PS2_DAT~combout\,
	datac => \kb0|num\(1),
	datad => \kb0|Decoder0~4_combout\,
	combout => \kb0|num[1]~4_combout\);

-- Location: LCFF_X8_Y22_N17
\kb0|num[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \PS2_CLK~combout\,
	datain => \kb0|num[1]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|num\(1));

-- Location: LCCOMB_X9_Y22_N22
\kb0|Selector7~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Selector7~1_combout\ = (\kb0|num\(4) & (((\kb0|num\(3))) # (!\kb0|num\(6)))) # (!\kb0|num\(4) & ((\kb0|num\(1) & (!\kb0|num\(6))) # (!\kb0|num\(1) & ((\kb0|num\(3))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010101110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(6),
	datab => \kb0|num\(1),
	datac => \kb0|num\(3),
	datad => \kb0|num\(4),
	combout => \kb0|Selector7~1_combout\);

-- Location: LCCOMB_X9_Y22_N16
\kb0|Selector7~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Selector7~2_combout\ = (\kb0|Selector7~1_combout\) # ((\kb0|num\(6) & (!\kb0|num\(3) & \kb0|num\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(6),
	datab => \kb0|num\(3),
	datac => \kb0|num\(0),
	datad => \kb0|Selector7~1_combout\,
	combout => \kb0|Selector7~2_combout\);

-- Location: LCCOMB_X8_Y22_N28
\kb0|Decoder0~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Decoder0~5_combout\ = (!\kb0|in\(2) & (\kb0|in\(1) & (!\kb0|in\(0) & \kb0|WideOr3~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|in\(2),
	datab => \kb0|in\(1),
	datac => \kb0|in\(0),
	datad => \kb0|WideOr3~0_combout\,
	combout => \kb0|Decoder0~5_combout\);

-- Location: LCCOMB_X7_Y22_N10
\kb0|num[2]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|num[2]~5_combout\ = (\kb0|Decoder0~5_combout\ & (\PS2_DAT~combout\)) # (!\kb0|Decoder0~5_combout\ & ((\kb0|num\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_DAT~combout\,
	datac => \kb0|num\(2),
	datad => \kb0|Decoder0~5_combout\,
	combout => \kb0|num[2]~5_combout\);

-- Location: LCFF_X7_Y22_N11
\kb0|num[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \PS2_CLK~combout\,
	datain => \kb0|num[2]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|num\(2));

-- Location: LCCOMB_X8_Y22_N30
\kb0|Decoder0~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Decoder0~7_combout\ = (\kb0|in\(2) & (\kb0|in\(1) & (\kb0|in\(0) & \kb0|WideOr3~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|in\(2),
	datab => \kb0|in\(1),
	datac => \kb0|in\(0),
	datad => \kb0|WideOr3~0_combout\,
	combout => \kb0|Decoder0~7_combout\);

-- Location: LCCOMB_X7_Y22_N8
\kb0|num[7]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|num[7]~7_combout\ = (\kb0|Decoder0~7_combout\ & (\PS2_DAT~combout\)) # (!\kb0|Decoder0~7_combout\ & ((\kb0|num\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_DAT~combout\,
	datac => \kb0|num\(7),
	datad => \kb0|Decoder0~7_combout\,
	combout => \kb0|num[7]~7_combout\);

-- Location: LCFF_X7_Y22_N9
\kb0|num[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \PS2_CLK~combout\,
	datain => \kb0|num[7]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|num\(7));

-- Location: LCCOMB_X7_Y22_N14
\kb0|KB[0]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|KB[0]~3_combout\ = (!\kb0|num\(7) & \kb0|num\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kb0|num\(7),
	datad => \kb0|num\(3),
	combout => \kb0|KB[0]~3_combout\);

-- Location: LCCOMB_X7_Y22_N6
\kb0|KB[0]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|KB[0]~4_combout\ = (\kb0|num\(4) & (((\kb0|num\(2)) # (!\kb0|KB[0]~3_combout\)) # (!\kb0|num\(0)))) # (!\kb0|num\(4) & (\kb0|KB[0]~3_combout\ & (\kb0|num\(0) $ (\kb0|num\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101001101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(4),
	datab => \kb0|num\(0),
	datac => \kb0|KB[0]~3_combout\,
	datad => \kb0|num\(2),
	combout => \kb0|KB[0]~4_combout\);

-- Location: LCCOMB_X9_Y22_N20
\kb0|Decoder0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Decoder0~1_combout\ = (\kb0|in\(1) & (\kb0|in\(2) & (!\kb0|in\(0) & \kb0|WideOr3~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|in\(1),
	datab => \kb0|in\(2),
	datac => \kb0|in\(0),
	datad => \kb0|WideOr3~0_combout\,
	combout => \kb0|Decoder0~1_combout\);

-- Location: LCCOMB_X8_Y22_N10
\kb0|num[6]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|num[6]~1_combout\ = (\kb0|Decoder0~1_combout\ & (\PS2_DAT~combout\)) # (!\kb0|Decoder0~1_combout\ & ((\kb0|num\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \PS2_DAT~combout\,
	datac => \kb0|num\(6),
	datad => \kb0|Decoder0~1_combout\,
	combout => \kb0|num[6]~1_combout\);

-- Location: LCFF_X8_Y22_N11
\kb0|num[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \PS2_CLK~combout\,
	datain => \kb0|num[6]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|num\(6));

-- Location: LCCOMB_X8_Y22_N0
\kb0|KB[0]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|KB[0]~5_combout\ = (\kb0|num\(0) & ((\kb0|num\(2) & (!\kb0|num\(4))) # (!\kb0|num\(2) & ((!\kb0|KB[0]~3_combout\))))) # (!\kb0|num\(0) & (((\kb0|num\(4) & \kb0|KB[0]~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010110001110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(2),
	datab => \kb0|num\(4),
	datac => \kb0|num\(0),
	datad => \kb0|KB[0]~3_combout\,
	combout => \kb0|KB[0]~5_combout\);

-- Location: LCCOMB_X8_Y22_N24
\kb0|KB[0]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|KB[0]~7_combout\ = (!\kb0|num\(6) & ((\kb0|num\(2) & (!\kb0|num\(1))) # (!\kb0|num\(2) & ((\kb0|KB[0]~5_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001100000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(2),
	datab => \kb0|num\(6),
	datac => \kb0|num\(1),
	datad => \kb0|KB[0]~5_combout\,
	combout => \kb0|KB[0]~7_combout\);

-- Location: LCCOMB_X8_Y22_N18
\kb0|KB[0]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|KB[0]~6_combout\ = (\kb0|num\(1) & (!\kb0|num\(2) & ((\kb0|num\(6)) # (!\kb0|KB[0]~5_combout\)))) # (!\kb0|num\(1) & ((\kb0|num\(6) & ((!\kb0|KB[0]~5_combout\))) # (!\kb0|num\(6) & (\kb0|num\(2) & \kb0|KB[0]~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100001001011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(2),
	datab => \kb0|num\(6),
	datac => \kb0|num\(1),
	datad => \kb0|KB[0]~5_combout\,
	combout => \kb0|KB[0]~6_combout\);

-- Location: LCCOMB_X8_Y22_N14
\kb0|KB[0]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|KB[0]~8_combout\ = (\kb0|KB[0]~4_combout\ & (\kb0|KB[0]~6_combout\ & (\kb0|KB[0]~2_combout\ $ (\kb0|KB[0]~7_combout\)))) # (!\kb0|KB[0]~4_combout\ & (\kb0|KB[0]~2_combout\ & (\kb0|KB[0]~7_combout\ & !\kb0|KB[0]~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|KB[0]~2_combout\,
	datab => \kb0|KB[0]~4_combout\,
	datac => \kb0|KB[0]~7_combout\,
	datad => \kb0|KB[0]~6_combout\,
	combout => \kb0|KB[0]~8_combout\);

-- Location: LCFF_X7_Y22_N31
\kb0|KB[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kb0|KP~clkctrl_outclk\,
	datain => \kb0|KB[0]~0_combout\,
	sdata => \kb0|Selector7~2_combout\,
	sload => \kb0|ALT_INV_num\(2),
	ena => \kb0|KB[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|KB\(0));

-- Location: LCCOMB_X9_Y22_N28
\kb0|Selector6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Selector6~0_combout\ = (\kb0|num\(6) & (!\kb0|num\(0) & (!\kb0|num\(3) & \kb0|num\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(6),
	datab => \kb0|num\(0),
	datac => \kb0|num\(3),
	datad => \kb0|num\(1),
	combout => \kb0|Selector6~0_combout\);

-- Location: LCCOMB_X9_Y22_N2
\kb0|Selector6~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Selector6~1_combout\ = (\kb0|Selector6~0_combout\) # ((\kb0|num\(6) & (\kb0|num\(3) & \kb0|num\(4))) # (!\kb0|num\(6) & ((\kb0|num\(3)) # (\kb0|num\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(6),
	datab => \kb0|num\(3),
	datac => \kb0|num\(4),
	datad => \kb0|Selector6~0_combout\,
	combout => \kb0|Selector6~1_combout\);

-- Location: LCCOMB_X8_Y22_N12
\kb0|KB[1]~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|KB[1]~feeder_combout\ = \kb0|Selector6~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \kb0|Selector6~1_combout\,
	combout => \kb0|KB[1]~feeder_combout\);

-- Location: LCCOMB_X9_Y22_N14
\kb0|num[0]~_wirecell\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|num[0]~_wirecell_combout\ = !\kb0|num\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kb0|num\(0),
	combout => \kb0|num[0]~_wirecell_combout\);

-- Location: LCFF_X8_Y22_N13
\kb0|KB[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kb0|KP~clkctrl_outclk\,
	datain => \kb0|KB[1]~feeder_combout\,
	sdata => \kb0|num[0]~_wirecell_combout\,
	sload => \kb0|num\(2),
	ena => \kb0|KB[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|KB\(1));

-- Location: LCCOMB_X9_Y22_N24
\kb0|Selector5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Selector5~0_combout\ = (!\kb0|num\(4) & ((\kb0|num\(1)) # (!\kb0|num\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011011101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(6),
	datab => \kb0|num\(1),
	datad => \kb0|num\(4),
	combout => \kb0|Selector5~0_combout\);

-- Location: LCCOMB_X8_Y22_N22
\kb0|KB[2]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|KB[2]~1_combout\ = (\kb0|num\(3) & ((\kb0|Selector5~0_combout\))) # (!\kb0|num\(3) & (\kb0|Selector5~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|Selector5~1_combout\,
	datab => \kb0|num\(3),
	datad => \kb0|Selector5~0_combout\,
	combout => \kb0|KB[2]~1_combout\);

-- Location: LCCOMB_X9_Y22_N8
\kb0|Selector5~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Selector5~2_combout\ = (!\kb0|num\(0) & ((\kb0|num\(5)) # (\kb0|num\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(5),
	datac => \kb0|num\(0),
	datad => \kb0|num\(6),
	combout => \kb0|Selector5~2_combout\);

-- Location: LCFF_X8_Y22_N23
\kb0|KB[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kb0|KP~clkctrl_outclk\,
	datain => \kb0|KB[2]~1_combout\,
	sdata => \kb0|Selector5~2_combout\,
	sload => \kb0|num\(2),
	ena => \kb0|KB[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|KB\(2));

-- Location: LCCOMB_X8_Y22_N4
\kb0|Selector4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Selector4~0_combout\ = (!\kb0|num\(6) & (((\kb0|num\(2)) # (!\kb0|num\(3))) # (!\kb0|num\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|num\(6),
	datab => \kb0|num\(4),
	datac => \kb0|num\(3),
	datad => \kb0|num\(2),
	combout => \kb0|Selector4~0_combout\);

-- Location: LCCOMB_X8_Y22_N8
\kb0|Selector4~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kb0|Selector4~1_combout\ = (\kb0|Selector4~0_combout\) # ((\kb0|num\(0) & \kb0|num\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kb0|num\(0),
	datac => \kb0|Selector4~0_combout\,
	datad => \kb0|num\(2),
	combout => \kb0|Selector4~1_combout\);

-- Location: LCFF_X8_Y22_N9
\kb0|KB[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kb0|KP~clkctrl_outclk\,
	datain => \kb0|Selector4~1_combout\,
	ena => \kb0|KB[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kb0|KB\(3));

-- Location: LCCOMB_X4_Y22_N26
\press_d0~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \press_d0~feeder_combout\ = \kb0|KP~regout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \kb0|KP~regout\,
	combout => \press_d0~feeder_combout\);

-- Location: LCFF_X4_Y22_N27
press_d0 : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \press_d0~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \press_d0~regout\);

-- Location: PIN_R22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(0),
	combout => \KEY~combout\(0));

-- Location: LCCOMB_X4_Y22_N20
\press_d1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \press_d1~0_combout\ = (\KEY~combout\(0) & ((\press_d0~regout\))) # (!\KEY~combout\(0) & (\kb0|KP~regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kb0|KP~regout\,
	datac => \KEY~combout\(0),
	datad => \press_d0~regout\,
	combout => \press_d1~0_combout\);

-- Location: LCFF_X4_Y22_N21
press_d1 : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \press_d1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \press_d1~regout\);

-- Location: LCCOMB_X4_Y22_N4
\signalPressed~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \signalPressed~0_combout\ = (!\press_d0~regout\ & \press_d1~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \press_d0~regout\,
	datac => \press_d1~regout\,
	combout => \signalPressed~0_combout\);

-- Location: PIN_V12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(3),
	combout => \SW~combout\(3));

-- Location: LCCOMB_X6_Y22_N0
\slave0|Add1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|Add1~0_combout\ = \slave0|counter\(0) $ (VCC)
-- \slave0|Add1~1\ = CARRY(\slave0|counter\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \slave0|counter\(0),
	datad => VCC,
	combout => \slave0|Add1~0_combout\,
	cout => \slave0|Add1~1\);

-- Location: LCCOMB_X4_Y22_N14
\slave0|counter~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter~6_combout\ = (\slave0|Add1~0_combout\ & ((\kb0|KB\(0)) # (!\slave0|counter[9]~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|KB\(0),
	datac => \slave0|counter[9]~5_combout\,
	datad => \slave0|Add1~0_combout\,
	combout => \slave0|counter~6_combout\);

-- Location: LCCOMB_X4_Y22_N22
\slave0|counter[9]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter[9]~0_combout\ = ((\kb0|KB\(1) & \kb0|KB\(2))) # (!\KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \KEY~combout\(0),
	datac => \kb0|KB\(1),
	datad => \kb0|KB\(2),
	combout => \slave0|counter[9]~0_combout\);

-- Location: LCCOMB_X4_Y22_N6
\slave0|counter~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter~7_combout\ = (!\slave0|counter[9]~0_combout\ & ((\kb0|KB\(2) & (\SW~combout\(0))) # (!\kb0|KB\(2) & ((\slave0|counter~6_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(0),
	datab => \kb0|KB\(2),
	datac => \slave0|counter~6_combout\,
	datad => \slave0|counter[9]~0_combout\,
	combout => \slave0|counter~7_combout\);

-- Location: LCCOMB_X4_Y22_N28
\slave0|counter[9]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter[9]~8_combout\ = (\kb0|KB\(3) & (\kb0|KB\(1) $ (((!\kb0|KB\(0) & \kb0|KB\(2))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|KB\(0),
	datab => \kb0|KB\(2),
	datac => \kb0|KB\(1),
	datad => \kb0|KB\(3),
	combout => \slave0|counter[9]~8_combout\);

-- Location: LCCOMB_X4_Y22_N30
\slave0|counter[9]~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter[9]~9_combout\ = ((\press_d1~regout\ & (!\press_d0~regout\ & \slave0|counter[9]~8_combout\))) # (!\KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \press_d1~regout\,
	datab => \press_d0~regout\,
	datac => \KEY~combout\(0),
	datad => \slave0|counter[9]~8_combout\,
	combout => \slave0|counter[9]~9_combout\);

-- Location: LCFF_X4_Y22_N7
\slave0|counter[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \slave0|counter~7_combout\,
	ena => \slave0|counter[9]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \slave0|counter\(0));

-- Location: LCCOMB_X6_Y22_N2
\slave0|Add1~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|Add1~2_combout\ = (\slave0|counter\(1) & ((\kb0|KB\(0) & (\slave0|Add1~1\ & VCC)) # (!\kb0|KB\(0) & (!\slave0|Add1~1\)))) # (!\slave0|counter\(1) & ((\kb0|KB\(0) & (!\slave0|Add1~1\)) # (!\kb0|KB\(0) & ((\slave0|Add1~1\) # (GND)))))
-- \slave0|Add1~3\ = CARRY((\slave0|counter\(1) & (!\kb0|KB\(0) & !\slave0|Add1~1\)) # (!\slave0|counter\(1) & ((!\slave0|Add1~1\) # (!\kb0|KB\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(1),
	datab => \kb0|KB\(0),
	datad => VCC,
	cin => \slave0|Add1~1\,
	combout => \slave0|Add1~2_combout\,
	cout => \slave0|Add1~3\);

-- Location: LCCOMB_X6_Y22_N4
\slave0|Add1~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|Add1~4_combout\ = ((\slave0|counter\(2) $ (\kb0|KB\(0) $ (!\slave0|Add1~3\)))) # (GND)
-- \slave0|Add1~5\ = CARRY((\slave0|counter\(2) & ((\kb0|KB\(0)) # (!\slave0|Add1~3\))) # (!\slave0|counter\(2) & (\kb0|KB\(0) & !\slave0|Add1~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(2),
	datab => \kb0|KB\(0),
	datad => VCC,
	cin => \slave0|Add1~3\,
	combout => \slave0|Add1~4_combout\,
	cout => \slave0|Add1~5\);

-- Location: LCCOMB_X6_Y22_N8
\slave0|Add1~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|Add1~8_combout\ = ((\slave0|counter\(4) $ (\kb0|KB\(0) $ (!\slave0|Add1~7\)))) # (GND)
-- \slave0|Add1~9\ = CARRY((\slave0|counter\(4) & ((\kb0|KB\(0)) # (!\slave0|Add1~7\))) # (!\slave0|counter\(4) & (\kb0|KB\(0) & !\slave0|Add1~7\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(4),
	datab => \kb0|KB\(0),
	datad => VCC,
	cin => \slave0|Add1~7\,
	combout => \slave0|Add1~8_combout\,
	cout => \slave0|Add1~9\);

-- Location: LCCOMB_X5_Y22_N14
\slave0|counter~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter~16_combout\ = (!\slave0|counter[9]~14_combout\ & ((\kb0|KB\(2) & (\SW~combout\(4))) # (!\kb0|KB\(2) & ((\slave0|Add1~8_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(4),
	datab => \kb0|KB\(2),
	datac => \slave0|Add1~8_combout\,
	datad => \slave0|counter[9]~14_combout\,
	combout => \slave0|counter~16_combout\);

-- Location: LCFF_X5_Y22_N15
\slave0|counter[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \slave0|counter~16_combout\,
	ena => \slave0|counter[9]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \slave0|counter\(4));

-- Location: LCCOMB_X6_Y22_N10
\slave0|Add1~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|Add1~10_combout\ = (\slave0|counter\(5) & ((\kb0|KB\(0) & (\slave0|Add1~9\ & VCC)) # (!\kb0|KB\(0) & (!\slave0|Add1~9\)))) # (!\slave0|counter\(5) & ((\kb0|KB\(0) & (!\slave0|Add1~9\)) # (!\kb0|KB\(0) & ((\slave0|Add1~9\) # (GND)))))
-- \slave0|Add1~11\ = CARRY((\slave0|counter\(5) & (!\kb0|KB\(0) & !\slave0|Add1~9\)) # (!\slave0|counter\(5) & ((!\slave0|Add1~9\) # (!\kb0|KB\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(5),
	datab => \kb0|KB\(0),
	datad => VCC,
	cin => \slave0|Add1~9\,
	combout => \slave0|Add1~10_combout\,
	cout => \slave0|Add1~11\);

-- Location: LCCOMB_X6_Y22_N12
\slave0|Add1~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|Add1~12_combout\ = ((\slave0|counter\(6) $ (\kb0|KB\(0) $ (!\slave0|Add1~11\)))) # (GND)
-- \slave0|Add1~13\ = CARRY((\slave0|counter\(6) & ((\kb0|KB\(0)) # (!\slave0|Add1~11\))) # (!\slave0|counter\(6) & (\kb0|KB\(0) & !\slave0|Add1~11\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(6),
	datab => \kb0|KB\(0),
	datad => VCC,
	cin => \slave0|Add1~11\,
	combout => \slave0|Add1~12_combout\,
	cout => \slave0|Add1~13\);

-- Location: LCCOMB_X5_Y22_N10
\slave0|counter~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter~18_combout\ = (!\slave0|counter[9]~14_combout\ & ((\kb0|KB\(2) & (\SW~combout\(6))) # (!\kb0|KB\(2) & ((\slave0|Add1~12_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(6),
	datab => \kb0|KB\(2),
	datac => \slave0|Add1~12_combout\,
	datad => \slave0|counter[9]~14_combout\,
	combout => \slave0|counter~18_combout\);

-- Location: LCFF_X5_Y22_N11
\slave0|counter[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \slave0|counter~18_combout\,
	ena => \slave0|counter[9]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \slave0|counter\(6));

-- Location: LCCOMB_X5_Y22_N8
\slave0|counter~17\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter~17_combout\ = (!\slave0|counter[9]~14_combout\ & ((\kb0|KB\(2) & (\SW~combout\(5))) # (!\kb0|KB\(2) & ((\slave0|Add1~10_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(5),
	datab => \kb0|KB\(2),
	datac => \slave0|Add1~10_combout\,
	datad => \slave0|counter[9]~14_combout\,
	combout => \slave0|counter~17_combout\);

-- Location: LCFF_X5_Y22_N9
\slave0|counter[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \slave0|counter~17_combout\,
	ena => \slave0|counter[9]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \slave0|counter\(5));

-- Location: LCCOMB_X4_Y22_N18
\slave0|counter[9]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter[9]~2_combout\ = (\slave0|counter\(7) & (\slave0|counter\(4) & (\slave0|counter\(6) & \slave0|counter\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(7),
	datab => \slave0|counter\(4),
	datac => \slave0|counter\(6),
	datad => \slave0|counter\(5),
	combout => \slave0|counter[9]~2_combout\);

-- Location: LCCOMB_X6_Y22_N14
\slave0|Add1~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|Add1~14_combout\ = (\slave0|counter\(7) & ((\kb0|KB\(0) & (\slave0|Add1~13\ & VCC)) # (!\kb0|KB\(0) & (!\slave0|Add1~13\)))) # (!\slave0|counter\(7) & ((\kb0|KB\(0) & (!\slave0|Add1~13\)) # (!\kb0|KB\(0) & ((\slave0|Add1~13\) # (GND)))))
-- \slave0|Add1~15\ = CARRY((\slave0|counter\(7) & (!\kb0|KB\(0) & !\slave0|Add1~13\)) # (!\slave0|counter\(7) & ((!\slave0|Add1~13\) # (!\kb0|KB\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(7),
	datab => \kb0|KB\(0),
	datad => VCC,
	cin => \slave0|Add1~13\,
	combout => \slave0|Add1~14_combout\,
	cout => \slave0|Add1~15\);

-- Location: LCCOMB_X6_Y22_N16
\slave0|Add1~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|Add1~16_combout\ = ((\kb0|KB\(0) $ (\slave0|counter\(8) $ (!\slave0|Add1~15\)))) # (GND)
-- \slave0|Add1~17\ = CARRY((\kb0|KB\(0) & ((\slave0|counter\(8)) # (!\slave0|Add1~15\))) # (!\kb0|KB\(0) & (\slave0|counter\(8) & !\slave0|Add1~15\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|KB\(0),
	datab => \slave0|counter\(8),
	datad => VCC,
	cin => \slave0|Add1~15\,
	combout => \slave0|Add1~16_combout\,
	cout => \slave0|Add1~17\);

-- Location: LCCOMB_X5_Y22_N22
\slave0|counter~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter~20_combout\ = (!\slave0|counter[9]~14_combout\ & ((\kb0|KB\(2) & (\SW~combout\(8))) # (!\kb0|KB\(2) & ((\slave0|Add1~16_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(8),
	datab => \kb0|KB\(2),
	datac => \slave0|Add1~16_combout\,
	datad => \slave0|counter[9]~14_combout\,
	combout => \slave0|counter~20_combout\);

-- Location: LCFF_X5_Y22_N23
\slave0|counter[8]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \slave0|counter~20_combout\,
	ena => \slave0|counter[9]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \slave0|counter\(8));

-- Location: LCCOMB_X6_Y22_N18
\slave0|Add1~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|Add1~18_combout\ = (\kb0|KB\(0) & ((\slave0|counter\(9) & (\slave0|Add1~17\ & VCC)) # (!\slave0|counter\(9) & (!\slave0|Add1~17\)))) # (!\kb0|KB\(0) & ((\slave0|counter\(9) & (!\slave0|Add1~17\)) # (!\slave0|counter\(9) & ((\slave0|Add1~17\) # 
-- (GND)))))
-- \slave0|Add1~19\ = CARRY((\kb0|KB\(0) & (!\slave0|counter\(9) & !\slave0|Add1~17\)) # (!\kb0|KB\(0) & ((!\slave0|Add1~17\) # (!\slave0|counter\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|KB\(0),
	datab => \slave0|counter\(9),
	datad => VCC,
	cin => \slave0|Add1~17\,
	combout => \slave0|Add1~18_combout\,
	cout => \slave0|Add1~19\);

-- Location: PIN_L2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[9]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(9),
	combout => \SW~combout\(9));

-- Location: LCCOMB_X5_Y22_N28
\slave0|counter~21\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter~21_combout\ = (!\slave0|counter[9]~14_combout\ & ((\kb0|KB\(2) & ((\SW~combout\(9)))) # (!\kb0|KB\(2) & (\slave0|Add1~18_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|KB\(2),
	datab => \slave0|Add1~18_combout\,
	datac => \SW~combout\(9),
	datad => \slave0|counter[9]~14_combout\,
	combout => \slave0|counter~21_combout\);

-- Location: LCFF_X5_Y22_N29
\slave0|counter[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \slave0|counter~21_combout\,
	ena => \slave0|counter[9]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \slave0|counter\(9));

-- Location: LCCOMB_X6_Y22_N20
\slave0|Add1~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|Add1~20_combout\ = ((\kb0|KB\(0) $ (\slave0|counter\(10) $ (!\slave0|Add1~19\)))) # (GND)
-- \slave0|Add1~21\ = CARRY((\kb0|KB\(0) & ((\slave0|counter\(10)) # (!\slave0|Add1~19\))) # (!\kb0|KB\(0) & (\slave0|counter\(10) & !\slave0|Add1~19\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|KB\(0),
	datab => \slave0|counter\(10),
	datad => VCC,
	cin => \slave0|Add1~19\,
	combout => \slave0|Add1~20_combout\,
	cout => \slave0|Add1~21\);

-- Location: LCCOMB_X5_Y22_N26
\slave0|counter~23\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter~23_combout\ = (!\slave0|counter[11]~22_combout\ & (\slave0|Add1~20_combout\ & ((\kb0|KB\(0)) # (!\slave0|counter[9]~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter[11]~22_combout\,
	datab => \slave0|counter[9]~5_combout\,
	datac => \kb0|KB\(0),
	datad => \slave0|Add1~20_combout\,
	combout => \slave0|counter~23_combout\);

-- Location: LCFF_X5_Y22_N27
\slave0|counter[10]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \slave0|counter~23_combout\,
	ena => \slave0|counter[9]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \slave0|counter\(10));

-- Location: LCCOMB_X4_Y22_N8
\slave0|counter[9]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter[9]~3_combout\ = (\slave0|counter\(11) & (\slave0|counter\(9) & (\slave0|counter\(10) & \slave0|counter\(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(11),
	datab => \slave0|counter\(9),
	datac => \slave0|counter\(10),
	datad => \slave0|counter\(8),
	combout => \slave0|counter[9]~3_combout\);

-- Location: LCCOMB_X6_Y22_N22
\slave0|Add1~22\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|Add1~22_combout\ = (\kb0|KB\(0) & ((\slave0|counter\(11) & (\slave0|Add1~21\ & VCC)) # (!\slave0|counter\(11) & (!\slave0|Add1~21\)))) # (!\kb0|KB\(0) & ((\slave0|counter\(11) & (!\slave0|Add1~21\)) # (!\slave0|counter\(11) & ((\slave0|Add1~21\) # 
-- (GND)))))
-- \slave0|Add1~23\ = CARRY((\kb0|KB\(0) & (!\slave0|counter\(11) & !\slave0|Add1~21\)) # (!\kb0|KB\(0) & ((!\slave0|Add1~21\) # (!\slave0|counter\(11)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|KB\(0),
	datab => \slave0|counter\(11),
	datad => VCC,
	cin => \slave0|Add1~21\,
	combout => \slave0|Add1~22_combout\,
	cout => \slave0|Add1~23\);

-- Location: LCCOMB_X5_Y22_N16
\slave0|counter~24\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter~24_combout\ = (!\slave0|counter[11]~22_combout\ & (\slave0|Add1~22_combout\ & ((\kb0|KB\(0)) # (!\slave0|counter[9]~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter[11]~22_combout\,
	datab => \slave0|counter[9]~5_combout\,
	datac => \kb0|KB\(0),
	datad => \slave0|Add1~22_combout\,
	combout => \slave0|counter~24_combout\);

-- Location: LCFF_X5_Y22_N17
\slave0|counter[11]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \slave0|counter~24_combout\,
	ena => \slave0|counter[9]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \slave0|counter\(11));

-- Location: LCCOMB_X6_Y22_N24
\slave0|Add1~24\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|Add1~24_combout\ = ((\kb0|KB\(0) $ (\slave0|counter\(12) $ (!\slave0|Add1~23\)))) # (GND)
-- \slave0|Add1~25\ = CARRY((\kb0|KB\(0) & ((\slave0|counter\(12)) # (!\slave0|Add1~23\))) # (!\kb0|KB\(0) & (\slave0|counter\(12) & !\slave0|Add1~23\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|KB\(0),
	datab => \slave0|counter\(12),
	datad => VCC,
	cin => \slave0|Add1~23\,
	combout => \slave0|Add1~24_combout\,
	cout => \slave0|Add1~25\);

-- Location: LCCOMB_X5_Y22_N18
\slave0|counter~25\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter~25_combout\ = (!\slave0|counter[11]~22_combout\ & (\slave0|Add1~24_combout\ & ((\kb0|KB\(0)) # (!\slave0|counter[9]~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter[11]~22_combout\,
	datab => \slave0|counter[9]~5_combout\,
	datac => \kb0|KB\(0),
	datad => \slave0|Add1~24_combout\,
	combout => \slave0|counter~25_combout\);

-- Location: LCFF_X5_Y22_N19
\slave0|counter[12]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \slave0|counter~25_combout\,
	ena => \slave0|counter[9]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \slave0|counter\(12));

-- Location: LCCOMB_X6_Y22_N26
\slave0|Add1~26\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|Add1~26_combout\ = (\kb0|KB\(0) & ((\slave0|counter\(13) & (\slave0|Add1~25\ & VCC)) # (!\slave0|counter\(13) & (!\slave0|Add1~25\)))) # (!\kb0|KB\(0) & ((\slave0|counter\(13) & (!\slave0|Add1~25\)) # (!\slave0|counter\(13) & ((\slave0|Add1~25\) # 
-- (GND)))))
-- \slave0|Add1~27\ = CARRY((\kb0|KB\(0) & (!\slave0|counter\(13) & !\slave0|Add1~25\)) # (!\kb0|KB\(0) & ((!\slave0|Add1~25\) # (!\slave0|counter\(13)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|KB\(0),
	datab => \slave0|counter\(13),
	datad => VCC,
	cin => \slave0|Add1~25\,
	combout => \slave0|Add1~26_combout\,
	cout => \slave0|Add1~27\);

-- Location: LCCOMB_X5_Y22_N24
\slave0|counter~26\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter~26_combout\ = (!\slave0|counter[11]~22_combout\ & (\slave0|Add1~26_combout\ & ((\kb0|KB\(0)) # (!\slave0|counter[9]~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter[11]~22_combout\,
	datab => \kb0|KB\(0),
	datac => \slave0|Add1~26_combout\,
	datad => \slave0|counter[9]~5_combout\,
	combout => \slave0|counter~26_combout\);

-- Location: LCFF_X5_Y22_N25
\slave0|counter[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \slave0|counter~26_combout\,
	ena => \slave0|counter[9]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \slave0|counter\(13));

-- Location: LCCOMB_X6_Y22_N28
\slave0|Add1~28\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|Add1~28_combout\ = ((\kb0|KB\(0) $ (\slave0|counter\(14) $ (!\slave0|Add1~27\)))) # (GND)
-- \slave0|Add1~29\ = CARRY((\kb0|KB\(0) & ((\slave0|counter\(14)) # (!\slave0|Add1~27\))) # (!\kb0|KB\(0) & (\slave0|counter\(14) & !\slave0|Add1~27\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|KB\(0),
	datab => \slave0|counter\(14),
	datad => VCC,
	cin => \slave0|Add1~27\,
	combout => \slave0|Add1~28_combout\,
	cout => \slave0|Add1~29\);

-- Location: LCCOMB_X4_Y22_N0
\slave0|counter~27\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter~27_combout\ = (!\slave0|counter[11]~22_combout\ & (\slave0|Add1~28_combout\ & ((\kb0|KB\(0)) # (!\slave0|counter[9]~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter[11]~22_combout\,
	datab => \kb0|KB\(0),
	datac => \slave0|counter[9]~5_combout\,
	datad => \slave0|Add1~28_combout\,
	combout => \slave0|counter~27_combout\);

-- Location: LCFF_X4_Y22_N1
\slave0|counter[14]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \slave0|counter~27_combout\,
	ena => \slave0|counter[9]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \slave0|counter\(14));

-- Location: LCCOMB_X4_Y22_N2
\slave0|counter[9]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter[9]~4_combout\ = (\slave0|counter\(15) & (\slave0|counter\(12) & (\slave0|counter\(14) & \slave0|counter\(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(15),
	datab => \slave0|counter\(12),
	datac => \slave0|counter\(14),
	datad => \slave0|counter\(13),
	combout => \slave0|counter[9]~4_combout\);

-- Location: LCCOMB_X4_Y22_N16
\slave0|counter[9]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter[9]~5_combout\ = (\slave0|counter[9]~1_combout\ & (\slave0|counter[9]~2_combout\ & (\slave0|counter[9]~3_combout\ & \slave0|counter[9]~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter[9]~1_combout\,
	datab => \slave0|counter[9]~2_combout\,
	datac => \slave0|counter[9]~3_combout\,
	datad => \slave0|counter[9]~4_combout\,
	combout => \slave0|counter[9]~5_combout\);

-- Location: LCCOMB_X4_Y22_N12
\slave0|counter[9]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter[9]~14_combout\ = (\slave0|counter[9]~0_combout\) # ((!\kb0|KB\(0) & (!\kb0|KB\(2) & \slave0|counter[9]~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|KB\(0),
	datab => \kb0|KB\(2),
	datac => \slave0|counter[9]~5_combout\,
	datad => \slave0|counter[9]~0_combout\,
	combout => \slave0|counter[9]~14_combout\);

-- Location: LCCOMB_X5_Y22_N12
\slave0|counter~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter~15_combout\ = (!\slave0|counter[9]~14_combout\ & ((\kb0|KB\(2) & ((\SW~combout\(3)))) # (!\kb0|KB\(2) & (\slave0|Add1~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|Add1~6_combout\,
	datab => \kb0|KB\(2),
	datac => \SW~combout\(3),
	datad => \slave0|counter[9]~14_combout\,
	combout => \slave0|counter~15_combout\);

-- Location: LCFF_X5_Y22_N13
\slave0|counter[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \slave0|counter~15_combout\,
	ena => \slave0|counter[9]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \slave0|counter\(3));

-- Location: PIN_M22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(2),
	combout => \SW~combout\(2));

-- Location: LCCOMB_X5_Y22_N2
\slave0|counter~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter~12_combout\ = (\slave0|Add1~4_combout\ & ((\kb0|KB\(0)) # (!\slave0|counter[9]~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|KB\(0),
	datab => \slave0|counter[9]~5_combout\,
	datad => \slave0|Add1~4_combout\,
	combout => \slave0|counter~12_combout\);

-- Location: LCCOMB_X5_Y22_N30
\slave0|counter~13\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter~13_combout\ = (!\slave0|counter[9]~0_combout\ & ((\kb0|KB\(2) & (\SW~combout\(2))) # (!\kb0|KB\(2) & ((\slave0|counter~12_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|KB\(2),
	datab => \slave0|counter[9]~0_combout\,
	datac => \SW~combout\(2),
	datad => \slave0|counter~12_combout\,
	combout => \slave0|counter~13_combout\);

-- Location: LCFF_X5_Y22_N31
\slave0|counter[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \slave0|counter~13_combout\,
	ena => \slave0|counter[9]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \slave0|counter\(2));

-- Location: LCCOMB_X5_Y22_N20
\slave0|counter~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter~10_combout\ = (\slave0|Add1~2_combout\ & ((\kb0|KB\(0)) # (!\slave0|counter[9]~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kb0|KB\(0),
	datab => \slave0|counter[9]~5_combout\,
	datad => \slave0|Add1~2_combout\,
	combout => \slave0|counter~10_combout\);

-- Location: LCCOMB_X5_Y22_N0
\slave0|counter~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter~11_combout\ = (!\slave0|counter[9]~0_combout\ & ((\kb0|KB\(2) & (\SW~combout\(1))) # (!\kb0|KB\(2) & ((\slave0|counter~10_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(1),
	datab => \kb0|KB\(2),
	datac => \slave0|counter~10_combout\,
	datad => \slave0|counter[9]~0_combout\,
	combout => \slave0|counter~11_combout\);

-- Location: LCFF_X5_Y22_N1
\slave0|counter[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \slave0|counter~11_combout\,
	ena => \slave0|counter[9]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \slave0|counter\(1));

-- Location: LCCOMB_X3_Y22_N30
\dec0|WideOr6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr6~0_combout\ = (\slave0|counter\(3) & (\slave0|counter\(0) & (\slave0|counter\(2) $ (\slave0|counter\(1))))) # (!\slave0|counter\(3) & (!\slave0|counter\(1) & (\slave0|counter\(2) $ (\slave0|counter\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(3),
	datab => \slave0|counter\(2),
	datac => \slave0|counter\(1),
	datad => \slave0|counter\(0),
	combout => \dec0|WideOr6~0_combout\);

-- Location: LCCOMB_X3_Y22_N18
\dec0|WideOr5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr5~0_combout\ = (\slave0|counter\(3) & ((\slave0|counter\(0) & ((\slave0|counter\(1)))) # (!\slave0|counter\(0) & (\slave0|counter\(2))))) # (!\slave0|counter\(3) & (\slave0|counter\(2) & (\slave0|counter\(1) $ (\slave0|counter\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(3),
	datab => \slave0|counter\(2),
	datac => \slave0|counter\(1),
	datad => \slave0|counter\(0),
	combout => \dec0|WideOr5~0_combout\);

-- Location: LCCOMB_X3_Y22_N14
\dec0|WideOr4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr4~0_combout\ = (\slave0|counter\(3) & (\slave0|counter\(2) & ((\slave0|counter\(1)) # (!\slave0|counter\(0))))) # (!\slave0|counter\(3) & (!\slave0|counter\(2) & (\slave0|counter\(1) & !\slave0|counter\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(3),
	datab => \slave0|counter\(2),
	datac => \slave0|counter\(1),
	datad => \slave0|counter\(0),
	combout => \dec0|WideOr4~0_combout\);

-- Location: LCCOMB_X3_Y22_N12
\dec0|WideOr3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr3~0_combout\ = (\slave0|counter\(1) & ((\slave0|counter\(2) & ((\slave0|counter\(0)))) # (!\slave0|counter\(2) & (\slave0|counter\(3) & !\slave0|counter\(0))))) # (!\slave0|counter\(1) & (!\slave0|counter\(3) & (\slave0|counter\(2) $ 
-- (\slave0|counter\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000100100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(3),
	datab => \slave0|counter\(2),
	datac => \slave0|counter\(1),
	datad => \slave0|counter\(0),
	combout => \dec0|WideOr3~0_combout\);

-- Location: LCCOMB_X3_Y22_N10
\dec0|WideOr2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr2~0_combout\ = (\slave0|counter\(1) & (!\slave0|counter\(3) & ((\slave0|counter\(0))))) # (!\slave0|counter\(1) & ((\slave0|counter\(2) & (!\slave0|counter\(3))) # (!\slave0|counter\(2) & ((\slave0|counter\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101011100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(3),
	datab => \slave0|counter\(2),
	datac => \slave0|counter\(1),
	datad => \slave0|counter\(0),
	combout => \dec0|WideOr2~0_combout\);

-- Location: LCCOMB_X3_Y22_N22
\dec0|WideOr1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr1~0_combout\ = (\slave0|counter\(2) & (\slave0|counter\(0) & (\slave0|counter\(3) $ (\slave0|counter\(1))))) # (!\slave0|counter\(2) & (!\slave0|counter\(3) & ((\slave0|counter\(1)) # (\slave0|counter\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101100100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(3),
	datab => \slave0|counter\(2),
	datac => \slave0|counter\(1),
	datad => \slave0|counter\(0),
	combout => \dec0|WideOr1~0_combout\);

-- Location: LCCOMB_X3_Y22_N8
\dec0|WideOr0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec0|WideOr0~0_combout\ = (\slave0|counter\(0) & ((\slave0|counter\(3)) # (\slave0|counter\(2) $ (\slave0|counter\(1))))) # (!\slave0|counter\(0) & ((\slave0|counter\(1)) # (\slave0|counter\(3) $ (\slave0|counter\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111011110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(3),
	datab => \slave0|counter\(2),
	datac => \slave0|counter\(1),
	datad => \slave0|counter\(0),
	combout => \dec0|WideOr0~0_combout\);

-- Location: LCCOMB_X5_Y22_N4
\slave0|counter~19\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter~19_combout\ = (!\slave0|counter[9]~14_combout\ & ((\kb0|KB\(2) & (\SW~combout\(7))) # (!\kb0|KB\(2) & ((\slave0|Add1~14_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(7),
	datab => \kb0|KB\(2),
	datac => \slave0|Add1~14_combout\,
	datad => \slave0|counter[9]~14_combout\,
	combout => \slave0|counter~19_combout\);

-- Location: LCFF_X5_Y22_N5
\slave0|counter[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \slave0|counter~19_combout\,
	ena => \slave0|counter[9]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \slave0|counter\(7));

-- Location: LCCOMB_X1_Y22_N12
\dec1|WideOr6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr6~0_combout\ = (\slave0|counter\(7) & (\slave0|counter\(4) & (\slave0|counter\(5) $ (\slave0|counter\(6))))) # (!\slave0|counter\(7) & (!\slave0|counter\(5) & (\slave0|counter\(4) $ (\slave0|counter\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(5),
	datab => \slave0|counter\(7),
	datac => \slave0|counter\(4),
	datad => \slave0|counter\(6),
	combout => \dec1|WideOr6~0_combout\);

-- Location: LCCOMB_X1_Y22_N2
\dec1|WideOr5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr5~0_combout\ = (\slave0|counter\(5) & ((\slave0|counter\(4) & (\slave0|counter\(7))) # (!\slave0|counter\(4) & ((\slave0|counter\(6)))))) # (!\slave0|counter\(5) & (\slave0|counter\(6) & (\slave0|counter\(7) $ (\slave0|counter\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001111010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(5),
	datab => \slave0|counter\(7),
	datac => \slave0|counter\(4),
	datad => \slave0|counter\(6),
	combout => \dec1|WideOr5~0_combout\);

-- Location: LCCOMB_X1_Y22_N28
\dec1|WideOr4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr4~0_combout\ = (\slave0|counter\(7) & (\slave0|counter\(6) & ((\slave0|counter\(5)) # (!\slave0|counter\(4))))) # (!\slave0|counter\(7) & (\slave0|counter\(5) & (!\slave0|counter\(4) & !\slave0|counter\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(5),
	datab => \slave0|counter\(7),
	datac => \slave0|counter\(4),
	datad => \slave0|counter\(6),
	combout => \dec1|WideOr4~0_combout\);

-- Location: LCCOMB_X1_Y22_N22
\dec1|WideOr3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr3~0_combout\ = (\slave0|counter\(5) & ((\slave0|counter\(4) & ((\slave0|counter\(6)))) # (!\slave0|counter\(4) & (\slave0|counter\(7) & !\slave0|counter\(6))))) # (!\slave0|counter\(5) & (!\slave0|counter\(7) & (\slave0|counter\(4) $ 
-- (\slave0|counter\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000100011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(5),
	datab => \slave0|counter\(7),
	datac => \slave0|counter\(4),
	datad => \slave0|counter\(6),
	combout => \dec1|WideOr3~0_combout\);

-- Location: LCCOMB_X1_Y22_N16
\dec1|WideOr2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr2~0_combout\ = (\slave0|counter\(5) & (!\slave0|counter\(7) & (\slave0|counter\(4)))) # (!\slave0|counter\(5) & ((\slave0|counter\(6) & (!\slave0|counter\(7))) # (!\slave0|counter\(6) & ((\slave0|counter\(4))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000101110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(5),
	datab => \slave0|counter\(7),
	datac => \slave0|counter\(4),
	datad => \slave0|counter\(6),
	combout => \dec1|WideOr2~0_combout\);

-- Location: LCCOMB_X1_Y22_N26
\dec1|WideOr1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr1~0_combout\ = (\slave0|counter\(5) & (!\slave0|counter\(7) & ((\slave0|counter\(4)) # (!\slave0|counter\(6))))) # (!\slave0|counter\(5) & (\slave0|counter\(4) & (\slave0|counter\(7) $ (!\slave0|counter\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000000110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(5),
	datab => \slave0|counter\(7),
	datac => \slave0|counter\(4),
	datad => \slave0|counter\(6),
	combout => \dec1|WideOr1~0_combout\);

-- Location: LCCOMB_X1_Y22_N20
\dec1|WideOr0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec1|WideOr0~0_combout\ = (\slave0|counter\(4) & ((\slave0|counter\(7)) # (\slave0|counter\(5) $ (\slave0|counter\(6))))) # (!\slave0|counter\(4) & ((\slave0|counter\(5)) # (\slave0|counter\(7) $ (\slave0|counter\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(5),
	datab => \slave0|counter\(7),
	datac => \slave0|counter\(4),
	datad => \slave0|counter\(6),
	combout => \dec1|WideOr0~0_combout\);

-- Location: LCCOMB_X1_Y22_N10
\dec2|WideOr6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr6~0_combout\ = (\slave0|counter\(11) & (\slave0|counter\(8) & (\slave0|counter\(10) $ (\slave0|counter\(9))))) # (!\slave0|counter\(11) & (!\slave0|counter\(9) & (\slave0|counter\(10) $ (\slave0|counter\(8)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(11),
	datab => \slave0|counter\(10),
	datac => \slave0|counter\(9),
	datad => \slave0|counter\(8),
	combout => \dec2|WideOr6~0_combout\);

-- Location: LCCOMB_X1_Y23_N20
\dec2|WideOr5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr5~0_combout\ = (\slave0|counter\(9) & ((\slave0|counter\(8) & ((\slave0|counter\(11)))) # (!\slave0|counter\(8) & (\slave0|counter\(10))))) # (!\slave0|counter\(9) & (\slave0|counter\(10) & (\slave0|counter\(8) $ (\slave0|counter\(11)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101000101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(10),
	datab => \slave0|counter\(9),
	datac => \slave0|counter\(8),
	datad => \slave0|counter\(11),
	combout => \dec2|WideOr5~0_combout\);

-- Location: LCCOMB_X1_Y23_N26
\dec2|WideOr4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr4~0_combout\ = (\slave0|counter\(10) & (\slave0|counter\(11) & ((\slave0|counter\(9)) # (!\slave0|counter\(8))))) # (!\slave0|counter\(10) & (\slave0|counter\(9) & (!\slave0|counter\(8) & !\slave0|counter\(11))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(10),
	datab => \slave0|counter\(9),
	datac => \slave0|counter\(8),
	datad => \slave0|counter\(11),
	combout => \dec2|WideOr4~0_combout\);

-- Location: LCCOMB_X1_Y23_N12
\dec2|WideOr3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr3~0_combout\ = (\slave0|counter\(9) & ((\slave0|counter\(10) & (\slave0|counter\(8))) # (!\slave0|counter\(10) & (!\slave0|counter\(8) & \slave0|counter\(11))))) # (!\slave0|counter\(9) & (!\slave0|counter\(11) & (\slave0|counter\(10) $ 
-- (\slave0|counter\(8)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010010010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(10),
	datab => \slave0|counter\(9),
	datac => \slave0|counter\(8),
	datad => \slave0|counter\(11),
	combout => \dec2|WideOr3~0_combout\);

-- Location: LCCOMB_X1_Y23_N22
\dec2|WideOr2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr2~0_combout\ = (\slave0|counter\(9) & (((\slave0|counter\(8) & !\slave0|counter\(11))))) # (!\slave0|counter\(9) & ((\slave0|counter\(10) & ((!\slave0|counter\(11)))) # (!\slave0|counter\(10) & (\slave0|counter\(8)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000011110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(10),
	datab => \slave0|counter\(9),
	datac => \slave0|counter\(8),
	datad => \slave0|counter\(11),
	combout => \dec2|WideOr2~0_combout\);

-- Location: LCCOMB_X1_Y23_N0
\dec2|WideOr1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr1~0_combout\ = (\slave0|counter\(10) & (\slave0|counter\(8) & (\slave0|counter\(9) $ (\slave0|counter\(11))))) # (!\slave0|counter\(10) & (!\slave0|counter\(11) & ((\slave0|counter\(9)) # (\slave0|counter\(8)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000011010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(10),
	datab => \slave0|counter\(9),
	datac => \slave0|counter\(8),
	datad => \slave0|counter\(11),
	combout => \dec2|WideOr1~0_combout\);

-- Location: LCCOMB_X1_Y23_N2
\dec2|WideOr0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec2|WideOr0~0_combout\ = (\slave0|counter\(8) & ((\slave0|counter\(11)) # (\slave0|counter\(10) $ (\slave0|counter\(9))))) # (!\slave0|counter\(8) & ((\slave0|counter\(9)) # (\slave0|counter\(10) $ (\slave0|counter\(11)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110101101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(10),
	datab => \slave0|counter\(9),
	datac => \slave0|counter\(8),
	datad => \slave0|counter\(11),
	combout => \dec2|WideOr0~0_combout\);

-- Location: LCCOMB_X6_Y22_N30
\slave0|Add1~30\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|Add1~30_combout\ = \slave0|counter\(15) $ (\slave0|Add1~29\ $ (\kb0|KB\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(15),
	datad => \kb0|KB\(0),
	cin => \slave0|Add1~29\,
	combout => \slave0|Add1~30_combout\);

-- Location: LCCOMB_X5_Y22_N6
\slave0|counter~28\ : cycloneii_lcell_comb
-- Equation(s):
-- \slave0|counter~28_combout\ = (!\slave0|counter[11]~22_combout\ & (\slave0|Add1~30_combout\ & ((\kb0|KB\(0)) # (!\slave0|counter[9]~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter[11]~22_combout\,
	datab => \slave0|counter[9]~5_combout\,
	datac => \kb0|KB\(0),
	datad => \slave0|Add1~30_combout\,
	combout => \slave0|counter~28_combout\);

-- Location: LCFF_X5_Y22_N7
\slave0|counter[15]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_50~clkctrl_outclk\,
	datain => \slave0|counter~28_combout\,
	ena => \slave0|counter[9]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \slave0|counter\(15));

-- Location: LCCOMB_X3_Y22_N26
\dec3|WideOr6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr6~0_combout\ = (\slave0|counter\(15) & (\slave0|counter\(12) & (\slave0|counter\(13) $ (\slave0|counter\(14))))) # (!\slave0|counter\(15) & (!\slave0|counter\(13) & (\slave0|counter\(14) $ (\slave0|counter\(12)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(13),
	datab => \slave0|counter\(15),
	datac => \slave0|counter\(14),
	datad => \slave0|counter\(12),
	combout => \dec3|WideOr6~0_combout\);

-- Location: LCCOMB_X3_Y22_N28
\dec3|WideOr5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr5~0_combout\ = (\slave0|counter\(13) & ((\slave0|counter\(12) & (\slave0|counter\(15))) # (!\slave0|counter\(12) & ((\slave0|counter\(14)))))) # (!\slave0|counter\(13) & (\slave0|counter\(14) & (\slave0|counter\(15) $ (\slave0|counter\(12)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(13),
	datab => \slave0|counter\(15),
	datac => \slave0|counter\(14),
	datad => \slave0|counter\(12),
	combout => \dec3|WideOr5~0_combout\);

-- Location: LCCOMB_X3_Y22_N4
\dec3|WideOr4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr4~0_combout\ = (\slave0|counter\(15) & (\slave0|counter\(14) & ((\slave0|counter\(13)) # (!\slave0|counter\(12))))) # (!\slave0|counter\(15) & (\slave0|counter\(13) & (!\slave0|counter\(14) & !\slave0|counter\(12))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(13),
	datab => \slave0|counter\(15),
	datac => \slave0|counter\(14),
	datad => \slave0|counter\(12),
	combout => \dec3|WideOr4~0_combout\);

-- Location: LCCOMB_X3_Y22_N6
\dec3|WideOr3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr3~0_combout\ = (\slave0|counter\(13) & ((\slave0|counter\(14) & ((\slave0|counter\(12)))) # (!\slave0|counter\(14) & (\slave0|counter\(15) & !\slave0|counter\(12))))) # (!\slave0|counter\(13) & (!\slave0|counter\(15) & (\slave0|counter\(14) $ 
-- (\slave0|counter\(12)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000100011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(13),
	datab => \slave0|counter\(15),
	datac => \slave0|counter\(14),
	datad => \slave0|counter\(12),
	combout => \dec3|WideOr3~0_combout\);

-- Location: LCCOMB_X3_Y22_N24
\dec3|WideOr2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr2~0_combout\ = (\slave0|counter\(13) & (!\slave0|counter\(15) & ((\slave0|counter\(12))))) # (!\slave0|counter\(13) & ((\slave0|counter\(14) & (!\slave0|counter\(15))) # (!\slave0|counter\(14) & ((\slave0|counter\(12))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011011100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(13),
	datab => \slave0|counter\(15),
	datac => \slave0|counter\(14),
	datad => \slave0|counter\(12),
	combout => \dec3|WideOr2~0_combout\);

-- Location: LCCOMB_X3_Y22_N20
\dec3|WideOr1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr1~0_combout\ = (\slave0|counter\(13) & (!\slave0|counter\(15) & ((\slave0|counter\(12)) # (!\slave0|counter\(14))))) # (!\slave0|counter\(13) & (\slave0|counter\(12) & (\slave0|counter\(15) $ (!\slave0|counter\(14)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110001100000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(13),
	datab => \slave0|counter\(15),
	datac => \slave0|counter\(14),
	datad => \slave0|counter\(12),
	combout => \dec3|WideOr1~0_combout\);

-- Location: LCCOMB_X3_Y22_N0
\dec3|WideOr0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dec3|WideOr0~0_combout\ = (\slave0|counter\(12) & ((\slave0|counter\(15)) # (\slave0|counter\(13) $ (\slave0|counter\(14))))) # (!\slave0|counter\(12) & ((\slave0|counter\(13)) # (\slave0|counter\(15) $ (\slave0|counter\(14)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111010111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \slave0|counter\(13),
	datab => \slave0|counter\(15),
	datac => \slave0|counter\(14),
	datad => \slave0|counter\(12),
	combout => \dec3|WideOr0~0_combout\);

-- Location: PIN_R21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(1));

-- Location: PIN_T22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(2));

-- Location: PIN_T21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(3));

-- Location: PIN_R20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \kb0|KB\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(0));

-- Location: PIN_R19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \kb0|KB\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(1));

-- Location: PIN_U19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \kb0|KB\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(2));

-- Location: PIN_Y19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \kb0|KB\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(3));

-- Location: PIN_T18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(4));

-- Location: PIN_V19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(5));

-- Location: PIN_Y18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(6));

-- Location: PIN_U18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[7]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(7));

-- Location: PIN_R18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[8]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(8));

-- Location: PIN_R17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[9]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(9));

-- Location: PIN_U22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDG[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \signalPressed~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDG(0));

-- Location: PIN_U21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDG[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDG(1));

-- Location: PIN_J2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(0));

-- Location: PIN_J1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(1));

-- Location: PIN_H2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(2));

-- Location: PIN_H1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(3));

-- Location: PIN_F2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(4));

-- Location: PIN_F1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(5));

-- Location: PIN_E2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec0|ALT_INV_WideOr0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(6));

-- Location: PIN_E1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(0));

-- Location: PIN_H6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(1));

-- Location: PIN_H5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(2));

-- Location: PIN_H4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(3));

-- Location: PIN_G3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(4));

-- Location: PIN_D2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(5));

-- Location: PIN_D1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec1|ALT_INV_WideOr0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(6));

-- Location: PIN_G5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(0));

-- Location: PIN_G6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(1));

-- Location: PIN_C2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|WideOr4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(2));

-- Location: PIN_C1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(3));

-- Location: PIN_E3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(4));

-- Location: PIN_E4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(5));

-- Location: PIN_D3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec2|ALT_INV_WideOr0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(6));

-- Location: PIN_F4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(0));

-- Location: PIN_D5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(1));

-- Location: PIN_D6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|WideOr4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(2));

-- Location: PIN_J4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(3));

-- Location: PIN_L8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(4));

-- Location: PIN_F3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(5));

-- Location: PIN_D4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \dec3|ALT_INV_WideOr0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(6));
END structure;


