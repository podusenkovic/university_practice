library verilog;
use verilog.vl_types.all;
entity keyboard is
    port(
        ps2clk          : in     vl_logic;
        ps2dat          : in     vl_logic;
        KP              : out    vl_logic;
        KB              : out    vl_logic_vector(3 downto 0)
    );
end keyboard;
