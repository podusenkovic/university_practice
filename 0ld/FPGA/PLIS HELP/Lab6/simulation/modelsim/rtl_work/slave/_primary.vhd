library verilog;
use verilog.vl_types.all;
entity slave is
    port(
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        SW              : in     vl_logic_vector(9 downto 0);
        KButton         : in     vl_logic_vector(3 downto 0);
        pressed         : in     vl_logic;
        counter         : out    vl_logic_vector(15 downto 0)
    );
end slave;
