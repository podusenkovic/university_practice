module countEchoTime (input 			echo,
							 input 			clk,
							 input 			rst,
							 output reg [20:0] tacts);
	
	parameter MAX_TICKS = 16'd49999999;
	reg[20:0] cntr;
	reg check;
	
	always@(posedge clk)
		if (rst) cntr <= 16'b0;
		else if (echo)
					cntr <= cntr + 1;
			  else if (cntr != 16'b0) begin 
					tacts = cntr;
					cntr <= 16'b0;
			  end
		 
endmodule 