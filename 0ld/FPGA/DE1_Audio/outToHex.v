module outToHex(input 		[20:0] tacts,
					 output     [6:0]  HEX0, HEX1, HEX2, HEX3);

					 
	wire[15:0] decs;
	assign decs[3:0] = (tacts / 25 / 58 / 2)%10;
	assign decs[7:4] = ((tacts / 25 / 58 / 2)%100)/10;
	assign decs[11:8] = ((tacts / 25 / 58 / 2)%1000)/100;
	assign decs[15:12] = (tacts / 25 / 58 / 2)/1000;
	
	wire[15:0] outter;
	
	assign outter = (decs < 15'd500) ? decs : 16'b1010101110111100;
	
	dec dec0(.in(outter[3:0]),	
				.out(HEX0));
				
	dec dec1(.in(outter[7:4]),
				.out(HEX1));
				
	dec dec2(.in(outter[11:8]),
				.out(HEX2));		
				
	dec dec3(.in(outter[15:12]),
				.out(HEX3));
endmodule 