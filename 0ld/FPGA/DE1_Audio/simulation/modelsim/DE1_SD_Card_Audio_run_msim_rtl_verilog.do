transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+C:/Users/u133180/Desktop/DE1_Audio {C:/Users/u133180/Desktop/DE1_Audio/sinus_maker.v}
vlog -vlog01compat -work work +incdir+C:/Users/u133180/Desktop/DE1_Audio {C:/Users/u133180/Desktop/DE1_Audio/outToHex.v}
vlog -vlog01compat -work work +incdir+C:/Users/u133180/Desktop/DE1_Audio {C:/Users/u133180/Desktop/DE1_Audio/one_sec_counter.v}
vlog -vlog01compat -work work +incdir+C:/Users/u133180/Desktop/DE1_Audio {C:/Users/u133180/Desktop/DE1_Audio/main.v}
vlog -vlog01compat -work work +incdir+C:/Users/u133180/Desktop/DE1_Audio {C:/Users/u133180/Desktop/DE1_Audio/dec.v}
vlog -vlog01compat -work work +incdir+C:/Users/u133180/Desktop/DE1_Audio {C:/Users/u133180/Desktop/DE1_Audio/createTriggerSignal.v}
vlog -vlog01compat -work work +incdir+C:/Users/u133180/Desktop/DE1_Audio {C:/Users/u133180/Desktop/DE1_Audio/countEchoTime.v}
vlog -vlog01compat -work work +incdir+C:/Users/u133180/Desktop/DE1_Audio {C:/Users/u133180/Desktop/DE1_Audio/cordic.v}

