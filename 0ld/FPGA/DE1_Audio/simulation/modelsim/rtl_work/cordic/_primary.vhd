library verilog;
use verilog.vl_types.all;
entity cordic is
    generic(
        width           : integer := 16
    );
    port(
        clock           : in     vl_logic;
        cosine          : out    vl_logic_vector;
        sine            : out    vl_logic_vector;
        x_start         : in     vl_logic_vector;
        y_start         : in     vl_logic_vector;
        angle           : in     vl_logic_vector(31 downto 0)
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of width : constant is 1;
end cordic;
