library verilog;
use verilog.vl_types.all;
entity countEchoTime is
    generic(
        MAX_TICKS       : vl_logic_vector(0 to 15) := (Hi1, Hi1, Hi1, Hi1, Hi0, Hi0, Hi0, Hi0, Hi0, Hi1, Hi1, Hi1, Hi1, Hi1, Hi1, Hi1)
    );
    port(
        echo            : in     vl_logic;
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        tacts           : out    vl_logic_vector(20 downto 0)
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of MAX_TICKS : constant is 1;
end countEchoTime;
