library verilog;
use verilog.vl_types.all;
entity main is
    port(
        CLOCK_50        : in     vl_logic;
        echo            : in     vl_logic;
        echo_2          : in     vl_logic;
        SW              : in     vl_logic_vector(9 downto 0);
        KEY             : in     vl_logic_vector(3 downto 0);
        trigger         : out    vl_logic;
        trigger_2       : out    vl_logic;
        LEDR            : out    vl_logic_vector(9 downto 0);
        HEX0            : out    vl_logic_vector(6 downto 0);
        HEX1            : out    vl_logic_vector(6 downto 0);
        HEX2            : out    vl_logic_vector(6 downto 0);
        HEX3            : out    vl_logic_vector(6 downto 0)
    );
end main;
