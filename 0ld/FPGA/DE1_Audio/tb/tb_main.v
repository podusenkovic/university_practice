`timescale 1ns/1ps
module tb_main();

		reg  			  CLOCK_50;
		reg  [9:0]    SW;
		reg  [3:0]	  KEY;
		reg 			  echo;
		reg 			  echo_2;
		wire 			  trigger;
		wire 			  trigger_2;
		wire [6:0]    HEX0;
		wire [6:0]    HEX1;
		wire [6:0]    HEX2;
		wire [6:0]    HEX3;
		wire [9:0]    LEDR;
		
		main DUT(.CLOCK_50(CLOCK_50),
					.echo(echo),
					.echo_2(echo_2),
					.trigger(trigger),
					.trigger_2(trigger_2),
					.SW(SW),
					.LEDR(LEDR),
					.KEY(KEY),
					.HEX0(HEX0),
					.HEX1(HEX1),
					.HEX2(HEX2),
					.HEX3(HEX3));
		
		reg[20:0] echoTime = 20'd10000;
		
		initial begin
			SW = 10'b0;
			KEY = 4'b1110;
			#5;
			KEY = 4'b1111;
			echo = 1'b0;
			
		end
		
		initial begin
			CLOCK_50 = 1'b1;
			forever #10 CLOCK_50 = !CLOCK_50;
		end
		
		initial begin
			KEY[0] = 0;
			#100 ;
			KEY[0] = 1;
			#10000500;
			echoTime = 20'd40000;
			#10000500;
			echoTime = 20'd50000;
			#10000500;
			echoTime = 20'd100000;
			#10000500;
			echoTime = 20'd130000;
			#10000500;
			echoTime = 20'd2600000;
			#10000500;
			echoTime = 20'd3500000;
			#10000500;
			echoTime = 20'd5000000;
		end
		
		reg trig_d0, trig_d1, trig_d2;
		
		always@(posedge CLOCK_50)
		if (~KEY[0]) begin
			trig_d0 <= trigger;
			trig_d1 <= trigger;
			trig_d2 <= trigger;
		end 
		else begin
			trig_d0 <= trigger;
			trig_d1 <= trig_d0;
			trig_d2 <= trig_d1;
		end
		
		reg[19:0] 	counter = 20'b0;
		always@(posedge CLOCK_50) begin
			if (trig_d2 && !trig_d1) 
				echo = 1'b1;
			if (~echo)
				counter = 14'b0;
			else if (counter > echoTime)
				echo = 1'b0;
			else counter <= counter + 1'b1;
		end
		
		
		reg trig_2_d0, trig_2_d1, trig_2_d2;
		always@(posedge CLOCK_50)
		if (~KEY[0]) begin
			trig_2_d0 <= trigger_2;
			trig_2_d1 <= trigger_2;
			trig_2_d2 <= trigger_2;
		end 
		else begin
			trig_2_d0 <= trigger_2;
			trig_2_d1 <= trig_2_d0;
			trig_2_d2 <= trig_2_d1;
		end
		
		reg[19:0] 	counter_2 = 20'b0;
		always@(posedge CLOCK_50) begin
			if (trig_2_d2 && !trig_2_d1) 
				echo_2 = 1'b1;
			if (~echo_2)
				counter_2 = 14'b0;
			else if (counter_2 > echoTime)
				echo_2 = 1'b0;
			else counter_2 <= counter_2 + 1'b1;
		end
		
		
endmodule 