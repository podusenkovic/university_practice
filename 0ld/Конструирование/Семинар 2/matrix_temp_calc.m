clc, clear
Q1 = 1; Q2 = 1; Q3 = 2; Q4 = 2; R = 1; Toc = 20;
B = [2*Q1 * R ; 2*Q2 * R + 2*Toc; 2*Q3 * R;2* Q4  * R + 2*Toc]
A = [2, -1, -1, 0; -1, 4, 0, -1; -1, 0, 2, -1; 0, -1, -1, 4]
X = A^(-1) * B