clear, clc, close all

step = 0.001;
eps = 0.005;

T = 1;
ksi = 0.2;
K = 10;

w = 0:step:100;
W = K./(T^2*1i^2*w.^2 + 2*ksi*T*w*1i + 1 + K);
U = real(W); V = imag(W);
A = abs(W); phi = angle(W);
figure
plot(w, A)
figure
plot(w, phi)
[maxA, imax] = max(A);
M = maxA / A(1)
wp = imax * step
w0_bandwidth = 0;
w1_bandwidth = 0;
for i=1:length(A)
    if (abs(A(i) - maxA/sqrt(2)) < eps && w0_bandwidth == 0)
        w0_bandwidth = i * step;
    else 
        if (abs(A(i) - maxA/sqrt(2)) < eps && w0_bandwidth ~= 0)
            w1_bandwidth = i * step;
            break;
        end
    end
    if ((A(i) - 1) < eps)
        w_sr = i * step;
    end
end
disp("Bandwidth");
disp(sprintf("\t%5.3f <= w <= %5.3f", w0_bandwidth, w1_bandwidth));

w_sr
prev_val = phi(1);
for i=2:length(phi)
    if ((phi(i) + pi/2) * (prev_val + pi/2) <= 0)
        w_of_phi_zero = i * step
    end
    prev_val = phi(i);
end

zapas_proch_phase = (phi(w_sr/step) + pi/2) / pi * 180
zapas_proch_ampl = 20*log10(A(w_of_phi_zero/step))