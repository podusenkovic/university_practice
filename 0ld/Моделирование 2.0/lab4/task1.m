clear, clc

T = 10;
ksi = 0.02;
K = 1;

W = @(p)1/(100*p^2 + 0.4*p + 1);
h = @(t)1 - (0.5 + 0.01*1i).*exp((-0.002-0.1i)*t).*(1 + 0.04*1i + exp(0.2*1i*t));

step = 0.01;
delta = 0.05;
h_yst = 1;
last_val = 0;
h_max = h(0);
h_min = h(0);
for i=0:step:10000
    if (abs(h(i) - h_yst) < delta * h_yst)
        if (last_val == 0)
            last_val = i;
        end
    else
        if (last_val ~= 0)
            last_val = 0;
        end
    end
    if (h(i) > h_max)
        h_max = h(i);
    else
        if (h(i) < h_min)
            h_min = h(i);
        end
    end
end
tp = last_val
sigma = abs((h_max - h_yst) / h_yst) * 100
w = 2*pi / T
n = 0; prev_val = h(0);
for i=1:step:tp
    if ((h(i) - h_yst) * (prev_val - h_yst) <= 0) 
        n = n + 1;
    end
    prev_val = h(i);
end
n = round(n / 2)
tmax = 0;
for i=1:step:tp
    if (h(i) == h_max)
        tmax = i;
        break;
    end
end
tmax

tn = 0;
for i=1:step:tp
    if (h(i) >= h_yst)
        tn = i;
        break;
    end
end
tn 

GrayFaceNoSpace = abs((h_max - h_yst) / (h_yst - h_min))
