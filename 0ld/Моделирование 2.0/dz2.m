max_val = 100;
for a0 = max_val:-1:1
    for a1 = max_val:-1:1
        for a2 = max_val:-1:1
            for a3 = max_val:-1:1
                for a4 = max_val:-1:1
                    for a5 = max_val:-1:1
                        a = [a0 a1 a2 a3 a4 a5];
                        matr = [a(2:2:end) zeros(1,2);
                            a(1:2:end) zeros(1,2);
                            0 a(2:2:end) 0;
                            0 a(1:2:end) 0;
                            zeros(1,2) a(2:2:end)];
                        
                        count = 0;
                        for i = 1:5
                            result(i) = det(matr(1:i, 1:i));
                            if (result(i) > 0)
                                count = count + 1;
                            end
                        end
                        if (count == 5) 
                            a
                            result
                        end
                    end
                end
            end
        end
    end
end