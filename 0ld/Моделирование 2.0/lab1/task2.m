%lab1_task2
clear, clc, close all;
T = 1.7;
T1 = T;
T2 = 0.34;
ksi = 0.3;
k = 0.2;
step = 0.0001;
w = step:step:100;
w1 = step:step:1/T;
w2 = w((length(w1) + 1):end);

U = (k*(1 - T*T*w.*w))./((1 - T*T*w.*w).^2 + (2*ksi*T*w).^2);
V = (-2*k*ksi*T*w)./((1 - T*T*w.*w).^2 + (2*ksi*T*w).^2);
A = k ./ sqrt(((1 - T*T*w.*w).^2 + (2*ksi*T*w).^2));

phi = [-atan(2*ksi*T*w1 ./ (1 - T*T*w1.*w1)) (-pi-atan(2*ksi*T*w2 ./ (1 - T*T*w2.*w2)))];
L = 20*log10(A);
L_rounded = [(20*log10(k).*ones(1,length(w1))) (20*log10(k) - 20*log10(T*T*w2.*w2))];
figure
subplot(2,1,1);
plot(w,A); 
grid on; title('A(\omega)');
subplot(2,1,2);
plot(w,phi);  
grid on; title('\phi(\omega)');

figure
semilogx(w,L);
hold on, grid on, title('lg(A(\omega))');
semilogx(w,L_rounded);
legend('normal lg(A(\omega))','rounded lg(A(\omega))');

sys = tf([k],[T^2, 2*ksi*T, 1])
ltiview({'step', 'impulse', 'bode', 'bode'}, sys)