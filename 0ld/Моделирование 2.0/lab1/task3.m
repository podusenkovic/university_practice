%lab1_task3
clear, clc, close all;
T = 1.7;
T1 = T;
T2 = 0.34;
ksi = 0.3;
k = 0.2;
step = 0.0001;
w = step:step:100;
W = k*i*w./((T1*i*w + 1).*(T2^2*i^2*w.^2 + 2*ksi*T2*w*i + 1));
U = real(W); V = imag(W);
A = abs(W); phi = angle(W);
L = 20*log10(A);

w1 = step:step:1/T1;
y1 = 20*log10(k*w1);
w2 = (1/T1):step:1/T2;
y2 = 20*log10(k*w2) - 20*log10(T1*w2);
w3 = (1/T2 + step):step:100;
y3 = 20*log10(k*w3) - 20*log10(T1*w3) - 20*log10(T2^2*w3.^2);

y = [y1 y2 y3];

subplot(2,1,1);
semilogx(w, L); grid; title('lg(A(\omega))');
subplot(2,1,2);
semilogx(w, y); grid; title('rounded lg(A(\omega))');

W1 = k*j*w;
phi1 = angle(W1);
W2 = 1./(T1*j*w + 1);
phi2 = angle(W2);
W3 = 1./(T2^2*i^2*w.^2 + 2*ksi*T2*w*i + 1);
phi3 = angle(W3);

figure
subplot(3,1,1);
semilogx(w,phi1); grid; title('\phi_1(\omega)');
subplot(3,1,2);
semilogx(w,phi2); grid; title('\phi_2(\omega)');
subplot(3,1,3);
semilogx(w,phi3); grid; title('\phi_3(\omega)');

figure 
semilogx(w, phi, w, phi1 + phi2 + phi3); grid;
legend('\phi(\omega)', 'rounded \phi(\omega)');
max_diff_between_pfis = max(abs(phi - (phi1 + phi2 + phi3)))



