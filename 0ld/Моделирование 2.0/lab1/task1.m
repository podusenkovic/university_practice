%lab1_task1
clear, clc, close all;
T = 1.7;
T1 = T;
T2 = 0.34;
ksi = 0.3;
k = 0.2;
step = 0.0001;
w = step:step:100;
U = k./((T * w).^2 + 1);
V = (-k*T*w)./((T * w).^2 + 1);

A = k ./ sqrt((T*w).^2 + 1);
phi = -atan(w*T);
L = 20*log10(A);
w1 = step:step:1/T;
w2 = w((length(w1) + 1):end);
L_rounded = [20*log10(k).*ones(1,length(w1)) 20*log10(k) - 20*log10(T.*w2)];
figure
subplot(2,1,1);
plot(w,A); 
grid on; title('A(\omega)');
subplot(2,1,2);
plot(w,phi);  
grid on; title('\phi(\omega)');

figure
semilogx(w,L);
hold on, grid on, title('lg(A(\omega))');
semilogx(w,L_rounded);
legend('normal lg(A(\omega))','rounded lg(A(\omega))');

sys = tf([k],[T, 1])
ltiview({'step', 'impulse', 'bode', 'bode'}, sys)