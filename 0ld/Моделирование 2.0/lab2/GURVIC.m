function result = GURVIC(coef_row)
    a = coef_row;
    n = length(a) - 1;
    gurvic_matrix = zeros(n,n);
    for i = 1:n
       %create matrix
       gurvic_matrix(i, :) = [zeros(1,floor((i-1)/2)) a((1 + mod(i,2)):2:end) zeros(1, n - length(a((1 + mod(i,2)):2:end)) - floor((i-1)/2))];
    end
    gurvic_matrix
    count = 0;
    for i = 1:n
        result_of_determinates(i) = det(gurvic_matrix(1:i, 1:i));
        if (result_of_determinates(i) < 0)
            count = count + 1;
        end
    end
    result_of_determinates
    if (count == 0)
        %disp("������� ���������")
        result = 0;
    else 
        %disp("������� �� ���������")
        result = 1;
    end
end