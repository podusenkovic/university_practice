function result = MIKHAILOV(coef_row)
    a = coef_row;
    n = length(a) - 1;
    %w = 0:0.01:1E4;
    X = 0; Y = 0;
    
    Djw = @(w)sum(a.*((j*w).^(n:-1:0)));
    
    X = @(w)real(Djw(w));
    Y = @(w)imag(Djw(w));
    
    ezplot(X,Y);
    
    result = 1;
    
    if (X(0) < 0 || Y(0) ~= 0) 
        return;
    end
    
    angles = zeros(1, 1E3/0.01);
    values = 0.01:0.01:1E3;
    for i = 1:length(values)
        angles(i) = angle(Djw(values(i)));
    end
    angles = unwrap(angles);
    if ((angles(end)) < (pi/2 * n) && (angles(end)) > (pi/2 * (n-1)))
        if (angles == sort(angles))
            result = 0;
        else 
            result = 1;
        end
    else
        result = 1;
    end
        