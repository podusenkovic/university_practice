ALTER FUNCTION create_user_and_give_him_role (@user_name sysname, @passw0rd sysname, @default_db sysname, @role_name sysname)
RETURNS int
AS BEGIN 
	EXEC sp_addlogin @user_name, @passw0rd, @default_db
	EXEC sp_addrolemember @role_name, @user_name
	RETURN 0
END