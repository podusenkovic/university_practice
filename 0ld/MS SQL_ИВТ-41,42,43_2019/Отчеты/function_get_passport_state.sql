CREATE FUNCTION get_passport_state (@pass_number int)
RETURNS varchar(50)
AS BEGIN
	RETURN (SELECT [Статус готовности] 
			FROM [Технические_паспорта] 
			WHERE [Номер паспорта] = @pass_number)
END