CREATE PROCEDURE change_tech_pass (@pass_num int,
								   @data_vvoda date, 
								   @ploshad float, 
								   @etaz int, 
								   @tech_sost varchar(150), 
								   @proved_systems varchar(150),
								   @kad_num varchar(16),
								   @inventar_num nchar(15),
								   @price money,
								   @naznach varchar(25),
								   @state varchar(50))
								   AS
								   UPDATE [�����������_��������] SET 
								   [���� ����� ������ � ������������] = @data_vvoda,
								   [������� �������] = @ploshad,
								   [���������] = @etaz,
								   [����������� ��������� ��������] = @tech_sost,
								   [�������� ����������� ���������� ������] = @proved_systems,
								   [����������� �����] = @kad_num,
								   [����������� �����] = @inventar_num,
								   [��������� ���������] = @price,
								   [����������] = @naznach,
								   [������ ����������] = @state
								   WHERE [����� ��������] = @pass_num
