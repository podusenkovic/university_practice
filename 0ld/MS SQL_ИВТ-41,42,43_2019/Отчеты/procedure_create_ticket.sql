ALTER PROCEDURE [dbo].[create_ticket]  (@FIO_person varchar(150), 
								@birthday date, 
								@pass_num nchar(12), 
								@place_born varchar(150), 
								@place_registered varchar(150),
								@data_vvoda date, 
								@ploshad float, 
								@etaz int, 
								@tech_sost varchar(150), 
								@proved_systems varchar(150),
								@kad_num varchar(16),
								@inventar_num nchar(15),
								@price money,
								@naznach varchar(25),
								@state varchar(50))
								AS begin
								EXEC create_person @FIO_person, @birthday, @pass_num, @place_born, @place_registered

								EXEC create_tech_pass @data_vvoda, @ploshad, @etaz, @tech_sost, @proved_systems, @kad_num, @inventar_num, @price, @naznach, @state
								declare @tech_pass_num int;
								SELECT @tech_pass_num = MAX([Номер паспорта]) FROM [Технические_паспорта]
								INSERT INTO [Связь_Собственники_Техпаспорта] VALUES (@pass_num, @tech_pass_num);

								END