CREATE PROC drop_user_if_it_has_the_role (@userName NVARCHAR(50), @roleName NVARCHAR(50))
AS
DECLARE @rdRole NVARCHAR(50);
DECLARE @t TABLE(UserName NVARCHAR(50), RoleName NVARCHAR(50), LoginName NVARCHAR(50), DefDBName NVARCHAR(50), DefSchemaName NVARCHAR(50), UserID SMALLINT, [SID]  VARBINARY(100))

INSERT INTO @t
EXEC sp_helpuser @userName

SET @rdRole = (SELECT RoleName FROM @t);

IF @rdRole = @roleName
	EXEC sp_dropuser @userName;
ELSE
	PRINT('������������ '+'"'+@userName+'"'+' �� ����� ���� '+'"'+@roleName+'"');