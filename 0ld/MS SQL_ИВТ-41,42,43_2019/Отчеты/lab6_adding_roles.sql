USE DB_TechPassports;

--EXEC sp_droprole ��������
EXEC sp_addrole ��������
GRANT EXECUTE ON create_ticket TO ��������
GRANT EXECUTE ON change_person TO ��������
GRANT EXECUTE ON change_tech_pass TO ��������
GRANT EXECUTE ON delete_ticket TO ��������
GRANT EXECUTE ON get_tech_pass TO ��������
GRANT EXECUTE ON get_person_data TO ��������

--EXEC sp_droprole �������������
EXEC sp_addrole �������������
GRANT EXECUTE ON delete_ticket TO �������������
GRANT EXECUTE ON change_tech_pass_state TO �������������
GRANT EXECUTE ON get_tech_pass TO �������������
GRANT EXECUTE ON get_person_data TO �������������

--EXEC sp_droprole ����������_����������
EXEC sp_addrole ����������_����������
GRANT EXECUTE ON change_tech_pass_state TO ����������_����������