CREATE PROCEDURE change_tech_pass_state (@tech_pass_num int, @new_state varchar(50))
AS
UPDATE [Технические_паспорта] SET [Статус готовности] = @new_state
WHERE [Номер паспорта] = @tech_pass_num