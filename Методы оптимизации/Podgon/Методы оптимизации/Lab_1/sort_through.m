function [ result , num_of_steps ] = sort_through( a , b , eps )

num_of_steps = (b - a) / eps;
step = (b - a) / num_of_steps;
array = zeros(num_of_steps,1);

for i = 0:1:num_of_steps
    x = a + i * step;
    array(i+1) = func(x);
end

result = min(array);

end

