function [ ymin, xmin ] = mid_point_search_r( a, b, eps, h )

iterations = 0;
xmin = (a + b)/2;
diff_f = r_diff(xmin, h);

while(abs(diff_f) > eps)
    if(diff_f > 0)
        b = xmin;
    else
        a = xmin;
    end
    
    xmin = (a + b)/2;
    diff_f = r_diff(xmin, h);    
    iterations = iterations + 1;
end

fprintf('Iterations done: %d', iterations);
ymin = f(xmin);

end
