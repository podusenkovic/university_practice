function [ ymin, xmin ] = brutalforce( a, b, eps )

n         = ceil((b-a)/eps); %����� �������� ���������
eps_n     = (b-a)/n; %�������� ����������
xmin      = a; %��������� X
ymin      = f(xmin); %��������� Y
iteration = 1; % ����� ��������

for i = a+eps_n : eps_n : b
    x = i;
    y = f(x);
    iteration = iteration + 1;
    if(y <= ymin)
        ymin = y;
        xmin = x;
    end
end
fprintf('Iterations done: %d', iteration);

end
