function [ ymin, xmin ] = mid_point_search( a, b, eps )

iterations = 0;
xmin = (a + b)/2;
diff_f = df(xmin);

while(abs(diff_f) > eps)
    if(diff_f > 0)
        b = xmin;% ������ ������� [a;xmin]
    else
        a = xmin;% ������ ������� [xmin;b]
    end
    
    xmin = (a + b)/2;% ������� ��������� xmin
    diff_f = df(xmin);    
    iterations = iterations + 1;
end

fprintf('Iterations done: %d', iterations);
ymin = f(xmin);

end
