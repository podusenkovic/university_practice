function [ ymin, xmin ] = chord_method( a, b, eps )

iterations = 0;
diff_a = df(a);
diff_b = df(b);
xmin = a - ((diff_a * (a - b)) / (diff_a - diff_b));
diff_xmin = df(xmin);

while(abs(diff_xmin) > eps)
    if(diff_xmin > 0)
        b = xmin;
        diff_b = diff_xmin;
    else
        a = xmin;
        diff_a = diff_xmin;
    end    
    xmin = a - ((diff_a * (a - b)) / (diff_a - diff_b));
    diff_xmin = df(xmin);    
    iterations = iterations + 1;
end

ymin = f(xmin);
fprintf('Iterations done: %d', iterations);

end
