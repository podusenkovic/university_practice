function [left, right] = part_5 (eps)
a  = -5;
b  = 5;
x0 = a;
while (1)
    x0 = x0+eps;
    [ ymin, xmin ] = newton_rafson(x0, eps);
    if(isnan(ymin)==0)
        break;
    end
end
left = x0;
x0 = b;
while (1)
    x0 = x0-eps;
    [ ymin, xmin ] = newton_rafson(x0, eps);
    if(isnan(ymin)==0)
        break;
    end
end
right = x0;
end
