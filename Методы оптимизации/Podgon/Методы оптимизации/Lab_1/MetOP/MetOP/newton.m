function [ ymin, xmin ] = newton( x0, eps )
iterations = 0;
xmin = x0;
diff_xmin  = df(x0);
diff2_xmin = df2(x0);
xmin = xmin - (diff_xmin / diff2_xmin);

while (abs(diff_xmin) > eps)
    
    diff_xmin  = df(xmin);
    diff2_xmin = df2(xmin);
    
    xmin = xmin - (diff_xmin / diff2_xmin);
    iterations = iterations + 1;
end

ymin = f(xmin);
fprintf('Iterations done: %d', iterations);

end
