function [ ymin, xmin ] = newton_l( x0, eps, h )
iterations = 0;
xmin = x0;
diff_xmin  = l_diff(x0, h);
diff2_xmin = (l_diff(x0, h) - l_diff(x0-h, h))/h;
xmin = xmin - (diff_xmin / diff2_xmin);

while (abs(diff_xmin) > eps)
    
    diff_xmin  = l_diff(xmin, h);
    diff2_xmin = (l_diff(xmin, h) - l_diff(xmin-h, h))/h;
    
    xmin = xmin - (diff_xmin / diff2_xmin);
    iterations = iterations + 1;
end

ymin = f(xmin);
fprintf('Iterations done: %d', iterations);

end