function [xmin, ymin] = min_dihotomy(a, b, eps)

delta = 0.2 * eps;
n = 0;
while (1)
    x1 = (b + a - delta) / 2;
    x2 = (b + a + delta) / 2;
    f1 = f(x1);
    f2 = f(x2);
    n = n + 2;
    
    if (f1 <= f2)
        b = x2;
    else
        a = x1;
    end
    
    eps_curr = (b - a) / 2;
    
    if (eps_curr <= eps)
        break;
    end
end
xmin = (a + b) / 2;
ymin = f(xmin);
n = n + 1;
fprintf('Iterations done: %d', n);
