function [ ymin, xmin ] = chord_method_r( a, b, eps, h )

iterations = 0;
diff_a = r_diff(a, h);
diff_b = r_diff(b, h);
xmin = a - ((diff_a * (a - b)) / (diff_a - diff_b));
diff_xmin = r_diff(xmin, h);

while(abs(diff_xmin) > eps)
    if(diff_xmin > 0)
        b = xmin;
        diff_b = diff_xmin;
    else
        a = xmin;
        diff_a = diff_xmin;
    end    
    xmin = a - ((diff_a * (a - b)) / (diff_a - diff_b));
    diff_xmin = r_diff(xmin, h);    
    iterations = iterations + 1;
end

ymin = f(xmin);
fprintf('Iterations done: %d', iterations);

end
