function [ ymin, xmin ] = newton_rafson( x0, eps )

iterations = 0;
xmin = x0;
diff_xmin  = df(x0);
diff2_xmin = df2(x0);
tau = (diff2_xmin^2) / (diff2_xmin^2 + (xmin - (diff_xmin / diff2_xmin))^2);
xmin = xmin - tau*(diff_xmin / diff2_xmin);

while (abs(diff_xmin) > eps)
    
    diff_xmin  = df(xmin);
    diff2_xmin = df2(xmin);
    
    x_t = xmin - (diff_xmin/diff2_xmin);
    tau = (diff_xmin^2) / (diff_xmin^2 + (df(x_t))^2);
    
    xmin = xmin - tau*(diff_xmin / diff2_xmin);
    iterations = iterations + 1;
end

ymin = f(xmin);
fprintf('Iterations done: %d', iterations);

end