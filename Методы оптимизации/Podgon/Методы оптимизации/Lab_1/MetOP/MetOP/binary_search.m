function [ ymin, xmin ] = binary_search( a, b, eps )

delta = 1;%���
x = a;
direction = -1;% �����������

ymin = f(x);
xmin = a;

n = 0;

while ( delta > eps )%���� ��� ������ ��������
    delta = delta / 4;
    direction = - direction;
    
    while(1)
        x = x + direction * delta;
        y = f(x);
        n = n + 1;
        
        if (y <= ymin)
            ymin = y;
            xmin = x;
        end

        if (y > ymin)%���� ������� ������ ���������� -break
            if ( delta > eps)%���� �������� �� ���������� - ������������ min 
                xmin = x;
                ymin = y;
            end
            break;
        end
        
        if (x == b)% ���� ����� �� ���� ��������� -break
            xmin = x;
            ymin = y;
            break;
        end
        
        if (x == a)% ���� ����� �� ���� ��������� -break
            xmin = x;
            ymin = y;
            break;
        end
        
    end

end

fprintf('Iterations done: %d', n);

end
