function [ ymin, xmin ] = marquardt( x0, eps )

iterations = 0;
xmin = x0;
diff_xmin  = df(x0);
diff2_xmin = df2(x0);
mu = abs(diff2_xmin) * 10;
xmin_old = xmin;
xmin = xmin - (diff_xmin / (diff2_xmin + mu));

while (abs(diff_xmin) > eps)
    
    if (f(xmin) < f(xmin_old))
        mu = mu / 2;
    else
        mu = mu * 2;
    end
    
    diff_xmin  = df(xmin);
    diff2_xmin = df2(xmin);    
    xmin_old = xmin;
    xmin = xmin - (diff_xmin / (diff2_xmin + mu));
    iterations = iterations + 1;
end

ymin = f(xmin);
fprintf('Iterations done: %d', iterations);

end