function [ ymin, xmin ] = newton_r( x0, eps, h )
iterations = 0;
xmin = x0;
diff_xmin  = r_diff(x0, h);
diff2_xmin = (r_diff(x0+h, h) - r_diff(x0, h))/h;
xmin = xmin - (diff_xmin / diff2_xmin);

while (abs(diff_xmin) > eps)
    
    diff_xmin  = r_diff(xmin, h);
    diff2_xmin = (r_diff(xmin+h, h) - r_diff(xmin, h))/h;
    
    xmin = xmin - (diff_xmin / diff2_xmin);
    iterations = iterations + 1;
end

ymin = f(xmin);
fprintf('Iterations done: %d', iterations);

end
