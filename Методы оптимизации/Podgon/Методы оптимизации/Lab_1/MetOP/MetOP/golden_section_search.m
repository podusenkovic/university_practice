function [ xmin, ymin ] = golden_section_search(a, b, eps)

n = 0;
tau = (sqrt(5)-1)/2;

x1 = b-(b-a)*tau;
x2 = a+(b-a)*tau;
    
y1 = f(x1);
y2 = f(x2);

eps_n = (b-a)/2;

while (eps_n > eps)
    
    if( y1 <= y2 )
        b = x2;  
        x2 = x1;
        y2 = y1;    
        x1 = a + (b-x2);
        y1 = f(x1);
    else
        a = x1;
        x1 = x2;
        y1 = y2;
        x2 = b - (x1-a);
        y2 = f(x2);
    end
    
    eps_n = (b-a)/2;
    n = n + 1;
end

xmin = (a+b)/2;
ymin = f(xmin);
n = n + 1;
fprintf('Iterations done: %d', n);

end