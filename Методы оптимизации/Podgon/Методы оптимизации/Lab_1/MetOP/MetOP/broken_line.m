function [ pn ] = broken_line( a, b, L, eps)

iterations = 0;
x0 = (f(a) - f(b) + L*(a+b)) / (2*L);
y0 = (f(a) + f(b) + L*(a-b)) / 2;
delta = (f(x0)-y0)/(2*L);% ������� ������
%������� ����� ����� ��������
xn_1 = x0 - delta;
xn_2 = x0 + delta;
pn = (f(x0) + y0) / 2;%������� ����� ���������. ���-���
eps_i = 2*L*delta;

while (eps_i > eps)
   
   f_xn1 = f(xn_1);
   f_xn2 = f(xn_2);
   pn_1 = (f_xn1 + pn) / 2;
   pn_2 = (f_xn2 + pn) / 2;
   
   if(pn_1 < pn_2)
       delta = (f_xn1-pn)/(2*L);
       xn_1 = xn_1 - delta;
       xn_2 = xn_1 + delta;
       pn = pn_1;
   else
       delta = (f_xn2-pn)/(2*L);
       xn_1 = xn_2 - delta;
       xn_2 = xn_2 + delta;
       pn = pn_2;
   end
   
   eps_i = 2*L*delta;
   iterations = iterations + 1;
end

fprintf('Iterations done: %d', iterations);

end