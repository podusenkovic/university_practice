function [ ymin, xmin ] = newton_c( x0, eps, h )
iterations = 0;
xmin = x0;
diff_xmin  = c_diff(x0, h);
diff2_xmin = (c_diff(x0+(h/2), h) - c_diff(x0-(h/2), h))/h;
xmin = xmin - (diff_xmin / diff2_xmin);

while (abs(diff_xmin) > eps)
    
    diff_xmin  = r_diff(xmin, h);
    diff2_xmin = (c_diff(xmin+(h/2), h) - c_diff(xmin-(h/2), h))/h;
    
    xmin = xmin - (diff_xmin / diff2_xmin);
    iterations = iterations + 1;
end

ymin = f(xmin);
fprintf('Iterations done: %d', iterations);

end
