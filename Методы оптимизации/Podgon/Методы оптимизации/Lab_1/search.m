function [ result , num_of_steps ] = search( a , b , eps )

num_of_steps = 0;
flag = 0;
delta = 4 * eps;
i = 1;
x1 = a;
x2 = x1;

while(flag == 0)
    
    buf = x1;
    x1 = x2 + delta;
    x2 = buf;
    
    if((func(x1) <= func(x2)) && (x1 ~= a) && (x1 ~= b))
        i = i + 1;
    elseif (abs(delta) < eps)
        flag = 1;
    else
        delta = -delta / 4;
        i = i + 1;
    end
    
    num_of_steps = num_of_steps + 2;
end

result = func(x1);

end