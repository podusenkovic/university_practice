function [] = zolotoe_sechenie(a, b, eps, f)

iter = 0;
t = (sqrt(5)-1)/2;

x1 = b-(b-a)*t;
x2 = a+(b-a)*t;
    
y1 = f(x1);
y2 = f(x2);

current_eps = (b-a)/2;

while (current_eps > eps)
    
    if( y1 <= y2 )
        b = x2;  
        x2 = x1;
        y2 = y1;    
        x1 = a + (b-x2);
        y1 = f(x1);
    else
        a = x1;
        x1 = x2;
        y1 = y2;
        x2 = b - (x1-a);
        y2 = f(x2);
    end
    
    current_eps = (b-a)/2;
    iter = iter + 1;
end

xmin = (a+b)/2;
ymin = f(xmin);
iter = iter + 1;

fprintf('Iter = %d\n', iter);
fprintf('Xmin = %d\n', xmin);
fprintf('Ymin = %d\n', ymin);

end