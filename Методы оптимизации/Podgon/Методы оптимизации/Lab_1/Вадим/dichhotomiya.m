function [] = dichhotomiya(a, b, eps, f)

step = 0.2 * eps;
iter = 0;
while (1)
    x1 = (b + a - step) / 2;
    x2 = (b + a + step) / 2;
    f1 = f(x1);
    f2 = f(x2);
    iter = iter + 2;
    
    if (f1 <= f2)
        b = x2;
    else
        a = x1;
    end
    
    current_eps = (b - a) / 2;
    
    if (current_eps <= eps)
        break;
    end
end
xmin = (a + b) / 2;
ymin = f(xmin);
iter = iter + 1;

fprintf('Iter = %d\n', iter);
fprintf('Xmin = %d\n', xmin);
fprintf('Ymin = %d\n', ymin);

end
