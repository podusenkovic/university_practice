function [] = newton( x0, eps, f, df, d2f )

iter = 0;
xmin = x0;
df_xmin  = df(x0);
d2f_xmin = d2f(x0);
xmin = xmin - (df_xmin / d2f_xmin);

while (abs(df_xmin) > eps)
    
    df_xmin  = df(xmin);
    d2f_xmin = d2f(xmin);
    
    xmin = xmin - (df_xmin / d2f_xmin);
    iter = iter + 1;
end

ymin = f(xmin);

fprintf('Iter = %d\n', iter);
fprintf('Xmin = %d\n', xmin);
fprintf('Ymin = %d\n', ymin);

end
