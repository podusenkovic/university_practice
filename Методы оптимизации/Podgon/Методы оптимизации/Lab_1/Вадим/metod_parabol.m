function [] = metod_parabol(x1, x2, x3, eps, f)

f1 = f(x1);
f2 = f(x2);
f3 = f(x3);

xmin = x2;
xmin_old = x1;

iter = 0;

while(abs(xmin-xmin_old) > eps)
    
    a1 = (f2-f1)/(x2-x1);
    a2 = (1/(x3-x2)) * (((f3-f1)/(x3-x1)) - ((f2-f1)/(x2-x1)));
    
    xmin_old = xmin;
    xmin = (x1+x2-(a1/a2))/2;
    ymin = f(xmin);
    
    if(xmin > x2)
        x1 = x2;
        f1 = f2;
        x2 = xmin;
        f2 = ymin;
    else
        x3 = x2;
        f3 = f2;
        x2 = xmin;
        f2 = ymin;
    end
    iter = iter + 1;
end

fprintf('Iter = %d\n', iter);
fprintf('Xmin = %d\n', xmin);
fprintf('Ymin = %d\n', ymin);

end