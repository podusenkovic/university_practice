function [] = chordi( a, b, eps, f, df )

iter = 0;
df_a = df(a);
df_b = df(b);
xmin = a - ((df_a * (a - b)) / (df_a - df_b));
df_xmin = df(xmin);

while(abs(df_xmin) > eps)
    if(df_xmin > 0)
        b = xmin;
        df_b = df_xmin;
    else
        a = xmin;
        df_a = df_xmin;
    end    
    xmin = a - ((df_a * (a - b)) / (df_a - df_b));
    df_xmin = df(xmin);    
    iter = iter + 1;
end

ymin = f(xmin);

fprintf('Iter = %d\n', iter);
fprintf('Xmin = %d\n', xmin);
fprintf('Ymin = %d\n', ymin);

end
