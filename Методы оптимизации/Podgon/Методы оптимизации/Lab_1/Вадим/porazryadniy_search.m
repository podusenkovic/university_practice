function [] = porazryadniy_search( a, b, eps, f )

step = 0.5;%���
x = a;
dir = -1;% �����������

ymin = f(x);
xmin = a;

iter = 0;

while ( step > eps )%���� ��� ������ ��������
    step = step / 2;
    dir  = -dir;
    
    while(1)
        x = x + dir * step;
        y = f(x);
        iter = iter + 1;
        
        if (y <= ymin)
            ymin = y;
            xmin = x;
        end

        if (y > ymin)%���� ������� ������ ���������� - break
            if ( step > eps)%���� �������� �� ���������� - ������������ min 
                xmin = x;
                ymin = y;
            end
            break;
        end
        
        if (x == b)% ���� ����� �� ���� ��������� - break
            xmin = x;
            ymin = y;
            break;
        end
        
        if (x == a)% ���� ����� �� ���� ��������� - break
            xmin = x;
            ymin = y;
            break;
        end
        
    end

end

fprintf('Iter = %d\n', iter);
fprintf('Xmin = %d\n', xmin);
fprintf('Ymin = %d\n', ymin);

end
