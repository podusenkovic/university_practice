function [] = sredniya_tochka( a, b, eps, f, df )

iter = 0;
xmin = (a + b)/2;
df_xmin = df(xmin);

while(abs(df_xmin) > eps)
    if(df_xmin > 0)
        b = xmin;% ������ ������� [a;xmin]
    else
        a = xmin;% ������ ������� [xmin;b]
    end
    
    xmin = (a + b)/2;% ������� ��������� xmin
    df_xmin = df(xmin);    
    iter = iter + 1;
end

ymin = f(xmin);

fprintf('Iter = %d\n', iter);
fprintf('Xmin = %d\n', xmin);
fprintf('Ymin = %d\n', ymin);

end
