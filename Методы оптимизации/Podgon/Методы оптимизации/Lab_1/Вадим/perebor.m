function [] = perebor( a, b, eps, f )
 
n         = ceil((b-a)/eps); %����� �������� ���������
eps       = (b-a)/n; %�������� ����������
xmin      = a;       %��������� X
ymin      = f(xmin); %��������� Y
iter      = 1;       %����� ��������
 
for i = a+eps : eps : b
    x = i;
    y = f(x);
    iter = iter + 1;
    if(y <= ymin)
        ymin = y;
        xmin = x;
    end
end
 
fprintf('Iter = %d\n', iter);
fprintf('Xmin = %d\n', xmin);
fprintf('Ymin = %d\n', ymin);
 
end
