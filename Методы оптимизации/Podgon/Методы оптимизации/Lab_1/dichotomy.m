function [ result , num_of_steps ] = dichotomy( a , b , eps )

num_of_steps = 0;
interval = inf;
delta = eps/4;

while(interval > eps)
    
    x1 = (a + b - delta) / 2;
    x2 = (a + b + delta) / 2;

    if (func(x1) <= func(x2))
        b = x2;
    else
        a = x1;
    end
    
    interval = (b - a) / 2;
    num_of_steps = num_of_steps + 2;
end

x = (a + b) / 2;
result = func(x);

end

