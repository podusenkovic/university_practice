function [ result , num_of_steps ] = parabola( a , b , eps )

num_of_steps = 0;
flag = 0;
it = 0;

while(flag == 0)

    it = it + 1;
    shift = rand(1,1) / 10;
    x1 = a + shift;
    x2 = x1 + shift;
    f1 = func(x1);
    f2 = func(x2);
    while(f1 < f2)
        x2 = x2 + shift;
        f2 = func(x2);
        num_of_steps = num_of_steps + 1;
    end
    x3 = x2 + shift;
    f3 = func(x3);
    while(f2 >= f3)
        x3 = x3 + shift;
        f3 = func(x3);
        num_of_steps = num_of_steps + 1;
    end
    a1 = (f2 - f1) / (x2 - x1);
    a2 = (1 / (x3 - x2)) * (((f3 - f1)) / (x3 - x1)) - ((f2 - f1) / (x2 - x1));
    if(it == 1)
        x_prev = 0.5 * (x1 + x2 - a1 / a2);
    elseif(it == 2)
        x_next = 0.5 * (x1 + x2 - a1 / a2);
    else
        x_prev = x_next;
        x_next = 0.5 * (x1 + x2 - a1 / a2);
    end
    
    if (it > 1)
            if (abs(x_next - x_prev) < eps)
                flag = 1;
            end
    end
    
    num_of_steps = num_of_steps + 2;
    
end
result = func(x_next);
end

