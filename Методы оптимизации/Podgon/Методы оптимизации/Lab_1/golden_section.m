function [ result , num_of_steps ] = golden_section( a , b , eps )

num_of_steps = 0;
flag = 0;
t = (sqrt(5) - 1) / 2;
x2 = a + t * (b - a);
x1 = a + b - x2;
f1 = func(x1);
f2 = func(x2);
    
while(flag == 0)
    if (f1 <= f2)
        b = x2;
        x2 = x1;
        f2 = f1;
        x1 = a + b - x1;
        f1 = func(x1);
    else
        a = x1;
        x1 = x2;
        f1 = f2;
        x2 = a + b - x2;
        f2 = func(x2);
    end
    
    num_of_steps = num_of_steps + 1;
    interval = (b - a) / 2;
    
    if (interval < eps)
        flag = 1;
    end
end

x = (a + b) / 2;
result = func(x);

end

