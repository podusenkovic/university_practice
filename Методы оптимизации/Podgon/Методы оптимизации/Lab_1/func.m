function [ result ] = func( x )
%result = x + 1 / (x^2);
result = exp(x) - 1 - x - x^2 / 2 - x^3 / 6;
end

