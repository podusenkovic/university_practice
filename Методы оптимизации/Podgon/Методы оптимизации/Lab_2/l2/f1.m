function [ y ] = f1( x )
a = 1;
y = x(1).^2 + a * x(2).^2;

end