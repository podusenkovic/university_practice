function [ ymin, xmin ] = grad_descent( x1, x2, alpha, eps )
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here

iterations = 0;
ymin = f(x1,x2);
grad = calculate_grad(x1,x2);

while( grad_norm(grad) > eps )
    
    y1 = x1 - alpha*grad(1);
    y2 = x2 - alpha*grad(2);
    f_y = f(y1, y2);
    
    if (f_y < ymin)
        x1 = y1;
        x2 = y2;
        ymin = f_y;
        grad = calculate_grad(x1,x2);
    else
        alpha = alpha/2;
    end
    iterations = iterations + 1;
end

xmin = [x1, x2];

fprintf('Iterations done: %d', iterations);

end

