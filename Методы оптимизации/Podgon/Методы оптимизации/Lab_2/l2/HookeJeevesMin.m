function [xmin, fmin, iterCnt, fCnt] = HookeJeevesMin(func, x0, delta, gamma, eps)
    iterCnt = 0;
    fCnt = 0;

    x = x0;
    
    while (norm(delta) >= eps)
        iterCnt = iterCnt + 1;
        
        newx = FindMinDir(func, x, delta);
        
        if (newx == x)
            delta = delta ./ gamma;
            continue;
        end
        
        fmin = @(alpha) func(x + alpha*(newx - x));
        [alpha, fCnt1d] = Smartfind(fmin, 0, 100, 1, eps);
        fCnt = fCnt + fCnt1d;
        
        x = x + alpha * (newx - x);
    end
    
    xmin = x;
    fmin = func(x);
end

function [x, fCnt] = FindMinDir(f, x0, delta)
    fCnt = 0;

    n = numel(x0);

    x = x0;
    for j = 1:n
        e = zeros(n, 1);
        e(j) = 1;
        
        y = x - delta(j)*e;
        if (f(y) < f(x))        fCnt = fCnt + 1;
            x = y;
            continue;
        end
        
        y = x + delta(j)*e;
        if (f(y) < f(x))        fCnt = fCnt + 1;
            x = y;
        end
    end
end
