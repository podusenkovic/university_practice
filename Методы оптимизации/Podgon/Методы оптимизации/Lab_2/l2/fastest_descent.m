function [ ymin, xmin ] = fastest_descent( x1, x2, alpha, eps )
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here

iterations = 0;
calls = 0;

grad = calculate_grad(x1,x2);
calls = calls + 2;

while( grad_norm(grad) > eps )
    
    [ alpha, f_y, y1, y2, m_calls ] = minimize_alpha_fdescent( x1, x2, alpha, grad, eps );
    calls = calls + m_calls;
    
    x1 = y1;
    x2 = y2;
    ymin = f_y;
    grad = calculate_grad(x1,x2);
    calls = calls + 2;

    iterations = iterations + 1;
end

xmin = [x1, x2];

fprintf('Iterations done: %d', iterations);
fprintf('\nFunction calls done: %d', calls);
end

