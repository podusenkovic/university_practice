function [xmin, fmin, iterCnt, fCnt] = SimplexMin(x0, simplexSize, eps)
    iterCnt = 0;
    fCnt = 0;

    n = numel(x0);
    
    simplex = SimplexByCenter(x0, simplexSize);
    
    % �������� ������� � �������� ��������
    funcVals = zeros(1, n+1);
    for i = 1:n+1
        funcVals(i) = f1(simplex(:,i));   fCnt = fCnt + 1;
    end
    
    while (simplexSize > eps)
        iterCnt = iterCnt + 1;
        
        % ���������� ��������� ������
        [~, indexes] = sort(funcVals);
        funcVals = funcVals(indexes);
        simplex = simplex(:, indexes);

        foundNewValue = 0;
        
        % ���� �����, ������� ����� ��������
        for j = n+1:-1:2
            summ = zeros(n, 1);
            for k = 1:n+1
                if (k ~= j)
                    summ = summ + simplex(:,k);
                end
            end
            xc = summ / n;

            oldx = simplex(:, j);
            newx = 2 * xc - simplex(:, j);
            if (f1(newx) < f1(oldx))
                simplex(:,j) = newx;
                funcVals(j) = f1(newx);   fCnt = fCnt + 1;
                foundNewValue = 1;
                break;
            end
        end

        % ���� ����� ���, �� ��������� ������ ��������
        if (~foundNewValue)
            simplexSize = simplexSize / 2;
            simplex = SimplexByCenter(SimplexCenter(simplex), simplexSize);
            funcVals = zeros(1, n+1);
            for i = 1:n+1
                funcVals(i) = f1(simplex(:,i));   fCnt = fCnt + 1;
            end
        end
    end
    
    % ��������� ������� ����� ����������� ���������
    summ = zeros(n, 1);
    for k = 1:n+1
        summ = summ + simplex(:,k);
    end
    
    xmin = summ / (n+1);
    fmin = f1(xmin);
end

function points = SimplexByCenter(x0, simplexSize)
    n = numel(x0);
    points = zeros(n, n+1);
    for i = 1:n+1
       for j = 1:n
           if (j < i - 1)
               points(j, i) = x0(j);
           elseif (j == i - 1)
               points(j, i) = x0(j) + sqrt(j/2/(j+1)) * simplexSize;
           else
               points(j, i) = x0(j) - 1 / sqrt(2*j*(j+1)) * simplexSize;
           end
       end
    end
end

function center = SimplexCenter(points)
    n = size(points, 1);
    summ = zeros(n, 1);
    for k = 1:n+1
        summ = summ + points(:,k);
    end
    center = summ / (n+1);
end