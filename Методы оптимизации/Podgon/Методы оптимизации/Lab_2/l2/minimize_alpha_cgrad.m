function [ alpha_min, y_min, y1, y2 ] = minimize_alpha_cgrad( x1, x2, p1, p2, alpha )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
eps = 0.001;

alpha_min = alpha;

y1 = x1 + alpha*p1;
y2 = x2 + alpha*p2;
y_min = f(y1, y2);

delta = 1;
direction = -1;

n = 0;

while ( delta > eps )
    delta = delta / 4;
    direction = - direction;
    
    while(1)
        alpha = alpha + direction * delta;
        
        y1 = x1 + alpha*p1;
        y2 = x2 + alpha*p2;
        y = f(y1, y2);

        
        n = n + 1;
        
        if (y <= y_min)
            y_min = y;
            alpha_min = alpha;
        end

        if (y > y_min)
            if ( delta > eps) 
            alpha_min = alpha;
            y_min = y;
            end
            break;
        end
        
        %if (alpha == b)
        %    alpha_min = alpha;
        %    y_min = y;
        %    break;
        %end
        
        if (alpha == 0)
            alpha_min = alpha;
            y_min = y;
            break;
        end
        
    end

end




end

