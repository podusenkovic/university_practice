function [ grad ] = calculate_grad( x1, x2 )

grad = [df_dx1(x1,x2), df_dx2(x1,x2)];

end

