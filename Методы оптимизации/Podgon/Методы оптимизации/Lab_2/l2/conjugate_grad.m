function [ ymin, xmin ] = conjugate_grad( x1, x2, alpha, eps )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

iterations = 0;

grad = calculate_grad(x1, x2);
p = -grad;


while( grad_norm(grad) > eps )
    [ alpha, ymin, x1, x2 ] = minimize_alpha_cgrad( x1, x2, p(1), p(2), alpha );

    %x1 = x1 + alpha * p(1);
    %x2 = x2 + alpha * p(2);

    grad_old = grad;
    grad = calculate_grad(x1, x2);

    beta = (grad_norm(grad)^2)/(grad_norm(grad_old)^2);

    p(1) = -grad(1) + beta*p(1);
    p(2) = -grad(2) + beta*p(2);
    
    iterations = iterations + 1;
end

xmin = [x1, x2];

fprintf('Iterations done: %d', iterations);


end

