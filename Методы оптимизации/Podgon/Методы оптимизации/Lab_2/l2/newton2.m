function [ ymin, xmin ] = newton2( x1, x2, eps )
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here

iterations = 0;

x(1) = x1;
x(2) = x2;

A = [ df_d2x1(x(1), x(2)),   df_dx1_dx2(x(1), x(2)); ...
      df_dx2_dx1(x(1), x(2)) df_d2x2(x(1),x(2))];

Ainv = inv(A);
grad = calculate_grad(x(1), x(2));

while( grad_norm(grad) > eps )

    x = x - (Ainv*grad')';
    
    
    A = [ df_d2x1(x(1), x(2)),   df_dx1_dx2(x(1), x(2)); ...
      df_dx2_dx1(x(1), x(2)) df_d2x2(x(1),x(2))];

    Ainv = inv(A);
    grad = calculate_grad(x(1), x(2));
    iterations = iterations + 1;
end

ymin = f(x(1), x(2));
xmin = x;

fprintf('Iterations done: %d', iterations);

end

