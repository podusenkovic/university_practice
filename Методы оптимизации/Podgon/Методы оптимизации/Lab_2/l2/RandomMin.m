function [xmin, fmin, iterCnt, fCnt] = RandomMin(func, x0, alpha, gamma, M, eps)
    iterCnt = 0;
    fCnt = 0;

    n = numel(x0);
    x = x0;
    
    tryCnt = 0;
    
    while (alpha > eps)
        iterCnt = iterCnt + 1;
        
        xi = rand(n, 1) * 2 - 1;
        y = x + alpha * (xi / norm(xi));
        if (func(y) < func(x))              fCnt = fCnt + 1;
            x = y;
            tryCnt = 0;
            continue;
        end
        
        tryCnt = tryCnt + 1;
        if (tryCnt >= M)
            alpha = alpha / gamma;
        end
    end
    
    xmin = x;
    fmin = func(x);
end