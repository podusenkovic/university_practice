function [ norm ] = grad_norm( grad )

norm = sqrt(grad(1)^2 + grad(2)^2);

end

