function [xmin, fmin, iterCnt, fCnt] = DescentMin(func, x0, min1d, eps1d, eps)
    iterCnt = 0;
    fCnt = 0;

    n = numel(x0);
    x = x0;
    
    sumPathLength = eps + 1;
    while(sumPathLength > eps)  
        sumPathLength = 0;
        for i = 1:n
            iterCnt = iterCnt + 1;
            
            e = zeros(n, 1);
            e(i) = 1;
            func1d = @(alpha) func(x + alpha * e);
            
            MAX_ALPHA_VALUE = 1000;
            [alpha, fCnt1d] = feval(min1d, func1d, -MAX_ALPHA_VALUE, MAX_ALPHA_VALUE, 0, eps1d);
            fCnt = fCnt + fCnt1d;
            
            x = x + alpha * e;
            sumPathLength = sumPathLength + abs(alpha);
        end
    end
    
    xmin = x;
    fmin = func(x);
end