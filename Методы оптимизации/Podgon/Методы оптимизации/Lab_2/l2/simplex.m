function [ ymin, xmin ] = simplex( x01, x02, l, eps )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

sigma = 2;

n = 2;
x11 = x01 + ((sqrt(n + 1) - 1)*l/(n*sqrt(2)));
x12 = x02 + ((sqrt(n + 1) + n - 1)*l/(n*sqrt(2)));
x21 = x12;
x22 = x11;

f0 = f(x01, x02);
f1 = f(x11, x12);
f2 = f(x21, x22);





while(l >= eps)

    if (max([f0, f1, f2]) == f0)
        x01 = 2*((x11 + x21)/2)-x01;
        x02 = 2*((x12 + x22)/2)-x02;
        f_old = f0;
        f0 = f(x01, x02);

        if(f0 >= f_old)
            l = l/sigma;
            x11 = x01 + ((sqrt(n + 1) - 1)*l/(n*sqrt(2)));
            x12 = x02 + ((sqrt(n + 1) + n - 1)*l/(n*sqrt(2)));
            f1 = f(x11, x12);
            x21 = x12;
            x22 = x11;
            f2 = f(x21, x22);
        end
    end

    if (max([f0, f1, f2]) == f1)
        x11 = 2*((x21 + x01)/2)-x11;
        x12 = 2*((x22 + x02)/2)-x12;
        f_old = f1;
        f1 = f(x11, x12);

        if(f1 >= f_old)
            l = l/sigma;
            x01 = x11 + ((sqrt(n + 1) - 1)*l/(n*sqrt(2)));
            x02 = x12 + ((sqrt(n + 1) + n - 1)*l/(n*sqrt(2)));
            f0 = f(x01, x02);
            x21 = x02;
            x22 = x01; 
            f2 = f(x21, x22);
        end
    end

    if (max([f0, f1, f2]) == f2)
        x21 = 2*((x11 + x01)/2)-x21;
        x22 = 2*((x12 + x02)/2)-x22;
        f_old = f2;
        f2 = f(x21, x22);

        if(f2 >= f_old)
            l = l/sigma; 
            x01 = x11 + ((sqrt(n + 1) - 1)*l/(n*sqrt(2)));
            x02 = x12 + ((sqrt(n + 1) + n - 1)*l/(n*sqrt(2)));
            f0 = f(x01, x02);
            x11 = x02;
            x12 = x01; 
            f1 = f(x11, x12);
        end
    end
    
end

ymin = f0;
xmin = [x01, x02];

end

