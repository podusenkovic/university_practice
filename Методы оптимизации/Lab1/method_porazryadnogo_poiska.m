function [fmin, xmin, N] = method_porazryadnogo_poiska(foo, a, b, eps, output)
    delta = (b - a) / 4; dir = 1;
    fmin = foo(a); xmin = a;
    x = a; N = 1;
    while (delta > eps)
        while(1)
            x = x + dir * delta; y = foo(x); N = N + 1;
            if (y <= fmin)
                xmin = x; fmin = y;
            else
                if (delta > eps)
                    xmin = x; fmin = y;
                end
                break;
            end
            if ((x == b) || (x == a))
                xmin = x; fmin = y;
                break;
            end
        end
        delta = delta / 4;
        dir  = -dir;
    end
    if output
        fprintf('PORAZRYADNII: f = %s; f(%f) = min(f(X)) | X c [%3.1f,%3.1f] = %f; delta = %f; N = %d;\n', sym(foo), xmin, a, b, fmin, delta, N);    
    end
end