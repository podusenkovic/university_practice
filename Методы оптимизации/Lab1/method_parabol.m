function [fmin, xmin, N] = method_parabol(foo, a, b, eps, output)
    [x1, x2, x3, N] = zolotoe_helper(foo, a, b);
    fx1 = foo(x1); fx2 = foo(x2); fx3 = foo(x3);
    N = N + 3;
    iter = 0;
    while (true)
        iter = iter + 1;
        a0 = fx1;
        a1 = (fx2 - fx1)/(x2 - x1);
        a2 = 1/(x3 - x2) * ((fx3 - fx1)/(x3 - x1) - (fx2 - fx1)/(x2 - x1));
        if (iter > 1)
            xmin_prev = xmin;
        end
        xmin = (x1 + x2 - a1/a2)/2;
        fmin = foo(xmin); N = N + 1;
        
        if (x1 < xmin && xmin < x2)
            if (fmin >= fx2)
                x1 = xmin; fx1 = fmin;
            else 
                x3 = x2; fx3 = fx2;
                x2 = xmin; fx2 = fmin;
            end
        elseif (x2 < xmin && xmin < x3)
            if (fmin >= fx2)
                x3 = xmin; fx3 = fmin;              
            else 
                x1 = x2; fx1 = fx2;
                x2 = xmin; fx2 = fmin;
            end
        end
        
        if (iter >= 2 && abs(xmin - xmin_prev) < eps)
            break;
        end
    end
    if output
        fprintf('PARABOLKA   : f = %s; f(%f) = min(f(X)) | X c [%3.1f,%3.1f] = %f; delta = %f; N = %d;\n', sym(foo), xmin, a, b, fmin, abs(xmin - xmin_prev), N);
    end
end


function [x1, x2, x3, N] = zolotoe_helper(foo, a, b)
    tau = (sqrt(5) - 1) / 2;
    N = 0;
    x1 = a + (1 - tau) * (b - a); 
    fx1 = foo(x1); N = N + 1;
    x2 = a + tau * (b - a);
    fx2 = foo(x2); N = N + 1;
    while(true)
        if (fx1 <= fx2)
            b = x2; fb = fx2;
            x2 = x1; fx2 = fx1;
            x1 = b - tau * (b - a);
            fx1 = foo(x1); N = N + 1;
            if (fx1 >= fx2 && fx2 <= fb && x1 < x2 && x2 < b)
                x1 = x1; x2 = x2; x3 = b;
                return;
            end
        else
            a = x1; fa = fx1;
            x1 = x2; fx1 = fx2;
            x2 = b - (1 - tau) * (b - a);
            fx2 = foo(x2); N = N + 1;
            if (fa >= fx1 && fx1 <= fx2 && a < x1 && x1 < x2)
                x3 = x2; x2 = x1; x1 = a;
                return;
            end
        end
    end
end