function [fmin, xmin, N, rashoditsa] = method_newtone(foo, a, b, eps, output, diff_type, dx)
    N = 0;  
        
    if (diff_type == 0)
        dfoo = matlabFunction(diff(sym(foo)));
        d2foo = matlabFunction(diff(sym(foo), 2)); 
    elseif (diff_type == 1)
        dfoo = @(x)(foo(x) - foo(x - dx))/dx;
        d2foo = @(x)(foo(x + dx) - 2 * foo(x) + foo(x - dx))/(dx * dx);
    elseif (diff_type == 2)
        dfoo = @(x)(foo(x + dx) - foo(x))/dx;
        d2foo = @(x)(foo(x + dx) - 2 * foo(x) + foo(x - dx))/(dx * dx);
    elseif (diff_type == 3)
        dfoo = @(x)(foo(x + dx) - foo(x - dx))/(2 * dx);
        d2foo = @(x)(foo(x + dx) - 2 * foo(x) + foo(x - dx))/(dx * dx);
    end
    
    
    xmin = (b - a) / 2 + a;
    rashoditsa = 0;
    
    while(true)
        dfx = dfoo(xmin); N = N + 1;
        if (abs(dfx) <= eps)
            break;
        end
        d2fx = d2foo(xmin); N = N + 1;
        xmin = xmin - dfx/d2fx;
        if (xmin < a || xmin > b )
            if rashoditsa == 3
                break;
            end
            rashoditsa = rashoditsa + 1;
        end
    end
    
    fmin = foo(xmin); N = N + 1;
    if (rashoditsa ~= 3)
        if output
            fprintf('NEWTONE     : f = %s; f(%f) = min(f(X)) | X c [%3.1f,%3.1f] = %f; df/dx(x) = %s; d2f/dx2(x) = %s; N = %d;\n', sym(foo), xmin, a, b, fmin, sym(dfoo), sym(d2foo), N); 
        end
        rashoditsa = 0;
    else
        if output
            fprintf('NEWTONE     : f = %s; Rashoditsa pri X c [%3.1f,%3.1f] | Nach pribl = %3.1f\n', sym(foo), a, b, (b - a) / 2 + a);
        end
        rashoditsa = 1;
    end
end