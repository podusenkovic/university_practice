function [fmin, xmin, N] = method_dihotomii(foo, a, b, eps, output)
    delta = 0.2 * eps; N = 0;
    na = a; nb = b;
    while (abs((nb - na) / 2) >= eps)
        x1 = (nb + na - delta) / 2; x2 = (nb + na + delta) / 2;
        f1 = foo(x1);               f2 = foo(x2);
        N = N + 2;
        if (f1 <= f2)
            nb = x2;
        else
            na = x1;
        end
    end
    xmin = (na + nb) / 2;
    fmin = foo(xmin);
    N = N + 1;
    if output
        fprintf('DIHOTOMIA   : f = %s; f(%f) = min(f(X)) | X c [%3.1f,%3.1f] = %f; delta = %f; N = %d;\n', sym(foo), xmin, a, b, fmin, delta, N);
    end
end