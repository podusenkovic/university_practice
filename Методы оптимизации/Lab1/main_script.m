clear, clc, close all

%---------------------#1,2--------------------------------
eps = 10E-3; m_ten_eps = -3;
foo = @(x)(x.^2 - 2*x + exp(-x)); a = -1; b = 1.5;
%foo = @(x)(x.^3 - 3*sin(x)); a = 0; b = 1;
%foo = @(x)(x.^4 + x.^2 + x + 1); a = -1; b = 0;
disp('���������� ����������� �������. ������ ������');
method_perebora(foo, a, b, eps, 1);
method_porazryadnogo_poiska(foo, a, b, eps, 1);
method_dihotomii(foo, a, b, eps, 1);
method_zolotogo_sechenia(foo, a, b, eps, 1);
method_parabol(foo, a, b, eps, 1);
disp('������, ������������ ���������� � ����������� ������� �������')
method_srednei_tochki(foo, a, b, eps, 1, 0);
method_hord(foo, a, b, eps, 1, 0);
method_newtone(foo, a, b, eps, 1, 0);

%---------------------#3--------------------------------
ezplot(foo, [a b]);
f_id = fopen('task3.csv','w');
fprintf(f_id, "�����;��������;���������� ���������� �������(df,d2f)\n");
for eps = (10 .^ (-1:-1:m_ten_eps))
    [fmin, fmax, N] = method_perebora(foo, a, b, eps, 0); 
    fprintf(f_id, "Perebor;%10f;%d\n", eps, N);
end
fprintf(f_id, "\n");

for eps = (10 .^ (-1:-1:m_ten_eps))
    [fmin, fmax, N] = method_porazryadnogo_poiska(foo, a, b, eps, 0); 
    fprintf(f_id, "Porazryadnii poisk;%10f;%d\n", eps, N);
end
fprintf(f_id, "\n");

for eps = (10 .^ (-1:-1:m_ten_eps))
    [fmin, fmax, N] = method_dihotomii(foo, a, b, eps, 0); 
    fprintf(f_id, "Dihotomia;%10f;%d\n", eps, N);
end
fprintf(f_id, "\n");

for eps = (10 .^ (-1:-1:m_ten_eps))
    [fmin, fmax, N] = method_zolotogo_sechenia(foo, a, b, eps, 0); 
    fprintf(f_id, "Zolotoe sechenie;%10f;%d\n", eps, N);
end
fprintf(f_id, "\n");

for eps = (10 .^ (-1:-1:m_ten_eps))
    [fmin, fmax, N] = method_parabol(foo, a, b, eps, 0); 
    fprintf(f_id, "Paraboli;%10f;%d\n", eps, N);
end
fprintf(f_id, "\n");

for eps = (10 .^ (-1:-1:m_ten_eps))
    [fmin, fmax, N] = method_srednei_tochki(foo, a, b, eps, 0, 0); 
    fprintf(f_id, "Srednei tochki;%10f;%d\n", eps, N);
end
fprintf(f_id, "\n");

for eps = (10 .^ (-1:-1:m_ten_eps))
    [fmin, fmax, N] = method_hord(foo, a, b, eps, 0, 0); 
    fprintf(f_id, "Hord;%10f;%d\n", eps, N);
end
fprintf(f_id, "\n");

for eps = (10 .^ (-1:-1:m_ten_eps))
    [fmin, fmax, N] = method_newtone(foo, a, b, eps, 0, 0); 
    fprintf(f_id, "Newtone;%10f;%d\n", eps, N);
end
fprintf(f_id, "\n");

fclose(f_id);

%---------------------#4--------------------------------
appr_names = ["", ", ������ ��������", ", ����������� ��������", ", ����� ��������"];
f_id = fopen('task4.csv','w');
fprintf(f_id, "�����;��������;��� dx;���������� ���������� �������(df,d2f)\n");

for appr = 1:length(appr_names)
    for eps = (10 .^ (-1:-1:m_ten_eps))
        if (appr == 1)
            [fmin, fmax, N] = method_srednei_tochki(foo, a, b, eps, 0, appr - 1); 
            fprintf(f_id, "Newtone%s;%10f;-;%d\n", appr_names(appr), eps, N);
            continue;
        end
        for dx = (10 .^ (-1:-1:(log10(eps) + 1)))
            [fmin, fmax, N] = method_srednei_tochki(foo, a, b, eps, 0, appr - 1, dx); 
            fprintf(f_id, "Srednei tochki%s;%10f;%10f;%d\n", appr_names(appr), eps, dx, N);
        end
    end
end
fprintf(f_id, "\n");

for appr = 1:length(appr_names)
    for eps = (10 .^ (-1:-1:m_ten_eps))
        if (appr == 1)
            [fmin, fmax, N] = method_hord(foo, a, b, eps, 0, appr - 1); 
            fprintf(f_id, "Newtone%s;%10f;-;%d\n", appr_names(appr), eps, N);
            continue;
        end
        for dx = (10 .^ (-1:-1:(log10(eps) + 1)))
            [fmin, fmax, N] = method_hord(foo, a, b, eps, 0, appr - 1, dx); 
            fprintf(f_id, "Hord%s;%10f;%10f;%d\n", appr_names(appr), eps, dx, N);
        end
    end
end
fprintf(f_id, "\n");

for appr = 1:length(appr_names)
    for eps = (10 .^ (-1:-1:m_ten_eps))
        if (appr == 1)
            [fmin, fmax, N] = method_newtone(foo, a, b, eps, 0, appr - 1); 
            fprintf(f_id, "Newtone%s;%10f;-;%d\n", appr_names(appr), eps, N);
            continue;
        end
        for dx = (10 .^ (-1:-1:(log10(eps) + 1)))
            [fmin, fmax, N] = method_newtone(foo, a, b, eps, 0, appr - 1, dx); 
            fprintf(f_id, "Newtone%s;%10f;%10f;%d\n", appr_names(appr), eps, dx, N);
        end
    end
end
fprintf(f_id, "\n");
fclose(f_id);

%---------------------#5--------------------------------
foo = @(x)(x * atan(x) - (log(1 + x.^2)) / 2);
figure
ezplot(foo)
a = -1; b = 1.1;
[fmin, fmax, N] = method_newtone(foo, a, b, eps, 1, 0, dx);
a = -1; b = 5;
[fmin, fmax, N] = method_newtone(foo, a, b, eps, 1, 0, dx);
a = -1; b = 10;
[fmin, fmax, N] = method_newtone(foo, a, b, eps, 1, 0, dx);

rash = 1; a = -10; b = -9.9;
while(true)
    [fmin, fmax, N, rash] = method_newtone(foo, a, b, eps, 0, 0, dx);
    if (~rash)
        break;
    end
    a = a + 0.1; b = b + 0.1;
end
left_nach_pribl = (b - a) / 2 + a;
while(true)
    [fmin, fmax, N, rash] = method_newtone(foo, a, b, eps, 0, 0, dx);
    if (rash)
        break;
    end
    a = a + 0.1; b = b + 0.1;
end
right_nach_pribl = (b - a) / 2 + a;
fprintf('Diapazon znachenii chtobi ne rashodilas f = %s : X c [%3.1f,%3.1f]\n', sym(foo), left_nach_pribl, right_nach_pribl);

a = -1; b = 1.1;
[fmin, fmax, N] = method_newtone_rafson(foo, a, b, eps, 1, 0, dx);
a = -1; b = 5;
[fmin, fmax, N] = method_newtone_rafson(foo, a, b, eps, 1, 0, dx);
a = -1; b = 10;
[fmin, fmax, N] = method_newtone_rafson(foo, a, b, eps, 1, 0, dx);

a = -1; b = 1.1;
[fmin, fmax, N] = method_markvardt(foo, a, b, eps, 1, 0, dx);
a = -1; b = 5;
[fmin, fmax, N] = method_markvardt(foo, a, b, eps, 1, 0, dx);
a = -1; b = 10;
[fmin, fmax, N] = method_markvardt(foo, a, b, eps, 1, 0, dx);

%---------------------#6--------------------------------

