function [fmin, xmin, N] = method_perebora(foo, a, b, eps, output)
    [fmin, xmin] = min(foo(a:eps:b));
    xmin = (a + eps * xmin);
    N = length(a:eps:b);
    if output
        fprintf('PEREBOR     : f = %s; f(%f) = min(f(X)) | X c [%3.1f,%3.1f] = %f; eps = %f; N = %d;\n', sym(foo), xmin, a, b, fmin, eps, length(a:eps:b));
    end
end