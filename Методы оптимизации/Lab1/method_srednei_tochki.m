function [fmin, xmin, N] = method_srednei_tochki(foo, a, b, eps, output, diff_type, dx)
    N = 0;
    if (diff_type == 0)
        dfoo = matlabFunction(diff(sym(foo)));
    elseif (diff_type == 1)
        dfoo = @(x)(foo(x) - foo(x - dx))/dx;
    elseif (diff_type == 2)
        dfoo = @(x)(foo(x + dx) - foo(x))/dx;
    elseif (diff_type == 3)
        dfoo = @(x)(foo(x + dx) - foo(x - dx))/(2 * dx);
    end
    
    na = a; nb = b;
       
    while (true)
        xmin = (na + nb)/2; dfmin = dfoo(xmin);
        N = N + 1;
        if (abs(dfmin) <= eps)
            break;
        end
        if (dfmin > 0)
            nb = xmin;
        else
            na = xmin;
        end
    end
    fmin = foo(xmin); N = N + 1;
    if output
        fprintf('SR.TOCHKA   : f = %s; f(%f) = min(f(X)) | X c [%3.1f,%3.1f] = %f; df/dx(x) = %s; N = %d;\n', sym(foo), xmin, a, b, fmin, sym(dfoo), N);
    end
end
