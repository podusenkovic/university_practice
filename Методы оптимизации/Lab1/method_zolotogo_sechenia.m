function [fmin, xmin, N] = method_zolotogo_sechenia(foo, a, b, eps, output)
    na = a;
    nb = b;
    tau = (sqrt(5) - 1) / 2;
    eps_n = (nb - na) / 2; 
    N = 0;
    
    x1 = na + (1 - tau) * (nb - na); 
    fx1 = foo(x1); N = N + 1;
    x2 = na + tau * (nb - na);
    fx2 = foo(x2); N = N + 1;
    while(eps_n > eps)
        if (fx1 <= fx2)
            nb = x2; 
            x2 = x1;
            fx2 = fx1;
            x1 = nb - tau * (nb - na);
            fx1 = foo(x1); N = N + 1;
        else
            na = x1; 
            x1 = x2;
            fx1 = fx2;
            x2 = nb - (1 - tau) * (nb - na);
            fx2 = foo(x2); N = N + 1;                        
        end
        eps_n = eps_n * tau;
    end
    xmin = (na + nb) / 2;
    fmin = (fx1 + fx2) / 2;
    if output
        fprintf('ZOLOTOE SECH: f = %s; f(%f) = min(f(X)) | X c [%3.1f,%3.1f] = %f; eps_n = %f; N = %d;\n', sym(foo), xmin, a, b, fmin, eps_n, N);    
    end
end